package org.ramisme.pringles;

import java.io.File;

import net.minecraft.client.Minecraft;

import org.ramisme.pringles.events.manager.EventManager;
import org.ramisme.pringles.handlers.BindsHandler;
import org.ramisme.pringles.handlers.FontHandler;
import org.ramisme.pringles.handlers.FriendsHandler;
import org.ramisme.pringles.handlers.GuiHandler;
import org.ramisme.pringles.io.files.CommandFile;
import org.ramisme.pringles.io.files.ModulesFile;
import org.ramisme.pringles.modules.manager.CommandManager;
import org.ramisme.pringles.modules.manager.ModuleManager;

/**
 * Pringles' main class.
 * 
 * @author Ramisme
 * @since Apr 3, 2013
 */
public final class Pringles {
	private static final File directory = new File(Minecraft.getMinecraftDir(),
			"pringles");
	// private static final ChatClient client = new ChatClient(
	// "srv.stl-missouri.com", 6969);

	private static final Pringles INSTANCE = new Pringles();
	private final Factory factory;

	public static Pringles getInstance() {
		return INSTANCE;
	}

	private Pringles() {
		if (!this.directory.exists()) {
			this.directory.mkdirs();
		}

		this.factory = new Factory() {
			private final CommandManager command = new CommandManager();
			private final EventManager event = new EventManager();
			private final ModuleManager module = new ModuleManager();
			private final FontHandler font = new FontHandler();

			@Override
			public CommandManager getCommandManager() {
				return this.command;
			}

			@Override
			public EventManager getEventManager() {
				return this.event;
			}

			@Override
			public ModuleManager getModuleManager() {
				return this.module;
			}

			@Override
			public FontHandler getFontHandler() {
				return font;
			}
		};
	}

	public void onLoad() {
		this.factory.getModuleManager().onLoad();
		FriendsHandler.getInstance().onLoad();
		BindsHandler.getInstance().onLoad();
		GuiHandler.getInstance().onLoad();
		CommandFile.getInstance().save();
		ModulesFile.getInstance().save();
	}

	public File getDirectory() {
		return this.directory;
	}

	public Factory getFactory() {
		return this.factory;
	}

}
