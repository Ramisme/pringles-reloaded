package org.ramisme.pringles.events.keyboard;

import org.ramisme.pringles.events.Event;

public class KeyEvent extends Event {

	private final int keyCode;

	public KeyEvent(final int keyCode) {
		this.keyCode = keyCode;
	}

	public int getKeyCode() {
		return keyCode;
	}

}
