package org.ramisme.pringles.events.packet;

import net.minecraft.src.Packet;

import org.ramisme.pringles.events.Event;

public final class PacketReadEvent extends Event {
	private final Packet packet;

	public PacketReadEvent(final Packet packet) {
		this.packet = packet;
	}

	public Packet getPacket() {
		return this.packet;
	}

}
