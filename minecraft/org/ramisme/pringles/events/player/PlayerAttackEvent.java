package org.ramisme.pringles.events.player;

import net.minecraft.src.Entity;

import org.ramisme.pringles.events.Event;

public class PlayerAttackEvent extends Event {
	private final Entity entity;

	public PlayerAttackEvent(final Entity entity) {
		this.entity = entity;
	}

	public Entity getEntity() {
		return entity;
	}

}
