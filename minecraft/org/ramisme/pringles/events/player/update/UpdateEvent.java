package org.ramisme.pringles.events.player.update;

import org.ramisme.pringles.events.Event;

/**
 * Update events. (This is a template. All update events will look the same.)
 * 
 * @author Ramisme
 * 
 */

public class UpdateEvent extends Event {

}
