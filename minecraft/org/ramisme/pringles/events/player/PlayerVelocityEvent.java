package org.ramisme.pringles.events.player;

import org.ramisme.pringles.events.Event;

/**
 * Velocity event. Specifically for anti-knockback
 * 
 * @author Ramisme
 * 
 */

public class PlayerVelocityEvent extends Event {

}
