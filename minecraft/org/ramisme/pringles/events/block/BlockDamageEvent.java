package org.ramisme.pringles.events.block;

import org.ramisme.pringles.events.Event;

public class BlockDamageEvent extends Event {
	private final int[] coords = new int[3];
	private int hitDelay = 5;
	private float damageModifier = 1;

	public BlockDamageEvent(final int xPos, final int yPos, final int zPos) {
		setCoords(xPos, yPos, zPos);
	}

	public float getDamageModifier() {
		return damageModifier;
	}

	public void setDamageModifier(final float modifier) {
		damageModifier = modifier;
	}

	public void setHitDelay(final int hitDelay) {
		this.hitDelay = hitDelay;
	}

	public int getHitDelay() {
		return hitDelay;
	}

	public int[] getBlockCoords() {
		return coords;
	}

	public void setCoords(final int xPos, final int yPos, final int zPos) {
		coords[0] = xPos;
		coords[1] = yPos;
		coords[2] = zPos;
	}

}
