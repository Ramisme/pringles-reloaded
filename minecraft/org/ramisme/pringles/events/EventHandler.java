package org.ramisme.pringles.events;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Methods that are called by events must use this annotation.
 * 
 * @author Ramisme
 * @since Apr 3, 2013
 * 
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface EventHandler {

	EventPriority priority() default EventPriority.NORMAL;

}
