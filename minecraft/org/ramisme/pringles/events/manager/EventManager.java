package org.ramisme.pringles.events.manager;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.ramisme.pringles.events.Event;
import org.ramisme.pringles.events.EventHandler;
import org.ramisme.pringles.events.EventListener;
import org.ramisme.pringles.events.EventPriority;
import org.ramisme.pringles.modules.Module;

public class EventManager {
	private static final Map<EventListener, List<Method>> eventMethods = new HashMap<EventListener, List<Method>>();

	public void clearListeners() {
		synchronized (eventMethods) {
			eventMethods.clear();
		}
	}

	public void registerListener(final EventListener listener) {
		final List<Method> methodsList = new ArrayList<Method>();
		for (final Method method : listener.getClass().getMethods()) {
			if (method.isAnnotationPresent(EventHandler.class)
					&& method.getParameterTypes().length == 1) {
				System.out.println("Event registered at: "
						+ listener.getClass().getSimpleName() + "#"
						+ method.getName());
				methodsList.add(method);
			}
		}

		synchronized (eventMethods) {
			eventMethods.put(listener, methodsList);
		}
	}

	public void sendEvent(final Event event) {
		synchronized (eventMethods) {
			for (EventPriority priority : EventPriority.values()) {
				prepareEvent(event, priority);
			}
		}
	}

	private void prepareEvent(final Event event, final EventPriority priority) {
		final Map<Method, EventListener> methods = new HashMap<Method, EventListener>();

		synchronized (eventMethods) {
			for (final Entry<EventListener, List<Method>> entry : eventMethods
					.entrySet()) {
				for (final Method method : entry.getValue()) {
					if (method.getAnnotation(EventHandler.class).priority() != priority) {
						continue;
					}

					if (event.getClass().isAssignableFrom(
							method.getParameterTypes()[0])) {
						if (entry.getKey() instanceof Module
								&& !((Module) entry.getKey()).isEnabled()) {
							continue;
						}

						methods.put(method, entry.getKey());
					}

				}
			}
		}

		if (methods.isEmpty()) {
			return;
		}

		for (final Entry<Method, EventListener> entry : methods.entrySet()) {
			try {
				entry.getKey().invoke(entry.getValue(), event);
			} catch (final Exception e) {
				e.printStackTrace();
			}
		}
	}

	private boolean doesMethodMatch(final Event event, final Method method,
			final EventPriority priority) {
		if (!method.isAnnotationPresent(EventHandler.class)) {
			return false;
		}

		if (!event.getClass().isAssignableFrom(method.getParameterTypes()[0])) {
			return false;
		}

		EventHandler eventHandler = (EventHandler) method
				.getAnnotation(EventHandler.class);
		if (eventHandler.priority() != priority) {
			return false;
		}

		return true;
	}

}
