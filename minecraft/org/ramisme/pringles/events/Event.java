package org.ramisme.pringles.events;

/**
 * Any class that should be able to be fired remotely should extend this class.
 * 
 * @author Ramisme
 * @since Apr 3, 2013
 * 
 */
public class Event {
	private boolean isCancelled;

	public boolean isCancelled() {
		return isCancelled;
	}

	public void setCancelled(final boolean flag) {
		isCancelled = flag;
	}

}
