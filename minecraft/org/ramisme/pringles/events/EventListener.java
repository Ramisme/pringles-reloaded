package org.ramisme.pringles.events;

/**
 * Any class that should listen for event calls must implement this interface.
 * 
 * @author Ramisme
 * @since Apr 3, 2013
 * 
 */
public interface EventListener {

}
