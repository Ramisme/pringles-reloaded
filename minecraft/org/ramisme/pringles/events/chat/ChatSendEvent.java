package org.ramisme.pringles.events.chat;

import org.ramisme.pringles.events.Event;

public class ChatSendEvent extends Event {
	private String message;

	public ChatSendEvent(final String par1) {
		message = par1;
	}

	public String getMessage() {
		return message;
	}

	// Most likely won't ever be used, but is
	// still here just in case.
	public void setMessage(final String message) {
		this.message = message;
	}

}
