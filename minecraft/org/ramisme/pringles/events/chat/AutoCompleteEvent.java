package org.ramisme.pringles.events.chat;

import org.ramisme.pringles.events.Event;

public final class AutoCompleteEvent extends Event {
	private final String[] response;
	
	public AutoCompleteEvent(final String[] response) {
		this.response = response;
	}
	
	public String[] getResponse() {
		return this.response;
	}
}
