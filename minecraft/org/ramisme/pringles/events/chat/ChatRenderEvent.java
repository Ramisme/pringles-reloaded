package org.ramisme.pringles.events.chat;

import org.ramisme.pringles.events.Event;

public class ChatRenderEvent extends Event {
	private String message;

	public ChatRenderEvent(final String message) {
		this.message = message;
	}

	public void setMessage(final String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

}
