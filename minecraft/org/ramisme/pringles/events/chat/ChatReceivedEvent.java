package org.ramisme.pringles.events.chat;

import org.ramisme.pringles.events.Event;

/**
 * Handles all incoming chat packets.
 * 
 * @author Ramisme
 * 
 */
public class ChatReceivedEvent extends Event {
	private String message = "message";

	public ChatReceivedEvent(final String par1) {
		message = par1;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(final String par1Str) {
		message = par1Str;
	}

}
