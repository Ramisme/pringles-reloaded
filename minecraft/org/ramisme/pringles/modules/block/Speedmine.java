package org.ramisme.pringles.modules.block;

import net.minecraft.src.Block;
import net.minecraft.src.ItemStack;
import net.minecraft.src.Packet16BlockItemSwitch;

import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.events.EventHandler;
import org.ramisme.pringles.events.EventListener;
import org.ramisme.pringles.events.block.BlockDamageEvent;
import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.modules.addons.ModuleCategory;

/**
 * Speed up the block breaking process.
 * 
 * @author Ramisme
 * @since Apr 23, 2013
 * 
 */
public class Speedmine extends Module implements EventListener {

	public Speedmine() {
		super(ModuleCategory.BLOCK, "Speedmine", "M");
		this.setModuleAuthor("Ramisme");

		Pringles.getInstance().getFactory().getEventManager()
				.registerListener(this);
	}

	@EventHandler
	public void onBlockDamageEvent(final BlockDamageEvent event) {
		final int[] coords = event.getBlockCoords();
		autoTool(coords[0], coords[1], coords[2]);
		event.setHitDelay(0);
		event.setDamageModifier(getBlockModifier(event.getBlockCoords()));
	}

	/**
	 * Return the necessary damage for breaking the specified block.
	 * 
	 * @author Stul Missouri
	 * @param blockCoords
	 * @return
	 */
	private float getBlockModifier(int[] blockCoords) {
		final int x = (int) blockCoords[0];
		final int y = (int) blockCoords[0];
		final int z = (int) blockCoords[0];
		final Block block = Block.blocksList[getWrapper().getWorld()
				.getBlockId(x, y, z)];
		if (block == null) {
			return 1;
		}

		final double damage = 1 / (1 + getWrapper().getPlayer().inventory
				.getStrVsBlock(block));
		final double hardness = 1 / (1 + block.getBlockHardness(getWrapper()
				.getWorld(), x, y, z));
		double root = Math.sqrt((damage * damage) + (hardness * hardness));
		root *= 0.8;

		return ((float) Math.max(1, 1 + root));
	}

	/**
	 * Determine the best tool for damaging the current block.
	 * 
	 * @param posX
	 * @param posY
	 * @param posZ
	 */
	private void autoTool(final int posX, final int posY, final int posZ) {
		double damage = 1;
		int itemSlot = -1;

		final int blockId = getWrapper().getWorld()
				.getBlockId(posX, posY, posZ);
		final Block block = Block.blocksList[blockId];
		if (block == null) {
			return;
		}

		for (int slot = 0; slot < 9; slot++) {
			final ItemStack itemStack = getWrapper().getPlayer().inventory.mainInventory[slot];
			if (itemStack == null) {
				continue;
			}

			final double curDamage = itemStack.getStrVsBlock(block);
			if (curDamage > damage) {
				damage = curDamage;
				itemSlot = slot;
			}
		}

		if (itemSlot > -1) {
			getWrapper().getPlayer().inventory.currentItem = itemSlot;
			getWrapper().getPlayerController().updateController();
			getWrapper().sendPacket(new Packet16BlockItemSwitch(itemSlot));
		}
	}

}
