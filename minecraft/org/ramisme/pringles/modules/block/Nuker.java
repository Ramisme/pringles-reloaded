package org.ramisme.pringles.modules.block;

import net.minecraft.src.Block;
import net.minecraft.src.Packet14BlockDig;

import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.events.EventHandler;
import org.ramisme.pringles.events.EventListener;
import org.ramisme.pringles.events.block.BlockClickEvent;
import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.modules.addons.ModuleCategory;
import org.ramisme.pringles.threads.manager.ThreadManager;

/**
 * Destroys blocks around you
 * 
 * @author Ramisme
 * @since Apr 21, 2013
 */
public class Nuker extends Module implements EventListener {

	public Nuker() {
		super(ModuleCategory.BLOCK, "Nuker", "NONE");

		Pringles.getInstance().getFactory().getEventManager()
				.registerListener(this);
	}

	@EventHandler
	public void onClickEvent(final BlockClickEvent event) {
		final int[] coords = event.getBlockCoords();
		final Block block = Block.blocksList[getWrapper().getWorld()
				.getBlockId(coords[0], coords[1], coords[2])];

		ThreadManager.getInstance().executeThread(new Nuke());
		event.setCancelled(true);
	}

	private final class Nuke implements Runnable {

		@Override
		public void run() {
			final int range = 6;
			for (int x = -range; x < range; x++) {
				for (int y = -range; y < range; y++) {
					for (int z = -range; z < range; z++) {
						final int posX = (int) (x + getWrapper().getPlayer().posX), posY = (int) (y + getWrapper()
								.getPlayer().posY), posZ = (int) (z + getWrapper()
								.getPlayer().posZ);
						final int blockId = getWrapper().getWorld().getBlockId(
								posX, posY, posZ);
						if (blockId > 0) {
							getWrapper()
									.sendPacket(
											new Packet14BlockDig(0, posX, posY,
													posZ, 1));
						}
					}
				}
			}
		}

	}

}
