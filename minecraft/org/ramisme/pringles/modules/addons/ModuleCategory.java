package org.ramisme.pringles.modules.addons;

/**
 * Categories for modules.
 * 
 * @author Ramisme
 * @since Mar 19, 2013
 */
public enum ModuleCategory {
	
	MOVEMENT(0xFF13C422), COMBAT(0xFFA9A9A9), WORLD(0xFF6495ED), RENDER(
	        0xFFD9D9D9), DERP(0xFF3FD987), BLOCK(0xFF2899CD), CHAT(), MISC(), COMMAND(), NONE(-1, false);
	
	private int	    color;
	private boolean	gui;
	
	private ModuleCategory() {
		color = -1;
		gui = false;
	}
	
	private ModuleCategory(final int color) {
		this.color = color;
		gui = true;
	}
	
	private ModuleCategory(final int color, final boolean gui) {
		this.color = color;
		this.gui = gui;
	}
	
	public boolean getInGui() {
		return gui;
	}
	
	public int getModuleColor() {
		return color;
	}
	
}