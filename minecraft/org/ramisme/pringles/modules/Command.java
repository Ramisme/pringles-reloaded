package org.ramisme.pringles.modules;

/**
 * Used for modules designated specifically for a single command.
 * 
 * @author ramisme
 * 
 */
public interface Command {

	/**
	 * Fired on command execution
	 * 
	 * @param command
	 *            Full command
	 * @param arguments
	 *            Command arguments
	 */
	public boolean onCommand(final String command, final String arguments[]);

	/**
	 * Returns the proper usage of the command.
	 * 
	 * @return
	 */
	public String getUsage();

	/**
	 * Returns the command prefix.
	 * 
	 * @return
	 */
	public String getCommand();

}
