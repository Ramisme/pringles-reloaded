package org.ramisme.pringles.modules.chat;

import net.minecraft.src.GuiNewChat;

import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.modules.addons.ModuleCategory;
import org.ramisme.pringles.ui.ingame.ChatUI;

/**
 * Ingame chat modifications.
 * 
 * @author Ramisme
 * @since Apr 19, 2013
 * 
 */
public class Chat extends Module {

	public Chat() {
		super(ModuleCategory.CHAT, "Chat", "NONE");
		this.setModuleAuthor("Lynxaa");
		this.setInGui(false);
		this.setModuleState(true);
	}

	@Override
	public void onEnable() {
		getWrapper().getMinecraft().ingameGUI.persistantChatGUI = new ChatUI();
	}

	@Override
	public void onDisable() {
		getWrapper().getMinecraft().ingameGUI.persistantChatGUI = new GuiNewChat(
				getWrapper().getMinecraft());
	}

}
