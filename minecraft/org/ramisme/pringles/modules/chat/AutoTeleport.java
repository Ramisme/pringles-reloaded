package org.ramisme.pringles.modules.chat;

import java.util.Map.Entry;
import java.util.regex.Pattern;

import net.minecraft.src.StringUtils;

import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.events.EventHandler;
import org.ramisme.pringles.events.EventListener;
import org.ramisme.pringles.events.chat.ChatReceivedEvent;
import org.ramisme.pringles.handlers.FriendsHandler;
import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.modules.addons.ModuleCategory;

/**
 * Automatically accept teleport requests from friends.
 * 
 * @author Ramisme
 * @since Apr 6, 2013
 * 
 */
public class AutoTeleport extends Module implements EventListener {
	private static final String REGEX = "(([0-9a-zA-Z_]+) has requested to teleport to you.|([0-9a-zA-Z_]+) has requested that you teleport to them.)";

	public AutoTeleport() {
		super(ModuleCategory.CHAT, "AutoTeleport", "NONE");
		this.setInGui(false);
		this.setModuleState(true);

		Pringles.getInstance().getFactory().getEventManager()
				.registerListener(this);
	}

	@EventHandler
	public void onChatReceived(final ChatReceivedEvent event) {
		final String message = StringUtils
				.stripControlCodes(event.getMessage());
		if (!Pattern.compile(REGEX, Pattern.CASE_INSENSITIVE).matcher(message)
				.find()) {
			return;
		}

		for (final Entry<String, String> entry : FriendsHandler.getInstance()
				.getFriendsList().entrySet()) {
			if (Pattern.compile(entry.getKey(), Pattern.CASE_INSENSITIVE)
					.matcher(message).find()) {
				getChatHandler().sendChat("/tpaccept");
				break;
			}
		}

	}

}
