package org.ramisme.pringles.modules;

import org.lwjgl.input.Keyboard;
import org.ramisme.pringles.Logger;
import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.Wrapper;
import org.ramisme.pringles.handlers.ChatHandler;
import org.ramisme.pringles.modules.addons.ModuleCategory;

/**
 * Basic module extended only when created specific modular objects.
 * 
 * @author Ramisme
 * @since Apr 6, 2013
 * 
 */
public class Module {
	private boolean enabled, gui, toggleable = true;
	private int keyBind;
	private int moduleColor;
	private String moduleAuthor, moduleName, moduleTag;
	private ModuleCategory category;

	public Module(final ModuleCategory category, final String moduleName,
			final String keyBind) {
		this.setCategory(category);
		this.setModuleName(moduleName);
		this.setKeyBind(keyBind);
		this.setModuleAuthor("Ramisme");
		this.setModuleTag(moduleName);
		this.setInGui(true);
		this.setModuleColor(getCategory().getModuleColor());
		Logger.log("Loaded " + moduleName);

		this.registerCommand(new AbstractCommand(this.moduleName, "", String
				.format("Toggle %s.", this.moduleName)) {

			@Override
			public boolean onCommand(String command, String[] args) {
				if (isToggleable()) {
					onToggle(false);
				}

				return true;
			}

		});
	}

	public void registerCommand(final AbstractCommand command) {
		synchronized (Pringles.getInstance().getFactory().getCommandManager()
				.getCommandsList()) {
			Pringles.getInstance().getFactory().getCommandManager()
					.addCommand(command);
		}
	}

	/**
	 * Fired when a module is disabled
	 */
	protected void onDisable() {

	}

	/**
	 * Fired when a module is enabled
	 */
	protected void onEnable() {

	}

	/**
	 * Return an instance of the ChatHandler object
	 * 
	 * @return
	 */
	protected final ChatHandler getChatHandler() {
		return ChatHandler.getInstance();
	}

	/**
	 * Return an instance of the Wrapper object
	 * 
	 * @return
	 */
	protected final Wrapper getWrapper() {
		return Wrapper.getInstance();
	}

	/**
	 * Return the value of the default module color
	 * 
	 * @return
	 */
	public final int getDefaultModuleColor() {
		return getCategory().getModuleColor();
	}

	/**
	 * Return the current module keybind object.
	 * 
	 * @return
	 */
	public final int getKeyBind() {
		return keyBind;
	}

	/**
	 * Change the current module keybind based on string interpretation
	 * 
	 * @param key
	 */
	public final void setKeyBind(final String key) {
		keyBind = Keyboard.getKeyIndex(key);
	}

	/**
	 * Directly change the module keybind
	 * 
	 * @param key
	 */
	public final void setKeyBind(final int key) {
		keyBind = key;
	}

	/**
	 * Return a boolean determining whether the module is enabled or not.
	 * 
	 * @return
	 */
	public final boolean isEnabled() {
		return enabled;
	}

	/**
	 * Set the current module's state
	 * 
	 * @param flag
	 */
	public void setModuleState(final boolean flag) {
		enabled = flag;
	}

	/**
	 * Return whether the module should appear in the GUI or not.
	 * 
	 * @return
	 */
	public final boolean isInGui() {
		return gui;
	}

	/**
	 * Determine whether the module should appear in the GUI or not
	 * 
	 * @param flag
	 */
	protected final void setInGui(final boolean flag) {
		gui = flag;
	}

	/**
	 * Return whether the module is toggleable or not.
	 * 
	 * @return
	 */
	public final boolean isToggleable() {
		return toggleable;
	}

	/**
	 * Determine whether the current module can be toggled or not
	 * 
	 * @param flag
	 */
	protected final void setToggleable(final boolean flag) {
		toggleable = flag;
	}

	/**
	 * Fired when a module is toggled by key.
	 */
	public final void onToggle() {
		onToggle(true);
	}

	/**
	 * Fired when a module is toggled
	 * 
	 * @param isKey
	 */
	public void onToggle(final boolean isKey) {
		if (!this.isToggleable()) {
			return;
		}

		if (!enabled) {
			this.enabled = true;
			onEnable();
		} else {
			this.enabled = false;
			onDisable();
		}

		if (isKey) {
			return;
		}
		getChatHandler().addChat(
				getModuleName() + (enabled ? " enabled." : " disabled."));
	}

	/**
	 * @return the moduleAuthor
	 */
	public final String getModuleAuthor() {
		return moduleAuthor;
	}

	/**
	 * @return the moduleName
	 */
	public final String getModuleName() {
		return moduleName;
	}

	/**
	 * @return the moduleTag
	 */
	public final String getModuleTag() {
		return moduleTag;
	}

	/**
	 * @return the moduleColor
	 */
	public final int getModuleColor() {
		return moduleColor;
	}

	/**
	 * @return the category
	 */
	public final ModuleCategory getCategory() {
		return category;
	}

	/**
	 * @param moduleAuthor
	 *            the moduleAuthor to set
	 */
	protected final void setModuleAuthor(String moduleAuthor) {
		this.moduleAuthor = moduleAuthor;
	}

	/**
	 * @param moduleName
	 *            the moduleName to set
	 */
	protected final void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	/**
	 * @param moduleTag
	 *            the moduleTag to set
	 */
	protected final void setModuleTag(String moduleTag) {
		this.moduleTag = moduleTag;
	}

	/**
	 * @param moduleColor
	 *            the moduleColor to set
	 */
	protected final void setModuleColor(int moduleColor) {
		this.moduleColor = moduleColor;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	protected final void setCategory(ModuleCategory category) {
		this.category = category;
	}

}