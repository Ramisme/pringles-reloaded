package org.ramisme.pringles.modules.render;

import org.ramisme.pringles.events.EventHandler;
import org.ramisme.pringles.events.render.RenderWorldEvent;
import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.modules.addons.ModuleCategory;

/**
 * Waypoint management module.
 * 
 * @author Ramisme
 */
public final class Waypoints extends Module {
	
	public Waypoints() {
		super(ModuleCategory.RENDER, "Waypoints", "NONE");
		this.setModuleState(true);
		this.setInGui(false);
	}
	
	@EventHandler
	public void onRenderEvent(final RenderWorldEvent event) {
		
	}

}
