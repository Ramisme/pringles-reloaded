package org.ramisme.pringles.modules.render;

import java.util.List;

import net.minecraft.src.AxisAlignedBB;
import net.minecraft.src.EntityLiving;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.RenderManager;

import org.lwjgl.opengl.GL11;
import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.events.EventHandler;
import org.ramisme.pringles.events.EventListener;
import org.ramisme.pringles.events.render.RenderWorldEvent;
import org.ramisme.pringles.handlers.FriendsHandler;
import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.modules.addons.ModuleCategory;
import org.ramisme.pringles.opengl.Render3D;
import org.ramisme.pringles.opengl.RenderUtilities;

/**
 * Render 3-dimensional lines to each individual player.
 * 
 * @author Ramisme
 * @since Apr 17, 2013
 * 
 */
public final class Tracers extends Module implements EventListener {

	public Tracers() {
		super(ModuleCategory.RENDER, "Tracers", "L");
		this.setModuleAuthor("Ramisme");
		this.setInGui(false);

		Pringles.getInstance().getFactory().getEventManager()
				.registerListener(this);
	}

	@EventHandler
	public void onRenderWorldEvent(final RenderWorldEvent event) {
		for (final EntityPlayer entity : (List<EntityPlayer>) getWrapper()
				.getWorld().playerEntities) {
			if (entity == getWrapper().getPlayer()) {
				continue;
			}

			if (!entity.isEntityAlive()) {
				continue;
			}

			final double posX = entity.posX - RenderManager.renderPosX, posY = entity.posY
					- RenderManager.renderPosY, posZ = entity.posZ
					- RenderManager.renderPosZ;

			final float distance = getWrapper().getPlayer()
					.getDistanceToEntity(entity);
			final float colorDist = distance / 4;

			if (!(distance < 140)) {
				return;
			}

			getWrapper().getMinecraft().entityRenderer.disableLightmap(0);

			if (entity instanceof EntityPlayer
					&& FriendsHandler.getInstance().isFriend(
							((EntityPlayer) entity).username)) {
				final float[] c = RenderUtilities.getInstance().getARGB(
						0xFF2DC8E4);
				GL11.glColor3d(c[1], c[2], c[3]);
			} else {
				GL11.glColor3f(20 - (colorDist / 15), (colorDist / 15), 0);
			}

			GL11.glLineWidth(1.6F);
			if (entity.hurtTime > 0) {
				final float[] hurtColor = RenderUtilities.getInstance()
						.getARGB(0xFFFAAC58);
				GL11.glColor4f(hurtColor[0], hurtColor[2], hurtColor[3],
						hurtColor[0]);
			}

			Render3D.getInstance().drawLine(0, 0, 0, posX, posY, posZ);
			GL11.glPushMatrix();
			GL11.glDepthFunc(GL11.GL_LEQUAL);
			GL11.glHint(GL11.GL_PERSPECTIVE_CORRECTION_HINT, GL11.GL_FASTEST);
			GL11.glTranslated(posX, posY, posZ);
			GL11.glRotatef(-(entity.rotationYaw % 360), 0, 1, 0);
			GL11.glTranslated(-posX, -posY, -posZ);
			Render3D.getInstance().drawOutlinedBoundingBox(
					getEntityBoundingBox(entity));
			Render3D.getInstance().drawEspLines(getEntityBoundingBox(entity));
			GL11.glPopMatrix();
		}
	}

	/**
	 * Returns an AxisAlignedBB instance with the correct size and shape that
	 * the entity parameter requires. Written to handle all entity sizes and
	 * shapes.
	 * 
	 * @param entity
	 *            Target entity
	 * @return AxisAlignedBB
	 */
	private AxisAlignedBB getEntityBoundingBox(final EntityLiving entity) {
		final double scale = 1.5D;
		final double x = entity.posX - RenderManager.renderPosX, y = entity.posY
				- RenderManager.renderPosY, z = entity.posZ
				- RenderManager.renderPosZ;
		final double width = entity.width / scale, height = entity.height, yOffset = entity.yOffset;
		return new AxisAlignedBB(x - width, y - yOffset, z - width, x + width,
				y + height + (entity.isSneaking() ? 0.04 : 0.22), z + width);
	}

	/**
	 * @author Lynxaa
	 * @param entity
	 * @param distance
	 */
	private void setFadedColor(final EntityPlayer entity, final float distance) {
		float playerDistance = getWrapper().getPlayer().getDistanceToEntity(
				entity);
		if (FriendsHandler.getInstance().isFriend(entity.username)) {
			GL11.glColor4f(
					RenderUtilities.getInstance().getARGB(0xFF00BFFF)[1],
					RenderUtilities.getInstance().getARGB(0xFF00BFFF)[2],
					RenderUtilities.getInstance().getARGB(0xFF00BFFF)[3],
					RenderUtilities.getInstance().getARGB(0xFF00BFFF)[0]);
		} else if (playerDistance < 10) {
			GL11.glColor3f(1f, 0f, 0f);
		} else if (playerDistance < 64) {
			GL11.glColor3f(10f - (playerDistance / distance),
					(playerDistance / distance), 0f);
		} else if (playerDistance >= 64) {
			GL11.glColor3f(0f, 1f, 0f);
		}
	}

	/**
	 * @author Lynxaa
	 * @param entity
	 */
	private void colourBox(EntityPlayer entity) {
		float playerDistance = getWrapper().getPlayer().getDistanceToEntity(
				entity);
		boolean isVisable = getWrapper().getPlayer().canEntityBeSeen(entity);
		if (FriendsHandler.getInstance().isFriend(entity.username)) {
			GL11.glColor4f(
					RenderUtilities.getInstance().getARGB(0xFF00BFFF)[1],
					RenderUtilities.getInstance().getARGB(0xFF00BFFF)[2],
					RenderUtilities.getInstance().getARGB(0xFF00BFFF)[3],
					RenderUtilities.getInstance().getARGB(0xFF00BFFF)[0]);
		} else if (entity.hurtTime >= 2) {
			GL11.glColor4f(
					RenderUtilities.getInstance().getARGB(0xFFFAAC58)[1],
					RenderUtilities.getInstance().getARGB(0xFFFAAC58)[2],
					RenderUtilities.getInstance().getARGB(0xFFFAAC58)[3],
					RenderUtilities.getInstance().getARGB(0xFFFAAC58)[0]);
		} else if (playerDistance < 10 && isVisable) {
			GL11.glColor4f(
					RenderUtilities.getInstance().getARGB(0xFFFF0000)[1],
					RenderUtilities.getInstance().getARGB(0xFFFF0000)[2],
					RenderUtilities.getInstance().getARGB(0xFFFF0000)[3],
					RenderUtilities.getInstance().getARGB(0xFFFF0000)[0]);
		} else {
			GL11.glColor4f(
					RenderUtilities.getInstance().getARGB(0xFF00FF00)[1],
					RenderUtilities.getInstance().getARGB(0xFF00FF00)[2],
					RenderUtilities.getInstance().getARGB(0xFF00FF00)[3],
					RenderUtilities.getInstance().getARGB(0xFF00FF00)[0]);
		}
	}

}
