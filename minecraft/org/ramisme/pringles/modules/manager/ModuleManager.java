package org.ramisme.pringles.modules.manager;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.modules.ModuleListener;
import org.ramisme.pringles.modules.block.Nuker;
import org.ramisme.pringles.modules.block.Speedmine;
import org.ramisme.pringles.modules.chat.AutoTeleport;
import org.ramisme.pringles.modules.chat.Chat;
import org.ramisme.pringles.modules.combat.AutoEat;
import org.ramisme.pringles.modules.combat.AutoSword;
import org.ramisme.pringles.modules.combat.Criticals;
import org.ramisme.pringles.modules.combat.KillAura;
import org.ramisme.pringles.modules.combat.NoPush;
import org.ramisme.pringles.modules.movement.Flight;
import org.ramisme.pringles.modules.movement.Freecam;
import org.ramisme.pringles.modules.movement.NoFall;
import org.ramisme.pringles.modules.movement.Parkour;
import org.ramisme.pringles.modules.movement.Sprint;
import org.ramisme.pringles.modules.other.Bind;
import org.ramisme.pringles.modules.other.Friends;
import org.ramisme.pringles.modules.other.Gui;
import org.ramisme.pringles.modules.render.Tracers;
import org.ramisme.pringles.modules.world.Brightness;
import org.ramisme.pringles.modules.world.NoWeather;
import org.ramisme.pringles.modules.world.wallhack.Wallhack;

/**
 * Manage module objects.
 * 
 * @author Ramisme
 * @since Apr 3, 2013
 * 
 */
public class ModuleManager {
	private final Map<String, Module> modulesList = new HashMap<String, Module>();

	public void addModule(final Module module) {
		synchronized (modulesList) {
			modulesList.put(module.getModuleName(), module);
		}
	}

	public Module getModule(final String name) {
		synchronized (modulesList) {
			for (final Entry<String, Module> entry : modulesList.entrySet()) {
				if (entry.getKey().equalsIgnoreCase(name)) {
					return entry.getValue();
				}
			}
		}

		return null;
	}

	public Module[] getModulesList() {
		return modulesList.values().toArray(
				new Module[modulesList.values().size()]);
	}

	private synchronized void loadModules() {
		modulesList.clear();
		addModule(new Wallhack());
		addModule(new KillAura());
		addModule(new Criticals());
		addModule(new AutoEat());
		addModule(new Sprint());
		addModule(new NoPush());
		addModule(new Friends());
		addModule(new Brightness());
		addModule(new Freecam());
		addModule(new Flight());
		addModule(new Parkour());
		addModule(new AutoTeleport());
		// addModule(new Sneak());
		addModule(new Bind());
		addModule(new Speedmine());
		addModule(new AutoSword());
		addModule(new NoFall());
		addModule(new Chat());
		addModule(new Tracers());
		addModule(new Nuker());
		addModule(new NoWeather());
		addModule(new Gui());
	}

	public void onLoad() {
		Pringles.getInstance().getFactory().getEventManager()
				.registerListener(new ModuleListener());
		loadModules();
	}

}
