package org.ramisme.pringles.modules.manager;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.ramisme.pringles.modules.AbstractCommand;

/**
 * Manage command objects.
 * 
 * @author Ramisme
 * @since Apr 3, 2013
 * 
 */
public class CommandManager {
	private static final Map<String, AbstractCommand> commandsList = new HashMap<String, AbstractCommand>();

	public Map<String, AbstractCommand> getCommandsList() {
		return Collections.unmodifiableMap(commandsList);
	}

	public void addCommand(final AbstractCommand command) {
		synchronized (commandsList) {
			commandsList.put(command.getCommand(), command);
		}
	}

	public AbstractCommand getCommand(final String cmd) {
		synchronized (commandsList) {
			for (final Entry<String, AbstractCommand> entry : commandsList
					.entrySet()) {
				if (entry.getValue().getCommand().equalsIgnoreCase(cmd)) {
					return entry.getValue();
				}
			}
		}
		return null;
	}

}
