package org.ramisme.pringles.modules;

import org.ramisme.pringles.modules.Module;

/**
 * The abstract command class allows for execution of events when a command is
 * called.
 * 
 * @author Ramisme
 * @since Apr 3, 2013
 * 
 */
public abstract class AbstractCommand {
	private String command, usage, description;

	public AbstractCommand(final String command, final String usage,
			final String description) {
		this.command = command;
		this.usage = usage;
		this.description = description;
	}

	/**
	 * Execute a specified command.
	 * 
	 * @param command
	 *            Full command.
	 * @param args
	 *            Command arguments.
	 * @return
	 */
	public abstract boolean onCommand(String command, String[] args);

	public String getCommand() {
		return command;
	}

	public String getDescription() {
		return description;
	}

	public String getUsage() {
		return usage;
	}

	public void setCommand(final String command) {
		this.command = command;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public void setUsage(final String usage) {
		this.usage = usage;
	}

}
