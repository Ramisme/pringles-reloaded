package org.ramisme.pringles.modules.movement;

import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.events.EventHandler;
import org.ramisme.pringles.events.EventListener;
import org.ramisme.pringles.events.player.update.MotionUpdateEvent;
import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.modules.addons.ModuleCategory;

/**
 * Automatically sprints at a slightly higher speed.
 * 
 * @author Ramisme
 * @since Apr 4, 2013
 * 
 */
public class Sprint extends Module implements EventListener {

	public Sprint() {
		super(ModuleCategory.MOVEMENT, "Sprint", "V");

		Pringles.getInstance().getFactory().getEventManager()
				.registerListener(this);
	}

	@EventHandler
	public void onUpdateEvent(final MotionUpdateEvent event) {
		if (getWrapper().getPlayer().isSneaking()
				|| !(getWrapper().getPlayer().movementInput.moveForward > 0)
				|| getWrapper().getPlayer().isEating()
				|| getWrapper().getPlayer().getFoodStats().getFoodLevel() < 4
				|| getWrapper().getPlayer().isCollidedHorizontally
				|| getWrapper().getPlayer().isOnLadder()) {
			return;
		}

		if (!getWrapper().getPlayer().isSprinting()) {
			getWrapper().getPlayer().setSprinting(true);
			return;
		}

		getWrapper().getPlayer().landMovementFactor *= 1.2F;
	}

}
