package org.ramisme.pringles.modules.movement;

import net.minecraft.src.EntityOtherPlayerMP;
import net.minecraft.src.EntityPlayer;

import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.events.EventHandler;
import org.ramisme.pringles.events.EventListener;
import org.ramisme.pringles.events.EventPriority;
import org.ramisme.pringles.events.player.update.MotionUpdateEvent;
import org.ramisme.pringles.handlers.FreecamHandler;
import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.modules.addons.ModuleCategory;

/**
 * Cancel the sending of motion update packets to allow for free movement around
 * already loaded chunks without the server's noticing.
 * 
 * @author Ramisme
 * @since Apr 6, 2013
 * 
 */
public class Freecam extends Module implements EventListener {
	private final FreecamHandler handler = new FreecamHandler();

	public Freecam() {
		super(ModuleCategory.MOVEMENT, "Freecam", "O");

		Pringles.getInstance().getFactory().getEventManager()
				.registerListener(this);
	}

	@Override
	public void onEnable() {
		EntityPlayer entity = new EntityOtherPlayerMP(getWrapper().getWorld(),
				getWrapper().getPlayer().username);
		entity.copyDataFrom(getWrapper().getPlayer(), true);
		entity.posY -= getWrapper().getPlayer().yOffset;
		entity.setSneaking(getWrapper().getPlayer().isSneaking());
		getWrapper().getWorld().addEntityToWorld(-1, entity);
		handler.saveLocation(getWrapper().getPlayer());
	}

	@Override
	public void onDisable() {
		getWrapper().getWorld().removeEntityFromWorld(-1);
		handler.resetLocation();
		getWrapper().getPlayer().noClip = false;
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onUpdateEvent(final MotionUpdateEvent event) {
		event.setCancelled(true);
		getWrapper().getPlayer().noClip = Pringles.getInstance().getFactory()
				.getModuleManager().getModule("flight").isEnabled();
		if (Pringles.getInstance().getFactory().getModuleManager()
				.getModule("flight").isEnabled()) {
			getWrapper().getPlayer().onGround = false;
		}
	}

}
