package org.ramisme.pringles.modules.movement;

import net.minecraft.src.Packet10Flying;

import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.events.EventHandler;
import org.ramisme.pringles.events.EventListener;
import org.ramisme.pringles.events.player.update.MotionUpdateEvent;
import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.modules.addons.ModuleCategory;

public class NoFall extends Module implements EventListener {

	public NoFall() {
		super(ModuleCategory.MOVEMENT, "NoFall", "N");

		Pringles.getInstance().getFactory().getEventManager()
				.registerListener(this);
	}

	@EventHandler
	public void onPreMotionEvent(final MotionUpdateEvent event) {
		if (!getWrapper().getPlayer().onGround) {
			getWrapper().sendPacket(new Packet10Flying(true));
		}
	}
}
