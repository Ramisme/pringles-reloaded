package org.ramisme.pringles.modules.movement;

import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.events.EventHandler;
import org.ramisme.pringles.events.EventListener;
import org.ramisme.pringles.events.player.update.UpdateEvent;
import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.modules.addons.ModuleCategory;

/**
 * Allows for the player to not to fall off blocks, and if they do it'll set
 * their motion upwards until nocheatplus pulls them back.
 * 
 * @author Lynxaa
 * @since Apr 27, 2013
 */
public class Parkour extends Module implements EventListener {
	public Parkour() {
		super(ModuleCategory.MOVEMENT, "Parkour", "NONE");
		this.setModuleAuthor("Lynxaa");

		Pringles.getInstance().getFactory().getEventManager()
				.registerListener(this);
	}

	@EventHandler
	public void onUpdate(final UpdateEvent event) {
		if (getWrapper().getPlayer() != null) {
			if (getWrapper().getPlayer().fallDistance > 3
					&& !getWrapper().getPlayer().onGround) {
				getWrapper().getPlayer().motionY = 0.2;
				getWrapper().getPlayer().fallDistance = 0.0F;
			}
		}
	}

}
