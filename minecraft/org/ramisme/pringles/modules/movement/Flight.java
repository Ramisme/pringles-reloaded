package org.ramisme.pringles.modules.movement;

import net.minecraft.src.Packet11PlayerPosition;
import net.minecraft.src.Packet19EntityAction;

import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.events.EventHandler;
import org.ramisme.pringles.events.EventListener;
import org.ramisme.pringles.events.EventPriority;
import org.ramisme.pringles.events.player.update.MotionUpdateEvent;
import org.ramisme.pringles.modules.AbstractCommand;
import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.modules.addons.ModuleCategory;

/**
 * Notch fly mode.
 * 
 * @author Ramisme
 * @since Apr 6, 2013
 * 
 */
public class Flight extends Module implements EventListener {
	private boolean nocheat = false;

	public Flight() {
		super(ModuleCategory.MOVEMENT, "Flight", "F");

		this.registerCommand(new AbstractCommand("stp", "",
				"Teleport to spawn.") {

			@Override
			public boolean onCommand(String command, String[] args) {
				getWrapper().getPlayer().setPositionAndUpdate(Double.NaN,
						Double.NaN, Double.NaN);
				getWrapper().sendPacket(
						new Packet11PlayerPosition(Double.NaN, Double.NaN,
								Double.NaN, Double.NaN, false));
				getChatHandler().addChat("Teleported.");
				return true;
			}

		});

		this.registerCommand(new AbstractCommand("up", "<int>",
				"Teleport veritcally") {

			@Override
			public boolean onCommand(String command, String[] args) {
				final int distance = Integer.valueOf(args[1]);
				getWrapper().getPlayer().setPositionAndUpdate(
						getWrapper().getPlayer().posX,
						getWrapper().getPlayer().posY + distance,
						getWrapper().getPlayer().posZ);
				getChatHandler().addChat("Teleported.");
				return true;
			}

		});

		this.registerCommand(new AbstractCommand("fnc", "", "Change fly mode") {

			@Override
			public boolean onCommand(String command, String[] args) {
				nocheat = !nocheat;
				getChatHandler().addChat("Flight mode updated.");
				return true;
			}

		});

		Pringles.getInstance().getFactory().getEventManager()
				.registerListener(this);
	}

	@Override
	public void onEnable() {
		getWrapper().getPlayer().capabilities.isFlying = true;
	}

	@EventHandler(priority = EventPriority.CRITICAL)
	public void onUpdateEvent(final MotionUpdateEvent event) {
		getWrapper().getPlayer().capabilities.isFlying = true;

		if (nocheat) {
			getWrapper().sendPacket(
					new Packet19EntityAction(getWrapper().getPlayer(), 3));
			getWrapper().getPlayer().motionX *= 0.6;
			getWrapper().getPlayer().motionY *= 0.6;
			getWrapper().getPlayer().motionZ *= 0.6;
		}
	}

	@Override
	public void onDisable() {
		getWrapper().getPlayer().capabilities.isFlying = false;
	}

}
