package org.ramisme.pringles.modules.movement;

import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.events.EventHandler;
import org.ramisme.pringles.events.EventListener;
import org.ramisme.pringles.events.EventPriority;
import org.ramisme.pringles.events.player.update.MotionUpdateEvent;
import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.modules.addons.ModuleCategory;

/**
 * Hide the player's nametag behind blocks.
 * 
 * @author Ramisme
 * @since Apr 6, 2013
 * 
 */
public class Sneak extends Module implements EventListener {

	public Sneak() {
		super(ModuleCategory.MOVEMENT, "Sneak", "Y");

		Pringles.getInstance().getFactory().getEventManager()
				.registerListener(this);
	}

	@Override
	public void onEnable() {
		getWrapper().getGameSettings().keyBindSneak.pressed = true;
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onUpdateEvent(final MotionUpdateEvent event) {
		getWrapper().getGameSettings().keyBindSneak.pressed = true;
	}

	@Override
	public void onDisable() {
		getWrapper().getGameSettings().keyBindSneak.pressed = false;
	}

}
