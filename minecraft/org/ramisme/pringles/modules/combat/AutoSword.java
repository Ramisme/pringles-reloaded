package org.ramisme.pringles.modules.combat;

import net.minecraft.src.EntityLiving;
import net.minecraft.src.ItemStack;

import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.events.EventHandler;
import org.ramisme.pringles.events.EventListener;
import org.ramisme.pringles.events.player.PlayerAttackEvent;
import org.ramisme.pringles.handlers.FriendsHandler;
import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.modules.addons.ModuleCategory;

/**
 * Automatically switches to the best weapon when attacking an entity.
 * 
 * NOTE: The automatic switching of weapons handled within the KillAura module
 * is completely separate from this module.
 * 
 * @author Ramisme
 * @since Apr 16, 2013
 * 
 */
public final class AutoSword extends Module implements EventListener {

	public AutoSword() {
		super(ModuleCategory.COMBAT, "AutoSword", "NONE");

		Pringles.getInstance().getFactory().getEventManager()
				.registerListener(this);
	}

	@EventHandler
	public void onEntityAttack(final PlayerAttackEvent event) {
		if (event.isCancelled()) {
			return;
		}

		if ((event.getEntity() instanceof EntityLiving)
				&& FriendsHandler.getInstance().isFriend(
						(EntityLiving) event.getEntity())) {
			return;
		}

		double damage = 0;
		int weaponSlot = 1;

		for (int slot = 0; slot < 9; slot++) {
			final ItemStack itemStack = getWrapper().getPlayer().inventory.mainInventory[slot];
			if (itemStack == null) {
				continue;
			}

			final double curDamage = itemStack.getDamageVsEntity(event
					.getEntity());
			if (curDamage > damage) {
				damage = curDamage;
				weaponSlot = slot;
			}
		}

		if (weaponSlot >= 0) {
			getWrapper().getPlayer().inventory.currentItem = weaponSlot;
		}
	}

}