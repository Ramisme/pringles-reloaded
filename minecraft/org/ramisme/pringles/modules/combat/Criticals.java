package org.ramisme.pringles.modules.combat;

import net.minecraft.src.EntityLiving;

import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.events.EventHandler;
import org.ramisme.pringles.events.EventListener;
import org.ramisme.pringles.events.player.PlayerAttackEvent;
import org.ramisme.pringles.events.player.update.MotionUpdateEvent;
import org.ramisme.pringles.events.player.update.UpdateEvent;
import org.ramisme.pringles.handlers.ChatHandler;
import org.ramisme.pringles.modules.AbstractCommand;
import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.modules.addons.ModuleCategory;
import org.ramisme.pringles.modules.combat.api.EntityManager;
import org.ramisme.pringles.modules.combat.api.PositionManager;

/**
 * Forces critical hits while attacking.
 * 
 * @author Ramisme
 * @since Apr 21, 2013
 * 
 */
public class Criticals extends Module implements EventListener {
	private final PositionManager positionManager = new PositionManager();
	private final EntityManager entityManager = new EntityManager();

	private double oldYPosition;
	private boolean onGround;

	public Criticals() {
		super(ModuleCategory.COMBAT, "Criticals", "NONE");
		setModuleAuthor("Ramisme");

		Pringles.getInstance().getFactory().getEventManager()
				.registerListener(this);
	}

	@EventHandler
	public void preMotionUpdate(final MotionUpdateEvent event) {
		this.onGround = getWrapper().getPlayer().onGround;
		getWrapper().getPlayer().onGround = false;
	}

	@EventHandler
	public void postMotionUpdate(final UpdateEvent event) {
		getWrapper().getPlayer().onGround = this.onGround;
	}

	@EventHandler
	public void onAttackEntity(final PlayerAttackEvent event) {
		if (canCriticalHit()) {
			getWrapper().getPlayer().motionY = 0.1379F;
		}
	}

	private boolean canCriticalHit() {
		return !getWrapper().getPlayer().isOnLadder()
				&& !getWrapper().getPlayer().isInWater()
				&& !getWrapper().getGameSettings().keyBindJump.pressed
				&& getWrapper().getPlayer().onGround
				&& getWrapper().getPlayer().swingProgress > 0
				&& isBlockAir(getWrapper().getPlayer().posX, getWrapper()
						.getPlayer().posY + 1, getWrapper().getPlayer().posZ);
	}

	private boolean isBlockAir(double posX, double posY, double posZ) {
		return getWrapper().getWorld().getBlockId((int) posX, (int) posY,
				(int) posZ) == 0;
	}
}
