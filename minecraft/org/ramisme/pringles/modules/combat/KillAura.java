package org.ramisme.pringles.modules.combat;

import net.minecraft.src.EntityLiving;
import net.minecraft.src.ItemStack;

import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.events.EventHandler;
import org.ramisme.pringles.events.EventListener;
import org.ramisme.pringles.events.player.update.MotionUpdateEvent;
import org.ramisme.pringles.handlers.FreecamHandler;
import org.ramisme.pringles.modules.AbstractCommand;
import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.modules.addons.ModuleCategory;
import org.ramisme.pringles.modules.combat.api.EntityManager;
import org.ramisme.pringles.modules.combat.api.RotationManager;
import org.ramisme.pringles.modules.combat.api.TimeManager;

/**
 * Automatically attack the closest entity.
 * 
 * @author Ramisme
 * @since Apr 3, 2013
 * 
 */
public final class KillAura extends Module implements EventListener {
	private final TimeManager timer = new TimeManager();
	private final EntityManager entityManager = new EntityManager();
	private final FreecamHandler rotationHandler = new FreecamHandler();
	private final RotationManager rotationsManager;
	private EntityLiving target = null;

	private boolean criticals = false;
	private double curPositionY;

	private double range = 3.8;
	private double speed = 5;

	public KillAura() {
		super(ModuleCategory.COMBAT, "KillAura", "H");
		rotationsManager = new RotationManager(entityManager);

		this.registerCommand(new AbstractCommand("ks", "<speed>",
				"Changes attack speed.") {

			@Override
			public boolean onCommand(String command, String[] args) {
				final double speed = Long.valueOf(args[1]);
				if (speed > 15) {
					getChatHandler()
							.addChat(
									"\247cAttack speeds of more than 15 are not recommended.");
				}

				setSpeed(speed);
				getChatHandler().addChat("Updated attack speed.");
				return true;
			}

		});

		this.registerCommand(new AbstractCommand("kr", "<range>",
				"Changes attack range.") {

			@Override
			public boolean onCommand(String command, String[] args) {
				final double range = Double.valueOf(args[1]);
				if (range > 4.5D) {
					getChatHandler()
							.addChat(
									"\247cRanges of more than 4.5 are not recommended!");
				}

				setRange(range);
				getChatHandler().addChat("Updated aura range.");
				return true;
			}

		});

		Pringles.getInstance().getFactory().getEventManager()
				.registerListener(this);
	}

	@Override
	public void onDisable() {
		rotationsManager.setRotationYaw(getWrapper().getPlayer().rotationYaw);
		rotationsManager
				.setRotationPitch(getWrapper().getPlayer().rotationPitch);
	}

	@Override
	public void onEnable() {
		rotationsManager.setRotationYaw(getWrapper().getPlayer().rotationYaw);
		rotationsManager
				.setRotationPitch(getWrapper().getPlayer().rotationPitch);
	}

	@EventHandler
	public void onUpdateEvent(final MotionUpdateEvent event) {
		timer.setCurrent(timer.getNanoTime());
		target = entityManager.getEntity(90F, getRange());
		if (target != null) {
			float yaw = rotationsManager.getFixedYaw(
					rotationsManager.getRotationYaw(),
					entityManager.getPlayerRotations(target)[0]);
			float pitch = entityManager.getPlayerRotations(target)[1];

			event.setRotationYaw(yaw);
			event.setRotationPitch(pitch);

			final boolean areRotationsEqual = rotationsManager
					.areRotationsEqual(rotationsManager.getRotationYaw(),
							entityManager.getPlayerRotations(target)[0]);
			if (timer.delay(timer.convertToMillis(getSpeed()))
					&& areRotationsEqual) {
				event.setRotationYaw(yaw);
				event.setRotationPitch(pitch);
				attackEntity(target);
			}

			return;
		}

		event.setRotationYaw(getWrapper().getPlayer().rotationYaw);
		event.setRotationPitch(getWrapper().getPlayer().rotationPitch);
	}

	/**
	 * Attack the specified target.
	 * 
	 * @param target
	 *            Target to attack.
	 */
	private void attackEntity(final EntityLiving target) {
		getWeapon(target);
		getWrapper().getPlayer().swingItem();
		getWrapper().getPlayer().setSprinting(false);
		getWrapper().getPlayerController().attackEntity(
				getWrapper().getPlayer(), target);
		timer.setLast(timer.getNanoTime());
	}

	/**
	 * Switch to the best weapon when attacking the specified target.
	 * 
	 * @param target
	 *            The target currently being attacked
	 */
	private void getWeapon(final EntityLiving target) {
		double damage = 1;
		int weaponSlot = -1;

		for (int slot = 0; slot < 9; slot++) {
			ItemStack itemStack = getWrapper().getPlayer().inventory.mainInventory[slot];
			if (itemStack == null) {
				continue;
			}

			double curDamage = itemStack.getDamageVsEntity(target);
			if (curDamage > damage) {
				damage = curDamage;
				weaponSlot = slot;
			}
		}

		if (weaponSlot > -1) {
			getWrapper().getPlayer().inventory.currentItem = weaponSlot;
		}
		
		getWrapper().getMinecraft().playerController.updateController();
	}

	public double getRange() {
		return range;
	}

	public void setRange(final double newValue) {
		this.range = newValue;
	}

	private void setSpeed(final double speed) {
		this.speed = speed;
	}

	private double getSpeed() {
		return this.speed;
	}

}
