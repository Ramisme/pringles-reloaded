package org.ramisme.pringles.modules.combat;

import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.events.EventHandler;
import org.ramisme.pringles.events.EventListener;
import org.ramisme.pringles.events.player.PlayerVelocityEvent;
import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.modules.addons.ModuleCategory;

/**
 * Prevents player velocity affects.
 * 
 * @author Ramisme
 * @since Apr 4, 2013
 * 
 */
public class NoPush extends Module implements EventListener {

	public NoPush() {
		super(ModuleCategory.COMBAT, "NoPush", "NONE");
		this.setModuleState(true);

		Pringles.getInstance().getFactory().getEventManager()
				.registerListener(this);
	}

	@EventHandler
	public void onPlayerVelocity(final PlayerVelocityEvent event) {
		event.setCancelled(true);
	}

}
