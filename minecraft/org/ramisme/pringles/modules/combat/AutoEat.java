package org.ramisme.pringles.modules.combat;

import net.minecraft.src.InventoryPlayer;
import net.minecraft.src.Item;
import net.minecraft.src.ItemFood;
import net.minecraft.src.ItemStack;
import net.minecraft.src.Packet15Place;
import net.minecraft.src.Packet16BlockItemSwitch;

import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.events.EventHandler;
import org.ramisme.pringles.events.EventListener;
import org.ramisme.pringles.events.EventPriority;
import org.ramisme.pringles.events.player.update.MotionUpdateEvent;
import org.ramisme.pringles.events.player.update.UpdateEvent;
import org.ramisme.pringles.modules.AbstractCommand;
import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.modules.addons.ModuleCategory;
import org.ramisme.pringles.modules.combat.api.TimeManager;

/**
 * Instantly eat food when the player's health is low (specifically for servers
 * with instant eat plugins).
 * 
 * @author Ramisme
 * @since Mar 19, 2013
 */
public final class AutoEat extends Module implements EventListener {
	private final int MUSHROOM_SOUP = Item.bowlSoup.itemID,
			MUSHROOM_BOWL = Item.bowlEmpty.itemID;
	private int health = 7;
	private int selectedItem = MUSHROOM_SOUP;
	private final TimeManager timeManager = new TimeManager();

	public AutoEat() {
		super(ModuleCategory.COMBAT, "AutoEat", "NONE");

		this.registerCommand(new AbstractCommand("ah", "[health]",
				"Allows you to change the health threshold for the autosoup.") {
			@Override
			public boolean onCommand(String command, String[] args) {
				int newValue = Integer.parseInt(args[1]);
				AutoEat.this.health = newValue;
				getChatHandler().addChat(
						String.format(
								"AutoEat Will now eat ID<%s> when below ",
								AutoEat.this.selectedItem)
								+ newValue + " heart(s).");
				return true;
			}
		});

		this.registerCommand(new AbstractCommand("id", "[id]",
				"Changes the autosoup id") {

			@Override
			public boolean onCommand(String command, String[] args) {
				final int newId = Integer.parseInt(args[1]);
				AutoEat.this.selectedItem = newId;
				getChatHandler().addChat("Updated ID");
				return true;
			}

		});

		this.registerCommand(new AbstractCommand("ids", "<?>",
				"Possible item ids") {

			@Override
			public boolean onCommand(String command, String[] args) {
				String list = "ID List: ";
				for (final Item item : Item.itemsList) {
					if (item == null) {
						continue;
					}

					if (!(item instanceof ItemFood)) {
						continue;
					}

					list += String.format("%s; %s, ", item.getStatName(),
							item.itemID);
				}

				list = (list.substring(0, list.length() - 2) + ".");
				getChatHandler().addChat(list);
				return true;
			}

		});

		Pringles.getInstance().getFactory().getEventManager()
				.registerListener(this);
	}

	@EventHandler(priority = EventPriority.CRITICAL)
	public void onPreUpdateEvent(final UpdateEvent event) {
		if (this.selectedItem != this.MUSHROOM_SOUP) {
			return;
		}

		for (int slot = 0; slot < 36; slot++) {
			final ItemStack itemStack = getWrapper().getPlayer().inventory.mainInventory[slot];
			if (itemStack == null) {
				continue;
			}

			if (slot >= 9 && itemStack.itemID == MUSHROOM_BOWL) {
				getWrapper().getPlayerController().windowClick(0, slot, 0, 1,
						getWrapper().getPlayer());
			}
		}
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onUpdateEvent(final MotionUpdateEvent event) {
		this.setModuleTag(String.format("%s (%s)", this.getModuleName(),
				this.getItemsLeft()));

		final int originalSlot = getWrapper().getPlayer().inventory.currentItem;
		final InventoryPlayer inventoryplayer = getWrapper().getPlayer().inventory;
		timeManager.setCurrent(timeManager.getNanoTime());

		if (!isHealthLow()) {
			return;
		}

		for (int slot = 0; slot < 36; slot++) {
			final ItemStack itemStack = getWrapper().getPlayer().inventory.mainInventory[slot];
			if (itemStack == null) {
				continue;
			}

			if ((!isHealthLow() || !(itemStack.itemID == this.selectedItem) || !timeManager
					.delay(20))) {
				continue;
			}

			if (slot < 9) {
				getWrapper().getPlayer().inventory.currentItem = slot;
				getWrapper().sendPacket(new Packet16BlockItemSwitch(slot));
				getWrapper().sendPacket(
						new Packet15Place(-1, -1, -1, -1, itemStack, 0, 0, 0));
				getWrapper().getMinecraft().playerController.updateController();
				break;
			} else if (slot >= 9 && isHotbarEmpty() && timeManager.delay(40)) {
				getWrapper().getMinecraft().playerController.windowClick(0,
						slot, 0, 0, getWrapper().getMinecraft().thePlayer);
				getWrapper().getMinecraft().playerController.windowClick(0,
						getFirstAvailableSlot(), 1, 0, getWrapper()
								.getMinecraft().thePlayer);
				getWrapper().getMinecraft().playerController.updateController();
				timeManager.setLast(timeManager.getCurrent());
				break;
			}
		}

		getWrapper().getMinecraft().playerController.updateController();
	}

	/**
	 * Return whether there are full soups left in the main hotbar.
	 * 
	 * @return
	 */
	private boolean isHotbarEmpty() {
		boolean flag = true;
		for (int slot = 36; slot < 44; slot++) {
			final ItemStack itemstack = getWrapper().getPlayer().inventoryContainer
					.getSlot(slot).getStack();
			if (itemstack == null) {
				continue;
			}

			if (itemstack.itemID == this.selectedItem) {
				flag = false;
			}
		}

		return flag;
	}

	/**
	 * Return the first slot available for switching items.
	 * 
	 * @return
	 */
	private int getFirstAvailableSlot() {
		for (int slot = 36; slot < 44; slot++) {
			final ItemStack itemStack = getWrapper().getPlayer().inventoryContainer
					.getSlot(slot).getStack();
			if (itemStack == null) {
				continue;
			}

			if (itemStack.itemID == this.selectedItem) {
				return slot;
			}
		}

		return 38;
	}

	/**
	 * Determine if the player's health is low enough to eat a soup.
	 * 
	 * @return
	 */
	private boolean isHealthLow() {
		return getWrapper().getPlayer().getHealth() <= getHealth();
	}

	/**
	 * Return the minimum amount of health required to have in order to eat a
	 * new soup.
	 * 
	 * @return
	 */
	private int getHealth() {
		return health * 2;
	}

	/**
	 * Set the minimum health for eating soup.
	 * 
	 * @param health
	 */
	private void setHealth(final int health) {
		this.health = health;
	}

	private boolean sleep(long threshold, TimeManager time) {
		return (time.getCurrent() - time.getLast() >= threshold || time
				.getLast() == -1);
	}

	private int getItemsLeft() {
		int left = 0;
		for (int slot = 0; slot < 36; slot++) {
			final ItemStack itemstack = getWrapper().getPlayer().inventory.mainInventory[slot];
			if (itemstack == null) {
				continue;
			}

			if (itemstack.itemID == this.selectedItem) {
				left++;
			}
		}

		return left;
	}

}