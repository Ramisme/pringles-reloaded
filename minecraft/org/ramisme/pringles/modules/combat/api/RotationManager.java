package org.ramisme.pringles.modules.combat.api;

import net.minecraft.src.MathHelper;

/**
 * Manages local rotations.
 * 
 * @author Ramisme
 * @since Apr 23, 2013
 * 
 */
public final class RotationManager {
	private float rotationYaw, rotationPitch;
	private final EntityManager entityManager;

	public RotationManager(final EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public float getFixedYaw(final float currentRotation,
			final float nextRotation) {
		if (areRotationsEqual(currentRotation, nextRotation)) {
			setRotationYaw(nextRotation);
			return nextRotation;
		}

		final float yaw = updateRotation(currentRotation, nextRotation, 60);
		setRotationYaw(yaw);
		return yaw;
	}

	public boolean areRotationsEqual(final float current, final float next) {
		return (entityManager.getDistanceBetweenAngles(current, next) < 20);
	}

	private float updateRotation(final float par1, final float par2,
			final float par3) {
		float var4 = MathHelper.wrapAngleTo180_float(par2 - par1);

		if (var4 > par3) {
			var4 = par3;
		}

		if (var4 < -par3) {
			var4 = -par3;
		}

		return par1 + var4;
	}

	public void setRotations(final float yaw, final float pitch) {
		setRotationYaw(yaw);
		setRotationPitch(pitch);
	}

	public float getRotationYaw() {
		return rotationYaw;
	}

	public void setRotationYaw(float rotationYaw) {
		this.rotationYaw = rotationYaw;
	}

	public float getRotationPitch() {
		return rotationPitch;
	}

	public void setRotationPitch(float rotationPitch) {
		this.rotationPitch = rotationPitch;
	}

}
