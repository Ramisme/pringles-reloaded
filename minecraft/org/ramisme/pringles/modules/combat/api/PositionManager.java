package org.ramisme.pringles.modules.combat.api;

public final class PositionManager {
	private static final PositionManager instance = new PositionManager();
	
	public static final PositionManager getInstance() {
		return instance;
	}
	
	public boolean onGround;
	
	public boolean getOnGround() {
		return onGround;
	}
	
	public void setOnGround(final boolean onGround) {
		this.onGround = onGround;
	}
	
}
