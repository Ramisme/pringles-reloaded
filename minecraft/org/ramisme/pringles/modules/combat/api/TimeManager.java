package org.ramisme.pringles.modules.combat.api;

import java.util.concurrent.TimeUnit;

/**
 * Basic millisecond-based time manager.
 * 
 * @author Ramisme
 * @since Apr 3, 2013
 * 
 */
public class TimeManager {
	private long last, current;

	/**
	 * Return whether the delay has been completed
	 * 
	 * @param time
	 *            Amount of elapsed time in milliseconds
	 * @return
	 */
	public boolean delay(final long time) {
		return TimeUnit.MILLISECONDS.convert(current - last,
				TimeUnit.NANOSECONDS) >= time;
	}

	/**
	 * Return whether the delay has been completed using a custom conversion
	 * unit.
	 * 
	 * @param time
	 * @param conversion
	 * @return
	 */
	public boolean delay(final long time, final TimeUnit conversion) {
		return conversion.convert(current - last, TimeUnit.NANOSECONDS) >= time;
	}

	/**
	 * Convert the current time to milliseconds.
	 * 
	 * @param time
	 * @return
	 */
	public long convertToMillis(final double time) {
		return (long) (1000 / time);
	}

	/**
	 * Return the last time
	 * 
	 * @return
	 */
	public long getLast() {
		return last;
	}

	/**
	 * Return the current time
	 * 
	 * @return
	 */
	public long getCurrent() {
		return current;
	}

	/**
	 * Set the last time, or reset the current time.
	 * 
	 * @param last
	 */
	public void setLast(final long last) {
		this.last = last;
	}

	/**
	 * Set the current time
	 * 
	 * @param current
	 */
	public void setCurrent(final long current) {
		this.current = current;
	}

	/**
	 * Return the current time in milliseconds.
	 * 
	 * @return
	 */
	public long getNanoTime() {
		return System.nanoTime();
	}

}
