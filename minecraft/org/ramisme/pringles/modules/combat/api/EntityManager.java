package org.ramisme.pringles.modules.combat.api;

import java.util.List;

import net.minecraft.src.Entity;
import net.minecraft.src.EntityLiving;
import net.minecraft.src.MathHelper;
import net.minecraft.src.MovingObjectPosition;
import net.minecraft.src.Vec3;

import org.ramisme.pringles.Wrapper;
import org.ramisme.pringles.handlers.FriendsHandler;

/**
 * Manage entity relations.
 * 
 * @author Ramisme
 * @since Apr 3, 2013
 * 
 */
public final class EntityManager {

	/**
	 * Return the closest entity in relation to the player's current location.
	 * 
	 * @param range
	 *            Largest possible distance between entities.
	 * @return Closest <tt>EntityLiving</tt> object
	 */
	public EntityLiving getClosestEntity(final double range) {
		EntityLiving target = null;
		double tempRange = range;

		for (final Entity entity : (List<Entity>) Wrapper.getInstance()
				.getWorld().loadedEntityList) {
			if (!(entity instanceof EntityLiving)) {
				continue;
			}

			final EntityLiving living = (EntityLiving) entity;
			if (FriendsHandler.getInstance().isFriend(living)) {
				continue;
			}

			if (!living.isEntityAlive()) {
				continue;
			}

			if (!canAttackEntity(living) || !canEntityBeSeen(living)) {
				continue;
			}

			if (FriendsHandler.getInstance().isFriend(living)) {
				continue;
			}

			final double distance = Wrapper.getInstance().getPlayer()
					.getDistanceToEntity(living);
			if (distance < tempRange) {
				tempRange = distance;
				target = living;
			}
		}

		return target;
	}

	/**
	 * Returns the entity closest to the cross hairs.
	 * 
	 * @param maxRange
	 *            Max distance between entities.
	 * @param maxRotation
	 *            Maximum available rotation distance.
	 * @return
	 */
	public EntityLiving getClosestEntityToCursor(final double maxRange,
			final double maxRotation) {
		EntityLiving tempEntity = null;
		double rotation = maxRotation;

		for (final Entity entity : (List<Entity>) Wrapper.getInstance()
				.getWorld().loadedEntityList) {
			if (!(entity instanceof EntityLiving)) {
				continue;
			}

			final EntityLiving living = (EntityLiving) entity;
			if (FriendsHandler.getInstance().isFriend(living)) {
				continue;
			}

			if (!living.isEntityAlive()) {
				continue;
			}

			if (!canAttackEntity(living) || !canEntityBeSeen(living)) {
				continue;
			}

			final double curDistance = Wrapper.getInstance().getPlayer()
					.getDistanceToEntity(living);

			if (curDistance > maxRange) {
				continue;
			}

			final float[] aimAngles = getPlayerRotations(living);
			final double curRotation = getDistanceBetweenAngles(aimAngles[0],
					Wrapper.getInstance().getPlayer().rotationYaw);
			if (curRotation < rotation) {
				tempEntity = living;
				rotation = curDistance;
			}
		}

		return tempEntity;
	}

	/**
	 * Return the entity that matches the query.
	 * 
	 * @param maxRotation
	 *            Maximum rotation distance
	 * @param maxDistance
	 *            Maximum location distance
	 * @return
	 */
	public EntityLiving getEntity(final float maxRotation,
			final double maxDistance) {
		final EntityLiving distanceOriented = getClosestEntity(maxDistance);
		final EntityLiving rotationOriented = getClosestEntityToCursor(
				maxDistance, maxRotation);
		return rotationOriented != null ? rotationOriented : distanceOriented;
	}

	/**
	 * Determines whether the entity is available to attack.
	 * 
	 * @param entity
	 * @return
	 */
	private boolean canAttackEntity(final EntityLiving entity) {
		final boolean isNotSelf = entity != Wrapper.getInstance().getPlayer();
		final boolean canAttack = entity.isEntityAlive()
				&& entity.deathTime == 0;
		final boolean entityAvailable = canAttack && isNotSelf;
		return entityAvailable;
	}

	/**
	 * Return a float array containing the necessary rotations needed for facing
	 * the target entity living object.
	 * 
	 * @param entity
	 *            Expected target
	 * @return
	 */
	public float[] getPlayerRotations(final EntityLiving entity) {
		final double x = entity.posX - Wrapper.getInstance().getPlayer().posX;
		final double z = entity.posZ - Wrapper.getInstance().getPlayer().posZ;
		final double y = Wrapper.getInstance().getPlayer().posY
				+ Wrapper.getInstance().getPlayer().getEyeHeight()
				- (entity.posY + entity.getEyeHeight());
		final double helper = MathHelper.sqrt_double(x * x + z * z);
		final float newYaw = (float) (Math.atan2(z, x) * 180 / Math.PI) - 90;
		final float newPitch = (float) (Math.atan2(y, helper) * 180 / Math.PI);
		return new float[] { newYaw, newPitch };
	}

	/**
	 * Return the correct distance between two specified angles.
	 * 
	 * @param par1
	 * @param par2
	 * @return
	 */
	public double getDistanceBetweenAngles(final float par1, final float par2) {
		float angle = Math.abs(par1 - par2) % 360;
		if (angle > 180) {
			angle = 360 - angle;
		}

		return angle;
	}

	/**
	 * Determines whether the entity is in visible sight.
	 * 
	 * @param entity
	 *            Target entity
	 * @return
	 */
	private boolean canEntityBeSeen(final EntityLiving entity) {
		if (Wrapper.getInstance().getPlayer().canEntityBeSeen(entity)) {
			return true;
		} else {
			return isEntityVisible(entity);
		}
	}

	/**
	 * Determines whether the entity is visible based on ray tracing.
	 * 
	 * @param par1
	 *            Target entity
	 * @return
	 */
	private boolean isEntityVisible(final Entity par1) {
		return rayTrace(
				Wrapper.getInstance()
						.getWorld()
						.getWorldVec3Pool()
						.getVecFromPool(Wrapper.getInstance().getPlayer().posX,
								Wrapper.getInstance().getPlayer().posY,
								Wrapper.getInstance().getPlayer().posZ),
				Wrapper.getInstance().getWorld().getWorldVec3Pool()
						.getVecFromPool(par1.posX, par1.posY, par1.posZ)) == null;
	}

	/**
	 * Return a MovingObjectPosition ray trace object.
	 * 
	 * @param par1
	 *            Vec3 point one
	 * @param par2
	 *            Vec3 point two
	 * @return
	 */
	private MovingObjectPosition rayTrace(final Vec3 par1, final Vec3 par2) {
		return Wrapper.getInstance().getWorld()
				.rayTraceBlocks_do_do(par1, par2, false, true);
	}

}
