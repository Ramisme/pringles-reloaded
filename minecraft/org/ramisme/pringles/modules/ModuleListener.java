package org.ramisme.pringles.modules;

import java.util.Map.Entry;

import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.events.EventHandler;
import org.ramisme.pringles.events.EventListener;
import org.ramisme.pringles.events.chat.ChatSendEvent;
import org.ramisme.pringles.events.keyboard.KeyEvent;
import org.ramisme.pringles.handlers.ChatHandler;

/**
 * Basic event listener implementation for modules.
 * 
 * @author Ramisme
 * @since Apr 4, 2013
 * 
 */
public class ModuleListener implements EventListener {

	@EventHandler
	public void onChatSend(final ChatSendEvent event) {
		if (!(event.getMessage().startsWith("."))) {
			return;
		}

		final String message = event.getMessage().substring(1);
		event.setCancelled(true);

		final String[] args = message.split(" ");
		for (final Entry<String, AbstractCommand> entry : Pringles
				.getInstance().getFactory().getCommandManager()
				.getCommandsList().entrySet()) {
			final AbstractCommand command = entry.getValue();
			if (args[0].equalsIgnoreCase(command.getCommand())) {
				if (!command.onCommand(event.getMessage(), args)) {
					ChatHandler.getInstance().addChat(
							String.format("Invalid syntax! Try: %s %s",
									args[0], command.getUsage()));
					break;
				}
			}
		}

		for (final Module module : Pringles.getInstance().getFactory()
				.getModuleManager().getModulesList()) {
			if (!(module instanceof Command)) {
				continue;
			}

			final Command command = (Command) module;
			if (!event.getMessage().substring(1)
					.startsWith(command.getCommand())) {
				continue;
			}

			if (!command.onCommand(event.getMessage(), args)) {
				ChatHandler.getInstance().addChat(
						String.format("Invalid syntax! Try: %s %s", args[0],
								command.getUsage()));
				break;
			}

		}
	}

	@EventHandler
	public void onKeyPress(final KeyEvent event) {
		if (event.getKeyCode() <= 0) {
			return;
		}
		for (final Module module : Pringles.getInstance().getFactory()
				.getModuleManager().getModulesList()) {
			if (!module.isToggleable()) {
				continue;
			}

			if (event.getKeyCode() == module.getKeyBind()) {
				module.onToggle();
			}
		}
	}

}
