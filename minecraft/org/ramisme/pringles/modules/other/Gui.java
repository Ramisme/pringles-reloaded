package org.ramisme.pringles.modules.other;

import org.ramisme.pringles.handlers.GuiHandler;
import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.modules.addons.ModuleCategory;
import org.ramisme.pringles.ui.click.GuiClick;

public class Gui extends Module {

	public Gui() {
		super(ModuleCategory.NONE, "Gui", "RSHIFT");
		setInGui(false);
		this.setModuleAuthor("Lynxaa");
	}

	@Override
	public void onEnable() {
		getWrapper().getMinecraft().displayGuiScreen(new GuiClick());
		setModuleState(false);
		GuiHandler.getInstance().onLoad();
	}
}
