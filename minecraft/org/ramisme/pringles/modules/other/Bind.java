package org.ramisme.pringles.modules.other;

import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.handlers.BindsHandler;
import org.ramisme.pringles.modules.AbstractCommand;
import org.ramisme.pringles.modules.Command;
import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.modules.addons.ModuleCategory;

/**
 * Manage keybinds.
 * 
 * @author Ramisme
 * @since Apr 6, 2013
 * 
 */
public class Bind extends Module implements Command {

	public Bind() {
		super(ModuleCategory.MISC, "Bind", "NONE");
		this.setToggleable(false);
	}

	@Override
	public boolean onCommand(String command, String[] arguments) {
		final Module module = Pringles.getInstance().getFactory()
				.getModuleManager().getModule(arguments[1]);
		if (module == null) {
			getChatHandler().addChat("Could not find module!");
			return true;
		}

		final String bind = arguments[2].toUpperCase();

		module.setKeyBind(bind);
		getChatHandler().addChat(
				String.format("Bound %s to %s.", module.getModuleName(), bind));
		BindsHandler.getInstance().onSave();
		return true;
	}

	@Override
	public String getCommand() {
		return "bind";
	}

	@Override
	public String getUsage() {
		return "<module> <key>";
	}

}
