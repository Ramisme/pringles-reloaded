package org.ramisme.pringles.modules.other;

import java.util.Map.Entry;
import java.util.regex.Pattern;

import net.minecraft.src.EntityLiving;
import net.minecraft.src.StringUtils;

import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.events.EventHandler;
import org.ramisme.pringles.events.EventListener;
import org.ramisme.pringles.events.EventPriority;
import org.ramisme.pringles.events.chat.ChatRenderEvent;
import org.ramisme.pringles.events.player.PlayerAttackEvent;
import org.ramisme.pringles.handlers.FriendsHandler;
import org.ramisme.pringles.modules.AbstractCommand;
import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.modules.addons.ModuleCategory;

/**
 * Manage friends.
 * 
 * @author Ramisme
 * @since Apr 4, 2013
 * 
 */
public class Friends extends Module implements EventListener {

	public Friends() {
		super(ModuleCategory.MISC, "Friends", "NONE");
		this.setModuleState(true);
		this.setInGui(false);

		this.registerCommand(new AbstractCommand("add", "<name> <alias>",
				"Add a friend to the protect list.") {

			@Override
			public boolean onCommand(String command, String[] args) {
				if (args.length < 3) {
					return false;
				}

				if (FriendsHandler.getInstance().isFriend(args[1])) {
					getChatHandler().addChat("Friend already added.");
					return true;
				}

				final String alias = command.substring(this.getCommand()
						.length() + args[1].length() + 3);
				FriendsHandler.getInstance().addFriend(args[1], alias);
				getChatHandler().addChat("Added.");
				FriendsHandler.getInstance().onSave();
				return true;
			}

		});

		this.registerCommand(new AbstractCommand("del", "<alias>",
				"Remove a friend from the protect list") {

			@Override
			public boolean onCommand(String command, String[] args) {
				if (args.length < 2) {
					return false;
				}

				final String alias = command.substring(this.getCommand()
						.length() + 2);

				for (final Entry<String, String> entry : FriendsHandler
						.getInstance().getFriendsList().entrySet()) {
					if (entry.getValue().equalsIgnoreCase(alias)) {
						FriendsHandler.getInstance().removeFriend(alias);
						getChatHandler().addChat("Removed.");
						FriendsHandler.getInstance().onSave();
						return true;
					}
				}

				getChatHandler().addChat("\247cFriend not found.");
				return true;
			}

		});

		this.registerCommand(new AbstractCommand("clear", "",
				"Clear the list of current friends.") {

			@Override
			public boolean onCommand(String command, String[] args) {
				FriendsHandler.getInstance().clearFriends();
				getChatHandler().addChat("Friends cleared!");
				return true;
			}

		});

		Pringles.getInstance().getFactory().getEventManager()
				.registerListener(this);
	}

	@EventHandler(priority = EventPriority.CRITICAL)
	public void onEntityAttack(final PlayerAttackEvent event) {
		if (event.getEntity() instanceof EntityLiving) {
			if (FriendsHandler.getInstance().isFriend(
					(EntityLiving) event.getEntity())) {
				event.setCancelled(true);
				return;
			}
		}
	}

	@EventHandler
	public void onChatRenderEvent(final ChatRenderEvent event) {
		final String message = StringUtils
				.stripControlCodes(event.getMessage());
		for (final Entry<String, String> entry : FriendsHandler.getInstance()
				.getFriendsList().entrySet()) {
			if (message.contains(entry.getKey())) {
				final char color = getLastColor(event.getMessage(), event
						.getMessage().indexOf(entry.getKey()));
				final String chatMessage = event.getMessage();
				if (Pattern.compile(entry.getKey(), Pattern.CASE_INSENSITIVE)
						.matcher(message).find()) {
					event.setMessage(chatMessage.replace(entry.getKey(),
							"\2479" + entry.getValue() + "\247" + color));
				}
			}
		}
	}

	private char getLastColor(final String par1Str, final int par2Int) {
		for (int i = par2Int; i > -1; i--) {
			if (i != 0 && par1Str.charAt(i - 1) == '\247') {
				return par1Str.charAt(i);
			}
		}

		return 'f';
	}

}
