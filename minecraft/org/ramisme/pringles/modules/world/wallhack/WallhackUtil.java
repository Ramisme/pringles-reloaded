package org.ramisme.pringles.modules.world.wallhack;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import net.minecraft.src.Block;

/**
 * Setup the basic wallhack functions and utilities.
 * 
 * @author Ramisme
 * @since Apr 19, 2013
 * 
 */
public final class WallhackUtil {
	private static final WallhackUtil instance = new WallhackUtil();
	private int blockOpacity = 120;
	private final List<Integer> blockList;

	private Wallhack wallhackInstance;

	public WallhackUtil() {
		blockList = new LinkedList(Arrays.asList(new Integer[] { 8, 9, 10, 11,
				Block.oreDiamond.blockID }));
	}

	public static final WallhackUtil getInstance() {
		return instance;
	}

	public List<Integer> getBlockList() {
		return blockList;
	}

	public void setWallhackInstance(final Wallhack wallhack) {
		this.wallhackInstance = wallhack;
	}

	public void setOpacity(final int opacity) {
		blockOpacity = opacity;
	}

	public int getOpacity() {
		return blockOpacity;
	}

	public synchronized void addBlock(final int blockID) {
		synchronized (blockList) {
			if (!blockList.contains(blockID) && blockID != -1) {
				blockList.add(blockID);
			}
		}
	}

	public synchronized void removeBlock(final int blockID) {
		synchronized (blockList) {
			if (blockList.contains(blockID)) {
				blockList.remove(blockList.indexOf(blockID));
			}
		}
	}

	public synchronized boolean isWallhackBlock(final int blockID) {
		synchronized (blockList) {
			return blockList.contains(blockID);
		}
	}

	public boolean isEnabled() {
		return this.wallhackInstance != null
				&& this.wallhackInstance.isEnabled();
	}

}
