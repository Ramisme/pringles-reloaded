package org.ramisme.pringles.modules.world.wallhack;

import java.io.IOException;

import net.minecraft.src.Block;
import net.minecraft.src.StringTranslate;

import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.io.files.AbstractFileHandler;
import org.ramisme.pringles.modules.AbstractCommand;
import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.modules.addons.ModuleCategory;

/**
 * Displays ores for easy mining.
 * 
 * @author Ramisme
 * @since Apr 19, 2013
 * 
 */
public final class Wallhack extends Module {
	private int placeholder;
	private float oldGamma;

	public Wallhack() {
		super(ModuleCategory.WORLD, "Wallhack", "X");
		WallhackUtil.getInstance().setWallhackInstance(this);
		final WallhackManager manager = new WallhackManager();
		manager.init();

		this.registerCommand(new AbstractCommand("aid", "<block name>",
				"Add a new block to the wallhack ID list") {

			@Override
			public boolean onCommand(String command, String[] args) {
				final int blockId = getIdFromName(args[1]);
				if (blockId <= 0) {
					getChatHandler().addChat("Invalid block ID.");
					return true;
				}

				WallhackUtil.getInstance().addBlock(blockId);
				reloadLocalRenderers(400);
				getChatHandler().addChat("Added.");
				manager.save();
				return true;
			}

		});

		this.registerCommand(new AbstractCommand("did", "<block name>",
				"Remove a current block fromt the wallhack ID list") {

			@Override
			public boolean onCommand(String command, String[] args) {
				final int blockId = getIdFromName(args[1]);
				if (blockId <= 0) {
					getChatHandler().addChat("Invalid block ID.");
					return true;
				}

				WallhackUtil.getInstance().removeBlock(blockId);
				reloadLocalRenderers(400);
				getChatHandler().addChat("Removed.");
				manager.save();
				return true;
			}

		});

	}

	@Override
	public void onToggle(final boolean isKey) {
		super.onToggle(isKey);
		// reloadLocalRenderers(600);
		getWrapper().getMinecraft().renderGlobal.loadRenderers();
		WallhackUtil.getInstance().setOpacity(200);
	}

	@Override
	public void onEnable() {
		oldGamma = getWrapper().getGameSettings().gammaSetting;
		placeholder = getWrapper().getGameSettings().ambientOcclusion;
		getWrapper().getGameSettings().ambientOcclusion = 0;
		getWrapper().getGameSettings().gammaSetting = 4F;
	}

	@Override
	public void onDisable() {
		getWrapper().getGameSettings().ambientOcclusion = placeholder;
		getWrapper().getGameSettings().gammaSetting = oldGamma;
	}

	/**
	 * Reload the local renderers
	 * 
	 * @author Ramisme
	 * @param distance
	 */
	private void reloadLocalRenderers(final int distance) {
		getWrapper().getWorld().markBlockRangeForRenderUpdate(
				(int) getWrapper().getPlayer().posX - distance,
				(int) getWrapper().getPlayer().posY - distance,
				(int) getWrapper().getPlayer().posZ - distance,
				(int) getWrapper().getPlayer().posX + distance,
				(int) getWrapper().getPlayer().posY + distance,
				(int) getWrapper().getPlayer().posZ + distance);
	}

	/**
	 * Return a blockID given the block name.
	 * 
	 * @param name
	 * @return
	 */
	private int getIdFromName(final String name) {
		for (final Block block : Block.blocksList) {
			if (block == null) {
				continue;
			}

			final String label = StringTranslate.getInstance()
					.translateKey(block.getLocalizedName()).trim()
					.replace(" ", "");
			System.out.println(label);

			if (label.equalsIgnoreCase(name)) {
				return block.blockID;
			}
		}

		return -1;
	}

	private final class WallhackManager extends AbstractFileHandler {
		final WallhackUtil wallhackUtil = WallhackUtil.getInstance();

		public WallhackManager() {
			super(Pringles.getInstance().getDirectory(), "wallhack.properties");
		}

		@Override
		public void init() {
			if (!file.exists()) {
				try {
					file.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			load();
		}

		@Override
		public void save() {
			manager.startWriting();
			for (int id : wallhackUtil.getBlockList()) {
				manager.writeString(String.format("%s", id));
			}
			manager.stopWriting();
		}

		@Override
		public void load() {
			manager.startReading();

			String line;
			while ((line = manager.readLine()) != null) {
				wallhackUtil.addBlock(Integer.valueOf(line));
			}
			manager.stopReading();
		}

	}

}
