package org.ramisme.pringles.modules.world;

import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.modules.addons.ModuleCategory;

/**
 * Disables rendering of the weather.
 * 
 * @author Lynxaa
 * @since Apr 22, 2013.
 */
public class NoWeather extends Module {

	public NoWeather() {
		super(ModuleCategory.WORLD, "NoWeather", "NONE");
		this.setModuleAuthor("Lynxaa");
		this.setModuleState(true);
	}
}
