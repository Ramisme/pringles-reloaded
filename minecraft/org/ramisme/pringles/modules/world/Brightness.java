package org.ramisme.pringles.modules.world;

import net.minecraft.src.PotionEffect;

import org.ramisme.pringles.events.EventHandler;
import org.ramisme.pringles.events.player.update.UpdateEvent;
import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.modules.addons.ModuleCategory;

/**
 * Brighten up your world!
 * 
 * @author Ramisme
 * @since Apr 4, 2013
 * 
 */
public class Brightness extends Module {

	public Brightness() {
		super(ModuleCategory.WORLD, "Brightness", "C");
	}

}
