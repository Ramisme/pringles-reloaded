package org.ramisme.pringles;

/**
 * Logging utility class.
 * 
 * @author Ramisme
 * @since Apr 3, 2013
 * 
 */
public final class Logger {

	public static void log(final String message) {
		System.out.println("[Pringles]: " + message);
	}

}
