package org.ramisme.pringles.io.files;

import java.io.IOException;

import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.io.IOManager;

public final class UserFile extends AbstractFileHandler {
	private static final UserFile instance = new UserFile();
	public String nickname = "Default";
	public boolean firstRun = true;

	public static UserFile getInstance() {
		return instance;
	}

	public UserFile() {
		super(Pringles.getInstance().getDirectory(), "rundata.properties");
		init();
	}

	@Override
	public void init() {
		if (!this.file.exists()) {
			try {
				this.file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void save() {
		IOManager manager = IOManager.newFileManager(this.file);
		manager.startWriting();
		manager.writeString("#Pringles User Information File.");
		manager.writeString("first-run:false");
		manager.writeString(String.format("name:%s", nickname));
		manager.stopWriting();
	}

	@Override
	public void load() {
		IOManager manager = IOManager.newFileManager(this.file);
		manager.startReading();

		String line;
		while ((line = manager.readLine()) != null) {
			if (line.startsWith("#")) {
				continue;
			}

			parseReadData(line);
		}
	}

	private void parseReadData(final String message) {
		if (message.startsWith("first-run")) {
			firstRun = false;
		} else if (message.startsWith("name")) {
			this.nickname = message.split(":")[1];
		}
	}
}
