package org.ramisme.pringles.io.files;

import java.io.File;

import org.ramisme.pringles.io.IOManager;

/**
 * Basic File handler
 * 
 * @author Ramisme
 * @since Jun 5, 2013
 */
public abstract class AbstractFileHandler {
	protected final File file;
	protected final IOManager manager;

	public AbstractFileHandler(final File directory, final String file) {
		this.file = new File(directory, file);
		this.manager = IOManager.newFileManager(this.file);
	}

	/**
	 * Initiate file management
	 */
	public abstract void init();

	/**
	 * Save contents to file
	 */
	public abstract void save();

	/**
	 * Load file contents
	 */
	public abstract void load();

}
