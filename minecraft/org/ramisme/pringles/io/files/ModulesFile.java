package org.ramisme.pringles.io.files;

import java.io.File;
import java.io.IOException;

import org.lwjgl.input.Keyboard;
import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.io.IOManager;
import org.ramisme.pringles.modules.Module;

public final class ModulesFile extends AbstractFileHandler {
	private static final ModulesFile instance = new ModulesFile();

	public static ModulesFile getInstance() {
		return instance;
	}

	public ModulesFile() {
		super(Pringles.getInstance().getDirectory(), "modules.properties");
		init();
	}

	@Override
	public void init() {
		if (!this.file.exists()) {
			try {
				this.file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void save() {
		IOManager manager = IOManager.newFileManager(this.file);
		manager.startWriting();
		manager.writeString("#Pringles Module File.");
		manager.writeString("");

		for (final Module module : Pringles.getInstance().getFactory()
				.getModuleManager().getModulesList()) {
			final String name = module.getModuleName();
			final String author = module.getModuleAuthor();
			manager.writeString("Name: " + name);

			if (module.getKeyBind() > 1) {
				manager.writeString("Key: "
						+ Keyboard.getKeyName(module.getKeyBind()));
			} else {
				manager.writeString("Key: NONE");
			}

			manager.writeString("Author: " + author);
			manager.writeString("");
		}

		manager.stopWriting();
	}
	
	@Override
	public void load() {
		return;
	}
}
