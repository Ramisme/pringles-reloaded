package org.ramisme.pringles.io.files;

import java.io.File;
import java.io.IOException;
import java.util.Map.Entry;

import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.io.IOManager;
import org.ramisme.pringles.modules.AbstractCommand;

public final class CommandFile extends AbstractFileHandler {
	private static final CommandFile instance = new CommandFile();

	public static CommandFile getInstance() {
		return instance;
	}

	public CommandFile() {
		super(Pringles.getInstance().getDirectory(), "commands.properties");
	}

	@Override
	public void init() {
		if (!this.file.exists()) {
			try {
				this.file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void save() {
		IOManager manager = IOManager.newFileManager(this.file);
		manager.startWriting();
		manager.writeString("#Pringles Command File.");
		manager.writeString("");

		for (final Entry<String, AbstractCommand> entry : Pringles
				.getInstance().getFactory().getCommandManager()
				.getCommandsList().entrySet()) {
			final AbstractCommand command = entry.getValue();
			String cmd = command.getCommand();
			String usage = command.getUsage();
			String description = command.getDescription();
			manager.writeString("Command: " + cmd);
			manager.writeString("Usage: " + usage);
			manager.writeString("Description: " + description);
			manager.writeString("");
		}

		manager.stopWriting();
	}

	@Override
	public void load() {
		return;
	}
}
