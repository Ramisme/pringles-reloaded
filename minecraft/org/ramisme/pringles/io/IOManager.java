package org.ramisme.pringles.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 * Basic IOManager implementation for URL and File management.
 * 
 * @author Ramisme
 * @since Apr 3, 2013
 * 
 */
public class IOManager {
	private File file;
	private URL url;

	private BufferedReader reader;
	private BufferedWriter writer;

	public static IOManager newFileManager(final File file) {
		return new IOManager(file);
	}

	public static IOManager newUrlManager(final URL url) {
		return new IOManager(url);
	}

	private IOManager(final File file) {
		this.file = file;
	}

	private IOManager(final URL url) {
		this.url = url;
	}

	public String readLine() {
		String read = null;
		try {
			read = reader.readLine();
		} catch (final IOException e) {
			e.printStackTrace();
		}

		return read;
	}

	public void startReading() {
		try {
			if (file != null) {
				reader = new BufferedReader(new FileReader(file));
			} else {
				final URLConnection connection = url.openConnection();
				connection.setRequestProperty("User-Agent",
						"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
				reader = new BufferedReader(new InputStreamReader(
						connection.getInputStream()));
			}
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}

	public void startWriting() {
		try {
			if (file != null) {
				writer = new BufferedWriter(new FileWriter(file));
			}
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}

	public void stopReading() {
		try {
			reader.close();
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}

	public void stopWriting() {
		try {
			writer.flush();
			writer.close();
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}

	public void writeString(String par1) {
		try {
			writer.write(par1);
			writer.newLine();
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}
}
