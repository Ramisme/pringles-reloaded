package org.ramisme.pringles.threads;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;

import net.minecraft.src.Session;

import org.ramisme.pringles.Logger;
import org.ramisme.pringles.Wrapper;
import org.ramisme.pringles.io.IOManager;

/**
 * The LoginThread class implements Runnable and is able to be executed as a
 * thread in order to set the current session to a new one returned from
 * login.minecraft.net using a username and password.
 * 
 * To create a new LoginThread instance, call
 * <code>newLoginThread(String, String)</code>
 * 
 * @author Ramisme
 * @since Mar 12, 2013
 */
public final class LoginThread implements Runnable {
	private String username, password;
	private String information;

	public static LoginThread newLoginThread(final String username,
			final String password) {
		return new LoginThread(username, password);
	}

	private LoginThread(final String username, final String password) {
		this.username = username;
		this.password = password;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		String result = "";
		try {
			final String data = String
					.format("https://login.minecraft.net/?user=%s&password=%s&version=13",
							encode(username), encode(password));
			final URL url = new URL(data);
			final IOManager manager = IOManager.newUrlManager(url);
			manager.startReading();
			final String inputLine = manager.readLine();
			manager.stopReading();
			String session[];
			if (!inputLine.contains(":")) {
				result += "\247cERROR: " + inputLine;
				Logger.log(result);
				this.information = result;
				return;
			}

			session = inputLine.split(":");

			Wrapper.getInstance().getMinecraft().session = new Session(
					session[2], session[3]);
			result += ("\2472SUCCESS: " + session[2]);
			this.information = result;
			Logger.log(result);
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	public String getInformation() {
		return information;
	}

	private final String encode(final String par1) {
		try {
			return URLEncoder.encode(par1, "UTF-8");
		} catch (final UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}

}
