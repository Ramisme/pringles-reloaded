package org.ramisme.pringles.threads.manager;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * The ThreadManager class manages and executes threads safely using the
 * java.util.concurrent package.
 * 
 * @author Ramisme
 * @since Apr 4, 2013
 */
public class ThreadManager {
	private static ExecutorService threadPool = null;

	private static final ThreadManager INSTANCE = new ThreadManager(20);

	public static final ThreadManager getInstance() {
		return INSTANCE;
	}

	public ThreadManager(final int threads) {
		threadPool = Executors.newFixedThreadPool(threads);
	}

	/**
	 * Directly execute a Runnable implementation
	 * 
	 * @param par1
	 *            Runnable implementation
	 */
	public void executeThread(final Runnable par1) {
		threadPool.execute(par1);
	}

	/**
	 * Shut down the thread executor.
	 */
	public void shutdownThreadExecutor() {
		threadPool.shutdown();
	}

	/**
	 * Submit an implementation of the Callable interface to the thread manager.
	 * 
	 * @param par1
	 *            Callable implementation
	 * @return Future
	 */
	public Future submitCallable(final Callable par1) {
		return threadPool.submit(par1);
	}

	/**
	 * Submit an implementation of the Runnable interface to the thread manager.
	 * 
	 * @param par1
	 *            Runnable implementation
	 * @return Future
	 */
	public Future submitThread(final Runnable par1) {
		return threadPool.submit(par1);
	}

}
