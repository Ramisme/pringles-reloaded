package org.ramisme.pringles.slick;

import java.awt.Font;
import java.util.regex.Pattern;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import org.ramisme.pringles.Wrapper;
import org.ramisme.pringles.opengl.Render2D;

/**
 * Unicode fontrenderer
 * 
 * @author Lynxaa
 * @since Apr 19, 2013
 * 
 */
public class UnicodeFontRenderer {
	private UnicodeFont unicode;
	private final Pattern colors = Pattern.compile("(?i)\\u00A7[K-OR]");

	public UnicodeFontRenderer(Font font) {

		unicode = new UnicodeFont(font);
		unicode.addAsciiGlyphs();
		unicode.addNeheGlyphs();
		unicode.getEffects().add(new ColorEffect(java.awt.Color.white));

		try {
			unicode.loadGlyphs();
		} catch (SlickException exception) {
			throw new RuntimeException(exception);
		}
	}

	public void drawString(String par1Str, int par1, int par2, int par3,
			boolean par4) {

		int width = 0, length = 0;
		boolean rendered = false;
		String[] messages;
		par1Str = stripSpecialCodes(par1Str);

		GL11.glPushMatrix();
		GL11.glColor4f(1, 1, 1, 1);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_LINE_SMOOTH);
		GL11.glHint(GL11.GL_POINT_SMOOTH_HINT, GL11.GL_NICEST);
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL11.glDepthMask(false);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glScaled(0.5, 0.5, 0.5);

		if (par1Str.contains(ChatColour.SECTION)) {
			messages = par1Str.split(ChatColour.SECTION);
			if (!par1Str.startsWith(ChatColour.SECTION)) {
				if (par4) {
					unicode.drawString(
							par1 * 2 + 1,
							par2 * 2 + 1,
							messages[0],
							new Color(
									ColourHelper.getInstance().getRGBA(par3)[0],
									ColourHelper.getInstance().getRGBA(par3)[1],
									ColourHelper.getInstance().getRGBA(par3)[2],
									ColourHelper.getInstance().getRGBA(par3)[3])
									.darker(1f));
				}

				unicode.drawString(par1 * 2, par2 * 2, messages[0], new Color(
						ColourHelper.getInstance().getRGBA(par3)[0],
						ColourHelper.getInstance().getRGBA(par3)[1],
						ColourHelper.getInstance().getRGBA(par3)[2],
						ColourHelper.getInstance().getRGBA(par3)[3]));
				width += unicode.getWidth(messages[0]);
				rendered = true;
			}

			for (; length != messages.length; length++) {
				if (length == 0 && rendered)
					continue;

				String str = messages[length];
				if (str.length() == 0)
					continue;
				char identifier = 'z';
				try {
					identifier = str.charAt(0);
				} catch (Exception e) {
					e.printStackTrace();
				}
				int index = "0123456789abcdefk".indexOf(("" + identifier)
						.toLowerCase());
				if (index == -1)
					break;
				int colorcode = Wrapper.getInstance().getMinecraft().fontRenderer
						.getColorCode()[index];
				messages[length] = removeCharAt(messages[length], 0);

				if (par4) {
					unicode.drawString(
							par1 * 2 + width + 1,
							par2 * 2 + 1,
							messages[length],
							new Color(ColourHelper.getInstance().getRGBA(
									colorcode)[0], ColourHelper.getInstance()
									.getRGBA(colorcode)[1], ColourHelper
									.getInstance().getRGBA(colorcode)[2],
									ColourHelper.getInstance().getRGBA(par3)[3])
									.darker(1f));
				}
				unicode.drawString(
						par1 * 2 + width,
						par2 * 2,
						messages[length],
						new Color(
								ColourHelper.getInstance().getRGBA(colorcode)[0],
								ColourHelper.getInstance().getRGBA(colorcode)[1],
								ColourHelper.getInstance().getRGBA(colorcode)[2],
								ColourHelper.getInstance().getRGBA(par3)[3]));
				width += unicode.getWidth(messages[length]);
			}
		} else {
			if (par4) {
				unicode.drawString(par1 * 2 + 1, par2 * 2 + 1, par1Str,
						new Color(ColourHelper.getInstance().getRGBA(par3)[0],
								ColourHelper.getInstance().getRGBA(par3)[1],
								ColourHelper.getInstance().getRGBA(par3)[2],
								ColourHelper.getInstance().getRGBA(par3)[3])
								.darker(1f));
			}
			unicode.drawString(par1 * 2, par2 * 2, par1Str, new Color(
					ColourHelper.getInstance().getRGBA(par3)[0], ColourHelper
							.getInstance().getRGBA(par3)[1], ColourHelper
							.getInstance().getRGBA(par3)[2], ColourHelper
							.getInstance().getRGBA(par3)[3]));
		}
		GL11.glDisable(GL11.GL_LINE_SMOOTH);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glDepthMask(true);
		GL11.glPopMatrix();
	}

	public void drawStringWithShadow(String par1Str, int par1, int par2,
			int par3) {
		drawString(par1Str, par1, par2, par3, true);
	}

	public void drawString(String par1Str, int par1, int par2, int par3) {
		drawString(par1Str, par1, par2, par3, false);
	}

	private String stripSpecialCodes(final String par1Str) {
		return colors.matcher(par1Str).replaceAll("");
	}

	public static String removeCharAt(String par1Str, int par1) {
		return par1Str.substring(0, par1) + par1Str.substring(par1 + 1);
	}

}