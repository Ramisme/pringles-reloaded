package org.ramisme.pringles.slick;
/**
 * Single place containing all Minecraft color codes.
 * 
 * @author Lynxaa
 * @since Apr 19, 2013
 * 
 */
public final class ColourHelper {
	private static final ColourHelper instance = new ColourHelper(); 
	
	public static final ColourHelper getInstance() {
		return instance;
	}
	
	public final float[] getRGBA(int par1) {
		float red = (float) (par1 >> 16 & 255) / 255f;
		float green = (float) (par1 >> 8 & 255) / 255f;
		float blue = (float) (par1 & 255) / 255f;
		float alpha = (float) (par1 >> 25 & 255) / 255f;
		return new float[] { red, green, blue, alpha };
	}
}
