package org.ramisme.pringles;

import org.ramisme.pringles.events.manager.EventManager;
import org.ramisme.pringles.handlers.FontHandler;
import org.ramisme.pringles.modules.manager.CommandManager;
import org.ramisme.pringles.modules.manager.ModuleManager;

/**
 * Instance creator.
 * 
 * @author Ramisme
 * @since Apr 3, 2013
 * 
 */
public interface Factory {
	/**
	 * Return an instance of the command manager
	 * 
	 * @return
	 */
	public CommandManager getCommandManager();

	/**
	 * Return an instance of the event manager
	 * 
	 * @return
	 */
	public EventManager getEventManager();

	/**
	 * Return an instance of the module manager
	 * 
	 * @return
	 */
	public ModuleManager getModuleManager();
	
	/**
	 * Returns an instance of the font handler
	 * 
	 * @return
	 */
	public FontHandler getFontHandler();
}
