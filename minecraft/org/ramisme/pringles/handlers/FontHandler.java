package org.ramisme.pringles.handlers;

import java.awt.Font;

import org.ramisme.pringles.slick.UnicodeFontRenderer;

/**
 * Handles Slick's Font Render, Handles the instances and drawing of the fonts.
 * 
 * @author Element
 * @since 17/4/2013
 */
public final class FontHandler {
	private static FontHandler instance = new FontHandler();

	private static UnicodeFontRenderer unicode, gui, bold, lithium;

	public FontHandler() {
		if (unicode == null) {
			unicode = new UnicodeFontRenderer(
					new Font("Verdana", Font.BOLD, 17));
		}
		
		if (lithium == null) {
			lithium = new UnicodeFontRenderer(new Font("Trebuchet MS", Font.TRUETYPE_FONT, 16));
		}

		if (gui == null) {
			gui = new UnicodeFontRenderer(new Font("Verdana",
					Font.TRUETYPE_FONT, 16));
		}

		if (bold == null) {
			bold = new UnicodeFontRenderer(new Font("Verdana Bold", Font.BOLD,
					16));
		}
	}

	public static FontHandler getInstance() {
		return instance;
	}

	public static UnicodeFontRenderer getUnicodeFont() {
		return unicode;
	}

	public static UnicodeFontRenderer getGuiFont() {
		return gui;
	}
	
	public static UnicodeFontRenderer getBoldFont() {
		return bold;
	}

	public static UnicodeFontRenderer getLithiumFont() {
		return lithium;
	}

}
