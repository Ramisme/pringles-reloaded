package org.ramisme.pringles.handlers;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import net.minecraft.src.EntityLiving;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.StringUtils;

import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.io.IOManager;

/**
 * Manage friends.
 * 
 * @author Ramisme
 * @since Apr 4, 2013
 * 
 */
public final class FriendsHandler {
	private File friendsFile;
	private static final Map<String, String> friendsList = new HashMap<String, String>();
	private static final FriendsHandler instance = new FriendsHandler();

	public static FriendsHandler getInstance() {
		return instance;
	}

	private FriendsHandler() {
		this.friendsFile = new File(Pringles.getInstance().getDirectory(),
				"friends.properties");
		if (!this.friendsFile.exists()) {
			try {
				this.friendsFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public Map<String, String> getFriendsList() {
		return Collections.unmodifiableMap(friendsList);
	}

	public String getFriendAlias(final String username) {
		synchronized (friendsList) {
			for (final Entry<String, String> entry : friendsList.entrySet()) {
				if (entry.getKey().equalsIgnoreCase(username)) {
					return entry.getValue();
				}
			}
		}
		return null;
	}

	public void addFriend(String name, final String alias) {
		synchronized (friendsList) {
			name = StringUtils.stripControlCodes(name);
			for (final Entry<String, String> entry : friendsList.entrySet()) {
				if (entry.getKey().equalsIgnoreCase(alias)
						|| entry.getValue().equalsIgnoreCase(name)) {
					friendsList.remove(entry.getKey());
					break;
				}
			}

			friendsList.put(name, alias);
		}
	}

	public void removeFriend(final String alias) {
		synchronized (friendsList) {
			for (final Entry<String, String> entry : friendsList.entrySet()) {
				if (entry.getValue().equalsIgnoreCase(alias)) {
					friendsList.remove(entry.getKey());
					break;
				}
			}
		}
	}

	public void clearFriends() {
		synchronized (friendsList) {
			friendsList.clear();
		}
	}

	public boolean isFriend(String name) {
		name = StringUtils.stripControlCodes(name).toLowerCase();
		synchronized (friendsList) {
			for (final Entry<String, String> entry : friendsList.entrySet()) {
				if (Pattern.compile(entry.getKey(), Pattern.CASE_INSENSITIVE)
						.matcher(name).find()) {
					return true;
				}
			}
		}

		return false;
	}

	public boolean isFriend(final EntityLiving entityLiving) {
		if (!(entityLiving instanceof EntityPlayer)) {
			return false;
		}

		return isFriend((((EntityPlayer) entityLiving).username).toLowerCase());
	}

	public void onLoad() {
		IOManager manager = IOManager.newFileManager(this.friendsFile);
		manager.startReading();

		String friend;

		while ((friend = manager.readLine()) != null) {
			if (!friend.contains(":")) {
				continue;
			}

			synchronized (friendsList) {
				friendsList.put(friend.split(":")[0], friend.split(":")[1]);
			}
		}

		manager.stopReading();
	}

	public void onSave() {
		IOManager manager = IOManager.newFileManager(this.friendsFile);
		manager.startWriting();

		synchronized (friendsList) {
			for (final Entry<String, String> entry : friendsList.entrySet()) {
				manager.writeString(String.format("%s:%s", entry.getKey(),
						entry.getValue()));
			}
		}

		manager.stopWriting();
	}

}
