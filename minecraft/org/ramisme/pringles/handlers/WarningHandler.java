package org.ramisme.pringles.handlers;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import net.minecraft.src.GuiIngame;

import org.ramisme.pringles.Wrapper;
import org.ramisme.pringles.modules.combat.api.TimeManager;
import org.ramisme.pringles.opengl.Render2D;

/**
 * Manages the warning system.
 * 
 * @author Ramisme
 * 
 */
public final class WarningHandler {
	private final TimeManager timeManager = new TimeManager();
	private static final Map<String, Long[]> warnings = new LinkedHashMap<String, Long[]>();
	private static WarningHandler instance;

	public enum WarningType {
		WARNING, INFO;
	}

	public static WarningHandler getInstance() {
		synchronized (WarningHandler.class) {
			if (instance == null) {
				instance = new WarningHandler();
			}

			return instance;
		}
	}

	public Map<String, Long[]> getWarnings() {
		return Collections.unmodifiableMap(warnings);
	}

	public Entry<String, Long[]>[] getEntries() {
		return warnings.entrySet().toArray(
				new Entry[warnings.entrySet().size()]);
	}

	public void updateWarnings() {
		synchronized (warnings) {
			for (final Entry<String, Long[]> entry : getEntries()) {
				if (entry == null) {
					continue;
				}

				final float differenceInSeconds = TimeUnit.SECONDS.convert(
						System.nanoTime() - entry.getValue()[0],
						TimeUnit.NANOSECONDS);

				if (differenceInSeconds >= entry.getValue()[1]) {
					System.out.println("Time is up!: " + entry.getKey());
					this.removeWarning(entry.getKey());
				}
			}
		}
	}

	public void renderWarnings(final GuiIngame currentScreen) {
		final int center = getWrapper().getScaledResolution().getScaledWidth() / 2;
		int yPosition = 2;

		final int width = getWrapper().getScaledResolution().getScaledWidth();
		final int height = getWrapper().getScaledResolution().getScaledHeight();

		synchronized (getWarnings()) {
			for (final Entry<String, Long[]> entry : getWarnings().entrySet()) {
				Render2D.getInstance().drawBorderedGradientRect(80, yPosition,
						width - 80, yPosition + 16, 0x80000000, 0x90000000,
						0xFF000000);

				String message = entry.getKey();

				currentScreen.drawCenteredString(
						getWrapper().getMinecraft().fontRenderer, message,
						center, yPosition + 4, 0xFFFFFFFF);
				yPosition += 18;
			}
		}
	}

	public void addWarning(final WarningType type, String message,
			final long time) {
		synchronized (warnings) {
			switch (type) {
			case WARNING:
				message = ("[\247cWARNING\247f]: " + message);
				break;
			case INFO:
				message = ("[\2479INFO\247f]: " + message);
				break;
			}
			warnings.put(message, new Long[] { System.nanoTime(), time });
		}
	}

	public void removeWarning(final String warning) {
		synchronized (warnings) {
			warnings.remove(warning);
		}
	}

	private Wrapper getWrapper() {
		return Wrapper.getInstance();
	}

}
