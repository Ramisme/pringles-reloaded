package org.ramisme.pringles.handlers;

import java.io.File;
import java.io.IOException;

import org.ramisme.pringles.Logger;
import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.io.IOManager;
import org.ramisme.pringles.ui.click.Manager;
import org.ramisme.pringles.ui.click.components.Panel;

public class GuiHandler {
	private File guiFile;
	private static final GuiHandler instance = new GuiHandler();

	public static GuiHandler getInstance() {
		return instance;
	}

	private GuiHandler() {
		this.guiFile = new File(Pringles.getInstance().getDirectory(),
				"gui.properties");
		if (!this.guiFile.exists()) {
			try {
				this.guiFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void onSave() {
		IOManager manager = IOManager.newFileManager(this.guiFile);
		manager.startWriting();
		for (final Panel window : Manager.getInstance().getPanelList()) {
			manager.writeString(window.getTitle() + ":" + window.getXPos()
					+ ":" + window.getYPos() + ":" + window.isEnabled());
		}
		manager.stopWriting();
	}

	public void onLoad() {
		IOManager manager = IOManager.newFileManager(this.guiFile);
		manager.startReading();
		String info;
		while ((info = manager.readLine()) != null) {
			if (!info.contains(":")) {
				continue;
			}

			for (final Panel window : Manager.getInstance().getPanelList()) {
				if (info.split(":")[0].startsWith(window.getTitle())) {
					window.setXPos(Integer.parseInt(info.split(":")[1]));
					window.setYPos(Integer.parseInt(info.split(":")[2]));
					window.setEnabled(Boolean.parseBoolean(info.split(":")[3]));
				}
			}
		}
		manager.stopReading();
	}

}
