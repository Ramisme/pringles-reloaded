package org.ramisme.pringles.handlers;

import java.io.File;
import java.io.IOException;
import java.util.Map.Entry;

import org.lwjgl.input.Keyboard;
import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.io.IOManager;
import org.ramisme.pringles.modules.Module;

/**
 * Manage keybinds.
 * 
 * @author Ramisme
 * @since Apr 4, 2013
 * 
 */
public final class BindsHandler {
	private File bindsFile;
	private static final BindsHandler instance = new BindsHandler();

	public static BindsHandler getInstance() {
		return instance;
	}

	private BindsHandler() {
		this.bindsFile = new File(Pringles.getInstance().getDirectory(),
				"binds.properties");
		if (!this.bindsFile.exists()) {
			try {
				this.bindsFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void onLoad() {
		IOManager manager = IOManager.newFileManager(this.bindsFile);
		manager.startReading();

		String bind;
		while ((bind = manager.readLine()) != null) {
			if (!bind.contains(":")) {
				continue;
			}
			final Module module = Pringles.getInstance().getFactory()
					.getModuleManager().getModule(bind.split(":")[0]);
			if (module == null) {
				continue;
			}
			module.setKeyBind(bind.split(":")[1]);
		}

		manager.stopReading();
	}

	public void onSave() {
		IOManager manager = IOManager.newFileManager(this.bindsFile);
		manager.startWriting();

		for (final Module module : Pringles.getInstance().getFactory()
				.getModuleManager().getModulesList()) {
			manager.writeString(String.format("%s:%s", module.getModuleName(),
					Keyboard.getKeyName(module.getKeyBind())));
		}

		manager.stopWriting();
	}

}
