package org.ramisme.pringles.handlers;

import org.ramisme.pringles.Wrapper;

/**
 * Handle sending messages both to the client and to the server.
 * 
 * @author Ramisme
 * @since Apr 3, 2013
 * 
 */
public final class ChatHandler {
	private static ChatHandler instance = new ChatHandler();

	public static ChatHandler getInstance() {
		return instance;
	}

	public void sendChat(final String message) {
		Wrapper.getInstance().getPlayer().sendChatMessage(message);
	}

	public void addChat(final String message) {
		final String prefix = "\2477[\2479Pringles\2477]: \247f";
		Wrapper.getInstance().getPlayer().addChatMessage(prefix + message);
	}

}
