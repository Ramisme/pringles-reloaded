package org.ramisme.pringles.opengl;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;

/**
 * The Render2D class handles all 2D OpenGL rendering methods. Generally
 * speaking, these methods are used in GUI rendering.
 * 
 * @author Ramisme
 * @since Mar 19, 2013
 */
public class Render2D {

	/**
	 * Final instance value of the Render2D class.
	 */
	private static final Render2D INSTANCE = new Render2D();

	/**
	 * Return the instance value
	 * 
	 * @return
	 */
	public static final Render2D getInstance() {
		return INSTANCE;
	}

	/**
	 * Draw the border lines around a specified rectangular area.
	 * 
	 * @param par1
	 * @param par2
	 * @param par3
	 * @param par4
	 * @param par5
	 */
	private void drawBorder(final int par1, final int par2, final int par3,
			final int par4, final int par5) {
		final float color[] = RenderUtilities.getInstance().getARGB(par5);

		GL11.glPushMatrix();
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

		GL11.glColor4f(color[1], color[2], color[3], color[0]);
		GL11.glLineWidth(2f);

		GL11.glBegin(GL11.GL_LINE_STRIP);
		GL11.glVertex2d(par1, par4);
		GL11.glVertex2d(par3, par4);
		GL11.glVertex2d(par3, par2);
		GL11.glVertex2d(par1, par2);
		GL11.glVertex2d(par1, par4);
		GL11.glEnd();

		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glPopMatrix();
	}

	/**
	 * Draw a gradient rectangle with border lines.
	 * 
	 * @param par1
	 * @param par2
	 * @param par3
	 * @param par4
	 * @param inside
	 * @param inside1
	 * @param border
	 */
	public void drawBorderedGradientRect(final int par1, final int par2,
			final int par3, final int par4, final int inside,
			final int inside1, final int border) {
		drawGradientRect(par1, par2, par3, par4, inside, inside1);
		drawBorder(par1, par2, par3, par4, border);
	}

	/**
	 * Draw a normal rectangle with border lines.
	 * 
	 * @param par1
	 * @param par2
	 * @param par3
	 * @param par4
	 * @param inside
	 * @param border
	 */
	public void drawBorderedRect(final int par1, final int par2,
			final int par3, final int par4, final int inside, final int border) {
		drawRect(par1, par2, par3, par4, inside);
		drawBorder(par1, par2, par3, par4, border);
	}

	/**
	 * Draw a basic gradient rectangle.
	 * 
	 * @param par1
	 * @param par2
	 * @param par3
	 * @param par4
	 * @param par5
	 * @param par6
	 */
	public void drawGradientRect(final int par1, final int par2,
			final int par3, final int par4, final int par5, final int par6) {

		GL11.glPushMatrix();
		final float color[] = RenderUtilities.getInstance().getARGB(par6);
		final float color1[] = RenderUtilities.getInstance().getARGB(par5);

		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_ALPHA_TEST);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glShadeModel(GL11.GL_SMOOTH);

		GL11.glBegin(GL11.GL_QUADS);

		GL11.glColor4f(color[1], color[2], color[3], color[0]);
		GL11.glVertex2d(par1, par4);
		GL11.glVertex2d(par3, par4);

		GL11.glColor4f(color1[1], color1[2], color1[3], color1[0]);
		GL11.glVertex2d(par3, par2);
		GL11.glVertex2d(par1, par2);

		GL11.glEnd();

		GL11.glShadeModel(GL11.GL_FLAT);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_ALPHA_TEST);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glPopMatrix();
	}

	/**
	 * Draw a basic rectangle
	 * 
	 * @param par1
	 * @param par2
	 * @param par3
	 * @param par4
	 * @param par5
	 */
	public void drawRect(final double par1, final double par2,
			final double par3, final double par4, final int par5) {
		GL11.glPushMatrix();
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL13.GL_MULTISAMPLE);
		GL11.glHint(GL11.GL_LINE_SMOOTH_HINT, GL11.GL_NICEST);

		final float color[] = RenderUtilities.getInstance().getARGB(par5);
		GL11.glColor4f(color[1], color[2], color[3], color[0]);

		GL11.glBegin(GL11.GL_QUADS);
		GL11.glVertex2d(par1, par4);
		GL11.glVertex2d(par3, par4);
		GL11.glVertex2d(par3, par2);
		GL11.glVertex2d(par1, par2);
		GL11.glEnd();

		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glPopMatrix();
	}

}