package org.ramisme.pringles.ui.screens.modulemanager;

import org.ramisme.pringles.Wrapper;

import net.minecraft.src.GuiButton;
import net.minecraft.src.GuiMainMenu;
import net.minecraft.src.GuiScreen;

public class ModuleManagerUI extends GuiScreen {
	
	ModuleSlot moduleSlot;
	private static int selectedSlot = -1;
	
	@Override
	public void initGui() {
		moduleSlot = new ModuleSlot(this);
		buttonList.add(new GuiButton(1, this.width / 2 - 100, this.height - 30, "Back"));
	}
	
	@Override
	public void actionPerformed(GuiButton button) {
		switch(button.id) {
		case 1:
			Wrapper.getInstance().getMinecraft().displayGuiScreen(new GuiMainMenu());
		}
	}
	
	public static void setSelectedSlot(int newSlot) {
		selectedSlot = newSlot;
	}
	
	public static int getSelectedSlot() {
		return selectedSlot;
	}
	
	@Override
	public void drawScreen(int par1, int par2, float par3) {	
		drawDefaultBackground();
		moduleSlot.drawScreen(par1, par2, par3);
		drawCenteredString(this.fontRenderer, "Module Manager", this.width / 2, 6, 16777215);
		super.drawScreen(par1, par2, par3);
	}
	
}
