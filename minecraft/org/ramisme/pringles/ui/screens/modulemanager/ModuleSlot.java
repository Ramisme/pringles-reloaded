package org.ramisme.pringles.ui.screens.modulemanager;

import net.minecraft.src.FontRenderer;
import net.minecraft.src.GuiScreen;
import net.minecraft.src.GuiSlot;
import net.minecraft.src.Tessellator;

import org.lwjgl.input.Keyboard;
import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.Wrapper;
import org.ramisme.pringles.modules.Module;

public class ModuleSlot extends GuiSlot {

	public ModuleSlot(GuiScreen par1GuiScreen) {
		super(Wrapper.getInstance().getMinecraft(), par1GuiScreen.width,
				par1GuiScreen.height, 32, par1GuiScreen.height - 60, 36);
	}

	@Override
	protected int getSize() {
		return Pringles.getInstance().getFactory().getModuleManager()
				.getModulesList().length;
	}

	@Override
	protected void elementClicked(int var1, boolean var2) {
		ModuleManagerUI.setSelectedSlot(var1);

		if (!var2) {
			return;
		}

		final Module currentModule = Pringles.getInstance().getFactory()
				.getModuleManager().getModulesList()[var1];
		currentModule.setModuleState(!currentModule.isEnabled());
	}

	@Override
	protected boolean isSelected(int var1) {
		return var1 == ModuleManagerUI.getSelectedSlot();
	}

	@Override
	protected void drawBackground() {
		Wrapper.getInstance().getMinecraft().currentScreen
				.drawDefaultBackground();
	}

	@Override
	protected void drawSlot(int var1, int var2, int var3, int var4,
			Tessellator var5) {
		Module module = Pringles.getInstance().getFactory().getModuleManager()
				.getModulesList()[var1];
		if (module != null) {
			final FontRenderer fontRenderer = Wrapper.getInstance()
					.getMinecraft().fontRenderer;
			final String moduleTag = module.getModuleTag();
			final String moduleAuthor = module.getModuleAuthor();
			// 0xFF3e3e3e
			fontRenderer.drawStringWithShadow(moduleTag, var2 + 2, var3 + 2,
					module.getModuleColor());
			fontRenderer.drawStringWithShadow("Author: " + moduleAuthor,
					var2 + 2, var3 + 12, 0xFF3e3e3e);
			fontRenderer.drawStringWithShadow(
					"Key: " + Keyboard.getKeyName(module.getKeyBind()),
					var2 + 2, var3 + 22, 0xFF3e3e3e);
		}
	}

}
