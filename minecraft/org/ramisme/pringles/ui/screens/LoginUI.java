package org.ramisme.pringles.ui.screens;

import java.util.concurrent.Future;

import net.minecraft.src.GuiButton;
import net.minecraft.src.GuiMainMenu;
import net.minecraft.src.GuiScreen;
import net.minecraft.src.GuiTextField;

import org.lwjgl.input.Keyboard;
import org.ramisme.pringles.Wrapper;
import org.ramisme.pringles.threads.LoginThread;
import org.ramisme.pringles.threads.manager.ThreadManager;

/**
 * Extension of the GuiScreen class to display a basic login screen.
 * 
 * @author Ramisme
 * @since Apr 4, 2013
 * 
 */
public class LoginUI extends GuiScreen {

	private static GuiTextField userField, passField, altField;

	private int statusTimeout = 0;
	private Future future;
	private LoginThread loginThread;

	@Override
	public void initGui() {
		Keyboard.enableRepeatEvents(true);
		userField = new GuiTextField(
				Wrapper.getInstance().getMinecraft().fontRenderer,
				width / 2 - 100, height / 4, 200, 20);
		passField = new GuiTextField(
				Wrapper.getInstance().getMinecraft().fontRenderer,
				width / 2 - 100, height / 4 + 24, 200, 20);
		altField = new GuiTextField(
				Wrapper.getInstance().getMinecraft().fontRenderer,
				width / 2 - 100, height / 4 + 48, 200, 20);
		userField.setFocused(true);

		buttonList.clear();
		buttonList.add(new GuiButton(0, width / 2 - 102, height / 4 + 100, 204,
				20, "Login"));
		buttonList.add(new GuiButton(1, width / 2 - 102, height / 4 + 124, 204,
				20, "Back"));
	}

	@Override
	public void onGuiClosed() {
		Keyboard.enableRepeatEvents(false);
	}

	@Override
	public void actionPerformed(final GuiButton button) {
		final int id = button.id;
		switch (id) {
		case 1:
			Wrapper.getInstance().getMinecraft()
					.displayGuiScreen(new GuiMainMenu());
			break;
		case 0:
			if (passField.getText().length() < 4
					&& userField.getText().length() > 2) {
				Wrapper.getInstance().getMinecraft().session.username = userField
						.getText().trim();
				return;
			} else if (this.altField.getText().length() > 5
					&& this.altField.getText().contains(":")) {
				final String account = altField.getText();
				System.out.println(account);
				loginThread = LoginThread.newLoginThread(account.split(":")[0],
						account.split(":")[1]);
				future = ThreadManager.getInstance().submitThread(loginThread);
				return;
			}

			loginThread = LoginThread.newLoginThread(userField.getText(),
					passField.getText());
			future = ThreadManager.getInstance().submitThread(loginThread);
			break;
		}
	}

	@Override
	public void updateScreen() {
		if (statusTimeout > 0) {
			statusTimeout--;
		}
		userField.updateCursorCounter();
		passField.updateCursorCounter();
		altField.updateCursorCounter();
	}

	@Override
	public void mouseClicked(final int par1, final int par2, final int par3) {
		userField.mouseClicked(par1, par2, par3);
		passField.mouseClicked(par1, par2, par3);
		altField.mouseClicked(par1, par2, par3);
		super.mouseClicked(par1, par2, par3);
	}

	@Override
	public void keyTyped(final char par1, final int par2) {
		userField.textboxKeyTyped(par1, par2);
		passField.textboxKeyTyped(par1, par2);
		altField.textboxKeyTyped(par1, par2);

		switch (par2) {
		case Keyboard.KEY_TAB:
			if (userField.isFocused()) {
				userField.setFocused(false);
				passField.setFocused(true);
				altField.setFocused(false);
			} else if (passField.isFocused()) {
				userField.setFocused(false);
				passField.setFocused(false);
				altField.setFocused(true);
			}
		case Keyboard.KEY_RETURN:
			actionPerformed(new GuiButton(0, 0, 0, ""));
			break;
		}

		super.keyTyped(par1, par2);
	}

	@Override
	public void drawScreen(final int par1, final int par2, final float par3) {
		drawDefaultBackground();
		userField.drawTextBox();
		passField.drawTextBox();
		altField.drawTextBox();

		drawCenteredString(Wrapper.getInstance().getMinecraft().fontRenderer,
				"Login to Minecraft", width / 2, height / 4 - 20, 0xffffff);

		final StringBuffer message = new StringBuffer();
		if (future != null && loginThread != null) {
			if (!future.isDone()) {
				message.append("\2477\247kElementIsSexy");
			} else {
				if ((loginThread != null) && future.isDone()) {
					message.append(loginThread.getInformation());
				}
			}
		} else {
			message.append("\2472"
					+ Wrapper.getInstance().getMinecraft().session.username);
		}

		drawString(
				fontRenderer,
				message.toString(),
				width / 2 - fontRenderer.getStringWidth(message.toString()) / 2,
				height / 4 + 80, 0xffffff);
		super.drawScreen(par1, par2, par3);
	}
}