package org.ramisme.pringles.ui.click.panels;

import org.lwjgl.opengl.GL11;
import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.Wrapper;
import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.modules.addons.ModuleCategory;
import org.ramisme.pringles.ui.click.GuiClick;
import org.ramisme.pringles.ui.click.components.Button;
import org.ramisme.pringles.ui.click.components.Component;
import org.ramisme.pringles.ui.click.components.Panel;
import org.ramisme.pringles.ui.click.components.combo.ComboBox;
import org.ramisme.pringles.ui.click.themes.handler.ThemeManager;
import org.ramisme.pringles.ui.click.themes.pringles.PringlesButton;

/**
 * Basic extra panel to manage themes.
 * 
 * @author Ramisme
 * @since Jun 15, 2013
 */
public final class ThemePanel extends Panel {
	private final ThemeManager themeManager = GuiClick.themeManager;
	private Panel panel;

	private ComboBox comboBox;

	public ThemePanel(int xPos, int yPos, int width, int height, String name) {
		super(xPos, yPos, width, height, name, ModuleCategory.NONE);
		this.panel = themeManager.getTheme().getThemedPanel(xPos, yPos, width,
				height, this.getTitle(), ModuleCategory.DERP);
		this.comboBox = themeManager.getTheme().getThemedComboBox(xPos + 2,
				yPos + 16, width - 4, 14, "Themes");
	}

	@Override
	public void draw(int x, int y) {
		panel.draw(x, y);

		if (panel.isEnabled()) {
			comboBox.draw(x, y);
			panel.setHeight(18 + comboBox.getHeight());
		}
	}

	@Override
	public void mouseClicked(final int x, final int y, final int key) {
		super.mouseClicked(x, y, key);
		if (panel != null) {
			if (panel.isMouseOver(x, key) || panel.isMouseInHeader(x, y, key)) {
				panel.mouseClicked(x, y, key);

				if (comboBox.isMouseOver(x, key)) {
					comboBox.mouseClicked(x, y, key);
				}
			}
		}
	}

	@Override
	public void mouseMovedOrUp(int x, int y, int key) {
		if (panel != null) {
			panel.mouseMovedOrUp(x, y, key);
		}
	}

}
