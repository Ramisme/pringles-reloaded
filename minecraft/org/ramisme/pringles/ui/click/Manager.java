package org.ramisme.pringles.ui.click;

import java.util.ArrayList;
import java.util.List;

import org.ramisme.pringles.ui.click.components.Panel;

/**
 * Object manager for clickable UI purposes.
 * 
 * @author Ramisme
 * @since Apr 17, 2013
 * 
 */
public final class Manager {
	private static final Manager instance = new Manager();
	private final List<Panel> panelList = new ArrayList<Panel>();

	/**
	 * Returns the panel list.
	 * 
	 * @return
	 */
	public List<Panel> getPanelList() {
		return this.panelList;
	}

	public Panel[] getPanels() {
		return panelList.toArray(new Panel[panelList.size()]);
	}

	public void addPanel(final Panel panel) {
		synchronized (panelList) {
			this.panelList.add(panel);
		}
	}

	public void clearPanels() {
		synchronized (panelList) {
			this.panelList.clear();
		}
	}

	public static Manager getInstance() {
		return instance;
	}

}
