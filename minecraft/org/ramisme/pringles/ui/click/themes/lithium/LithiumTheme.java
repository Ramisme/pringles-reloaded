package org.ramisme.pringles.ui.click.themes.lithium;

import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.modules.addons.ModuleCategory;
import org.ramisme.pringles.ui.click.components.Button;
import org.ramisme.pringles.ui.click.components.Label;
import org.ramisme.pringles.ui.click.components.Panel;
import org.ramisme.pringles.ui.click.components.combo.ComboBox;
import org.ramisme.pringles.ui.click.components.combo.ComboComponent;
import org.ramisme.pringles.ui.click.themes.handler.Theme;
import org.ramisme.pringles.ui.click.themes.pringles.combo.PringlesComboBox;
import org.ramisme.pringles.ui.click.themes.pringles.combo.PringlesComboComponent;

/**
 * Basic Pringles theme
 * 
 * @author Ramisme
 * @since Jun 5, 2013
 */
public final class LithiumTheme implements Theme {

	@Override
	public Panel getThemedPanel(int x, int y, int width, int height,
			String label, ModuleCategory category) {
		return (new LithiumPanel(x, y, width, height, label, category));
	}

	@Override
	public Label getThemedLabel(int x, int y, Module module) {
		return (new LithiumLabel(x, y, module));
	}

	@Override
	public Label getThemedLabel(int x, int y, String string) {
		return (new LithiumLabel(x, y, string));
	}

	@Override
	public Button getThemedButton(final int x, final int y, final int width, final int height, final Module module) {
		return (new LithiumButton(x, y, width, height, module));
	}

	@Override
	public ComboComponent getThemedComboComponent(int x, int y, int width,
			int height, String label) {
		return (new PringlesComboComponent(x, y, width, height, label));
	}

	@Override
	public ComboBox getThemedComboBox(int x, int y, int width, int height,
			String label) {
		return (new PringlesComboBox(x, y, width, height, label));
	}

}
