package org.ramisme.pringles.ui.click.themes.lithium;

import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.ui.click.components.Label;

public class LithiumLabel extends Label {
	private Module module;

	public LithiumLabel(final int xPos, final int yPos, final Module module) {
		super(xPos, yPos, module.getModuleName());
		this.module = module;
	}
	
	public LithiumLabel(final int xPos, final int yPos, final String label) {
		super(xPos, yPos, label);
	}

	@Override
	public void draw(final int x, final int y) {
		this.color = this.module.isEnabled() ? 0xFFFFFFFF : 0xccFFFFFF;
		super.draw(x, y);
	}
}
