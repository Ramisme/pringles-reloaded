package org.ramisme.pringles.ui.click.themes.lithium;

import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.opengl.Render2D;
import org.ramisme.pringles.ui.click.components.Button;

public final class LithiumButton extends Button {
	private boolean enabled;
	private Module module;

	public LithiumButton(int x, int y, int width, int height, Module module) {
		super(x, y, width, height, module);
		this.module = module;
		this.enabled = module.isEnabled();
	}
	
	private float alpha = 0;

	@Override
	public void draw(final int x, final int y) {
		if (module.isEnabled()) {
			Render2D.getInstance().drawRect(xPos, yPos, xPos + width, yPos + height, isMouseOver(x, y) ? 0x8f00B3FF : 0x9f00B3FF);
		} else {
			//0x9f5E5E5E
			Render2D.getInstance().drawRect(xPos, yPos, xPos + width, yPos + height, isMouseOver(x, y) ? 0x8f5E5E5E : 0x9f5E5E5E);
		}
		
		/*if (isMouseOver(x, y) && !module.isActive()) {
			if (alpha < 0.75F) alpha++;
			float[] color = { 0.3f, 0.3f, 0.3f, alpha};
			Render2D.getInstance().drawRect(xPos, yPos, xPos + width, yPos + height, 0x8f5E5E5E);
			alpha = -1;
		}*/

		final LithiumLabel label = new LithiumLabel(xPos + 3, yPos + 2, module);
		label.draw(x, y);
	}

	@Override
	public void mouseClicked(final int x, final int y, final int key) {
		super.mouseClicked(x, y, key);
		toggleModule();
	}

	@Override
	public void mouseMovedOrUp(final int x, final int y, final int key) {
		return;
	}

	public void toggleModule() {
		if (module == null) {
			return;
		}

		module.onToggle();
	}
	
	public Module getClickedModule() {
		return module;
	}

}
