package org.ramisme.pringles.ui.click.themes.lithium;

import org.lwjgl.opengl.GL11;
import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.Wrapper;
import org.ramisme.pringles.handlers.FontHandler;
import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.modules.addons.ModuleCategory;
import org.ramisme.pringles.ui.click.components.Button;
import org.ramisme.pringles.ui.click.components.Component;
import org.ramisme.pringles.ui.click.components.Panel;
import org.ramisme.pringles.ui.click.themes.handler.ThemeManager;

/**
 * @author Ramisme
 */
public class LithiumPanel extends Panel {
	private final ThemeManager themeManager = ThemeManager.getInstance();

	public LithiumPanel(final int xPos, final int yPos, final int width,
			final int height, final String label,
			final ModuleCategory moduleCategory) {
		super(xPos, yPos, width, height, label, moduleCategory);
		
		this.setFont(FontHandler.getLithiumFont());
	}

	@Override
	public void draw(int x, int y) {
		handleDragging(x, y);
		this.drawObjects();

		GL11.glPushMatrix();
		GL11.glEnable(GL11.GL_BLEND);

		if (this.enabled) {
			render.drawRect(getXPos(), getYPos() + 12, getXPos() + getWidth(), getYPos() + getHeight() - 1, 0x40000000);

			for (Component component : getComponents()) {
				GL11.glPushMatrix();
				GL11.glTranslated(0, -1, 0);
				component.draw(x, y);
				GL11.glPopMatrix();
			}
		}

		if (this.current == this) font.drawStringWithShadow("Current Panel: " + this.getTitle(), 2, Wrapper.getInstance().getScaledResolution().getScaledHeight() - 12, 0xFFFFFFFF);

		render.drawRect(xPos, yPos, xPos + width, yPos + 13, 0xFF94918b);
		font.drawStringWithShadow(getTitle(), getXPos() + 4, getYPos() + 2, 0xFFFFFFFF);		
		GL11.glPopMatrix();		
	}

	/**
	 * Called when the panel is drawn
	 * 
	 * @param panel
	 */
	protected void drawObjects() {
		this.clearComponents();

		int yPos = 16;
		for (final Module module : Pringles.getInstance().getFactory()
				.getModuleManager().getModulesList()) {
			final Button button = themeManager.getTheme()
					.getThemedButton(this.xPos + 2, this.yPos + yPos,
							this.width - 4, 14, module);
			if (module.getCategory() == category) {
				this.addComponent(button);
				if (!this.components.isEmpty()) {
					yPos += button.getHeight() + 2;
					this.setHeight(yPos);
				}
			}
		}
	}

	protected void handleDragging(int x, int y) {
		int height = this.getHeight();
		int width = this.getWidth();

		if (dragging) {
			xPos = (x - startX);
			yPos = (y - startY);

			if (yPos <= 0)
				yPos = 4;
			else if (isEnabled()
					&& yPos + height >= getWrapper().getScaledResolution()
					.getScaledHeight())
				yPos = getWrapper().getScaledResolution().getScaledHeight() - 4;
			if (yPos + height >= getWrapper().getScaledResolution()
					.getScaledHeight() && isEnabled())
				yPos = (getWrapper().getScaledResolution().getScaledHeight()
						- height - 4);
			else if (yPos + 14 >= getWrapper().getScaledResolution()
					.getScaledHeight() && !isEnabled())
				yPos = getWrapper().getScaledResolution().getScaledHeight() - 18;
			if (xPos <= 0)
				xPos = 4;
			if (xPos + width >= getWrapper().getScaledResolution()
					.getScaledWidth())
				xPos = (getWrapper().getScaledResolution().getScaledWidth()
						- width - 4);
		}
	}

	public boolean isMouseInHeader(final int par1, final int par2,
			final int par3) {
		return par1 > xPos && par1 < xPos + width && par2 > yPos
				&& par2 < yPos + (enabled ? height : yPos + 14);
	}

	@Override
	public void mouseClicked(final int x, final int y, final int key) {
		startX = x - xPos;
		startY = y - yPos;

		final boolean canDrag = x > xPos && x < xPos + width && y > yPos
				&& y < yPos + 14;
		final boolean isInsidePanel = x > xPos && x < xPos + width && y > yPos
				&& y < yPos + height;

		if (isInsidePanel && (this.enabled || (canDrag && key == 1))) {
			this.current = this;
		}

		if (canDrag) {
			if (key == 0) {
				this.dragging = true;
			} else if (key == 1) {
				this.enabled = !this.enabled;
			}

			super.mouseClicked(x, y, key);
		}

		if (!enabled) {
			return;
		}

		for (final Component c : this.getComponents()) {
			if (c instanceof LithiumButton) {
				LithiumButton button = (LithiumButton) c;
				if (button.isMouseOver(x, y)) {
					button.toggleModule();
					if (button.getClickedModule().isEnabled()) {
						Wrapper.getInstance().getMinecraft().sndManager.playSoundFX("random.click", 1F, 0.6F);
					} else {
						Wrapper.getInstance().getMinecraft().sndManager.playSoundFX("random.click", 1F, 1F);
					}
				}
			}
		}

	}

	@Override
	public void mouseMovedOrUp(int x, int y, int key) {
		if (key == 0) {
			this.dragging = false;
		}
	}

}
