package org.ramisme.pringles.ui.click.themes.handler;

import java.util.LinkedList;
import java.util.List;

/**
 * Manage themes.
 * 
 * @author Ramisme
 * @since Jun 5, 2013
 */
public final class ThemeManager {
	private Theme theme;
	private static final List<Theme> themes = new LinkedList<Theme>();
	private static final ThemeManager instance = new ThemeManager();

	public static ThemeManager getInstance() {
		return instance;
	}

	public Theme[] getThemes() {
		return themes.toArray(new Theme[themes.size()]);
	}

	public void setTheme(final Theme theme) {
		this.theme = theme;
	}

	public Theme getTheme() {
		return this.theme;
	}

}
