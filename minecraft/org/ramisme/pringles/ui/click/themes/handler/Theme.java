package org.ramisme.pringles.ui.click.themes.handler;

import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.modules.addons.ModuleCategory;
import org.ramisme.pringles.ui.click.components.Button;
import org.ramisme.pringles.ui.click.components.Label;
import org.ramisme.pringles.ui.click.components.Panel;
import org.ramisme.pringles.ui.click.components.combo.ComboBox;
import org.ramisme.pringles.ui.click.components.combo.ComboComponent;

/**
 * Custom theme control.
 * 
 * @author Ramisme
 * @since Jun 5, 2013
 */
public interface Theme {

	/**
	 * Return the custom panel instance.
	 * 
	 * @param panel
	 * @return
	 */
	public Panel getThemedPanel(final int x, final int y, final int width,
			final int height, final String label, final ModuleCategory category);

	/**
	 * Return the custom label instance.
	 * 
	 * @return
	 */
	public Label getThemedLabel(final int x, final int y, final Module Module);

	/**
	 * Return the custom label instance with a specified
	 * <code>String<code> label.
	 * 
	 * @param x
	 * @param y
	 * @param label
	 * @return
	 */
	public Label getThemedLabel(final int x, final int y, final String label);

	/**
	 * Return the custom button instance.
	 * 
	 * @param button
	 * @return
	 */
	public Button getThemedButton(final int x, final int y, final int width,
			final int height, final Module module);

	/**
	 * Return the custom combo box component instance.
	 * 
	 * @param button
	 * @return
	 */
	public ComboComponent getThemedComboComponent(final int x, final int y,
			final int width, final int height, final String label);

	/**
	 * Return the custom combo box instance.
	 * 
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param label
	 * @return
	 */
	public ComboBox getThemedComboBox(final int x, final int y,
			final int width, final int height, final String label);

}
