package org.ramisme.pringles.ui.click.themes.huzuni;

import org.lwjgl.opengl.GL11;
import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.Wrapper;
import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.modules.addons.ModuleCategory;
import org.ramisme.pringles.ui.click.components.Button;
import org.ramisme.pringles.ui.click.components.Component;
import org.ramisme.pringles.ui.click.components.Panel;
import org.ramisme.pringles.ui.click.themes.handler.ThemeManager;
import org.ramisme.pringles.ui.click.themes.pringles.PringlesButton;

/**
 * 
 * @author Ramisme
 * @since Jun 5, 2013
 */
public class HuzuniPanel extends Panel {
	private final ThemeManager themeManager = ThemeManager.getInstance();

	public HuzuniPanel(final int xPos, final int yPos, final int width,
			final int height, final String label,
			final ModuleCategory moduleCategory) {
		super(xPos, yPos, width, height, label, moduleCategory);

		this.setFont(Pringles.getInstance().getFactory().getFontHandler()
				.getBoldFont());
	}

	@Override
	public void draw(int x, int y) {
		handleDragging(x, y);
		this.drawObjects();

		GL11.glPushMatrix();
		GL11.glEnable(GL11.GL_BLEND);

		if (this.enabled) {
			render.drawBorderedRect(getXPos(), getYPos() + 14, getXPos()
					+ getWidth(), getYPos() + getHeight(), 0xFF212226,
					0xFF333333);

			for (Component component : getComponents()) {
				component.draw(x, y);
			}
		}

		render.drawBorderedRect(xPos, yPos, xPos + width, yPos + 14,
				0xFF212226, 0xFF333333);

		font.drawString(getTitle(), getXPos() + 4, getYPos() + 1, 0xFFFFFFFF);

		GL11.glPopMatrix();
	}

	/**
	 * Called when the panel is drawn
	 * 
	 * @param panel
	 */
	protected void drawObjects() {
		this.clearComponents();

		int yPos = 16;
		for (final Module module : Pringles.getInstance().getFactory()
				.getModuleManager().getModulesList()) {
			final Button button = themeManager.getTheme()
					.getThemedButton(this.xPos + 2, this.yPos + yPos,
							this.width - 4, 14, module);
			if (module.getCategory() == category) {
				this.addComponent(button);
				yPos += button.getHeight() + 2;
				this.setHeight(yPos);
			}
		}
	}

	protected void handleDragging(int x, int y) {
		int height = this.getHeight();
		int width = this.getWidth();

		if (dragging) {
			xPos = (x - startX);
			yPos = (y - startY);

			if (yPos <= 0)
				yPos = 4;
			else if (isEnabled()
					&& yPos + height >= getWrapper().getScaledResolution()
							.getScaledHeight())
				yPos = getWrapper().getScaledResolution().getScaledHeight() - 4;
			if (yPos + height >= getWrapper().getScaledResolution()
					.getScaledHeight() && isEnabled())
				yPos = (getWrapper().getScaledResolution().getScaledHeight()
						- height - 4);
			else if (yPos + 14 >= getWrapper().getScaledResolution()
					.getScaledHeight() && !isEnabled())
				yPos = getWrapper().getScaledResolution().getScaledHeight() - 18;
			if (xPos <= 0)
				xPos = 4;
			if (xPos + width >= getWrapper().getScaledResolution()
					.getScaledWidth())
				xPos = (getWrapper().getScaledResolution().getScaledWidth()
						- width - 4);
		}
	}

	@Override
	public void mouseClicked(final int x, final int y, final int key) {
		super.mouseClicked(x, y, key);

		startX = x - xPos;
		startY = y - yPos;

		final boolean canDrag = x > xPos && x < xPos + width && y > yPos
				&& y < yPos + 14;
		final boolean isInsidePanel = x > xPos && x < xPos + width && y > yPos
				&& y < yPos + height;

		if (isInsidePanel && (this.enabled || (canDrag && key == 1))) {
			this.current = this;
		}

		if (canDrag) {
			if (key == 0) {
				this.dragging = true;
			} else if (key == 1) {
				this.enabled = !this.enabled;
			}

			super.mouseClicked(x, y, key);
		}

		if (!enabled) {
			return;
		}

		for (final Component c : this.getComponents()) {
			if (c instanceof Button) {
				Button button = (Button) c;
				if (button.isMouseOver(x, y)) {
					button.toggle();
					Wrapper.getInstance().getMinecraft().sndManager
							.playSoundFX("random.click", 1.0F, 1.0F);
				}
			}
		}

	}

	@Override
	public void mouseMovedOrUp(int x, int y, int key) {
		if (key == 0) {
			this.dragging = false;
		}
	}

}
