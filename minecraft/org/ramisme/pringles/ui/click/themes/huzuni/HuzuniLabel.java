package org.ramisme.pringles.ui.click.themes.huzuni;

import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.ui.click.components.Label;

public class HuzuniLabel extends Label {

	public HuzuniLabel(final int xPos, final int yPos, final Module module) {
		super(xPos, yPos, module);
	}

	public HuzuniLabel(final int xPos, final int yPos, final String label) {
		super(xPos, yPos, label);
	}

	@Override
	public void draw(final int x, final int y) {
		if (module != null) {
			this.color = this.module.isEnabled() ? 0xFF2E9AFE : 0xFFFFFFFF;
		}
		super.draw(x, y);
	}
}
