package org.ramisme.pringles.ui.click.themes.huzuni;

import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.opengl.Render2D;
import org.ramisme.pringles.ui.click.GuiClick;
import org.ramisme.pringles.ui.click.components.Button;
import org.ramisme.pringles.ui.click.components.Label;

public final class HuzuniButton extends Button {

	public HuzuniButton(int x, int y, int width, int height, Module module) {
		super(x, y, width, height, module);
	}

	@Override
	public void draw(final int x, final int y) {
		if (isMouseOver(x, y)) {
			Render2D.getInstance().drawBorderedRect(xPos, yPos, xPos + width,
					yPos + height, 0x20000000, 0x80000000);
		}

		if (module.isEnabled()) {
			Render2D.getInstance().drawBorderedRect(xPos, yPos, xPos + width,
					yPos + height, 0xFF2e68a2, 0xFF4c99e5);
		} else {
			Render2D.getInstance().drawBorderedRect(xPos, yPos, xPos + width,
					yPos + height, 0xFF3f403f, 0xFF666666);
		}

		final Label label = GuiClick.themeManager.getTheme().getThemedLabel(
				xPos + 2, yPos + 2, module);
		label.draw(x, y);
	}

	@Override
	public void mouseClicked(final int x, final int y, final int key) {
		super.mouseClicked(x, y, key);
		toggle();
	}

	@Override
	public void mouseMovedOrUp(final int x, final int y, final int key) {
		return;
	}

}
