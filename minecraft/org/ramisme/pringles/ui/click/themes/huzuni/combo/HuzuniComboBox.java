package org.ramisme.pringles.ui.click.themes.huzuni.combo;

import java.util.LinkedList;
import java.util.List;

import org.ramisme.pringles.opengl.Render2D;
import org.ramisme.pringles.ui.click.GuiClick;
import org.ramisme.pringles.ui.click.components.Label;
import org.ramisme.pringles.ui.click.components.combo.ComboBox;
import org.ramisme.pringles.ui.click.components.combo.ComboComponent;
import org.ramisme.pringles.ui.click.themes.handler.Theme;

/**
 * Pringles themed combobox.
 * 
 * @author Ramisme
 * @since Jun 15, 2013
 */
public final class HuzuniComboBox extends ComboBox<Theme> {
	private final List<ComboComponent> comboComponents = new LinkedList<ComboComponent>();

	public HuzuniComboBox(int xPos, int yPos, int width, int height, String label) {
		super(xPos, yPos, width, height, label, themeManager.getThemes());
	}

	@Override
	public void draw(int x, int y) {
		if (isMouseOver(x, y)) {
			Render2D.getInstance().drawBorderedRect(xPos, yPos, xPos + width,
					yPos + height, 0x20000000, 0x80000000);
		}

		if (this.isOpen) {
			Render2D.getInstance().drawBorderedRect(xPos, yPos, xPos + width,
					yPos + height, 0xFF2e68a2, 0xFF4c99e5);
		} else {
			Render2D.getInstance().drawBorderedRect(xPos, yPos, xPos + width,
					yPos + height, 0xFF3f403f, 0xFF666666);
		}

		final Label displayLabel = GuiClick.themeManager.getTheme()
				.getThemedLabel(xPos + 2, yPos + 2, label);
		displayLabel.draw(x, y);

		if (this.isOpen) {
			int posY = yPos;
			for (final Theme theme : this.components) {
				final ComboComponent component = themeManager.getTheme()
						.getThemedComboComponent(xPos, posY, width, height,
								theme.getClass().getName());
				posY += (this.height + 2);
				comboComponents.add(component);
				component.draw(x, y);
			}
		}
	}

	@Override
	public void mouseClicked(final int x, final int y, final int key) {
		super.mouseClicked(x, y, key);
		this.isOpen = !this.isOpen;

		if (this.isOpen) {
			for (final ComboComponent component : comboComponents) {
				component.mouseClicked(x, y, key);
			}
		}
	}

	@Override
	public void mouseMovedOrUp(int x, int y, int key) {
		return;
	}

}
