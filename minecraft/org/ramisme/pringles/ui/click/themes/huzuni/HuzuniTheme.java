package org.ramisme.pringles.ui.click.themes.huzuni;

import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.modules.addons.ModuleCategory;
import org.ramisme.pringles.ui.click.components.Button;
import org.ramisme.pringles.ui.click.components.Label;
import org.ramisme.pringles.ui.click.components.Panel;
import org.ramisme.pringles.ui.click.components.combo.ComboBox;
import org.ramisme.pringles.ui.click.components.combo.ComboComponent;
import org.ramisme.pringles.ui.click.themes.handler.Theme;
import org.ramisme.pringles.ui.click.themes.huzuni.combo.HuzuniComboBox;
import org.ramisme.pringles.ui.click.themes.huzuni.combo.HuzuniComboComponent;

/**
 * Basic Huzuni theme
 * 
 * @author Ramisme
 * @since Jun 5, 2013
 */
public final class HuzuniTheme implements Theme {

	@Override
	public Panel getThemedPanel(int x, int y, int width, int height,
			String label, ModuleCategory category) {
		return (new HuzuniPanel(x, y, width, height, label, category));
	}

	@Override
	public Label getThemedLabel(int x, int y, Module module) {
		return (new HuzuniLabel(x, y, module));
	}

	@Override
	public Label getThemedLabel(int x, int y, String string) {
		return (new HuzuniLabel(x, y, string));
	}

	@Override
	public Button getThemedButton(final int x, final int y, final int width,
			final int height, final Module module) {
		return (new HuzuniButton(x, y, width, height, module));
	}

	@Override
	public ComboComponent getThemedComboComponent(int x, int y, int width,
			int height, String label) {
		return (new HuzuniComboComponent(x, y, width, height, label));
	}

	@Override
	public ComboBox getThemedComboBox(int x, int y, int width, int height,
			String label) {
		return (new HuzuniComboBox(x, y, width, height, label));
	}

}
