package org.ramisme.pringles.ui.click.themes.huzuni.combo;

import org.ramisme.pringles.opengl.Render2D;
import org.ramisme.pringles.ui.click.GuiClick;
import org.ramisme.pringles.ui.click.components.Label;
import org.ramisme.pringles.ui.click.components.combo.ComboComponent;

/**
 * Manage combo box components.
 * 
 * @author Ramisme
 * @since Jun 15, 2013
 */
public final class HuzuniComboComponent<Theme> extends ComboComponent {

	public HuzuniComboComponent(int xPos, int yPos, int width, int height,
			String label) {
		super(xPos, yPos, width, height, label);
	}

	@Override
	public void draw(int x, int y) {
		if (isMouseOver(x, y)) {
			Render2D.getInstance().drawBorderedRect(xPos, yPos, xPos + width,
					yPos + height, 0x20000000, 0x80000000);
		}

		if (this.isSelected()) {
			Render2D.getInstance().drawBorderedRect(xPos, yPos, xPos + width,
					yPos + height, 0xFF2e68a2, 0xFF4c99e5);
		} else {
			Render2D.getInstance().drawBorderedRect(xPos, yPos, xPos + width,
					yPos + height, 0xFF3f403f, 0xFF666666);
		}

		final Label displayLabel = GuiClick.themeManager.getTheme()
				.getThemedLabel(xPos + 2, yPos + 2, label);
		displayLabel.draw(x, y);
	}

	@Override
	public void mouseClicked(int x, int y, int key) {
		super.mouseClicked(x, y, key);

		if (isMouseOver(x, y) && key == 0) {
			this.selected = !this.isSelected();
		}
	}

	@Override
	public void mouseMovedOrUp(int x, int y, int key) {
		return;
	}

}
