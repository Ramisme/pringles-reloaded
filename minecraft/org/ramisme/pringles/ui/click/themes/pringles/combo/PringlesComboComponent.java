package org.ramisme.pringles.ui.click.themes.pringles.combo;

import org.ramisme.pringles.opengl.Render2D;
import org.ramisme.pringles.ui.click.GuiClick;
import org.ramisme.pringles.ui.click.components.Label;
import org.ramisme.pringles.ui.click.components.combo.ComboComponent;
import org.ramisme.pringles.ui.click.themes.handler.Theme;

/**
 * Manage combo box components.
 * 
 * @author Ramisme
 * @since Jun 15, 2013
 */
public final class PringlesComboComponent extends ComboComponent<Theme> {

	public PringlesComboComponent(int xPos, int yPos, int width, int height,
			String label) {
		super(xPos, yPos, width, height, label);
	}

	@Override
	public void draw(int x, int y) {
		Render2D.getInstance().drawBorderedRect(xPos, yPos, xPos + width,
				yPos + height, 0x40000000, 0x80000000);

		if (isMouseOver(x, y)) {
			Render2D.getInstance().drawBorderedRect(xPos, yPos, xPos + width,
					yPos + height, 0x20000000, 0x80000000);
		}

		if (this.isSelected()) {
			Render2D.getInstance().drawBorderedRect(xPos, yPos, xPos + width,
					yPos + height, 0x3000569C, 0x80000000);
		}

		final Label displayLabel = GuiClick.themeManager.getTheme()
				.getThemedLabel(xPos + 2, yPos + 2, label);
		displayLabel.draw(x, y);
	}

	@Override
	public void toggle() {
		super.toggle();
		themeManager.setTheme(getSelected());
	}

	@Override
	public void mouseClicked(int x, int y, int key) {
		super.mouseClicked(x, y, key);

		if (isMouseOver(x, y) && key == 0) {
			this.selected = !this.isSelected();
		}
	}

	@Override
	public void mouseMovedOrUp(int x, int y, int key) {
		return;
	}

}
