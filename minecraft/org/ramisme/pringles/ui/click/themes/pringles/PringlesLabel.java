package org.ramisme.pringles.ui.click.themes.pringles;

import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.ui.click.components.Component;
import org.ramisme.pringles.ui.click.components.Label;
import org.ramisme.pringles.ui.click.components.Panel;

public class PringlesLabel extends Label {

	public PringlesLabel(final int xPos, final int yPos, final Module module) {
		super(xPos, yPos, module);
	}

	public PringlesLabel(final int xPos, final int yPos, final String label) {
		super(xPos, yPos, label);
	}

	@Override
	public void draw(final int x, final int y) {
		if (module != null) {
			this.color = this.module.isEnabled() ? 0xFF2E9AFE : 0xFFFFFFFF;
		}
		super.draw(x, y);
	}
}
