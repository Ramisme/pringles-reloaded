package org.ramisme.pringles.ui.click.themes.pringles;

import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.opengl.Render2D;
import org.ramisme.pringles.ui.click.GuiClick;
import org.ramisme.pringles.ui.click.components.Button;
import org.ramisme.pringles.ui.click.components.Label;

public final class PringlesButton extends Button {

	public PringlesButton(int x, int y, int width, int height, Module module) {
		super(x, y, width, height, module);
	}

	@Override
	public void draw(final int x, final int y) {
		Render2D.getInstance().drawBorderedRect(xPos, yPos, xPos + width,
				yPos + height, 0x40000000, 0x80000000);

		if (isMouseOver(x, y)) {
			Render2D.getInstance().drawBorderedRect(xPos, yPos, xPos + width,
					yPos + height, 0x20000000, 0x80000000);
		}

		if (module.isEnabled()) {
			Render2D.getInstance().drawBorderedRect(xPos, yPos, xPos + width,
					yPos + height, 0x3000569C, 0x80000000);
		}

		final Label label = GuiClick.themeManager.getTheme().getThemedLabel(
				xPos + 2, yPos + 2, module);
		label.draw(x, y);
	}

	@Override
	public void mouseClicked(final int x, final int y, final int key) {
		super.mouseClicked(x, y, key);
		toggle();
	}

	@Override
	public void mouseMovedOrUp(final int x, final int y, final int key) {
		return;
	}

}
