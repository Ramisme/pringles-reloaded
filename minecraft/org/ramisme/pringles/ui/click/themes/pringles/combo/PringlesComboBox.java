package org.ramisme.pringles.ui.click.themes.pringles.combo;

import org.ramisme.pringles.opengl.Render2D;
import org.ramisme.pringles.ui.click.GuiClick;
import org.ramisme.pringles.ui.click.components.Label;
import org.ramisme.pringles.ui.click.components.combo.ComboBox;
import org.ramisme.pringles.ui.click.components.combo.ComboComponent;
import org.ramisme.pringles.ui.click.themes.handler.Theme;

/**
 * Pringles themed combobox.
 * 
 * @author Ramisme
 * @since Jun 15, 2013
 */
public final class PringlesComboBox extends ComboBox<Theme> {

	public PringlesComboBox(int xPos, int yPos, int width, int height,
			String label) {
		super(xPos, yPos, width, height, label, themeManager.getThemes());
	}

	@Override
	public void draw(int x, int y) {
		Render2D.getInstance().drawBorderedRect(xPos, yPos, xPos + width,
				yPos + height, 0x40000000, 0x80000000);

		if (isMouseOver(x, y)) {
			Render2D.getInstance().drawBorderedRect(xPos, yPos, xPos + width,
					yPos + height, 0x20000000, 0x80000000);
		}

		if (this.isOpen) {
			Render2D.getInstance().drawBorderedRect(xPos, yPos, xPos + width,
					yPos + height, 0x3000569C, 0x80000000);
		}

		final Label label = themeManager.getTheme().getThemedLabel(xPos + 2,
				yPos + 2, this.label);
		label.draw(x, y);

		if (this.isOpen) {
			int posY = yPos;
			for (final Theme theme : this.components) {
				final ComboComponent<Theme> component = themeManager.getTheme()
						.getThemedComboComponent(xPos, posY, width, height,
								theme.getClass().getName());
				component.draw(x, posY);
				posY += (this.height + 2);
			}
		}
	}

	@Override
	public void mouseClicked(final int x, final int y, final int key) {
		super.mouseClicked(x, y, key);
		this.isOpen = !this.isOpen;

	}

	@Override
	public void mouseMovedOrUp(int x, int y, int key) {
		return;
	}

}
