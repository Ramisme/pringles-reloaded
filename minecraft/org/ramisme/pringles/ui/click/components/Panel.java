package org.ramisme.pringles.ui.click.components;

import java.util.LinkedList;
import java.util.List;

import org.lwjgl.opengl.GL11;
import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.Wrapper;
import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.modules.addons.ModuleCategory;
import org.ramisme.pringles.opengl.Render2D;
import org.ramisme.pringles.slick.UnicodeFontRenderer;
import org.ramisme.pringles.ui.click.themes.pringles.PringlesButton;

/**
 * @author Ramisme
 * @since Apr 17, 2013
 */
public abstract class Panel extends Component {
	protected final List<Component> components = new LinkedList<Component>();

	protected final Render2D render = Render2D.getInstance();
	protected UnicodeFontRenderer font;

	private String title;
	protected boolean dragging, enabled;

	protected int startX, startY;
	protected ModuleCategory category;
	protected static Panel current;

	public Panel(final int xPos, final int yPos, final int width,
			final int height, final String name, final ModuleCategory category) {
		super(xPos, yPos, width, height);
		this.title = name.toUpperCase();
		this.category = category;
		font = Pringles.getInstance().getFactory().getFontHandler()
				.getGuiFont();
	}

	public boolean isMouseInHeader(final int par1, final int par2,
			final int par3) {
		return par1 > xPos && par1 < xPos + width && par2 > yPos
				&& par2 < yPos + (enabled ? height : yPos + 14);
	}

	public void addComponent(final Component component) {
		if (this.components.contains(component)) {
			this.components.remove(component);
		} else {
			this.components.add(component);
		}
	}

	public String getTitle() {
		return title;
	}

	public boolean isDragging() {
		return dragging;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public Component[] getComponents() {
		return components.toArray(new Component[components.size()]);
	}

	public void clearComponents() {
		this.components.clear();
	}

	public void setEnabled(boolean state) {
		this.enabled = state;
	}

	public ModuleCategory getCategory() {
		return this.category;
	}

	public void setCategory(final ModuleCategory category) {
		this.category = category;
	}
	
	public void setFont(final UnicodeFontRenderer font) {
		this.font = font;
	}

}
