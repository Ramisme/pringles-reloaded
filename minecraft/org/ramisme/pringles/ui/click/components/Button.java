package org.ramisme.pringles.ui.click.components;

import org.ramisme.pringles.modules.Module;

/**
 * Clickable region.
 * 
 * @author ramisme
 * 
 */
public abstract class Button extends Component {
	protected Module module;

	public Button(final int xPos, final int yPos, final Module module) {
		this(xPos, yPos, 20, 14, module);
	}

	public Button(final int xPos, final int yPos, final int width,
			final int height, final Module module) {
		super(xPos, yPos, width, height);
		this.module = module;
	}

	public void toggle() {
		if (module == null) {
			return;
		}

		module.onToggle();
	}

}
