package org.ramisme.pringles.ui.click.components;

import org.ramisme.pringles.Wrapper;
import org.ramisme.pringles.ui.click.GuiClick;
import org.ramisme.pringles.ui.click.themes.handler.ThemeManager;

/**
 * Base component object.
 * 
 * @author Ramisme
 * @since Apr 17, 2013
 * 
 */
public abstract class Component {
	protected final static ThemeManager themeManager = GuiClick.themeManager;
	protected int xPos, yPos, width, height;

	public Component(final int xPos, final int yPos, final int width,
			final int height) {
		this.xPos = xPos;
		this.yPos = yPos;
		this.width = width;
		this.height = height;
	}

	/**
	 * Draw the component on the screen.
	 */
	public abstract void draw(int x, int y);

	/**
	 * Fired when the mouse is clicked.
	 * 
	 * @param x
	 * @param y
	 */
	public void mouseClicked(int x, int y, int key) {
		getWrapper().getMinecraft().sndManager
				.playSoundFX("click.random", 1, 1);
	}

	/**
	 * 
	 * @param x
	 * @param y
	 */
	public abstract void mouseMovedOrUp(int x, int y, int key);

	/**
	 * Determines whether the mouse is hovering over the specified button
	 * region.
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean isMouseOver(final int x, final int y) {
		return (x > this.xPos && x < this.xPos + this.width && y > this.yPos && y < this.yPos
				+ this.height);
	}

	public int getXPos() {
		return xPos;
	}

	public void setXPos(int xPos) {
		this.xPos = xPos;
	}

	public int getYPos() {
		return yPos;
	}

	public void setYPos(int yPos) {
		this.yPos = yPos;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	protected Wrapper getWrapper() {
		return Wrapper.getInstance();
	}

}
