package org.ramisme.pringles.ui.click.components;

import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.slick.UnicodeFontRenderer;

/**
 * @author Ramisme
 */
public abstract class Label extends Component {
	protected int color = 0xFFFFFFFF;
	protected Module module;
	protected String label;

	public Label(int xPos, int yPos, String label) {
		super(xPos, yPos, -1, -1);
		this.label = label;
	}

	public Label(int xPos, int yPos, Module module) {
		super(xPos, yPos, -1, -1);
		this.module = module;
	}

	@Override
	public void draw(final int x, final int y) {
		getFontRenderer().drawStringWithShadow(
				module == null ? label : module.getModuleTag(), xPos, yPos,
				color);
	}

	@Override
	public void mouseMovedOrUp(final int x, final int y, final int key) {
		return;
	}

	protected UnicodeFontRenderer getFontRenderer() {
		return Pringles.getInstance().getFactory().getFontHandler()
				.getGuiFont();
	}
}
