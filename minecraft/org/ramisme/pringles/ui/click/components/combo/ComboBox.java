package org.ramisme.pringles.ui.click.components.combo;

import java.util.LinkedList;
import java.util.List;

import org.ramisme.pringles.ui.click.components.Component;

/**
 * Generic <code>ComboBox</code> that accepts multiple parameter types.
 * 
 * @author Ramisme
 * @since Jun 15, 2013
 */
public abstract class ComboBox<T> extends Component {
	protected List<T> components = new LinkedList<T>();
	protected boolean isOpen;
	protected final String label;
	protected Type type = Type.SINGLE_SELECT;

	protected ComboComponent singleSelected;
	protected List<ComboComponent> multiSelected = new LinkedList<ComboComponent>();

	public ComboBox(int xPos, int yPos, int width, int height, String label) {
		this(xPos, yPos, width, height, label, null);
	}

	public ComboBox(int xPos, int yPos, int width, int height, String label,
			T[] components) {
		super(xPos, yPos, width, height);
		this.label = label;

		for (final T component : components) {
			this.add(component);
		}
	}

	protected void setType(final Type type) {
		this.type = type;
	}

	protected Type getType() {
		return this.type;
	}

	/**
	 * Register a component for the <code>ComboBox</code>
	 * 
	 * @param component
	 */
	public void add(final T component) {
		synchronized (this.components) {
			this.components.add(component);
		}
	}

	/**
	 * Add to the selected components.
	 * 
	 * @param component
	 */
	public void addSelected(final ComboComponent component) {
		synchronized (this.multiSelected) {
			this.multiSelected.add(component);
		}
	}

	/**
	 * Remove a currently selected component.
	 * 
	 * @param component
	 */
	public void removeSelected(final ComboComponent component) {
		synchronized (this.multiSelected) {
			if (component.isSelected()) {
				this.multiSelected.remove(component);
			}
		}
	}

	/**
	 * Set the single selected component.
	 * 
	 * @param component
	 */
	public void setSelected(final ComboComponent component) {
		this.singleSelected = component;
	}

	public enum Type {
		MULTI_SELECT, SINGLE_SELECT;
	}

}
