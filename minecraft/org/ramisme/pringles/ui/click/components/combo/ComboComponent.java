package org.ramisme.pringles.ui.click.components.combo;

import org.ramisme.pringles.ui.click.GuiClick;
import org.ramisme.pringles.ui.click.components.Component;
import org.ramisme.pringles.ui.click.themes.handler.Theme;

/**
 * Components added the the <code>ComboBox</code> component container list.
 * 
 * @author Ramisme
 * @since Jun 15, 2013
 */
public abstract class ComboComponent<T> extends Component {
	protected boolean selected;
	protected T selectedObject;
	protected String label;

	public ComboComponent(int xPos, int yPos, int width, int height,
			String label) {
		super(xPos, yPos, width, height);
		this.label = label;
	}

	public void toggle() {
		this.selected = !this.selected;
		GuiClick.themeManager.setTheme((Theme)getSelected());
	}

	public boolean isSelected() {
		return this.selected;
	}

	public T getSelected() {
		return this.selectedObject;
	}

}
