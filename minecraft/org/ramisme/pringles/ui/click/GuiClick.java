package org.ramisme.pringles.ui.click;

import net.minecraft.src.GuiScreen;

import org.ramisme.pringles.Wrapper;
import org.ramisme.pringles.handlers.GuiHandler;
import org.ramisme.pringles.modules.addons.ModuleCategory;
import org.ramisme.pringles.ui.click.components.Panel;
import org.ramisme.pringles.ui.click.themes.handler.ThemeManager;
import org.ramisme.pringles.ui.click.themes.lithium.LithiumTheme;

/**
 * Handles all actual rendering of the Pringles GUI.
 * 
 * @author Ramisme
 * @since Apr 17, 2013
 * 
 */
public class GuiClick extends GuiScreen {
	public final static ThemeManager themeManager = ThemeManager.getInstance();
	private static Manager manager = Manager.getInstance();

	public GuiClick() {
		themeManager.setTheme(new LithiumTheme());

		manager.clearPanels();
		if (!manager.getPanelList().isEmpty()) {
			return;
		}

		int posX = 4, posY = 4;
		for (final ModuleCategory category : ModuleCategory.values()) {
			if (category == ModuleCategory.NONE
					|| category == ModuleCategory.MISC
					|| category == ModuleCategory.DERP
					|| category == ModuleCategory.COMMAND) {
				continue;
			}

			final Panel panel = themeManager.getTheme().getThemedPanel(posX,
					posY, 100, 10, category.name(), category);
			System.out.println(panel.getClass().getCanonicalName());
			manager.addPanel(panel);

			posX += (panel.getWidth() + 4);
			if (posX >= Wrapper.getInstance().getScaledResolution()
					.getScaledWidth() - 100) {
				posX = 4;
				posY += 16;
			}
		}

		GuiHandler.getInstance().onLoad();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.minecraft.src.GuiScreen#mouseClicked(int, int, int)
	 */
	@Override
	public void mouseClicked(final int par1, final int par2, final int par3) {
		super.mouseClicked(par1, par2, par3);
		for (final Panel window : manager.getPanels()) {
			window.mouseClicked(par1, par2, par3);
			if (((Panel) window).isMouseInHeader(par1, par2, par3)) {
				manager.getPanelList().remove(window);
				manager.getPanelList().add(manager.getPanelList().size(),
						window);
				return;
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.minecraft.src.GuiScreen#mouseMovedOrUp(int, int, int)
	 */
	@Override
	public void mouseMovedOrUp(final int par1, final int par2, final int par3) {
		super.mouseMovedOrUp(par1, par2, par3);

		for (final Panel window : manager.getPanels()) {
			window.mouseMovedOrUp(par1, par2, par3);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.minecraft.src.GuiScreen#drawScreen(int, int, float)
	 */
	@Override
	public void drawScreen(final int par1, final int par2, final float par3) {
		super.drawScreen(par1, par2, par3);
		drawDefaultBackground();

		for (final Panel window : manager.getPanels()) {
			window.draw(par1, par2);
		}
	}

	public void onGuiClosed() {
		GuiHandler.getInstance().onSave();
	}
}
