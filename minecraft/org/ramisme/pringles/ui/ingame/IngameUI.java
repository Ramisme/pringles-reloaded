package org.ramisme.pringles.ui.ingame;

import net.minecraft.client.Minecraft;
import net.minecraft.src.FontRenderer;
import net.minecraft.src.GuiIngame;

import org.lwjgl.opengl.GL11;
import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.Wrapper;
import org.ramisme.pringles.handlers.WarningHandler;
import org.ramisme.pringles.modules.Module;
import org.ramisme.pringles.modules.world.wallhack.WallhackUtil;

/**
 * Ingame UI handler.
 * 
 * @author Ramisme
 * @since Apr 6, 2013
 * 
 */
public class IngameUI extends GuiIngame {
	private final WarningHandler warning = WarningHandler.getInstance();

	public IngameUI(final Minecraft par1Minecraft) {
		super(par1Minecraft);
	}

	@Override
	public void renderGameOverlay(final float par1, final boolean par2,
			final int par3, final int par4) {
		super.renderGameOverlay(par1, par2, par3, par4);
		warning.updateWarnings();

		/*
		 * Spider-Man 'cause fuck you.
		 * 
		 * GL11.glPushMatrix();
		 * Wrapper.getInstance().getMinecraft().renderEngine
		 * .bindTexture("/textures/spiderman.png"); GL11.glColor4f(1, 1, 1,
		 * 0.2F); GL11.glEnable(GL11.GL_BLEND); drawTexturedModalRect(0, 0, 0,
		 * 0, Wrapper.getInstance() .getScaledResolution().getScaledWidth(),
		 * Wrapper.getInstance() .getScaledResolution().getScaledHeight());
		 * GL11.glPopMatrix();
		 */

		if (WallhackUtil.getInstance().isEnabled()) {
			GL11.glDisable(GL11.GL_BLEND);
		}

		final FontRenderer fontRenderer = Wrapper.getInstance().getMinecraft().fontRenderer;

		if (!Wrapper.getInstance().getMinecraft().gameSettings.showDebugInfo) {
			drawString(fontRenderer, "Pringles", 2, 2, 0xFFFFFFFF);

			final int width = Wrapper.getInstance().getScaledResolution()
					.getScaledWidth();
			int moduleY = 2;
			for (final Module module : Pringles.getInstance().getFactory()
					.getModuleManager().getModulesList()) {
				if (!(module.isEnabled() && module.isInGui())) {
					continue;
				}
				final String moduleName = module.getModuleTag();
				final int moduleX = width
						- fontRenderer.getStringWidth(moduleName) - 2;
				fontRenderer.drawStringWithShadow(moduleName, moduleX, moduleY,
						module.getModuleColor());
				moduleY += 10;
			}
		}

		warning.renderWarnings(this);
	}

}