package org.ramisme.pringles.ui.ingame;

import net.minecraft.client.Minecraft;
import net.minecraft.src.ChatLine;
import net.minecraft.src.GuiNewChat;
import net.minecraft.src.MathHelper;
import net.minecraft.src.StringUtils;

import org.lwjgl.opengl.GL11;
import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.Wrapper;
import org.ramisme.pringles.events.chat.ChatReceivedEvent;
import org.ramisme.pringles.handlers.FontHandler;
import org.ramisme.pringles.opengl.Render2D;

/**
 * Ingame Chat UI Handler. This chat will behave exactly like the Vanilla chat,
 * resizing etc.
 * 
 * @author Lynxaa
 * @since 17/4/2013
 */

public class ChatUI extends GuiNewChat {
	private Minecraft mc;

	public boolean dragging = false;
	private int posX, posY, mouseX, mouseY, startY;

	private int length;

	public ChatUI() {
		super(Wrapper.getInstance().getMinecraft());
		mc = Wrapper.getInstance().getMinecraft();
	}

	public void chatDragged(int x, int y) {
		posX = x - mouseX;
		posY = y - mouseY;
	}

	@Override
	public void drawChat(int par1) {
		if (this.mc.gameSettings.chatVisibility != 2) {
			int var2 = this.func_96127_i();
			boolean var3 = false, show = false;
			int var4 = 0;
			int var5 = this.field_96134_d.size();
			float var6 = this.mc.gameSettings.chatOpacity * 0.9F + 0.1F;

			if (var5 > 0) {
				if (this.getChatOpen()) {
					var3 = true;
				}

				float var7 = this.func_96131_h();
				int var8 = MathHelper.ceiling_float_int((float) this.func_96126_f() / var7);
				length = var8;
				GL11.glPushMatrix();
				GL11.glTranslated(posX, posY, 0);
				GL11.glScalef(var7, var7, 1.0F);
				int var9;
				int var11;
				int var14;
				int help = 0;
				int opacity;

				if (var5 > 0) {
					for (int k = 0; k + this.field_73768_d < this.field_96134_d
							.size() && k < var2; ++k) {

						ChatLine chatline = (ChatLine) this.field_96134_d.get(k
								+ this.field_73768_d);

						if (chatline != null) {
							int j1 = par1 - chatline.getUpdatedCounter();
							if (j1 >= 200 && !var3)
								continue;
							double d = (double) j1 / 200D;
							d = 1.0D - d;
							d *= 10D;
							if (d < 0.0D)
								d = 0.0D;
							if (d > 1.0D)
								d = 1.0D;
							d *= d;

							int l1 = (int) (255D * d);
							if (var3)
								l1 = 255;
							var4++;
							if (l1 > 3) {
								int j2 = -k * 9;
								help = j2 + 10;
								show = true;
							}
						}
					}
					if (show) {
						startY = help;
						if (this.getChatOpen()) {
							GL11.glPushMatrix();
							GL11.glTranslated(0, 0.7, 0);
							Render2D.getInstance().drawBorderedRect(3, help - 31, length + 10, help - 18, dragging ? 0x60000000 : 0x40000000, dragging ? 0x80000000 : 0x60000000);
							FontHandler.getBoldFont().drawStringWithShadow("Chat", 7, help - 30, -1);
							GL11.glPopMatrix();
						}
						Render2D.getInstance().drawBorderedRect(3, 8, length + 10, help - 16, dragging ? 0x60000000 : 0x40000000, dragging ? 0x80000000 : 0x60000000);
						drawRect(3, 8, length + 10, help - 16, dragging ? 0x60000000 : 0x40000000);
					}
				}

				for (var9 = 0; var9 + this.field_73768_d < this.field_96134_d
						.size() && var9 < var2; ++var9) {
					ChatLine var10 = (ChatLine) this.field_96134_d.get(var9
							+ this.field_73768_d);

					if (var10 != null) {
						var11 = par1 - var10.getUpdatedCounter();

						if (var11 < 200 || var3) {
							double var12 = (double) var11 / 200.0D;
							var12 = 1.0D - var12;
							var12 *= 10.0D;

							if (var12 < 0.0D) {
								var12 = 0.0D;
							}

							if (var12 > 1.0D) {
								var12 = 1.0D;
							}

							var12 *= var12;
							var14 = (int) (255.0D);

							if (var3) {
								var14 = 255;
							}

							var14 = (int) ((float) var14 * var6);
							++var4;

							if (var14 > 3) {
								byte var15 = 3;
								int var16 = -var9 * 9;
								GL11.glEnable(GL11.GL_BLEND);
								final ChatReceivedEvent event = new ChatReceivedEvent(var10.getChatLineString());
								Pringles.getInstance().getFactory().getEventManager().sendEvent(event);
								String var17 = event.getMessage();

								if (!this.mc.gameSettings.chatColours) {
									var17 = StringUtils.stripControlCodes(var17);
								}

								FontHandler.getInstance().getBoldFont().drawStringWithShadow(var17, var15 + 4, var16 - 5, 16777215 + (var14 << 24));
							}
						}
					}
				}

				GL11.glPopMatrix();
			}
		}
	}

	public void mouseClicked(int x, int y, int b) {
		position[0] = x;
		position[1] = y;
		if(b == 0) {
			int width = MathHelper.ceiling_float_int((float) this.func_96126_f() / this.func_96131_h()) + 20;
			if (x >= 3 + posX && y >= startY - 31 + Wrapper.getInstance().getScaledResolution().getScaledHeight() - 48 + posY && x <= width + posX && y <= startY - 17 + Wrapper.getInstance().getScaledResolution().getScaledHeight() - 48 + posY) {
				mouseX = x - posX;
				mouseY = y - posY;
				dragging = true;
			}
		}
	}

	private int[] position = new int[2];
	public void mouseMovedOrUp(int x, int y, int b) {
		position[0] = x;
		position[1] = y;

		if(b == 0) {
			dragging = false;
		}
	}

}