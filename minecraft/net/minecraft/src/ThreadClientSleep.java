package net.minecraft.src;

import net.minecraft.client.Minecraft;

public class ThreadClientSleep extends Thread {
	/** A reference to the Minecraft object. */
	final Minecraft mc;

	public ThreadClientSleep(final Minecraft par1Minecraft, final String par2Str) {
		super(par2Str);
		mc = par1Minecraft;
	}

	@Override
	public void run() {
		while (mc.running) {
			try {
				Thread.sleep(2147483647L);
			} catch (final InterruptedException var2) {
				;
			}
		}
	}
}
