package net.minecraft.src;

import java.util.Comparator;

public class EntitySorter implements Comparator {
	/** Entity position X */
	private final double entityPosX;

	/** Entity position Y */
	private final double entityPosY;

	/** Entity position Z */
	private final double entityPosZ;

	public EntitySorter(final Entity par1Entity) {
		entityPosX = -par1Entity.posX;
		entityPosY = -par1Entity.posY;
		entityPosZ = -par1Entity.posZ;
	}

	/**
	 * Sorts the two world renderers according to their distance to a given
	 * entity.
	 */
	public int sortByDistanceToEntity(final WorldRenderer par1WorldRenderer,
			final WorldRenderer par2WorldRenderer) {
		final double var3 = par1WorldRenderer.posXPlus + entityPosX;
		final double var5 = par1WorldRenderer.posYPlus + entityPosY;
		final double var7 = par1WorldRenderer.posZPlus + entityPosZ;
		final double var9 = par2WorldRenderer.posXPlus + entityPosX;
		final double var11 = par2WorldRenderer.posYPlus + entityPosY;
		final double var13 = par2WorldRenderer.posZPlus + entityPosZ;
		return (int) ((var3 * var3 + var5 * var5 + var7 * var7 - (var9 * var9
				+ var11 * var11 + var13 * var13)) * 1024.0D);
	}

	@Override
	public int compare(final Object par1Obj, final Object par2Obj) {
		return sortByDistanceToEntity((WorldRenderer) par1Obj,
				(WorldRenderer) par2Obj);
	}
}
