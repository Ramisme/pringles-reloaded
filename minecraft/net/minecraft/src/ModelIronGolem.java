package net.minecraft.src;

public class ModelIronGolem extends ModelBase {
	/** The head model for the iron golem. */
	public ModelRenderer ironGolemHead;

	/** The body model for the iron golem. */
	public ModelRenderer ironGolemBody;

	/** The right arm model for the iron golem. */
	public ModelRenderer ironGolemRightArm;

	/** The left arm model for the iron golem. */
	public ModelRenderer ironGolemLeftArm;

	/** The left leg model for the Iron Golem. */
	public ModelRenderer ironGolemLeftLeg;

	/** The right leg model for the Iron Golem. */
	public ModelRenderer ironGolemRightLeg;

	public ModelIronGolem() {
		this(0.0F);
	}

	public ModelIronGolem(final float par1) {
		this(par1, -7.0F);
	}

	public ModelIronGolem(final float par1, final float par2) {
		final short var3 = 128;
		final short var4 = 128;
		ironGolemHead = new ModelRenderer(this).setTextureSize(var3, var4);
		ironGolemHead.setRotationPoint(0.0F, 0.0F + par2, -2.0F);
		ironGolemHead.setTextureOffset(0, 0).addBox(-4.0F, -12.0F, -5.5F, 8,
				10, 8, par1);
		ironGolemHead.setTextureOffset(24, 0).addBox(-1.0F, -5.0F, -7.5F, 2, 4,
				2, par1);
		ironGolemBody = new ModelRenderer(this).setTextureSize(var3, var4);
		ironGolemBody.setRotationPoint(0.0F, 0.0F + par2, 0.0F);
		ironGolemBody.setTextureOffset(0, 40).addBox(-9.0F, -2.0F, -6.0F, 18,
				12, 11, par1);
		ironGolemBody.setTextureOffset(0, 70).addBox(-4.5F, 10.0F, -3.0F, 9, 5,
				6, par1 + 0.5F);
		ironGolemRightArm = new ModelRenderer(this).setTextureSize(var3, var4);
		ironGolemRightArm.setRotationPoint(0.0F, -7.0F, 0.0F);
		ironGolemRightArm.setTextureOffset(60, 21).addBox(-13.0F, -2.5F, -3.0F,
				4, 30, 6, par1);
		ironGolemLeftArm = new ModelRenderer(this).setTextureSize(var3, var4);
		ironGolemLeftArm.setRotationPoint(0.0F, -7.0F, 0.0F);
		ironGolemLeftArm.setTextureOffset(60, 58).addBox(9.0F, -2.5F, -3.0F, 4,
				30, 6, par1);
		ironGolemLeftLeg = new ModelRenderer(this, 0, 22).setTextureSize(var3,
				var4);
		ironGolemLeftLeg.setRotationPoint(-4.0F, 18.0F + par2, 0.0F);
		ironGolemLeftLeg.setTextureOffset(37, 0).addBox(-3.5F, -3.0F, -3.0F, 6,
				16, 5, par1);
		ironGolemRightLeg = new ModelRenderer(this, 0, 22).setTextureSize(var3,
				var4);
		ironGolemRightLeg.mirror = true;
		ironGolemRightLeg.setTextureOffset(60, 0).setRotationPoint(5.0F,
				18.0F + par2, 0.0F);
		ironGolemRightLeg.addBox(-3.5F, -3.0F, -3.0F, 6, 16, 5, par1);
	}

	/**
	 * Sets the models various rotation angles then renders the model.
	 */
	@Override
	public void render(final Entity par1Entity, final float par2,
			final float par3, final float par4, final float par5,
			final float par6, final float par7) {
		setRotationAngles(par2, par3, par4, par5, par6, par7, par1Entity);
		ironGolemHead.render(par7);
		ironGolemBody.render(par7);
		ironGolemLeftLeg.render(par7);
		ironGolemRightLeg.render(par7);
		ironGolemRightArm.render(par7);
		ironGolemLeftArm.render(par7);
	}

	/**
	 * Sets the model's various rotation angles. For bipeds, par1 and par2 are
	 * used for animating the movement of arms and legs, where par1 represents
	 * the time(so that arms and legs swing back and forth) and par2 represents
	 * how "far" arms and legs can swing at most.
	 */
	@Override
	public void setRotationAngles(final float par1, final float par2,
			final float par3, final float par4, final float par5,
			final float par6, final Entity par7Entity) {
		ironGolemHead.rotateAngleY = par4 / (180F / (float) Math.PI);
		ironGolemHead.rotateAngleX = par5 / (180F / (float) Math.PI);
		ironGolemLeftLeg.rotateAngleX = -1.5F * func_78172_a(par1, 13.0F)
				* par2;
		ironGolemRightLeg.rotateAngleX = 1.5F * func_78172_a(par1, 13.0F)
				* par2;
		ironGolemLeftLeg.rotateAngleY = 0.0F;
		ironGolemRightLeg.rotateAngleY = 0.0F;
	}

	/**
	 * Used for easily adding entity-dependent animations. The second and third
	 * float params here are the same second and third as in the
	 * setRotationAngles method.
	 */
	@Override
	public void setLivingAnimations(final EntityLiving par1EntityLiving,
			final float par2, final float par3, final float par4) {
		final EntityIronGolem var5 = (EntityIronGolem) par1EntityLiving;
		final int var6 = var5.getAttackTimer();

		if (var6 > 0) {
			ironGolemRightArm.rotateAngleX = -2.0F + 1.5F
					* func_78172_a(var6 - par4, 10.0F);
			ironGolemLeftArm.rotateAngleX = -2.0F + 1.5F
					* func_78172_a(var6 - par4, 10.0F);
		} else {
			final int var7 = var5.getHoldRoseTick();

			if (var7 > 0) {
				ironGolemRightArm.rotateAngleX = -0.8F + 0.025F
						* func_78172_a(var7, 70.0F);
				ironGolemLeftArm.rotateAngleX = 0.0F;
			} else {
				ironGolemRightArm.rotateAngleX = (-0.2F + 1.5F * func_78172_a(
						par2, 13.0F)) * par3;
				ironGolemLeftArm.rotateAngleX = (-0.2F - 1.5F * func_78172_a(
						par2, 13.0F)) * par3;
			}
		}
	}

	private float func_78172_a(final float par1, final float par2) {
		return (Math.abs(par1 % par2 - par2 * 0.5F) - par2 * 0.25F)
				/ (par2 * 0.25F);
	}
}
