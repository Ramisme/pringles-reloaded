package net.minecraft.src;

public class ModelBox {
	/**
	 * The (x,y,z) vertex positions and (u,v) texture coordinates for each of
	 * the 8 points on a cube
	 */
	private final PositionTextureVertex[] vertexPositions;

	/** An array of 6 TexturedQuads, one for each face of a cube */
	private final TexturedQuad[] quadList;

	/** X vertex coordinate of lower box corner */
	public final float posX1;

	/** Y vertex coordinate of lower box corner */
	public final float posY1;

	/** Z vertex coordinate of lower box corner */
	public final float posZ1;

	/** X vertex coordinate of upper box corner */
	public final float posX2;

	/** Y vertex coordinate of upper box corner */
	public final float posY2;

	/** Z vertex coordinate of upper box corner */
	public final float posZ2;
	public String field_78247_g;

	public ModelBox(final ModelRenderer par1ModelRenderer, final int par2,
			final int par3, float par4, float par5, float par6, final int par7,
			final int par8, final int par9, final float par10) {
		posX1 = par4;
		posY1 = par5;
		posZ1 = par6;
		posX2 = par4 + par7;
		posY2 = par5 + par8;
		posZ2 = par6 + par9;
		vertexPositions = new PositionTextureVertex[8];
		quadList = new TexturedQuad[6];
		float var11 = par4 + par7;
		float var12 = par5 + par8;
		float var13 = par6 + par9;
		par4 -= par10;
		par5 -= par10;
		par6 -= par10;
		var11 += par10;
		var12 += par10;
		var13 += par10;

		if (par1ModelRenderer.mirror) {
			final float var14 = var11;
			var11 = par4;
			par4 = var14;
		}

		final PositionTextureVertex var23 = new PositionTextureVertex(par4,
				par5, par6, 0.0F, 0.0F);
		final PositionTextureVertex var15 = new PositionTextureVertex(var11,
				par5, par6, 0.0F, 8.0F);
		final PositionTextureVertex var16 = new PositionTextureVertex(var11,
				var12, par6, 8.0F, 8.0F);
		final PositionTextureVertex var17 = new PositionTextureVertex(par4,
				var12, par6, 8.0F, 0.0F);
		final PositionTextureVertex var18 = new PositionTextureVertex(par4,
				par5, var13, 0.0F, 0.0F);
		final PositionTextureVertex var19 = new PositionTextureVertex(var11,
				par5, var13, 0.0F, 8.0F);
		final PositionTextureVertex var20 = new PositionTextureVertex(var11,
				var12, var13, 8.0F, 8.0F);
		final PositionTextureVertex var21 = new PositionTextureVertex(par4,
				var12, var13, 8.0F, 0.0F);
		vertexPositions[0] = var23;
		vertexPositions[1] = var15;
		vertexPositions[2] = var16;
		vertexPositions[3] = var17;
		vertexPositions[4] = var18;
		vertexPositions[5] = var19;
		vertexPositions[6] = var20;
		vertexPositions[7] = var21;
		quadList[0] = new TexturedQuad(new PositionTextureVertex[] { var19,
				var15, var16, var20 }, par2 + par9 + par7, par3 + par9, par2
				+ par9 + par7 + par9, par3 + par9 + par8,
				par1ModelRenderer.textureWidth, par1ModelRenderer.textureHeight);
		quadList[1] = new TexturedQuad(new PositionTextureVertex[] { var23,
				var18, var21, var17 }, par2, par3 + par9, par2 + par9, par3
				+ par9 + par8, par1ModelRenderer.textureWidth,
				par1ModelRenderer.textureHeight);
		quadList[2] = new TexturedQuad(new PositionTextureVertex[] { var19,
				var18, var23, var15 }, par2 + par9, par3, par2 + par9 + par7,
				par3 + par9, par1ModelRenderer.textureWidth,
				par1ModelRenderer.textureHeight);
		quadList[3] = new TexturedQuad(new PositionTextureVertex[] { var16,
				var17, var21, var20 }, par2 + par9 + par7, par3 + par9, par2
				+ par9 + par7 + par7, par3, par1ModelRenderer.textureWidth,
				par1ModelRenderer.textureHeight);
		quadList[4] = new TexturedQuad(new PositionTextureVertex[] { var15,
				var23, var17, var16 }, par2 + par9, par3 + par9, par2 + par9
				+ par7, par3 + par9 + par8, par1ModelRenderer.textureWidth,
				par1ModelRenderer.textureHeight);
		quadList[5] = new TexturedQuad(new PositionTextureVertex[] { var18,
				var19, var20, var21 }, par2 + par9 + par7 + par9, par3 + par9,
				par2 + par9 + par7 + par9 + par7, par3 + par9 + par8,
				par1ModelRenderer.textureWidth, par1ModelRenderer.textureHeight);

		if (par1ModelRenderer.mirror) {
			for (final TexturedQuad element : quadList) {
				element.flipFace();
			}
		}
	}

	/**
	 * Draw the six sided box defined by this ModelBox
	 */
	public void render(final Tessellator par1Tessellator, final float par2) {
		for (final TexturedQuad element : quadList) {
			element.draw(par1Tessellator, par2);
		}
	}

	public ModelBox func_78244_a(final String par1Str) {
		field_78247_g = par1Str;
		return this;
	}
}
