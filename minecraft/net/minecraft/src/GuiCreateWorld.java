package net.minecraft.src;

import java.util.Random;

import org.lwjgl.input.Keyboard;

public class GuiCreateWorld extends GuiScreen {
	private final GuiScreen parentGuiScreen;
	private GuiTextField textboxWorldName;
	private GuiTextField textboxSeed;
	private String folderName;

	/** hardcore', 'creative' or 'survival */
	private String gameMode = "survival";
	private boolean generateStructures = true;
	private boolean commandsAllowed = false;

	/** True iif player has clicked buttonAllowCommands at least once */
	private boolean commandsToggled = false;

	/** toggles when GUIButton 7 is pressed */
	private boolean bonusItems = false;

	/** True if and only if gameMode.equals("hardcore") */
	private boolean isHardcore = false;
	private boolean createClicked;

	/**
	 * True if the extra options (Seed box, structure toggle button, world type
	 * button, etc.) are being shown
	 */
	private boolean moreOptions;

	/** The GUIButton that you click to change game modes. */
	private GuiButton buttonGameMode;

	/**
	 * The GUIButton that you click to get to options like the seed when
	 * creating a world.
	 */
	private GuiButton moreWorldOptions;

	/** The GuiButton in the 'More World Options' screen. Toggles ON/OFF */
	private GuiButton buttonGenerateStructures;
	private GuiButton buttonBonusItems;

	/** The GuiButton in the more world options screen. */
	private GuiButton buttonWorldType;
	private GuiButton buttonAllowCommands;

	/** GuiButton in the more world options screen. */
	private GuiButton buttonCustomize;

	/** The first line of text describing the currently selected game mode. */
	private String gameModeDescriptionLine1;

	/** The second line of text describing the currently selected game mode. */
	private String gameModeDescriptionLine2;

	/** The current textboxSeed text */
	private String seed;

	/** E.g. New World, Neue Welt, Nieuwe wereld, Neuvo Mundo */
	private String localizedNewWorldText;
	private int worldTypeId = 0;

	/** Generator options to use when creating the world. */
	public String generatorOptionsToUse = "";

	/**
	 * If the world name is one of these, it'll be surrounded with underscores.
	 */
	private static final String[] ILLEGAL_WORLD_NAMES = new String[] { "CON",
			"COM", "PRN", "AUX", "CLOCK$", "NUL", "COM1", "COM2", "COM3",
			"COM4", "COM5", "COM6", "COM7", "COM8", "COM9", "LPT1", "LPT2",
			"LPT3", "LPT4", "LPT5", "LPT6", "LPT7", "LPT8", "LPT9" };

	public GuiCreateWorld(final GuiScreen par1GuiScreen) {
		parentGuiScreen = par1GuiScreen;
		seed = "";
		localizedNewWorldText = StatCollector
				.translateToLocal("selectWorld.newWorld");
	}

	/**
	 * Called from the main game loop to update the screen.
	 */
	@Override
	public void updateScreen() {
		textboxWorldName.updateCursorCounter();
		textboxSeed.updateCursorCounter();
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui() {
		final StringTranslate var1 = StringTranslate.getInstance();
		Keyboard.enableRepeatEvents(true);
		buttonList.clear();
		buttonList.add(new GuiButton(0, width / 2 - 155, height - 28, 150, 20,
				var1.translateKey("selectWorld.create")));
		buttonList.add(new GuiButton(1, width / 2 + 5, height - 28, 150, 20,
				var1.translateKey("gui.cancel")));
		buttonList.add(buttonGameMode = new GuiButton(2, width / 2 - 75, 115,
				150, 20, var1.translateKey("selectWorld.gameMode")));
		buttonList.add(moreWorldOptions = new GuiButton(3, width / 2 - 75, 187,
				150, 20, var1.translateKey("selectWorld.moreWorldOptions")));
		buttonList.add(buttonGenerateStructures = new GuiButton(4,
				width / 2 - 155, 100, 150, 20, var1
						.translateKey("selectWorld.mapFeatures")));
		buttonGenerateStructures.drawButton = false;
		buttonList.add(buttonBonusItems = new GuiButton(7, width / 2 + 5, 151,
				150, 20, var1.translateKey("selectWorld.bonusItems")));
		buttonBonusItems.drawButton = false;
		buttonList.add(buttonWorldType = new GuiButton(5, width / 2 + 5, 100,
				150, 20, var1.translateKey("selectWorld.mapType")));
		buttonWorldType.drawButton = false;
		buttonList.add(buttonAllowCommands = new GuiButton(6, width / 2 - 155,
				151, 150, 20, var1.translateKey("selectWorld.allowCommands")));
		buttonAllowCommands.drawButton = false;
		buttonList.add(buttonCustomize = new GuiButton(8, width / 2 + 5, 120,
				150, 20, var1.translateKey("selectWorld.customizeType")));
		buttonCustomize.drawButton = false;
		textboxWorldName = new GuiTextField(fontRenderer, width / 2 - 100, 60,
				200, 20);
		textboxWorldName.setFocused(true);
		textboxWorldName.setText(localizedNewWorldText);
		textboxSeed = new GuiTextField(fontRenderer, width / 2 - 100, 60, 200,
				20);
		textboxSeed.setText(seed);
		func_82288_a(moreOptions);
		makeUseableName();
		updateButtonText();
	}

	/**
	 * Makes a the name for a world save folder based on your world name,
	 * replacing specific characters for _s and appending -s to the end until a
	 * free name is available.
	 */
	private void makeUseableName() {
		folderName = textboxWorldName.getText().trim();
		final char[] var1 = ChatAllowedCharacters.allowedCharactersArray;
		final int var2 = var1.length;

		for (int var3 = 0; var3 < var2; ++var3) {
			final char var4 = var1[var3];
			folderName = folderName.replace(var4, '_');
		}

		if (MathHelper.stringNullOrLengthZero(folderName)) {
			folderName = "World";
		}

		folderName = GuiCreateWorld
				.func_73913_a(mc.getSaveLoader(), folderName);
	}

	private void updateButtonText() {
		final StringTranslate var1 = StringTranslate.getInstance();
		buttonGameMode.displayString = var1
				.translateKey("selectWorld.gameMode")
				+ " "
				+ var1.translateKey("selectWorld.gameMode." + gameMode);
		gameModeDescriptionLine1 = var1.translateKey("selectWorld.gameMode."
				+ gameMode + ".line1");
		gameModeDescriptionLine2 = var1.translateKey("selectWorld.gameMode."
				+ gameMode + ".line2");
		buttonGenerateStructures.displayString = var1
				.translateKey("selectWorld.mapFeatures") + " ";

		if (generateStructures) {
			buttonGenerateStructures.displayString = buttonGenerateStructures.displayString
					+ var1.translateKey("options.on");
		} else {
			buttonGenerateStructures.displayString = buttonGenerateStructures.displayString
					+ var1.translateKey("options.off");
		}

		buttonBonusItems.displayString = var1
				.translateKey("selectWorld.bonusItems") + " ";

		if (bonusItems && !isHardcore) {
			buttonBonusItems.displayString = buttonBonusItems.displayString
					+ var1.translateKey("options.on");
		} else {
			buttonBonusItems.displayString = buttonBonusItems.displayString
					+ var1.translateKey("options.off");
		}

		buttonWorldType.displayString = var1
				.translateKey("selectWorld.mapType")
				+ " "
				+ var1.translateKey(WorldType.worldTypes[worldTypeId]
						.getTranslateName());
		buttonAllowCommands.displayString = var1
				.translateKey("selectWorld.allowCommands") + " ";

		if (commandsAllowed && !isHardcore) {
			buttonAllowCommands.displayString = buttonAllowCommands.displayString
					+ var1.translateKey("options.on");
		} else {
			buttonAllowCommands.displayString = buttonAllowCommands.displayString
					+ var1.translateKey("options.off");
		}
	}

	public static String func_73913_a(final ISaveFormat par0ISaveFormat,
			String par1Str) {
		par1Str = par1Str.replaceAll("[\\./\"]", "_");
		final String[] var2 = GuiCreateWorld.ILLEGAL_WORLD_NAMES;
		final int var3 = var2.length;

		for (int var4 = 0; var4 < var3; ++var4) {
			final String var5 = var2[var4];

			if (par1Str.equalsIgnoreCase(var5)) {
				par1Str = "_" + par1Str + "_";
			}
		}

		while (par0ISaveFormat.getWorldInfo(par1Str) != null) {
			par1Str = par1Str + "-";
		}

		return par1Str;
	}

	/**
	 * Called when the screen is unloaded. Used to disable keyboard repeat
	 * events
	 */
	@Override
	public void onGuiClosed() {
		Keyboard.enableRepeatEvents(false);
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	@Override
	protected void actionPerformed(final GuiButton par1GuiButton) {
		if (par1GuiButton.enabled) {
			if (par1GuiButton.id == 1) {
				mc.displayGuiScreen(parentGuiScreen);
			} else if (par1GuiButton.id == 0) {
				mc.displayGuiScreen((GuiScreen) null);

				if (createClicked) {
					return;
				}

				createClicked = true;
				long var2 = new Random().nextLong();
				final String var4 = textboxSeed.getText();

				if (!MathHelper.stringNullOrLengthZero(var4)) {
					try {
						final long var5 = Long.parseLong(var4);

						if (var5 != 0L) {
							var2 = var5;
						}
					} catch (final NumberFormatException var7) {
						var2 = var4.hashCode();
					}
				}

				final EnumGameType var8 = EnumGameType.getByName(gameMode);
				final WorldSettings var6 = new WorldSettings(var2, var8,
						generateStructures, isHardcore,
						WorldType.worldTypes[worldTypeId]);
				var6.func_82750_a(generatorOptionsToUse);

				if (bonusItems && !isHardcore) {
					var6.enableBonusChest();
				}

				if (commandsAllowed && !isHardcore) {
					var6.enableCommands();
				}

				mc.launchIntegratedServer(folderName, textboxWorldName
						.getText().trim(), var6);
			} else if (par1GuiButton.id == 3) {
				func_82287_i();
			} else if (par1GuiButton.id == 2) {
				if (gameMode.equals("survival")) {
					if (!commandsToggled) {
						commandsAllowed = false;
					}

					isHardcore = false;
					gameMode = "hardcore";
					isHardcore = true;
					buttonAllowCommands.enabled = false;
					buttonBonusItems.enabled = false;
					updateButtonText();
				} else if (gameMode.equals("hardcore")) {
					if (!commandsToggled) {
						commandsAllowed = true;
					}

					isHardcore = false;
					gameMode = "creative";
					updateButtonText();
					isHardcore = false;
					buttonAllowCommands.enabled = true;
					buttonBonusItems.enabled = true;
				} else {
					if (!commandsToggled) {
						commandsAllowed = false;
					}

					gameMode = "survival";
					updateButtonText();
					buttonAllowCommands.enabled = true;
					buttonBonusItems.enabled = true;
					isHardcore = false;
				}

				updateButtonText();
			} else if (par1GuiButton.id == 4) {
				generateStructures = !generateStructures;
				updateButtonText();
			} else if (par1GuiButton.id == 7) {
				bonusItems = !bonusItems;
				updateButtonText();
			} else if (par1GuiButton.id == 5) {
				++worldTypeId;

				if (worldTypeId >= WorldType.worldTypes.length) {
					worldTypeId = 0;
				}

				while (WorldType.worldTypes[worldTypeId] == null
						|| !WorldType.worldTypes[worldTypeId].getCanBeCreated()) {
					++worldTypeId;

					if (worldTypeId >= WorldType.worldTypes.length) {
						worldTypeId = 0;
					}
				}

				generatorOptionsToUse = "";
				updateButtonText();
				func_82288_a(moreOptions);
			} else if (par1GuiButton.id == 6) {
				commandsToggled = true;
				commandsAllowed = !commandsAllowed;
				updateButtonText();
			} else if (par1GuiButton.id == 8) {
				mc.displayGuiScreen(new GuiCreateFlatWorld(this,
						generatorOptionsToUse));
			}
		}
	}

	private void func_82287_i() {
		func_82288_a(!moreOptions);
	}

	private void func_82288_a(final boolean par1) {
		moreOptions = par1;
		buttonGameMode.drawButton = !moreOptions;
		buttonGenerateStructures.drawButton = moreOptions;
		buttonBonusItems.drawButton = moreOptions;
		buttonWorldType.drawButton = moreOptions;
		buttonAllowCommands.drawButton = moreOptions;
		buttonCustomize.drawButton = moreOptions
				&& WorldType.worldTypes[worldTypeId] == WorldType.FLAT;
		StringTranslate var2;

		if (moreOptions) {
			var2 = StringTranslate.getInstance();
			moreWorldOptions.displayString = var2.translateKey("gui.done");
		} else {
			var2 = StringTranslate.getInstance();
			moreWorldOptions.displayString = var2
					.translateKey("selectWorld.moreWorldOptions");
		}
	}

	/**
	 * Fired when a key is typed. This is the equivalent of
	 * KeyListener.keyTyped(KeyEvent e).
	 */
	@Override
	protected void keyTyped(final char par1, final int par2) {
		if (textboxWorldName.isFocused() && !moreOptions) {
			textboxWorldName.textboxKeyTyped(par1, par2);
			localizedNewWorldText = textboxWorldName.getText();
		} else if (textboxSeed.isFocused() && moreOptions) {
			textboxSeed.textboxKeyTyped(par1, par2);
			seed = textboxSeed.getText();
		}

		if (par1 == 13) {
			actionPerformed((GuiButton) buttonList.get(0));
		}

		((GuiButton) buttonList.get(0)).enabled = textboxWorldName.getText()
				.length() > 0;
		makeUseableName();
	}

	/**
	 * Called when the mouse is clicked.
	 */
	@Override
	protected void mouseClicked(final int par1, final int par2, final int par3) {
		super.mouseClicked(par1, par2, par3);

		if (moreOptions) {
			textboxSeed.mouseClicked(par1, par2, par3);
		} else {
			textboxWorldName.mouseClicked(par1, par2, par3);
		}
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(final int par1, final int par2, final float par3) {
		final StringTranslate var4 = StringTranslate.getInstance();
		drawDefaultBackground();
		drawCenteredString(fontRenderer,
				var4.translateKey("selectWorld.create"), width / 2, 20,
				16777215);

		if (moreOptions) {
			drawString(fontRenderer,
					var4.translateKey("selectWorld.enterSeed"),
					width / 2 - 100, 47, 10526880);
			drawString(fontRenderer, var4.translateKey("selectWorld.seedInfo"),
					width / 2 - 100, 85, 10526880);
			drawString(fontRenderer,
					var4.translateKey("selectWorld.mapFeatures.info"),
					width / 2 - 150, 122, 10526880);
			drawString(fontRenderer,
					var4.translateKey("selectWorld.allowCommands.info"),
					width / 2 - 150, 172, 10526880);
			textboxSeed.drawTextBox();
		} else {
			drawString(fontRenderer,
					var4.translateKey("selectWorld.enterName"),
					width / 2 - 100, 47, 10526880);
			drawString(fontRenderer,
					var4.translateKey("selectWorld.resultFolder") + " "
							+ folderName, width / 2 - 100, 85, 10526880);
			textboxWorldName.drawTextBox();
			drawString(fontRenderer, gameModeDescriptionLine1, width / 2 - 100,
					137, 10526880);
			drawString(fontRenderer, gameModeDescriptionLine2, width / 2 - 100,
					149, 10526880);
		}

		super.drawScreen(par1, par2, par3);
	}

	public void func_82286_a(final WorldInfo par1WorldInfo) {
		localizedNewWorldText = StatCollector.translateToLocalFormatted(
				"selectWorld.newWorld.copyOf",
				new Object[] { par1WorldInfo.getWorldName() });
		seed = par1WorldInfo.getSeed() + "";
		worldTypeId = par1WorldInfo.getTerrainType().getWorldTypeID();
		generatorOptionsToUse = par1WorldInfo.getGeneratorOptions();
		generateStructures = par1WorldInfo.isMapFeaturesEnabled();
		commandsAllowed = par1WorldInfo.areCommandsAllowed();

		if (par1WorldInfo.isHardcoreModeEnabled()) {
			gameMode = "hardcore";
		} else if (par1WorldInfo.getGameType().isSurvivalOrAdventure()) {
			gameMode = "survival";
		} else if (par1WorldInfo.getGameType().isCreative()) {
			gameMode = "creative";
		}
	}
}
