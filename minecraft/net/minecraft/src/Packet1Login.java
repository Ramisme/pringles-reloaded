package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet1Login extends Packet {
	/** The player's entity ID */
	public int clientEntityId = 0;
	public WorldType terrainType;
	public boolean hardcoreMode;
	public EnumGameType gameType;

	/** -1: The Nether, 0: The Overworld, 1: The End */
	public int dimension;

	/** The difficulty setting byte. */
	public byte difficultySetting;

	/** Defaults to 128 */
	public byte worldHeight;

	/** The maximum players. */
	public byte maxPlayers;

	public Packet1Login() {
	}

	public Packet1Login(final int par1, final WorldType par2WorldType,
			final EnumGameType par3EnumGameType, final boolean par4,
			final int par5, final int par6, final int par7, final int par8) {
		clientEntityId = par1;
		terrainType = par2WorldType;
		dimension = par5;
		difficultySetting = (byte) par6;
		gameType = par3EnumGameType;
		worldHeight = (byte) par7;
		maxPlayers = (byte) par8;
		hardcoreMode = par4;
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		clientEntityId = par1DataInputStream.readInt();
		final String var2 = Packet.readString(par1DataInputStream, 16);
		terrainType = WorldType.parseWorldType(var2);

		if (terrainType == null) {
			terrainType = WorldType.DEFAULT;
		}

		final byte var3 = par1DataInputStream.readByte();
		hardcoreMode = (var3 & 8) == 8;
		final int var4 = var3 & -9;
		gameType = EnumGameType.getByID(var4);
		dimension = par1DataInputStream.readByte();
		difficultySetting = par1DataInputStream.readByte();
		worldHeight = par1DataInputStream.readByte();
		maxPlayers = par1DataInputStream.readByte();
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		par1DataOutputStream.writeInt(clientEntityId);
		Packet.writeString(
				terrainType == null ? "" : terrainType.getWorldTypeName(),
				par1DataOutputStream);
		int var2 = gameType.getID();

		if (hardcoreMode) {
			var2 |= 8;
		}

		par1DataOutputStream.writeByte(var2);
		par1DataOutputStream.writeByte(dimension);
		par1DataOutputStream.writeByte(difficultySetting);
		par1DataOutputStream.writeByte(worldHeight);
		par1DataOutputStream.writeByte(maxPlayers);
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleLogin(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		int var1 = 0;

		if (terrainType != null) {
			var1 = terrainType.getWorldTypeName().length();
		}

		return 6 + 2 * var1 + 4 + 4 + 1 + 1 + 1;
	}
}
