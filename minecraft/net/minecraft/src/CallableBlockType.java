package net.minecraft.src;

import java.util.concurrent.Callable;

final class CallableBlockType implements Callable {
	final int blockID;

	CallableBlockType(final int par1) {
		blockID = par1;
	}

	public String callBlockType() {
		try {
			return String.format("ID #%d (%s // %s)",
					new Object[] {
							Integer.valueOf(blockID),
							Block.blocksList[blockID].getUnlocalizedName(),
							Block.blocksList[blockID].getClass()
									.getCanonicalName() });
		} catch (final Throwable var2) {
			return "ID #" + blockID;
		}
	}

	@Override
	public Object call() {
		return callBlockType();
	}
}
