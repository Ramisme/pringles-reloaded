package net.minecraft.src;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public final class SpawnerAnimals {
	/** The 17x17 area around the player where mobs can spawn */
	private static HashMap eligibleChunksForSpawning = new HashMap();

	/** An array of entity classes that spawn at night. */
	protected static final Class[] nightSpawnEntities = new Class[] {
			EntitySpider.class, EntityZombie.class, EntitySkeleton.class };

	/**
	 * Given a chunk, find a random position in it.
	 */
	protected static ChunkPosition getRandomSpawningPointInChunk(
			final World par0World, final int par1, final int par2) {
		final Chunk var3 = par0World.getChunkFromChunkCoords(par1, par2);
		final int var4 = par1 * 16 + par0World.rand.nextInt(16);
		final int var5 = par2 * 16 + par0World.rand.nextInt(16);
		final int var6 = par0World.rand.nextInt(var3 == null ? par0World
				.getActualHeight() : var3.getTopFilledSegment() + 16 - 1);
		return new ChunkPosition(var4, var6, var5);
	}

	/**
	 * adds all chunks within the spawn radius of the players to
	 * eligibleChunksForSpawning. pars: the world, hostileCreatures,
	 * passiveCreatures. returns number of eligible chunks.
	 */
	public static final int findChunksForSpawning(
			final WorldServer par0WorldServer, final boolean par1,
			final boolean par2, final boolean par3) {
		if (!par1 && !par2) {
			return 0;
		} else {
			SpawnerAnimals.eligibleChunksForSpawning.clear();
			int var4;
			int var7;

			for (var4 = 0; var4 < par0WorldServer.playerEntities.size(); ++var4) {
				final EntityPlayer var5 = (EntityPlayer) par0WorldServer.playerEntities
						.get(var4);
				final int var6 = MathHelper.floor_double(var5.posX / 16.0D);
				var7 = MathHelper.floor_double(var5.posZ / 16.0D);
				final byte var8 = 8;

				for (int var9 = -var8; var9 <= var8; ++var9) {
					for (int var10 = -var8; var10 <= var8; ++var10) {
						final boolean var11 = var9 == -var8 || var9 == var8
								|| var10 == -var8 || var10 == var8;
						final ChunkCoordIntPair var12 = new ChunkCoordIntPair(
								var9 + var6, var10 + var7);

						if (!var11) {
							SpawnerAnimals.eligibleChunksForSpawning.put(var12,
									Boolean.valueOf(false));
						} else if (!SpawnerAnimals.eligibleChunksForSpawning
								.containsKey(var12)) {
							SpawnerAnimals.eligibleChunksForSpawning.put(var12,
									Boolean.valueOf(true));
						}
					}
				}
			}

			var4 = 0;
			final ChunkCoordinates var32 = par0WorldServer.getSpawnPoint();
			final EnumCreatureType[] var33 = EnumCreatureType.values();
			var7 = var33.length;

			for (int var34 = 0; var34 < var7; ++var34) {
				final EnumCreatureType var35 = var33[var34];

				if ((!var35.getPeacefulCreature() || par2)
						&& (var35.getPeacefulCreature() || par1)
						&& (!var35.getAnimal() || par3)
						&& par0WorldServer.countEntities(var35
								.getCreatureClass()) <= var35
								.getMaxNumberOfCreature()
								* SpawnerAnimals.eligibleChunksForSpawning
										.size() / 256) {
					final Iterator var37 = SpawnerAnimals.eligibleChunksForSpawning
							.keySet().iterator();
					label110:

					while (var37.hasNext()) {
						final ChunkCoordIntPair var36 = (ChunkCoordIntPair) var37
								.next();

						if (!((Boolean) SpawnerAnimals.eligibleChunksForSpawning
								.get(var36)).booleanValue()) {
							final ChunkPosition var38 = SpawnerAnimals
									.getRandomSpawningPointInChunk(
											par0WorldServer, var36.chunkXPos,
											var36.chunkZPos);
							final int var13 = var38.x;
							final int var14 = var38.y;
							final int var15 = var38.z;

							if (!par0WorldServer.isBlockNormalCube(var13,
									var14, var15)
									&& par0WorldServer.getBlockMaterial(var13,
											var14, var15) == var35
											.getCreatureMaterial()) {
								int var16 = 0;
								int var17 = 0;

								while (var17 < 3) {
									int var18 = var13;
									int var19 = var14;
									int var20 = var15;
									final byte var21 = 6;
									SpawnListEntry var22 = null;
									int var23 = 0;

									while (true) {
										if (var23 < 4) {
											label103: {
												var18 += par0WorldServer.rand
														.nextInt(var21)
														- par0WorldServer.rand
																.nextInt(var21);
												var19 += par0WorldServer.rand
														.nextInt(1)
														- par0WorldServer.rand
																.nextInt(1);
												var20 += par0WorldServer.rand
														.nextInt(var21)
														- par0WorldServer.rand
																.nextInt(var21);

												if (SpawnerAnimals
														.canCreatureTypeSpawnAtLocation(
																var35,
																par0WorldServer,
																var18, var19,
																var20)) {
													final float var24 = var18 + 0.5F;
													final float var25 = var19;
													final float var26 = var20 + 0.5F;

													if (par0WorldServer
															.getClosestPlayer(
																	var24,
																	var25,
																	var26,
																	24.0D) == null) {
														final float var27 = var24
																- var32.posX;
														final float var28 = var25
																- var32.posY;
														final float var29 = var26
																- var32.posZ;
														final float var30 = var27
																* var27
																+ var28
																* var28
																+ var29
																* var29;

														if (var30 >= 576.0F) {
															if (var22 == null) {
																var22 = par0WorldServer
																		.spawnRandomCreature(
																				var35,
																				var18,
																				var19,
																				var20);

																if (var22 == null) {
																	break label103;
																}
															}

															EntityLiving var39;

															try {
																var39 = (EntityLiving) var22.entityClass
																		.getConstructor(
																				new Class[] { World.class })
																		.newInstance(
																				new Object[] { par0WorldServer });
															} catch (final Exception var31) {
																var31.printStackTrace();
																return var4;
															}

															var39.setLocationAndAngles(
																	var24,
																	var25,
																	var26,
																	par0WorldServer.rand
																			.nextFloat() * 360.0F,
																	0.0F);

															if (var39
																	.getCanSpawnHere()) {
																++var16;
																par0WorldServer
																		.spawnEntityInWorld(var39);
																SpawnerAnimals
																		.creatureSpecificInit(
																				var39,
																				par0WorldServer,
																				var24,
																				var25,
																				var26);

																if (var16 >= var39
																		.getMaxSpawnedInChunk()) {
																	continue label110;
																}
															}

															var4 += var16;
														}
													}
												}

												++var23;
												continue;
											}
										}

										++var17;
										break;
									}
								}
							}
						}
					}
				}
			}

			return var4;
		}
	}

	/**
	 * Returns whether or not the specified creature type can spawn at the
	 * specified location.
	 */
	public static boolean canCreatureTypeSpawnAtLocation(
			final EnumCreatureType par0EnumCreatureType, final World par1World,
			final int par2, final int par3, final int par4) {
		if (par0EnumCreatureType.getCreatureMaterial() == Material.water) {
			return par1World.getBlockMaterial(par2, par3, par4).isLiquid()
					&& par1World.getBlockMaterial(par2, par3 - 1, par4)
							.isLiquid()
					&& !par1World.isBlockNormalCube(par2, par3 + 1, par4);
		} else if (!par1World
				.doesBlockHaveSolidTopSurface(par2, par3 - 1, par4)) {
			return false;
		} else {
			final int var5 = par1World.getBlockId(par2, par3 - 1, par4);
			return var5 != Block.bedrock.blockID
					&& !par1World.isBlockNormalCube(par2, par3, par4)
					&& !par1World.getBlockMaterial(par2, par3, par4).isLiquid()
					&& !par1World.isBlockNormalCube(par2, par3 + 1, par4);
		}
	}

	/**
	 * determines if a skeleton spawns on a spider, and if a sheep is a
	 * different color
	 */
	private static void creatureSpecificInit(
			final EntityLiving par0EntityLiving, final World par1World,
			final float par2, final float par3, final float par4) {
		par0EntityLiving.initCreature();
	}

	/**
	 * Called during chunk generation to spawn initial creatures.
	 */
	public static void performWorldGenSpawning(final World par0World,
			final BiomeGenBase par1BiomeGenBase, final int par2,
			final int par3, final int par4, final int par5,
			final Random par6Random) {
		final List var7 = par1BiomeGenBase
				.getSpawnableList(EnumCreatureType.creature);

		if (!var7.isEmpty()) {
			while (par6Random.nextFloat() < par1BiomeGenBase
					.getSpawningChance()) {
				final SpawnListEntry var8 = (SpawnListEntry) WeightedRandom
						.getRandomItem(par0World.rand, var7);
				final int var9 = var8.minGroupCount
						+ par6Random.nextInt(1 + var8.maxGroupCount
								- var8.minGroupCount);
				int var10 = par2 + par6Random.nextInt(par4);
				int var11 = par3 + par6Random.nextInt(par5);
				final int var12 = var10;
				final int var13 = var11;

				for (int var14 = 0; var14 < var9; ++var14) {
					boolean var15 = false;

					for (int var16 = 0; !var15 && var16 < 4; ++var16) {
						final int var17 = par0World.getTopSolidOrLiquidBlock(
								var10, var11);

						if (SpawnerAnimals.canCreatureTypeSpawnAtLocation(
								EnumCreatureType.creature, par0World, var10,
								var17, var11)) {
							final float var18 = var10 + 0.5F;
							final float var19 = var17;
							final float var20 = var11 + 0.5F;
							EntityLiving var21;

							try {
								var21 = (EntityLiving) var8.entityClass
										.getConstructor(
												new Class[] { World.class })
										.newInstance(new Object[] { par0World });
							} catch (final Exception var23) {
								var23.printStackTrace();
								continue;
							}

							var21.setLocationAndAngles(var18, var19, var20,
									par6Random.nextFloat() * 360.0F, 0.0F);
							par0World.spawnEntityInWorld(var21);
							SpawnerAnimals.creatureSpecificInit(var21,
									par0World, var18, var19, var20);
							var15 = true;
						}

						var10 += par6Random.nextInt(5) - par6Random.nextInt(5);

						for (var11 += par6Random.nextInt(5)
								- par6Random.nextInt(5); var10 < par2
								|| var10 >= par2 + par4 || var11 < par3
								|| var11 >= par3 + par4; var11 = var13
								+ par6Random.nextInt(5) - par6Random.nextInt(5)) {
							var10 = var12 + par6Random.nextInt(5)
									- par6Random.nextInt(5);
						}
					}
				}
			}
		}
	}
}
