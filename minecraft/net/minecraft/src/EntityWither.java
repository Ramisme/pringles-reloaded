package net.minecraft.src;

import java.util.List;

public class EntityWither extends EntityMob implements IBossDisplayData,
		IRangedAttackMob {
	private final float[] field_82220_d = new float[2];
	private final float[] field_82221_e = new float[2];
	private final float[] field_82217_f = new float[2];
	private final float[] field_82218_g = new float[2];
	private final int[] field_82223_h = new int[2];
	private final int[] field_82224_i = new int[2];
	private int field_82222_j;

	/** Selector used to determine the entities a wither boss should attack. */
	private static final IEntitySelector attackEntitySelector = new EntityWitherAttackFilter();

	public EntityWither(final World par1World) {
		super(par1World);
		setEntityHealth(getMaxHealth());
		texture = "/mob/wither.png";
		setSize(0.9F, 4.0F);
		isImmuneToFire = true;
		moveSpeed = 0.6F;
		getNavigator().setCanSwim(true);
		tasks.addTask(0, new EntityAISwimming(this));
		tasks.addTask(2, new EntityAIArrowAttack(this, moveSpeed, 40, 20.0F));
		tasks.addTask(5, new EntityAIWander(this, moveSpeed));
		tasks.addTask(6, new EntityAIWatchClosest(this, EntityPlayer.class,
				8.0F));
		tasks.addTask(7, new EntityAILookIdle(this));
		targetTasks.addTask(1, new EntityAIHurtByTarget(this, false));
		targetTasks.addTask(2, new EntityAINearestAttackableTarget(this,
				EntityLiving.class, 30.0F, 0, false, false,
				EntityWither.attackEntitySelector));
		experienceValue = 50;
	}

	@Override
	protected void entityInit() {
		super.entityInit();
		dataWatcher.addObject(16, new Integer(100));
		dataWatcher.addObject(17, new Integer(0));
		dataWatcher.addObject(18, new Integer(0));
		dataWatcher.addObject(19, new Integer(0));
		dataWatcher.addObject(20, new Integer(0));
	}

	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	@Override
	public void writeEntityToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeEntityToNBT(par1NBTTagCompound);
		par1NBTTagCompound.setInteger("Invul", func_82212_n());
	}

	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	@Override
	public void readEntityFromNBT(final NBTTagCompound par1NBTTagCompound) {
		super.readEntityFromNBT(par1NBTTagCompound);
		func_82215_s(par1NBTTagCompound.getInteger("Invul"));
		dataWatcher.updateObject(16, Integer.valueOf(health));
	}

	@Override
	public float getShadowSize() {
		return height / 8.0F;
	}

	/**
	 * Returns the sound this mob makes while it's alive.
	 */
	@Override
	protected String getLivingSound() {
		return "mob.wither.idle";
	}

	/**
	 * Returns the sound this mob makes when it is hurt.
	 */
	@Override
	protected String getHurtSound() {
		return "mob.wither.hurt";
	}

	/**
	 * Returns the sound this mob makes on death.
	 */
	@Override
	protected String getDeathSound() {
		return "mob.wither.death";
	}

	/**
	 * Returns the texture's file path as a String.
	 */
	@Override
	public String getTexture() {
		final int var1 = func_82212_n();
		return var1 > 0 && (var1 > 80 || var1 / 5 % 2 != 1) ? "/mob/wither_invul.png"
				: "/mob/wither.png";
	}

	/**
	 * Called frequently so the entity can update its state every tick as
	 * required. For example, zombies and skeletons use this to react to
	 * sunlight and start to burn.
	 */
	@Override
	public void onLivingUpdate() {
		if (!worldObj.isRemote) {
			dataWatcher.updateObject(16, Integer.valueOf(health));
		}

		motionY *= 0.6000000238418579D;
		double var4;
		double var6;
		double var8;

		if (!worldObj.isRemote && getWatchedTargetId(0) > 0) {
			final Entity var1 = worldObj.getEntityByID(getWatchedTargetId(0));

			if (var1 != null) {
				if (posY < var1.posY || !isArmored() && posY < var1.posY + 5.0D) {
					if (motionY < 0.0D) {
						motionY = 0.0D;
					}

					motionY += (0.5D - motionY) * 0.6000000238418579D;
				}

				final double var2 = var1.posX - posX;
				var4 = var1.posZ - posZ;
				var6 = var2 * var2 + var4 * var4;

				if (var6 > 9.0D) {
					var8 = MathHelper.sqrt_double(var6);
					motionX += (var2 / var8 * 0.5D - motionX) * 0.6000000238418579D;
					motionZ += (var4 / var8 * 0.5D - motionZ) * 0.6000000238418579D;
				}
			}
		}

		if (motionX * motionX + motionZ * motionZ > 0.05000000074505806D) {
			rotationYaw = (float) Math.atan2(motionZ, motionX)
					* (180F / (float) Math.PI) - 90.0F;
		}

		super.onLivingUpdate();
		int var20;

		for (var20 = 0; var20 < 2; ++var20) {
			field_82218_g[var20] = field_82221_e[var20];
			field_82217_f[var20] = field_82220_d[var20];
		}

		int var21;

		for (var20 = 0; var20 < 2; ++var20) {
			var21 = getWatchedTargetId(var20 + 1);
			Entity var3 = null;

			if (var21 > 0) {
				var3 = worldObj.getEntityByID(var21);
			}

			if (var3 != null) {
				var4 = func_82214_u(var20 + 1);
				var6 = func_82208_v(var20 + 1);
				var8 = func_82213_w(var20 + 1);
				final double var10 = var3.posX - var4;
				final double var12 = var3.posY + var3.getEyeHeight() - var6;
				final double var14 = var3.posZ - var8;
				final double var16 = MathHelper.sqrt_double(var10 * var10
						+ var14 * var14);
				final float var18 = (float) (Math.atan2(var14, var10) * 180.0D / Math.PI) - 90.0F;
				final float var19 = (float) -(Math.atan2(var12, var16) * 180.0D / Math.PI);
				field_82220_d[var20] = func_82204_b(field_82220_d[var20],
						var19, 40.0F);
				field_82221_e[var20] = func_82204_b(field_82221_e[var20],
						var18, 10.0F);
			} else {
				field_82221_e[var20] = func_82204_b(field_82221_e[var20],
						renderYawOffset, 10.0F);
			}
		}

		final boolean var22 = isArmored();

		for (var21 = 0; var21 < 3; ++var21) {
			final double var23 = func_82214_u(var21);
			final double var5 = func_82208_v(var21);
			final double var7 = func_82213_w(var21);
			worldObj.spawnParticle("smoke", var23 + rand.nextGaussian()
					* 0.30000001192092896D, var5 + rand.nextGaussian()
					* 0.30000001192092896D, var7 + rand.nextGaussian()
					* 0.30000001192092896D, 0.0D, 0.0D, 0.0D);

			if (var22 && worldObj.rand.nextInt(4) == 0) {
				worldObj.spawnParticle("mobSpell", var23 + rand.nextGaussian()
						* 0.30000001192092896D, var5 + rand.nextGaussian()
						* 0.30000001192092896D, var7 + rand.nextGaussian()
						* 0.30000001192092896D, 0.699999988079071D,
						0.699999988079071D, 0.5D);
			}
		}

		if (func_82212_n() > 0) {
			for (var21 = 0; var21 < 3; ++var21) {
				worldObj.spawnParticle("mobSpell", posX + rand.nextGaussian()
						* 1.0D, posY + rand.nextFloat() * 3.3F,
						posZ + rand.nextGaussian() * 1.0D, 0.699999988079071D,
						0.699999988079071D, 0.8999999761581421D);
			}
		}
	}

	@Override
	protected void updateAITasks() {
		int var1;

		if (func_82212_n() > 0) {
			var1 = func_82212_n() - 1;

			if (var1 <= 0) {
				worldObj.newExplosion(this, posX, posY + getEyeHeight(), posZ,
						7.0F, false, worldObj.getGameRules()
								.getGameRuleBooleanValue("mobGriefing"));
				worldObj.func_82739_e(1013, (int) posX, (int) posY, (int) posZ,
						0);
			}

			func_82215_s(var1);

			if (ticksExisted % 10 == 0) {
				heal(10);
			}
		} else {
			super.updateAITasks();
			int var12;

			for (var1 = 1; var1 < 3; ++var1) {
				if (ticksExisted >= field_82223_h[var1 - 1]) {
					field_82223_h[var1 - 1] = ticksExisted + 10
							+ rand.nextInt(10);

					if (worldObj.difficultySetting >= 2) {
						final int var10001 = var1 - 1;
						final int var10003 = field_82224_i[var1 - 1];
						field_82224_i[var10001] = field_82224_i[var1 - 1] + 1;

						if (var10003 > 15) {
							final float var2 = 10.0F;
							final float var3 = 5.0F;
							final double var4 = MathHelper
									.getRandomDoubleInRange(rand, posX - var2,
											posX + var2);
							final double var6 = MathHelper
									.getRandomDoubleInRange(rand, posY - var3,
											posY + var3);
							final double var8 = MathHelper
									.getRandomDoubleInRange(rand, posZ - var2,
											posZ + var2);
							func_82209_a(var1 + 1, var4, var6, var8, true);
							field_82224_i[var1 - 1] = 0;
						}
					}

					var12 = getWatchedTargetId(var1);

					if (var12 > 0) {
						final Entity var14 = worldObj.getEntityByID(var12);

						if (var14 != null && var14.isEntityAlive()
								&& getDistanceSqToEntity(var14) <= 900.0D
								&& canEntityBeSeen(var14)) {
							func_82216_a(var1 + 1, (EntityLiving) var14);
							field_82223_h[var1 - 1] = ticksExisted + 40
									+ rand.nextInt(20);
							field_82224_i[var1 - 1] = 0;
						} else {
							func_82211_c(var1, 0);
						}
					} else {
						final List var13 = worldObj.selectEntitiesWithinAABB(
								EntityLiving.class,
								boundingBox.expand(20.0D, 8.0D, 20.0D),
								EntityWither.attackEntitySelector);

						for (int var16 = 0; var16 < 10 && !var13.isEmpty(); ++var16) {
							final EntityLiving var5 = (EntityLiving) var13
									.get(rand.nextInt(var13.size()));

							if (var5 != this && var5.isEntityAlive()
									&& canEntityBeSeen(var5)) {
								if (var5 instanceof EntityPlayer) {
									if (!((EntityPlayer) var5).capabilities.disableDamage) {
										func_82211_c(var1, var5.entityId);
									}
								} else {
									func_82211_c(var1, var5.entityId);
								}

								break;
							}

							var13.remove(var5);
						}
					}
				}
			}

			if (getAttackTarget() != null) {
				func_82211_c(0, getAttackTarget().entityId);
			} else {
				func_82211_c(0, 0);
			}

			if (field_82222_j > 0) {
				--field_82222_j;

				if (field_82222_j == 0
						&& worldObj.getGameRules().getGameRuleBooleanValue(
								"mobGriefing")) {
					var1 = MathHelper.floor_double(posY);
					var12 = MathHelper.floor_double(posX);
					final int var15 = MathHelper.floor_double(posZ);
					boolean var18 = false;

					for (int var17 = -1; var17 <= 1; ++var17) {
						for (int var19 = -1; var19 <= 1; ++var19) {
							for (int var7 = 0; var7 <= 3; ++var7) {
								final int var20 = var12 + var17;
								final int var9 = var1 + var7;
								final int var10 = var15 + var19;
								final int var11 = worldObj.getBlockId(var20,
										var9, var10);

								if (var11 > 0
										&& var11 != Block.bedrock.blockID
										&& var11 != Block.endPortal.blockID
										&& var11 != Block.endPortalFrame.blockID) {
									var18 = worldObj.destroyBlock(var20, var9,
											var10, true) || var18;
								}
							}
						}
					}

					if (var18) {
						worldObj.playAuxSFXAtEntity((EntityPlayer) null, 1012,
								(int) posX, (int) posY, (int) posZ, 0);
					}
				}
			}

			if (ticksExisted % 20 == 0) {
				heal(1);
			}
		}
	}

	public void func_82206_m() {
		func_82215_s(220);
		setEntityHealth(getMaxHealth() / 3);
	}

	/**
	 * Sets the Entity inside a web block.
	 */
	@Override
	public void setInWeb() {
	}

	/**
	 * Returns the current armor value as determined by a call to
	 * InventoryPlayer.getTotalArmorValue
	 */
	@Override
	public int getTotalArmorValue() {
		return 4;
	}

	private double func_82214_u(final int par1) {
		if (par1 <= 0) {
			return posX;
		} else {
			final float var2 = (renderYawOffset + 180 * (par1 - 1)) / 180.0F
					* (float) Math.PI;
			final float var3 = MathHelper.cos(var2);
			return posX + var3 * 1.3D;
		}
	}

	private double func_82208_v(final int par1) {
		return par1 <= 0 ? posY + 3.0D : posY + 2.2D;
	}

	private double func_82213_w(final int par1) {
		if (par1 <= 0) {
			return posZ;
		} else {
			final float var2 = (renderYawOffset + 180 * (par1 - 1)) / 180.0F
					* (float) Math.PI;
			final float var3 = MathHelper.sin(var2);
			return posZ + var3 * 1.3D;
		}
	}

	private float func_82204_b(final float par1, final float par2,
			final float par3) {
		float var4 = MathHelper.wrapAngleTo180_float(par2 - par1);

		if (var4 > par3) {
			var4 = par3;
		}

		if (var4 < -par3) {
			var4 = -par3;
		}

		return par1 + var4;
	}

	private void func_82216_a(final int par1,
			final EntityLiving par2EntityLiving) {
		func_82209_a(par1, par2EntityLiving.posX, par2EntityLiving.posY
				+ par2EntityLiving.getEyeHeight() * 0.5D,
				par2EntityLiving.posZ, par1 == 0 && rand.nextFloat() < 0.001F);
	}

	private void func_82209_a(final int par1, final double par2,
			final double par4, final double par6, final boolean par8) {
		worldObj.playAuxSFXAtEntity((EntityPlayer) null, 1014, (int) posX,
				(int) posY, (int) posZ, 0);
		final double var9 = func_82214_u(par1);
		final double var11 = func_82208_v(par1);
		final double var13 = func_82213_w(par1);
		final double var15 = par2 - var9;
		final double var17 = par4 - var11;
		final double var19 = par6 - var13;
		final EntityWitherSkull var21 = new EntityWitherSkull(worldObj, this,
				var15, var17, var19);

		if (par8) {
			var21.setInvulnerable(true);
		}

		var21.posY = var11;
		var21.posX = var9;
		var21.posZ = var13;
		worldObj.spawnEntityInWorld(var21);
	}

	/**
	 * Attack the specified entity using a ranged attack.
	 */
	@Override
	public void attackEntityWithRangedAttack(
			final EntityLiving par1EntityLiving, final float par2) {
		func_82216_a(0, par1EntityLiving);
	}

	/**
	 * Called when the entity is attacked.
	 */
	@Override
	public boolean attackEntityFrom(final DamageSource par1DamageSource,
			final int par2) {
		if (isEntityInvulnerable()) {
			return false;
		} else if (par1DamageSource == DamageSource.drown) {
			return false;
		} else if (func_82212_n() > 0) {
			return false;
		} else {
			Entity var3;

			if (isArmored()) {
				var3 = par1DamageSource.getSourceOfDamage();

				if (var3 instanceof EntityArrow) {
					return false;
				}
			}

			var3 = par1DamageSource.getEntity();

			if (var3 != null
					&& !(var3 instanceof EntityPlayer)
					&& var3 instanceof EntityLiving
					&& ((EntityLiving) var3).getCreatureAttribute() == getCreatureAttribute()) {
				return false;
			} else {
				if (field_82222_j <= 0) {
					field_82222_j = 20;
				}

				for (int var4 = 0; var4 < field_82224_i.length; ++var4) {
					field_82224_i[var4] += 3;
				}

				return super.attackEntityFrom(par1DamageSource, par2);
			}
		}
	}

	/**
	 * Drop 0-2 items of this living's type. @param par1 - Whether this entity
	 * has recently been hit by a player. @param par2 - Level of Looting used to
	 * kill this mob.
	 */
	@Override
	protected void dropFewItems(final boolean par1, final int par2) {
		dropItem(Item.netherStar.itemID, 1);
	}

	/**
	 * Makes the entity despawn if requirements are reached
	 */
	@Override
	protected void despawnEntity() {
		entityAge = 0;
	}

	@Override
	public int getBrightnessForRender(final float par1) {
		return 15728880;
	}

	/**
	 * Returns true if other Entities should be prevented from moving through
	 * this Entity.
	 */
	@Override
	public boolean canBeCollidedWith() {
		return !isDead;
	}

	/**
	 * Returns the health points of the dragon.
	 */
	@Override
	public int getBossHealth() {
		return dataWatcher.getWatchableObjectInt(16);
	}

	/**
	 * Called when the mob is falling. Calculates and applies fall damage.
	 */
	@Override
	protected void fall(final float par1) {
	}

	/**
	 * adds a PotionEffect to the entity
	 */
	@Override
	public void addPotionEffect(final PotionEffect par1PotionEffect) {
	}

	/**
	 * Returns true if the newer Entity AI code should be run
	 */
	@Override
	protected boolean isAIEnabled() {
		return true;
	}

	@Override
	public int getMaxHealth() {
		return 300;
	}

	public float func_82207_a(final int par1) {
		return field_82221_e[par1];
	}

	public float func_82210_r(final int par1) {
		return field_82220_d[par1];
	}

	public int func_82212_n() {
		return dataWatcher.getWatchableObjectInt(20);
	}

	public void func_82215_s(final int par1) {
		dataWatcher.updateObject(20, Integer.valueOf(par1));
	}

	/**
	 * Returns the target entity ID if present, or -1 if not @param par1 The
	 * target offset, should be from 0-2
	 */
	public int getWatchedTargetId(final int par1) {
		return dataWatcher.getWatchableObjectInt(17 + par1);
	}

	public void func_82211_c(final int par1, final int par2) {
		dataWatcher.updateObject(17 + par1, Integer.valueOf(par2));
	}

	/**
	 * Returns whether the wither is armored with its boss armor or not by
	 * checking whether its health is below half of its maximum.
	 */
	public boolean isArmored() {
		return getBossHealth() <= getMaxHealth() / 2;
	}

	/**
	 * Get this Entity's EnumCreatureAttribute
	 */
	@Override
	public EnumCreatureAttribute getCreatureAttribute() {
		return EnumCreatureAttribute.UNDEAD;
	}

	/**
	 * Called when a player mounts an entity. e.g. mounts a pig, mounts a boat.
	 */
	@Override
	public void mountEntity(final Entity par1Entity) {
		ridingEntity = null;
	}
}
