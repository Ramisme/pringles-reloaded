package net.minecraft.src;

public class ServerData {
	public String serverName;
	public String serverIP;

	/**
	 * the string indicating number of players on and capacity of the server
	 * that is shown on the server browser (i.e. "5/20" meaning 5 slots used out
	 * of 20 slots total)
	 */
	public String populationInfo;

	/**
	 * (better variable name would be 'hostname') server name as displayed in
	 * the server browser's second line (grey text)
	 */
	public String serverMOTD;

	/** last server ping that showed up in the server browser */
	public long pingToServer;
	public int field_82821_f = 61;

	/** Game version for this server. */
	public String gameVersion = "1.5.2";
	public boolean field_78841_f = false;
	private boolean field_78842_g = true;
	private boolean acceptsTextures = false;

	/** Whether to hide the IP address for this server. */
	private boolean hideAddress = false;

	public ServerData(final String par1Str, final String par2Str) {
		serverName = par1Str;
		serverIP = par2Str;
	}

	/**
	 * Returns an NBTTagCompound with the server's name, IP and maybe
	 * acceptTextures.
	 */
	public NBTTagCompound getNBTCompound() {
		final NBTTagCompound var1 = new NBTTagCompound();
		var1.setString("name", serverName);
		var1.setString("ip", serverIP);
		var1.setBoolean("hideAddress", hideAddress);

		if (!field_78842_g) {
			var1.setBoolean("acceptTextures", acceptsTextures);
		}

		return var1;
	}

	public boolean getAcceptsTextures() {
		return acceptsTextures;
	}

	public boolean func_78840_c() {
		return field_78842_g;
	}

	public void setAcceptsTextures(final boolean par1) {
		acceptsTextures = par1;
		field_78842_g = false;
	}

	public boolean isHidingAddress() {
		return hideAddress;
	}

	public void setHideAddress(final boolean par1) {
		hideAddress = par1;
	}

	/**
	 * Takes an NBTTagCompound with 'name' and 'ip' keys, returns a ServerData
	 * instance.
	 */
	public static ServerData getServerDataFromNBTCompound(
			final NBTTagCompound par0NBTTagCompound) {
		final ServerData var1 = new ServerData(
				par0NBTTagCompound.getString("name"),
				par0NBTTagCompound.getString("ip"));
		var1.hideAddress = par0NBTTagCompound.getBoolean("hideAddress");

		if (par0NBTTagCompound.hasKey("acceptTextures")) {
			var1.setAcceptsTextures(par0NBTTagCompound
					.getBoolean("acceptTextures"));
		}

		return var1;
	}
}
