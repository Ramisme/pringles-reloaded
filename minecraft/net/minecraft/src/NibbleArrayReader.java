package net.minecraft.src;

public class NibbleArrayReader {
	public final byte[] data;
	private final int depthBits;
	private final int depthBitsPlusFour;

	public NibbleArrayReader(final byte[] par1ArrayOfByte, final int par2) {
		data = par1ArrayOfByte;
		depthBits = par2;
		depthBitsPlusFour = par2 + 4;
	}

	public int get(final int par1, final int par2, final int par3) {
		final int var4 = par1 << depthBitsPlusFour | par3 << depthBits | par2;
		final int var5 = var4 >> 1;
		final int var6 = var4 & 1;
		return var6 == 0 ? data[var5] & 15 : data[var5] >> 4 & 15;
	}
}
