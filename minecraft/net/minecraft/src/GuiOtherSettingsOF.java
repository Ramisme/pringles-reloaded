package net.minecraft.src;

public class GuiOtherSettingsOF extends GuiScreen {
	private final GuiScreen prevScreen;
	protected String title = "Other Settings";
	private final GameSettings settings;
	private static EnumOptions[] enumOptions = new EnumOptions[] {
			EnumOptions.LAGOMETER, EnumOptions.PROFILER, EnumOptions.WEATHER,
			EnumOptions.TIME, EnumOptions.USE_FULLSCREEN,
			EnumOptions.FULLSCREEN_MODE, EnumOptions.ANAGLYPH,
			EnumOptions.AUTOSAVE_TICKS };
	private int lastMouseX = 0;
	private int lastMouseY = 0;
	private long mouseStillTime = 0L;

	public GuiOtherSettingsOF(final GuiScreen var1, final GameSettings var2) {
		prevScreen = var1;
		settings = var2;
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui() {
		final StringTranslate var1 = StringTranslate.getInstance();
		int var2 = 0;
		final EnumOptions[] var3 = GuiOtherSettingsOF.enumOptions;
		final int var4 = var3.length;

		for (int var5 = 0; var5 < var4; ++var5) {
			final EnumOptions var6 = var3[var5];
			final int var7 = width / 2 - 155 + var2 % 2 * 160;
			final int var8 = height / 6 + 21 * (var2 / 2) - 10;

			if (!var6.getEnumFloat()) {
				buttonList.add(new GuiSmallButton(var6.returnEnumOrdinal(),
						var7, var8, var6, settings.getKeyBinding(var6)));
			} else {
				buttonList.add(new GuiSlider(var6.returnEnumOrdinal(), var7,
						var8, var6, settings.getKeyBinding(var6), settings
								.getOptionFloatValue(var6)));
			}

			++var2;
		}

		buttonList.add(new GuiButton(210, width / 2 - 100, height / 6 + 168
				+ 11 - 22, "Reset Video Settings..."));
		buttonList.add(new GuiButton(200, width / 2 - 100,
				height / 6 + 168 + 11, var1.translateKey("gui.done")));
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	@Override
	protected void actionPerformed(final GuiButton var1) {
		if (var1.enabled) {
			if (var1.id < 100 && var1 instanceof GuiSmallButton) {
				settings.setOptionValue(
						((GuiSmallButton) var1).returnEnumOptions(), 1);
				var1.displayString = settings.getKeyBinding(EnumOptions
						.getEnumOptions(var1.id));
			}

			if (var1.id == 200) {
				mc.gameSettings.saveOptions();
				mc.displayGuiScreen(prevScreen);
			}

			if (var1.id == 210) {
				mc.gameSettings.saveOptions();
				final GuiYesNo var2 = new GuiYesNo(this,
						"Reset all video settings to their default values?",
						"", 9999);
				mc.displayGuiScreen(var2);
			}

			if (var1.id != EnumOptions.CLOUD_HEIGHT.ordinal()) {
				final ScaledResolution var5 = new ScaledResolution(
						mc.gameSettings, mc.displayWidth, mc.displayHeight);
				final int var3 = var5.getScaledWidth();
				final int var4 = var5.getScaledHeight();
				setWorldAndResolution(mc, var3, var4);
			}
		}
	}

	@Override
	public void confirmClicked(final boolean var1, final int var2) {
		if (var1) {
			mc.gameSettings.resetSettings();
		}

		mc.displayGuiScreen(this);
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(final int var1, final int var2, final float var3) {
		drawDefaultBackground();
		drawCenteredString(fontRenderer, title, width / 2, 20, 16777215);
		super.drawScreen(var1, var2, var3);

		if (Math.abs(var1 - lastMouseX) <= 5
				&& Math.abs(var2 - lastMouseY) <= 5) {
			final short var4 = 700;

			if (System.currentTimeMillis() >= mouseStillTime + var4) {
				final int var5 = width / 2 - 150;
				int var6 = height / 6 - 5;

				if (var2 <= var6 + 98) {
					var6 += 105;
				}

				final int var7 = var5 + 150 + 150;
				final int var8 = var6 + 84 + 10;
				final GuiButton var9 = getSelectedButton(var1, var2);

				if (var9 != null) {
					final String var10 = getButtonName(var9.displayString);
					final String[] var11 = getTooltipLines(var10);

					if (var11 == null) {
						return;
					}

					drawGradientRect(var5, var6, var7, var8, -536870912,
							-536870912);

					for (int var12 = 0; var12 < var11.length; ++var12) {
						final String var13 = var11[var12];
						fontRenderer.drawStringWithShadow(var13, var5 + 5, var6
								+ 5 + var12 * 11, 14540253);
					}
				}
			}
		} else {
			lastMouseX = var1;
			lastMouseY = var2;
			mouseStillTime = System.currentTimeMillis();
		}
	}

	private String[] getTooltipLines(final String var1) {
		return var1.equals("Autosave") ? new String[] { "Autosave interval",
				"Default autosave interval (2s) is NOT RECOMMENDED.",
				"Autosave causes the famous Lag Spike of Death." }
				: var1.equals("Lagometer") ? new String[] { "Lagometer",
						" OFF - no lagometer, faster",
						" ON - debug screen with lagometer, slower",
						"Shows the lagometer on the debug screen (F3).",
						"* White - tick", "* Red - chunk loading",
						"* Green - frame rendering + internal server" }
						: var1.equals("Debug Profiler") ? new String[] {
								"Debug Profiler",
								"  ON - debug profiler is active, slower",
								"  OFF - debug profiler is not active, faster",
								"The debug profiler collects and shows debug information",
								"when the debug screen is open (F3)" }
								: var1.equals("Time") ? new String[] { "Time",
										" Default - normal day/night cycles",
										" Day Only - day only",
										" Night Only - night only",
										"The time setting is only effective in CREATIVE mode." }
										: var1.equals("Weather") ? new String[] {
												"Weather",
												"  ON - weather is active, slower",
												"  OFF - weather is not active, faster",
												"The weather controls rain, snow and thunderstorms." }
												: var1.equals("Fullscreen") ? new String[] {
														"Fullscreen resolution",
														"  Default - use desktop screen resolution, slower",
														"  WxH - use custom screen resolution, may be faster",
														"The selected resolution is used in fullscreen mode (F11)." }
														: null;
	}

	private String getButtonName(final String var1) {
		final int var2 = var1.indexOf(58);
		return var2 < 0 ? var1 : var1.substring(0, var2);
	}

	private GuiButton getSelectedButton(final int var1, final int var2) {
		for (int var3 = 0; var3 < buttonList.size(); ++var3) {
			final GuiButton var4 = (GuiButton) buttonList.get(var3);
			final boolean var5 = var1 >= var4.xPosition
					&& var2 >= var4.yPosition
					&& var1 < var4.xPosition + var4.width
					&& var2 < var4.yPosition + var4.height;

			if (var5) {
				return var4;
			}
		}

		return null;
	}
}
