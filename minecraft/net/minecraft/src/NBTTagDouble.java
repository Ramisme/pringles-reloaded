package net.minecraft.src;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class NBTTagDouble extends NBTBase {
	/** The double value for the tag. */
	public double data;

	public NBTTagDouble(final String par1Str) {
		super(par1Str);
	}

	public NBTTagDouble(final String par1Str, final double par2) {
		super(par1Str);
		data = par2;
	}

	/**
	 * Write the actual data contents of the tag, implemented in NBT extension
	 * classes
	 */
	@Override
	void write(final DataOutput par1DataOutput) throws IOException {
		par1DataOutput.writeDouble(data);
	}

	/**
	 * Read the actual data contents of the tag, implemented in NBT extension
	 * classes
	 */
	@Override
	void load(final DataInput par1DataInput) throws IOException {
		data = par1DataInput.readDouble();
	}

	/**
	 * Gets the type byte for the tag.
	 */
	@Override
	public byte getId() {
		return (byte) 6;
	}

	@Override
	public String toString() {
		return "" + data;
	}

	/**
	 * Creates a clone of the tag.
	 */
	@Override
	public NBTBase copy() {
		return new NBTTagDouble(getName(), data);
	}

	@Override
	public boolean equals(final Object par1Obj) {
		if (super.equals(par1Obj)) {
			final NBTTagDouble var2 = (NBTTagDouble) par1Obj;
			return data == var2.data;
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		final long var1 = Double.doubleToLongBits(data);
		return super.hashCode() ^ (int) (var1 ^ var1 >>> 32);
	}
}
