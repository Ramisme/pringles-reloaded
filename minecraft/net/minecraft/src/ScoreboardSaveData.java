package net.minecraft.src;

import java.util.Collection;
import java.util.Iterator;

import net.minecraft.server.MinecraftServer;

public class ScoreboardSaveData extends WorldSavedData {
	private Scoreboard field_96507_a;
	private NBTTagCompound field_96506_b;

	public ScoreboardSaveData() {
		this("scoreboard");
	}

	public ScoreboardSaveData(final String par1Str) {
		super(par1Str);
	}

	public void func_96499_a(final Scoreboard par1Scoreboard) {
		field_96507_a = par1Scoreboard;

		if (field_96506_b != null) {
			readFromNBT(field_96506_b);
		}
	}

	/**
	 * reads in data from the NBTTagCompound into this MapDataBase
	 */
	@Override
	public void readFromNBT(final NBTTagCompound par1NBTTagCompound) {
		if (field_96507_a == null) {
			field_96506_b = par1NBTTagCompound;
		} else {
			func_96501_b(par1NBTTagCompound.getTagList("Objectives"));
			func_96500_c(par1NBTTagCompound.getTagList("PlayerScores"));

			if (par1NBTTagCompound.hasKey("DisplaySlots")) {
				func_96504_c(par1NBTTagCompound.getCompoundTag("DisplaySlots"));
			}

			if (par1NBTTagCompound.hasKey("Teams")) {
				func_96498_a(par1NBTTagCompound.getTagList("Teams"));
			}
		}
	}

	protected void func_96498_a(final NBTTagList par1NBTTagList) {
		for (int var2 = 0; var2 < par1NBTTagList.tagCount(); ++var2) {
			final NBTTagCompound var3 = (NBTTagCompound) par1NBTTagList
					.tagAt(var2);
			final ScorePlayerTeam var4 = field_96507_a.func_96527_f(var3
					.getString("Name"));
			var4.func_96664_a(var3.getString("DisplayName"));
			var4.func_96666_b(var3.getString("Prefix"));
			var4.func_96662_c(var3.getString("Suffix"));

			if (var3.hasKey("AllowFriendlyFire")) {
				var4.func_96660_a(var3.getBoolean("AllowFriendlyFire"));
			}

			if (var3.hasKey("SeeFriendlyInvisibles")) {
				var4.func_98300_b(var3.getBoolean("SeeFriendlyInvisibles"));
			}

			func_96502_a(var4, var3.getTagList("Players"));
		}
	}

	protected void func_96502_a(final ScorePlayerTeam par1ScorePlayerTeam,
			final NBTTagList par2NBTTagList) {
		for (int var3 = 0; var3 < par2NBTTagList.tagCount(); ++var3) {
			field_96507_a.func_96521_a(
					((NBTTagString) par2NBTTagList.tagAt(var3)).data,
					par1ScorePlayerTeam);
		}
	}

	protected void func_96504_c(final NBTTagCompound par1NBTTagCompound) {
		for (int var2 = 0; var2 < 3; ++var2) {
			if (par1NBTTagCompound.hasKey("slot_" + var2)) {
				final String var3 = par1NBTTagCompound
						.getString("slot_" + var2);
				final ScoreObjective var4 = field_96507_a.getObjective(var3);
				field_96507_a.func_96530_a(var2, var4);
			}
		}
	}

	protected void func_96501_b(final NBTTagList par1NBTTagList) {
		for (int var2 = 0; var2 < par1NBTTagList.tagCount(); ++var2) {
			final NBTTagCompound var3 = (NBTTagCompound) par1NBTTagList
					.tagAt(var2);
			final ScoreObjectiveCriteria var4 = (ScoreObjectiveCriteria) ScoreObjectiveCriteria.field_96643_a
					.get(var3.getString("CriteriaName"));
			final ScoreObjective var5 = field_96507_a.func_96535_a(
					var3.getString("Name"), var4);
			var5.setDisplayName(var3.getString("DisplayName"));
		}
	}

	protected void func_96500_c(final NBTTagList par1NBTTagList) {
		for (int var2 = 0; var2 < par1NBTTagList.tagCount(); ++var2) {
			final NBTTagCompound var3 = (NBTTagCompound) par1NBTTagList
					.tagAt(var2);
			final ScoreObjective var4 = field_96507_a.getObjective(var3
					.getString("Objective"));
			final Score var5 = field_96507_a.func_96529_a(
					var3.getString("Name"), var4);
			var5.func_96647_c(var3.getInteger("Score"));
		}
	}

	/**
	 * write data to NBTTagCompound from this MapDataBase, similar to Entities
	 * and TileEntities
	 */
	@Override
	public void writeToNBT(final NBTTagCompound par1NBTTagCompound) {
		if (field_96507_a == null) {
			MinecraftServer
					.getServer()
					.getLogAgent()
					.logWarning(
							"Tried to save scoreboard without having a scoreboard...");
		} else {
			par1NBTTagCompound.setTag("Objectives", func_96505_b());
			par1NBTTagCompound.setTag("PlayerScores", func_96503_e());
			par1NBTTagCompound.setTag("Teams", func_96496_a());
			func_96497_d(par1NBTTagCompound);
		}
	}

	protected NBTTagList func_96496_a() {
		final NBTTagList var1 = new NBTTagList();
		final Collection var2 = field_96507_a.func_96525_g();
		final Iterator var3 = var2.iterator();

		while (var3.hasNext()) {
			final ScorePlayerTeam var4 = (ScorePlayerTeam) var3.next();
			final NBTTagCompound var5 = new NBTTagCompound();
			var5.setString("Name", var4.func_96661_b());
			var5.setString("DisplayName", var4.func_96669_c());
			var5.setString("Prefix", var4.func_96668_e());
			var5.setString("Suffix", var4.func_96663_f());
			var5.setBoolean("AllowFriendlyFire", var4.func_96665_g());
			var5.setBoolean("SeeFriendlyInvisibles", var4.func_98297_h());
			final NBTTagList var6 = new NBTTagList();
			final Iterator var7 = var4.getMembershipCollection().iterator();

			while (var7.hasNext()) {
				final String var8 = (String) var7.next();
				var6.appendTag(new NBTTagString("", var8));
			}

			var5.setTag("Players", var6);
			var1.appendTag(var5);
		}

		return var1;
	}

	protected void func_96497_d(final NBTTagCompound par1NBTTagCompound) {
		final NBTTagCompound var2 = new NBTTagCompound();
		boolean var3 = false;

		for (int var4 = 0; var4 < 3; ++var4) {
			final ScoreObjective var5 = field_96507_a.func_96539_a(var4);

			if (var5 != null) {
				var2.setString("slot_" + var4, var5.getName());
				var3 = true;
			}
		}

		if (var3) {
			par1NBTTagCompound.setCompoundTag("DisplaySlots", var2);
		}
	}

	protected NBTTagList func_96505_b() {
		final NBTTagList var1 = new NBTTagList();
		final Collection var2 = field_96507_a.getScoreObjectives();
		final Iterator var3 = var2.iterator();

		while (var3.hasNext()) {
			final ScoreObjective var4 = (ScoreObjective) var3.next();
			final NBTTagCompound var5 = new NBTTagCompound();
			var5.setString("Name", var4.getName());
			var5.setString("CriteriaName", var4.getCriteria().func_96636_a());
			var5.setString("DisplayName", var4.getDisplayName());
			var1.appendTag(var5);
		}

		return var1;
	}

	protected NBTTagList func_96503_e() {
		final NBTTagList var1 = new NBTTagList();
		final Collection var2 = field_96507_a.func_96528_e();
		final Iterator var3 = var2.iterator();

		while (var3.hasNext()) {
			final Score var4 = (Score) var3.next();
			final NBTTagCompound var5 = new NBTTagCompound();
			var5.setString("Name", var4.func_96653_e());
			var5.setString("Objective", var4.func_96645_d().getName());
			var5.setInteger("Score", var4.func_96652_c());
			var1.appendTag(var5);
		}

		return var1;
	}
}
