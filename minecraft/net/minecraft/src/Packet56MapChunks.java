package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

public class Packet56MapChunks extends Packet {
	private int[] chunkPostX;
	private int[] chunkPosZ;
	public int[] field_73590_a;
	public int[] field_73588_b;

	/** The compressed chunk data buffer */
	private byte[] chunkDataBuffer;
	private byte[][] field_73584_f;

	/** total size of the compressed data */
	private int dataLength;

	/**
	 * Whether or not the chunk data contains a light nibble array. This is true
	 * in the main world, false in the end + nether.
	 */
	private boolean skyLightSent;
	private static byte[] chunkDataNotCompressed = new byte[0];

	public Packet56MapChunks() {
	}

	public Packet56MapChunks(final List par1List) {
		final int var2 = par1List.size();
		chunkPostX = new int[var2];
		chunkPosZ = new int[var2];
		field_73590_a = new int[var2];
		field_73588_b = new int[var2];
		field_73584_f = new byte[var2][];
		skyLightSent = !par1List.isEmpty()
				&& !((Chunk) par1List.get(0)).worldObj.provider.hasNoSky;
		int var3 = 0;

		for (int var4 = 0; var4 < var2; ++var4) {
			final Chunk var5 = (Chunk) par1List.get(var4);
			final Packet51MapChunkData var6 = Packet51MapChunk.getMapChunkData(
					var5, true, 65535);

			if (Packet56MapChunks.chunkDataNotCompressed.length < var3
					+ var6.compressedData.length) {
				final byte[] var7 = new byte[var3 + var6.compressedData.length];
				System.arraycopy(Packet56MapChunks.chunkDataNotCompressed, 0,
						var7, 0,
						Packet56MapChunks.chunkDataNotCompressed.length);
				Packet56MapChunks.chunkDataNotCompressed = var7;
			}

			System.arraycopy(var6.compressedData, 0,
					Packet56MapChunks.chunkDataNotCompressed, var3,
					var6.compressedData.length);
			var3 += var6.compressedData.length;
			chunkPostX[var4] = var5.xPosition;
			chunkPosZ[var4] = var5.zPosition;
			field_73590_a[var4] = var6.chunkExistFlag;
			field_73588_b[var4] = var6.chunkHasAddSectionFlag;
			field_73584_f[var4] = var6.compressedData;
		}

		final Deflater var11 = new Deflater(-1);

		try {
			var11.setInput(Packet56MapChunks.chunkDataNotCompressed, 0, var3);
			var11.finish();
			chunkDataBuffer = new byte[var3];
			dataLength = var11.deflate(chunkDataBuffer);
		} finally {
			var11.end();
		}
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		final short var2 = par1DataInputStream.readShort();
		dataLength = par1DataInputStream.readInt();
		skyLightSent = par1DataInputStream.readBoolean();
		chunkPostX = new int[var2];
		chunkPosZ = new int[var2];
		field_73590_a = new int[var2];
		field_73588_b = new int[var2];
		field_73584_f = new byte[var2][];

		if (Packet56MapChunks.chunkDataNotCompressed.length < dataLength) {
			Packet56MapChunks.chunkDataNotCompressed = new byte[dataLength];
		}

		par1DataInputStream.readFully(Packet56MapChunks.chunkDataNotCompressed,
				0, dataLength);
		final byte[] var3 = new byte[196864 * var2];
		final Inflater var4 = new Inflater();
		var4.setInput(Packet56MapChunks.chunkDataNotCompressed, 0, dataLength);

		try {
			var4.inflate(var3);
		} catch (final DataFormatException var12) {
			throw new IOException("Bad compressed data format");
		} finally {
			var4.end();
		}

		int var5 = 0;

		for (int var6 = 0; var6 < var2; ++var6) {
			chunkPostX[var6] = par1DataInputStream.readInt();
			chunkPosZ[var6] = par1DataInputStream.readInt();
			field_73590_a[var6] = par1DataInputStream.readShort();
			field_73588_b[var6] = par1DataInputStream.readShort();
			int var7 = 0;
			int var8 = 0;
			int var9;

			for (var9 = 0; var9 < 16; ++var9) {
				var7 += field_73590_a[var6] >> var9 & 1;
				var8 += field_73588_b[var6] >> var9 & 1;
			}

			var9 = 2048 * 4 * var7 + 256;
			var9 += 2048 * var8;

			if (skyLightSent) {
				var9 += 2048 * var7;
			}

			field_73584_f[var6] = new byte[var9];
			System.arraycopy(var3, var5, field_73584_f[var6], 0, var9);
			var5 += var9;
		}
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		par1DataOutputStream.writeShort(chunkPostX.length);
		par1DataOutputStream.writeInt(dataLength);
		par1DataOutputStream.writeBoolean(skyLightSent);
		par1DataOutputStream.write(chunkDataBuffer, 0, dataLength);

		for (int var2 = 0; var2 < chunkPostX.length; ++var2) {
			par1DataOutputStream.writeInt(chunkPostX[var2]);
			par1DataOutputStream.writeInt(chunkPosZ[var2]);
			par1DataOutputStream
					.writeShort((short) (field_73590_a[var2] & 65535));
			par1DataOutputStream
					.writeShort((short) (field_73588_b[var2] & 65535));
		}
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleMapChunks(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 6 + dataLength + 12 * getNumberOfChunkInPacket();
	}

	public int getChunkPosX(final int par1) {
		return chunkPostX[par1];
	}

	public int getChunkPosZ(final int par1) {
		return chunkPosZ[par1];
	}

	public int getNumberOfChunkInPacket() {
		return chunkPostX.length;
	}

	public byte[] getChunkCompressedData(final int par1) {
		return field_73584_f[par1];
	}
}
