package net.minecraft.src;

public class ItemMultiTextureTile extends ItemBlock {
	private final Block theBlock;
	private final String[] field_82804_b;

	public ItemMultiTextureTile(final int par1, final Block par2Block,
			final String[] par3ArrayOfStr) {
		super(par1);
		theBlock = par2Block;
		field_82804_b = par3ArrayOfStr;
		setMaxDamage(0);
		setHasSubtypes(true);
	}

	/**
	 * Gets an icon index based on an item's damage value
	 */
	@Override
	public Icon getIconFromDamage(final int par1) {
		return theBlock.getIcon(2, par1);
	}

	/**
	 * Returns the metadata of the block which this Item (ItemBlock) can place
	 */
	@Override
	public int getMetadata(final int par1) {
		return par1;
	}

	/**
	 * Returns the unlocalized name of this item. This version accepts an
	 * ItemStack so different stacks can have different names based on their
	 * damage or NBT.
	 */
	@Override
	public String getUnlocalizedName(final ItemStack par1ItemStack) {
		int var2 = par1ItemStack.getItemDamage();

		if (var2 < 0 || var2 >= field_82804_b.length) {
			var2 = 0;
		}

		return super.getUnlocalizedName() + "." + field_82804_b[var2];
	}
}
