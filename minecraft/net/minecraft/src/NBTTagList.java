package net.minecraft.src;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class NBTTagList extends NBTBase {
	/** The array list containing the tags encapsulated in this list. */
	private List tagList = new ArrayList();

	/**
	 * The type byte for the tags in the list - they must all be of the same
	 * type.
	 */
	private byte tagType;

	public NBTTagList() {
		super("");
	}

	public NBTTagList(final String par1Str) {
		super(par1Str);
	}

	/**
	 * Write the actual data contents of the tag, implemented in NBT extension
	 * classes
	 */
	@Override
	void write(final DataOutput par1DataOutput) throws IOException {
		if (!tagList.isEmpty()) {
			tagType = ((NBTBase) tagList.get(0)).getId();
		} else {
			tagType = 1;
		}

		par1DataOutput.writeByte(tagType);
		par1DataOutput.writeInt(tagList.size());

		for (int var2 = 0; var2 < tagList.size(); ++var2) {
			((NBTBase) tagList.get(var2)).write(par1DataOutput);
		}
	}

	/**
	 * Read the actual data contents of the tag, implemented in NBT extension
	 * classes
	 */
	@Override
	void load(final DataInput par1DataInput) throws IOException {
		tagType = par1DataInput.readByte();
		final int var2 = par1DataInput.readInt();
		tagList = new ArrayList();

		for (int var3 = 0; var3 < var2; ++var3) {
			final NBTBase var4 = NBTBase.newTag(tagType, (String) null);
			var4.load(par1DataInput);
			tagList.add(var4);
		}
	}

	/**
	 * Gets the type byte for the tag.
	 */
	@Override
	public byte getId() {
		return (byte) 9;
	}

	@Override
	public String toString() {
		return "" + tagList.size() + " entries of type "
				+ NBTBase.getTagName(tagType);
	}

	/**
	 * Adds the provided tag to the end of the list. There is no check to verify
	 * this tag is of the same type as any previous tag.
	 */
	public void appendTag(final NBTBase par1NBTBase) {
		tagType = par1NBTBase.getId();
		tagList.add(par1NBTBase);
	}

	/**
	 * Removes a tag at the given index.
	 */
	public NBTBase removeTag(final int par1) {
		return (NBTBase) tagList.remove(par1);
	}

	/**
	 * Retrieves the tag at the specified index from the list.
	 */
	public NBTBase tagAt(final int par1) {
		return (NBTBase) tagList.get(par1);
	}

	/**
	 * Returns the number of tags in the list.
	 */
	public int tagCount() {
		return tagList.size();
	}

	/**
	 * Creates a clone of the tag.
	 */
	@Override
	public NBTBase copy() {
		final NBTTagList var1 = new NBTTagList(getName());
		var1.tagType = tagType;
		final Iterator var2 = tagList.iterator();

		while (var2.hasNext()) {
			final NBTBase var3 = (NBTBase) var2.next();
			final NBTBase var4 = var3.copy();
			var1.tagList.add(var4);
		}

		return var1;
	}

	@Override
	public boolean equals(final Object par1Obj) {
		if (super.equals(par1Obj)) {
			final NBTTagList var2 = (NBTTagList) par1Obj;

			if (tagType == var2.tagType) {
				return tagList.equals(var2.tagList);
			}
		}

		return false;
	}

	@Override
	public int hashCode() {
		return super.hashCode() ^ tagList.hashCode();
	}
}
