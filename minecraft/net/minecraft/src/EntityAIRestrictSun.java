package net.minecraft.src;

public class EntityAIRestrictSun extends EntityAIBase {
	private final EntityCreature theEntity;

	public EntityAIRestrictSun(final EntityCreature par1EntityCreature) {
		theEntity = par1EntityCreature;
	}

	/**
	 * Returns whether the EntityAIBase should begin execution.
	 */
	@Override
	public boolean shouldExecute() {
		return theEntity.worldObj.isDaytime();
	}

	/**
	 * Execute a one shot task or start executing a continuous task
	 */
	@Override
	public void startExecuting() {
		theEntity.getNavigator().setAvoidSun(true);
	}

	/**
	 * Resets the task
	 */
	@Override
	public void resetTask() {
		theEntity.getNavigator().setAvoidSun(false);
	}
}
