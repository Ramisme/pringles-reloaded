package net.minecraft.src;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import paulscode.sound.SoundSystem;
import paulscode.sound.SoundSystemConfig;
import paulscode.sound.codecs.CodecJOrbis;
import paulscode.sound.codecs.CodecWav;
import paulscode.sound.libraries.LibraryLWJGLOpenAL;

public class SoundManager {
	/** A reference to the sound system. */
	private static SoundSystem sndSystem;

	/** Sound pool containing sounds. */
	private final SoundPool soundPoolSounds = new SoundPool();

	/** Sound pool containing streaming audio. */
	private final SoundPool soundPoolStreaming = new SoundPool();

	/** Sound pool containing music. */
	private final SoundPool soundPoolMusic = new SoundPool();

	/**
	 * The last ID used when a sound is played, passed into SoundSystem to give
	 * active sounds a unique ID
	 */
	private int latestSoundID = 0;

	/** A reference to the game settings. */
	private GameSettings options;

	/** Identifiers of all currently playing sounds. Type: HashSet<String> */
	private final Set playingSounds = new HashSet();
	private final List field_92072_h = new ArrayList();

	/** Set to true when the SoundManager has been initialised. */
	private static boolean loaded = false;

	/** RNG. */
	private final Random rand = new Random();
	private int ticksBeforeMusic;

	public SoundManager() {
		ticksBeforeMusic = rand.nextInt(12000);
	}

	/**
	 * Used for loading sound settings from GameSettings
	 */
	public void loadSoundSettings(final GameSettings par1GameSettings) {
		soundPoolStreaming.isGetRandomSound = false;
		options = par1GameSettings;

		if (!SoundManager.loaded
				&& (par1GameSettings == null
						|| par1GameSettings.soundVolume != 0.0F || par1GameSettings.musicVolume != 0.0F)) {
			tryToSetLibraryAndCodecs();
		}
	}

	/**
	 * Tries to add the paulscode library and the relevant codecs. If it fails,
	 * the volumes (sound and music) will be set to zero in the options file.
	 */
	private void tryToSetLibraryAndCodecs() {
		try {
			final float var1 = options.soundVolume;
			final float var2 = options.musicVolume;
			options.soundVolume = 0.0F;
			options.musicVolume = 0.0F;
			options.saveOptions();
			SoundSystemConfig.addLibrary(LibraryLWJGLOpenAL.class);
			SoundSystemConfig.setCodec("ogg", CodecJOrbis.class);
			SoundSystemConfig.setCodec("mus", CodecMus.class);
			SoundSystemConfig.setCodec("wav", CodecWav.class);
			SoundManager.sndSystem = new SoundSystem();
			options.soundVolume = var1;
			options.musicVolume = var2;
			options.saveOptions();
		} catch (final Throwable var3) {
			var3.printStackTrace();
			System.err
					.println("error linking with the LibraryJavaSound plug-in");
		}

		SoundManager.loaded = true;
	}

	/**
	 * Called when one of the sound level options has changed.
	 */
	public void onSoundOptionsChanged() {
		if (!SoundManager.loaded
				&& (options.soundVolume != 0.0F || options.musicVolume != 0.0F)) {
			tryToSetLibraryAndCodecs();
		}

		if (SoundManager.loaded) {
			if (options.musicVolume == 0.0F) {
				SoundManager.sndSystem.stop("BgMusic");
				SoundManager.sndSystem.stop("streaming");
			} else {
				SoundManager.sndSystem
						.setVolume("BgMusic", options.musicVolume);
				SoundManager.sndSystem.setVolume("streaming",
						options.musicVolume);
			}
		}
	}

	/**
	 * Called when Minecraft is closing down.
	 */
	public void closeMinecraft() {
		if (SoundManager.loaded) {
			SoundManager.sndSystem.cleanup();
		}
	}

	/**
	 * Adds a sounds with the name from the file. Args: name, file
	 */
	public void addSound(final String par1Str, final File par2File) {
		soundPoolSounds.addSound(par1Str, par2File);
	}

	/**
	 * Adds an audio file to the streaming SoundPool.
	 */
	public void addStreaming(final String par1Str, final File par2File) {
		soundPoolStreaming.addSound(par1Str, par2File);
	}

	/**
	 * Adds an audio file to the music SoundPool.
	 */
	public void addMusic(final String par1Str, final File par2File) {
		soundPoolMusic.addSound(par1Str, par2File);
	}

	/**
	 * If its time to play new music it starts it up.
	 */
	public void playRandomMusicIfReady() {
		if (SoundManager.loaded && options.musicVolume != 0.0F) {
			if (!SoundManager.sndSystem.playing("BgMusic")
					&& !SoundManager.sndSystem.playing("streaming")) {
				if (ticksBeforeMusic > 0) {
					--ticksBeforeMusic;
					return;
				}

				final SoundPoolEntry var1 = soundPoolMusic.getRandomSound();

				if (var1 != null) {
					ticksBeforeMusic = rand.nextInt(12000) + 12000;
					SoundManager.sndSystem.backgroundMusic("BgMusic",
							var1.soundUrl, var1.soundName, false);
					SoundManager.sndSystem.setVolume("BgMusic",
							options.musicVolume);
					SoundManager.sndSystem.play("BgMusic");
				}
			}
		}
	}

	/**
	 * Sets the listener of sounds
	 */
	public void setListener(final EntityLiving par1EntityLiving,
			final float par2) {
		if (SoundManager.loaded && options.soundVolume != 0.0F) {
			if (par1EntityLiving != null) {
				final float var3 = par1EntityLiving.prevRotationPitch
						+ (par1EntityLiving.rotationPitch - par1EntityLiving.prevRotationPitch)
						* par2;
				final float var4 = par1EntityLiving.prevRotationYaw
						+ (par1EntityLiving.rotationYaw - par1EntityLiving.prevRotationYaw)
						* par2;
				final double var5 = par1EntityLiving.prevPosX
						+ (par1EntityLiving.posX - par1EntityLiving.prevPosX)
						* par2;
				final double var7 = par1EntityLiving.prevPosY
						+ (par1EntityLiving.posY - par1EntityLiving.prevPosY)
						* par2;
				final double var9 = par1EntityLiving.prevPosZ
						+ (par1EntityLiving.posZ - par1EntityLiving.prevPosZ)
						* par2;
				final float var11 = MathHelper.cos(-var4 * 0.017453292F
						- (float) Math.PI);
				final float var12 = MathHelper.sin(-var4 * 0.017453292F
						- (float) Math.PI);
				final float var13 = -var12;
				final float var14 = -MathHelper.sin(-var3 * 0.017453292F
						- (float) Math.PI);
				final float var15 = -var11;
				final float var16 = 0.0F;
				final float var17 = 1.0F;
				final float var18 = 0.0F;
				SoundManager.sndSystem.setListenerPosition((float) var5,
						(float) var7, (float) var9);
				SoundManager.sndSystem.setListenerOrientation(var13, var14,
						var15, var16, var17, var18);
			}
		}
	}

	/**
	 * Stops all currently playing sounds
	 */
	public void stopAllSounds() {
		final Iterator var1 = playingSounds.iterator();

		while (var1.hasNext()) {
			final String var2 = (String) var1.next();
			SoundManager.sndSystem.stop(var2);
		}

		playingSounds.clear();
	}

	public void playStreaming(final String par1Str, final float par2,
			final float par3, final float par4) {
		if (SoundManager.loaded
				&& (options.soundVolume != 0.0F || par1Str == null)) {
			final String var5 = "streaming";

			if (SoundManager.sndSystem.playing(var5)) {
				SoundManager.sndSystem.stop(var5);
			}

			if (par1Str != null) {
				final SoundPoolEntry var6 = soundPoolStreaming
						.getRandomSoundFromSoundPool(par1Str);

				if (var6 != null) {
					if (SoundManager.sndSystem.playing("BgMusic")) {
						SoundManager.sndSystem.stop("BgMusic");
					}

					final float var7 = 16.0F;
					SoundManager.sndSystem.newStreamingSource(true, var5,
							var6.soundUrl, var6.soundName, false, par2, par3,
							par4, 2, var7 * 4.0F);
					SoundManager.sndSystem.setVolume(var5,
							0.5F * options.soundVolume);
					SoundManager.sndSystem.play(var5);
				}
			}
		}
	}

	/**
	 * Updates the sound associated with the entity with that entity's position
	 * and velocity. Args: the entity
	 */
	public void updateSoundLocation(final Entity par1Entity) {
		this.updateSoundLocation(par1Entity, par1Entity);
	}

	/**
	 * Updates the sound associated with soundEntity with the position and
	 * velocity of trackEntity. Args: soundEntity, trackEntity
	 */
	public void updateSoundLocation(final Entity par1Entity,
			final Entity par2Entity) {
		final String var3 = "entity_" + par1Entity.entityId;

		if (playingSounds.contains(var3)) {
			if (SoundManager.sndSystem.playing(var3)) {
				SoundManager.sndSystem.setPosition(var3,
						(float) par2Entity.posX, (float) par2Entity.posY,
						(float) par2Entity.posZ);
				SoundManager.sndSystem.setVelocity(var3,
						(float) par2Entity.motionX, (float) par2Entity.motionY,
						(float) par2Entity.motionZ);
			} else {
				playingSounds.remove(var3);
			}
		}
	}

	/**
	 * Returns true if a sound is currently associated with the given entity, or
	 * false otherwise.
	 */
	public boolean isEntitySoundPlaying(final Entity par1Entity) {
		if (par1Entity != null && SoundManager.loaded) {
			final String var2 = "entity_" + par1Entity.entityId;
			return SoundManager.sndSystem.playing(var2);
		} else {
			return false;
		}
	}

	/**
	 * Stops playing the sound associated with the given entity
	 */
	public void stopEntitySound(final Entity par1Entity) {
		if (par1Entity != null && SoundManager.loaded) {
			final String var2 = "entity_" + par1Entity.entityId;

			if (playingSounds.contains(var2)) {
				if (SoundManager.sndSystem.playing(var2)) {
					SoundManager.sndSystem.stop(var2);
				}

				playingSounds.remove(var2);
			}
		}
	}

	/**
	 * Sets the volume of the sound associated with the given entity, if one is
	 * playing. The volume is scaled by the global sound volume. Args: the
	 * entity, the volume (from 0 to 1)
	 */
	public void setEntitySoundVolume(final Entity par1Entity, final float par2) {
		if (par1Entity != null && SoundManager.loaded) {
			if (SoundManager.loaded && options.soundVolume != 0.0F) {
				final String var3 = "entity_" + par1Entity.entityId;

				if (SoundManager.sndSystem.playing(var3)) {
					SoundManager.sndSystem.setVolume(var3, par2
							* options.soundVolume);
				}
			}
		}
	}

	/**
	 * Sets the pitch of the sound associated with the given entity, if one is
	 * playing. Args: the entity, the pitch
	 */
	public void setEntitySoundPitch(final Entity par1Entity, final float par2) {
		if (par1Entity != null && SoundManager.loaded) {
			if (SoundManager.loaded && options.soundVolume != 0.0F) {
				final String var3 = "entity_" + par1Entity.entityId;

				if (SoundManager.sndSystem.playing(var3)) {
					SoundManager.sndSystem.setPitch(var3, par2);
				}
			}
		}
	}

	/**
	 * If a sound is already playing from the given entity, update the position
	 * and velocity of that sound to match the entity. Otherwise, start playing
	 * a sound from that entity. Setting the last flag to true will prevent
	 * other sounds from overriding this one. Args: The sound name, the entity,
	 * the volume, the pitch, priority
	 */
	public void playEntitySound(final String par1Str, final Entity par2Entity,
			float par3, final float par4, final boolean par5) {
		if (par2Entity != null) {
			if (SoundManager.loaded
					&& (options.soundVolume != 0.0F || par1Str == null)) {
				final String var6 = "entity_" + par2Entity.entityId;

				if (playingSounds.contains(var6)) {
					this.updateSoundLocation(par2Entity);
				} else {
					if (SoundManager.sndSystem.playing(var6)) {
						SoundManager.sndSystem.stop(var6);
					}

					if (par1Str == null) {
						return;
					}

					final SoundPoolEntry var7 = soundPoolSounds
							.getRandomSoundFromSoundPool(par1Str);

					if (var7 != null && par3 > 0.0F) {
						float var8 = 16.0F;

						if (par3 > 1.0F) {
							var8 *= par3;
						}

						SoundManager.sndSystem.newSource(par5, var6,
								var7.soundUrl, var7.soundName, false,
								(float) par2Entity.posX,
								(float) par2Entity.posY,
								(float) par2Entity.posZ, 2, var8);
						SoundManager.sndSystem.setLooping(var6, true);
						SoundManager.sndSystem.setPitch(var6, par4);

						if (par3 > 1.0F) {
							par3 = 1.0F;
						}

						SoundManager.sndSystem.setVolume(var6, par3
								* options.soundVolume);
						SoundManager.sndSystem.setVelocity(var6,
								(float) par2Entity.motionX,
								(float) par2Entity.motionY,
								(float) par2Entity.motionZ);
						SoundManager.sndSystem.play(var6);
						playingSounds.add(var6);
					}
				}
			}
		}
	}

	/**
	 * Plays a sound. Args: soundName, x, y, z, volume, pitch
	 */
	public void playSound(final String par1Str, final float par2,
			final float par3, final float par4, float par5, final float par6) {
		if (SoundManager.loaded && options.soundVolume != 0.0F) {
			final SoundPoolEntry var7 = soundPoolSounds
					.getRandomSoundFromSoundPool(par1Str);

			if (var7 != null && par5 > 0.0F) {
				latestSoundID = (latestSoundID + 1) % 256;
				final String var8 = "sound_" + latestSoundID;
				float var9 = 16.0F;

				if (par5 > 1.0F) {
					var9 *= par5;
				}

				SoundManager.sndSystem.newSource(par5 > 1.0F, var8,
						var7.soundUrl, var7.soundName, false, par2, par3, par4,
						2, var9);
				SoundManager.sndSystem.setPitch(var8, par6);

				if (par5 > 1.0F) {
					par5 = 1.0F;
				}

				SoundManager.sndSystem.setVolume(var8, par5
						* options.soundVolume);
				SoundManager.sndSystem.play(var8);
			}
		}
	}

	/**
	 * Plays a sound effect with the volume and pitch of the parameters passed.
	 * The sound isn't affected by position of the player (full volume and
	 * center balanced)
	 */
	public void playSoundFX(final String par1Str, float par2, final float par3) {
		if (SoundManager.loaded && options.soundVolume != 0.0F) {
			final SoundPoolEntry var4 = soundPoolSounds
					.getRandomSoundFromSoundPool(par1Str);

			if (var4 != null) {
				latestSoundID = (latestSoundID + 1) % 256;
				final String var5 = "sound_" + latestSoundID;
				SoundManager.sndSystem.newSource(false, var5, var4.soundUrl,
						var4.soundName, false, 0.0F, 0.0F, 0.0F, 0, 0.0F);

				if (par2 > 1.0F) {
					par2 = 1.0F;
				}

				par2 *= 0.25F;
				SoundManager.sndSystem.setPitch(var5, par3);
				SoundManager.sndSystem.setVolume(var5, par2
						* options.soundVolume);
				SoundManager.sndSystem.play(var5);
			}
		}
	}

	/**
	 * Pauses all currently playing sounds
	 */
	public void pauseAllSounds() {
		final Iterator var1 = playingSounds.iterator();

		while (var1.hasNext()) {
			final String var2 = (String) var1.next();
			SoundManager.sndSystem.pause(var2);
		}
	}

	/**
	 * Resumes playing all currently playing sounds (after pauseAllSounds)
	 */
	public void resumeAllSounds() {
		final Iterator var1 = playingSounds.iterator();

		while (var1.hasNext()) {
			final String var2 = (String) var1.next();
			SoundManager.sndSystem.play(var2);
		}
	}

	public void func_92071_g() {
		if (!field_92072_h.isEmpty()) {
			final Iterator var1 = field_92072_h.iterator();

			while (var1.hasNext()) {
				final ScheduledSound var2 = (ScheduledSound) var1.next();
				--var2.field_92064_g;

				if (var2.field_92064_g <= 0) {
					playSound(var2.field_92069_a, var2.field_92067_b,
							var2.field_92068_c, var2.field_92065_d,
							var2.field_92066_e, var2.field_92063_f);
					var1.remove();
				}
			}
		}
	}

	public void func_92070_a(final String par1Str, final float par2,
			final float par3, final float par4, final float par5,
			final float par6, final int par7) {
		field_92072_h.add(new ScheduledSound(par1Str, par2, par3, par4, par5,
				par6, par7));
	}
}
