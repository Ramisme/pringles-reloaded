package net.minecraft.src;

public class EntityAIWatchClosest extends EntityAIBase {
	private final EntityLiving theWatcher;

	/** The closest entity which is being watched by this one. */
	protected Entity closestEntity;
	private final float field_75333_c;
	private int lookTime;
	private final float field_75331_e;
	private final Class watchedClass;

	public EntityAIWatchClosest(final EntityLiving par1EntityLiving,
			final Class par2Class, final float par3) {
		theWatcher = par1EntityLiving;
		watchedClass = par2Class;
		field_75333_c = par3;
		field_75331_e = 0.02F;
		setMutexBits(2);
	}

	public EntityAIWatchClosest(final EntityLiving par1EntityLiving,
			final Class par2Class, final float par3, final float par4) {
		theWatcher = par1EntityLiving;
		watchedClass = par2Class;
		field_75333_c = par3;
		field_75331_e = par4;
		setMutexBits(2);
	}

	/**
	 * Returns whether the EntityAIBase should begin execution.
	 */
	@Override
	public boolean shouldExecute() {
		if (theWatcher.getRNG().nextFloat() >= field_75331_e) {
			return false;
		} else {
			if (watchedClass == EntityPlayer.class) {
				closestEntity = theWatcher.worldObj.getClosestPlayerToEntity(
						theWatcher, field_75333_c);
			} else {
				closestEntity = theWatcher.worldObj
						.findNearestEntityWithinAABB(watchedClass,
								theWatcher.boundingBox.expand(field_75333_c,
										3.0D, field_75333_c), theWatcher);
			}

			return closestEntity != null;
		}
	}

	/**
	 * Returns whether an in-progress EntityAIBase should continue executing
	 */
	@Override
	public boolean continueExecuting() {
		return !closestEntity.isEntityAlive() ? false : theWatcher
				.getDistanceSqToEntity(closestEntity) > field_75333_c
				* field_75333_c ? false : lookTime > 0;
	}

	/**
	 * Execute a one shot task or start executing a continuous task
	 */
	@Override
	public void startExecuting() {
		lookTime = 40 + theWatcher.getRNG().nextInt(40);
	}

	/**
	 * Resets the task
	 */
	@Override
	public void resetTask() {
		closestEntity = null;
	}

	/**
	 * Updates the task
	 */
	@Override
	public void updateTask() {
		theWatcher.getLookHelper().setLookPosition(closestEntity.posX,
				closestEntity.posY + closestEntity.getEyeHeight(),
				closestEntity.posZ, 10.0F, theWatcher.getVerticalFaceSpeed());
		--lookTime;
	}
}
