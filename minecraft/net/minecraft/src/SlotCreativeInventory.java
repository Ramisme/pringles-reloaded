package net.minecraft.src;

class SlotCreativeInventory extends Slot {
	private final Slot theSlot;

	final GuiContainerCreative theCreativeInventory;

	public SlotCreativeInventory(
			final GuiContainerCreative par1GuiContainerCreative,
			final Slot par2Slot, final int par3) {
		super(par2Slot.inventory, par3, 0, 0);
		theCreativeInventory = par1GuiContainerCreative;
		theSlot = par2Slot;
	}

	@Override
	public void onPickupFromSlot(final EntityPlayer par1EntityPlayer,
			final ItemStack par2ItemStack) {
		theSlot.onPickupFromSlot(par1EntityPlayer, par2ItemStack);
	}

	/**
	 * Check if the stack is a valid item for this slot. Always true beside for
	 * the armor slots.
	 */
	@Override
	public boolean isItemValid(final ItemStack par1ItemStack) {
		return theSlot.isItemValid(par1ItemStack);
	}

	/**
	 * Helper fnct to get the stack in the slot.
	 */
	@Override
	public ItemStack getStack() {
		return theSlot.getStack();
	}

	/**
	 * Returns if this slot contains a stack.
	 */
	@Override
	public boolean getHasStack() {
		return theSlot.getHasStack();
	}

	/**
	 * Helper method to put a stack in the slot.
	 */
	@Override
	public void putStack(final ItemStack par1ItemStack) {
		theSlot.putStack(par1ItemStack);
	}

	/**
	 * Called when the stack in a Slot changes
	 */
	@Override
	public void onSlotChanged() {
		theSlot.onSlotChanged();
	}

	/**
	 * Returns the maximum stack size for a given slot (usually the same as
	 * getInventoryStackLimit(), but 1 in the case of armor slots)
	 */
	@Override
	public int getSlotStackLimit() {
		return theSlot.getSlotStackLimit();
	}

	/**
	 * Returns the icon index on items.png that is used as background image of
	 * the slot.
	 */
	@Override
	public Icon getBackgroundIconIndex() {
		return theSlot.getBackgroundIconIndex();
	}

	/**
	 * Decrease the size of the stack in slot (first int arg) by the amount of
	 * the second int arg. Returns the new stack.
	 */
	@Override
	public ItemStack decrStackSize(final int par1) {
		return theSlot.decrStackSize(par1);
	}

	/**
	 * returns true if this slot is in par2 of par1
	 */
	@Override
	public boolean isSlotInInventory(final IInventory par1IInventory,
			final int par2) {
		return theSlot.isSlotInInventory(par1IInventory, par2);
	}

	static Slot func_75240_a(
			final SlotCreativeInventory par0SlotCreativeInventory) {
		return par0SlotCreativeInventory.theSlot;
	}
}
