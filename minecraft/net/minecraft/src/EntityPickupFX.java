package net.minecraft.src;

import org.lwjgl.opengl.GL11;

public class EntityPickupFX extends EntityFX {
	private final Entity entityToPickUp;
	private final Entity entityPickingUp;
	private int age = 0;
	private int maxAge = 0;

	/** renamed from yOffset to fix shadowing Entity.yOffset */
	private final float yOffs;

	public EntityPickupFX(final World par1World, final Entity par2Entity,
			final Entity par3Entity, final float par4) {
		super(par1World, par2Entity.posX, par2Entity.posY, par2Entity.posZ,
				par2Entity.motionX, par2Entity.motionY, par2Entity.motionZ);
		entityToPickUp = par2Entity;
		entityPickingUp = par3Entity;
		maxAge = 3;
		yOffs = par4;
	}

	@Override
	public void renderParticle(final Tessellator par1Tessellator,
			final float par2, final float par3, final float par4,
			final float par5, final float par6, final float par7) {
		float var8 = (age + par2) / maxAge;
		var8 *= var8;
		final double var9 = entityToPickUp.posX;
		final double var11 = entityToPickUp.posY;
		final double var13 = entityToPickUp.posZ;
		final double var15 = entityPickingUp.lastTickPosX
				+ (entityPickingUp.posX - entityPickingUp.lastTickPosX) * par2;
		final double var17 = entityPickingUp.lastTickPosY
				+ (entityPickingUp.posY - entityPickingUp.lastTickPosY) * par2
				+ yOffs;
		final double var19 = entityPickingUp.lastTickPosZ
				+ (entityPickingUp.posZ - entityPickingUp.lastTickPosZ) * par2;
		double var21 = var9 + (var15 - var9) * var8;
		double var23 = var11 + (var17 - var11) * var8;
		double var25 = var13 + (var19 - var13) * var8;
		MathHelper.floor_double(var21);
		MathHelper.floor_double(var23 + yOffset / 2.0F);
		MathHelper.floor_double(var25);
		final int var30 = getBrightnessForRender(par2);
		final int var31 = var30 % 65536;
		final int var32 = var30 / 65536;
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit,
				var31 / 1.0F, var32 / 1.0F);
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		var21 -= EntityFX.interpPosX;
		var23 -= EntityFX.interpPosY;
		var25 -= EntityFX.interpPosZ;
		RenderManager.instance.renderEntityWithPosYaw(entityToPickUp,
				(float) var21, (float) var23, (float) var25,
				entityToPickUp.rotationYaw, par2);
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate() {
		++age;

		if (age == maxAge) {
			setDead();
		}
	}

	@Override
	public int getFXLayer() {
		return 3;
	}
}
