package net.minecraft.src;

public abstract class EntityFlying extends EntityLiving {
	public EntityFlying(final World par1World) {
		super(par1World);
	}

	/**
	 * Called when the mob is falling. Calculates and applies fall damage.
	 */
	@Override
	protected void fall(final float par1) {
	}

	/**
	 * Takes in the distance the entity has fallen this tick and whether its on
	 * the ground to update the fall distance and deal fall damage if landing on
	 * the ground. Args: distanceFallenThisTick, onGround
	 */
	@Override
	protected void updateFallState(final double par1, final boolean par3) {
	}

	/**
	 * Moves the entity based on the specified heading. Args: strafe, forward
	 */
	@Override
	public void moveEntityWithHeading(final float par1, final float par2) {
		if (isInWater()) {
			moveFlying(par1, par2, 0.02F);
			moveEntity(motionX, motionY, motionZ);
			motionX *= 0.800000011920929D;
			motionY *= 0.800000011920929D;
			motionZ *= 0.800000011920929D;
		} else if (handleLavaMovement()) {
			moveFlying(par1, par2, 0.02F);
			moveEntity(motionX, motionY, motionZ);
			motionX *= 0.5D;
			motionY *= 0.5D;
			motionZ *= 0.5D;
		} else {
			float var3 = 0.91F;

			if (onGround) {
				var3 = 0.54600006F;
				final int var4 = worldObj.getBlockId(
						MathHelper.floor_double(posX),
						MathHelper.floor_double(boundingBox.minY) - 1,
						MathHelper.floor_double(posZ));

				if (var4 > 0) {
					var3 = Block.blocksList[var4].slipperiness * 0.91F;
				}
			}

			final float var8 = 0.16277136F / (var3 * var3 * var3);
			moveFlying(par1, par2, onGround ? 0.1F * var8 : 0.02F);
			var3 = 0.91F;

			if (onGround) {
				var3 = 0.54600006F;
				final int var5 = worldObj.getBlockId(
						MathHelper.floor_double(posX),
						MathHelper.floor_double(boundingBox.minY) - 1,
						MathHelper.floor_double(posZ));

				if (var5 > 0) {
					var3 = Block.blocksList[var5].slipperiness * 0.91F;
				}
			}

			moveEntity(motionX, motionY, motionZ);
			motionX *= var3;
			motionY *= var3;
			motionZ *= var3;
		}

		prevLimbYaw = limbYaw;
		final double var10 = posX - prevPosX;
		final double var9 = posZ - prevPosZ;
		float var7 = MathHelper.sqrt_double(var10 * var10 + var9 * var9) * 4.0F;

		if (var7 > 1.0F) {
			var7 = 1.0F;
		}

		limbYaw += (var7 - limbYaw) * 0.4F;
		limbSwing += limbYaw;
	}

	/**
	 * returns true if this entity is by a ladder, false otherwise
	 */
	@Override
	public boolean isOnLadder() {
		return false;
	}
}
