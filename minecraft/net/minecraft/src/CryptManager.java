package net.minecraft.src;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.crypto.BufferedBlockCipher;
import org.bouncycastle.crypto.CipherKeyGenerator;
import org.bouncycastle.crypto.KeyGenerationParameters;
import org.bouncycastle.crypto.engines.AESFastEngine;
import org.bouncycastle.crypto.io.CipherInputStream;
import org.bouncycastle.crypto.io.CipherOutputStream;
import org.bouncycastle.crypto.modes.CFBBlockCipher;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class CryptManager {
	/** ISO_8859_1 */
	public static final Charset charSet = Charset.forName("ISO_8859_1");

	/**
	 * Generate a new shared secret AES key from a secure random source
	 */
	public static SecretKey createNewSharedKey() {
		final CipherKeyGenerator var0 = new CipherKeyGenerator();
		var0.init(new KeyGenerationParameters(new SecureRandom(), 128));
		return new SecretKeySpec(var0.generateKey(), "AES");
	}

	public static KeyPair createNewKeyPair() {
		try {
			final KeyPairGenerator var0 = KeyPairGenerator.getInstance("RSA");
			var0.initialize(1024);
			return var0.generateKeyPair();
		} catch (final NoSuchAlgorithmException var1) {
			var1.printStackTrace();
			System.err.println("Key pair generation failed!");
			return null;
		}
	}

	/**
	 * Compute a serverId hash for use by sendSessionRequest()
	 */
	public static byte[] getServerIdHash(final String par0Str,
			final PublicKey par1PublicKey, final SecretKey par2SecretKey) {
		try {
			return CryptManager.digestOperation(
					"SHA-1",
					new byte[][] { par0Str.getBytes("ISO_8859_1"),
							par2SecretKey.getEncoded(),
							par1PublicKey.getEncoded() });
		} catch (final UnsupportedEncodingException var4) {
			var4.printStackTrace();
			return null;
		}
	}

	/**
	 * Compute a message digest on arbitrary byte[] data
	 */
	private static byte[] digestOperation(final String par0Str,
			final byte[]... par1ArrayOfByte) {
		try {
			final MessageDigest var2 = MessageDigest.getInstance(par0Str);
			final byte[][] var3 = par1ArrayOfByte;
			final int var4 = par1ArrayOfByte.length;

			for (int var5 = 0; var5 < var4; ++var5) {
				final byte[] var6 = var3[var5];
				var2.update(var6);
			}

			return var2.digest();
		} catch (final NoSuchAlgorithmException var7) {
			var7.printStackTrace();
			return null;
		}
	}

	/**
	 * Create a new PublicKey from encoded X.509 data
	 */
	public static PublicKey decodePublicKey(final byte[] par0ArrayOfByte) {
		try {
			final X509EncodedKeySpec var1 = new X509EncodedKeySpec(
					par0ArrayOfByte);
			final KeyFactory var2 = KeyFactory.getInstance("RSA");
			return var2.generatePublic(var1);
		} catch (final NoSuchAlgorithmException var3) {
			var3.printStackTrace();
		} catch (final InvalidKeySpecException var4) {
			var4.printStackTrace();
		}

		System.err.println("Public key reconstitute failed!");
		return null;
	}

	/**
	 * Decrypt shared secret AES key using RSA private key
	 */
	public static SecretKey decryptSharedKey(final PrivateKey par0PrivateKey,
			final byte[] par1ArrayOfByte) {
		return new SecretKeySpec(CryptManager.decryptData(par0PrivateKey,
				par1ArrayOfByte), "AES");
	}

	/**
	 * Encrypt byte[] data with RSA public key
	 */
	public static byte[] encryptData(final Key par0Key,
			final byte[] par1ArrayOfByte) {
		return CryptManager.cipherOperation(1, par0Key, par1ArrayOfByte);
	}

	/**
	 * Decrypt byte[] data with RSA private key
	 */
	public static byte[] decryptData(final Key par0Key,
			final byte[] par1ArrayOfByte) {
		return CryptManager.cipherOperation(2, par0Key, par1ArrayOfByte);
	}

	/**
	 * Encrypt or decrypt byte[] data using the specified key
	 */
	private static byte[] cipherOperation(final int par0, final Key par1Key,
			final byte[] par2ArrayOfByte) {
		try {
			return CryptManager.createTheCipherInstance(par0,
					par1Key.getAlgorithm(), par1Key).doFinal(par2ArrayOfByte);
		} catch (final IllegalBlockSizeException var4) {
			var4.printStackTrace();
		} catch (final BadPaddingException var5) {
			var5.printStackTrace();
		}

		System.err.println("Cipher data failed!");
		return null;
	}

	/**
	 * Creates the Cipher Instance.
	 */
	private static Cipher createTheCipherInstance(final int par0,
			final String par1Str, final Key par2Key) {
		try {
			final Cipher var3 = Cipher.getInstance(par1Str);
			var3.init(par0, par2Key);
			return var3;
		} catch (final InvalidKeyException var4) {
			var4.printStackTrace();
		} catch (final NoSuchAlgorithmException var5) {
			var5.printStackTrace();
		} catch (final NoSuchPaddingException var6) {
			var6.printStackTrace();
		}

		System.err.println("Cipher creation failed!");
		return null;
	}

	/**
	 * Create a new BufferedBlockCipher instance
	 */
	private static BufferedBlockCipher createBufferedBlockCipher(
			final boolean par0, final Key par1Key) {
		final BufferedBlockCipher var2 = new BufferedBlockCipher(
				new CFBBlockCipher(new AESFastEngine(), 8));
		var2.init(par0,
				new ParametersWithIV(new KeyParameter(par1Key.getEncoded()),
						par1Key.getEncoded(), 0, 16));
		return var2;
	}

	public static OutputStream encryptOuputStream(
			final SecretKey par0SecretKey, final OutputStream par1OutputStream) {
		return new CipherOutputStream(par1OutputStream,
				CryptManager.createBufferedBlockCipher(true, par0SecretKey));
	}

	public static InputStream decryptInputStream(final SecretKey par0SecretKey,
			final InputStream par1InputStream) {
		return new CipherInputStream(par1InputStream,
				CryptManager.createBufferedBlockCipher(false, par0SecretKey));
	}

	static {
		Security.addProvider(new BouncyCastleProvider());
	}
}
