package net.minecraft.src;

public class PlayerCapabilities {
	/** Disables player damage. */
	public boolean disableDamage = false;

	/** Sets/indicates whether the player is flying. */
	public boolean isFlying = false;

	/** whether or not to allow the player to fly when they double jump. */
	public boolean allowFlying = false;

	/**
	 * Used to determine if creative mode is enabled, and therefore if items
	 * should be depleted on usage
	 */
	public boolean isCreativeMode = false;

	/** Indicates whether the player is allowed to modify the surroundings */
	public boolean allowEdit = true;
	private float flySpeed = 0.05F;
	private float walkSpeed = 0.1F;

	public void writeCapabilitiesToNBT(final NBTTagCompound par1NBTTagCompound) {
		final NBTTagCompound var2 = new NBTTagCompound();
		var2.setBoolean("invulnerable", disableDamage);
		var2.setBoolean("flying", isFlying);
		var2.setBoolean("mayfly", allowFlying);
		var2.setBoolean("instabuild", isCreativeMode);
		var2.setBoolean("mayBuild", allowEdit);
		var2.setFloat("flySpeed", flySpeed);
		var2.setFloat("walkSpeed", walkSpeed);
		par1NBTTagCompound.setTag("abilities", var2);
	}

	public void readCapabilitiesFromNBT(final NBTTagCompound par1NBTTagCompound) {
		if (par1NBTTagCompound.hasKey("abilities")) {
			final NBTTagCompound var2 = par1NBTTagCompound
					.getCompoundTag("abilities");
			disableDamage = var2.getBoolean("invulnerable");
			isFlying = var2.getBoolean("flying");
			allowFlying = var2.getBoolean("mayfly");
			isCreativeMode = var2.getBoolean("instabuild");

			if (var2.hasKey("flySpeed")) {
				flySpeed = var2.getFloat("flySpeed");
				walkSpeed = var2.getFloat("walkSpeed");
			}

			if (var2.hasKey("mayBuild")) {
				allowEdit = var2.getBoolean("mayBuild");
			}
		}
	}

	public float getFlySpeed() {
		return flySpeed;
	}

	public void setFlySpeed(final float par1) {
		flySpeed = par1;
	}

	public float getWalkSpeed() {
		return walkSpeed;
	}

	public void setPlayerWalkSpeed(final float par1) {
		walkSpeed = par1;
	}
}
