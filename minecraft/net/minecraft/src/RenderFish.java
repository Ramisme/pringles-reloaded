package net.minecraft.src;

import net.minecraft.client.Minecraft;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

public class RenderFish extends Render {
	/**
	 * Actually renders the fishing line and hook
	 */
	public void doRenderFishHook(final EntityFishHook par1EntityFishHook,
			final double par2, final double par4, final double par6,
			final float par8, final float par9) {
		GL11.glPushMatrix();
		GL11.glTranslatef((float) par2, (float) par4, (float) par6);
		GL11.glEnable(GL12.GL_RESCALE_NORMAL);
		GL11.glScalef(0.5F, 0.5F, 0.5F);
		final byte var10 = 1;
		final byte var11 = 2;
		loadTexture("/particles.png");
		final Tessellator var12 = Tessellator.instance;
		final float var13 = (var10 * 8 + 0) / 128.0F;
		final float var14 = (var10 * 8 + 8) / 128.0F;
		final float var15 = (var11 * 8 + 0) / 128.0F;
		final float var16 = (var11 * 8 + 8) / 128.0F;
		final float var17 = 1.0F;
		final float var18 = 0.5F;
		final float var19 = 0.5F;
		GL11.glRotatef(180.0F - renderManager.playerViewY, 0.0F, 1.0F, 0.0F);
		GL11.glRotatef(-renderManager.playerViewX, 1.0F, 0.0F, 0.0F);
		var12.startDrawingQuads();
		var12.setNormal(0.0F, 1.0F, 0.0F);
		var12.addVertexWithUV(0.0F - var18, 0.0F - var19, 0.0D, var13, var16);
		var12.addVertexWithUV(var17 - var18, 0.0F - var19, 0.0D, var14, var16);
		var12.addVertexWithUV(var17 - var18, 1.0F - var19, 0.0D, var14, var15);
		var12.addVertexWithUV(0.0F - var18, 1.0F - var19, 0.0D, var13, var15);
		var12.draw();
		GL11.glDisable(GL12.GL_RESCALE_NORMAL);
		GL11.glPopMatrix();

		if (par1EntityFishHook.angler != null) {
			final float var20 = par1EntityFishHook.angler
					.getSwingProgress(par9);
			final float var21 = MathHelper.sin(MathHelper.sqrt_float(var20)
					* (float) Math.PI);
			final Vec3 var22 = par1EntityFishHook.worldObj.getWorldVec3Pool()
					.getVecFromPool(-0.5D, 0.03D, 0.8D);
			var22.rotateAroundX(-(par1EntityFishHook.angler.prevRotationPitch + (par1EntityFishHook.angler.rotationPitch - par1EntityFishHook.angler.prevRotationPitch)
					* par9)
					* (float) Math.PI / 180.0F);
			var22.rotateAroundY(-(par1EntityFishHook.angler.prevRotationYaw + (par1EntityFishHook.angler.rotationYaw - par1EntityFishHook.angler.prevRotationYaw)
					* par9)
					* (float) Math.PI / 180.0F);
			var22.rotateAroundY(var21 * 0.5F);
			var22.rotateAroundX(-var21 * 0.7F);
			double var23 = par1EntityFishHook.angler.prevPosX
					+ (par1EntityFishHook.angler.posX - par1EntityFishHook.angler.prevPosX)
					* par9 + var22.xCoord;
			double var25 = par1EntityFishHook.angler.prevPosY
					+ (par1EntityFishHook.angler.posY - par1EntityFishHook.angler.prevPosY)
					* par9 + var22.yCoord;
			double var27 = par1EntityFishHook.angler.prevPosZ
					+ (par1EntityFishHook.angler.posZ - par1EntityFishHook.angler.prevPosZ)
					* par9 + var22.zCoord;
			final double var29 = par1EntityFishHook.angler != Minecraft
					.getMinecraft().thePlayer ? (double) par1EntityFishHook.angler
					.getEyeHeight() : 0.0D;

			if (renderManager.options.thirdPersonView > 0
					|| par1EntityFishHook.angler != Minecraft.getMinecraft().thePlayer) {
				final float var31 = (par1EntityFishHook.angler.prevRenderYawOffset + (par1EntityFishHook.angler.renderYawOffset - par1EntityFishHook.angler.prevRenderYawOffset)
						* par9)
						* (float) Math.PI / 180.0F;
				final double var32 = MathHelper.sin(var31);
				final double var34 = MathHelper.cos(var31);
				var23 = par1EntityFishHook.angler.prevPosX
						+ (par1EntityFishHook.angler.posX - par1EntityFishHook.angler.prevPosX)
						* par9 - var34 * 0.35D - var32 * 0.85D;
				var25 = par1EntityFishHook.angler.prevPosY
						+ var29
						+ (par1EntityFishHook.angler.posY - par1EntityFishHook.angler.prevPosY)
						* par9 - 0.45D;
				var27 = par1EntityFishHook.angler.prevPosZ
						+ (par1EntityFishHook.angler.posZ - par1EntityFishHook.angler.prevPosZ)
						* par9 - var32 * 0.35D + var34 * 0.85D;
			}

			final double var46 = par1EntityFishHook.prevPosX
					+ (par1EntityFishHook.posX - par1EntityFishHook.prevPosX)
					* par9;
			final double var33 = par1EntityFishHook.prevPosY
					+ (par1EntityFishHook.posY - par1EntityFishHook.prevPosY)
					* par9 + 0.25D;
			final double var35 = par1EntityFishHook.prevPosZ
					+ (par1EntityFishHook.posZ - par1EntityFishHook.prevPosZ)
					* par9;
			final double var37 = (float) (var23 - var46);
			final double var39 = (float) (var25 - var33);
			final double var41 = (float) (var27 - var35);
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			GL11.glDisable(GL11.GL_LIGHTING);
			var12.startDrawing(3);
			var12.setColorOpaque_I(0);
			final byte var43 = 16;

			for (int var44 = 0; var44 <= var43; ++var44) {
				final float var45 = (float) var44 / (float) var43;
				var12.addVertex(par2 + var37 * var45, par4 + var39
						* (var45 * var45 + var45) * 0.5D + 0.25D, par6 + var41
						* var45);
			}

			var12.draw();
			GL11.glEnable(GL11.GL_LIGHTING);
			GL11.glEnable(GL11.GL_TEXTURE_2D);
		}
	}

	/**
	 * Actually renders the given argument. This is a synthetic bridge method,
	 * always casting down its argument and then handing it off to a worker
	 * function which does the actual work. In all probabilty, the class Render
	 * is generic (Render<T extends Entity) and this method has signature public
	 * void doRender(T entity, double d, double d1, double d2, float f, float
	 * f1). But JAD is pre 1.5 so doesn't do that.
	 */
	@Override
	public void doRender(final Entity par1Entity, final double par2,
			final double par4, final double par6, final float par8,
			final float par9) {
		doRenderFishHook((EntityFishHook) par1Entity, par2, par4, par6, par8,
				par9);
	}
}
