package net.minecraft.src;

public class EnchantmentProtection extends Enchantment {
	/** Holds the name to be translated of each protection type. */
	private static final String[] protectionName = new String[] { "all",
			"fire", "fall", "explosion", "projectile" };

	/**
	 * Holds the base factor of enchantability needed to be able to use the
	 * enchant.
	 */
	private static final int[] baseEnchantability = new int[] { 1, 10, 5, 5, 3 };

	/**
	 * Holds how much each level increased the enchantability factor to be able
	 * to use this enchant.
	 */
	private static final int[] levelEnchantability = new int[] { 11, 8, 6, 8, 6 };

	/**
	 * Used on the formula of base enchantability, this is the 'window' factor
	 * of values to be able to use thing enchant.
	 */
	private static final int[] thresholdEnchantability = new int[] { 20, 12,
			10, 12, 15 };

	/**
	 * Defines the type of protection of the enchantment, 0 = all, 1 = fire, 2 =
	 * fall (feather fall), 3 = explosion and 4 = projectile.
	 */
	public final int protectionType;

	public EnchantmentProtection(final int par1, final int par2, final int par3) {
		super(par1, par2, EnumEnchantmentType.armor);
		protectionType = par3;

		if (par3 == 2) {
			type = EnumEnchantmentType.armor_feet;
		}
	}

	/**
	 * Returns the minimal value of enchantability needed on the enchantment
	 * level passed.
	 */
	@Override
	public int getMinEnchantability(final int par1) {
		return EnchantmentProtection.baseEnchantability[protectionType]
				+ (par1 - 1)
				* EnchantmentProtection.levelEnchantability[protectionType];
	}

	/**
	 * Returns the maximum value of enchantability nedded on the enchantment
	 * level passed.
	 */
	@Override
	public int getMaxEnchantability(final int par1) {
		return getMinEnchantability(par1)
				+ EnchantmentProtection.thresholdEnchantability[protectionType];
	}

	/**
	 * Returns the maximum level that the enchantment can have.
	 */
	@Override
	public int getMaxLevel() {
		return 4;
	}

	/**
	 * Calculates de damage protection of the enchantment based on level and
	 * damage source passed.
	 */
	@Override
	public int calcModifierDamage(final int par1,
			final DamageSource par2DamageSource) {
		if (par2DamageSource.canHarmInCreative()) {
			return 0;
		} else {
			final float var3 = (6 + par1 * par1) / 3.0F;
			return protectionType == 0 ? MathHelper.floor_float(var3 * 0.75F)
					: protectionType == 1 && par2DamageSource.isFireDamage() ? MathHelper
							.floor_float(var3 * 1.25F)
							: protectionType == 2
									&& par2DamageSource == DamageSource.fall ? MathHelper
									.floor_float(var3 * 2.5F)
									: protectionType == 3
											&& par2DamageSource.isExplosion() ? MathHelper
											.floor_float(var3 * 1.5F)
											: protectionType == 4
													&& par2DamageSource
															.isProjectile() ? MathHelper
													.floor_float(var3 * 1.5F)
													: 0;
		}
	}

	/**
	 * Return the name of key in translation table of this enchantment.
	 */
	@Override
	public String getName() {
		return "enchantment.protect."
				+ EnchantmentProtection.protectionName[protectionType];
	}

	/**
	 * Determines if the enchantment passed can be applyied together with this
	 * enchantment.
	 */
	@Override
	public boolean canApplyTogether(final Enchantment par1Enchantment) {
		if (par1Enchantment instanceof EnchantmentProtection) {
			final EnchantmentProtection var2 = (EnchantmentProtection) par1Enchantment;
			return var2.protectionType == protectionType ? false
					: protectionType == 2 || var2.protectionType == 2;
		} else {
			return super.canApplyTogether(par1Enchantment);
		}
	}

	public static int func_92093_a(final Entity par0Entity, int par1) {
		final int var2 = EnchantmentHelper.getMaxEnchantmentLevel(
				Enchantment.fireProtection.effectId,
				par0Entity.getLastActiveItems());

		if (var2 > 0) {
			par1 -= MathHelper.floor_float((float) par1 * (float) var2 * 0.15F);
		}

		return par1;
	}

	public static double func_92092_a(final Entity par0Entity, double par1) {
		final int var3 = EnchantmentHelper.getMaxEnchantmentLevel(
				Enchantment.blastProtection.effectId,
				par0Entity.getLastActiveItems());

		if (var3 > 0) {
			par1 -= MathHelper.floor_double(par1 * (var3 * 0.15F));
		}

		return par1;
	}
}
