package net.minecraft.src;

import java.util.ArrayList;

public class CompactArrayList {
	private ArrayList list;
	private int initialCapacity;
	private float loadFactor;
	private int countValid;

	public CompactArrayList() {
		this(10, 0.75F);
	}

	public CompactArrayList(final int var1) {
		this(var1, 0.75F);
	}

	public CompactArrayList(final int var1, final float var2) {
		list = null;
		initialCapacity = 0;
		loadFactor = 1.0F;
		countValid = 0;
		list = new ArrayList(var1);
		initialCapacity = var1;
		loadFactor = var2;
	}

	public void add(final int var1, final Object var2) {
		if (var2 != null) {
			++countValid;
		}

		list.add(var1, var2);
	}

	public boolean add(final Object var1) {
		if (var1 != null) {
			++countValid;
		}

		return list.add(var1);
	}

	public Object set(final int var1, final Object var2) {
		final Object var3 = list.set(var1, var2);

		if (var2 != var3) {
			if (var3 == null) {
				++countValid;
			}

			if (var2 == null) {
				--countValid;
			}
		}

		return var3;
	}

	public Object remove(final int var1) {
		final Object var2 = list.remove(var1);

		if (var2 != null) {
			--countValid;
		}

		return var2;
	}

	public void clear() {
		list.clear();
		countValid = 0;
	}

	public void compact() {
		if (countValid <= 0 && list.size() <= 0) {
			clear();
		} else if (list.size() > initialCapacity) {
			final float var1 = countValid * 1.0F / list.size();

			if (var1 <= loadFactor) {
				int var2 = 0;
				int var3;

				for (var3 = 0; var3 < list.size(); ++var3) {
					final Object var4 = list.get(var3);

					if (var4 != null) {
						if (var3 != var2) {
							list.set(var2, var4);
						}

						++var2;
					}
				}

				for (var3 = list.size() - 1; var3 >= var2; --var3) {
					list.remove(var3);
				}
			}
		}
	}

	public boolean contains(final Object var1) {
		return list.contains(var1);
	}

	public Object get(final int var1) {
		return list.get(var1);
	}

	public boolean isEmpty() {
		return list.isEmpty();
	}

	public int size() {
		return list.size();
	}

	public int getCountValid() {
		return countValid;
	}
}
