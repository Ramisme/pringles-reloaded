package net.minecraft.src;

public class EntityAITradePlayer extends EntityAIBase {
	private final EntityVillager villager;

	public EntityAITradePlayer(final EntityVillager par1EntityVillager) {
		villager = par1EntityVillager;
		setMutexBits(5);
	}

	/**
	 * Returns whether the EntityAIBase should begin execution.
	 */
	@Override
	public boolean shouldExecute() {
		if (!villager.isEntityAlive()) {
			return false;
		} else if (villager.isInWater()) {
			return false;
		} else if (!villager.onGround) {
			return false;
		} else if (villager.velocityChanged) {
			return false;
		} else {
			final EntityPlayer var1 = villager.getCustomer();
			return var1 == null ? false
					: villager.getDistanceSqToEntity(var1) > 16.0D ? false
							: var1.openContainer instanceof Container;
		}
	}

	/**
	 * Execute a one shot task or start executing a continuous task
	 */
	@Override
	public void startExecuting() {
		villager.getNavigator().clearPathEntity();
	}

	/**
	 * Resets the task
	 */
	@Override
	public void resetTask() {
		villager.setCustomer((EntityPlayer) null);
	}
}
