package net.minecraft.src;

public class EntityAILeapAtTarget extends EntityAIBase {
	/** The entity that is leaping. */
	EntityLiving leaper;

	/** The entity that the leaper is leaping towards. */
	EntityLiving leapTarget;

	/** The entity's motionY after leaping. */
	float leapMotionY;

	public EntityAILeapAtTarget(final EntityLiving par1EntityLiving,
			final float par2) {
		leaper = par1EntityLiving;
		leapMotionY = par2;
		setMutexBits(5);
	}

	/**
	 * Returns whether the EntityAIBase should begin execution.
	 */
	@Override
	public boolean shouldExecute() {
		leapTarget = leaper.getAttackTarget();

		if (leapTarget == null) {
			return false;
		} else {
			final double var1 = leaper.getDistanceSqToEntity(leapTarget);
			return var1 >= 4.0D && var1 <= 16.0D ? !leaper.onGround ? false
					: leaper.getRNG().nextInt(5) == 0 : false;
		}
	}

	/**
	 * Returns whether an in-progress EntityAIBase should continue executing
	 */
	@Override
	public boolean continueExecuting() {
		return !leaper.onGround;
	}

	/**
	 * Execute a one shot task or start executing a continuous task
	 */
	@Override
	public void startExecuting() {
		final double var1 = leapTarget.posX - leaper.posX;
		final double var3 = leapTarget.posZ - leaper.posZ;
		final float var5 = MathHelper.sqrt_double(var1 * var1 + var3 * var3);
		leaper.motionX += var1 / var5 * 0.5D * 0.800000011920929D
				+ leaper.motionX * 0.20000000298023224D;
		leaper.motionZ += var3 / var5 * 0.5D * 0.800000011920929D
				+ leaper.motionZ * 0.20000000298023224D;
		leaper.motionY = leapMotionY;
	}
}
