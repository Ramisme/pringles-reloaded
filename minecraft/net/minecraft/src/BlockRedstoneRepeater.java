package net.minecraft.src;

import java.util.Random;

public class BlockRedstoneRepeater extends BlockRedstoneLogic {
	/** The offsets for the two torches in redstone repeater blocks. */
	public static final double[] repeaterTorchOffset = new double[] { -0.0625D,
			0.0625D, 0.1875D, 0.3125D };

	/** The states in which the redstone repeater blocks can be. */
	private static final int[] repeaterState = new int[] { 1, 2, 3, 4 };

	protected BlockRedstoneRepeater(final int par1, final boolean par2) {
		super(par1, par2);
	}

	/**
	 * Called upon block activation (right click on the block.)
	 */
	@Override
	public boolean onBlockActivated(final World par1World, final int par2,
			final int par3, final int par4,
			final EntityPlayer par5EntityPlayer, final int par6,
			final float par7, final float par8, final float par9) {
		final int var10 = par1World.getBlockMetadata(par2, par3, par4);
		int var11 = (var10 & 12) >> 2;
		var11 = var11 + 1 << 2 & 12;
		par1World.setBlockMetadataWithNotify(par2, par3, par4, var11 | var10
				& 3, 3);
		return true;
	}

	@Override
	protected int func_94481_j_(final int par1) {
		return BlockRedstoneRepeater.repeaterState[(par1 & 12) >> 2] * 2;
	}

	@Override
	protected BlockRedstoneLogic func_94485_e() {
		return Block.redstoneRepeaterActive;
	}

	@Override
	protected BlockRedstoneLogic func_94484_i() {
		return Block.redstoneRepeaterIdle;
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return Item.redstoneRepeater.itemID;
	}

	/**
	 * only called by clickMiddleMouseButton , and passed to
	 * inventory.setCurrentItem (along with isCreative)
	 */
	@Override
	public int idPicked(final World par1World, final int par2, final int par3,
			final int par4) {
		return Item.redstoneRepeater.itemID;
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return 15;
	}

	@Override
	public boolean func_94476_e(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		return func_94482_f(par1IBlockAccess, par2, par3, par4, par5) > 0;
	}

	@Override
	protected boolean func_94477_d(final int par1) {
		return BlockRedstoneLogic.isRedstoneRepeaterBlockID(par1);
	}

	/**
	 * A randomly called display update to be able to add particles or other
	 * items for display
	 */
	@Override
	public void randomDisplayTick(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		if (isRepeaterPowered) {
			final int var6 = par1World.getBlockMetadata(par2, par3, par4);
			final int var7 = BlockDirectional.getDirection(var6);
			final double var8 = par2 + 0.5F + (par5Random.nextFloat() - 0.5F)
					* 0.2D;
			final double var10 = par3 + 0.4F + (par5Random.nextFloat() - 0.5F)
					* 0.2D;
			final double var12 = par4 + 0.5F + (par5Random.nextFloat() - 0.5F)
					* 0.2D;
			double var14 = 0.0D;
			double var16 = 0.0D;

			if (par5Random.nextInt(2) == 0) {
				switch (var7) {
				case 0:
					var16 = -0.3125D;
					break;

				case 1:
					var14 = 0.3125D;
					break;

				case 2:
					var16 = 0.3125D;
					break;

				case 3:
					var14 = -0.3125D;
				}
			} else {
				final int var18 = (var6 & 12) >> 2;

				switch (var7) {
				case 0:
					var16 = BlockRedstoneRepeater.repeaterTorchOffset[var18];
					break;

				case 1:
					var14 = -BlockRedstoneRepeater.repeaterTorchOffset[var18];
					break;

				case 2:
					var16 = -BlockRedstoneRepeater.repeaterTorchOffset[var18];
					break;

				case 3:
					var14 = BlockRedstoneRepeater.repeaterTorchOffset[var18];
				}
			}

			par1World.spawnParticle("reddust", var8 + var14, var10, var12
					+ var16, 0.0D, 0.0D, 0.0D);
		}
	}

	/**
	 * ejects contained items into the world, and notifies neighbours of an
	 * update, as appropriate
	 */
	@Override
	public void breakBlock(final World par1World, final int par2,
			final int par3, final int par4, final int par5, final int par6) {
		super.breakBlock(par1World, par2, par3, par4, par5, par6);
		func_94483_i_(par1World, par2, par3, par4);
	}
}
