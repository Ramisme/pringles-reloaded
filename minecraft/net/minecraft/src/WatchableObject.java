package net.minecraft.src;

public class WatchableObject {
	private final int objectType;

	/** id of max 31 */
	private final int dataValueId;
	private Object watchedObject;
	private boolean watched;

	public WatchableObject(final int par1, final int par2, final Object par3Obj) {
		dataValueId = par2;
		watchedObject = par3Obj;
		objectType = par1;
		watched = true;
	}

	public int getDataValueId() {
		return dataValueId;
	}

	public void setObject(final Object par1Obj) {
		watchedObject = par1Obj;
	}

	public Object getObject() {
		return watchedObject;
	}

	public int getObjectType() {
		return objectType;
	}

	public boolean isWatched() {
		return watched;
	}

	public void setWatched(final boolean par1) {
		watched = par1;
	}

	/**
	 * Set whether the specified watchable object is being watched.
	 */
	static boolean setWatchableObjectWatched(
			final WatchableObject par0WatchableObject, final boolean par1) {
		return par0WatchableObject.watched = par1;
	}
}
