package net.minecraft.src;

public class DestroyBlockProgress {
	private final int partialBlockX;
	private final int partialBlockY;
	private final int partialBlockZ;

	/**
	 * damage ranges from 1 to 10. -1 causes the client to delete the partial
	 * block renderer.
	 */
	private int partialBlockProgress;

	/**
	 * keeps track of how many ticks this PartiallyDestroyedBlock already exists
	 */
	private int createdAtCloudUpdateTick;

	public DestroyBlockProgress(final int par1, final int par2, final int par3,
			final int par4) {
		partialBlockX = par2;
		partialBlockY = par3;
		partialBlockZ = par4;
	}

	public int getPartialBlockX() {
		return partialBlockX;
	}

	public int getPartialBlockY() {
		return partialBlockY;
	}

	public int getPartialBlockZ() {
		return partialBlockZ;
	}

	/**
	 * inserts damage value into this partially destroyed Block. -1 causes
	 * client renderer to delete it, otherwise ranges from 1 to 10
	 */
	public void setPartialBlockDamage(int par1) {
		if (par1 > 10) {
			par1 = 10;
		}

		partialBlockProgress = par1;
	}

	public int getPartialBlockDamage() {
		return partialBlockProgress;
	}

	/**
	 * saves the current Cloud update tick into the PartiallyDestroyedBlock
	 */
	public void setCloudUpdateTick(final int par1) {
		createdAtCloudUpdateTick = par1;
	}

	/**
	 * retrieves the 'date' at which the PartiallyDestroyedBlock was created
	 */
	public int getCreationCloudUpdateTick() {
		return createdAtCloudUpdateTick;
	}
}
