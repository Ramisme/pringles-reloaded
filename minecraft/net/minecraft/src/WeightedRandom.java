package net.minecraft.src;

import java.util.Collection;
import java.util.Iterator;
import java.util.Random;

public class WeightedRandom {
	/**
	 * Returns the total weight of all items in a collection.
	 */
	public static int getTotalWeight(final Collection par0Collection) {
		int var1 = 0;
		WeightedRandomItem var3;

		for (final Iterator var2 = par0Collection.iterator(); var2.hasNext(); var1 += var3.itemWeight) {
			var3 = (WeightedRandomItem) var2.next();
		}

		return var1;
	}

	/**
	 * Returns a random choice from the input items, with a total weight value.
	 */
	public static WeightedRandomItem getRandomItem(final Random par0Random,
			final Collection par1Collection, final int par2) {
		if (par2 <= 0) {
			throw new IllegalArgumentException();
		} else {
			int var3 = par0Random.nextInt(par2);
			final Iterator var4 = par1Collection.iterator();
			WeightedRandomItem var5;

			do {
				if (!var4.hasNext()) {
					return null;
				}

				var5 = (WeightedRandomItem) var4.next();
				var3 -= var5.itemWeight;
			} while (var3 >= 0);

			return var5;
		}
	}

	/**
	 * Returns a random choice from the input items.
	 */
	public static WeightedRandomItem getRandomItem(final Random par0Random,
			final Collection par1Collection) {
		return WeightedRandom.getRandomItem(par0Random, par1Collection,
				WeightedRandom.getTotalWeight(par1Collection));
	}

	/**
	 * Returns the total weight of all items in a array.
	 */
	public static int getTotalWeight(
			final WeightedRandomItem[] par0ArrayOfWeightedRandomItem) {
		int var1 = 0;
		final WeightedRandomItem[] var2 = par0ArrayOfWeightedRandomItem;
		final int var3 = par0ArrayOfWeightedRandomItem.length;

		for (int var4 = 0; var4 < var3; ++var4) {
			final WeightedRandomItem var5 = var2[var4];
			var1 += var5.itemWeight;
		}

		return var1;
	}

	/**
	 * Returns a random choice from the input array of items, with a total
	 * weight value.
	 */
	public static WeightedRandomItem getRandomItem(final Random par0Random,
			final WeightedRandomItem[] par1ArrayOfWeightedRandomItem,
			final int par2) {
		if (par2 <= 0) {
			throw new IllegalArgumentException();
		} else {
			int var3 = par0Random.nextInt(par2);
			final WeightedRandomItem[] var4 = par1ArrayOfWeightedRandomItem;
			final int var5 = par1ArrayOfWeightedRandomItem.length;

			for (int var6 = 0; var6 < var5; ++var6) {
				final WeightedRandomItem var7 = var4[var6];
				var3 -= var7.itemWeight;

				if (var3 < 0) {
					return var7;
				}
			}

			return null;
		}
	}

	/**
	 * Returns a random choice from the input items.
	 */
	public static WeightedRandomItem getRandomItem(final Random par0Random,
			final WeightedRandomItem[] par1ArrayOfWeightedRandomItem) {
		return WeightedRandom.getRandomItem(par0Random,
				par1ArrayOfWeightedRandomItem,
				WeightedRandom.getTotalWeight(par1ArrayOfWeightedRandomItem));
	}
}
