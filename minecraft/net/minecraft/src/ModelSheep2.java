package net.minecraft.src;

public class ModelSheep2 extends ModelQuadruped {
	private float field_78153_i;

	public ModelSheep2() {
		super(12, 0.0F);
		head = new ModelRenderer(this, 0, 0);
		head.addBox(-3.0F, -4.0F, -6.0F, 6, 6, 8, 0.0F);
		head.setRotationPoint(0.0F, 6.0F, -8.0F);
		body = new ModelRenderer(this, 28, 8);
		body.addBox(-4.0F, -10.0F, -7.0F, 8, 16, 6, 0.0F);
		body.setRotationPoint(0.0F, 5.0F, 2.0F);
	}

	/**
	 * Used for easily adding entity-dependent animations. The second and third
	 * float params here are the same second and third as in the
	 * setRotationAngles method.
	 */
	@Override
	public void setLivingAnimations(final EntityLiving par1EntityLiving,
			final float par2, final float par3, final float par4) {
		super.setLivingAnimations(par1EntityLiving, par2, par3, par4);
		head.rotationPointY = 6.0F + ((EntitySheep) par1EntityLiving)
				.func_70894_j(par4) * 9.0F;
		field_78153_i = ((EntitySheep) par1EntityLiving).func_70890_k(par4);
	}

	/**
	 * Sets the model's various rotation angles. For bipeds, par1 and par2 are
	 * used for animating the movement of arms and legs, where par1 represents
	 * the time(so that arms and legs swing back and forth) and par2 represents
	 * how "far" arms and legs can swing at most.
	 */
	@Override
	public void setRotationAngles(final float par1, final float par2,
			final float par3, final float par4, final float par5,
			final float par6, final Entity par7Entity) {
		super.setRotationAngles(par1, par2, par3, par4, par5, par6, par7Entity);
		head.rotateAngleX = field_78153_i;
	}
}
