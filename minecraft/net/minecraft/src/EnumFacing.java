package net.minecraft.src;

public enum EnumFacing {
	DOWN(0, 1, 0, -1, 0), UP(1, 0, 0, 1, 0), NORTH(2, 3, 0, 0, -1), SOUTH(3, 2,
			0, 0, 1), EAST(4, 5, -1, 0, 0), WEST(5, 4, 1, 0, 0);

	/** Face order for D-U-N-S-E-W. */
	private final int order_a;

	private final int frontOffsetX;
	private final int frontOffsetY;
	private final int frontOffsetZ;

	/** List of all values in EnumFacing. Order is D-U-N-S-E-W. */
	private static final EnumFacing[] faceList = new EnumFacing[6];

	private EnumFacing(final int par3, final int par4, final int par5,
			final int par6, final int par7) {
		order_a = par3;
		frontOffsetX = par5;
		frontOffsetY = par6;
		frontOffsetZ = par7;
	}

	/**
	 * Returns a offset that addresses the block in front of this facing.
	 */
	public int getFrontOffsetX() {
		return frontOffsetX;
	}

	public int getFrontOffsetY() {
		return frontOffsetY;
	}

	/**
	 * Returns a offset that addresses the block in front of this facing.
	 */
	public int getFrontOffsetZ() {
		return frontOffsetZ;
	}

	/**
	 * Returns the facing that represents the block in front of it.
	 */
	public static EnumFacing getFront(final int par0) {
		return EnumFacing.faceList[par0 % EnumFacing.faceList.length];
	}

	static {
		final EnumFacing[] var0 = EnumFacing.values();
		final int var1 = var0.length;

		for (int var2 = 0; var2 < var1; ++var2) {
			final EnumFacing var3 = var0[var2];
			EnumFacing.faceList[var3.order_a] = var3;
		}
	}
}
