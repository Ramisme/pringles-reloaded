package net.minecraft.src;

import java.util.concurrent.Callable;

class CallableType implements Callable {
	/** Reference to the DecitatedServer object. */
	final DedicatedServer theDecitatedServer;

	CallableType(final DedicatedServer par1DedicatedServer) {
		theDecitatedServer = par1DedicatedServer;
	}

	public String getType() {
		final String var1 = theDecitatedServer.getServerModName();
		return !var1.equals("vanilla") ? "Definitely; Server brand changed to \'"
				+ var1 + "\'"
				: "Unknown (can\'t tell)";
	}

	@Override
	public Object call() {
		return getType();
	}
}
