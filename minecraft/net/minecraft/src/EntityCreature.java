package net.minecraft.src;

public abstract class EntityCreature extends EntityLiving {
	private PathEntity pathToEntity;

	/** The Entity this EntityCreature is set to attack. */
	protected Entity entityToAttack;

	/**
	 * returns true if a creature has attacked recently only used for creepers
	 * and skeletons
	 */
	protected boolean hasAttacked = false;

	/** Used to make a creature speed up and wander away when hit. */
	protected int fleeingTick = 0;

	public EntityCreature(final World par1World) {
		super(par1World);
	}

	/**
	 * Disables a mob's ability to move on its own while true.
	 */
	protected boolean isMovementCeased() {
		return false;
	}

	@Override
	protected void updateEntityActionState() {
		worldObj.theProfiler.startSection("ai");

		if (fleeingTick > 0) {
			--fleeingTick;
		}

		hasAttacked = isMovementCeased();
		final float var1 = 16.0F;

		if (entityToAttack == null) {
			entityToAttack = findPlayerToAttack();

			if (entityToAttack != null) {
				pathToEntity = worldObj.getPathEntityToEntity(this,
						entityToAttack, var1, true, false, false, true);
			}
		} else if (entityToAttack.isEntityAlive()) {
			final float var2 = entityToAttack.getDistanceToEntity(this);

			if (canEntityBeSeen(entityToAttack)) {
				attackEntity(entityToAttack, var2);
			}
		} else {
			entityToAttack = null;
		}

		worldObj.theProfiler.endSection();

		if (!hasAttacked && entityToAttack != null
				&& (pathToEntity == null || rand.nextInt(20) == 0)) {
			pathToEntity = worldObj.getPathEntityToEntity(this, entityToAttack,
					var1, true, false, false, true);
		} else if (!hasAttacked
				&& (pathToEntity == null && rand.nextInt(180) == 0
						|| rand.nextInt(120) == 0 || fleeingTick > 0)
				&& entityAge < 100) {
			updateWanderPath();
		}

		final int var21 = MathHelper.floor_double(boundingBox.minY + 0.5D);
		final boolean var3 = isInWater();
		final boolean var4 = handleLavaMovement();
		rotationPitch = 0.0F;

		if (pathToEntity != null && rand.nextInt(100) != 0) {
			worldObj.theProfiler.startSection("followpath");
			Vec3 var5 = pathToEntity.getPosition(this);
			final double var6 = width * 2.0F;

			while (var5 != null
					&& var5.squareDistanceTo(posX, var5.yCoord, posZ) < var6
							* var6) {
				pathToEntity.incrementPathIndex();

				if (pathToEntity.isFinished()) {
					var5 = null;
					pathToEntity = null;
				} else {
					var5 = pathToEntity.getPosition(this);
				}
			}

			isJumping = false;

			if (var5 != null) {
				final double var8 = var5.xCoord - posX;
				final double var10 = var5.zCoord - posZ;
				final double var12 = var5.yCoord - var21;
				final float var14 = (float) (Math.atan2(var10, var8) * 180.0D / Math.PI) - 90.0F;
				float var15 = MathHelper.wrapAngleTo180_float(var14
						- rotationYaw);
				moveForward = moveSpeed;

				if (var15 > 30.0F) {
					var15 = 30.0F;
				}

				if (var15 < -30.0F) {
					var15 = -30.0F;
				}

				rotationYaw += var15;

				if (hasAttacked && entityToAttack != null) {
					final double var16 = entityToAttack.posX - posX;
					final double var18 = entityToAttack.posZ - posZ;
					final float var20 = rotationYaw;
					rotationYaw = (float) (Math.atan2(var18, var16) * 180.0D / Math.PI) - 90.0F;
					var15 = (var20 - rotationYaw + 90.0F) * (float) Math.PI
							/ 180.0F;
					moveStrafing = -MathHelper.sin(var15) * moveForward * 1.0F;
					moveForward = MathHelper.cos(var15) * moveForward * 1.0F;
				}

				if (var12 > 0.0D) {
					isJumping = true;
				}
			}

			if (entityToAttack != null) {
				faceEntity(entityToAttack, 30.0F, 30.0F);
			}

			if (isCollidedHorizontally && !hasPath()) {
				isJumping = true;
			}

			if (rand.nextFloat() < 0.8F && (var3 || var4)) {
				isJumping = true;
			}

			worldObj.theProfiler.endSection();
		} else {
			super.updateEntityActionState();
			pathToEntity = null;
		}
	}

	/**
	 * Time remaining during which the Animal is sped up and flees.
	 */
	protected void updateWanderPath() {
		worldObj.theProfiler.startSection("stroll");
		boolean var1 = false;
		int var2 = -1;
		int var3 = -1;
		int var4 = -1;
		float var5 = -99999.0F;

		for (int var6 = 0; var6 < 10; ++var6) {
			final int var7 = MathHelper.floor_double(posX + rand.nextInt(13)
					- 6.0D);
			final int var8 = MathHelper.floor_double(posY + rand.nextInt(7)
					- 3.0D);
			final int var9 = MathHelper.floor_double(posZ + rand.nextInt(13)
					- 6.0D);
			final float var10 = getBlockPathWeight(var7, var8, var9);

			if (var10 > var5) {
				var5 = var10;
				var2 = var7;
				var3 = var8;
				var4 = var9;
				var1 = true;
			}
		}

		if (var1) {
			pathToEntity = worldObj.getEntityPathToXYZ(this, var2, var3, var4,
					10.0F, true, false, false, true);
		}

		worldObj.theProfiler.endSection();
	}

	/**
	 * Basic mob attack. Default to touch of death in EntityCreature. Overridden
	 * by each mob to define their attack.
	 */
	protected void attackEntity(final Entity par1Entity, final float par2) {
	}

	/**
	 * Takes a coordinate in and returns a weight to determine how likely this
	 * creature will try to path to the block. Args: x, y, z
	 */
	public float getBlockPathWeight(final int par1, final int par2,
			final int par3) {
		return 0.0F;
	}

	/**
	 * Finds the closest player within 16 blocks to attack, or null if this
	 * Entity isn't interested in attacking (Animals, Spiders at day, peaceful
	 * PigZombies).
	 */
	protected Entity findPlayerToAttack() {
		return null;
	}

	/**
	 * Checks if the entity's current position is a valid location to spawn this
	 * entity.
	 */
	@Override
	public boolean getCanSpawnHere() {
		final int var1 = MathHelper.floor_double(posX);
		final int var2 = MathHelper.floor_double(boundingBox.minY);
		final int var3 = MathHelper.floor_double(posZ);
		return super.getCanSpawnHere()
				&& getBlockPathWeight(var1, var2, var3) >= 0.0F;
	}

	/**
	 * Returns true if entity has a path to follow
	 */
	public boolean hasPath() {
		return pathToEntity != null;
	}

	/**
	 * sets the Entities walk path in EntityCreature
	 */
	public void setPathToEntity(final PathEntity par1PathEntity) {
		pathToEntity = par1PathEntity;
	}

	/**
	 * Returns current entities target
	 */
	public Entity getEntityToAttack() {
		return entityToAttack;
	}

	/**
	 * Sets the entity which is to be attacked.
	 */
	public void setTarget(final Entity par1Entity) {
		entityToAttack = par1Entity;
	}

	/**
	 * This method returns a value to be applied directly to entity speed, this
	 * factor is less than 1 when a slowdown potion effect is applied, more than
	 * 1 when a haste potion effect is applied and 2 for fleeing entities.
	 */
	@Override
	public float getSpeedModifier() {
		float var1 = super.getSpeedModifier();

		if (fleeingTick > 0 && !isAIEnabled()) {
			var1 *= 2.0F;
		}

		return var1;
	}
}
