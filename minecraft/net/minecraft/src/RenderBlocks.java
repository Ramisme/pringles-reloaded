package net.minecraft.src;

import net.minecraft.client.Minecraft;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.ramisme.pringles.modules.world.wallhack.WallhackUtil;

public class RenderBlocks {
	/** The IBlockAccess used by this instance of RenderBlocks */
	public IBlockAccess blockAccess;

	/**
	 * If set to >=0, all block faces will be rendered using this texture index
	 */
	public Icon overrideBlockTexture = null;

	/**
	 * Set to true if the texture should be flipped horizontally during
	 * render*Face
	 */
	public boolean flipTexture = false;

	/**
	 * If true, renders all faces on all blocks rather than using the logic in
	 * Block.shouldSideBeRendered. Unused.
	 */
	public boolean renderAllFaces = false;

	/** Fancy grass side matching biome */
	public static boolean fancyGrass = true;
	public static boolean cfgGrassFix = true;
	public boolean useInventoryTint = true;

	/** The minimum X value for rendering (default 0.0). */
	public double renderMinX;

	/** The maximum X value for rendering (default 1.0). */
	public double renderMaxX;

	/** The minimum Y value for rendering (default 0.0). */
	public double renderMinY;

	/** The maximum Y value for rendering (default 1.0). */
	public double renderMaxY;

	/** The minimum Z value for rendering (default 0.0). */
	public double renderMinZ;

	/** The maximum Z value for rendering (default 1.0). */
	public double renderMaxZ;

	/**
	 * Set by overrideBlockBounds, to keep this class from changing the visual
	 * bounding box.
	 */
	public boolean lockBlockBounds = false;
	public boolean partialRenderBounds = false;
	public final Minecraft minecraftRB;
	public int uvRotateEast = 0;
	public int uvRotateWest = 0;
	public int uvRotateSouth = 0;
	public int uvRotateNorth = 0;
	public int uvRotateTop = 0;
	public int uvRotateBottom = 0;

	/** Whether ambient occlusion is enabled or not */
	public boolean enableAO;

	/**
	 * Used as a scratch variable for ambient occlusion on the north/bottom/east
	 * corner.
	 */
	public float aoLightValueScratchXYZNNN;

	/**
	 * Used as a scratch variable for ambient occlusion between the bottom face
	 * and the north face.
	 */
	public float aoLightValueScratchXYNN;

	/**
	 * Used as a scratch variable for ambient occlusion on the north/bottom/west
	 * corner.
	 */
	public float aoLightValueScratchXYZNNP;

	/**
	 * Used as a scratch variable for ambient occlusion between the bottom face
	 * and the east face.
	 */
	public float aoLightValueScratchYZNN;

	/**
	 * Used as a scratch variable for ambient occlusion between the bottom face
	 * and the west face.
	 */
	public float aoLightValueScratchYZNP;

	/**
	 * Used as a scratch variable for ambient occlusion on the south/bottom/east
	 * corner.
	 */
	public float aoLightValueScratchXYZPNN;

	/**
	 * Used as a scratch variable for ambient occlusion between the bottom face
	 * and the south face.
	 */
	public float aoLightValueScratchXYPN;

	/**
	 * Used as a scratch variable for ambient occlusion on the south/bottom/west
	 * corner.
	 */
	public float aoLightValueScratchXYZPNP;

	/**
	 * Used as a scratch variable for ambient occlusion on the north/top/east
	 * corner.
	 */
	public float aoLightValueScratchXYZNPN;

	/**
	 * Used as a scratch variable for ambient occlusion between the top face and
	 * the north face.
	 */
	public float aoLightValueScratchXYNP;

	/**
	 * Used as a scratch variable for ambient occlusion on the north/top/west
	 * corner.
	 */
	public float aoLightValueScratchXYZNPP;

	/**
	 * Used as a scratch variable for ambient occlusion between the top face and
	 * the east face.
	 */
	public float aoLightValueScratchYZPN;

	/**
	 * Used as a scratch variable for ambient occlusion on the south/top/east
	 * corner.
	 */
	public float aoLightValueScratchXYZPPN;

	/**
	 * Used as a scratch variable for ambient occlusion between the top face and
	 * the south face.
	 */
	public float aoLightValueScratchXYPP;

	/**
	 * Used as a scratch variable for ambient occlusion between the top face and
	 * the west face.
	 */
	public float aoLightValueScratchYZPP;

	/**
	 * Used as a scratch variable for ambient occlusion on the south/top/west
	 * corner.
	 */
	public float aoLightValueScratchXYZPPP;

	/**
	 * Used as a scratch variable for ambient occlusion between the north face
	 * and the east face.
	 */
	public float aoLightValueScratchXZNN;

	/**
	 * Used as a scratch variable for ambient occlusion between the south face
	 * and the east face.
	 */
	public float aoLightValueScratchXZPN;

	/**
	 * Used as a scratch variable for ambient occlusion between the north face
	 * and the west face.
	 */
	public float aoLightValueScratchXZNP;

	/**
	 * Used as a scratch variable for ambient occlusion between the south face
	 * and the west face.
	 */
	public float aoLightValueScratchXZPP;

	/** Ambient occlusion brightness XYZNNN */
	public int aoBrightnessXYZNNN;

	/** Ambient occlusion brightness XYNN */
	public int aoBrightnessXYNN;

	/** Ambient occlusion brightness XYZNNP */
	public int aoBrightnessXYZNNP;

	/** Ambient occlusion brightness YZNN */
	public int aoBrightnessYZNN;

	/** Ambient occlusion brightness YZNP */
	public int aoBrightnessYZNP;

	/** Ambient occlusion brightness XYZPNN */
	public int aoBrightnessXYZPNN;

	/** Ambient occlusion brightness XYPN */
	public int aoBrightnessXYPN;

	/** Ambient occlusion brightness XYZPNP */
	public int aoBrightnessXYZPNP;

	/** Ambient occlusion brightness XYZNPN */
	public int aoBrightnessXYZNPN;

	/** Ambient occlusion brightness XYNP */
	public int aoBrightnessXYNP;

	/** Ambient occlusion brightness XYZNPP */
	public int aoBrightnessXYZNPP;

	/** Ambient occlusion brightness YZPN */
	public int aoBrightnessYZPN;

	/** Ambient occlusion brightness XYZPPN */
	public int aoBrightnessXYZPPN;

	/** Ambient occlusion brightness XYPP */
	public int aoBrightnessXYPP;

	/** Ambient occlusion brightness YZPP */
	public int aoBrightnessYZPP;

	/** Ambient occlusion brightness XYZPPP */
	public int aoBrightnessXYZPPP;

	/** Ambient occlusion brightness XZNN */
	public int aoBrightnessXZNN;

	/** Ambient occlusion brightness XZPN */
	public int aoBrightnessXZPN;

	/** Ambient occlusion brightness XZNP */
	public int aoBrightnessXZNP;

	/** Ambient occlusion brightness XZPP */
	public int aoBrightnessXZPP;

	/** Brightness top left */
	public int brightnessTopLeft;

	/** Brightness bottom left */
	public int brightnessBottomLeft;

	/** Brightness bottom right */
	public int brightnessBottomRight;

	/** Brightness top right */
	public int brightnessTopRight;

	/** Red color value for the top left corner */
	public float colorRedTopLeft;

	/** Red color value for the bottom left corner */
	public float colorRedBottomLeft;

	/** Red color value for the bottom right corner */
	public float colorRedBottomRight;

	/** Red color value for the top right corner */
	public float colorRedTopRight;

	/** Green color value for the top left corner */
	public float colorGreenTopLeft;

	/** Green color value for the bottom left corner */
	public float colorGreenBottomLeft;

	/** Green color value for the bottom right corner */
	public float colorGreenBottomRight;

	/** Green color value for the top right corner */
	public float colorGreenTopRight;

	/** Blue color value for the top left corner */
	public float colorBlueTopLeft;

	/** Blue color value for the bottom left corner */
	public float colorBlueBottomLeft;

	/** Blue color value for the bottom right corner */
	public float colorBlueBottomRight;

	/** Blue color value for the top right corner */
	public float colorBlueTopRight;
	public boolean aoLightValuesCalculated;
	public float aoLightValueOpaque = 0.2F;

	public RenderBlocks(final IBlockAccess par1IBlockAccess) {
		blockAccess = par1IBlockAccess;
		minecraftRB = Minecraft.getMinecraft();
		aoLightValueOpaque = 1.0F - Config.getAmbientOcclusionLevel() * 0.8F;
	}

	public RenderBlocks() {
		minecraftRB = Minecraft.getMinecraft();
	}

	/**
	 * Sets overrideBlockTexture
	 */
	public void setOverrideBlockTexture(final Icon par1Icon) {
		overrideBlockTexture = par1Icon;
	}

	/**
	 * Clear override block texture
	 */
	public void clearOverrideBlockTexture() {
		overrideBlockTexture = null;
	}

	public boolean hasOverrideBlockTexture() {
		return overrideBlockTexture != null;
	}

	/**
	 * Sets the bounding box for the block to draw in, e.g. 0.25-0.75 on all
	 * axes for a half-size, centered block.
	 */
	public void setRenderBounds(final double par1, final double par3,
			final double par5, final double par7, final double par9,
			final double par11) {
		if (!lockBlockBounds) {
			renderMinX = par1;
			renderMaxX = par7;
			renderMinY = par3;
			renderMaxY = par9;
			renderMinZ = par5;
			renderMaxZ = par11;
			partialRenderBounds = minecraftRB.gameSettings.ambientOcclusion >= 2
					&& (renderMinX > 0.0D || renderMaxX < 1.0D
							|| renderMinY > 0.0D || renderMaxY < 1.0D
							|| renderMinZ > 0.0D || renderMaxZ < 1.0D);
		}
	}

	/**
	 * Like setRenderBounds, but automatically pulling the bounds from the given
	 * Block.
	 */
	public void setRenderBoundsFromBlock(final Block par1Block) {
		if (!lockBlockBounds) {
			renderMinX = par1Block.getBlockBoundsMinX();
			renderMaxX = par1Block.getBlockBoundsMaxX();
			renderMinY = par1Block.getBlockBoundsMinY();
			renderMaxY = par1Block.getBlockBoundsMaxY();
			renderMinZ = par1Block.getBlockBoundsMinZ();
			renderMaxZ = par1Block.getBlockBoundsMaxZ();
			partialRenderBounds = minecraftRB.gameSettings.ambientOcclusion >= 2
					&& (renderMinX > 0.0D || renderMaxX < 1.0D
							|| renderMinY > 0.0D || renderMaxY < 1.0D
							|| renderMinZ > 0.0D || renderMaxZ < 1.0D);
		}
	}

	/**
	 * Like setRenderBounds, but locks the values so that RenderBlocks won't
	 * change them. If you use this, you must call unlockBlockBounds after you
	 * finish rendering!
	 */
	public void overrideBlockBounds(final double par1, final double par3,
			final double par5, final double par7, final double par9,
			final double par11) {
		renderMinX = par1;
		renderMaxX = par7;
		renderMinY = par3;
		renderMaxY = par9;
		renderMinZ = par5;
		renderMaxZ = par11;
		lockBlockBounds = true;
		partialRenderBounds = minecraftRB.gameSettings.ambientOcclusion >= 2
				&& (renderMinX > 0.0D || renderMaxX < 1.0D || renderMinY > 0.0D
						|| renderMaxY < 1.0D || renderMinZ > 0.0D || renderMaxZ < 1.0D);
	}

	/**
	 * Unlocks the visual bounding box so that RenderBlocks can change it again.
	 */
	public void unlockBlockBounds() {
		lockBlockBounds = false;
	}

	/**
	 * Renders a block using the given texture instead of the block's own
	 * default texture
	 */
	public void renderBlockUsingTexture(final Block par1Block, final int par2,
			final int par3, final int par4, final Icon par5Icon) {
		setOverrideBlockTexture(par5Icon);
		renderBlockByRenderType(par1Block, par2, par3, par4);
		clearOverrideBlockTexture();
	}

	/**
	 * Render all faces of a block
	 */
	public void renderBlockAllFaces(final Block par1Block, final int par2,
			final int par3, final int par4) {
		renderAllFaces = true;
		renderBlockByRenderType(par1Block, par2, par3, par4);
		renderAllFaces = false;
	}

	/**
	 * Renders the block at the given coordinates using the block's rendering
	 * type
	 */
	public boolean renderBlockByRenderType(final Block par1Block,
			final int par2, final int par3, final int par4) {
		final int var5 = par1Block.getRenderType();

		// TODO: RenderBlocks#renderBlockByRenderType
		if (WallhackUtil.getInstance().isEnabled()) {
			renderAllFaces = WallhackUtil.getInstance().isWallhackBlock(
					par1Block.blockID);
		}

		if (var5 == -1) {
			return false;
		} else {
			par1Block.setBlockBoundsBasedOnState(blockAccess, par2, par3, par4);

			if (Config.isBetterSnow() && par1Block == Block.signPost
					&& hasSnowNeighbours(par2, par3, par4)) {
				renderSnow(par2, par3, par4, Block.snow.maxY);
			}

			setRenderBoundsFromBlock(par1Block);

			switch (var5) {
			case 0:
				return renderStandardBlock(par1Block, par2, par3, par4);

			case 1:
				return renderCrossedSquares(par1Block, par2, par3, par4);

			case 2:
				return renderBlockTorch(par1Block, par2, par3, par4);

			case 3:
				return renderBlockFire((BlockFire) par1Block, par2, par3, par4);

			case 4:
				return renderBlockFluids(par1Block, par2, par3, par4);

			case 5:
				return renderBlockRedstoneWire(par1Block, par2, par3, par4);

			case 6:
				return renderBlockCrops(par1Block, par2, par3, par4);

			case 7:
				return renderBlockDoor(par1Block, par2, par3, par4);

			case 8:
				return renderBlockLadder(par1Block, par2, par3, par4);

			case 9:
				return renderBlockMinecartTrack((BlockRailBase) par1Block,
						par2, par3, par4);

			case 10:
				return renderBlockStairs((BlockStairs) par1Block, par2, par3,
						par4);

			case 11:
				return renderBlockFence((BlockFence) par1Block, par2, par3,
						par4);

			case 12:
				return renderBlockLever(par1Block, par2, par3, par4);

			case 13:
				return renderBlockCactus(par1Block, par2, par3, par4);

			case 14:
				return renderBlockBed(par1Block, par2, par3, par4);

			case 15:
				return renderBlockRepeater((BlockRedstoneRepeater) par1Block,
						par2, par3, par4);

			case 16:
				return renderPistonBase(par1Block, par2, par3, par4, false);

			case 17:
				return renderPistonExtension(par1Block, par2, par3, par4, true);

			case 18:
				return renderBlockPane((BlockPane) par1Block, par2, par3, par4);

			case 19:
				return renderBlockStem(par1Block, par2, par3, par4);

			case 20:
				return renderBlockVine(par1Block, par2, par3, par4);

			case 21:
				return renderBlockFenceGate((BlockFenceGate) par1Block, par2,
						par3, par4);

			case 22:
			default:
				if (Reflector.ModLoader.exists()) {
					return Reflector.callBoolean(
							Reflector.ModLoader_renderWorldBlock,
							new Object[] { this, blockAccess,
									Integer.valueOf(par2),
									Integer.valueOf(par3),
									Integer.valueOf(par4), par1Block,
									Integer.valueOf(var5) });
				} else {
					if (Reflector.FMLRenderAccessLibrary.exists()) {
						return Reflector
								.callBoolean(
										Reflector.FMLRenderAccessLibrary_renderWorldBlock,
										new Object[] { this, blockAccess,
												Integer.valueOf(par2),
												Integer.valueOf(par3),
												Integer.valueOf(par4),
												par1Block,
												Integer.valueOf(var5) });
					}

					return false;
				}

			case 23:
				return renderBlockLilyPad(par1Block, par2, par3, par4);

			case 24:
				return renderBlockCauldron((BlockCauldron) par1Block, par2,
						par3, par4);

			case 25:
				return renderBlockBrewingStand((BlockBrewingStand) par1Block,
						par2, par3, par4);

			case 26:
				return renderBlockEndPortalFrame(
						(BlockEndPortalFrame) par1Block, par2, par3, par4);

			case 27:
				return renderBlockDragonEgg((BlockDragonEgg) par1Block, par2,
						par3, par4);

			case 28:
				return renderBlockCocoa((BlockCocoa) par1Block, par2, par3,
						par4);

			case 29:
				return renderBlockTripWireSource(par1Block, par2, par3, par4);

			case 30:
				return renderBlockTripWire(par1Block, par2, par3, par4);

			case 31:
				return renderBlockLog(par1Block, par2, par3, par4);

			case 32:
				return renderBlockWall((BlockWall) par1Block, par2, par3, par4);

			case 33:
				return renderBlockFlowerpot((BlockFlowerPot) par1Block, par2,
						par3, par4);

			case 34:
				return renderBlockBeacon((BlockBeacon) par1Block, par2, par3,
						par4);

			case 35:
				return renderBlockAnvil((BlockAnvil) par1Block, par2, par3,
						par4);

			case 36:
				return renderBlockRedstoneLogic((BlockRedstoneLogic) par1Block,
						par2, par3, par4);

			case 37:
				return renderBlockComparator((BlockComparator) par1Block, par2,
						par3, par4);

			case 38:
				return renderBlockHopper((BlockHopper) par1Block, par2, par3,
						par4);

			case 39:
				return renderBlockQuartz(par1Block, par2, par3, par4);
			}
		}
	}

	/**
	 * Render BlockEndPortalFrame
	 */
	public boolean renderBlockEndPortalFrame(
			final BlockEndPortalFrame par1BlockEndPortalFrame, final int par2,
			final int par3, final int par4) {
		final int var5 = blockAccess.getBlockMetadata(par2, par3, par4);
		final int var6 = var5 & 3;

		if (var6 == 0) {
			uvRotateTop = 3;
		} else if (var6 == 3) {
			uvRotateTop = 1;
		} else if (var6 == 1) {
			uvRotateTop = 2;
		}

		if (!BlockEndPortalFrame.isEnderEyeInserted(var5)) {
			setRenderBounds(0.0D, 0.0D, 0.0D, 1.0D, 0.8125D, 1.0D);
			renderStandardBlock(par1BlockEndPortalFrame, par2, par3, par4);
			uvRotateTop = 0;
			return true;
		} else {
			renderAllFaces = true;
			setRenderBounds(0.0D, 0.0D, 0.0D, 1.0D, 0.8125D, 1.0D);
			renderStandardBlock(par1BlockEndPortalFrame, par2, par3, par4);
			setOverrideBlockTexture(par1BlockEndPortalFrame.func_94398_p());
			setRenderBounds(0.25D, 0.8125D, 0.25D, 0.75D, 1.0D, 0.75D);
			renderStandardBlock(par1BlockEndPortalFrame, par2, par3, par4);
			renderAllFaces = false;
			clearOverrideBlockTexture();
			uvRotateTop = 0;
			return true;
		}
	}

	/**
	 * render a bed at the given coordinates
	 */
	public boolean renderBlockBed(final Block par1Block, final int par2,
			final int par3, final int par4) {
		final Tessellator var5 = Tessellator.instance;
		final int var6 = blockAccess.getBlockMetadata(par2, par3, par4);
		int var7 = BlockDirectional.getDirection(var6);
		boolean var8 = BlockBed.isBlockHeadOfBed(var6);

		if (Reflector.ForgeBlock_getBedDirection.exists()) {
			var7 = Reflector.callInt(
					par1Block,
					Reflector.ForgeBlock_getBedDirection,
					new Object[] { blockAccess, Integer.valueOf(par2),
							Integer.valueOf(par3), Integer.valueOf(par4) });
		}

		if (Reflector.ForgeBlock_isBedFoot.exists()) {
			var8 = Reflector.callBoolean(par1Block,
					Reflector.ForgeBlock_isBedFoot, new Object[] { blockAccess,
							Integer.valueOf(par2), Integer.valueOf(par3),
							Integer.valueOf(par4) });
		}

		final float var9 = 0.5F;
		final float var10 = 1.0F;
		final float var11 = 0.8F;
		final float var12 = 0.6F;
		final int var13 = par1Block.getMixedBrightnessForBlock(blockAccess,
				par2, par3, par4);
		var5.setBrightness(var13);
		var5.setColorOpaque_F(var9, var9, var9);
		Icon var14 = this.getBlockIcon(par1Block, blockAccess, par2, par3,
				par4, 0);

		if (overrideBlockTexture != null) {
			var14 = overrideBlockTexture;
		}

		double var15 = var14.getMinU();
		double var17 = var14.getMaxU();
		double var19 = var14.getMinV();
		double var21 = var14.getMaxV();
		double var23 = par2 + renderMinX;
		double var25 = par2 + renderMaxX;
		double var27 = par3 + renderMinY + 0.1875D;
		double var29 = par4 + renderMinZ;
		double var31 = par4 + renderMaxZ;
		var5.addVertexWithUV(var23, var27, var31, var15, var21);
		var5.addVertexWithUV(var23, var27, var29, var15, var19);
		var5.addVertexWithUV(var25, var27, var29, var17, var19);
		var5.addVertexWithUV(var25, var27, var31, var17, var21);
		var5.setBrightness(par1Block.getMixedBrightnessForBlock(blockAccess,
				par2, par3 + 1, par4));
		var5.setColorOpaque_F(var10, var10, var10);
		var14 = this.getBlockIcon(par1Block, blockAccess, par2, par3, par4, 1);

		if (overrideBlockTexture != null) {
			var14 = overrideBlockTexture;
		}

		var15 = var14.getMinU();
		var17 = var14.getMaxU();
		var19 = var14.getMinV();
		var21 = var14.getMaxV();
		var23 = var15;
		var25 = var17;
		var27 = var19;
		var29 = var19;
		var31 = var15;
		double var33 = var17;
		double var35 = var21;
		double var37 = var21;

		if (var7 == 0) {
			var25 = var15;
			var27 = var21;
			var31 = var17;
			var37 = var19;
		} else if (var7 == 2) {
			var23 = var17;
			var29 = var21;
			var33 = var15;
			var35 = var19;
		} else if (var7 == 3) {
			var23 = var17;
			var29 = var21;
			var33 = var15;
			var35 = var19;
			var25 = var15;
			var27 = var21;
			var31 = var17;
			var37 = var19;
		}

		final double var39 = par2 + renderMinX;
		final double var41 = par2 + renderMaxX;
		final double var43 = par3 + renderMaxY;
		final double var45 = par4 + renderMinZ;
		final double var47 = par4 + renderMaxZ;
		var5.addVertexWithUV(var41, var43, var47, var31, var35);
		var5.addVertexWithUV(var41, var43, var45, var23, var27);
		var5.addVertexWithUV(var39, var43, var45, var25, var29);
		var5.addVertexWithUV(var39, var43, var47, var33, var37);
		int var49 = Direction.directionToFacing[var7];

		if (var8) {
			var49 = Direction.directionToFacing[Direction.rotateOpposite[var7]];
		}

		byte var50 = 4;

		switch (var7) {
		case 0:
			var50 = 5;
			break;

		case 1:
			var50 = 3;

		case 2:
		default:
			break;

		case 3:
			var50 = 2;
		}

		if (var49 != 2
				&& (renderAllFaces || par1Block.shouldSideBeRendered(
						blockAccess, par2, par3, par4 - 1, 2))) {
			var5.setBrightness(renderMinZ > 0.0D ? var13 : par1Block
					.getMixedBrightnessForBlock(blockAccess, par2, par3,
							par4 - 1));
			var5.setColorOpaque_F(var11, var11, var11);
			flipTexture = var50 == 2;
			renderFaceZNeg(par1Block, par2, par3, par4, this.getBlockIcon(
					par1Block, blockAccess, par2, par3, par4, 2));
		}

		if (var49 != 3
				&& (renderAllFaces || par1Block.shouldSideBeRendered(
						blockAccess, par2, par3, par4 + 1, 3))) {
			var5.setBrightness(renderMaxZ < 1.0D ? var13 : par1Block
					.getMixedBrightnessForBlock(blockAccess, par2, par3,
							par4 + 1));
			var5.setColorOpaque_F(var11, var11, var11);
			flipTexture = var50 == 3;
			renderFaceZPos(par1Block, par2, par3, par4, this.getBlockIcon(
					par1Block, blockAccess, par2, par3, par4, 3));
		}

		if (var49 != 4
				&& (renderAllFaces || par1Block.shouldSideBeRendered(
						blockAccess, par2 - 1, par3, par4, 4))) {
			var5.setBrightness(renderMinZ > 0.0D ? var13 : par1Block
					.getMixedBrightnessForBlock(blockAccess, par2 - 1, par3,
							par4));
			var5.setColorOpaque_F(var12, var12, var12);
			flipTexture = var50 == 4;
			renderFaceXNeg(par1Block, par2, par3, par4, this.getBlockIcon(
					par1Block, blockAccess, par2, par3, par4, 4));
		}

		if (var49 != 5
				&& (renderAllFaces || par1Block.shouldSideBeRendered(
						blockAccess, par2 + 1, par3, par4, 5))) {
			var5.setBrightness(renderMaxZ < 1.0D ? var13 : par1Block
					.getMixedBrightnessForBlock(blockAccess, par2 + 1, par3,
							par4));
			var5.setColorOpaque_F(var12, var12, var12);
			flipTexture = var50 == 5;
			renderFaceXPos(par1Block, par2, par3, par4, this.getBlockIcon(
					par1Block, blockAccess, par2, par3, par4, 5));
		}

		flipTexture = false;
		return true;
	}

	/**
	 * Render BlockBrewingStand
	 */
	public boolean renderBlockBrewingStand(
			final BlockBrewingStand par1BlockBrewingStand, final int par2,
			final int par3, final int par4) {
		setRenderBounds(0.4375D, 0.0D, 0.4375D, 0.5625D, 0.875D, 0.5625D);
		renderStandardBlock(par1BlockBrewingStand, par2, par3, par4);
		setOverrideBlockTexture(par1BlockBrewingStand.getBrewingStandIcon());
		setRenderBounds(0.5625D, 0.0D, 0.3125D, 0.9375D, 0.125D, 0.6875D);
		renderStandardBlock(par1BlockBrewingStand, par2, par3, par4);
		setRenderBounds(0.125D, 0.0D, 0.0625D, 0.5D, 0.125D, 0.4375D);
		renderStandardBlock(par1BlockBrewingStand, par2, par3, par4);
		setRenderBounds(0.125D, 0.0D, 0.5625D, 0.5D, 0.125D, 0.9375D);
		renderStandardBlock(par1BlockBrewingStand, par2, par3, par4);
		clearOverrideBlockTexture();
		final Tessellator var5 = Tessellator.instance;
		var5.setBrightness(par1BlockBrewingStand.getMixedBrightnessForBlock(
				blockAccess, par2, par3, par4));
		final float var6 = 1.0F;
		final int var7 = par1BlockBrewingStand.colorMultiplier(blockAccess,
				par2, par3, par4);
		float var8 = (var7 >> 16 & 255) / 255.0F;
		float var9 = (var7 >> 8 & 255) / 255.0F;
		float var10 = (var7 & 255) / 255.0F;

		if (EntityRenderer.anaglyphEnable) {
			final float var11 = (var8 * 30.0F + var9 * 59.0F + var10 * 11.0F) / 100.0F;
			final float var12 = (var8 * 30.0F + var9 * 70.0F) / 100.0F;
			final float var13 = (var8 * 30.0F + var10 * 70.0F) / 100.0F;
			var8 = var11;
			var9 = var12;
			var10 = var13;
		}

		var5.setColorOpaque_F(var6 * var8, var6 * var9, var6 * var10);
		Icon var32 = getBlockIconFromSideAndMetadata(par1BlockBrewingStand, 0,
				0);

		if (hasOverrideBlockTexture()) {
			var32 = overrideBlockTexture;
		}

		final double var33 = var32.getMinV();
		final double var14 = var32.getMaxV();
		final int var16 = blockAccess.getBlockMetadata(par2, par3, par4);

		for (int var17 = 0; var17 < 3; ++var17) {
			final double var18 = var17 * Math.PI * 2.0D / 3.0D + Math.PI / 2D;
			final double var20 = var32.getInterpolatedU(8.0D);
			double var22 = var32.getMaxU();

			if ((var16 & 1 << var17) != 0) {
				var22 = var32.getMinU();
			}

			final double var24 = par2 + 0.5D;
			final double var26 = par2 + 0.5D + Math.sin(var18) * 8.0D / 16.0D;
			final double var28 = par4 + 0.5D;
			final double var30 = par4 + 0.5D + Math.cos(var18) * 8.0D / 16.0D;
			var5.addVertexWithUV(var24, par3 + 1, var28, var20, var33);
			var5.addVertexWithUV(var24, par3 + 0, var28, var20, var14);
			var5.addVertexWithUV(var26, par3 + 0, var30, var22, var14);
			var5.addVertexWithUV(var26, par3 + 1, var30, var22, var33);
			var5.addVertexWithUV(var26, par3 + 1, var30, var22, var33);
			var5.addVertexWithUV(var26, par3 + 0, var30, var22, var14);
			var5.addVertexWithUV(var24, par3 + 0, var28, var20, var14);
			var5.addVertexWithUV(var24, par3 + 1, var28, var20, var33);
		}

		par1BlockBrewingStand.setBlockBoundsForItemRender();
		return true;
	}

	/**
	 * Render block cauldron
	 */
	public boolean renderBlockCauldron(final BlockCauldron par1BlockCauldron,
			final int par2, final int par3, final int par4) {
		renderStandardBlock(par1BlockCauldron, par2, par3, par4);
		final Tessellator var5 = Tessellator.instance;
		var5.setBrightness(par1BlockCauldron.getMixedBrightnessForBlock(
				blockAccess, par2, par3, par4));
		final float var6 = 1.0F;
		final int var7 = par1BlockCauldron.colorMultiplier(blockAccess, par2,
				par3, par4);
		float var8 = (var7 >> 16 & 255) / 255.0F;
		float var9 = (var7 >> 8 & 255) / 255.0F;
		float var10 = (var7 & 255) / 255.0F;
		float var11;

		if (EntityRenderer.anaglyphEnable) {
			final float var12 = (var8 * 30.0F + var9 * 59.0F + var10 * 11.0F) / 100.0F;
			var11 = (var8 * 30.0F + var9 * 70.0F) / 100.0F;
			final float var13 = (var8 * 30.0F + var10 * 70.0F) / 100.0F;
			var8 = var12;
			var9 = var11;
			var10 = var13;
		}

		var5.setColorOpaque_F(var6 * var8, var6 * var9, var6 * var10);
		final Icon var20 = par1BlockCauldron.getBlockTextureFromSide(2);
		var11 = 0.125F;
		renderFaceXPos(par1BlockCauldron, par2 - 1.0F + var11, par3, par4,
				var20);
		renderFaceXNeg(par1BlockCauldron, par2 + 1.0F - var11, par3, par4,
				var20);
		renderFaceZPos(par1BlockCauldron, par2, par3, par4 - 1.0F + var11,
				var20);
		renderFaceZNeg(par1BlockCauldron, par2, par3, par4 + 1.0F - var11,
				var20);
		final Icon var21 = BlockCauldron.func_94375_b("cauldron_inner");
		renderFaceYPos(par1BlockCauldron, par2, par3 - 1.0F + 0.25F, par4,
				var21);
		renderFaceYNeg(par1BlockCauldron, par2, par3 + 1.0F - 0.75F, par4,
				var21);
		int var14 = blockAccess.getBlockMetadata(par2, par3, par4);

		if (var14 > 0) {
			final Icon var15 = BlockFluid.func_94424_b("water");

			if (var14 > 3) {
				var14 = 3;
			}

			final int var16 = CustomColorizer.getFluidColor(Block.waterStill,
					blockAccess, par2, par3, par4);
			final float var17 = (var16 >> 16 & 255) / 255.0F;
			final float var18 = (var16 >> 8 & 255) / 255.0F;
			final float var19 = (var16 & 255) / 255.0F;
			var5.setColorOpaque_F(var17, var18, var19);
			renderFaceYPos(par1BlockCauldron, par2, par3 - 1.0F
					+ (6.0F + var14 * 3.0F) / 16.0F, par4, var15);
		}

		return true;
	}

	/**
	 * Renders flower pot
	 */
	public boolean renderBlockFlowerpot(
			final BlockFlowerPot par1BlockFlowerPot, final int par2,
			final int par3, final int par4) {
		renderStandardBlock(par1BlockFlowerPot, par2, par3, par4);
		final Tessellator var5 = Tessellator.instance;
		var5.setBrightness(par1BlockFlowerPot.getMixedBrightnessForBlock(
				blockAccess, par2, par3, par4));
		final float var6 = 1.0F;
		int var7 = par1BlockFlowerPot.colorMultiplier(blockAccess, par2, par3,
				par4);
		final Icon var8 = getBlockIconFromSide(par1BlockFlowerPot, 0);
		float var9 = (var7 >> 16 & 255) / 255.0F;
		float var10 = (var7 >> 8 & 255) / 255.0F;
		float var11 = (var7 & 255) / 255.0F;
		float var12;
		float var13;

		if (EntityRenderer.anaglyphEnable) {
			var12 = (var9 * 30.0F + var10 * 59.0F + var11 * 11.0F) / 100.0F;
			final float var14 = (var9 * 30.0F + var10 * 70.0F) / 100.0F;
			var13 = (var9 * 30.0F + var11 * 70.0F) / 100.0F;
			var9 = var12;
			var10 = var14;
			var11 = var13;
		}

		var5.setColorOpaque_F(var6 * var9, var6 * var10, var6 * var11);
		var12 = 0.1865F;
		renderFaceXPos(par1BlockFlowerPot, par2 - 0.5F + var12, par3, par4,
				var8);
		renderFaceXNeg(par1BlockFlowerPot, par2 + 0.5F - var12, par3, par4,
				var8);
		renderFaceZPos(par1BlockFlowerPot, par2, par3, par4 - 0.5F + var12,
				var8);
		renderFaceZNeg(par1BlockFlowerPot, par2, par3, par4 + 0.5F - var12,
				var8);
		renderFaceYPos(par1BlockFlowerPot, par2, par3 - 0.5F + var12 + 0.1875F,
				par4, this.getBlockIcon(Block.dirt));
		final int var19 = blockAccess.getBlockMetadata(par2, par3, par4);

		if (var19 != 0) {
			var13 = 0.0F;
			final float var15 = 4.0F;
			final float var16 = 0.0F;
			BlockFlower var17 = null;

			switch (var19) {
			case 1:
				var17 = Block.plantRed;
				break;

			case 2:
				var17 = Block.plantYellow;

			case 3:
			case 4:
			case 5:
			case 6:
			default:
				break;

			case 7:
				var17 = Block.mushroomRed;
				break;

			case 8:
				var17 = Block.mushroomBrown;
			}

			var5.addTranslation(var13 / 16.0F, var15 / 16.0F, var16 / 16.0F);

			if (var17 != null) {
				renderBlockByRenderType(var17, par2, par3, par4);
			} else if (var19 == 9) {
				renderAllFaces = true;
				final float var18 = 0.125F;
				setRenderBounds(0.5F - var18, 0.0D, 0.5F - var18, 0.5F + var18,
						0.25D, 0.5F + var18);
				renderStandardBlock(Block.cactus, par2, par3, par4);
				setRenderBounds(0.5F - var18, 0.25D, 0.5F - var18,
						0.5F + var18, 0.5D, 0.5F + var18);
				renderStandardBlock(Block.cactus, par2, par3, par4);
				setRenderBounds(0.5F - var18, 0.5D, 0.5F - var18, 0.5F + var18,
						0.75D, 0.5F + var18);
				renderStandardBlock(Block.cactus, par2, par3, par4);
				renderAllFaces = false;
				setRenderBounds(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
			} else if (var19 == 3) {
				drawCrossedSquares(Block.sapling, 0, par2, par3, par4, 0.75F);
			} else if (var19 == 5) {
				drawCrossedSquares(Block.sapling, 2, par2, par3, par4, 0.75F);
			} else if (var19 == 4) {
				drawCrossedSquares(Block.sapling, 1, par2, par3, par4, 0.75F);
			} else if (var19 == 6) {
				drawCrossedSquares(Block.sapling, 3, par2, par3, par4, 0.75F);
			} else if (var19 == 11) {
				var7 = Block.tallGrass.colorMultiplier(blockAccess, par2, par3,
						par4);
				var9 = (var7 >> 16 & 255) / 255.0F;
				var10 = (var7 >> 8 & 255) / 255.0F;
				var11 = (var7 & 255) / 255.0F;
				var5.setColorOpaque_F(var6 * var9, var6 * var10, var6 * var11);
				drawCrossedSquares(Block.tallGrass, 2, par2, par3, par4, 0.75F);
			} else if (var19 == 10) {
				drawCrossedSquares(Block.deadBush, 2, par2, par3, par4, 0.75F);
			}

			var5.addTranslation(-var13 / 16.0F, -var15 / 16.0F, -var16 / 16.0F);
		}

		return true;
	}

	/**
	 * Renders anvil
	 */
	public boolean renderBlockAnvil(final BlockAnvil par1BlockAnvil,
			final int par2, final int par3, final int par4) {
		return renderBlockAnvilMetadata(par1BlockAnvil, par2, par3, par4,
				blockAccess.getBlockMetadata(par2, par3, par4));
	}

	/**
	 * Renders anvil block with metadata
	 */
	public boolean renderBlockAnvilMetadata(final BlockAnvil par1BlockAnvil,
			final int par2, final int par3, final int par4, final int par5) {
		final Tessellator var6 = Tessellator.instance;
		var6.setBrightness(par1BlockAnvil.getMixedBrightnessForBlock(
				blockAccess, par2, par3, par4));
		final float var7 = 1.0F;
		final int var8 = par1BlockAnvil.colorMultiplier(blockAccess, par2,
				par3, par4);
		float var9 = (var8 >> 16 & 255) / 255.0F;
		float var10 = (var8 >> 8 & 255) / 255.0F;
		float var11 = (var8 & 255) / 255.0F;

		if (EntityRenderer.anaglyphEnable) {
			final float var12 = (var9 * 30.0F + var10 * 59.0F + var11 * 11.0F) / 100.0F;
			final float var13 = (var9 * 30.0F + var10 * 70.0F) / 100.0F;
			final float var14 = (var9 * 30.0F + var11 * 70.0F) / 100.0F;
			var9 = var12;
			var10 = var13;
			var11 = var14;
		}

		var6.setColorOpaque_F(var7 * var9, var7 * var10, var7 * var11);
		return renderBlockAnvilOrient(par1BlockAnvil, par2, par3, par4, par5,
				false);
	}

	/**
	 * Renders anvil block with orientation
	 */
	public boolean renderBlockAnvilOrient(final BlockAnvil par1BlockAnvil,
			final int par2, final int par3, final int par4, final int par5,
			final boolean par6) {
		final int var7 = par6 ? 0 : par5 & 3;
		boolean var8 = false;
		float var9 = 0.0F;

		switch (var7) {
		case 0:
			uvRotateSouth = 2;
			uvRotateNorth = 1;
			uvRotateTop = 3;
			uvRotateBottom = 3;
			break;

		case 1:
			uvRotateEast = 1;
			uvRotateWest = 2;
			uvRotateTop = 2;
			uvRotateBottom = 1;
			var8 = true;
			break;

		case 2:
			uvRotateSouth = 1;
			uvRotateNorth = 2;
			break;

		case 3:
			uvRotateEast = 2;
			uvRotateWest = 1;
			uvRotateTop = 1;
			uvRotateBottom = 2;
			var8 = true;
		}

		var9 = renderBlockAnvilRotate(par1BlockAnvil, par2, par3, par4, 0,
				var9, 0.75F, 0.25F, 0.75F, var8, par6, par5);
		var9 = renderBlockAnvilRotate(par1BlockAnvil, par2, par3, par4, 1,
				var9, 0.5F, 0.0625F, 0.625F, var8, par6, par5);
		var9 = renderBlockAnvilRotate(par1BlockAnvil, par2, par3, par4, 2,
				var9, 0.25F, 0.3125F, 0.5F, var8, par6, par5);
		renderBlockAnvilRotate(par1BlockAnvil, par2, par3, par4, 3, var9,
				0.625F, 0.375F, 1.0F, var8, par6, par5);
		setRenderBounds(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
		uvRotateEast = 0;
		uvRotateWest = 0;
		uvRotateSouth = 0;
		uvRotateNorth = 0;
		uvRotateTop = 0;
		uvRotateBottom = 0;
		return true;
	}

	/**
	 * Renders anvil block with rotation
	 */
	public float renderBlockAnvilRotate(final BlockAnvil par1BlockAnvil,
			final int par2, final int par3, final int par4, final int par5,
			final float par6, float par7, final float par8, float par9,
			final boolean par10, final boolean par11, final int par12) {
		if (par10) {
			final float var13 = par7;
			par7 = par9;
			par9 = var13;
		}

		par7 /= 2.0F;
		par9 /= 2.0F;
		par1BlockAnvil.field_82521_b = par5;
		setRenderBounds(0.5F - par7, par6, 0.5F - par9, 0.5F + par7, par6
				+ par8, 0.5F + par9);

		if (par11) {
			final Tessellator var14 = Tessellator.instance;
			var14.startDrawingQuads();
			var14.setNormal(0.0F, -1.0F, 0.0F);
			renderFaceYNeg(par1BlockAnvil, 0.0D, 0.0D, 0.0D,
					getBlockIconFromSideAndMetadata(par1BlockAnvil, 0, par12));
			var14.draw();
			var14.startDrawingQuads();
			var14.setNormal(0.0F, 1.0F, 0.0F);
			renderFaceYPos(par1BlockAnvil, 0.0D, 0.0D, 0.0D,
					getBlockIconFromSideAndMetadata(par1BlockAnvil, 1, par12));
			var14.draw();
			var14.startDrawingQuads();
			var14.setNormal(0.0F, 0.0F, -1.0F);
			renderFaceZNeg(par1BlockAnvil, 0.0D, 0.0D, 0.0D,
					getBlockIconFromSideAndMetadata(par1BlockAnvil, 2, par12));
			var14.draw();
			var14.startDrawingQuads();
			var14.setNormal(0.0F, 0.0F, 1.0F);
			renderFaceZPos(par1BlockAnvil, 0.0D, 0.0D, 0.0D,
					getBlockIconFromSideAndMetadata(par1BlockAnvil, 3, par12));
			var14.draw();
			var14.startDrawingQuads();
			var14.setNormal(-1.0F, 0.0F, 0.0F);
			renderFaceXNeg(par1BlockAnvil, 0.0D, 0.0D, 0.0D,
					getBlockIconFromSideAndMetadata(par1BlockAnvil, 4, par12));
			var14.draw();
			var14.startDrawingQuads();
			var14.setNormal(1.0F, 0.0F, 0.0F);
			renderFaceXPos(par1BlockAnvil, 0.0D, 0.0D, 0.0D,
					getBlockIconFromSideAndMetadata(par1BlockAnvil, 5, par12));
			var14.draw();
		} else {
			renderStandardBlock(par1BlockAnvil, par2, par3, par4);
		}

		return par6 + par8;
	}

	/**
	 * Renders a torch block at the given coordinates
	 */
	public boolean renderBlockTorch(final Block par1Block, final int par2,
			final int par3, final int par4) {
		final int var5 = blockAccess.getBlockMetadata(par2, par3, par4);
		final Tessellator var6 = Tessellator.instance;
		var6.setBrightness(par1Block.getMixedBrightnessForBlock(blockAccess,
				par2, par3, par4));
		var6.setColorOpaque_F(1.0F, 1.0F, 1.0F);
		final double var7 = 0.4000000059604645D;
		final double var9 = 0.5D - var7;
		final double var11 = 0.20000000298023224D;

		if (var5 == 1) {
			renderTorchAtAngle(par1Block, par2 - var9, par3 + var11, par4,
					-var7, 0.0D, 0);
		} else if (var5 == 2) {
			renderTorchAtAngle(par1Block, par2 + var9, par3 + var11, par4,
					var7, 0.0D, 0);
		} else if (var5 == 3) {
			renderTorchAtAngle(par1Block, par2, par3 + var11, par4 - var9,
					0.0D, -var7, 0);
		} else if (var5 == 4) {
			renderTorchAtAngle(par1Block, par2, par3 + var11, par4 + var9,
					0.0D, var7, 0);
		} else {
			renderTorchAtAngle(par1Block, par2, par3, par4, 0.0D, 0.0D, 0);

			if (par1Block != Block.torchWood && Config.isBetterSnow()
					&& hasSnowNeighbours(par2, par3, par4)) {
				renderSnow(par2, par3, par4, Block.snow.maxY);
			}
		}

		return true;
	}

	/**
	 * render a redstone repeater at the given coordinates
	 */
	public boolean renderBlockRepeater(
			final BlockRedstoneRepeater par1BlockRedstoneRepeater,
			final int par2, final int par3, final int par4) {
		final int var5 = blockAccess.getBlockMetadata(par2, par3, par4);
		final int var6 = var5 & 3;
		final int var7 = (var5 & 12) >> 2;
		final Tessellator var8 = Tessellator.instance;
		var8.setBrightness(par1BlockRedstoneRepeater
				.getMixedBrightnessForBlock(blockAccess, par2, par3, par4));
		var8.setColorOpaque_F(1.0F, 1.0F, 1.0F);
		final double var9 = -0.1875D;
		final boolean var11 = par1BlockRedstoneRepeater.func_94476_e(
				blockAccess, par2, par3, par4, var5);
		double var12 = 0.0D;
		double var14 = 0.0D;
		double var16 = 0.0D;
		double var18 = 0.0D;

		switch (var6) {
		case 0:
			var18 = -0.3125D;
			var14 = BlockRedstoneRepeater.repeaterTorchOffset[var7];
			break;

		case 1:
			var16 = 0.3125D;
			var12 = -BlockRedstoneRepeater.repeaterTorchOffset[var7];
			break;

		case 2:
			var18 = 0.3125D;
			var14 = -BlockRedstoneRepeater.repeaterTorchOffset[var7];
			break;

		case 3:
			var16 = -0.3125D;
			var12 = BlockRedstoneRepeater.repeaterTorchOffset[var7];
		}

		if (!var11) {
			renderTorchAtAngle(par1BlockRedstoneRepeater, par2 + var12, par3
					+ var9, par4 + var14, 0.0D, 0.0D, 0);
		} else {
			final Icon var20 = this.getBlockIcon(Block.bedrock);
			setOverrideBlockTexture(var20);
			float var21 = 2.0F;
			float var22 = 14.0F;
			float var23 = 7.0F;
			float var24 = 9.0F;

			switch (var6) {
			case 1:
			case 3:
				var21 = 7.0F;
				var22 = 9.0F;
				var23 = 2.0F;
				var24 = 14.0F;

			case 0:
			case 2:
			default:
				setRenderBounds(var21 / 16.0F + (float) var12, 0.125D, var23
						/ 16.0F + (float) var14, var22 / 16.0F + (float) var12,
						0.25D, var24 / 16.0F + (float) var14);
				final double var25 = var20.getInterpolatedU(var21);
				final double var27 = var20.getInterpolatedV(var23);
				final double var29 = var20.getInterpolatedU(var22);
				final double var31 = var20.getInterpolatedV(var24);
				var8.addVertexWithUV(par2 + var21 / 16.0F + var12,
						par3 + 0.25F, par4 + var23 / 16.0F + var14, var25,
						var27);
				var8.addVertexWithUV(par2 + var21 / 16.0F + var12,
						par3 + 0.25F, par4 + var24 / 16.0F + var14, var25,
						var31);
				var8.addVertexWithUV(par2 + var22 / 16.0F + var12,
						par3 + 0.25F, par4 + var24 / 16.0F + var14, var29,
						var31);
				var8.addVertexWithUV(par2 + var22 / 16.0F + var12,
						par3 + 0.25F, par4 + var23 / 16.0F + var14, var29,
						var27);
				renderStandardBlock(par1BlockRedstoneRepeater, par2, par3, par4);
				setRenderBounds(0.0D, 0.0D, 0.0D, 1.0D, 0.125D, 1.0D);
				clearOverrideBlockTexture();
			}
		}

		var8.setBrightness(par1BlockRedstoneRepeater
				.getMixedBrightnessForBlock(blockAccess, par2, par3, par4));
		var8.setColorOpaque_F(1.0F, 1.0F, 1.0F);
		renderTorchAtAngle(par1BlockRedstoneRepeater, par2 + var16,
				par3 + var9, par4 + var18, 0.0D, 0.0D, 0);
		renderBlockRedstoneLogic(par1BlockRedstoneRepeater, par2, par3, par4);
		return true;
	}

	public boolean renderBlockComparator(
			final BlockComparator par1BlockComparator, final int par2,
			final int par3, final int par4) {
		final Tessellator var5 = Tessellator.instance;
		var5.setBrightness(par1BlockComparator.getMixedBrightnessForBlock(
				blockAccess, par2, par3, par4));
		var5.setColorOpaque_F(1.0F, 1.0F, 1.0F);
		final int var6 = blockAccess.getBlockMetadata(par2, par3, par4);
		final int var7 = var6 & 3;
		double var8 = 0.0D;
		double var10 = -0.1875D;
		double var12 = 0.0D;
		double var14 = 0.0D;
		double var16 = 0.0D;
		Icon var18;

		if (par1BlockComparator.func_94490_c(var6)) {
			var18 = Block.torchRedstoneActive.getBlockTextureFromSide(0);
		} else {
			var10 -= 0.1875D;
			var18 = Block.torchRedstoneIdle.getBlockTextureFromSide(0);
		}

		switch (var7) {
		case 0:
			var12 = -0.3125D;
			var16 = 1.0D;
			break;

		case 1:
			var8 = 0.3125D;
			var14 = -1.0D;
			break;

		case 2:
			var12 = 0.3125D;
			var16 = -1.0D;
			break;

		case 3:
			var8 = -0.3125D;
			var14 = 1.0D;
		}

		renderTorchAtAngle(par1BlockComparator, par2 + 0.25D * var14 + 0.1875D
				* var16, par3 - 0.1875F,
				par4 + 0.25D * var16 + 0.1875D * var14, 0.0D, 0.0D, var6);
		renderTorchAtAngle(par1BlockComparator, par2 + 0.25D * var14 + -0.1875D
				* var16, par3 - 0.1875F, par4 + 0.25D * var16 + -0.1875D
				* var14, 0.0D, 0.0D, var6);
		setOverrideBlockTexture(var18);
		renderTorchAtAngle(par1BlockComparator, par2 + var8, par3 + var10, par4
				+ var12, 0.0D, 0.0D, var6);
		clearOverrideBlockTexture();
		renderBlockRedstoneLogicMetadata(par1BlockComparator, par2, par3, par4,
				var7);
		return true;
	}

	public boolean renderBlockRedstoneLogic(
			final BlockRedstoneLogic par1BlockRedstoneLogic, final int par2,
			final int par3, final int par4) {
		renderBlockRedstoneLogicMetadata(par1BlockRedstoneLogic, par2, par3,
				par4, blockAccess.getBlockMetadata(par2, par3, par4) & 3);
		return true;
	}

	public void renderBlockRedstoneLogicMetadata(
			final BlockRedstoneLogic par1BlockRedstoneLogic, final int par2,
			final int par3, final int par4, final int par5) {
		renderStandardBlock(par1BlockRedstoneLogic, par2, par3, par4);
		final Tessellator var6 = Tessellator.instance;
		var6.setBrightness(par1BlockRedstoneLogic.getMixedBrightnessForBlock(
				blockAccess, par2, par3, par4));
		var6.setColorOpaque_F(1.0F, 1.0F, 1.0F);
		final int var7 = blockAccess.getBlockMetadata(par2, par3, par4);
		final Icon var8 = getBlockIconFromSideAndMetadata(
				par1BlockRedstoneLogic, 1, var7);
		final double var9 = var8.getMinU();
		final double var11 = var8.getMaxU();
		final double var13 = var8.getMinV();
		final double var15 = var8.getMaxV();
		final double var17 = 0.125D;
		double var19 = par2 + 1;
		double var21 = par2 + 1;
		double var23 = par2 + 0;
		double var25 = par2 + 0;
		double var27 = par4 + 0;
		double var29 = par4 + 1;
		double var31 = par4 + 1;
		double var33 = par4 + 0;
		final double var35 = par3 + var17;

		if (par5 == 2) {
			var19 = var21 = par2 + 0;
			var23 = var25 = par2 + 1;
			var27 = var33 = par4 + 1;
			var29 = var31 = par4 + 0;
		} else if (par5 == 3) {
			var19 = var25 = par2 + 0;
			var21 = var23 = par2 + 1;
			var27 = var29 = par4 + 0;
			var31 = var33 = par4 + 1;
		} else if (par5 == 1) {
			var19 = var25 = par2 + 1;
			var21 = var23 = par2 + 0;
			var27 = var29 = par4 + 1;
			var31 = var33 = par4 + 0;
		}

		var6.addVertexWithUV(var25, var35, var33, var9, var13);
		var6.addVertexWithUV(var23, var35, var31, var9, var15);
		var6.addVertexWithUV(var21, var35, var29, var11, var15);
		var6.addVertexWithUV(var19, var35, var27, var11, var13);
	}

	/**
	 * Render all faces of the piston base
	 */
	public void renderPistonBaseAllFaces(final Block par1Block, final int par2,
			final int par3, final int par4) {
		renderAllFaces = true;
		renderPistonBase(par1Block, par2, par3, par4, true);
		renderAllFaces = false;
	}

	/**
	 * renders a block as a piston base
	 */
	public boolean renderPistonBase(final Block par1Block, final int par2,
			final int par3, final int par4, final boolean par5) {
		final int var6 = blockAccess.getBlockMetadata(par2, par3, par4);
		final boolean var7 = par5 || (var6 & 8) != 0;
		final int var8 = BlockPistonBase.getOrientation(var6);

		if (var7) {
			switch (var8) {
			case 0:
				uvRotateEast = 3;
				uvRotateWest = 3;
				uvRotateSouth = 3;
				uvRotateNorth = 3;
				setRenderBounds(0.0D, 0.25D, 0.0D, 1.0D, 1.0D, 1.0D);
				break;

			case 1:
				setRenderBounds(0.0D, 0.0D, 0.0D, 1.0D, 0.75D, 1.0D);
				break;

			case 2:
				uvRotateSouth = 1;
				uvRotateNorth = 2;
				setRenderBounds(0.0D, 0.0D, 0.25D, 1.0D, 1.0D, 1.0D);
				break;

			case 3:
				uvRotateSouth = 2;
				uvRotateNorth = 1;
				uvRotateTop = 3;
				uvRotateBottom = 3;
				setRenderBounds(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 0.75D);
				break;

			case 4:
				uvRotateEast = 1;
				uvRotateWest = 2;
				uvRotateTop = 2;
				uvRotateBottom = 1;
				setRenderBounds(0.25D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
				break;

			case 5:
				uvRotateEast = 2;
				uvRotateWest = 1;
				uvRotateTop = 1;
				uvRotateBottom = 2;
				setRenderBounds(0.0D, 0.0D, 0.0D, 0.75D, 1.0D, 1.0D);
			}

			((BlockPistonBase) par1Block).func_96479_b((float) renderMinX,
					(float) renderMinY, (float) renderMinZ, (float) renderMaxX,
					(float) renderMaxY, (float) renderMaxZ);
			renderStandardBlock(par1Block, par2, par3, par4);
			uvRotateEast = 0;
			uvRotateWest = 0;
			uvRotateSouth = 0;
			uvRotateNorth = 0;
			uvRotateTop = 0;
			uvRotateBottom = 0;
			setRenderBounds(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
			((BlockPistonBase) par1Block).func_96479_b((float) renderMinX,
					(float) renderMinY, (float) renderMinZ, (float) renderMaxX,
					(float) renderMaxY, (float) renderMaxZ);
		} else {
			switch (var8) {
			case 0:
				uvRotateEast = 3;
				uvRotateWest = 3;
				uvRotateSouth = 3;
				uvRotateNorth = 3;

			case 1:
			default:
				break;

			case 2:
				uvRotateSouth = 1;
				uvRotateNorth = 2;
				break;

			case 3:
				uvRotateSouth = 2;
				uvRotateNorth = 1;
				uvRotateTop = 3;
				uvRotateBottom = 3;
				break;

			case 4:
				uvRotateEast = 1;
				uvRotateWest = 2;
				uvRotateTop = 2;
				uvRotateBottom = 1;
				break;

			case 5:
				uvRotateEast = 2;
				uvRotateWest = 1;
				uvRotateTop = 1;
				uvRotateBottom = 2;
			}

			renderStandardBlock(par1Block, par2, par3, par4);
			uvRotateEast = 0;
			uvRotateWest = 0;
			uvRotateSouth = 0;
			uvRotateNorth = 0;
			uvRotateTop = 0;
			uvRotateBottom = 0;
		}

		return true;
	}

	/**
	 * Render piston rod up/down
	 */
	public void renderPistonRodUD(final double par1, final double par3,
			final double par5, final double par7, final double par9,
			final double par11, final float par13, final double par14) {
		Icon var16 = BlockPistonBase.func_94496_b("piston_side");

		if (hasOverrideBlockTexture()) {
			var16 = overrideBlockTexture;
		}

		final Tessellator var17 = Tessellator.instance;
		final double var18 = var16.getMinU();
		final double var20 = var16.getMinV();
		final double var22 = var16.getInterpolatedU(par14);
		final double var24 = var16.getInterpolatedV(4.0D);
		var17.setColorOpaque_F(par13, par13, par13);
		var17.addVertexWithUV(par1, par7, par9, var22, var20);
		var17.addVertexWithUV(par1, par5, par9, var18, var20);
		var17.addVertexWithUV(par3, par5, par11, var18, var24);
		var17.addVertexWithUV(par3, par7, par11, var22, var24);
	}

	/**
	 * Render piston rod south/north
	 */
	public void renderPistonRodSN(final double par1, final double par3,
			final double par5, final double par7, final double par9,
			final double par11, final float par13, final double par14) {
		Icon var16 = BlockPistonBase.func_94496_b("piston_side");

		if (hasOverrideBlockTexture()) {
			var16 = overrideBlockTexture;
		}

		final Tessellator var17 = Tessellator.instance;
		final double var18 = var16.getMinU();
		final double var20 = var16.getMinV();
		final double var22 = var16.getInterpolatedU(par14);
		final double var24 = var16.getInterpolatedV(4.0D);
		var17.setColorOpaque_F(par13, par13, par13);
		var17.addVertexWithUV(par1, par5, par11, var22, var20);
		var17.addVertexWithUV(par1, par5, par9, var18, var20);
		var17.addVertexWithUV(par3, par7, par9, var18, var24);
		var17.addVertexWithUV(par3, par7, par11, var22, var24);
	}

	/**
	 * Render piston rod east/west
	 */
	public void renderPistonRodEW(final double par1, final double par3,
			final double par5, final double par7, final double par9,
			final double par11, final float par13, final double par14) {
		Icon var16 = BlockPistonBase.func_94496_b("piston_side");

		if (hasOverrideBlockTexture()) {
			var16 = overrideBlockTexture;
		}

		final Tessellator var17 = Tessellator.instance;
		final double var18 = var16.getMinU();
		final double var20 = var16.getMinV();
		final double var22 = var16.getInterpolatedU(par14);
		final double var24 = var16.getInterpolatedV(4.0D);
		var17.setColorOpaque_F(par13, par13, par13);
		var17.addVertexWithUV(par3, par5, par9, var22, var20);
		var17.addVertexWithUV(par1, par5, par9, var18, var20);
		var17.addVertexWithUV(par1, par7, par11, var18, var24);
		var17.addVertexWithUV(par3, par7, par11, var22, var24);
	}

	/**
	 * Render all faces of the piston extension
	 */
	public void renderPistonExtensionAllFaces(final Block par1Block,
			final int par2, final int par3, final int par4, final boolean par5) {
		renderAllFaces = true;
		renderPistonExtension(par1Block, par2, par3, par4, par5);
		renderAllFaces = false;
	}

	/**
	 * renders the pushing part of a piston
	 */
	public boolean renderPistonExtension(final Block par1Block, final int par2,
			final int par3, final int par4, final boolean par5) {
		final int var6 = blockAccess.getBlockMetadata(par2, par3, par4);
		final int var7 = BlockPistonExtension.getDirectionMeta(var6);
		final float var8 = par1Block.getBlockBrightness(blockAccess, par2,
				par3, par4);
		final float var9 = par5 ? 1.0F : 0.5F;
		final double var10 = par5 ? 16.0D : 8.0D;

		switch (var7) {
		case 0:
			uvRotateEast = 3;
			uvRotateWest = 3;
			uvRotateSouth = 3;
			uvRotateNorth = 3;
			setRenderBounds(0.0D, 0.0D, 0.0D, 1.0D, 0.25D, 1.0D);
			renderStandardBlock(par1Block, par2, par3, par4);
			renderPistonRodUD(par2 + 0.375F, par2 + 0.625F, par3 + 0.25F, par3
					+ 0.25F + var9, par4 + 0.625F, par4 + 0.625F, var8 * 0.8F,
					var10);
			renderPistonRodUD(par2 + 0.625F, par2 + 0.375F, par3 + 0.25F, par3
					+ 0.25F + var9, par4 + 0.375F, par4 + 0.375F, var8 * 0.8F,
					var10);
			renderPistonRodUD(par2 + 0.375F, par2 + 0.375F, par3 + 0.25F, par3
					+ 0.25F + var9, par4 + 0.375F, par4 + 0.625F, var8 * 0.6F,
					var10);
			renderPistonRodUD(par2 + 0.625F, par2 + 0.625F, par3 + 0.25F, par3
					+ 0.25F + var9, par4 + 0.625F, par4 + 0.375F, var8 * 0.6F,
					var10);
			break;

		case 1:
			setRenderBounds(0.0D, 0.75D, 0.0D, 1.0D, 1.0D, 1.0D);
			renderStandardBlock(par1Block, par2, par3, par4);
			renderPistonRodUD(par2 + 0.375F, par2 + 0.625F, par3 - 0.25F + 1.0F
					- var9, par3 - 0.25F + 1.0F, par4 + 0.625F, par4 + 0.625F,
					var8 * 0.8F, var10);
			renderPistonRodUD(par2 + 0.625F, par2 + 0.375F, par3 - 0.25F + 1.0F
					- var9, par3 - 0.25F + 1.0F, par4 + 0.375F, par4 + 0.375F,
					var8 * 0.8F, var10);
			renderPistonRodUD(par2 + 0.375F, par2 + 0.375F, par3 - 0.25F + 1.0F
					- var9, par3 - 0.25F + 1.0F, par4 + 0.375F, par4 + 0.625F,
					var8 * 0.6F, var10);
			renderPistonRodUD(par2 + 0.625F, par2 + 0.625F, par3 - 0.25F + 1.0F
					- var9, par3 - 0.25F + 1.0F, par4 + 0.625F, par4 + 0.375F,
					var8 * 0.6F, var10);
			break;

		case 2:
			uvRotateSouth = 1;
			uvRotateNorth = 2;
			setRenderBounds(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 0.25D);
			renderStandardBlock(par1Block, par2, par3, par4);
			renderPistonRodSN(par2 + 0.375F, par2 + 0.375F, par3 + 0.625F,
					par3 + 0.375F, par4 + 0.25F, par4 + 0.25F + var9,
					var8 * 0.6F, var10);
			renderPistonRodSN(par2 + 0.625F, par2 + 0.625F, par3 + 0.375F,
					par3 + 0.625F, par4 + 0.25F, par4 + 0.25F + var9,
					var8 * 0.6F, var10);
			renderPistonRodSN(par2 + 0.375F, par2 + 0.625F, par3 + 0.375F,
					par3 + 0.375F, par4 + 0.25F, par4 + 0.25F + var9,
					var8 * 0.5F, var10);
			renderPistonRodSN(par2 + 0.625F, par2 + 0.375F, par3 + 0.625F,
					par3 + 0.625F, par4 + 0.25F, par4 + 0.25F + var9, var8,
					var10);
			break;

		case 3:
			uvRotateSouth = 2;
			uvRotateNorth = 1;
			uvRotateTop = 3;
			uvRotateBottom = 3;
			setRenderBounds(0.0D, 0.0D, 0.75D, 1.0D, 1.0D, 1.0D);
			renderStandardBlock(par1Block, par2, par3, par4);
			renderPistonRodSN(par2 + 0.375F, par2 + 0.375F, par3 + 0.625F,
					par3 + 0.375F, par4 - 0.25F + 1.0F - var9,
					par4 - 0.25F + 1.0F, var8 * 0.6F, var10);
			renderPistonRodSN(par2 + 0.625F, par2 + 0.625F, par3 + 0.375F,
					par3 + 0.625F, par4 - 0.25F + 1.0F - var9,
					par4 - 0.25F + 1.0F, var8 * 0.6F, var10);
			renderPistonRodSN(par2 + 0.375F, par2 + 0.625F, par3 + 0.375F,
					par3 + 0.375F, par4 - 0.25F + 1.0F - var9,
					par4 - 0.25F + 1.0F, var8 * 0.5F, var10);
			renderPistonRodSN(par2 + 0.625F, par2 + 0.375F, par3 + 0.625F,
					par3 + 0.625F, par4 - 0.25F + 1.0F - var9,
					par4 - 0.25F + 1.0F, var8, var10);
			break;

		case 4:
			uvRotateEast = 1;
			uvRotateWest = 2;
			uvRotateTop = 2;
			uvRotateBottom = 1;
			setRenderBounds(0.0D, 0.0D, 0.0D, 0.25D, 1.0D, 1.0D);
			renderStandardBlock(par1Block, par2, par3, par4);
			renderPistonRodEW(par2 + 0.25F, par2 + 0.25F + var9, par3 + 0.375F,
					par3 + 0.375F, par4 + 0.625F, par4 + 0.375F, var8 * 0.5F,
					var10);
			renderPistonRodEW(par2 + 0.25F, par2 + 0.25F + var9, par3 + 0.625F,
					par3 + 0.625F, par4 + 0.375F, par4 + 0.625F, var8, var10);
			renderPistonRodEW(par2 + 0.25F, par2 + 0.25F + var9, par3 + 0.375F,
					par3 + 0.625F, par4 + 0.375F, par4 + 0.375F, var8 * 0.6F,
					var10);
			renderPistonRodEW(par2 + 0.25F, par2 + 0.25F + var9, par3 + 0.625F,
					par3 + 0.375F, par4 + 0.625F, par4 + 0.625F, var8 * 0.6F,
					var10);
			break;

		case 5:
			uvRotateEast = 2;
			uvRotateWest = 1;
			uvRotateTop = 1;
			uvRotateBottom = 2;
			setRenderBounds(0.75D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
			renderStandardBlock(par1Block, par2, par3, par4);
			renderPistonRodEW(par2 - 0.25F + 1.0F - var9, par2 - 0.25F + 1.0F,
					par3 + 0.375F, par3 + 0.375F, par4 + 0.625F, par4 + 0.375F,
					var8 * 0.5F, var10);
			renderPistonRodEW(par2 - 0.25F + 1.0F - var9, par2 - 0.25F + 1.0F,
					par3 + 0.625F, par3 + 0.625F, par4 + 0.375F, par4 + 0.625F,
					var8, var10);
			renderPistonRodEW(par2 - 0.25F + 1.0F - var9, par2 - 0.25F + 1.0F,
					par3 + 0.375F, par3 + 0.625F, par4 + 0.375F, par4 + 0.375F,
					var8 * 0.6F, var10);
			renderPistonRodEW(par2 - 0.25F + 1.0F - var9, par2 - 0.25F + 1.0F,
					par3 + 0.625F, par3 + 0.375F, par4 + 0.625F, par4 + 0.625F,
					var8 * 0.6F, var10);
		}

		uvRotateEast = 0;
		uvRotateWest = 0;
		uvRotateSouth = 0;
		uvRotateNorth = 0;
		uvRotateTop = 0;
		uvRotateBottom = 0;
		setRenderBounds(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
		return true;
	}

	/**
	 * Renders a lever block at the given coordinates
	 */
	public boolean renderBlockLever(final Block par1Block, final int par2,
			final int par3, final int par4) {
		final int var5 = blockAccess.getBlockMetadata(par2, par3, par4);
		final int var6 = var5 & 7;
		final boolean var7 = (var5 & 8) > 0;
		final Tessellator var8 = Tessellator.instance;
		final boolean var9 = hasOverrideBlockTexture();

		if (!var9) {
			setOverrideBlockTexture(this.getBlockIcon(Block.cobblestone));
		}

		final float var10 = 0.25F;
		final float var11 = 0.1875F;
		final float var12 = 0.1875F;

		if (var6 == 5) {
			setRenderBounds(0.5F - var11, 0.0D, 0.5F - var10, 0.5F + var11,
					var12, 0.5F + var10);
		} else if (var6 == 6) {
			setRenderBounds(0.5F - var10, 0.0D, 0.5F - var11, 0.5F + var10,
					var12, 0.5F + var11);
		} else if (var6 == 4) {
			setRenderBounds(0.5F - var11, 0.5F - var10, 1.0F - var12,
					0.5F + var11, 0.5F + var10, 1.0D);
		} else if (var6 == 3) {
			setRenderBounds(0.5F - var11, 0.5F - var10, 0.0D, 0.5F + var11,
					0.5F + var10, var12);
		} else if (var6 == 2) {
			setRenderBounds(1.0F - var12, 0.5F - var10, 0.5F - var11, 1.0D,
					0.5F + var10, 0.5F + var11);
		} else if (var6 == 1) {
			setRenderBounds(0.0D, 0.5F - var10, 0.5F - var11, var12,
					0.5F + var10, 0.5F + var11);
		} else if (var6 == 0) {
			setRenderBounds(0.5F - var10, 1.0F - var12, 0.5F - var11,
					0.5F + var10, 1.0D, 0.5F + var11);
		} else if (var6 == 7) {
			setRenderBounds(0.5F - var11, 1.0F - var12, 0.5F - var10,
					0.5F + var11, 1.0D, 0.5F + var10);
		}

		renderStandardBlock(par1Block, par2, par3, par4);

		if (!var9) {
			clearOverrideBlockTexture();
		}

		var8.setBrightness(par1Block.getMixedBrightnessForBlock(blockAccess,
				par2, par3, par4));
		float var13 = 1.0F;

		if (Block.lightValue[par1Block.blockID] > 0) {
			var13 = 1.0F;
		}

		var8.setColorOpaque_F(var13, var13, var13);
		Icon var14 = getBlockIconFromSide(par1Block, 0);

		if (hasOverrideBlockTexture()) {
			var14 = overrideBlockTexture;
		}

		double var15 = var14.getMinU();
		double var17 = var14.getMinV();
		double var19 = var14.getMaxU();
		double var21 = var14.getMaxV();
		final Vec3[] var23 = new Vec3[8];
		final float var24 = 0.0625F;
		final float var25 = 0.0625F;
		final float var26 = 0.625F;
		var23[0] = blockAccess.getWorldVec3Pool().getVecFromPool(-var24, 0.0D,
				-var25);
		var23[1] = blockAccess.getWorldVec3Pool().getVecFromPool(var24, 0.0D,
				-var25);
		var23[2] = blockAccess.getWorldVec3Pool().getVecFromPool(var24, 0.0D,
				var25);
		var23[3] = blockAccess.getWorldVec3Pool().getVecFromPool(-var24, 0.0D,
				var25);
		var23[4] = blockAccess.getWorldVec3Pool().getVecFromPool(-var24, var26,
				-var25);
		var23[5] = blockAccess.getWorldVec3Pool().getVecFromPool(var24, var26,
				-var25);
		var23[6] = blockAccess.getWorldVec3Pool().getVecFromPool(var24, var26,
				var25);
		var23[7] = blockAccess.getWorldVec3Pool().getVecFromPool(-var24, var26,
				var25);

		for (int var27 = 0; var27 < 8; ++var27) {
			if (var7) {
				var23[var27].zCoord -= 0.0625D;
				var23[var27].rotateAroundX((float) Math.PI * 2F / 9F);
			} else {
				var23[var27].zCoord += 0.0625D;
				var23[var27].rotateAroundX(-((float) Math.PI * 2F / 9F));
			}

			if (var6 == 0 || var6 == 7) {
				var23[var27].rotateAroundZ((float) Math.PI);
			}

			if (var6 == 6 || var6 == 0) {
				var23[var27].rotateAroundY((float) Math.PI / 2F);
			}

			if (var6 > 0 && var6 < 5) {
				var23[var27].yCoord -= 0.375D;
				var23[var27].rotateAroundX((float) Math.PI / 2F);

				if (var6 == 4) {
					var23[var27].rotateAroundY(0.0F);
				}

				if (var6 == 3) {
					var23[var27].rotateAroundY((float) Math.PI);
				}

				if (var6 == 2) {
					var23[var27].rotateAroundY((float) Math.PI / 2F);
				}

				if (var6 == 1) {
					var23[var27].rotateAroundY(-((float) Math.PI / 2F));
				}

				var23[var27].xCoord += par2 + 0.5D;
				var23[var27].yCoord += par3 + 0.5F;
				var23[var27].zCoord += par4 + 0.5D;
			} else if (var6 != 0 && var6 != 7) {
				var23[var27].xCoord += par2 + 0.5D;
				var23[var27].yCoord += par3 + 0.125F;
				var23[var27].zCoord += par4 + 0.5D;
			} else {
				var23[var27].xCoord += par2 + 0.5D;
				var23[var27].yCoord += par3 + 0.875F;
				var23[var27].zCoord += par4 + 0.5D;
			}
		}

		Vec3 var32 = null;
		Vec3 var28 = null;
		Vec3 var29 = null;
		Vec3 var30 = null;

		for (int var31 = 0; var31 < 6; ++var31) {
			if (var31 == 0) {
				var15 = var14.getInterpolatedU(7.0D);
				var17 = var14.getInterpolatedV(6.0D);
				var19 = var14.getInterpolatedU(9.0D);
				var21 = var14.getInterpolatedV(8.0D);
			} else if (var31 == 2) {
				var15 = var14.getInterpolatedU(7.0D);
				var17 = var14.getInterpolatedV(6.0D);
				var19 = var14.getInterpolatedU(9.0D);
				var21 = var14.getMaxV();
			}

			if (var31 == 0) {
				var32 = var23[0];
				var28 = var23[1];
				var29 = var23[2];
				var30 = var23[3];
			} else if (var31 == 1) {
				var32 = var23[7];
				var28 = var23[6];
				var29 = var23[5];
				var30 = var23[4];
			} else if (var31 == 2) {
				var32 = var23[1];
				var28 = var23[0];
				var29 = var23[4];
				var30 = var23[5];
			} else if (var31 == 3) {
				var32 = var23[2];
				var28 = var23[1];
				var29 = var23[5];
				var30 = var23[6];
			} else if (var31 == 4) {
				var32 = var23[3];
				var28 = var23[2];
				var29 = var23[6];
				var30 = var23[7];
			} else if (var31 == 5) {
				var32 = var23[0];
				var28 = var23[3];
				var29 = var23[7];
				var30 = var23[4];
			}

			var8.addVertexWithUV(var32.xCoord, var32.yCoord, var32.zCoord,
					var15, var21);
			var8.addVertexWithUV(var28.xCoord, var28.yCoord, var28.zCoord,
					var19, var21);
			var8.addVertexWithUV(var29.xCoord, var29.yCoord, var29.zCoord,
					var19, var17);
			var8.addVertexWithUV(var30.xCoord, var30.yCoord, var30.zCoord,
					var15, var17);
		}

		if (Config.isBetterSnow() && hasSnowNeighbours(par2, par3, par4)) {
			renderSnow(par2, par3, par4, Block.snow.maxY);
		}

		return true;
	}

	/**
	 * Renders a trip wire source block at the given coordinates
	 */
	public boolean renderBlockTripWireSource(final Block par1Block,
			final int par2, final int par3, final int par4) {
		final Tessellator var5 = Tessellator.instance;
		final int var6 = blockAccess.getBlockMetadata(par2, par3, par4);
		final int var7 = var6 & 3;
		final boolean var8 = (var6 & 4) == 4;
		final boolean var9 = (var6 & 8) == 8;
		final boolean var10 = !blockAccess.doesBlockHaveSolidTopSurface(par2,
				par3 - 1, par4);
		final boolean var11 = hasOverrideBlockTexture();

		if (!var11) {
			setOverrideBlockTexture(this.getBlockIcon(Block.planks));
		}

		final float var12 = 0.25F;
		final float var13 = 0.125F;
		final float var14 = 0.125F;
		final float var15 = 0.3F - var12;
		final float var16 = 0.3F + var12;

		if (var7 == 2) {
			setRenderBounds(0.5F - var13, var15, 1.0F - var14, 0.5F + var13,
					var16, 1.0D);
		} else if (var7 == 0) {
			setRenderBounds(0.5F - var13, var15, 0.0D, 0.5F + var13, var16,
					var14);
		} else if (var7 == 1) {
			setRenderBounds(1.0F - var14, var15, 0.5F - var13, 1.0D, var16,
					0.5F + var13);
		} else if (var7 == 3) {
			setRenderBounds(0.0D, var15, 0.5F - var13, var14, var16,
					0.5F + var13);
		}

		renderStandardBlock(par1Block, par2, par3, par4);

		if (!var11) {
			clearOverrideBlockTexture();
		}

		var5.setBrightness(par1Block.getMixedBrightnessForBlock(blockAccess,
				par2, par3, par4));
		float var17 = 1.0F;

		if (Block.lightValue[par1Block.blockID] > 0) {
			var17 = 1.0F;
		}

		var5.setColorOpaque_F(var17, var17, var17);
		Icon var18 = getBlockIconFromSide(par1Block, 0);

		if (hasOverrideBlockTexture()) {
			var18 = overrideBlockTexture;
		}

		double var19 = var18.getMinU();
		double var21 = var18.getMinV();
		double var23 = var18.getMaxU();
		double var25 = var18.getMaxV();
		final Vec3[] var27 = new Vec3[8];
		final float var28 = 0.046875F;
		final float var29 = 0.046875F;
		final float var30 = 0.3125F;
		var27[0] = blockAccess.getWorldVec3Pool().getVecFromPool(-var28, 0.0D,
				-var29);
		var27[1] = blockAccess.getWorldVec3Pool().getVecFromPool(var28, 0.0D,
				-var29);
		var27[2] = blockAccess.getWorldVec3Pool().getVecFromPool(var28, 0.0D,
				var29);
		var27[3] = blockAccess.getWorldVec3Pool().getVecFromPool(-var28, 0.0D,
				var29);
		var27[4] = blockAccess.getWorldVec3Pool().getVecFromPool(-var28, var30,
				-var29);
		var27[5] = blockAccess.getWorldVec3Pool().getVecFromPool(var28, var30,
				-var29);
		var27[6] = blockAccess.getWorldVec3Pool().getVecFromPool(var28, var30,
				var29);
		var27[7] = blockAccess.getWorldVec3Pool().getVecFromPool(-var28, var30,
				var29);

		for (int var31 = 0; var31 < 8; ++var31) {
			var27[var31].zCoord += 0.0625D;

			if (var9) {
				var27[var31].rotateAroundX(0.5235988F);
				var27[var31].yCoord -= 0.4375D;
			} else if (var8) {
				var27[var31].rotateAroundX(0.08726647F);
				var27[var31].yCoord -= 0.4375D;
			} else {
				var27[var31].rotateAroundX(-((float) Math.PI * 2F / 9F));
				var27[var31].yCoord -= 0.375D;
			}

			var27[var31].rotateAroundX((float) Math.PI / 2F);

			if (var7 == 2) {
				var27[var31].rotateAroundY(0.0F);
			}

			if (var7 == 0) {
				var27[var31].rotateAroundY((float) Math.PI);
			}

			if (var7 == 1) {
				var27[var31].rotateAroundY((float) Math.PI / 2F);
			}

			if (var7 == 3) {
				var27[var31].rotateAroundY(-((float) Math.PI / 2F));
			}

			var27[var31].xCoord += par2 + 0.5D;
			var27[var31].yCoord += par3 + 0.3125F;
			var27[var31].zCoord += par4 + 0.5D;
		}

		Vec3 var62 = null;
		Vec3 var32 = null;
		Vec3 var33 = null;
		Vec3 var34 = null;
		final byte var35 = 7;
		final byte var36 = 9;
		final byte var37 = 9;
		final byte var38 = 16;

		for (int var39 = 0; var39 < 6; ++var39) {
			if (var39 == 0) {
				var62 = var27[0];
				var32 = var27[1];
				var33 = var27[2];
				var34 = var27[3];
				var19 = var18.getInterpolatedU(var35);
				var21 = var18.getInterpolatedV(var37);
				var23 = var18.getInterpolatedU(var36);
				var25 = var18.getInterpolatedV(var37 + 2);
			} else if (var39 == 1) {
				var62 = var27[7];
				var32 = var27[6];
				var33 = var27[5];
				var34 = var27[4];
			} else if (var39 == 2) {
				var62 = var27[1];
				var32 = var27[0];
				var33 = var27[4];
				var34 = var27[5];
				var19 = var18.getInterpolatedU(var35);
				var21 = var18.getInterpolatedV(var37);
				var23 = var18.getInterpolatedU(var36);
				var25 = var18.getInterpolatedV(var38);
			} else if (var39 == 3) {
				var62 = var27[2];
				var32 = var27[1];
				var33 = var27[5];
				var34 = var27[6];
			} else if (var39 == 4) {
				var62 = var27[3];
				var32 = var27[2];
				var33 = var27[6];
				var34 = var27[7];
			} else if (var39 == 5) {
				var62 = var27[0];
				var32 = var27[3];
				var33 = var27[7];
				var34 = var27[4];
			}

			var5.addVertexWithUV(var62.xCoord, var62.yCoord, var62.zCoord,
					var19, var25);
			var5.addVertexWithUV(var32.xCoord, var32.yCoord, var32.zCoord,
					var23, var25);
			var5.addVertexWithUV(var33.xCoord, var33.yCoord, var33.zCoord,
					var23, var21);
			var5.addVertexWithUV(var34.xCoord, var34.yCoord, var34.zCoord,
					var19, var21);
		}

		final float var63 = 0.09375F;
		final float var40 = 0.09375F;
		final float var41 = 0.03125F;
		var27[0] = blockAccess.getWorldVec3Pool().getVecFromPool(-var63, 0.0D,
				-var40);
		var27[1] = blockAccess.getWorldVec3Pool().getVecFromPool(var63, 0.0D,
				-var40);
		var27[2] = blockAccess.getWorldVec3Pool().getVecFromPool(var63, 0.0D,
				var40);
		var27[3] = blockAccess.getWorldVec3Pool().getVecFromPool(-var63, 0.0D,
				var40);
		var27[4] = blockAccess.getWorldVec3Pool().getVecFromPool(-var63, var41,
				-var40);
		var27[5] = blockAccess.getWorldVec3Pool().getVecFromPool(var63, var41,
				-var40);
		var27[6] = blockAccess.getWorldVec3Pool().getVecFromPool(var63, var41,
				var40);
		var27[7] = blockAccess.getWorldVec3Pool().getVecFromPool(-var63, var41,
				var40);

		for (int var42 = 0; var42 < 8; ++var42) {
			var27[var42].zCoord += 0.21875D;

			if (var9) {
				var27[var42].yCoord -= 0.09375D;
				var27[var42].zCoord -= 0.1625D;
				var27[var42].rotateAroundX(0.0F);
			} else if (var8) {
				var27[var42].yCoord += 0.015625D;
				var27[var42].zCoord -= 0.171875D;
				var27[var42].rotateAroundX(0.17453294F);
			} else {
				var27[var42].rotateAroundX(0.87266463F);
			}

			if (var7 == 2) {
				var27[var42].rotateAroundY(0.0F);
			}

			if (var7 == 0) {
				var27[var42].rotateAroundY((float) Math.PI);
			}

			if (var7 == 1) {
				var27[var42].rotateAroundY((float) Math.PI / 2F);
			}

			if (var7 == 3) {
				var27[var42].rotateAroundY(-((float) Math.PI / 2F));
			}

			var27[var42].xCoord += par2 + 0.5D;
			var27[var42].yCoord += par3 + 0.3125F;
			var27[var42].zCoord += par4 + 0.5D;
		}

		final byte var65 = 5;
		final byte var43 = 11;
		final byte var44 = 3;
		final byte var45 = 9;

		for (int var46 = 0; var46 < 6; ++var46) {
			if (var46 == 0) {
				var62 = var27[0];
				var32 = var27[1];
				var33 = var27[2];
				var34 = var27[3];
				var19 = var18.getInterpolatedU(var65);
				var21 = var18.getInterpolatedV(var44);
				var23 = var18.getInterpolatedU(var43);
				var25 = var18.getInterpolatedV(var45);
			} else if (var46 == 1) {
				var62 = var27[7];
				var32 = var27[6];
				var33 = var27[5];
				var34 = var27[4];
			} else if (var46 == 2) {
				var62 = var27[1];
				var32 = var27[0];
				var33 = var27[4];
				var34 = var27[5];
				var19 = var18.getInterpolatedU(var65);
				var21 = var18.getInterpolatedV(var44);
				var23 = var18.getInterpolatedU(var43);
				var25 = var18.getInterpolatedV(var44 + 2);
			} else if (var46 == 3) {
				var62 = var27[2];
				var32 = var27[1];
				var33 = var27[5];
				var34 = var27[6];
			} else if (var46 == 4) {
				var62 = var27[3];
				var32 = var27[2];
				var33 = var27[6];
				var34 = var27[7];
			} else if (var46 == 5) {
				var62 = var27[0];
				var32 = var27[3];
				var33 = var27[7];
				var34 = var27[4];
			}

			var5.addVertexWithUV(var62.xCoord, var62.yCoord, var62.zCoord,
					var19, var25);
			var5.addVertexWithUV(var32.xCoord, var32.yCoord, var32.zCoord,
					var23, var25);
			var5.addVertexWithUV(var33.xCoord, var33.yCoord, var33.zCoord,
					var23, var21);
			var5.addVertexWithUV(var34.xCoord, var34.yCoord, var34.zCoord,
					var19, var21);
		}

		if (var8) {
			final double var64 = var27[0].yCoord;
			final float var48 = 0.03125F;
			final float var49 = 0.5F - var48 / 2.0F;
			final float var50 = var49 + var48;
			this.getBlockIcon(Block.tripWire);
			final double var52 = var18.getMinU();
			final double var54 = var18.getInterpolatedV(var8 ? 2.0D : 0.0D);
			final double var56 = var18.getMaxU();
			final double var58 = var18.getInterpolatedV(var8 ? 4.0D : 2.0D);
			final double var60 = (var10 ? 3.5F : 1.5F) / 16.0D;
			var17 = par1Block.getBlockBrightness(blockAccess, par2, par3, par4) * 0.75F;
			var5.setColorOpaque_F(var17, var17, var17);

			if (var7 == 2) {
				var5.addVertexWithUV(par2 + var49, par3 + var60, par4 + 0.25D,
						var52, var54);
				var5.addVertexWithUV(par2 + var50, par3 + var60, par4 + 0.25D,
						var52, var58);
				var5.addVertexWithUV(par2 + var50, par3 + var60, par4, var56,
						var58);
				var5.addVertexWithUV(par2 + var49, par3 + var60, par4, var56,
						var54);
				var5.addVertexWithUV(par2 + var49, var64, par4 + 0.5D, var52,
						var54);
				var5.addVertexWithUV(par2 + var50, var64, par4 + 0.5D, var52,
						var58);
				var5.addVertexWithUV(par2 + var50, par3 + var60, par4 + 0.25D,
						var56, var58);
				var5.addVertexWithUV(par2 + var49, par3 + var60, par4 + 0.25D,
						var56, var54);
			} else if (var7 == 0) {
				var5.addVertexWithUV(par2 + var49, par3 + var60, par4 + 0.75D,
						var52, var54);
				var5.addVertexWithUV(par2 + var50, par3 + var60, par4 + 0.75D,
						var52, var58);
				var5.addVertexWithUV(par2 + var50, var64, par4 + 0.5D, var56,
						var58);
				var5.addVertexWithUV(par2 + var49, var64, par4 + 0.5D, var56,
						var54);
				var5.addVertexWithUV(par2 + var49, par3 + var60, par4 + 1,
						var52, var54);
				var5.addVertexWithUV(par2 + var50, par3 + var60, par4 + 1,
						var52, var58);
				var5.addVertexWithUV(par2 + var50, par3 + var60, par4 + 0.75D,
						var56, var58);
				var5.addVertexWithUV(par2 + var49, par3 + var60, par4 + 0.75D,
						var56, var54);
			} else if (var7 == 1) {
				var5.addVertexWithUV(par2, par3 + var60, par4 + var50, var52,
						var58);
				var5.addVertexWithUV(par2 + 0.25D, par3 + var60, par4 + var50,
						var56, var58);
				var5.addVertexWithUV(par2 + 0.25D, par3 + var60, par4 + var49,
						var56, var54);
				var5.addVertexWithUV(par2, par3 + var60, par4 + var49, var52,
						var54);
				var5.addVertexWithUV(par2 + 0.25D, par3 + var60, par4 + var50,
						var52, var58);
				var5.addVertexWithUV(par2 + 0.5D, var64, par4 + var50, var56,
						var58);
				var5.addVertexWithUV(par2 + 0.5D, var64, par4 + var49, var56,
						var54);
				var5.addVertexWithUV(par2 + 0.25D, par3 + var60, par4 + var49,
						var52, var54);
			} else {
				var5.addVertexWithUV(par2 + 0.5D, var64, par4 + var50, var52,
						var58);
				var5.addVertexWithUV(par2 + 0.75D, par3 + var60, par4 + var50,
						var56, var58);
				var5.addVertexWithUV(par2 + 0.75D, par3 + var60, par4 + var49,
						var56, var54);
				var5.addVertexWithUV(par2 + 0.5D, var64, par4 + var49, var52,
						var54);
				var5.addVertexWithUV(par2 + 0.75D, par3 + var60, par4 + var50,
						var52, var58);
				var5.addVertexWithUV(par2 + 1, par3 + var60, par4 + var50,
						var56, var58);
				var5.addVertexWithUV(par2 + 1, par3 + var60, par4 + var49,
						var56, var54);
				var5.addVertexWithUV(par2 + 0.75D, par3 + var60, par4 + var49,
						var52, var54);
			}
		}

		return true;
	}

	/**
	 * Renders a trip wire block at the given coordinates
	 */
	public boolean renderBlockTripWire(final Block par1Block, final int par2,
			final int par3, final int par4) {
		final Tessellator var5 = Tessellator.instance;
		Icon var6 = getBlockIconFromSide(par1Block, 0);
		final int var7 = blockAccess.getBlockMetadata(par2, par3, par4);
		final boolean var8 = (var7 & 4) == 4;
		final boolean var9 = (var7 & 2) == 2;

		if (hasOverrideBlockTexture()) {
			var6 = overrideBlockTexture;
		}

		var5.setBrightness(par1Block.getMixedBrightnessForBlock(blockAccess,
				par2, par3, par4));
		final float var10 = par1Block.getBlockBrightness(blockAccess, par2,
				par3, par4) * 0.75F;
		var5.setColorOpaque_F(var10, var10, var10);
		final double var11 = var6.getMinU();
		final double var13 = var6.getInterpolatedV(var8 ? 2.0D : 0.0D);
		final double var15 = var6.getMaxU();
		final double var17 = var6.getInterpolatedV(var8 ? 4.0D : 2.0D);
		final double var19 = (var9 ? 3.5F : 1.5F) / 16.0D;
		final boolean var21 = BlockTripWire.func_72148_a(blockAccess, par2,
				par3, par4, var7, 1);
		final boolean var22 = BlockTripWire.func_72148_a(blockAccess, par2,
				par3, par4, var7, 3);
		boolean var23 = BlockTripWire.func_72148_a(blockAccess, par2, par3,
				par4, var7, 2);
		boolean var24 = BlockTripWire.func_72148_a(blockAccess, par2, par3,
				par4, var7, 0);
		final float var25 = 0.03125F;
		final float var26 = 0.5F - var25 / 2.0F;
		final float var27 = var26 + var25;

		if (!var23 && !var22 && !var24 && !var21) {
			var23 = true;
			var24 = true;
		}

		if (var23) {
			var5.addVertexWithUV(par2 + var26, par3 + var19, par4 + 0.25D,
					var11, var13);
			var5.addVertexWithUV(par2 + var27, par3 + var19, par4 + 0.25D,
					var11, var17);
			var5.addVertexWithUV(par2 + var27, par3 + var19, par4, var15, var17);
			var5.addVertexWithUV(par2 + var26, par3 + var19, par4, var15, var13);
			var5.addVertexWithUV(par2 + var26, par3 + var19, par4, var15, var13);
			var5.addVertexWithUV(par2 + var27, par3 + var19, par4, var15, var17);
			var5.addVertexWithUV(par2 + var27, par3 + var19, par4 + 0.25D,
					var11, var17);
			var5.addVertexWithUV(par2 + var26, par3 + var19, par4 + 0.25D,
					var11, var13);
		}

		if (var23 || var24 && !var22 && !var21) {
			var5.addVertexWithUV(par2 + var26, par3 + var19, par4 + 0.5D,
					var11, var13);
			var5.addVertexWithUV(par2 + var27, par3 + var19, par4 + 0.5D,
					var11, var17);
			var5.addVertexWithUV(par2 + var27, par3 + var19, par4 + 0.25D,
					var15, var17);
			var5.addVertexWithUV(par2 + var26, par3 + var19, par4 + 0.25D,
					var15, var13);
			var5.addVertexWithUV(par2 + var26, par3 + var19, par4 + 0.25D,
					var15, var13);
			var5.addVertexWithUV(par2 + var27, par3 + var19, par4 + 0.25D,
					var15, var17);
			var5.addVertexWithUV(par2 + var27, par3 + var19, par4 + 0.5D,
					var11, var17);
			var5.addVertexWithUV(par2 + var26, par3 + var19, par4 + 0.5D,
					var11, var13);
		}

		if (var24 || var23 && !var22 && !var21) {
			var5.addVertexWithUV(par2 + var26, par3 + var19, par4 + 0.75D,
					var11, var13);
			var5.addVertexWithUV(par2 + var27, par3 + var19, par4 + 0.75D,
					var11, var17);
			var5.addVertexWithUV(par2 + var27, par3 + var19, par4 + 0.5D,
					var15, var17);
			var5.addVertexWithUV(par2 + var26, par3 + var19, par4 + 0.5D,
					var15, var13);
			var5.addVertexWithUV(par2 + var26, par3 + var19, par4 + 0.5D,
					var15, var13);
			var5.addVertexWithUV(par2 + var27, par3 + var19, par4 + 0.5D,
					var15, var17);
			var5.addVertexWithUV(par2 + var27, par3 + var19, par4 + 0.75D,
					var11, var17);
			var5.addVertexWithUV(par2 + var26, par3 + var19, par4 + 0.75D,
					var11, var13);
		}

		if (var24) {
			var5.addVertexWithUV(par2 + var26, par3 + var19, par4 + 1, var11,
					var13);
			var5.addVertexWithUV(par2 + var27, par3 + var19, par4 + 1, var11,
					var17);
			var5.addVertexWithUV(par2 + var27, par3 + var19, par4 + 0.75D,
					var15, var17);
			var5.addVertexWithUV(par2 + var26, par3 + var19, par4 + 0.75D,
					var15, var13);
			var5.addVertexWithUV(par2 + var26, par3 + var19, par4 + 0.75D,
					var15, var13);
			var5.addVertexWithUV(par2 + var27, par3 + var19, par4 + 0.75D,
					var15, var17);
			var5.addVertexWithUV(par2 + var27, par3 + var19, par4 + 1, var11,
					var17);
			var5.addVertexWithUV(par2 + var26, par3 + var19, par4 + 1, var11,
					var13);
		}

		if (var21) {
			var5.addVertexWithUV(par2, par3 + var19, par4 + var27, var11, var17);
			var5.addVertexWithUV(par2 + 0.25D, par3 + var19, par4 + var27,
					var15, var17);
			var5.addVertexWithUV(par2 + 0.25D, par3 + var19, par4 + var26,
					var15, var13);
			var5.addVertexWithUV(par2, par3 + var19, par4 + var26, var11, var13);
			var5.addVertexWithUV(par2, par3 + var19, par4 + var26, var11, var13);
			var5.addVertexWithUV(par2 + 0.25D, par3 + var19, par4 + var26,
					var15, var13);
			var5.addVertexWithUV(par2 + 0.25D, par3 + var19, par4 + var27,
					var15, var17);
			var5.addVertexWithUV(par2, par3 + var19, par4 + var27, var11, var17);
		}

		if (var21 || var22 && !var23 && !var24) {
			var5.addVertexWithUV(par2 + 0.25D, par3 + var19, par4 + var27,
					var11, var17);
			var5.addVertexWithUV(par2 + 0.5D, par3 + var19, par4 + var27,
					var15, var17);
			var5.addVertexWithUV(par2 + 0.5D, par3 + var19, par4 + var26,
					var15, var13);
			var5.addVertexWithUV(par2 + 0.25D, par3 + var19, par4 + var26,
					var11, var13);
			var5.addVertexWithUV(par2 + 0.25D, par3 + var19, par4 + var26,
					var11, var13);
			var5.addVertexWithUV(par2 + 0.5D, par3 + var19, par4 + var26,
					var15, var13);
			var5.addVertexWithUV(par2 + 0.5D, par3 + var19, par4 + var27,
					var15, var17);
			var5.addVertexWithUV(par2 + 0.25D, par3 + var19, par4 + var27,
					var11, var17);
		}

		if (var22 || var21 && !var23 && !var24) {
			var5.addVertexWithUV(par2 + 0.5D, par3 + var19, par4 + var27,
					var11, var17);
			var5.addVertexWithUV(par2 + 0.75D, par3 + var19, par4 + var27,
					var15, var17);
			var5.addVertexWithUV(par2 + 0.75D, par3 + var19, par4 + var26,
					var15, var13);
			var5.addVertexWithUV(par2 + 0.5D, par3 + var19, par4 + var26,
					var11, var13);
			var5.addVertexWithUV(par2 + 0.5D, par3 + var19, par4 + var26,
					var11, var13);
			var5.addVertexWithUV(par2 + 0.75D, par3 + var19, par4 + var26,
					var15, var13);
			var5.addVertexWithUV(par2 + 0.75D, par3 + var19, par4 + var27,
					var15, var17);
			var5.addVertexWithUV(par2 + 0.5D, par3 + var19, par4 + var27,
					var11, var17);
		}

		if (var22) {
			var5.addVertexWithUV(par2 + 0.75D, par3 + var19, par4 + var27,
					var11, var17);
			var5.addVertexWithUV(par2 + 1, par3 + var19, par4 + var27, var15,
					var17);
			var5.addVertexWithUV(par2 + 1, par3 + var19, par4 + var26, var15,
					var13);
			var5.addVertexWithUV(par2 + 0.75D, par3 + var19, par4 + var26,
					var11, var13);
			var5.addVertexWithUV(par2 + 0.75D, par3 + var19, par4 + var26,
					var11, var13);
			var5.addVertexWithUV(par2 + 1, par3 + var19, par4 + var26, var15,
					var13);
			var5.addVertexWithUV(par2 + 1, par3 + var19, par4 + var27, var15,
					var17);
			var5.addVertexWithUV(par2 + 0.75D, par3 + var19, par4 + var27,
					var11, var17);
		}

		return true;
	}

	/**
	 * Renders a fire block at the given coordinates
	 */
	public boolean renderBlockFire(final BlockFire par1BlockFire,
			final int par2, int par3, final int par4) {
		final Tessellator var5 = Tessellator.instance;
		final Icon var6 = par1BlockFire.func_94438_c(0);
		final Icon var7 = par1BlockFire.func_94438_c(1);
		Icon var8 = var6;

		if (hasOverrideBlockTexture()) {
			var8 = overrideBlockTexture;
		}

		var5.setColorOpaque_F(1.0F, 1.0F, 1.0F);
		var5.setBrightness(par1BlockFire.getMixedBrightnessForBlock(
				blockAccess, par2, par3, par4));
		double var9 = var8.getMinU();
		double var11 = var8.getMinV();
		double var13 = var8.getMaxU();
		double var15 = var8.getMaxV();
		float var17 = 1.4F;
		double var18;
		double var20;
		double var22;
		double var24;
		double var26;
		double var28;
		double var30;

		if (!blockAccess.doesBlockHaveSolidTopSurface(par2, par3 - 1, par4)
				&& !Block.fire.canBlockCatchFire(blockAccess, par2, par3 - 1,
						par4)) {
			final float var36 = 0.2F;
			final float var33 = 0.0625F;

			if ((par2 + par3 + par4 & 1) == 1) {
				var9 = var7.getMinU();
				var11 = var7.getMinV();
				var13 = var7.getMaxU();
				var15 = var7.getMaxV();
			}

			if ((par2 / 2 + par3 / 2 + par4 / 2 & 1) == 1) {
				var18 = var13;
				var13 = var9;
				var9 = var18;
			}

			if (Block.fire.canBlockCatchFire(blockAccess, par2 - 1, par3, par4)) {
				var5.addVertexWithUV(par2 + var36, par3 + var17 + var33,
						par4 + 1, var13, var11);
				var5.addVertexWithUV(par2 + 0, par3 + 0 + var33, par4 + 1,
						var13, var15);
				var5.addVertexWithUV(par2 + 0, par3 + 0 + var33, par4 + 0,
						var9, var15);
				var5.addVertexWithUV(par2 + var36, par3 + var17 + var33,
						par4 + 0, var9, var11);
				var5.addVertexWithUV(par2 + var36, par3 + var17 + var33,
						par4 + 0, var9, var11);
				var5.addVertexWithUV(par2 + 0, par3 + 0 + var33, par4 + 0,
						var9, var15);
				var5.addVertexWithUV(par2 + 0, par3 + 0 + var33, par4 + 1,
						var13, var15);
				var5.addVertexWithUV(par2 + var36, par3 + var17 + var33,
						par4 + 1, var13, var11);
			}

			if (Block.fire.canBlockCatchFire(blockAccess, par2 + 1, par3, par4)) {
				var5.addVertexWithUV(par2 + 1 - var36, par3 + var17 + var33,
						par4 + 0, var9, var11);
				var5.addVertexWithUV(par2 + 1 - 0, par3 + 0 + var33, par4 + 0,
						var9, var15);
				var5.addVertexWithUV(par2 + 1 - 0, par3 + 0 + var33, par4 + 1,
						var13, var15);
				var5.addVertexWithUV(par2 + 1 - var36, par3 + var17 + var33,
						par4 + 1, var13, var11);
				var5.addVertexWithUV(par2 + 1 - var36, par3 + var17 + var33,
						par4 + 1, var13, var11);
				var5.addVertexWithUV(par2 + 1 - 0, par3 + 0 + var33, par4 + 1,
						var13, var15);
				var5.addVertexWithUV(par2 + 1 - 0, par3 + 0 + var33, par4 + 0,
						var9, var15);
				var5.addVertexWithUV(par2 + 1 - var36, par3 + var17 + var33,
						par4 + 0, var9, var11);
			}

			if (Block.fire.canBlockCatchFire(blockAccess, par2, par3, par4 - 1)) {
				var5.addVertexWithUV(par2 + 0, par3 + var17 + var33, par4
						+ var36, var13, var11);
				var5.addVertexWithUV(par2 + 0, par3 + 0 + var33, par4 + 0,
						var13, var15);
				var5.addVertexWithUV(par2 + 1, par3 + 0 + var33, par4 + 0,
						var9, var15);
				var5.addVertexWithUV(par2 + 1, par3 + var17 + var33, par4
						+ var36, var9, var11);
				var5.addVertexWithUV(par2 + 1, par3 + var17 + var33, par4
						+ var36, var9, var11);
				var5.addVertexWithUV(par2 + 1, par3 + 0 + var33, par4 + 0,
						var9, var15);
				var5.addVertexWithUV(par2 + 0, par3 + 0 + var33, par4 + 0,
						var13, var15);
				var5.addVertexWithUV(par2 + 0, par3 + var17 + var33, par4
						+ var36, var13, var11);
			}

			if (Block.fire.canBlockCatchFire(blockAccess, par2, par3, par4 + 1)) {
				var5.addVertexWithUV(par2 + 1, par3 + var17 + var33, par4 + 1
						- var36, var9, var11);
				var5.addVertexWithUV(par2 + 1, par3 + 0 + var33, par4 + 1 - 0,
						var9, var15);
				var5.addVertexWithUV(par2 + 0, par3 + 0 + var33, par4 + 1 - 0,
						var13, var15);
				var5.addVertexWithUV(par2 + 0, par3 + var17 + var33, par4 + 1
						- var36, var13, var11);
				var5.addVertexWithUV(par2 + 0, par3 + var17 + var33, par4 + 1
						- var36, var13, var11);
				var5.addVertexWithUV(par2 + 0, par3 + 0 + var33, par4 + 1 - 0,
						var13, var15);
				var5.addVertexWithUV(par2 + 1, par3 + 0 + var33, par4 + 1 - 0,
						var9, var15);
				var5.addVertexWithUV(par2 + 1, par3 + var17 + var33, par4 + 1
						- var36, var9, var11);
			}

			if (Block.fire.canBlockCatchFire(blockAccess, par2, par3 + 1, par4)) {
				var18 = par2 + 0.5D + 0.5D;
				var20 = par2 + 0.5D - 0.5D;
				var22 = par4 + 0.5D + 0.5D;
				var24 = par4 + 0.5D - 0.5D;
				var26 = par2 + 0.5D - 0.5D;
				var28 = par2 + 0.5D + 0.5D;
				var30 = par4 + 0.5D - 0.5D;
				final double var34 = par4 + 0.5D + 0.5D;
				var9 = var6.getMinU();
				var11 = var6.getMinV();
				var13 = var6.getMaxU();
				var15 = var6.getMaxV();
				++par3;
				var17 = -0.2F;

				if ((par2 + par3 + par4 & 1) == 0) {
					var5.addVertexWithUV(var26, par3 + var17, par4 + 0, var13,
							var11);
					var5.addVertexWithUV(var18, par3 + 0, par4 + 0, var13,
							var15);
					var5.addVertexWithUV(var18, par3 + 0, par4 + 1, var9, var15);
					var5.addVertexWithUV(var26, par3 + var17, par4 + 1, var9,
							var11);
					var9 = var7.getMinU();
					var11 = var7.getMinV();
					var13 = var7.getMaxU();
					var15 = var7.getMaxV();
					var5.addVertexWithUV(var28, par3 + var17, par4 + 1, var13,
							var11);
					var5.addVertexWithUV(var20, par3 + 0, par4 + 1, var13,
							var15);
					var5.addVertexWithUV(var20, par3 + 0, par4 + 0, var9, var15);
					var5.addVertexWithUV(var28, par3 + var17, par4 + 0, var9,
							var11);
				} else {
					var5.addVertexWithUV(par2 + 0, par3 + var17, var34, var13,
							var11);
					var5.addVertexWithUV(par2 + 0, par3 + 0, var24, var13,
							var15);
					var5.addVertexWithUV(par2 + 1, par3 + 0, var24, var9, var15);
					var5.addVertexWithUV(par2 + 1, par3 + var17, var34, var9,
							var11);
					var9 = var7.getMinU();
					var11 = var7.getMinV();
					var13 = var7.getMaxU();
					var15 = var7.getMaxV();
					var5.addVertexWithUV(par2 + 1, par3 + var17, var30, var13,
							var11);
					var5.addVertexWithUV(par2 + 1, par3 + 0, var22, var13,
							var15);
					var5.addVertexWithUV(par2 + 0, par3 + 0, var22, var9, var15);
					var5.addVertexWithUV(par2 + 0, par3 + var17, var30, var9,
							var11);
				}
			}
		} else {
			double var32 = par2 + 0.5D + 0.2D;
			var18 = par2 + 0.5D - 0.2D;
			var20 = par4 + 0.5D + 0.2D;
			var22 = par4 + 0.5D - 0.2D;
			var24 = par2 + 0.5D - 0.3D;
			var26 = par2 + 0.5D + 0.3D;
			var28 = par4 + 0.5D - 0.3D;
			var30 = par4 + 0.5D + 0.3D;
			var5.addVertexWithUV(var24, par3 + var17, par4 + 1, var13, var11);
			var5.addVertexWithUV(var32, par3 + 0, par4 + 1, var13, var15);
			var5.addVertexWithUV(var32, par3 + 0, par4 + 0, var9, var15);
			var5.addVertexWithUV(var24, par3 + var17, par4 + 0, var9, var11);
			var5.addVertexWithUV(var26, par3 + var17, par4 + 0, var13, var11);
			var5.addVertexWithUV(var18, par3 + 0, par4 + 0, var13, var15);
			var5.addVertexWithUV(var18, par3 + 0, par4 + 1, var9, var15);
			var5.addVertexWithUV(var26, par3 + var17, par4 + 1, var9, var11);
			var9 = var7.getMinU();
			var11 = var7.getMinV();
			var13 = var7.getMaxU();
			var15 = var7.getMaxV();
			var5.addVertexWithUV(par2 + 1, par3 + var17, var30, var13, var11);
			var5.addVertexWithUV(par2 + 1, par3 + 0, var22, var13, var15);
			var5.addVertexWithUV(par2 + 0, par3 + 0, var22, var9, var15);
			var5.addVertexWithUV(par2 + 0, par3 + var17, var30, var9, var11);
			var5.addVertexWithUV(par2 + 0, par3 + var17, var28, var13, var11);
			var5.addVertexWithUV(par2 + 0, par3 + 0, var20, var13, var15);
			var5.addVertexWithUV(par2 + 1, par3 + 0, var20, var9, var15);
			var5.addVertexWithUV(par2 + 1, par3 + var17, var28, var9, var11);
			var32 = par2 + 0.5D - 0.5D;
			var18 = par2 + 0.5D + 0.5D;
			var20 = par4 + 0.5D - 0.5D;
			var22 = par4 + 0.5D + 0.5D;
			var24 = par2 + 0.5D - 0.4D;
			var26 = par2 + 0.5D + 0.4D;
			var28 = par4 + 0.5D - 0.4D;
			var30 = par4 + 0.5D + 0.4D;
			var5.addVertexWithUV(var24, par3 + var17, par4 + 0, var9, var11);
			var5.addVertexWithUV(var32, par3 + 0, par4 + 0, var9, var15);
			var5.addVertexWithUV(var32, par3 + 0, par4 + 1, var13, var15);
			var5.addVertexWithUV(var24, par3 + var17, par4 + 1, var13, var11);
			var5.addVertexWithUV(var26, par3 + var17, par4 + 1, var9, var11);
			var5.addVertexWithUV(var18, par3 + 0, par4 + 1, var9, var15);
			var5.addVertexWithUV(var18, par3 + 0, par4 + 0, var13, var15);
			var5.addVertexWithUV(var26, par3 + var17, par4 + 0, var13, var11);
			var9 = var6.getMinU();
			var11 = var6.getMinV();
			var13 = var6.getMaxU();
			var15 = var6.getMaxV();
			var5.addVertexWithUV(par2 + 0, par3 + var17, var30, var9, var11);
			var5.addVertexWithUV(par2 + 0, par3 + 0, var22, var9, var15);
			var5.addVertexWithUV(par2 + 1, par3 + 0, var22, var13, var15);
			var5.addVertexWithUV(par2 + 1, par3 + var17, var30, var13, var11);
			var5.addVertexWithUV(par2 + 1, par3 + var17, var28, var9, var11);
			var5.addVertexWithUV(par2 + 1, par3 + 0, var20, var9, var15);
			var5.addVertexWithUV(par2 + 0, par3 + 0, var20, var13, var15);
			var5.addVertexWithUV(par2 + 0, par3 + var17, var28, var13, var11);
		}

		return true;
	}

	/**
	 * Renders a redstone wire block at the given coordinates
	 */
	public boolean renderBlockRedstoneWire(final Block par1Block,
			final int par2, final int par3, final int par4) {
		final Tessellator var5 = Tessellator.instance;
		final int var6 = blockAccess.getBlockMetadata(par2, par3, par4);
		final Icon var7 = BlockRedstoneWire.func_94409_b("redstoneDust_cross");
		final Icon var8 = BlockRedstoneWire.func_94409_b("redstoneDust_line");
		final Icon var9 = BlockRedstoneWire
				.func_94409_b("redstoneDust_cross_overlay");
		final Icon var10 = BlockRedstoneWire
				.func_94409_b("redstoneDust_line_overlay");
		var5.setBrightness(par1Block.getMixedBrightnessForBlock(blockAccess,
				par2, par3, par4));
		final float var11 = 1.0F;
		final float var12 = var6 / 15.0F;
		float var13 = var12 * 0.6F + 0.4F;

		if (var6 == 0) {
			var13 = 0.3F;
		}

		float var14 = var12 * var12 * 0.7F - 0.5F;
		float var15 = var12 * var12 * 0.6F - 0.7F;

		if (var14 < 0.0F) {
			var14 = 0.0F;
		}

		if (var15 < 0.0F) {
			var15 = 0.0F;
		}

		final int var16 = CustomColorizer.getRedstoneColor(var6);

		if (var16 != -1) {
			final int var17 = var16 >> 16 & 255;
			final int var18 = var16 >> 8 & 255;
			final int var19 = var16 & 255;
			var13 = var17 / 255.0F;
			var14 = var18 / 255.0F;
			var15 = var19 / 255.0F;
		}

		var5.setColorOpaque_F(var13, var14, var15);
		boolean var30 = BlockRedstoneWire.isPowerProviderOrWire(blockAccess,
				par2 - 1, par3, par4, 1)
				|| !blockAccess.isBlockNormalCube(par2 - 1, par3, par4)
				&& BlockRedstoneWire.isPowerProviderOrWire(blockAccess,
						par2 - 1, par3 - 1, par4, -1);
		boolean var32 = BlockRedstoneWire.isPowerProviderOrWire(blockAccess,
				par2 + 1, par3, par4, 3)
				|| !blockAccess.isBlockNormalCube(par2 + 1, par3, par4)
				&& BlockRedstoneWire.isPowerProviderOrWire(blockAccess,
						par2 + 1, par3 - 1, par4, -1);
		boolean var31 = BlockRedstoneWire.isPowerProviderOrWire(blockAccess,
				par2, par3, par4 - 1, 2)
				|| !blockAccess.isBlockNormalCube(par2, par3, par4 - 1)
				&& BlockRedstoneWire.isPowerProviderOrWire(blockAccess, par2,
						par3 - 1, par4 - 1, -1);
		boolean var20 = BlockRedstoneWire.isPowerProviderOrWire(blockAccess,
				par2, par3, par4 + 1, 0)
				|| !blockAccess.isBlockNormalCube(par2, par3, par4 + 1)
				&& BlockRedstoneWire.isPowerProviderOrWire(blockAccess, par2,
						par3 - 1, par4 + 1, -1);

		if (!blockAccess.isBlockNormalCube(par2, par3 + 1, par4)) {
			if (blockAccess.isBlockNormalCube(par2 - 1, par3, par4)
					&& BlockRedstoneWire.isPowerProviderOrWire(blockAccess,
							par2 - 1, par3 + 1, par4, -1)) {
				var30 = true;
			}

			if (blockAccess.isBlockNormalCube(par2 + 1, par3, par4)
					&& BlockRedstoneWire.isPowerProviderOrWire(blockAccess,
							par2 + 1, par3 + 1, par4, -1)) {
				var32 = true;
			}

			if (blockAccess.isBlockNormalCube(par2, par3, par4 - 1)
					&& BlockRedstoneWire.isPowerProviderOrWire(blockAccess,
							par2, par3 + 1, par4 - 1, -1)) {
				var31 = true;
			}

			if (blockAccess.isBlockNormalCube(par2, par3, par4 + 1)
					&& BlockRedstoneWire.isPowerProviderOrWire(blockAccess,
							par2, par3 + 1, par4 + 1, -1)) {
				var20 = true;
			}
		}

		float var21 = par2 + 0;
		float var22 = par2 + 1;
		float var23 = par4 + 0;
		float var24 = par4 + 1;
		boolean var25 = false;

		if ((var30 || var32) && !var31 && !var20) {
			var25 = true;
		}

		if ((var31 || var20) && !var32 && !var30) {
			var25 = true;
		}

		if (!var25) {
			int var26 = 0;
			int var27 = 0;
			int var28 = 16;
			int var29 = 16;

			if (!var30) {
				var21 += 0.3125F;
			}

			if (!var30) {
				var26 += 5;
			}

			if (!var32) {
				var22 -= 0.3125F;
			}

			if (!var32) {
				var28 -= 5;
			}

			if (!var31) {
				var23 += 0.3125F;
			}

			if (!var31) {
				var27 += 5;
			}

			if (!var20) {
				var24 -= 0.3125F;
			}

			if (!var20) {
				var29 -= 5;
			}

			var5.addVertexWithUV(var22, par3 + 0.015625D, var24,
					var7.getInterpolatedU(var28), var7.getInterpolatedV(var29));
			var5.addVertexWithUV(var22, par3 + 0.015625D, var23,
					var7.getInterpolatedU(var28), var7.getInterpolatedV(var27));
			var5.addVertexWithUV(var21, par3 + 0.015625D, var23,
					var7.getInterpolatedU(var26), var7.getInterpolatedV(var27));
			var5.addVertexWithUV(var21, par3 + 0.015625D, var24,
					var7.getInterpolatedU(var26), var7.getInterpolatedV(var29));
			var5.setColorOpaque_F(var11, var11, var11);
			var5.addVertexWithUV(var22, par3 + 0.015625D, var24,
					var9.getInterpolatedU(var28), var9.getInterpolatedV(var29));
			var5.addVertexWithUV(var22, par3 + 0.015625D, var23,
					var9.getInterpolatedU(var28), var9.getInterpolatedV(var27));
			var5.addVertexWithUV(var21, par3 + 0.015625D, var23,
					var9.getInterpolatedU(var26), var9.getInterpolatedV(var27));
			var5.addVertexWithUV(var21, par3 + 0.015625D, var24,
					var9.getInterpolatedU(var26), var9.getInterpolatedV(var29));
		} else if (var25) {
			var5.addVertexWithUV(var22, par3 + 0.015625D, var24,
					var8.getMaxU(), var8.getMaxV());
			var5.addVertexWithUV(var22, par3 + 0.015625D, var23,
					var8.getMaxU(), var8.getMinV());
			var5.addVertexWithUV(var21, par3 + 0.015625D, var23,
					var8.getMinU(), var8.getMinV());
			var5.addVertexWithUV(var21, par3 + 0.015625D, var24,
					var8.getMinU(), var8.getMaxV());
			var5.setColorOpaque_F(var11, var11, var11);
			var5.addVertexWithUV(var22, par3 + 0.015625D, var24,
					var10.getMaxU(), var10.getMaxV());
			var5.addVertexWithUV(var22, par3 + 0.015625D, var23,
					var10.getMaxU(), var10.getMinV());
			var5.addVertexWithUV(var21, par3 + 0.015625D, var23,
					var10.getMinU(), var10.getMinV());
			var5.addVertexWithUV(var21, par3 + 0.015625D, var24,
					var10.getMinU(), var10.getMaxV());
		} else {
			var5.addVertexWithUV(var22, par3 + 0.015625D, var24,
					var8.getMaxU(), var8.getMaxV());
			var5.addVertexWithUV(var22, par3 + 0.015625D, var23,
					var8.getMinU(), var8.getMaxV());
			var5.addVertexWithUV(var21, par3 + 0.015625D, var23,
					var8.getMinU(), var8.getMinV());
			var5.addVertexWithUV(var21, par3 + 0.015625D, var24,
					var8.getMaxU(), var8.getMinV());
			var5.setColorOpaque_F(var11, var11, var11);
			var5.addVertexWithUV(var22, par3 + 0.015625D, var24,
					var10.getMaxU(), var10.getMaxV());
			var5.addVertexWithUV(var22, par3 + 0.015625D, var23,
					var10.getMinU(), var10.getMaxV());
			var5.addVertexWithUV(var21, par3 + 0.015625D, var23,
					var10.getMinU(), var10.getMinV());
			var5.addVertexWithUV(var21, par3 + 0.015625D, var24,
					var10.getMaxU(), var10.getMinV());
		}

		if (!blockAccess.isBlockNormalCube(par2, par3 + 1, par4)) {
			if (blockAccess.isBlockNormalCube(par2 - 1, par3, par4)
					&& blockAccess.getBlockId(par2 - 1, par3 + 1, par4) == Block.redstoneWire.blockID) {
				var5.setColorOpaque_F(var11 * var13, var11 * var14, var11
						* var15);
				var5.addVertexWithUV(par2 + 0.015625D, par3 + 1 + 0.021875F,
						par4 + 1, var8.getMaxU(), var8.getMinV());
				var5.addVertexWithUV(par2 + 0.015625D, par3 + 0, par4 + 1,
						var8.getMinU(), var8.getMinV());
				var5.addVertexWithUV(par2 + 0.015625D, par3 + 0, par4 + 0,
						var8.getMinU(), var8.getMaxV());
				var5.addVertexWithUV(par2 + 0.015625D, par3 + 1 + 0.021875F,
						par4 + 0, var8.getMaxU(), var8.getMaxV());
				var5.setColorOpaque_F(var11, var11, var11);
				var5.addVertexWithUV(par2 + 0.015625D, par3 + 1 + 0.021875F,
						par4 + 1, var10.getMaxU(), var10.getMinV());
				var5.addVertexWithUV(par2 + 0.015625D, par3 + 0, par4 + 1,
						var10.getMinU(), var10.getMinV());
				var5.addVertexWithUV(par2 + 0.015625D, par3 + 0, par4 + 0,
						var10.getMinU(), var10.getMaxV());
				var5.addVertexWithUV(par2 + 0.015625D, par3 + 1 + 0.021875F,
						par4 + 0, var10.getMaxU(), var10.getMaxV());
			}

			if (blockAccess.isBlockNormalCube(par2 + 1, par3, par4)
					&& blockAccess.getBlockId(par2 + 1, par3 + 1, par4) == Block.redstoneWire.blockID) {
				var5.setColorOpaque_F(var11 * var13, var11 * var14, var11
						* var15);
				var5.addVertexWithUV(par2 + 1 - 0.015625D, par3 + 0, par4 + 1,
						var8.getMinU(), var8.getMaxV());
				var5.addVertexWithUV(par2 + 1 - 0.015625D,
						par3 + 1 + 0.021875F, par4 + 1, var8.getMaxU(),
						var8.getMaxV());
				var5.addVertexWithUV(par2 + 1 - 0.015625D,
						par3 + 1 + 0.021875F, par4 + 0, var8.getMaxU(),
						var8.getMinV());
				var5.addVertexWithUV(par2 + 1 - 0.015625D, par3 + 0, par4 + 0,
						var8.getMinU(), var8.getMinV());
				var5.setColorOpaque_F(var11, var11, var11);
				var5.addVertexWithUV(par2 + 1 - 0.015625D, par3 + 0, par4 + 1,
						var10.getMinU(), var10.getMaxV());
				var5.addVertexWithUV(par2 + 1 - 0.015625D,
						par3 + 1 + 0.021875F, par4 + 1, var10.getMaxU(),
						var10.getMaxV());
				var5.addVertexWithUV(par2 + 1 - 0.015625D,
						par3 + 1 + 0.021875F, par4 + 0, var10.getMaxU(),
						var10.getMinV());
				var5.addVertexWithUV(par2 + 1 - 0.015625D, par3 + 0, par4 + 0,
						var10.getMinU(), var10.getMinV());
			}

			if (blockAccess.isBlockNormalCube(par2, par3, par4 - 1)
					&& blockAccess.getBlockId(par2, par3 + 1, par4 - 1) == Block.redstoneWire.blockID) {
				var5.setColorOpaque_F(var11 * var13, var11 * var14, var11
						* var15);
				var5.addVertexWithUV(par2 + 1, par3 + 0, par4 + 0.015625D,
						var8.getMinU(), var8.getMaxV());
				var5.addVertexWithUV(par2 + 1, par3 + 1 + 0.021875F,
						par4 + 0.015625D, var8.getMaxU(), var8.getMaxV());
				var5.addVertexWithUV(par2 + 0, par3 + 1 + 0.021875F,
						par4 + 0.015625D, var8.getMaxU(), var8.getMinV());
				var5.addVertexWithUV(par2 + 0, par3 + 0, par4 + 0.015625D,
						var8.getMinU(), var8.getMinV());
				var5.setColorOpaque_F(var11, var11, var11);
				var5.addVertexWithUV(par2 + 1, par3 + 0, par4 + 0.015625D,
						var10.getMinU(), var10.getMaxV());
				var5.addVertexWithUV(par2 + 1, par3 + 1 + 0.021875F,
						par4 + 0.015625D, var10.getMaxU(), var10.getMaxV());
				var5.addVertexWithUV(par2 + 0, par3 + 1 + 0.021875F,
						par4 + 0.015625D, var10.getMaxU(), var10.getMinV());
				var5.addVertexWithUV(par2 + 0, par3 + 0, par4 + 0.015625D,
						var10.getMinU(), var10.getMinV());
			}

			if (blockAccess.isBlockNormalCube(par2, par3, par4 + 1)
					&& blockAccess.getBlockId(par2, par3 + 1, par4 + 1) == Block.redstoneWire.blockID) {
				var5.setColorOpaque_F(var11 * var13, var11 * var14, var11
						* var15);
				var5.addVertexWithUV(par2 + 1, par3 + 1 + 0.021875F,
						par4 + 1 - 0.015625D, var8.getMaxU(), var8.getMinV());
				var5.addVertexWithUV(par2 + 1, par3 + 0, par4 + 1 - 0.015625D,
						var8.getMinU(), var8.getMinV());
				var5.addVertexWithUV(par2 + 0, par3 + 0, par4 + 1 - 0.015625D,
						var8.getMinU(), var8.getMaxV());
				var5.addVertexWithUV(par2 + 0, par3 + 1 + 0.021875F,
						par4 + 1 - 0.015625D, var8.getMaxU(), var8.getMaxV());
				var5.setColorOpaque_F(var11, var11, var11);
				var5.addVertexWithUV(par2 + 1, par3 + 1 + 0.021875F,
						par4 + 1 - 0.015625D, var10.getMaxU(), var10.getMinV());
				var5.addVertexWithUV(par2 + 1, par3 + 0, par4 + 1 - 0.015625D,
						var10.getMinU(), var10.getMinV());
				var5.addVertexWithUV(par2 + 0, par3 + 0, par4 + 1 - 0.015625D,
						var10.getMinU(), var10.getMaxV());
				var5.addVertexWithUV(par2 + 0, par3 + 1 + 0.021875F,
						par4 + 1 - 0.015625D, var10.getMaxU(), var10.getMaxV());
			}
		}

		if (Config.isBetterSnow() && hasSnowNeighbours(par2, par3, par4)) {
			renderSnow(par2, par3, par4, 0.01D);
		}

		return true;
	}

	/**
	 * Renders a minecart track block at the given coordinates
	 */
	public boolean renderBlockMinecartTrack(
			final BlockRailBase par1BlockRailBase, final int par2,
			final int par3, final int par4) {
		final Tessellator var5 = Tessellator.instance;
		int var6 = blockAccess.getBlockMetadata(par2, par3, par4);
		Icon var7 = getBlockIconFromSideAndMetadata(par1BlockRailBase, 0, var6);

		if (hasOverrideBlockTexture()) {
			var7 = overrideBlockTexture;
		}

		if (Config.isConnectedTextures() && overrideBlockTexture == null) {
			var7 = ConnectedTextures.getConnectedTexture(blockAccess,
					par1BlockRailBase, par2, par3, par4, 1, var7);
		}

		if (par1BlockRailBase.isPowered()) {
			var6 &= 7;
		}

		var5.setBrightness(par1BlockRailBase.getMixedBrightnessForBlock(
				blockAccess, par2, par3, par4));
		var5.setColorOpaque_F(1.0F, 1.0F, 1.0F);
		final double var8 = var7.getMinU();
		final double var10 = var7.getMinV();
		final double var12 = var7.getMaxU();
		final double var14 = var7.getMaxV();
		final double var16 = 0.0625D;
		double var18 = par2 + 1;
		double var20 = par2 + 1;
		double var22 = par2 + 0;
		double var24 = par2 + 0;
		double var26 = par4 + 0;
		double var28 = par4 + 1;
		double var30 = par4 + 1;
		double var32 = par4 + 0;
		double var34 = par3 + var16;
		double var36 = par3 + var16;
		double var38 = par3 + var16;
		double var40 = par3 + var16;

		if (var6 != 1 && var6 != 2 && var6 != 3 && var6 != 7) {
			if (var6 == 8) {
				var18 = var20 = par2 + 0;
				var22 = var24 = par2 + 1;
				var26 = var32 = par4 + 1;
				var28 = var30 = par4 + 0;
			} else if (var6 == 9) {
				var18 = var24 = par2 + 0;
				var20 = var22 = par2 + 1;
				var26 = var28 = par4 + 0;
				var30 = var32 = par4 + 1;
			}
		} else {
			var18 = var24 = par2 + 1;
			var20 = var22 = par2 + 0;
			var26 = var28 = par4 + 1;
			var30 = var32 = par4 + 0;
		}

		if (var6 != 2 && var6 != 4) {
			if (var6 == 3 || var6 == 5) {
				++var36;
				++var38;
			}
		} else {
			++var34;
			++var40;
		}

		var5.addVertexWithUV(var18, var34, var26, var12, var10);
		var5.addVertexWithUV(var20, var36, var28, var12, var14);
		var5.addVertexWithUV(var22, var38, var30, var8, var14);
		var5.addVertexWithUV(var24, var40, var32, var8, var10);
		var5.addVertexWithUV(var24, var40, var32, var8, var10);
		var5.addVertexWithUV(var22, var38, var30, var8, var14);
		var5.addVertexWithUV(var20, var36, var28, var12, var14);
		var5.addVertexWithUV(var18, var34, var26, var12, var10);

		if (Config.isBetterSnow() && hasSnowNeighbours(par2, par3, par4)) {
			renderSnow(par2, par3, par4, 0.05D);
		}

		return true;
	}

	/**
	 * Renders a ladder block at the given coordinates
	 */
	public boolean renderBlockLadder(final Block par1Block, final int par2,
			final int par3, final int par4) {
		final Tessellator var5 = Tessellator.instance;
		Icon var6 = getBlockIconFromSide(par1Block, 0);

		if (hasOverrideBlockTexture()) {
			var6 = overrideBlockTexture;
		}

		if (Config.isConnectedTextures() && overrideBlockTexture == null) {
			var6 = ConnectedTextures.getConnectedTexture(blockAccess,
					par1Block, par2, par3, par4, -1, var6);
		}

		var5.setBrightness(par1Block.getMixedBrightnessForBlock(blockAccess,
				par2, par3, par4));
		final float var7 = 1.0F;
		var5.setColorOpaque_F(var7, var7, var7);
		final double var8 = var6.getMinU();
		final double var10 = var6.getMinV();
		final double var12 = var6.getMaxU();
		final double var14 = var6.getMaxV();
		final int var16 = blockAccess.getBlockMetadata(par2, par3, par4);
		final double var17 = 0.0D;
		final double var19 = 0.05000000074505806D;

		if (var16 == 5) {
			var5.addVertexWithUV(par2 + var19, par3 + 1 + var17, par4 + 1
					+ var17, var8, var10);
			var5.addVertexWithUV(par2 + var19, par3 + 0 - var17, par4 + 1
					+ var17, var8, var14);
			var5.addVertexWithUV(par2 + var19, par3 + 0 - var17, par4 + 0
					- var17, var12, var14);
			var5.addVertexWithUV(par2 + var19, par3 + 1 + var17, par4 + 0
					- var17, var12, var10);
		}

		if (var16 == 4) {
			var5.addVertexWithUV(par2 + 1 - var19, par3 + 0 - var17, par4 + 1
					+ var17, var12, var14);
			var5.addVertexWithUV(par2 + 1 - var19, par3 + 1 + var17, par4 + 1
					+ var17, var12, var10);
			var5.addVertexWithUV(par2 + 1 - var19, par3 + 1 + var17, par4 + 0
					- var17, var8, var10);
			var5.addVertexWithUV(par2 + 1 - var19, par3 + 0 - var17, par4 + 0
					- var17, var8, var14);
		}

		if (var16 == 3) {
			var5.addVertexWithUV(par2 + 1 + var17, par3 + 0 - var17, par4
					+ var19, var12, var14);
			var5.addVertexWithUV(par2 + 1 + var17, par3 + 1 + var17, par4
					+ var19, var12, var10);
			var5.addVertexWithUV(par2 + 0 - var17, par3 + 1 + var17, par4
					+ var19, var8, var10);
			var5.addVertexWithUV(par2 + 0 - var17, par3 + 0 - var17, par4
					+ var19, var8, var14);
		}

		if (var16 == 2) {
			var5.addVertexWithUV(par2 + 1 + var17, par3 + 1 + var17, par4 + 1
					- var19, var8, var10);
			var5.addVertexWithUV(par2 + 1 + var17, par3 + 0 - var17, par4 + 1
					- var19, var8, var14);
			var5.addVertexWithUV(par2 + 0 - var17, par3 + 0 - var17, par4 + 1
					- var19, var12, var14);
			var5.addVertexWithUV(par2 + 0 - var17, par3 + 1 + var17, par4 + 1
					- var19, var12, var10);
		}

		return true;
	}

	/**
	 * Render block vine
	 */
	public boolean renderBlockVine(final Block par1Block, final int par2,
			final int par3, final int par4) {
		final Tessellator var5 = Tessellator.instance;
		Icon var6 = getBlockIconFromSide(par1Block, 0);

		if (hasOverrideBlockTexture()) {
			var6 = overrideBlockTexture;
		}

		if (Config.isConnectedTextures() && overrideBlockTexture == null) {
			var6 = ConnectedTextures.getConnectedTexture(blockAccess,
					par1Block, par2, par3, par4, -1, var6);
		}

		final float var7 = 1.0F;
		var5.setBrightness(par1Block.getMixedBrightnessForBlock(blockAccess,
				par2, par3, par4));
		final int var8 = CustomColorizer.getColorMultiplier(par1Block,
				blockAccess, par2, par3, par4);
		final float var9 = (var8 >> 16 & 255) / 255.0F;
		final float var10 = (var8 >> 8 & 255) / 255.0F;
		final float var11 = (var8 & 255) / 255.0F;
		var5.setColorOpaque_F(var7 * var9, var7 * var10, var7 * var11);
		final double var12 = var6.getMinU();
		final double var14 = var6.getMinV();
		final double var16 = var6.getMaxU();
		final double var18 = var6.getMaxV();
		final double var20 = 0.05000000074505806D;
		final int var22 = blockAccess.getBlockMetadata(par2, par3, par4);

		if ((var22 & 2) != 0) {
			var5.addVertexWithUV(par2 + var20, par3 + 1, par4 + 1, var12, var14);
			var5.addVertexWithUV(par2 + var20, par3 + 0, par4 + 1, var12, var18);
			var5.addVertexWithUV(par2 + var20, par3 + 0, par4 + 0, var16, var18);
			var5.addVertexWithUV(par2 + var20, par3 + 1, par4 + 0, var16, var14);
			var5.addVertexWithUV(par2 + var20, par3 + 1, par4 + 0, var16, var14);
			var5.addVertexWithUV(par2 + var20, par3 + 0, par4 + 0, var16, var18);
			var5.addVertexWithUV(par2 + var20, par3 + 0, par4 + 1, var12, var18);
			var5.addVertexWithUV(par2 + var20, par3 + 1, par4 + 1, var12, var14);
		}

		if ((var22 & 8) != 0) {
			var5.addVertexWithUV(par2 + 1 - var20, par3 + 0, par4 + 1, var16,
					var18);
			var5.addVertexWithUV(par2 + 1 - var20, par3 + 1, par4 + 1, var16,
					var14);
			var5.addVertexWithUV(par2 + 1 - var20, par3 + 1, par4 + 0, var12,
					var14);
			var5.addVertexWithUV(par2 + 1 - var20, par3 + 0, par4 + 0, var12,
					var18);
			var5.addVertexWithUV(par2 + 1 - var20, par3 + 0, par4 + 0, var12,
					var18);
			var5.addVertexWithUV(par2 + 1 - var20, par3 + 1, par4 + 0, var12,
					var14);
			var5.addVertexWithUV(par2 + 1 - var20, par3 + 1, par4 + 1, var16,
					var14);
			var5.addVertexWithUV(par2 + 1 - var20, par3 + 0, par4 + 1, var16,
					var18);
		}

		if ((var22 & 4) != 0) {
			var5.addVertexWithUV(par2 + 1, par3 + 0, par4 + var20, var16, var18);
			var5.addVertexWithUV(par2 + 1, par3 + 1, par4 + var20, var16, var14);
			var5.addVertexWithUV(par2 + 0, par3 + 1, par4 + var20, var12, var14);
			var5.addVertexWithUV(par2 + 0, par3 + 0, par4 + var20, var12, var18);
			var5.addVertexWithUV(par2 + 0, par3 + 0, par4 + var20, var12, var18);
			var5.addVertexWithUV(par2 + 0, par3 + 1, par4 + var20, var12, var14);
			var5.addVertexWithUV(par2 + 1, par3 + 1, par4 + var20, var16, var14);
			var5.addVertexWithUV(par2 + 1, par3 + 0, par4 + var20, var16, var18);
		}

		if ((var22 & 1) != 0) {
			var5.addVertexWithUV(par2 + 1, par3 + 1, par4 + 1 - var20, var12,
					var14);
			var5.addVertexWithUV(par2 + 1, par3 + 0, par4 + 1 - var20, var12,
					var18);
			var5.addVertexWithUV(par2 + 0, par3 + 0, par4 + 1 - var20, var16,
					var18);
			var5.addVertexWithUV(par2 + 0, par3 + 1, par4 + 1 - var20, var16,
					var14);
			var5.addVertexWithUV(par2 + 0, par3 + 1, par4 + 1 - var20, var16,
					var14);
			var5.addVertexWithUV(par2 + 0, par3 + 0, par4 + 1 - var20, var16,
					var18);
			var5.addVertexWithUV(par2 + 1, par3 + 0, par4 + 1 - var20, var12,
					var18);
			var5.addVertexWithUV(par2 + 1, par3 + 1, par4 + 1 - var20, var12,
					var14);
		}

		if (blockAccess.isBlockNormalCube(par2, par3 + 1, par4)) {
			var5.addVertexWithUV(par2 + 1, par3 + 1 - var20, par4 + 0, var12,
					var14);
			var5.addVertexWithUV(par2 + 1, par3 + 1 - var20, par4 + 1, var12,
					var18);
			var5.addVertexWithUV(par2 + 0, par3 + 1 - var20, par4 + 1, var16,
					var18);
			var5.addVertexWithUV(par2 + 0, par3 + 1 - var20, par4 + 0, var16,
					var14);
		}

		return true;
	}

	public boolean renderBlockPane(final BlockPane par1BlockPane,
			final int par2, final int par3, final int par4) {
		final int var5 = blockAccess.getHeight();
		final Tessellator var6 = Tessellator.instance;
		var6.setBrightness(par1BlockPane.getMixedBrightnessForBlock(
				blockAccess, par2, par3, par4));
		final float var7 = 1.0F;
		final int var8 = par1BlockPane.colorMultiplier(blockAccess, par2, par3,
				par4);
		float var9 = (var8 >> 16 & 255) / 255.0F;
		float var10 = (var8 >> 8 & 255) / 255.0F;
		float var11 = (var8 & 255) / 255.0F;

		if (EntityRenderer.anaglyphEnable) {
			final float var12 = (var9 * 30.0F + var10 * 59.0F + var11 * 11.0F) / 100.0F;
			final float var13 = (var9 * 30.0F + var10 * 70.0F) / 100.0F;
			final float var14 = (var9 * 30.0F + var11 * 70.0F) / 100.0F;
			var9 = var12;
			var10 = var13;
			var11 = var14;
		}

		var6.setColorOpaque_F(var7 * var9, var7 * var10, var7 * var11);
		ConnectedProperties var15 = null;
		int var106;
		Icon var104;
		Icon var105;

		if (hasOverrideBlockTexture()) {
			var104 = overrideBlockTexture;
			var105 = overrideBlockTexture;
		} else {
			var106 = blockAccess.getBlockMetadata(par2, par3, par4);
			var104 = getBlockIconFromSideAndMetadata(par1BlockPane, 0, var106);
			var105 = par1BlockPane.getSideTextureIndex();

			if (Config.isConnectedTextures()) {
				var15 = ConnectedTextures.getConnectedProperties(blockAccess,
						par1BlockPane, par2, par3, par4, -1, var104);
			}
		}

		Icon var16 = var104;
		Icon var17 = var104;
		Icon var18 = var104;
		int var19;

		if (var15 != null) {
			var19 = par1BlockPane.blockID;
			final int var20 = blockAccess.getBlockId(par2 + 1, par3, par4);
			final int var21 = blockAccess.getBlockId(par2 - 1, par3, par4);
			final int var22 = blockAccess.getBlockId(par2, par3 + 1, par4);
			final int var23 = blockAccess.getBlockId(par2, par3 - 1, par4);
			final int var24 = blockAccess.getBlockId(par2, par3, par4 + 1);
			final int var25 = blockAccess.getBlockId(par2, par3, par4 - 1);
			final boolean var26 = var20 == var19;
			final boolean var27 = var21 == var19;
			final boolean var28 = var22 == var19;
			final boolean var29 = var23 == var19;
			final boolean var30 = var24 == var19;
			final boolean var31 = var25 == var19;
			final int var32 = ConnectedTextures.getPaneTextureIndex(var26,
					var27, var28, var29);
			final int var33 = ConnectedTextures
					.getReversePaneTextureIndex(var32);
			final int var34 = ConnectedTextures.getPaneTextureIndex(var30,
					var31, var28, var29);
			final int var35 = ConnectedTextures
					.getReversePaneTextureIndex(var34);
			var104 = ConnectedTextures.getCtmTexture(var15, var32, var104);
			var16 = ConnectedTextures.getCtmTexture(var15, var33, var16);
			var17 = ConnectedTextures.getCtmTexture(var15, var34, var17);
			var18 = ConnectedTextures.getCtmTexture(var15, var35, var18);
		}

		var106 = var104.getOriginX();
		var19 = var104.getOriginY();
		final double var107 = var104.getMinU();
		final double var108 = var104.getInterpolatedU(8.0D);
		final double var109 = var104.getMaxU();
		final double var111 = var104.getMinV();
		final double var110 = var104.getMaxV();
		var16.getOriginX();
		var16.getOriginY();
		final double var114 = var16.getMinU();
		final double var115 = var16.getInterpolatedU(8.0D);
		final double var36 = var16.getMaxU();
		final double var38 = var16.getMinV();
		final double var40 = var16.getMaxV();
		var17.getOriginX();
		var17.getOriginY();
		final double var44 = var17.getMinU();
		final double var46 = var17.getInterpolatedU(8.0D);
		final double var48 = var17.getMaxU();
		final double var50 = var17.getMinV();
		final double var52 = var17.getMaxV();
		var18.getOriginX();
		var18.getOriginY();
		final double var56 = var18.getMinU();
		final double var58 = var18.getInterpolatedU(8.0D);
		final double var60 = var18.getMaxU();
		final double var62 = var18.getMinV();
		final double var64 = var18.getMaxV();
		var105.getOriginX();
		var105.getOriginY();
		final double var68 = var105.getInterpolatedU(7.0D);
		final double var70 = var105.getInterpolatedU(9.0D);
		final double var72 = var105.getMinV();
		final double var74 = var105.getInterpolatedV(8.0D);
		final double var76 = var105.getMaxV();
		final double var78 = par2;
		final double var80 = par2 + 0.5D;
		final double var82 = par2 + 1;
		final double var84 = par4;
		final double var86 = par4 + 0.5D;
		final double var88 = par4 + 1;
		final double var90 = par2 + 0.5D - 0.0625D;
		final double var92 = par2 + 0.5D + 0.0625D;
		final double var94 = par4 + 0.5D - 0.0625D;
		final double var96 = par4 + 0.5D + 0.0625D;
		final boolean var98 = par1BlockPane
				.canThisPaneConnectToThisBlockID(blockAccess.getBlockId(par2,
						par3, par4 - 1));
		final boolean var99 = par1BlockPane
				.canThisPaneConnectToThisBlockID(blockAccess.getBlockId(par2,
						par3, par4 + 1));
		final boolean var100 = par1BlockPane
				.canThisPaneConnectToThisBlockID(blockAccess.getBlockId(
						par2 - 1, par3, par4));
		final boolean var101 = par1BlockPane
				.canThisPaneConnectToThisBlockID(blockAccess.getBlockId(
						par2 + 1, par3, par4));
		final boolean var102 = par1BlockPane.shouldSideBeRendered(blockAccess,
				par2, par3 + 1, par4, 1);
		final boolean var103 = par1BlockPane.shouldSideBeRendered(blockAccess,
				par2, par3 - 1, par4, 0);

		if ((!var100 || !var101) && (var100 || var101 || var98 || var99)) {
			if (var100 && !var101) {
				var6.addVertexWithUV(var78, par3 + 1, var86, var107, var111);
				var6.addVertexWithUV(var78, par3 + 0, var86, var107, var110);
				var6.addVertexWithUV(var80, par3 + 0, var86, var108, var110);
				var6.addVertexWithUV(var80, par3 + 1, var86, var108, var111);
				var6.addVertexWithUV(var80, par3 + 1, var86, var115, var38);
				var6.addVertexWithUV(var80, par3 + 0, var86, var115, var40);
				var6.addVertexWithUV(var78, par3 + 0, var86, var36, var40);
				var6.addVertexWithUV(var78, par3 + 1, var86, var36, var38);

				if (!var99 && !var98) {
					var6.addVertexWithUV(var80, par3 + 1, var96, var68, var72);
					var6.addVertexWithUV(var80, par3 + 0, var96, var68, var76);
					var6.addVertexWithUV(var80, par3 + 0, var94, var70, var76);
					var6.addVertexWithUV(var80, par3 + 1, var94, var70, var72);
					var6.addVertexWithUV(var80, par3 + 1, var94, var68, var72);
					var6.addVertexWithUV(var80, par3 + 0, var94, var68, var76);
					var6.addVertexWithUV(var80, par3 + 0, var96, var70, var76);
					var6.addVertexWithUV(var80, par3 + 1, var96, var70, var72);
				}

				if (var102 || par3 < var5 - 1
						&& blockAccess.isAirBlock(par2 - 1, par3 + 1, par4)) {
					var6.addVertexWithUV(var78, par3 + 1 + 0.01D, var96, var70,
							var74);
					var6.addVertexWithUV(var80, par3 + 1 + 0.01D, var96, var70,
							var76);
					var6.addVertexWithUV(var80, par3 + 1 + 0.01D, var94, var68,
							var76);
					var6.addVertexWithUV(var78, par3 + 1 + 0.01D, var94, var68,
							var74);
					var6.addVertexWithUV(var80, par3 + 1 + 0.01D, var96, var70,
							var74);
					var6.addVertexWithUV(var78, par3 + 1 + 0.01D, var96, var70,
							var76);
					var6.addVertexWithUV(var78, par3 + 1 + 0.01D, var94, var68,
							var76);
					var6.addVertexWithUV(var80, par3 + 1 + 0.01D, var94, var68,
							var74);
				}

				if (var103 || par3 > 1
						&& blockAccess.isAirBlock(par2 - 1, par3 - 1, par4)) {
					var6.addVertexWithUV(var78, par3 - 0.01D, var96, var70,
							var74);
					var6.addVertexWithUV(var80, par3 - 0.01D, var96, var70,
							var76);
					var6.addVertexWithUV(var80, par3 - 0.01D, var94, var68,
							var76);
					var6.addVertexWithUV(var78, par3 - 0.01D, var94, var68,
							var74);
					var6.addVertexWithUV(var80, par3 - 0.01D, var96, var70,
							var74);
					var6.addVertexWithUV(var78, par3 - 0.01D, var96, var70,
							var76);
					var6.addVertexWithUV(var78, par3 - 0.01D, var94, var68,
							var76);
					var6.addVertexWithUV(var80, par3 - 0.01D, var94, var68,
							var74);
				}
			} else if (!var100 && var101) {
				var6.addVertexWithUV(var80, par3 + 1, var86, var108, var111);
				var6.addVertexWithUV(var80, par3 + 0, var86, var108, var110);
				var6.addVertexWithUV(var82, par3 + 0, var86, var109, var110);
				var6.addVertexWithUV(var82, par3 + 1, var86, var109, var111);
				var6.addVertexWithUV(var82, par3 + 1, var86, var114, var38);
				var6.addVertexWithUV(var82, par3 + 0, var86, var114, var40);
				var6.addVertexWithUV(var80, par3 + 0, var86, var115, var40);
				var6.addVertexWithUV(var80, par3 + 1, var86, var115, var38);

				if (!var99 && !var98) {
					var6.addVertexWithUV(var80, par3 + 1, var94, var68, var72);
					var6.addVertexWithUV(var80, par3 + 0, var94, var68, var76);
					var6.addVertexWithUV(var80, par3 + 0, var96, var70, var76);
					var6.addVertexWithUV(var80, par3 + 1, var96, var70, var72);
					var6.addVertexWithUV(var80, par3 + 1, var96, var68, var72);
					var6.addVertexWithUV(var80, par3 + 0, var96, var68, var76);
					var6.addVertexWithUV(var80, par3 + 0, var94, var70, var76);
					var6.addVertexWithUV(var80, par3 + 1, var94, var70, var72);
				}

				if (var102 || par3 < var5 - 1
						&& blockAccess.isAirBlock(par2 + 1, par3 + 1, par4)) {
					var6.addVertexWithUV(var80, par3 + 1 + 0.01D, var96, var70,
							var72);
					var6.addVertexWithUV(var82, par3 + 1 + 0.01D, var96, var70,
							var74);
					var6.addVertexWithUV(var82, par3 + 1 + 0.01D, var94, var68,
							var74);
					var6.addVertexWithUV(var80, par3 + 1 + 0.01D, var94, var68,
							var72);
					var6.addVertexWithUV(var82, par3 + 1 + 0.01D, var96, var70,
							var72);
					var6.addVertexWithUV(var80, par3 + 1 + 0.01D, var96, var70,
							var74);
					var6.addVertexWithUV(var80, par3 + 1 + 0.01D, var94, var68,
							var74);
					var6.addVertexWithUV(var82, par3 + 1 + 0.01D, var94, var68,
							var72);
				}

				if (var103 || par3 > 1
						&& blockAccess.isAirBlock(par2 + 1, par3 - 1, par4)) {
					var6.addVertexWithUV(var80, par3 - 0.01D, var96, var70,
							var72);
					var6.addVertexWithUV(var82, par3 - 0.01D, var96, var70,
							var74);
					var6.addVertexWithUV(var82, par3 - 0.01D, var94, var68,
							var74);
					var6.addVertexWithUV(var80, par3 - 0.01D, var94, var68,
							var72);
					var6.addVertexWithUV(var82, par3 - 0.01D, var96, var70,
							var72);
					var6.addVertexWithUV(var80, par3 - 0.01D, var96, var70,
							var74);
					var6.addVertexWithUV(var80, par3 - 0.01D, var94, var68,
							var74);
					var6.addVertexWithUV(var82, par3 - 0.01D, var94, var68,
							var72);
				}
			}
		} else {
			var6.addVertexWithUV(var78, par3 + 1, var86, var107, var111);
			var6.addVertexWithUV(var78, par3 + 0, var86, var107, var110);
			var6.addVertexWithUV(var82, par3 + 0, var86, var109, var110);
			var6.addVertexWithUV(var82, par3 + 1, var86, var109, var111);
			var6.addVertexWithUV(var82, par3 + 1, var86, var114, var38);
			var6.addVertexWithUV(var82, par3 + 0, var86, var114, var40);
			var6.addVertexWithUV(var78, par3 + 0, var86, var36, var40);
			var6.addVertexWithUV(var78, par3 + 1, var86, var36, var38);

			if (var102) {
				var6.addVertexWithUV(var78, par3 + 1 + 0.01D, var96, var70,
						var76);
				var6.addVertexWithUV(var82, par3 + 1 + 0.01D, var96, var70,
						var72);
				var6.addVertexWithUV(var82, par3 + 1 + 0.01D, var94, var68,
						var72);
				var6.addVertexWithUV(var78, par3 + 1 + 0.01D, var94, var68,
						var76);
				var6.addVertexWithUV(var82, par3 + 1 + 0.01D, var96, var70,
						var76);
				var6.addVertexWithUV(var78, par3 + 1 + 0.01D, var96, var70,
						var72);
				var6.addVertexWithUV(var78, par3 + 1 + 0.01D, var94, var68,
						var72);
				var6.addVertexWithUV(var82, par3 + 1 + 0.01D, var94, var68,
						var76);
			} else {
				if (par3 < var5 - 1
						&& blockAccess.isAirBlock(par2 - 1, par3 + 1, par4)) {
					var6.addVertexWithUV(var78, par3 + 1 + 0.01D, var96, var70,
							var74);
					var6.addVertexWithUV(var80, par3 + 1 + 0.01D, var96, var70,
							var76);
					var6.addVertexWithUV(var80, par3 + 1 + 0.01D, var94, var68,
							var76);
					var6.addVertexWithUV(var78, par3 + 1 + 0.01D, var94, var68,
							var74);
					var6.addVertexWithUV(var80, par3 + 1 + 0.01D, var96, var70,
							var74);
					var6.addVertexWithUV(var78, par3 + 1 + 0.01D, var96, var70,
							var76);
					var6.addVertexWithUV(var78, par3 + 1 + 0.01D, var94, var68,
							var76);
					var6.addVertexWithUV(var80, par3 + 1 + 0.01D, var94, var68,
							var74);
				}

				if (par3 < var5 - 1
						&& blockAccess.isAirBlock(par2 + 1, par3 + 1, par4)) {
					var6.addVertexWithUV(var80, par3 + 1 + 0.01D, var96, var70,
							var72);
					var6.addVertexWithUV(var82, par3 + 1 + 0.01D, var96, var70,
							var74);
					var6.addVertexWithUV(var82, par3 + 1 + 0.01D, var94, var68,
							var74);
					var6.addVertexWithUV(var80, par3 + 1 + 0.01D, var94, var68,
							var72);
					var6.addVertexWithUV(var82, par3 + 1 + 0.01D, var96, var70,
							var72);
					var6.addVertexWithUV(var80, par3 + 1 + 0.01D, var96, var70,
							var74);
					var6.addVertexWithUV(var80, par3 + 1 + 0.01D, var94, var68,
							var74);
					var6.addVertexWithUV(var82, par3 + 1 + 0.01D, var94, var68,
							var72);
				}
			}

			if (var103) {
				var6.addVertexWithUV(var78, par3 - 0.01D, var96, var70, var76);
				var6.addVertexWithUV(var82, par3 - 0.01D, var96, var70, var72);
				var6.addVertexWithUV(var82, par3 - 0.01D, var94, var68, var72);
				var6.addVertexWithUV(var78, par3 - 0.01D, var94, var68, var76);
				var6.addVertexWithUV(var82, par3 - 0.01D, var96, var70, var76);
				var6.addVertexWithUV(var78, par3 - 0.01D, var96, var70, var72);
				var6.addVertexWithUV(var78, par3 - 0.01D, var94, var68, var72);
				var6.addVertexWithUV(var82, par3 - 0.01D, var94, var68, var76);
			} else {
				if (par3 > 1
						&& blockAccess.isAirBlock(par2 - 1, par3 - 1, par4)) {
					var6.addVertexWithUV(var78, par3 - 0.01D, var96, var70,
							var74);
					var6.addVertexWithUV(var80, par3 - 0.01D, var96, var70,
							var76);
					var6.addVertexWithUV(var80, par3 - 0.01D, var94, var68,
							var76);
					var6.addVertexWithUV(var78, par3 - 0.01D, var94, var68,
							var74);
					var6.addVertexWithUV(var80, par3 - 0.01D, var96, var70,
							var74);
					var6.addVertexWithUV(var78, par3 - 0.01D, var96, var70,
							var76);
					var6.addVertexWithUV(var78, par3 - 0.01D, var94, var68,
							var76);
					var6.addVertexWithUV(var80, par3 - 0.01D, var94, var68,
							var74);
				}

				if (par3 > 1
						&& blockAccess.isAirBlock(par2 + 1, par3 - 1, par4)) {
					var6.addVertexWithUV(var80, par3 - 0.01D, var96, var70,
							var72);
					var6.addVertexWithUV(var82, par3 - 0.01D, var96, var70,
							var74);
					var6.addVertexWithUV(var82, par3 - 0.01D, var94, var68,
							var74);
					var6.addVertexWithUV(var80, par3 - 0.01D, var94, var68,
							var72);
					var6.addVertexWithUV(var82, par3 - 0.01D, var96, var70,
							var72);
					var6.addVertexWithUV(var80, par3 - 0.01D, var96, var70,
							var74);
					var6.addVertexWithUV(var80, par3 - 0.01D, var94, var68,
							var74);
					var6.addVertexWithUV(var82, par3 - 0.01D, var94, var68,
							var72);
				}
			}
		}

		if ((!var98 || !var99) && (var100 || var101 || var98 || var99)) {
			if (var98 && !var99) {
				var6.addVertexWithUV(var80, par3 + 1, var84, var44, var50);
				var6.addVertexWithUV(var80, par3 + 0, var84, var44, var52);
				var6.addVertexWithUV(var80, par3 + 0, var86, var46, var52);
				var6.addVertexWithUV(var80, par3 + 1, var86, var46, var50);
				var6.addVertexWithUV(var80, par3 + 1, var86, var58, var62);
				var6.addVertexWithUV(var80, par3 + 0, var86, var58, var64);
				var6.addVertexWithUV(var80, par3 + 0, var84, var60, var64);
				var6.addVertexWithUV(var80, par3 + 1, var84, var60, var62);

				if (!var101 && !var100) {
					var6.addVertexWithUV(var90, par3 + 1, var86, var68, var72);
					var6.addVertexWithUV(var90, par3 + 0, var86, var68, var76);
					var6.addVertexWithUV(var92, par3 + 0, var86, var70, var76);
					var6.addVertexWithUV(var92, par3 + 1, var86, var70, var72);
					var6.addVertexWithUV(var92, par3 + 1, var86, var68, var72);
					var6.addVertexWithUV(var92, par3 + 0, var86, var68, var76);
					var6.addVertexWithUV(var90, par3 + 0, var86, var70, var76);
					var6.addVertexWithUV(var90, par3 + 1, var86, var70, var72);
				}

				if (var102 || par3 < var5 - 1
						&& blockAccess.isAirBlock(par2, par3 + 1, par4 - 1)) {
					var6.addVertexWithUV(var90, par3 + 1 + 0.005D, var84,
							var70, var72);
					var6.addVertexWithUV(var90, par3 + 1 + 0.005D, var86,
							var70, var74);
					var6.addVertexWithUV(var92, par3 + 1 + 0.005D, var86,
							var68, var74);
					var6.addVertexWithUV(var92, par3 + 1 + 0.005D, var84,
							var68, var72);
					var6.addVertexWithUV(var90, par3 + 1 + 0.005D, var86,
							var70, var72);
					var6.addVertexWithUV(var90, par3 + 1 + 0.005D, var84,
							var70, var74);
					var6.addVertexWithUV(var92, par3 + 1 + 0.005D, var84,
							var68, var74);
					var6.addVertexWithUV(var92, par3 + 1 + 0.005D, var86,
							var68, var72);
				}

				if (var103 || par3 > 1
						&& blockAccess.isAirBlock(par2, par3 - 1, par4 - 1)) {
					var6.addVertexWithUV(var90, par3 - 0.005D, var84, var70,
							var72);
					var6.addVertexWithUV(var90, par3 - 0.005D, var86, var70,
							var74);
					var6.addVertexWithUV(var92, par3 - 0.005D, var86, var68,
							var74);
					var6.addVertexWithUV(var92, par3 - 0.005D, var84, var68,
							var72);
					var6.addVertexWithUV(var90, par3 - 0.005D, var86, var70,
							var72);
					var6.addVertexWithUV(var90, par3 - 0.005D, var84, var70,
							var74);
					var6.addVertexWithUV(var92, par3 - 0.005D, var84, var68,
							var74);
					var6.addVertexWithUV(var92, par3 - 0.005D, var86, var68,
							var72);
				}
			} else if (!var98 && var99) {
				var6.addVertexWithUV(var80, par3 + 1, var86, var46, var50);
				var6.addVertexWithUV(var80, par3 + 0, var86, var46, var52);
				var6.addVertexWithUV(var80, par3 + 0, var88, var48, var52);
				var6.addVertexWithUV(var80, par3 + 1, var88, var48, var50);
				var6.addVertexWithUV(var80, par3 + 1, var88, var56, var62);
				var6.addVertexWithUV(var80, par3 + 0, var88, var56, var64);
				var6.addVertexWithUV(var80, par3 + 0, var86, var58, var64);
				var6.addVertexWithUV(var80, par3 + 1, var86, var58, var62);

				if (!var101 && !var100) {
					var6.addVertexWithUV(var92, par3 + 1, var86, var68, var72);
					var6.addVertexWithUV(var92, par3 + 0, var86, var68, var76);
					var6.addVertexWithUV(var90, par3 + 0, var86, var70, var76);
					var6.addVertexWithUV(var90, par3 + 1, var86, var70, var72);
					var6.addVertexWithUV(var90, par3 + 1, var86, var68, var72);
					var6.addVertexWithUV(var90, par3 + 0, var86, var68, var76);
					var6.addVertexWithUV(var92, par3 + 0, var86, var70, var76);
					var6.addVertexWithUV(var92, par3 + 1, var86, var70, var72);
				}

				if (var102 || par3 < var5 - 1
						&& blockAccess.isAirBlock(par2, par3 + 1, par4 + 1)) {
					var6.addVertexWithUV(var90, par3 + 1 + 0.005D, var86,
							var68, var74);
					var6.addVertexWithUV(var90, par3 + 1 + 0.005D, var88,
							var68, var76);
					var6.addVertexWithUV(var92, par3 + 1 + 0.005D, var88,
							var70, var76);
					var6.addVertexWithUV(var92, par3 + 1 + 0.005D, var86,
							var70, var74);
					var6.addVertexWithUV(var90, par3 + 1 + 0.005D, var88,
							var68, var74);
					var6.addVertexWithUV(var90, par3 + 1 + 0.005D, var86,
							var68, var76);
					var6.addVertexWithUV(var92, par3 + 1 + 0.005D, var86,
							var70, var76);
					var6.addVertexWithUV(var92, par3 + 1 + 0.005D, var88,
							var70, var74);
				}

				if (var103 || par3 > 1
						&& blockAccess.isAirBlock(par2, par3 - 1, par4 + 1)) {
					var6.addVertexWithUV(var90, par3 - 0.005D, var86, var68,
							var74);
					var6.addVertexWithUV(var90, par3 - 0.005D, var88, var68,
							var76);
					var6.addVertexWithUV(var92, par3 - 0.005D, var88, var70,
							var76);
					var6.addVertexWithUV(var92, par3 - 0.005D, var86, var70,
							var74);
					var6.addVertexWithUV(var90, par3 - 0.005D, var88, var68,
							var74);
					var6.addVertexWithUV(var90, par3 - 0.005D, var86, var68,
							var76);
					var6.addVertexWithUV(var92, par3 - 0.005D, var86, var70,
							var76);
					var6.addVertexWithUV(var92, par3 - 0.005D, var88, var70,
							var74);
				}
			}
		} else {
			var6.addVertexWithUV(var80, par3 + 1, var88, var56, var62);
			var6.addVertexWithUV(var80, par3 + 0, var88, var56, var64);
			var6.addVertexWithUV(var80, par3 + 0, var84, var60, var64);
			var6.addVertexWithUV(var80, par3 + 1, var84, var60, var62);
			var6.addVertexWithUV(var80, par3 + 1, var84, var44, var50);
			var6.addVertexWithUV(var80, par3 + 0, var84, var44, var52);
			var6.addVertexWithUV(var80, par3 + 0, var88, var48, var52);
			var6.addVertexWithUV(var80, par3 + 1, var88, var48, var50);

			if (var102) {
				var6.addVertexWithUV(var92, par3 + 1 + 0.005D, var88, var70,
						var76);
				var6.addVertexWithUV(var92, par3 + 1 + 0.005D, var84, var70,
						var72);
				var6.addVertexWithUV(var90, par3 + 1 + 0.005D, var84, var68,
						var72);
				var6.addVertexWithUV(var90, par3 + 1 + 0.005D, var88, var68,
						var76);
				var6.addVertexWithUV(var92, par3 + 1 + 0.005D, var84, var70,
						var76);
				var6.addVertexWithUV(var92, par3 + 1 + 0.005D, var88, var70,
						var72);
				var6.addVertexWithUV(var90, par3 + 1 + 0.005D, var88, var68,
						var72);
				var6.addVertexWithUV(var90, par3 + 1 + 0.005D, var84, var68,
						var76);
			} else {
				if (par3 < var5 - 1
						&& blockAccess.isAirBlock(par2, par3 + 1, par4 - 1)) {
					var6.addVertexWithUV(var90, par3 + 1 + 0.005D, var84,
							var70, var72);
					var6.addVertexWithUV(var90, par3 + 1 + 0.005D, var86,
							var70, var74);
					var6.addVertexWithUV(var92, par3 + 1 + 0.005D, var86,
							var68, var74);
					var6.addVertexWithUV(var92, par3 + 1 + 0.005D, var84,
							var68, var72);
					var6.addVertexWithUV(var90, par3 + 1 + 0.005D, var86,
							var70, var72);
					var6.addVertexWithUV(var90, par3 + 1 + 0.005D, var84,
							var70, var74);
					var6.addVertexWithUV(var92, par3 + 1 + 0.005D, var84,
							var68, var74);
					var6.addVertexWithUV(var92, par3 + 1 + 0.005D, var86,
							var68, var72);
				}

				if (par3 < var5 - 1
						&& blockAccess.isAirBlock(par2, par3 + 1, par4 + 1)) {
					var6.addVertexWithUV(var90, par3 + 1 + 0.005D, var86,
							var68, var74);
					var6.addVertexWithUV(var90, par3 + 1 + 0.005D, var88,
							var68, var76);
					var6.addVertexWithUV(var92, par3 + 1 + 0.005D, var88,
							var70, var76);
					var6.addVertexWithUV(var92, par3 + 1 + 0.005D, var86,
							var70, var74);
					var6.addVertexWithUV(var90, par3 + 1 + 0.005D, var88,
							var68, var74);
					var6.addVertexWithUV(var90, par3 + 1 + 0.005D, var86,
							var68, var76);
					var6.addVertexWithUV(var92, par3 + 1 + 0.005D, var86,
							var70, var76);
					var6.addVertexWithUV(var92, par3 + 1 + 0.005D, var88,
							var70, var74);
				}
			}

			if (var103) {
				var6.addVertexWithUV(var92, par3 - 0.005D, var88, var70, var76);
				var6.addVertexWithUV(var92, par3 - 0.005D, var84, var70, var72);
				var6.addVertexWithUV(var90, par3 - 0.005D, var84, var68, var72);
				var6.addVertexWithUV(var90, par3 - 0.005D, var88, var68, var76);
				var6.addVertexWithUV(var92, par3 - 0.005D, var84, var70, var76);
				var6.addVertexWithUV(var92, par3 - 0.005D, var88, var70, var72);
				var6.addVertexWithUV(var90, par3 - 0.005D, var88, var68, var72);
				var6.addVertexWithUV(var90, par3 - 0.005D, var84, var68, var76);
			} else {
				if (par3 > 1
						&& blockAccess.isAirBlock(par2, par3 - 1, par4 - 1)) {
					var6.addVertexWithUV(var90, par3 - 0.005D, var84, var70,
							var72);
					var6.addVertexWithUV(var90, par3 - 0.005D, var86, var70,
							var74);
					var6.addVertexWithUV(var92, par3 - 0.005D, var86, var68,
							var74);
					var6.addVertexWithUV(var92, par3 - 0.005D, var84, var68,
							var72);
					var6.addVertexWithUV(var90, par3 - 0.005D, var86, var70,
							var72);
					var6.addVertexWithUV(var90, par3 - 0.005D, var84, var70,
							var74);
					var6.addVertexWithUV(var92, par3 - 0.005D, var84, var68,
							var74);
					var6.addVertexWithUV(var92, par3 - 0.005D, var86, var68,
							var72);
				}

				if (par3 > 1
						&& blockAccess.isAirBlock(par2, par3 - 1, par4 + 1)) {
					var6.addVertexWithUV(var90, par3 - 0.005D, var86, var68,
							var74);
					var6.addVertexWithUV(var90, par3 - 0.005D, var88, var68,
							var76);
					var6.addVertexWithUV(var92, par3 - 0.005D, var88, var70,
							var76);
					var6.addVertexWithUV(var92, par3 - 0.005D, var86, var70,
							var74);
					var6.addVertexWithUV(var90, par3 - 0.005D, var88, var68,
							var74);
					var6.addVertexWithUV(var90, par3 - 0.005D, var86, var68,
							var76);
					var6.addVertexWithUV(var92, par3 - 0.005D, var86, var70,
							var76);
					var6.addVertexWithUV(var92, par3 - 0.005D, var88, var70,
							var74);
				}
			}
		}

		if (Config.isBetterSnow() && hasSnowNeighbours(par2, par3, par4)) {
			renderSnow(par2, par3, par4, Block.snow.maxY);
		}

		return true;
	}

	/**
	 * Renders any block requiring croseed squares such as reeds, flowers, and
	 * mushrooms
	 */
	public boolean renderCrossedSquares(final Block par1Block, final int par2,
			final int par3, final int par4) {
		final Tessellator var5 = Tessellator.instance;
		var5.setBrightness(par1Block.getMixedBrightnessForBlock(blockAccess,
				par2, par3, par4));
		final float var6 = 1.0F;
		final int var7 = CustomColorizer.getColorMultiplier(par1Block,
				blockAccess, par2, par3, par4);
		float var8 = (var7 >> 16 & 255) / 255.0F;
		float var9 = (var7 >> 8 & 255) / 255.0F;
		float var10 = (var7 & 255) / 255.0F;

		if (EntityRenderer.anaglyphEnable) {
			final float var11 = (var8 * 30.0F + var9 * 59.0F + var10 * 11.0F) / 100.0F;
			final float var12 = (var8 * 30.0F + var9 * 70.0F) / 100.0F;
			final float var13 = (var8 * 30.0F + var10 * 70.0F) / 100.0F;
			var8 = var11;
			var9 = var12;
			var10 = var13;
		}

		var5.setColorOpaque_F(var6 * var8, var6 * var9, var6 * var10);
		double var19 = par2;
		double var20 = par3;
		double var15 = par4;

		if (par1Block == Block.tallGrass) {
			long var17 = par2 * 3129871 ^ par4 * 116129781L ^ par3;
			var17 = var17 * var17 * 42317861L + var17 * 11L;
			var19 += ((var17 >> 16 & 15L) / 15.0F - 0.5D) * 0.5D;
			var20 += ((var17 >> 20 & 15L) / 15.0F - 1.0D) * 0.2D;
			var15 += ((var17 >> 24 & 15L) / 15.0F - 0.5D) * 0.5D;
		}

		drawCrossedSquares(par1Block,
				blockAccess.getBlockMetadata(par2, par3, par4), var19, var20,
				var15, 1.0F);

		if (Config.isBetterSnow() && hasSnowNeighbours(par2, par3, par4)) {
			renderSnow(par2, par3, par4, Block.snow.maxY);
		}

		return true;
	}

	/**
	 * Render block stem
	 */
	public boolean renderBlockStem(final Block par1Block, final int par2,
			final int par3, final int par4) {
		final BlockStem var5 = (BlockStem) par1Block;
		final Tessellator var6 = Tessellator.instance;
		var6.setBrightness(var5.getMixedBrightnessForBlock(blockAccess, par2,
				par3, par4));
		final float var7 = 1.0F;
		final int var8 = CustomColorizer.getStemColorMultiplier(var5,
				blockAccess, par2, par3, par4);
		float var9 = (var8 >> 16 & 255) / 255.0F;
		float var10 = (var8 >> 8 & 255) / 255.0F;
		float var11 = (var8 & 255) / 255.0F;

		if (EntityRenderer.anaglyphEnable) {
			final float var12 = (var9 * 30.0F + var10 * 59.0F + var11 * 11.0F) / 100.0F;
			final float var13 = (var9 * 30.0F + var10 * 70.0F) / 100.0F;
			final float var14 = (var9 * 30.0F + var11 * 70.0F) / 100.0F;
			var9 = var12;
			var10 = var13;
			var11 = var14;
		}

		var6.setColorOpaque_F(var7 * var9, var7 * var10, var7 * var11);
		var5.setBlockBoundsBasedOnState(blockAccess, par2, par3, par4);
		final int var15 = var5.getState(blockAccess, par2, par3, par4);

		if (var15 < 0) {
			renderBlockStemSmall(var5,
					blockAccess.getBlockMetadata(par2, par3, par4), renderMaxY,
					par2, par3 - 0.0625F, par4);
		} else {
			renderBlockStemSmall(var5,
					blockAccess.getBlockMetadata(par2, par3, par4), 0.5D, par2,
					par3 - 0.0625F, par4);
			renderBlockStemBig(var5,
					blockAccess.getBlockMetadata(par2, par3, par4), var15,
					renderMaxY, par2, par3 - 0.0625F, par4);
		}

		return true;
	}

	/**
	 * Render block crops
	 */
	public boolean renderBlockCrops(final Block par1Block, final int par2,
			final int par3, final int par4) {
		final Tessellator var5 = Tessellator.instance;
		var5.setBrightness(par1Block.getMixedBrightnessForBlock(blockAccess,
				par2, par3, par4));
		var5.setColorOpaque_F(1.0F, 1.0F, 1.0F);
		renderBlockCropsImpl(par1Block,
				blockAccess.getBlockMetadata(par2, par3, par4), par2,
				par3 - 0.0625F, par4);
		return true;
	}

	/**
	 * Renders a torch at the given coordinates, with the base slanting at the
	 * given delta
	 */
	public void renderTorchAtAngle(final Block par1Block, double par2,
			final double par4, double par6, final double par8,
			final double par10, final int par12) {
		final Tessellator var13 = Tessellator.instance;
		Icon var14 = getBlockIconFromSideAndMetadata(par1Block, 0, par12);

		if (hasOverrideBlockTexture()) {
			var14 = overrideBlockTexture;
		}

		final double var15 = var14.getMinU();
		final double var17 = var14.getMinV();
		final double var19 = var14.getMaxU();
		final double var21 = var14.getMaxV();
		final double var23 = var14.getInterpolatedU(7.0D);
		final double var25 = var14.getInterpolatedV(6.0D);
		final double var27 = var14.getInterpolatedU(9.0D);
		final double var29 = var14.getInterpolatedV(8.0D);
		final double var31 = var14.getInterpolatedU(7.0D);
		final double var33 = var14.getInterpolatedV(13.0D);
		final double var35 = var14.getInterpolatedU(9.0D);
		final double var37 = var14.getInterpolatedV(15.0D);
		par2 += 0.5D;
		par6 += 0.5D;
		final double var39 = par2 - 0.5D;
		final double var41 = par2 + 0.5D;
		final double var43 = par6 - 0.5D;
		final double var45 = par6 + 0.5D;
		final double var47 = 0.0625D;
		final double var49 = 0.625D;
		var13.addVertexWithUV(par2 + par8 * (1.0D - var49) - var47, par4
				+ var49, par6 + par10 * (1.0D - var49) - var47, var23, var25);
		var13.addVertexWithUV(par2 + par8 * (1.0D - var49) - var47, par4
				+ var49, par6 + par10 * (1.0D - var49) + var47, var23, var29);
		var13.addVertexWithUV(par2 + par8 * (1.0D - var49) + var47, par4
				+ var49, par6 + par10 * (1.0D - var49) + var47, var27, var29);
		var13.addVertexWithUV(par2 + par8 * (1.0D - var49) + var47, par4
				+ var49, par6 + par10 * (1.0D - var49) - var47, var27, var25);
		var13.addVertexWithUV(par2 + var47 + par8, par4, par6 - var47 + par10,
				var35, var33);
		var13.addVertexWithUV(par2 + var47 + par8, par4, par6 + var47 + par10,
				var35, var37);
		var13.addVertexWithUV(par2 - var47 + par8, par4, par6 + var47 + par10,
				var31, var37);
		var13.addVertexWithUV(par2 - var47 + par8, par4, par6 - var47 + par10,
				var31, var33);
		var13.addVertexWithUV(par2 - var47, par4 + 1.0D, var43, var15, var17);
		var13.addVertexWithUV(par2 - var47 + par8, par4 + 0.0D, var43 + par10,
				var15, var21);
		var13.addVertexWithUV(par2 - var47 + par8, par4 + 0.0D, var45 + par10,
				var19, var21);
		var13.addVertexWithUV(par2 - var47, par4 + 1.0D, var45, var19, var17);
		var13.addVertexWithUV(par2 + var47, par4 + 1.0D, var45, var15, var17);
		var13.addVertexWithUV(par2 + par8 + var47, par4 + 0.0D, var45 + par10,
				var15, var21);
		var13.addVertexWithUV(par2 + par8 + var47, par4 + 0.0D, var43 + par10,
				var19, var21);
		var13.addVertexWithUV(par2 + var47, par4 + 1.0D, var43, var19, var17);
		var13.addVertexWithUV(var39, par4 + 1.0D, par6 + var47, var15, var17);
		var13.addVertexWithUV(var39 + par8, par4 + 0.0D, par6 + var47 + par10,
				var15, var21);
		var13.addVertexWithUV(var41 + par8, par4 + 0.0D, par6 + var47 + par10,
				var19, var21);
		var13.addVertexWithUV(var41, par4 + 1.0D, par6 + var47, var19, var17);
		var13.addVertexWithUV(var41, par4 + 1.0D, par6 - var47, var15, var17);
		var13.addVertexWithUV(var41 + par8, par4 + 0.0D, par6 - var47 + par10,
				var15, var21);
		var13.addVertexWithUV(var39 + par8, par4 + 0.0D, par6 - var47 + par10,
				var19, var21);
		var13.addVertexWithUV(var39, par4 + 1.0D, par6 - var47, var19, var17);
	}

	/**
	 * Utility function to draw crossed swuares
	 */
	public void drawCrossedSquares(final Block par1Block, final int par2,
			final double par3, final double par5, final double par7,
			final float par9) {
		final Tessellator var10 = Tessellator.instance;
		Icon var11 = getBlockIconFromSideAndMetadata(par1Block, 0, par2);

		if (hasOverrideBlockTexture()) {
			var11 = overrideBlockTexture;
		}

		if (Config.isConnectedTextures() && overrideBlockTexture == null) {
			var11 = ConnectedTextures.getConnectedTexture(blockAccess,
					par1Block, (int) par3, (int) par5, (int) par7, -1, var11);
		}

		final double var12 = var11.getMinU();
		final double var14 = var11.getMinV();
		final double var16 = var11.getMaxU();
		final double var18 = var11.getMaxV();
		final double var20 = 0.45D * par9;
		final double var22 = par3 + 0.5D - var20;
		final double var24 = par3 + 0.5D + var20;
		final double var26 = par7 + 0.5D - var20;
		final double var28 = par7 + 0.5D + var20;
		var10.addVertexWithUV(var22, par5 + par9, var26, var12, var14);
		var10.addVertexWithUV(var22, par5 + 0.0D, var26, var12, var18);
		var10.addVertexWithUV(var24, par5 + 0.0D, var28, var16, var18);
		var10.addVertexWithUV(var24, par5 + par9, var28, var16, var14);
		var10.addVertexWithUV(var24, par5 + par9, var28, var12, var14);
		var10.addVertexWithUV(var24, par5 + 0.0D, var28, var12, var18);
		var10.addVertexWithUV(var22, par5 + 0.0D, var26, var16, var18);
		var10.addVertexWithUV(var22, par5 + par9, var26, var16, var14);
		var10.addVertexWithUV(var22, par5 + par9, var28, var12, var14);
		var10.addVertexWithUV(var22, par5 + 0.0D, var28, var12, var18);
		var10.addVertexWithUV(var24, par5 + 0.0D, var26, var16, var18);
		var10.addVertexWithUV(var24, par5 + par9, var26, var16, var14);
		var10.addVertexWithUV(var24, par5 + par9, var26, var12, var14);
		var10.addVertexWithUV(var24, par5 + 0.0D, var26, var12, var18);
		var10.addVertexWithUV(var22, par5 + 0.0D, var28, var16, var18);
		var10.addVertexWithUV(var22, par5 + par9, var28, var16, var14);
	}

	/**
	 * Render block stem small
	 */
	public void renderBlockStemSmall(final Block par1Block, final int par2,
			final double par3, final double par5, final double par7,
			final double par9) {
		final Tessellator var11 = Tessellator.instance;
		Icon var12 = getBlockIconFromSideAndMetadata(par1Block, 0, par2);

		if (hasOverrideBlockTexture()) {
			var12 = overrideBlockTexture;
		}

		final double var13 = var12.getMinU();
		final double var15 = var12.getMinV();
		final double var17 = var12.getMaxU();
		final double var19 = var12.getInterpolatedV(par3 * 16.0D);
		final double var21 = par5 + 0.5D - 0.44999998807907104D;
		final double var23 = par5 + 0.5D + 0.44999998807907104D;
		final double var25 = par9 + 0.5D - 0.44999998807907104D;
		final double var27 = par9 + 0.5D + 0.44999998807907104D;
		var11.addVertexWithUV(var21, par7 + par3, var25, var13, var15);
		var11.addVertexWithUV(var21, par7 + 0.0D, var25, var13, var19);
		var11.addVertexWithUV(var23, par7 + 0.0D, var27, var17, var19);
		var11.addVertexWithUV(var23, par7 + par3, var27, var17, var15);
		var11.addVertexWithUV(var23, par7 + par3, var27, var13, var15);
		var11.addVertexWithUV(var23, par7 + 0.0D, var27, var13, var19);
		var11.addVertexWithUV(var21, par7 + 0.0D, var25, var17, var19);
		var11.addVertexWithUV(var21, par7 + par3, var25, var17, var15);
		var11.addVertexWithUV(var21, par7 + par3, var27, var13, var15);
		var11.addVertexWithUV(var21, par7 + 0.0D, var27, var13, var19);
		var11.addVertexWithUV(var23, par7 + 0.0D, var25, var17, var19);
		var11.addVertexWithUV(var23, par7 + par3, var25, var17, var15);
		var11.addVertexWithUV(var23, par7 + par3, var25, var13, var15);
		var11.addVertexWithUV(var23, par7 + 0.0D, var25, var13, var19);
		var11.addVertexWithUV(var21, par7 + 0.0D, var27, var17, var19);
		var11.addVertexWithUV(var21, par7 + par3, var27, var17, var15);
	}

	/**
	 * Render BlockLilyPad
	 */
	public boolean renderBlockLilyPad(final Block par1Block, final int par2,
			final int par3, final int par4) {
		final Tessellator var5 = Tessellator.instance;
		Icon var6 = getBlockIconFromSide(par1Block, 1);

		if (hasOverrideBlockTexture()) {
			var6 = overrideBlockTexture;
		}

		if (Config.isConnectedTextures() && overrideBlockTexture == null) {
			var6 = ConnectedTextures.getConnectedTexture(blockAccess,
					par1Block, par2, par3, par4, -1, var6);
		}

		final float var7 = 0.015625F;
		final double var8 = var6.getMinU();
		final double var10 = var6.getMinV();
		final double var12 = var6.getMaxU();
		final double var14 = var6.getMaxV();
		long var16 = par2 * 3129871 ^ par4 * 116129781L ^ par3;
		var16 = var16 * var16 * 42317861L + var16 * 11L;
		final int var18 = (int) (var16 >> 16 & 3L);
		var5.setBrightness(par1Block.getMixedBrightnessForBlock(blockAccess,
				par2, par3, par4));
		final float var19 = par2 + 0.5F;
		final float var20 = par4 + 0.5F;
		final float var21 = (var18 & 1) * 0.5F * (1 - var18 / 2 % 2 * 2);
		final float var22 = (var18 + 1 & 1) * 0.5F
				* (1 - (var18 + 1) / 2 % 2 * 2);
		final int var23 = CustomColorizer.getLilypadColor();
		var5.setColorOpaque_I(var23);
		var5.addVertexWithUV(var19 + var21 - var22, par3 + var7, var20 + var21
				+ var22, var8, var10);
		var5.addVertexWithUV(var19 + var21 + var22, par3 + var7, var20 - var21
				+ var22, var12, var10);
		var5.addVertexWithUV(var19 - var21 + var22, par3 + var7, var20 - var21
				- var22, var12, var14);
		var5.addVertexWithUV(var19 - var21 - var22, par3 + var7, var20 + var21
				- var22, var8, var14);
		var5.setColorOpaque_I((var23 & 16711422) >> 1);
		var5.addVertexWithUV(var19 - var21 - var22, par3 + var7, var20 + var21
				- var22, var8, var14);
		var5.addVertexWithUV(var19 - var21 + var22, par3 + var7, var20 - var21
				- var22, var12, var14);
		var5.addVertexWithUV(var19 + var21 + var22, par3 + var7, var20 - var21
				+ var22, var12, var10);
		var5.addVertexWithUV(var19 + var21 - var22, par3 + var7, var20 + var21
				+ var22, var8, var10);
		return true;
	}

	/**
	 * Render block stem big
	 */
	public void renderBlockStemBig(final BlockStem par1BlockStem,
			final int par2, final int par3, final double par4,
			final double par6, final double par8, final double par10) {
		final Tessellator var12 = Tessellator.instance;
		Icon var13 = par1BlockStem.func_94368_p();

		if (hasOverrideBlockTexture()) {
			var13 = overrideBlockTexture;
		}

		double var14 = var13.getMinU();
		final double var16 = var13.getMinV();
		double var18 = var13.getMaxU();
		final double var20 = var13.getMaxV();
		final double var22 = par6 + 0.5D - 0.5D;
		final double var24 = par6 + 0.5D + 0.5D;
		final double var26 = par10 + 0.5D - 0.5D;
		final double var28 = par10 + 0.5D + 0.5D;
		final double var30 = par6 + 0.5D;
		final double var32 = par10 + 0.5D;

		if ((par3 + 1) / 2 % 2 == 1) {
			final double var34 = var18;
			var18 = var14;
			var14 = var34;
		}

		if (par3 < 2) {
			var12.addVertexWithUV(var22, par8 + par4, var32, var14, var16);
			var12.addVertexWithUV(var22, par8 + 0.0D, var32, var14, var20);
			var12.addVertexWithUV(var24, par8 + 0.0D, var32, var18, var20);
			var12.addVertexWithUV(var24, par8 + par4, var32, var18, var16);
			var12.addVertexWithUV(var24, par8 + par4, var32, var18, var16);
			var12.addVertexWithUV(var24, par8 + 0.0D, var32, var18, var20);
			var12.addVertexWithUV(var22, par8 + 0.0D, var32, var14, var20);
			var12.addVertexWithUV(var22, par8 + par4, var32, var14, var16);
		} else {
			var12.addVertexWithUV(var30, par8 + par4, var28, var14, var16);
			var12.addVertexWithUV(var30, par8 + 0.0D, var28, var14, var20);
			var12.addVertexWithUV(var30, par8 + 0.0D, var26, var18, var20);
			var12.addVertexWithUV(var30, par8 + par4, var26, var18, var16);
			var12.addVertexWithUV(var30, par8 + par4, var26, var18, var16);
			var12.addVertexWithUV(var30, par8 + 0.0D, var26, var18, var20);
			var12.addVertexWithUV(var30, par8 + 0.0D, var28, var14, var20);
			var12.addVertexWithUV(var30, par8 + par4, var28, var14, var16);
		}
	}

	/**
	 * Render block crops implementation
	 */
	public void renderBlockCropsImpl(final Block par1Block, final int par2,
			final double par3, final double par5, final double par7) {
		final Tessellator var9 = Tessellator.instance;
		Icon var10 = getBlockIconFromSideAndMetadata(par1Block, 0, par2);

		if (hasOverrideBlockTexture()) {
			var10 = overrideBlockTexture;
		}

		final double var11 = var10.getMinU();
		final double var13 = var10.getMinV();
		final double var15 = var10.getMaxU();
		final double var17 = var10.getMaxV();
		double var19 = par3 + 0.5D - 0.25D;
		double var21 = par3 + 0.5D + 0.25D;
		double var23 = par7 + 0.5D - 0.5D;
		double var25 = par7 + 0.5D + 0.5D;
		var9.addVertexWithUV(var19, par5 + 1.0D, var23, var11, var13);
		var9.addVertexWithUV(var19, par5 + 0.0D, var23, var11, var17);
		var9.addVertexWithUV(var19, par5 + 0.0D, var25, var15, var17);
		var9.addVertexWithUV(var19, par5 + 1.0D, var25, var15, var13);
		var9.addVertexWithUV(var19, par5 + 1.0D, var25, var11, var13);
		var9.addVertexWithUV(var19, par5 + 0.0D, var25, var11, var17);
		var9.addVertexWithUV(var19, par5 + 0.0D, var23, var15, var17);
		var9.addVertexWithUV(var19, par5 + 1.0D, var23, var15, var13);
		var9.addVertexWithUV(var21, par5 + 1.0D, var25, var11, var13);
		var9.addVertexWithUV(var21, par5 + 0.0D, var25, var11, var17);
		var9.addVertexWithUV(var21, par5 + 0.0D, var23, var15, var17);
		var9.addVertexWithUV(var21, par5 + 1.0D, var23, var15, var13);
		var9.addVertexWithUV(var21, par5 + 1.0D, var23, var11, var13);
		var9.addVertexWithUV(var21, par5 + 0.0D, var23, var11, var17);
		var9.addVertexWithUV(var21, par5 + 0.0D, var25, var15, var17);
		var9.addVertexWithUV(var21, par5 + 1.0D, var25, var15, var13);
		var19 = par3 + 0.5D - 0.5D;
		var21 = par3 + 0.5D + 0.5D;
		var23 = par7 + 0.5D - 0.25D;
		var25 = par7 + 0.5D + 0.25D;
		var9.addVertexWithUV(var19, par5 + 1.0D, var23, var11, var13);
		var9.addVertexWithUV(var19, par5 + 0.0D, var23, var11, var17);
		var9.addVertexWithUV(var21, par5 + 0.0D, var23, var15, var17);
		var9.addVertexWithUV(var21, par5 + 1.0D, var23, var15, var13);
		var9.addVertexWithUV(var21, par5 + 1.0D, var23, var11, var13);
		var9.addVertexWithUV(var21, par5 + 0.0D, var23, var11, var17);
		var9.addVertexWithUV(var19, par5 + 0.0D, var23, var15, var17);
		var9.addVertexWithUV(var19, par5 + 1.0D, var23, var15, var13);
		var9.addVertexWithUV(var21, par5 + 1.0D, var25, var11, var13);
		var9.addVertexWithUV(var21, par5 + 0.0D, var25, var11, var17);
		var9.addVertexWithUV(var19, par5 + 0.0D, var25, var15, var17);
		var9.addVertexWithUV(var19, par5 + 1.0D, var25, var15, var13);
		var9.addVertexWithUV(var19, par5 + 1.0D, var25, var11, var13);
		var9.addVertexWithUV(var19, par5 + 0.0D, var25, var11, var17);
		var9.addVertexWithUV(var21, par5 + 0.0D, var25, var15, var17);
		var9.addVertexWithUV(var21, par5 + 1.0D, var25, var15, var13);
	}

	/**
	 * Renders a block based on the BlockFluids class at the given coordinates
	 */
	public boolean renderBlockFluids(final Block par1Block, final int par2,
			final int par3, final int par4) {
		final Tessellator var5 = Tessellator.instance;
		final int var6 = CustomColorizer.getFluidColor(par1Block, blockAccess,
				par2, par3, par4);
		final float var7 = (var6 >> 16 & 255) / 255.0F;
		final float var8 = (var6 >> 8 & 255) / 255.0F;
		final float var9 = (var6 & 255) / 255.0F;
		final boolean var10 = par1Block.shouldSideBeRendered(blockAccess, par2,
				par3 + 1, par4, 1);
		final boolean var11 = par1Block.shouldSideBeRendered(blockAccess, par2,
				par3 - 1, par4, 0);
		final boolean[] var12 = new boolean[] {
				par1Block.shouldSideBeRendered(blockAccess, par2, par3,
						par4 - 1, 2),
				par1Block.shouldSideBeRendered(blockAccess, par2, par3,
						par4 + 1, 3),
				par1Block.shouldSideBeRendered(blockAccess, par2 - 1, par3,
						par4, 4),
				par1Block.shouldSideBeRendered(blockAccess, par2 + 1, par3,
						par4, 5) };

		if (!var10 && !var11 && !var12[0] && !var12[1] && !var12[2]
				&& !var12[3]) {
			return false;
		} else {
			boolean var13 = false;
			final float var14 = 0.5F;
			final float var15 = 1.0F;
			final float var16 = 0.8F;
			final float var17 = 0.6F;
			final double var18 = 0.0D;
			final double var20 = 1.0D;
			final Material var22 = par1Block.blockMaterial;
			final int var23 = blockAccess.getBlockMetadata(par2, par3, par4);
			double var24 = getFluidHeight(par2, par3, par4, var22);
			double var26 = getFluidHeight(par2, par3, par4 + 1, var22);
			double var28 = getFluidHeight(par2 + 1, par3, par4 + 1, var22);
			double var30 = getFluidHeight(par2 + 1, par3, par4, var22);
			final double var32 = 0.0010000000474974513D;
			float var34;
			float var35;
			double var42;
			double var40;
			double var46;
			double var44;
			double var50;
			double var48;

			if (renderAllFaces || var10) {
				var13 = true;
				Icon var36 = getBlockIconFromSideAndMetadata(par1Block, 1,
						var23);
				final float var37 = (float) BlockFluid.getFlowDirection(
						blockAccess, par2, par3, par4, var22);

				if (var37 > -999.0F) {
					var36 = getBlockIconFromSideAndMetadata(par1Block, 2, var23);
				}

				var24 -= var32;
				var26 -= var32;
				var28 -= var32;
				var30 -= var32;
				double var38;
				double var52;

				if (var37 < -999.0F) {
					var40 = var36.getInterpolatedU(0.0D);
					var48 = var36.getInterpolatedV(0.0D);
					var38 = var40;
					var46 = var36.getInterpolatedV(16.0D);
					var44 = var36.getInterpolatedU(16.0D);
					var52 = var46;
					var42 = var44;
					var50 = var48;
				} else {
					var35 = MathHelper.sin(var37) * 0.25F;
					var34 = MathHelper.cos(var37) * 0.25F;
					var40 = var36
							.getInterpolatedU(8.0F + (-var34 - var35) * 16.0F);
					var48 = var36
							.getInterpolatedV(8.0F + (-var34 + var35) * 16.0F);
					var38 = var36
							.getInterpolatedU(8.0F + (-var34 + var35) * 16.0F);
					var46 = var36
							.getInterpolatedV(8.0F + (var34 + var35) * 16.0F);
					var44 = var36
							.getInterpolatedU(8.0F + (var34 + var35) * 16.0F);
					var52 = var36
							.getInterpolatedV(8.0F + (var34 - var35) * 16.0F);
					var42 = var36
							.getInterpolatedU(8.0F + (var34 - var35) * 16.0F);
					var50 = var36
							.getInterpolatedV(8.0F + (-var34 - var35) * 16.0F);
				}

				var5.setBrightness(par1Block.getMixedBrightnessForBlock(
						blockAccess, par2, par3, par4));
				var35 = 1.0F;
				var5.setColorOpaque_F(var15 * var35 * var7, var15 * var35
						* var8, var15 * var35 * var9);
				final double var54 = 3.90625E-5D;
				var5.addVertexWithUV(par2 + 0, par3 + var24, par4 + 0, var40
						+ var54, var48 + var54);
				var5.addVertexWithUV(par2 + 0, par3 + var26, par4 + 1, var38
						+ var54, var46 - var54);
				var5.addVertexWithUV(par2 + 1, par3 + var28, par4 + 1, var44
						- var54, var52 - var54);
				var5.addVertexWithUV(par2 + 1, par3 + var30, par4 + 0, var42
						- var54, var50 + var54);
			}

			if (renderAllFaces || var11) {
				var5.setBrightness(par1Block.getMixedBrightnessForBlock(
						blockAccess, par2, par3 - 1, par4));
				final float var58 = 1.0F;
				var5.setColorOpaque_F(var14 * var58 * var7, var14 * var58
						* var8, var14 * var58 * var9);
				renderFaceYNeg(par1Block, par2, par3 + var32, par4,
						getBlockIconFromSide(par1Block, 0));
				var13 = true;
			}

			for (int var57 = 0; var57 < 4; ++var57) {
				int var59 = par2;
				int var56 = par4;

				if (var57 == 0) {
					var56 = par4 - 1;
				}

				if (var57 == 1) {
					++var56;
				}

				if (var57 == 2) {
					var59 = par2 - 1;
				}

				if (var57 == 3) {
					++var59;
				}

				final Icon var39 = getBlockIconFromSideAndMetadata(par1Block,
						var57 + 2, var23);

				if (renderAllFaces || var12[var57]) {
					if (var57 == 0) {
						var40 = var24;
						var44 = var30;
						var42 = par2;
						var46 = par2 + 1;
						var48 = par4 + var32;
						var50 = par4 + var32;
					} else if (var57 == 1) {
						var40 = var28;
						var44 = var26;
						var42 = par2 + 1;
						var46 = par2;
						var48 = par4 + 1 - var32;
						var50 = par4 + 1 - var32;
					} else if (var57 == 2) {
						var40 = var26;
						var44 = var24;
						var42 = par2 + var32;
						var46 = par2 + var32;
						var48 = par4 + 1;
						var50 = par4;
					} else {
						var40 = var30;
						var44 = var28;
						var42 = par2 + 1 - var32;
						var46 = par2 + 1 - var32;
						var48 = par4;
						var50 = par4 + 1;
					}

					var13 = true;
					final float var60 = var39.getInterpolatedU(0.0D);
					var35 = var39.getInterpolatedU(8.0D);
					var34 = var39
							.getInterpolatedV((1.0D - var40) * 16.0D * 0.5D);
					final float var53 = var39
							.getInterpolatedV((1.0D - var44) * 16.0D * 0.5D);
					final float var61 = var39.getInterpolatedV(8.0D);
					var5.setBrightness(par1Block.getMixedBrightnessForBlock(
							blockAccess, var59, par3, var56));
					float var55 = 1.0F;

					if (var57 < 2) {
						var55 *= var16;
					} else {
						var55 *= var17;
					}

					var5.setColorOpaque_F(var15 * var55 * var7, var15 * var55
							* var8, var15 * var55 * var9);
					var5.addVertexWithUV(var42, par3 + var40, var48, var60,
							var34);
					var5.addVertexWithUV(var46, par3 + var44, var50, var35,
							var53);
					var5.addVertexWithUV(var46, par3 + 0, var50, var35, var61);
					var5.addVertexWithUV(var42, par3 + 0, var48, var60, var61);
				}
			}

			renderMinY = var18;
			renderMaxY = var20;
			return var13;
		}
	}

	/**
	 * Get fluid height
	 */
	public float getFluidHeight(final int par1, final int par2, final int par3,
			final Material par4Material) {
		int var5 = 0;
		float var6 = 0.0F;

		for (int var7 = 0; var7 < 4; ++var7) {
			final int var8 = par1 - (var7 & 1);
			final int var9 = par3 - (var7 >> 1 & 1);

			if (blockAccess.getBlockMaterial(var8, par2 + 1, var9) == par4Material) {
				return 1.0F;
			}

			final Material var10 = blockAccess.getBlockMaterial(var8, par2,
					var9);

			if (var10 == par4Material) {
				final int var11 = blockAccess
						.getBlockMetadata(var8, par2, var9);

				if (var11 >= 8 || var11 == 0) {
					var6 += BlockFluid.getFluidHeightPercent(var11) * 10.0F;
					var5 += 10;
				}

				var6 += BlockFluid.getFluidHeightPercent(var11);
				++var5;
			} else if (!var10.isSolid()) {
				++var6;
				++var5;
			}
		}

		return 1.0F - var6 / var5;
	}

	/**
	 * Renders a falling sand block
	 */
	public void renderBlockSandFalling(final Block par1Block,
			final World par2World, final int par3, final int par4,
			final int par5, final int par6) {
		final float var7 = 0.5F;
		final float var8 = 1.0F;
		final float var9 = 0.8F;
		final float var10 = 0.6F;
		final Tessellator var11 = Tessellator.instance;
		var11.startDrawingQuads();
		var11.setBrightness(par1Block.getMixedBrightnessForBlock(par2World,
				par3, par4, par5));
		final float var12 = 1.0F;
		float var13 = 1.0F;

		if (var13 < var12) {
			var13 = var12;
		}

		var11.setColorOpaque_F(var7 * var13, var7 * var13, var7 * var13);
		renderFaceYNeg(par1Block, -0.5D, -0.5D, -0.5D,
				getBlockIconFromSideAndMetadata(par1Block, 0, par6));
		var13 = 1.0F;

		if (var13 < var12) {
			var13 = var12;
		}

		var11.setColorOpaque_F(var8 * var13, var8 * var13, var8 * var13);
		renderFaceYPos(par1Block, -0.5D, -0.5D, -0.5D,
				getBlockIconFromSideAndMetadata(par1Block, 1, par6));
		var13 = 1.0F;

		if (var13 < var12) {
			var13 = var12;
		}

		var11.setColorOpaque_F(var9 * var13, var9 * var13, var9 * var13);
		renderFaceZNeg(par1Block, -0.5D, -0.5D, -0.5D,
				getBlockIconFromSideAndMetadata(par1Block, 2, par6));
		var13 = 1.0F;

		if (var13 < var12) {
			var13 = var12;
		}

		var11.setColorOpaque_F(var9 * var13, var9 * var13, var9 * var13);
		renderFaceZPos(par1Block, -0.5D, -0.5D, -0.5D,
				getBlockIconFromSideAndMetadata(par1Block, 3, par6));
		var13 = 1.0F;

		if (var13 < var12) {
			var13 = var12;
		}

		var11.setColorOpaque_F(var10 * var13, var10 * var13, var10 * var13);
		renderFaceXNeg(par1Block, -0.5D, -0.5D, -0.5D,
				getBlockIconFromSideAndMetadata(par1Block, 4, par6));
		var13 = 1.0F;

		if (var13 < var12) {
			var13 = var12;
		}

		var11.setColorOpaque_F(var10 * var13, var10 * var13, var10 * var13);
		renderFaceXPos(par1Block, -0.5D, -0.5D, -0.5D,
				getBlockIconFromSideAndMetadata(par1Block, 5, par6));
		var11.draw();
	}

	/**
	 * Renders a standard cube block at the given coordinates
	 */
	public boolean renderStandardBlock(final Block par1Block, final int par2,
			final int par3, final int par4) {
		final int var5 = CustomColorizer.getColorMultiplier(par1Block,
				blockAccess, par2, par3, par4);
		float var6 = (var5 >> 16 & 255) / 255.0F;
		float var7 = (var5 >> 8 & 255) / 255.0F;
		float var8 = (var5 & 255) / 255.0F;

		if (EntityRenderer.anaglyphEnable) {
			final float var9 = (var6 * 30.0F + var7 * 59.0F + var8 * 11.0F) / 100.0F;
			final float var10 = (var6 * 30.0F + var7 * 70.0F) / 100.0F;
			final float var11 = (var6 * 30.0F + var8 * 70.0F) / 100.0F;
			var6 = var9;
			var7 = var10;
			var8 = var11;
		}

		return Minecraft.isAmbientOcclusionEnabled()
				&& Block.lightValue[par1Block.blockID] == 0 ? partialRenderBounds ? func_102027_b(
				par1Block, par2, par3, par4, var6, var7, var8)
				: renderStandardBlockWithAmbientOcclusion(par1Block, par2,
						par3, par4, var6, var7, var8)
				: renderStandardBlockWithColorMultiplier(par1Block, par2, par3,
						par4, var6, var7, var8);
	}

	/**
	 * Renders a log block at the given coordinates
	 */
	public boolean renderBlockLog(final Block par1Block, final int par2,
			final int par3, final int par4) {
		final int var5 = blockAccess.getBlockMetadata(par2, par3, par4);
		final int var6 = var5 & 12;

		if (var6 == 4) {
			uvRotateEast = 1;
			uvRotateWest = 1;
			uvRotateTop = 1;
			uvRotateBottom = 1;
		} else if (var6 == 8) {
			uvRotateSouth = 1;
			uvRotateNorth = 1;
		}

		final boolean var7 = renderStandardBlock(par1Block, par2, par3, par4);
		uvRotateSouth = 0;
		uvRotateEast = 0;
		uvRotateWest = 0;
		uvRotateNorth = 0;
		uvRotateTop = 0;
		uvRotateBottom = 0;
		return var7;
	}

	public boolean renderBlockQuartz(final Block par1Block, final int par2,
			final int par3, final int par4) {
		final int var5 = blockAccess.getBlockMetadata(par2, par3, par4);

		if (var5 == 3) {
			uvRotateEast = 1;
			uvRotateWest = 1;
			uvRotateTop = 1;
			uvRotateBottom = 1;
		} else if (var5 == 4) {
			uvRotateSouth = 1;
			uvRotateNorth = 1;
		}

		final boolean var6 = renderStandardBlock(par1Block, par2, par3, par4);
		uvRotateSouth = 0;
		uvRotateEast = 0;
		uvRotateWest = 0;
		uvRotateNorth = 0;
		uvRotateTop = 0;
		uvRotateBottom = 0;
		return var6;
	}

	public boolean renderStandardBlockWithAmbientOcclusion(
			final Block par1Block, int par2, int par3, int par4,
			final float par5, final float par6, final float par7) {
		enableAO = true;
		final boolean var8 = Tessellator.instance.defaultTexture;
		final boolean var9 = Config.isBetterGrass() && var8;
		final boolean var10 = par1Block == Block.glass;
		boolean var11 = false;
		float var12 = 0.0F;
		float var13 = 0.0F;
		float var14 = 0.0F;
		float var15 = 0.0F;
		boolean var16 = true;
		int var17 = -1;
		final Tessellator var18 = Tessellator.instance;
		var18.setBrightness(983055);

		if (par1Block == Block.grass) {
			var16 = false;
		} else if (hasOverrideBlockTexture()) {
			var16 = false;
		}

		boolean var19;
		boolean var21;
		boolean var20;
		float var23;
		boolean var22;
		int var24;

		if (renderAllFaces
				|| par1Block.shouldSideBeRendered(blockAccess, par2, par3 - 1,
						par4, 0)) {
			if (renderMinY <= 0.0D) {
				--par3;
			}

			aoBrightnessXYNN = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2 - 1, par3, par4);
			aoBrightnessYZNN = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2, par3, par4 - 1);
			aoBrightnessYZNP = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2, par3, par4 + 1);
			aoBrightnessXYPN = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2 + 1, par3, par4);
			aoLightValueScratchXYNN = getAmbientOcclusionLightValue(
					blockAccess, par2 - 1, par3, par4);
			aoLightValueScratchYZNN = getAmbientOcclusionLightValue(
					blockAccess, par2, par3, par4 - 1);
			aoLightValueScratchYZNP = getAmbientOcclusionLightValue(
					blockAccess, par2, par3, par4 + 1);
			aoLightValueScratchXYPN = getAmbientOcclusionLightValue(
					blockAccess, par2 + 1, par3, par4);
			var20 = Block.canBlockGrass[blockAccess.getBlockId(par2 + 1,
					par3 - 1, par4)];
			var19 = Block.canBlockGrass[blockAccess.getBlockId(par2 - 1,
					par3 - 1, par4)];
			var22 = Block.canBlockGrass[blockAccess.getBlockId(par2, par3 - 1,
					par4 + 1)];
			var21 = Block.canBlockGrass[blockAccess.getBlockId(par2, par3 - 1,
					par4 - 1)];

			if (!var21 && !var19) {
				aoLightValueScratchXYZNNN = aoLightValueScratchXYNN;
				aoBrightnessXYZNNN = aoBrightnessXYNN;
			} else {
				aoLightValueScratchXYZNNN = getAmbientOcclusionLightValue(
						blockAccess, par2 - 1, par3, par4 - 1);
				aoBrightnessXYZNNN = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2 - 1, par3, par4 - 1);
			}

			if (!var22 && !var19) {
				aoLightValueScratchXYZNNP = aoLightValueScratchXYNN;
				aoBrightnessXYZNNP = aoBrightnessXYNN;
			} else {
				aoLightValueScratchXYZNNP = getAmbientOcclusionLightValue(
						blockAccess, par2 - 1, par3, par4 + 1);
				aoBrightnessXYZNNP = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2 - 1, par3, par4 + 1);
			}

			if (!var21 && !var20) {
				aoLightValueScratchXYZPNN = aoLightValueScratchXYPN;
				aoBrightnessXYZPNN = aoBrightnessXYPN;
			} else {
				aoLightValueScratchXYZPNN = getAmbientOcclusionLightValue(
						blockAccess, par2 + 1, par3, par4 - 1);
				aoBrightnessXYZPNN = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2 + 1, par3, par4 - 1);
			}

			if (!var22 && !var20) {
				aoLightValueScratchXYZPNP = aoLightValueScratchXYPN;
				aoBrightnessXYZPNP = aoBrightnessXYPN;
			} else {
				aoLightValueScratchXYZPNP = getAmbientOcclusionLightValue(
						blockAccess, par2 + 1, par3, par4 + 1);
				aoBrightnessXYZPNP = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2 + 1, par3, par4 + 1);
			}

			if (renderMinY <= 0.0D) {
				++par3;
			}

			if (var17 < 0) {
				var17 = par1Block.getMixedBrightnessForBlock(blockAccess, par2,
						par3, par4);
			}

			var24 = var17;

			if (renderMinY <= 0.0D
					|| !blockAccess.isBlockOpaqueCube(par2, par3 - 1, par4)) {
				var24 = par1Block.getMixedBrightnessForBlock(blockAccess, par2,
						par3 - 1, par4);
			}

			var23 = getAmbientOcclusionLightValue(blockAccess, par2, par3 - 1,
					par4);
			var12 = (aoLightValueScratchXYZNNP + aoLightValueScratchXYNN
					+ aoLightValueScratchYZNP + var23) / 4.0F;
			var15 = (aoLightValueScratchYZNP + var23
					+ aoLightValueScratchXYZPNP + aoLightValueScratchXYPN) / 4.0F;
			var14 = (var23 + aoLightValueScratchYZNN + aoLightValueScratchXYPN + aoLightValueScratchXYZPNN) / 4.0F;
			var13 = (aoLightValueScratchXYNN + aoLightValueScratchXYZNNN
					+ var23 + aoLightValueScratchYZNN) / 4.0F;
			brightnessTopLeft = getAoBrightness(aoBrightnessXYZNNP,
					aoBrightnessXYNN, aoBrightnessYZNP, var24);
			brightnessTopRight = getAoBrightness(aoBrightnessYZNP,
					aoBrightnessXYZPNP, aoBrightnessXYPN, var24);
			brightnessBottomRight = getAoBrightness(aoBrightnessYZNN,
					aoBrightnessXYPN, aoBrightnessXYZPNN, var24);
			brightnessBottomLeft = getAoBrightness(aoBrightnessXYNN,
					aoBrightnessXYZNNN, aoBrightnessYZNN, var24);

			if (var10) {
				var13 = var23;
				var14 = var23;
				var15 = var23;
				var12 = var23;
				brightnessTopLeft = brightnessTopRight = brightnessBottomRight = brightnessBottomLeft = var24;
			}

			if (var16) {
				colorRedTopLeft = colorRedBottomLeft = colorRedBottomRight = colorRedTopRight = par5 * 0.5F;
				colorGreenTopLeft = colorGreenBottomLeft = colorGreenBottomRight = colorGreenTopRight = par6 * 0.5F;
				colorBlueTopLeft = colorBlueBottomLeft = colorBlueBottomRight = colorBlueTopRight = par7 * 0.5F;
			} else {
				colorRedTopLeft = colorRedBottomLeft = colorRedBottomRight = colorRedTopRight = 0.5F;
				colorGreenTopLeft = colorGreenBottomLeft = colorGreenBottomRight = colorGreenTopRight = 0.5F;
				colorBlueTopLeft = colorBlueBottomLeft = colorBlueBottomRight = colorBlueTopRight = 0.5F;
			}

			colorRedTopLeft *= var12;
			colorGreenTopLeft *= var12;
			colorBlueTopLeft *= var12;
			colorRedBottomLeft *= var13;
			colorGreenBottomLeft *= var13;
			colorBlueBottomLeft *= var13;
			colorRedBottomRight *= var14;
			colorGreenBottomRight *= var14;
			colorBlueBottomRight *= var14;
			colorRedTopRight *= var15;
			colorGreenTopRight *= var15;
			colorBlueTopRight *= var15;
			renderFaceYNeg(par1Block, par2, par3, par4, this.getBlockIcon(
					par1Block, blockAccess, par2, par3, par4, 0));
			var11 = true;
		}

		if (renderAllFaces
				|| par1Block.shouldSideBeRendered(blockAccess, par2, par3 + 1,
						par4, 1)) {
			if (renderMaxY >= 1.0D) {
				++par3;
			}

			aoBrightnessXYNP = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2 - 1, par3, par4);
			aoBrightnessXYPP = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2 + 1, par3, par4);
			aoBrightnessYZPN = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2, par3, par4 - 1);
			aoBrightnessYZPP = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2, par3, par4 + 1);
			aoLightValueScratchXYNP = getAmbientOcclusionLightValue(
					blockAccess, par2 - 1, par3, par4);
			aoLightValueScratchXYPP = getAmbientOcclusionLightValue(
					blockAccess, par2 + 1, par3, par4);
			aoLightValueScratchYZPN = getAmbientOcclusionLightValue(
					blockAccess, par2, par3, par4 - 1);
			aoLightValueScratchYZPP = getAmbientOcclusionLightValue(
					blockAccess, par2, par3, par4 + 1);
			var20 = Block.canBlockGrass[blockAccess.getBlockId(par2 + 1,
					par3 + 1, par4)];
			var19 = Block.canBlockGrass[blockAccess.getBlockId(par2 - 1,
					par3 + 1, par4)];
			var22 = Block.canBlockGrass[blockAccess.getBlockId(par2, par3 + 1,
					par4 + 1)];
			var21 = Block.canBlockGrass[blockAccess.getBlockId(par2, par3 + 1,
					par4 - 1)];

			if (!var21 && !var19) {
				aoLightValueScratchXYZNPN = aoLightValueScratchXYNP;
				aoBrightnessXYZNPN = aoBrightnessXYNP;
			} else {
				aoLightValueScratchXYZNPN = getAmbientOcclusionLightValue(
						blockAccess, par2 - 1, par3, par4 - 1);
				aoBrightnessXYZNPN = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2 - 1, par3, par4 - 1);
			}

			if (!var21 && !var20) {
				aoLightValueScratchXYZPPN = aoLightValueScratchXYPP;
				aoBrightnessXYZPPN = aoBrightnessXYPP;
			} else {
				aoLightValueScratchXYZPPN = getAmbientOcclusionLightValue(
						blockAccess, par2 + 1, par3, par4 - 1);
				aoBrightnessXYZPPN = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2 + 1, par3, par4 - 1);
			}

			if (!var22 && !var19) {
				aoLightValueScratchXYZNPP = aoLightValueScratchXYNP;
				aoBrightnessXYZNPP = aoBrightnessXYNP;
			} else {
				aoLightValueScratchXYZNPP = getAmbientOcclusionLightValue(
						blockAccess, par2 - 1, par3, par4 + 1);
				aoBrightnessXYZNPP = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2 - 1, par3, par4 + 1);
			}

			if (!var22 && !var20) {
				aoLightValueScratchXYZPPP = aoLightValueScratchXYPP;
				aoBrightnessXYZPPP = aoBrightnessXYPP;
			} else {
				aoLightValueScratchXYZPPP = getAmbientOcclusionLightValue(
						blockAccess, par2 + 1, par3, par4 + 1);
				aoBrightnessXYZPPP = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2 + 1, par3, par4 + 1);
			}

			if (renderMaxY >= 1.0D) {
				--par3;
			}

			if (var17 < 0) {
				var17 = par1Block.getMixedBrightnessForBlock(blockAccess, par2,
						par3, par4);
			}

			var24 = var17;

			if (renderMaxY >= 1.0D
					|| !blockAccess.isBlockOpaqueCube(par2, par3 + 1, par4)) {
				var24 = par1Block.getMixedBrightnessForBlock(blockAccess, par2,
						par3 + 1, par4);
			}

			var23 = getAmbientOcclusionLightValue(blockAccess, par2, par3 + 1,
					par4);
			var15 = (aoLightValueScratchXYZNPP + aoLightValueScratchXYNP
					+ aoLightValueScratchYZPP + var23) / 4.0F;
			var12 = (aoLightValueScratchYZPP + var23
					+ aoLightValueScratchXYZPPP + aoLightValueScratchXYPP) / 4.0F;
			var13 = (var23 + aoLightValueScratchYZPN + aoLightValueScratchXYPP + aoLightValueScratchXYZPPN) / 4.0F;
			var14 = (aoLightValueScratchXYNP + aoLightValueScratchXYZNPN
					+ var23 + aoLightValueScratchYZPN) / 4.0F;
			brightnessTopRight = getAoBrightness(aoBrightnessXYZNPP,
					aoBrightnessXYNP, aoBrightnessYZPP, var24);
			brightnessTopLeft = getAoBrightness(aoBrightnessYZPP,
					aoBrightnessXYZPPP, aoBrightnessXYPP, var24);
			brightnessBottomLeft = getAoBrightness(aoBrightnessYZPN,
					aoBrightnessXYPP, aoBrightnessXYZPPN, var24);
			brightnessBottomRight = getAoBrightness(aoBrightnessXYNP,
					aoBrightnessXYZNPN, aoBrightnessYZPN, var24);

			if (var10) {
				var13 = var23;
				var14 = var23;
				var15 = var23;
				var12 = var23;
				brightnessTopLeft = brightnessTopRight = brightnessBottomRight = brightnessBottomLeft = var24;
			}

			colorRedTopLeft = colorRedBottomLeft = colorRedBottomRight = colorRedTopRight = par5;
			colorGreenTopLeft = colorGreenBottomLeft = colorGreenBottomRight = colorGreenTopRight = par6;
			colorBlueTopLeft = colorBlueBottomLeft = colorBlueBottomRight = colorBlueTopRight = par7;
			colorRedTopLeft *= var12;
			colorGreenTopLeft *= var12;
			colorBlueTopLeft *= var12;
			colorRedBottomLeft *= var13;
			colorGreenBottomLeft *= var13;
			colorBlueBottomLeft *= var13;
			colorRedBottomRight *= var14;
			colorGreenBottomRight *= var14;
			colorBlueBottomRight *= var14;
			colorRedTopRight *= var15;
			colorGreenTopRight *= var15;
			colorBlueTopRight *= var15;
			renderFaceYPos(par1Block, par2, par3, par4, this.getBlockIcon(
					par1Block, blockAccess, par2, par3, par4, 1));
			var11 = true;
		}

		Icon var25;

		if (renderAllFaces
				|| par1Block.shouldSideBeRendered(blockAccess, par2, par3,
						par4 - 1, 2)) {
			if (renderMinZ <= 0.0D) {
				--par4;
			}

			aoLightValueScratchXZNN = getAmbientOcclusionLightValue(
					blockAccess, par2 - 1, par3, par4);
			aoLightValueScratchYZNN = getAmbientOcclusionLightValue(
					blockAccess, par2, par3 - 1, par4);
			aoLightValueScratchYZPN = getAmbientOcclusionLightValue(
					blockAccess, par2, par3 + 1, par4);
			aoLightValueScratchXZPN = getAmbientOcclusionLightValue(
					blockAccess, par2 + 1, par3, par4);
			aoBrightnessXZNN = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2 - 1, par3, par4);
			aoBrightnessYZNN = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2, par3 - 1, par4);
			aoBrightnessYZPN = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2, par3 + 1, par4);
			aoBrightnessXZPN = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2 + 1, par3, par4);
			var20 = Block.canBlockGrass[blockAccess.getBlockId(par2 + 1, par3,
					par4 - 1)];
			var19 = Block.canBlockGrass[blockAccess.getBlockId(par2 - 1, par3,
					par4 - 1)];
			var22 = Block.canBlockGrass[blockAccess.getBlockId(par2, par3 + 1,
					par4 - 1)];
			var21 = Block.canBlockGrass[blockAccess.getBlockId(par2, par3 - 1,
					par4 - 1)];

			if (!var19 && !var21) {
				aoLightValueScratchXYZNNN = aoLightValueScratchXZNN;
				aoBrightnessXYZNNN = aoBrightnessXZNN;
			} else {
				aoLightValueScratchXYZNNN = getAmbientOcclusionLightValue(
						blockAccess, par2 - 1, par3 - 1, par4);
				aoBrightnessXYZNNN = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2 - 1, par3 - 1, par4);
			}

			if (!var19 && !var22) {
				aoLightValueScratchXYZNPN = aoLightValueScratchXZNN;
				aoBrightnessXYZNPN = aoBrightnessXZNN;
			} else {
				aoLightValueScratchXYZNPN = getAmbientOcclusionLightValue(
						blockAccess, par2 - 1, par3 + 1, par4);
				aoBrightnessXYZNPN = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2 - 1, par3 + 1, par4);
			}

			if (!var20 && !var21) {
				aoLightValueScratchXYZPNN = aoLightValueScratchXZPN;
				aoBrightnessXYZPNN = aoBrightnessXZPN;
			} else {
				aoLightValueScratchXYZPNN = getAmbientOcclusionLightValue(
						blockAccess, par2 + 1, par3 - 1, par4);
				aoBrightnessXYZPNN = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2 + 1, par3 - 1, par4);
			}

			if (!var20 && !var22) {
				aoLightValueScratchXYZPPN = aoLightValueScratchXZPN;
				aoBrightnessXYZPPN = aoBrightnessXZPN;
			} else {
				aoLightValueScratchXYZPPN = getAmbientOcclusionLightValue(
						blockAccess, par2 + 1, par3 + 1, par4);
				aoBrightnessXYZPPN = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2 + 1, par3 + 1, par4);
			}

			if (renderMinZ <= 0.0D) {
				++par4;
			}

			if (var17 < 0) {
				var17 = par1Block.getMixedBrightnessForBlock(blockAccess, par2,
						par3, par4);
			}

			var24 = var17;

			if (renderMinZ <= 0.0D
					|| !blockAccess.isBlockOpaqueCube(par2, par3, par4 - 1)) {
				var24 = par1Block.getMixedBrightnessForBlock(blockAccess, par2,
						par3, par4 - 1);
			}

			var23 = getAmbientOcclusionLightValue(blockAccess, par2, par3,
					par4 - 1);
			var12 = (aoLightValueScratchXZNN + aoLightValueScratchXYZNPN
					+ var23 + aoLightValueScratchYZPN) / 4.0F;
			var13 = (var23 + aoLightValueScratchYZPN + aoLightValueScratchXZPN + aoLightValueScratchXYZPPN) / 4.0F;
			var14 = (aoLightValueScratchYZNN + var23
					+ aoLightValueScratchXYZPNN + aoLightValueScratchXZPN) / 4.0F;
			var15 = (aoLightValueScratchXYZNNN + aoLightValueScratchXZNN
					+ aoLightValueScratchYZNN + var23) / 4.0F;
			brightnessTopLeft = getAoBrightness(aoBrightnessXZNN,
					aoBrightnessXYZNPN, aoBrightnessYZPN, var24);
			brightnessBottomLeft = getAoBrightness(aoBrightnessYZPN,
					aoBrightnessXZPN, aoBrightnessXYZPPN, var24);
			brightnessBottomRight = getAoBrightness(aoBrightnessYZNN,
					aoBrightnessXYZPNN, aoBrightnessXZPN, var24);
			brightnessTopRight = getAoBrightness(aoBrightnessXYZNNN,
					aoBrightnessXZNN, aoBrightnessYZNN, var24);

			if (var10) {
				var13 = var23;
				var14 = var23;
				var15 = var23;
				var12 = var23;
				brightnessTopLeft = brightnessTopRight = brightnessBottomRight = brightnessBottomLeft = var24;
			}

			if (var16) {
				colorRedTopLeft = colorRedBottomLeft = colorRedBottomRight = colorRedTopRight = par5 * 0.8F;
				colorGreenTopLeft = colorGreenBottomLeft = colorGreenBottomRight = colorGreenTopRight = par6 * 0.8F;
				colorBlueTopLeft = colorBlueBottomLeft = colorBlueBottomRight = colorBlueTopRight = par7 * 0.8F;
			} else {
				colorRedTopLeft = colorRedBottomLeft = colorRedBottomRight = colorRedTopRight = 0.8F;
				colorGreenTopLeft = colorGreenBottomLeft = colorGreenBottomRight = colorGreenTopRight = 0.8F;
				colorBlueTopLeft = colorBlueBottomLeft = colorBlueBottomRight = colorBlueTopRight = 0.8F;
			}

			colorRedTopLeft *= var12;
			colorGreenTopLeft *= var12;
			colorBlueTopLeft *= var12;
			colorRedBottomLeft *= var13;
			colorGreenBottomLeft *= var13;
			colorBlueBottomLeft *= var13;
			colorRedBottomRight *= var14;
			colorGreenBottomRight *= var14;
			colorBlueBottomRight *= var14;
			colorRedTopRight *= var15;
			colorGreenTopRight *= var15;
			colorBlueTopRight *= var15;
			var25 = this.getBlockIcon(par1Block, blockAccess, par2, par3, par4,
					2);

			if (var9) {
				var25 = fixAoSideGrassTexture(var25, par2, par3, par4, 2, par5,
						par6, par7);
			}

			renderFaceZNeg(par1Block, par2, par3, par4, var25);

			if (var8 && RenderBlocks.fancyGrass
					&& var25 == TextureUtils.iconGrassSide
					&& !hasOverrideBlockTexture()) {
				colorRedTopLeft *= par5;
				colorRedBottomLeft *= par5;
				colorRedBottomRight *= par5;
				colorRedTopRight *= par5;
				colorGreenTopLeft *= par6;
				colorGreenBottomLeft *= par6;
				colorGreenBottomRight *= par6;
				colorGreenTopRight *= par6;
				colorBlueTopLeft *= par7;
				colorBlueBottomLeft *= par7;
				colorBlueBottomRight *= par7;
				colorBlueTopRight *= par7;
				renderFaceZNeg(par1Block, par2, par3, par4,
						BlockGrass.getIconSideOverlay());
			}

			var11 = true;
		}

		if (renderAllFaces
				|| par1Block.shouldSideBeRendered(blockAccess, par2, par3,
						par4 + 1, 3)) {
			if (renderMaxZ >= 1.0D) {
				++par4;
			}

			aoLightValueScratchXZNP = getAmbientOcclusionLightValue(
					blockAccess, par2 - 1, par3, par4);
			aoLightValueScratchXZPP = getAmbientOcclusionLightValue(
					blockAccess, par2 + 1, par3, par4);
			aoLightValueScratchYZNP = getAmbientOcclusionLightValue(
					blockAccess, par2, par3 - 1, par4);
			aoLightValueScratchYZPP = getAmbientOcclusionLightValue(
					blockAccess, par2, par3 + 1, par4);
			aoBrightnessXZNP = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2 - 1, par3, par4);
			aoBrightnessXZPP = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2 + 1, par3, par4);
			aoBrightnessYZNP = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2, par3 - 1, par4);
			aoBrightnessYZPP = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2, par3 + 1, par4);
			var20 = Block.canBlockGrass[blockAccess.getBlockId(par2 + 1, par3,
					par4 + 1)];
			var19 = Block.canBlockGrass[blockAccess.getBlockId(par2 - 1, par3,
					par4 + 1)];
			var22 = Block.canBlockGrass[blockAccess.getBlockId(par2, par3 + 1,
					par4 + 1)];
			var21 = Block.canBlockGrass[blockAccess.getBlockId(par2, par3 - 1,
					par4 + 1)];

			if (!var19 && !var21) {
				aoLightValueScratchXYZNNP = aoLightValueScratchXZNP;
				aoBrightnessXYZNNP = aoBrightnessXZNP;
			} else {
				aoLightValueScratchXYZNNP = getAmbientOcclusionLightValue(
						blockAccess, par2 - 1, par3 - 1, par4);
				aoBrightnessXYZNNP = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2 - 1, par3 - 1, par4);
			}

			if (!var19 && !var22) {
				aoLightValueScratchXYZNPP = aoLightValueScratchXZNP;
				aoBrightnessXYZNPP = aoBrightnessXZNP;
			} else {
				aoLightValueScratchXYZNPP = getAmbientOcclusionLightValue(
						blockAccess, par2 - 1, par3 + 1, par4);
				aoBrightnessXYZNPP = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2 - 1, par3 + 1, par4);
			}

			if (!var20 && !var21) {
				aoLightValueScratchXYZPNP = aoLightValueScratchXZPP;
				aoBrightnessXYZPNP = aoBrightnessXZPP;
			} else {
				aoLightValueScratchXYZPNP = getAmbientOcclusionLightValue(
						blockAccess, par2 + 1, par3 - 1, par4);
				aoBrightnessXYZPNP = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2 + 1, par3 - 1, par4);
			}

			if (!var20 && !var22) {
				aoLightValueScratchXYZPPP = aoLightValueScratchXZPP;
				aoBrightnessXYZPPP = aoBrightnessXZPP;
			} else {
				aoLightValueScratchXYZPPP = getAmbientOcclusionLightValue(
						blockAccess, par2 + 1, par3 + 1, par4);
				aoBrightnessXYZPPP = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2 + 1, par3 + 1, par4);
			}

			if (renderMaxZ >= 1.0D) {
				--par4;
			}

			if (var17 < 0) {
				var17 = par1Block.getMixedBrightnessForBlock(blockAccess, par2,
						par3, par4);
			}

			var24 = var17;

			if (renderMaxZ >= 1.0D
					|| !blockAccess.isBlockOpaqueCube(par2, par3, par4 + 1)) {
				var24 = par1Block.getMixedBrightnessForBlock(blockAccess, par2,
						par3, par4 + 1);
			}

			var23 = getAmbientOcclusionLightValue(blockAccess, par2, par3,
					par4 + 1);
			var12 = (aoLightValueScratchXZNP + aoLightValueScratchXYZNPP
					+ var23 + aoLightValueScratchYZPP) / 4.0F;
			var15 = (var23 + aoLightValueScratchYZPP + aoLightValueScratchXZPP + aoLightValueScratchXYZPPP) / 4.0F;
			var14 = (aoLightValueScratchYZNP + var23
					+ aoLightValueScratchXYZPNP + aoLightValueScratchXZPP) / 4.0F;
			var13 = (aoLightValueScratchXYZNNP + aoLightValueScratchXZNP
					+ aoLightValueScratchYZNP + var23) / 4.0F;
			brightnessTopLeft = getAoBrightness(aoBrightnessXZNP,
					aoBrightnessXYZNPP, aoBrightnessYZPP, var24);
			brightnessTopRight = getAoBrightness(aoBrightnessYZPP,
					aoBrightnessXZPP, aoBrightnessXYZPPP, var24);
			brightnessBottomRight = getAoBrightness(aoBrightnessYZNP,
					aoBrightnessXYZPNP, aoBrightnessXZPP, var24);
			brightnessBottomLeft = getAoBrightness(aoBrightnessXYZNNP,
					aoBrightnessXZNP, aoBrightnessYZNP, var24);

			if (var10) {
				var13 = var23;
				var14 = var23;
				var15 = var23;
				var12 = var23;
				brightnessTopLeft = brightnessTopRight = brightnessBottomRight = brightnessBottomLeft = var24;
			}

			if (var16) {
				colorRedTopLeft = colorRedBottomLeft = colorRedBottomRight = colorRedTopRight = par5 * 0.8F;
				colorGreenTopLeft = colorGreenBottomLeft = colorGreenBottomRight = colorGreenTopRight = par6 * 0.8F;
				colorBlueTopLeft = colorBlueBottomLeft = colorBlueBottomRight = colorBlueTopRight = par7 * 0.8F;
			} else {
				colorRedTopLeft = colorRedBottomLeft = colorRedBottomRight = colorRedTopRight = 0.8F;
				colorGreenTopLeft = colorGreenBottomLeft = colorGreenBottomRight = colorGreenTopRight = 0.8F;
				colorBlueTopLeft = colorBlueBottomLeft = colorBlueBottomRight = colorBlueTopRight = 0.8F;
			}

			colorRedTopLeft *= var12;
			colorGreenTopLeft *= var12;
			colorBlueTopLeft *= var12;
			colorRedBottomLeft *= var13;
			colorGreenBottomLeft *= var13;
			colorBlueBottomLeft *= var13;
			colorRedBottomRight *= var14;
			colorGreenBottomRight *= var14;
			colorBlueBottomRight *= var14;
			colorRedTopRight *= var15;
			colorGreenTopRight *= var15;
			colorBlueTopRight *= var15;
			var25 = this.getBlockIcon(par1Block, blockAccess, par2, par3, par4,
					3);

			if (var9) {
				var25 = fixAoSideGrassTexture(var25, par2, par3, par4, 3, par5,
						par6, par7);
			}

			renderFaceZPos(par1Block, par2, par3, par4, var25);

			if (var8 && RenderBlocks.fancyGrass
					&& var25 == TextureUtils.iconGrassSide
					&& !hasOverrideBlockTexture()) {
				colorRedTopLeft *= par5;
				colorRedBottomLeft *= par5;
				colorRedBottomRight *= par5;
				colorRedTopRight *= par5;
				colorGreenTopLeft *= par6;
				colorGreenBottomLeft *= par6;
				colorGreenBottomRight *= par6;
				colorGreenTopRight *= par6;
				colorBlueTopLeft *= par7;
				colorBlueBottomLeft *= par7;
				colorBlueBottomRight *= par7;
				colorBlueTopRight *= par7;
				renderFaceZPos(par1Block, par2, par3, par4,
						BlockGrass.getIconSideOverlay());
			}

			var11 = true;
		}

		if (renderAllFaces
				|| par1Block.shouldSideBeRendered(blockAccess, par2 - 1, par3,
						par4, 4)) {
			if (renderMinX <= 0.0D) {
				--par2;
			}

			aoLightValueScratchXYNN = getAmbientOcclusionLightValue(
					blockAccess, par2, par3 - 1, par4);
			aoLightValueScratchXZNN = getAmbientOcclusionLightValue(
					blockAccess, par2, par3, par4 - 1);
			aoLightValueScratchXZNP = getAmbientOcclusionLightValue(
					blockAccess, par2, par3, par4 + 1);
			aoLightValueScratchXYNP = getAmbientOcclusionLightValue(
					blockAccess, par2, par3 + 1, par4);
			aoBrightnessXYNN = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2, par3 - 1, par4);
			aoBrightnessXZNN = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2, par3, par4 - 1);
			aoBrightnessXZNP = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2, par3, par4 + 1);
			aoBrightnessXYNP = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2, par3 + 1, par4);
			var20 = Block.canBlockGrass[blockAccess.getBlockId(par2 - 1,
					par3 + 1, par4)];
			var19 = Block.canBlockGrass[blockAccess.getBlockId(par2 - 1,
					par3 - 1, par4)];
			var22 = Block.canBlockGrass[blockAccess.getBlockId(par2 - 1, par3,
					par4 - 1)];
			var21 = Block.canBlockGrass[blockAccess.getBlockId(par2 - 1, par3,
					par4 + 1)];

			if (!var22 && !var19) {
				aoLightValueScratchXYZNNN = aoLightValueScratchXZNN;
				aoBrightnessXYZNNN = aoBrightnessXZNN;
			} else {
				aoLightValueScratchXYZNNN = getAmbientOcclusionLightValue(
						blockAccess, par2, par3 - 1, par4 - 1);
				aoBrightnessXYZNNN = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2, par3 - 1, par4 - 1);
			}

			if (!var21 && !var19) {
				aoLightValueScratchXYZNNP = aoLightValueScratchXZNP;
				aoBrightnessXYZNNP = aoBrightnessXZNP;
			} else {
				aoLightValueScratchXYZNNP = getAmbientOcclusionLightValue(
						blockAccess, par2, par3 - 1, par4 + 1);
				aoBrightnessXYZNNP = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2, par3 - 1, par4 + 1);
			}

			if (!var22 && !var20) {
				aoLightValueScratchXYZNPN = aoLightValueScratchXZNN;
				aoBrightnessXYZNPN = aoBrightnessXZNN;
			} else {
				aoLightValueScratchXYZNPN = getAmbientOcclusionLightValue(
						blockAccess, par2, par3 + 1, par4 - 1);
				aoBrightnessXYZNPN = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2, par3 + 1, par4 - 1);
			}

			if (!var21 && !var20) {
				aoLightValueScratchXYZNPP = aoLightValueScratchXZNP;
				aoBrightnessXYZNPP = aoBrightnessXZNP;
			} else {
				aoLightValueScratchXYZNPP = getAmbientOcclusionLightValue(
						blockAccess, par2, par3 + 1, par4 + 1);
				aoBrightnessXYZNPP = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2, par3 + 1, par4 + 1);
			}

			if (renderMinX <= 0.0D) {
				++par2;
			}

			if (var17 < 0) {
				var17 = par1Block.getMixedBrightnessForBlock(blockAccess, par2,
						par3, par4);
			}

			var24 = var17;

			if (renderMinX <= 0.0D
					|| !blockAccess.isBlockOpaqueCube(par2 - 1, par3, par4)) {
				var24 = par1Block.getMixedBrightnessForBlock(blockAccess,
						par2 - 1, par3, par4);
			}

			var23 = getAmbientOcclusionLightValue(blockAccess, par2 - 1, par3,
					par4);
			var15 = (aoLightValueScratchXYNN + aoLightValueScratchXYZNNP
					+ var23 + aoLightValueScratchXZNP) / 4.0F;
			var12 = (var23 + aoLightValueScratchXZNP + aoLightValueScratchXYNP + aoLightValueScratchXYZNPP) / 4.0F;
			var13 = (aoLightValueScratchXZNN + var23
					+ aoLightValueScratchXYZNPN + aoLightValueScratchXYNP) / 4.0F;
			var14 = (aoLightValueScratchXYZNNN + aoLightValueScratchXYNN
					+ aoLightValueScratchXZNN + var23) / 4.0F;
			brightnessTopRight = getAoBrightness(aoBrightnessXYNN,
					aoBrightnessXYZNNP, aoBrightnessXZNP, var24);
			brightnessTopLeft = getAoBrightness(aoBrightnessXZNP,
					aoBrightnessXYNP, aoBrightnessXYZNPP, var24);
			brightnessBottomLeft = getAoBrightness(aoBrightnessXZNN,
					aoBrightnessXYZNPN, aoBrightnessXYNP, var24);
			brightnessBottomRight = getAoBrightness(aoBrightnessXYZNNN,
					aoBrightnessXYNN, aoBrightnessXZNN, var24);

			if (var10) {
				var13 = var23;
				var14 = var23;
				var15 = var23;
				var12 = var23;
				brightnessTopLeft = brightnessTopRight = brightnessBottomRight = brightnessBottomLeft = var24;
			}

			if (var16) {
				colorRedTopLeft = colorRedBottomLeft = colorRedBottomRight = colorRedTopRight = par5 * 0.6F;
				colorGreenTopLeft = colorGreenBottomLeft = colorGreenBottomRight = colorGreenTopRight = par6 * 0.6F;
				colorBlueTopLeft = colorBlueBottomLeft = colorBlueBottomRight = colorBlueTopRight = par7 * 0.6F;
			} else {
				colorRedTopLeft = colorRedBottomLeft = colorRedBottomRight = colorRedTopRight = 0.6F;
				colorGreenTopLeft = colorGreenBottomLeft = colorGreenBottomRight = colorGreenTopRight = 0.6F;
				colorBlueTopLeft = colorBlueBottomLeft = colorBlueBottomRight = colorBlueTopRight = 0.6F;
			}

			colorRedTopLeft *= var12;
			colorGreenTopLeft *= var12;
			colorBlueTopLeft *= var12;
			colorRedBottomLeft *= var13;
			colorGreenBottomLeft *= var13;
			colorBlueBottomLeft *= var13;
			colorRedBottomRight *= var14;
			colorGreenBottomRight *= var14;
			colorBlueBottomRight *= var14;
			colorRedTopRight *= var15;
			colorGreenTopRight *= var15;
			colorBlueTopRight *= var15;
			var25 = this.getBlockIcon(par1Block, blockAccess, par2, par3, par4,
					4);

			if (var9) {
				var25 = fixAoSideGrassTexture(var25, par2, par3, par4, 4, par5,
						par6, par7);
			}

			renderFaceXNeg(par1Block, par2, par3, par4, var25);

			if (var8 && RenderBlocks.fancyGrass
					&& var25 == TextureUtils.iconGrassSide
					&& !hasOverrideBlockTexture()) {
				colorRedTopLeft *= par5;
				colorRedBottomLeft *= par5;
				colorRedBottomRight *= par5;
				colorRedTopRight *= par5;
				colorGreenTopLeft *= par6;
				colorGreenBottomLeft *= par6;
				colorGreenBottomRight *= par6;
				colorGreenTopRight *= par6;
				colorBlueTopLeft *= par7;
				colorBlueBottomLeft *= par7;
				colorBlueBottomRight *= par7;
				colorBlueTopRight *= par7;
				renderFaceXNeg(par1Block, par2, par3, par4,
						BlockGrass.getIconSideOverlay());
			}

			var11 = true;
		}

		if (renderAllFaces
				|| par1Block.shouldSideBeRendered(blockAccess, par2 + 1, par3,
						par4, 5)) {
			if (renderMaxX >= 1.0D) {
				++par2;
			}

			aoLightValueScratchXYPN = getAmbientOcclusionLightValue(
					blockAccess, par2, par3 - 1, par4);
			aoLightValueScratchXZPN = getAmbientOcclusionLightValue(
					blockAccess, par2, par3, par4 - 1);
			aoLightValueScratchXZPP = getAmbientOcclusionLightValue(
					blockAccess, par2, par3, par4 + 1);
			aoLightValueScratchXYPP = getAmbientOcclusionLightValue(
					blockAccess, par2, par3 + 1, par4);
			aoBrightnessXYPN = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2, par3 - 1, par4);
			aoBrightnessXZPN = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2, par3, par4 - 1);
			aoBrightnessXZPP = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2, par3, par4 + 1);
			aoBrightnessXYPP = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2, par3 + 1, par4);
			var20 = Block.canBlockGrass[blockAccess.getBlockId(par2 + 1,
					par3 + 1, par4)];
			var19 = Block.canBlockGrass[blockAccess.getBlockId(par2 + 1,
					par3 - 1, par4)];
			var22 = Block.canBlockGrass[blockAccess.getBlockId(par2 + 1, par3,
					par4 + 1)];
			var21 = Block.canBlockGrass[blockAccess.getBlockId(par2 + 1, par3,
					par4 - 1)];

			if (!var19 && !var21) {
				aoLightValueScratchXYZPNN = aoLightValueScratchXZPN;
				aoBrightnessXYZPNN = aoBrightnessXZPN;
			} else {
				aoLightValueScratchXYZPNN = getAmbientOcclusionLightValue(
						blockAccess, par2, par3 - 1, par4 - 1);
				aoBrightnessXYZPNN = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2, par3 - 1, par4 - 1);
			}

			if (!var19 && !var22) {
				aoLightValueScratchXYZPNP = aoLightValueScratchXZPP;
				aoBrightnessXYZPNP = aoBrightnessXZPP;
			} else {
				aoLightValueScratchXYZPNP = getAmbientOcclusionLightValue(
						blockAccess, par2, par3 - 1, par4 + 1);
				aoBrightnessXYZPNP = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2, par3 - 1, par4 + 1);
			}

			if (!var20 && !var21) {
				aoLightValueScratchXYZPPN = aoLightValueScratchXZPN;
				aoBrightnessXYZPPN = aoBrightnessXZPN;
			} else {
				aoLightValueScratchXYZPPN = getAmbientOcclusionLightValue(
						blockAccess, par2, par3 + 1, par4 - 1);
				aoBrightnessXYZPPN = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2, par3 + 1, par4 - 1);
			}

			if (!var20 && !var22) {
				aoLightValueScratchXYZPPP = aoLightValueScratchXZPP;
				aoBrightnessXYZPPP = aoBrightnessXZPP;
			} else {
				aoLightValueScratchXYZPPP = getAmbientOcclusionLightValue(
						blockAccess, par2, par3 + 1, par4 + 1);
				aoBrightnessXYZPPP = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2, par3 + 1, par4 + 1);
			}

			if (renderMaxX >= 1.0D) {
				--par2;
			}

			if (var17 < 0) {
				var17 = par1Block.getMixedBrightnessForBlock(blockAccess, par2,
						par3, par4);
			}

			var24 = var17;

			if (renderMaxX >= 1.0D
					|| !blockAccess.isBlockOpaqueCube(par2 + 1, par3, par4)) {
				var24 = par1Block.getMixedBrightnessForBlock(blockAccess,
						par2 + 1, par3, par4);
			}

			var23 = getAmbientOcclusionLightValue(blockAccess, par2 + 1, par3,
					par4);
			var12 = (aoLightValueScratchXYPN + aoLightValueScratchXYZPNP
					+ var23 + aoLightValueScratchXZPP) / 4.0F;
			var13 = (aoLightValueScratchXYZPNN + aoLightValueScratchXYPN
					+ aoLightValueScratchXZPN + var23) / 4.0F;
			var14 = (aoLightValueScratchXZPN + var23
					+ aoLightValueScratchXYZPPN + aoLightValueScratchXYPP) / 4.0F;
			var15 = (var23 + aoLightValueScratchXZPP + aoLightValueScratchXYPP + aoLightValueScratchXYZPPP) / 4.0F;
			brightnessTopLeft = getAoBrightness(aoBrightnessXYPN,
					aoBrightnessXYZPNP, aoBrightnessXZPP, var24);
			brightnessTopRight = getAoBrightness(aoBrightnessXZPP,
					aoBrightnessXYPP, aoBrightnessXYZPPP, var24);
			brightnessBottomRight = getAoBrightness(aoBrightnessXZPN,
					aoBrightnessXYZPPN, aoBrightnessXYPP, var24);
			brightnessBottomLeft = getAoBrightness(aoBrightnessXYZPNN,
					aoBrightnessXYPN, aoBrightnessXZPN, var24);

			if (var10) {
				var13 = var23;
				var14 = var23;
				var15 = var23;
				var12 = var23;
				brightnessTopLeft = brightnessTopRight = brightnessBottomRight = brightnessBottomLeft = var24;
			}

			if (var16) {
				colorRedTopLeft = colorRedBottomLeft = colorRedBottomRight = colorRedTopRight = par5 * 0.6F;
				colorGreenTopLeft = colorGreenBottomLeft = colorGreenBottomRight = colorGreenTopRight = par6 * 0.6F;
				colorBlueTopLeft = colorBlueBottomLeft = colorBlueBottomRight = colorBlueTopRight = par7 * 0.6F;
			} else {
				colorRedTopLeft = colorRedBottomLeft = colorRedBottomRight = colorRedTopRight = 0.6F;
				colorGreenTopLeft = colorGreenBottomLeft = colorGreenBottomRight = colorGreenTopRight = 0.6F;
				colorBlueTopLeft = colorBlueBottomLeft = colorBlueBottomRight = colorBlueTopRight = 0.6F;
			}

			colorRedTopLeft *= var12;
			colorGreenTopLeft *= var12;
			colorBlueTopLeft *= var12;
			colorRedBottomLeft *= var13;
			colorGreenBottomLeft *= var13;
			colorBlueBottomLeft *= var13;
			colorRedBottomRight *= var14;
			colorGreenBottomRight *= var14;
			colorBlueBottomRight *= var14;
			colorRedTopRight *= var15;
			colorGreenTopRight *= var15;
			colorBlueTopRight *= var15;
			var25 = this.getBlockIcon(par1Block, blockAccess, par2, par3, par4,
					5);

			if (var9) {
				var25 = fixAoSideGrassTexture(var25, par2, par3, par4, 5, par5,
						par6, par7);
			}

			renderFaceXPos(par1Block, par2, par3, par4, var25);

			if (var8 && RenderBlocks.fancyGrass
					&& var25 == TextureUtils.iconGrassSide
					&& !hasOverrideBlockTexture()) {
				colorRedTopLeft *= par5;
				colorRedBottomLeft *= par5;
				colorRedBottomRight *= par5;
				colorRedTopRight *= par5;
				colorGreenTopLeft *= par6;
				colorGreenBottomLeft *= par6;
				colorGreenBottomRight *= par6;
				colorGreenTopRight *= par6;
				colorBlueTopLeft *= par7;
				colorBlueBottomLeft *= par7;
				colorBlueBottomRight *= par7;
				colorBlueTopRight *= par7;
				renderFaceXPos(par1Block, par2, par3, par4,
						BlockGrass.getIconSideOverlay());
			}

			var11 = true;
		}

		enableAO = false;
		return var11;
	}

	public boolean func_102027_b(final Block par1Block, int par2, int par3,
			int par4, final float par5, final float par6, final float par7) {
		enableAO = true;
		boolean var8 = false;
		float var9 = 0.0F;
		float var10 = 0.0F;
		float var11 = 0.0F;
		float var12 = 0.0F;
		boolean var13 = true;
		final int var14 = par1Block.getMixedBrightnessForBlock(blockAccess,
				par2, par3, par4);
		final Tessellator var15 = Tessellator.instance;
		var15.setBrightness(983055);

		if (par1Block == Block.grass) {
			var13 = false;
		} else if (hasOverrideBlockTexture()) {
			var13 = false;
		}

		boolean var17;
		boolean var16;
		boolean var19;
		boolean var18;
		int var21;
		float var20;

		if (renderAllFaces
				|| par1Block.shouldSideBeRendered(blockAccess, par2, par3 - 1,
						par4, 0)) {
			if (renderMinY <= 0.0D) {
				--par3;
			}

			aoBrightnessXYNN = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2 - 1, par3, par4);
			aoBrightnessYZNN = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2, par3, par4 - 1);
			aoBrightnessYZNP = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2, par3, par4 + 1);
			aoBrightnessXYPN = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2 + 1, par3, par4);
			aoLightValueScratchXYNN = getAmbientOcclusionLightValue(
					blockAccess, par2 - 1, par3, par4);
			aoLightValueScratchYZNN = getAmbientOcclusionLightValue(
					blockAccess, par2, par3, par4 - 1);
			aoLightValueScratchYZNP = getAmbientOcclusionLightValue(
					blockAccess, par2, par3, par4 + 1);
			aoLightValueScratchXYPN = getAmbientOcclusionLightValue(
					blockAccess, par2 + 1, par3, par4);
			var17 = Block.canBlockGrass[blockAccess.getBlockId(par2 + 1,
					par3 - 1, par4)];
			var16 = Block.canBlockGrass[blockAccess.getBlockId(par2 - 1,
					par3 - 1, par4)];
			var19 = Block.canBlockGrass[blockAccess.getBlockId(par2, par3 - 1,
					par4 + 1)];
			var18 = Block.canBlockGrass[blockAccess.getBlockId(par2, par3 - 1,
					par4 - 1)];

			if (!var18 && !var16) {
				aoLightValueScratchXYZNNN = aoLightValueScratchXYNN;
				aoBrightnessXYZNNN = aoBrightnessXYNN;
			} else {
				aoLightValueScratchXYZNNN = getAmbientOcclusionLightValue(
						blockAccess, par2 - 1, par3, par4 - 1);
				aoBrightnessXYZNNN = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2 - 1, par3, par4 - 1);
			}

			if (!var19 && !var16) {
				aoLightValueScratchXYZNNP = aoLightValueScratchXYNN;
				aoBrightnessXYZNNP = aoBrightnessXYNN;
			} else {
				aoLightValueScratchXYZNNP = getAmbientOcclusionLightValue(
						blockAccess, par2 - 1, par3, par4 + 1);
				aoBrightnessXYZNNP = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2 - 1, par3, par4 + 1);
			}

			if (!var18 && !var17) {
				aoLightValueScratchXYZPNN = aoLightValueScratchXYPN;
				aoBrightnessXYZPNN = aoBrightnessXYPN;
			} else {
				aoLightValueScratchXYZPNN = getAmbientOcclusionLightValue(
						blockAccess, par2 + 1, par3, par4 - 1);
				aoBrightnessXYZPNN = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2 + 1, par3, par4 - 1);
			}

			if (!var19 && !var17) {
				aoLightValueScratchXYZPNP = aoLightValueScratchXYPN;
				aoBrightnessXYZPNP = aoBrightnessXYPN;
			} else {
				aoLightValueScratchXYZPNP = getAmbientOcclusionLightValue(
						blockAccess, par2 + 1, par3, par4 + 1);
				aoBrightnessXYZPNP = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2 + 1, par3, par4 + 1);
			}

			if (renderMinY <= 0.0D) {
				++par3;
			}

			var21 = var14;

			if (renderMinY <= 0.0D
					|| !blockAccess.isBlockOpaqueCube(par2, par3 - 1, par4)) {
				var21 = par1Block.getMixedBrightnessForBlock(blockAccess, par2,
						par3 - 1, par4);
			}

			var20 = getAmbientOcclusionLightValue(blockAccess, par2, par3 - 1,
					par4);
			var9 = (aoLightValueScratchXYZNNP + aoLightValueScratchXYNN
					+ aoLightValueScratchYZNP + var20) / 4.0F;
			var12 = (aoLightValueScratchYZNP + var20
					+ aoLightValueScratchXYZPNP + aoLightValueScratchXYPN) / 4.0F;
			var11 = (var20 + aoLightValueScratchYZNN + aoLightValueScratchXYPN + aoLightValueScratchXYZPNN) / 4.0F;
			var10 = (aoLightValueScratchXYNN + aoLightValueScratchXYZNNN
					+ var20 + aoLightValueScratchYZNN) / 4.0F;
			brightnessTopLeft = getAoBrightness(aoBrightnessXYZNNP,
					aoBrightnessXYNN, aoBrightnessYZNP, var21);
			brightnessTopRight = getAoBrightness(aoBrightnessYZNP,
					aoBrightnessXYZPNP, aoBrightnessXYPN, var21);
			brightnessBottomRight = getAoBrightness(aoBrightnessYZNN,
					aoBrightnessXYPN, aoBrightnessXYZPNN, var21);
			brightnessBottomLeft = getAoBrightness(aoBrightnessXYNN,
					aoBrightnessXYZNNN, aoBrightnessYZNN, var21);

			if (var13) {
				colorRedTopLeft = colorRedBottomLeft = colorRedBottomRight = colorRedTopRight = par5 * 0.5F;
				colorGreenTopLeft = colorGreenBottomLeft = colorGreenBottomRight = colorGreenTopRight = par6 * 0.5F;
				colorBlueTopLeft = colorBlueBottomLeft = colorBlueBottomRight = colorBlueTopRight = par7 * 0.5F;
			} else {
				colorRedTopLeft = colorRedBottomLeft = colorRedBottomRight = colorRedTopRight = 0.5F;
				colorGreenTopLeft = colorGreenBottomLeft = colorGreenBottomRight = colorGreenTopRight = 0.5F;
				colorBlueTopLeft = colorBlueBottomLeft = colorBlueBottomRight = colorBlueTopRight = 0.5F;
			}

			colorRedTopLeft *= var9;
			colorGreenTopLeft *= var9;
			colorBlueTopLeft *= var9;
			colorRedBottomLeft *= var10;
			colorGreenBottomLeft *= var10;
			colorBlueBottomLeft *= var10;
			colorRedBottomRight *= var11;
			colorGreenBottomRight *= var11;
			colorBlueBottomRight *= var11;
			colorRedTopRight *= var12;
			colorGreenTopRight *= var12;
			colorBlueTopRight *= var12;
			renderFaceYNeg(par1Block, par2, par3, par4, this.getBlockIcon(
					par1Block, blockAccess, par2, par3, par4, 0));
			var8 = true;
		}

		if (renderAllFaces
				|| par1Block.shouldSideBeRendered(blockAccess, par2, par3 + 1,
						par4, 1)) {
			if (renderMaxY >= 1.0D) {
				++par3;
			}

			aoBrightnessXYNP = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2 - 1, par3, par4);
			aoBrightnessXYPP = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2 + 1, par3, par4);
			aoBrightnessYZPN = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2, par3, par4 - 1);
			aoBrightnessYZPP = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2, par3, par4 + 1);
			aoLightValueScratchXYNP = getAmbientOcclusionLightValue(
					blockAccess, par2 - 1, par3, par4);
			aoLightValueScratchXYPP = getAmbientOcclusionLightValue(
					blockAccess, par2 + 1, par3, par4);
			aoLightValueScratchYZPN = getAmbientOcclusionLightValue(
					blockAccess, par2, par3, par4 - 1);
			aoLightValueScratchYZPP = getAmbientOcclusionLightValue(
					blockAccess, par2, par3, par4 + 1);
			var17 = Block.canBlockGrass[blockAccess.getBlockId(par2 + 1,
					par3 + 1, par4)];
			var16 = Block.canBlockGrass[blockAccess.getBlockId(par2 - 1,
					par3 + 1, par4)];
			var19 = Block.canBlockGrass[blockAccess.getBlockId(par2, par3 + 1,
					par4 + 1)];
			var18 = Block.canBlockGrass[blockAccess.getBlockId(par2, par3 + 1,
					par4 - 1)];

			if (!var18 && !var16) {
				aoLightValueScratchXYZNPN = aoLightValueScratchXYNP;
				aoBrightnessXYZNPN = aoBrightnessXYNP;
			} else {
				aoLightValueScratchXYZNPN = getAmbientOcclusionLightValue(
						blockAccess, par2 - 1, par3, par4 - 1);
				aoBrightnessXYZNPN = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2 - 1, par3, par4 - 1);
			}

			if (!var18 && !var17) {
				aoLightValueScratchXYZPPN = aoLightValueScratchXYPP;
				aoBrightnessXYZPPN = aoBrightnessXYPP;
			} else {
				aoLightValueScratchXYZPPN = getAmbientOcclusionLightValue(
						blockAccess, par2 + 1, par3, par4 - 1);
				aoBrightnessXYZPPN = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2 + 1, par3, par4 - 1);
			}

			if (!var19 && !var16) {
				aoLightValueScratchXYZNPP = aoLightValueScratchXYNP;
				aoBrightnessXYZNPP = aoBrightnessXYNP;
			} else {
				aoLightValueScratchXYZNPP = getAmbientOcclusionLightValue(
						blockAccess, par2 - 1, par3, par4 + 1);
				aoBrightnessXYZNPP = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2 - 1, par3, par4 + 1);
			}

			if (!var19 && !var17) {
				aoLightValueScratchXYZPPP = aoLightValueScratchXYPP;
				aoBrightnessXYZPPP = aoBrightnessXYPP;
			} else {
				aoLightValueScratchXYZPPP = getAmbientOcclusionLightValue(
						blockAccess, par2 + 1, par3, par4 + 1);
				aoBrightnessXYZPPP = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2 + 1, par3, par4 + 1);
			}

			if (renderMaxY >= 1.0D) {
				--par3;
			}

			var21 = var14;

			if (renderMaxY >= 1.0D
					|| !blockAccess.isBlockOpaqueCube(par2, par3 + 1, par4)) {
				var21 = par1Block.getMixedBrightnessForBlock(blockAccess, par2,
						par3 + 1, par4);
			}

			var20 = getAmbientOcclusionLightValue(blockAccess, par2, par3 + 1,
					par4);
			var12 = (aoLightValueScratchXYZNPP + aoLightValueScratchXYNP
					+ aoLightValueScratchYZPP + var20) / 4.0F;
			var9 = (aoLightValueScratchYZPP + var20 + aoLightValueScratchXYZPPP + aoLightValueScratchXYPP) / 4.0F;
			var10 = (var20 + aoLightValueScratchYZPN + aoLightValueScratchXYPP + aoLightValueScratchXYZPPN) / 4.0F;
			var11 = (aoLightValueScratchXYNP + aoLightValueScratchXYZNPN
					+ var20 + aoLightValueScratchYZPN) / 4.0F;
			brightnessTopRight = getAoBrightness(aoBrightnessXYZNPP,
					aoBrightnessXYNP, aoBrightnessYZPP, var21);
			brightnessTopLeft = getAoBrightness(aoBrightnessYZPP,
					aoBrightnessXYZPPP, aoBrightnessXYPP, var21);
			brightnessBottomLeft = getAoBrightness(aoBrightnessYZPN,
					aoBrightnessXYPP, aoBrightnessXYZPPN, var21);
			brightnessBottomRight = getAoBrightness(aoBrightnessXYNP,
					aoBrightnessXYZNPN, aoBrightnessYZPN, var21);
			colorRedTopLeft = colorRedBottomLeft = colorRedBottomRight = colorRedTopRight = par5;
			colorGreenTopLeft = colorGreenBottomLeft = colorGreenBottomRight = colorGreenTopRight = par6;
			colorBlueTopLeft = colorBlueBottomLeft = colorBlueBottomRight = colorBlueTopRight = par7;
			colorRedTopLeft *= var9;
			colorGreenTopLeft *= var9;
			colorBlueTopLeft *= var9;
			colorRedBottomLeft *= var10;
			colorGreenBottomLeft *= var10;
			colorBlueBottomLeft *= var10;
			colorRedBottomRight *= var11;
			colorGreenBottomRight *= var11;
			colorBlueBottomRight *= var11;
			colorRedTopRight *= var12;
			colorGreenTopRight *= var12;
			colorBlueTopRight *= var12;
			renderFaceYPos(par1Block, par2, par3, par4, this.getBlockIcon(
					par1Block, blockAccess, par2, par3, par4, 1));
			var8 = true;
		}

		float var23;
		float var22;
		float var25;
		float var24;
		int var27;
		int var26;
		int var29;
		int var28;
		Icon var30;

		if (renderAllFaces
				|| par1Block.shouldSideBeRendered(blockAccess, par2, par3,
						par4 - 1, 2)) {
			if (renderMinZ <= 0.0D) {
				--par4;
			}

			aoLightValueScratchXZNN = getAmbientOcclusionLightValue(
					blockAccess, par2 - 1, par3, par4);
			aoLightValueScratchYZNN = getAmbientOcclusionLightValue(
					blockAccess, par2, par3 - 1, par4);
			aoLightValueScratchYZPN = getAmbientOcclusionLightValue(
					blockAccess, par2, par3 + 1, par4);
			aoLightValueScratchXZPN = getAmbientOcclusionLightValue(
					blockAccess, par2 + 1, par3, par4);
			aoBrightnessXZNN = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2 - 1, par3, par4);
			aoBrightnessYZNN = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2, par3 - 1, par4);
			aoBrightnessYZPN = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2, par3 + 1, par4);
			aoBrightnessXZPN = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2 + 1, par3, par4);
			var17 = Block.canBlockGrass[blockAccess.getBlockId(par2 + 1, par3,
					par4 - 1)];
			var16 = Block.canBlockGrass[blockAccess.getBlockId(par2 - 1, par3,
					par4 - 1)];
			var19 = Block.canBlockGrass[blockAccess.getBlockId(par2, par3 + 1,
					par4 - 1)];
			var18 = Block.canBlockGrass[blockAccess.getBlockId(par2, par3 - 1,
					par4 - 1)];

			if (!var16 && !var18) {
				aoLightValueScratchXYZNNN = aoLightValueScratchXZNN;
				aoBrightnessXYZNNN = aoBrightnessXZNN;
			} else {
				aoLightValueScratchXYZNNN = getAmbientOcclusionLightValue(
						blockAccess, par2 - 1, par3 - 1, par4);
				aoBrightnessXYZNNN = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2 - 1, par3 - 1, par4);
			}

			if (!var16 && !var19) {
				aoLightValueScratchXYZNPN = aoLightValueScratchXZNN;
				aoBrightnessXYZNPN = aoBrightnessXZNN;
			} else {
				aoLightValueScratchXYZNPN = getAmbientOcclusionLightValue(
						blockAccess, par2 - 1, par3 + 1, par4);
				aoBrightnessXYZNPN = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2 - 1, par3 + 1, par4);
			}

			if (!var17 && !var18) {
				aoLightValueScratchXYZPNN = aoLightValueScratchXZPN;
				aoBrightnessXYZPNN = aoBrightnessXZPN;
			} else {
				aoLightValueScratchXYZPNN = getAmbientOcclusionLightValue(
						blockAccess, par2 + 1, par3 - 1, par4);
				aoBrightnessXYZPNN = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2 + 1, par3 - 1, par4);
			}

			if (!var17 && !var19) {
				aoLightValueScratchXYZPPN = aoLightValueScratchXZPN;
				aoBrightnessXYZPPN = aoBrightnessXZPN;
			} else {
				aoLightValueScratchXYZPPN = getAmbientOcclusionLightValue(
						blockAccess, par2 + 1, par3 + 1, par4);
				aoBrightnessXYZPPN = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2 + 1, par3 + 1, par4);
			}

			if (renderMinZ <= 0.0D) {
				++par4;
			}

			var21 = var14;

			if (renderMinZ <= 0.0D
					|| !blockAccess.isBlockOpaqueCube(par2, par3, par4 - 1)) {
				var21 = par1Block.getMixedBrightnessForBlock(blockAccess, par2,
						par3, par4 - 1);
			}

			var20 = getAmbientOcclusionLightValue(blockAccess, par2, par3,
					par4 - 1);
			var23 = (aoLightValueScratchXZNN + aoLightValueScratchXYZNPN
					+ var20 + aoLightValueScratchYZPN) / 4.0F;
			var22 = (var20 + aoLightValueScratchYZPN + aoLightValueScratchXZPN + aoLightValueScratchXYZPPN) / 4.0F;
			var25 = (aoLightValueScratchYZNN + var20
					+ aoLightValueScratchXYZPNN + aoLightValueScratchXZPN) / 4.0F;
			var24 = (aoLightValueScratchXYZNNN + aoLightValueScratchXZNN
					+ aoLightValueScratchYZNN + var20) / 4.0F;
			var9 = (float) (var23 * renderMaxY * (1.0D - renderMinX) + var22
					* renderMinY * renderMinX + var25 * (1.0D - renderMaxY)
					* renderMinX + var24 * (1.0D - renderMaxY)
					* (1.0D - renderMinX));
			var10 = (float) (var23 * renderMaxY * (1.0D - renderMaxX) + var22
					* renderMaxY * renderMaxX + var25 * (1.0D - renderMaxY)
					* renderMaxX + var24 * (1.0D - renderMaxY)
					* (1.0D - renderMaxX));
			var11 = (float) (var23 * renderMinY * (1.0D - renderMaxX) + var22
					* renderMinY * renderMaxX + var25 * (1.0D - renderMinY)
					* renderMaxX + var24 * (1.0D - renderMinY)
					* (1.0D - renderMaxX));
			var12 = (float) (var23 * renderMinY * (1.0D - renderMinX) + var22
					* renderMinY * renderMinX + var25 * (1.0D - renderMinY)
					* renderMinX + var24 * (1.0D - renderMinY)
					* (1.0D - renderMinX));
			var27 = getAoBrightness(aoBrightnessXZNN, aoBrightnessXYZNPN,
					aoBrightnessYZPN, var21);
			var26 = getAoBrightness(aoBrightnessYZPN, aoBrightnessXZPN,
					aoBrightnessXYZPPN, var21);
			var29 = getAoBrightness(aoBrightnessYZNN, aoBrightnessXYZPNN,
					aoBrightnessXZPN, var21);
			var28 = getAoBrightness(aoBrightnessXYZNNN, aoBrightnessXZNN,
					aoBrightnessYZNN, var21);
			brightnessTopLeft = mixAoBrightness(var27, var26, var29, var28,
					renderMaxY * (1.0D - renderMinX), renderMaxY * renderMinX,
					(1.0D - renderMaxY) * renderMinX, (1.0D - renderMaxY)
							* (1.0D - renderMinX));
			brightnessBottomLeft = mixAoBrightness(var27, var26, var29, var28,
					renderMaxY * (1.0D - renderMaxX), renderMaxY * renderMaxX,
					(1.0D - renderMaxY) * renderMaxX, (1.0D - renderMaxY)
							* (1.0D - renderMaxX));
			brightnessBottomRight = mixAoBrightness(var27, var26, var29, var28,
					renderMinY * (1.0D - renderMaxX), renderMinY * renderMaxX,
					(1.0D - renderMinY) * renderMaxX, (1.0D - renderMinY)
							* (1.0D - renderMaxX));
			brightnessTopRight = mixAoBrightness(var27, var26, var29, var28,
					renderMinY * (1.0D - renderMinX), renderMinY * renderMinX,
					(1.0D - renderMinY) * renderMinX, (1.0D - renderMinY)
							* (1.0D - renderMinX));

			if (var13) {
				colorRedTopLeft = colorRedBottomLeft = colorRedBottomRight = colorRedTopRight = par5 * 0.8F;
				colorGreenTopLeft = colorGreenBottomLeft = colorGreenBottomRight = colorGreenTopRight = par6 * 0.8F;
				colorBlueTopLeft = colorBlueBottomLeft = colorBlueBottomRight = colorBlueTopRight = par7 * 0.8F;
			} else {
				colorRedTopLeft = colorRedBottomLeft = colorRedBottomRight = colorRedTopRight = 0.8F;
				colorGreenTopLeft = colorGreenBottomLeft = colorGreenBottomRight = colorGreenTopRight = 0.8F;
				colorBlueTopLeft = colorBlueBottomLeft = colorBlueBottomRight = colorBlueTopRight = 0.8F;
			}

			colorRedTopLeft *= var9;
			colorGreenTopLeft *= var9;
			colorBlueTopLeft *= var9;
			colorRedBottomLeft *= var10;
			colorGreenBottomLeft *= var10;
			colorBlueBottomLeft *= var10;
			colorRedBottomRight *= var11;
			colorGreenBottomRight *= var11;
			colorBlueBottomRight *= var11;
			colorRedTopRight *= var12;
			colorGreenTopRight *= var12;
			colorBlueTopRight *= var12;
			var30 = this.getBlockIcon(par1Block, blockAccess, par2, par3, par4,
					2);
			renderFaceZNeg(par1Block, par2, par3, par4, var30);

			if (RenderBlocks.fancyGrass
					&& var30.getIconName().equals("grass_side")
					&& !hasOverrideBlockTexture()) {
				colorRedTopLeft *= par5;
				colorRedBottomLeft *= par5;
				colorRedBottomRight *= par5;
				colorRedTopRight *= par5;
				colorGreenTopLeft *= par6;
				colorGreenBottomLeft *= par6;
				colorGreenBottomRight *= par6;
				colorGreenTopRight *= par6;
				colorBlueTopLeft *= par7;
				colorBlueBottomLeft *= par7;
				colorBlueBottomRight *= par7;
				colorBlueTopRight *= par7;
				renderFaceZNeg(par1Block, par2, par3, par4,
						BlockGrass.getIconSideOverlay());
			}

			var8 = true;
		}

		if (renderAllFaces
				|| par1Block.shouldSideBeRendered(blockAccess, par2, par3,
						par4 + 1, 3)) {
			if (renderMaxZ >= 1.0D) {
				++par4;
			}

			aoLightValueScratchXZNP = getAmbientOcclusionLightValue(
					blockAccess, par2 - 1, par3, par4);
			aoLightValueScratchXZPP = getAmbientOcclusionLightValue(
					blockAccess, par2 + 1, par3, par4);
			aoLightValueScratchYZNP = getAmbientOcclusionLightValue(
					blockAccess, par2, par3 - 1, par4);
			aoLightValueScratchYZPP = getAmbientOcclusionLightValue(
					blockAccess, par2, par3 + 1, par4);
			aoBrightnessXZNP = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2 - 1, par3, par4);
			aoBrightnessXZPP = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2 + 1, par3, par4);
			aoBrightnessYZNP = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2, par3 - 1, par4);
			aoBrightnessYZPP = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2, par3 + 1, par4);
			var17 = Block.canBlockGrass[blockAccess.getBlockId(par2 + 1, par3,
					par4 + 1)];
			var16 = Block.canBlockGrass[blockAccess.getBlockId(par2 - 1, par3,
					par4 + 1)];
			var19 = Block.canBlockGrass[blockAccess.getBlockId(par2, par3 + 1,
					par4 + 1)];
			var18 = Block.canBlockGrass[blockAccess.getBlockId(par2, par3 - 1,
					par4 + 1)];

			if (!var16 && !var18) {
				aoLightValueScratchXYZNNP = aoLightValueScratchXZNP;
				aoBrightnessXYZNNP = aoBrightnessXZNP;
			} else {
				aoLightValueScratchXYZNNP = getAmbientOcclusionLightValue(
						blockAccess, par2 - 1, par3 - 1, par4);
				aoBrightnessXYZNNP = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2 - 1, par3 - 1, par4);
			}

			if (!var16 && !var19) {
				aoLightValueScratchXYZNPP = aoLightValueScratchXZNP;
				aoBrightnessXYZNPP = aoBrightnessXZNP;
			} else {
				aoLightValueScratchXYZNPP = getAmbientOcclusionLightValue(
						blockAccess, par2 - 1, par3 + 1, par4);
				aoBrightnessXYZNPP = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2 - 1, par3 + 1, par4);
			}

			if (!var17 && !var18) {
				aoLightValueScratchXYZPNP = aoLightValueScratchXZPP;
				aoBrightnessXYZPNP = aoBrightnessXZPP;
			} else {
				aoLightValueScratchXYZPNP = getAmbientOcclusionLightValue(
						blockAccess, par2 + 1, par3 - 1, par4);
				aoBrightnessXYZPNP = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2 + 1, par3 - 1, par4);
			}

			if (!var17 && !var19) {
				aoLightValueScratchXYZPPP = aoLightValueScratchXZPP;
				aoBrightnessXYZPPP = aoBrightnessXZPP;
			} else {
				aoLightValueScratchXYZPPP = getAmbientOcclusionLightValue(
						blockAccess, par2 + 1, par3 + 1, par4);
				aoBrightnessXYZPPP = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2 + 1, par3 + 1, par4);
			}

			if (renderMaxZ >= 1.0D) {
				--par4;
			}

			var21 = var14;

			if (renderMaxZ >= 1.0D
					|| !blockAccess.isBlockOpaqueCube(par2, par3, par4 + 1)) {
				var21 = par1Block.getMixedBrightnessForBlock(blockAccess, par2,
						par3, par4 + 1);
			}

			var20 = getAmbientOcclusionLightValue(blockAccess, par2, par3,
					par4 + 1);
			var23 = (aoLightValueScratchXZNP + aoLightValueScratchXYZNPP
					+ var20 + aoLightValueScratchYZPP) / 4.0F;
			var22 = (var20 + aoLightValueScratchYZPP + aoLightValueScratchXZPP + aoLightValueScratchXYZPPP) / 4.0F;
			var25 = (aoLightValueScratchYZNP + var20
					+ aoLightValueScratchXYZPNP + aoLightValueScratchXZPP) / 4.0F;
			var24 = (aoLightValueScratchXYZNNP + aoLightValueScratchXZNP
					+ aoLightValueScratchYZNP + var20) / 4.0F;
			var9 = (float) (var23 * renderMaxY * (1.0D - renderMinX) + var22
					* renderMaxY * renderMinX + var25 * (1.0D - renderMaxY)
					* renderMinX + var24 * (1.0D - renderMaxY)
					* (1.0D - renderMinX));
			var10 = (float) (var23 * renderMinY * (1.0D - renderMinX) + var22
					* renderMinY * renderMinX + var25 * (1.0D - renderMinY)
					* renderMinX + var24 * (1.0D - renderMinY)
					* (1.0D - renderMinX));
			var11 = (float) (var23 * renderMinY * (1.0D - renderMaxX) + var22
					* renderMinY * renderMaxX + var25 * (1.0D - renderMinY)
					* renderMaxX + var24 * (1.0D - renderMinY)
					* (1.0D - renderMaxX));
			var12 = (float) (var23 * renderMaxY * (1.0D - renderMaxX) + var22
					* renderMaxY * renderMaxX + var25 * (1.0D - renderMaxY)
					* renderMaxX + var24 * (1.0D - renderMaxY)
					* (1.0D - renderMaxX));
			var27 = getAoBrightness(aoBrightnessXZNP, aoBrightnessXYZNPP,
					aoBrightnessYZPP, var21);
			var26 = getAoBrightness(aoBrightnessYZPP, aoBrightnessXZPP,
					aoBrightnessXYZPPP, var21);
			var29 = getAoBrightness(aoBrightnessYZNP, aoBrightnessXYZPNP,
					aoBrightnessXZPP, var21);
			var28 = getAoBrightness(aoBrightnessXYZNNP, aoBrightnessXZNP,
					aoBrightnessYZNP, var21);
			brightnessTopLeft = mixAoBrightness(var27, var28, var29, var26,
					renderMaxY * (1.0D - renderMinX), (1.0D - renderMaxY)
							* (1.0D - renderMinX), (1.0D - renderMaxY)
							* renderMinX, renderMaxY * renderMinX);
			brightnessBottomLeft = mixAoBrightness(var27, var28, var29, var26,
					renderMinY * (1.0D - renderMinX), (1.0D - renderMinY)
							* (1.0D - renderMinX), (1.0D - renderMinY)
							* renderMinX, renderMinY * renderMinX);
			brightnessBottomRight = mixAoBrightness(var27, var28, var29, var26,
					renderMinY * (1.0D - renderMaxX), (1.0D - renderMinY)
							* (1.0D - renderMaxX), (1.0D - renderMinY)
							* renderMaxX, renderMinY * renderMaxX);
			brightnessTopRight = mixAoBrightness(var27, var28, var29, var26,
					renderMaxY * (1.0D - renderMaxX), (1.0D - renderMaxY)
							* (1.0D - renderMaxX), (1.0D - renderMaxY)
							* renderMaxX, renderMaxY * renderMaxX);

			if (var13) {
				colorRedTopLeft = colorRedBottomLeft = colorRedBottomRight = colorRedTopRight = par5 * 0.8F;
				colorGreenTopLeft = colorGreenBottomLeft = colorGreenBottomRight = colorGreenTopRight = par6 * 0.8F;
				colorBlueTopLeft = colorBlueBottomLeft = colorBlueBottomRight = colorBlueTopRight = par7 * 0.8F;
			} else {
				colorRedTopLeft = colorRedBottomLeft = colorRedBottomRight = colorRedTopRight = 0.8F;
				colorGreenTopLeft = colorGreenBottomLeft = colorGreenBottomRight = colorGreenTopRight = 0.8F;
				colorBlueTopLeft = colorBlueBottomLeft = colorBlueBottomRight = colorBlueTopRight = 0.8F;
			}

			colorRedTopLeft *= var9;
			colorGreenTopLeft *= var9;
			colorBlueTopLeft *= var9;
			colorRedBottomLeft *= var10;
			colorGreenBottomLeft *= var10;
			colorBlueBottomLeft *= var10;
			colorRedBottomRight *= var11;
			colorGreenBottomRight *= var11;
			colorBlueBottomRight *= var11;
			colorRedTopRight *= var12;
			colorGreenTopRight *= var12;
			colorBlueTopRight *= var12;
			var30 = this.getBlockIcon(par1Block, blockAccess, par2, par3, par4,
					3);
			renderFaceZPos(par1Block, par2, par3, par4, var30);

			if (RenderBlocks.fancyGrass
					&& var30.getIconName().equals("grass_side")
					&& !hasOverrideBlockTexture()) {
				colorRedTopLeft *= par5;
				colorRedBottomLeft *= par5;
				colorRedBottomRight *= par5;
				colorRedTopRight *= par5;
				colorGreenTopLeft *= par6;
				colorGreenBottomLeft *= par6;
				colorGreenBottomRight *= par6;
				colorGreenTopRight *= par6;
				colorBlueTopLeft *= par7;
				colorBlueBottomLeft *= par7;
				colorBlueBottomRight *= par7;
				colorBlueTopRight *= par7;
				renderFaceZPos(par1Block, par2, par3, par4,
						BlockGrass.getIconSideOverlay());
			}

			var8 = true;
		}

		if (renderAllFaces
				|| par1Block.shouldSideBeRendered(blockAccess, par2 - 1, par3,
						par4, 4)) {
			if (renderMinX <= 0.0D) {
				--par2;
			}

			aoLightValueScratchXYNN = getAmbientOcclusionLightValue(
					blockAccess, par2, par3 - 1, par4);
			aoLightValueScratchXZNN = getAmbientOcclusionLightValue(
					blockAccess, par2, par3, par4 - 1);
			aoLightValueScratchXZNP = getAmbientOcclusionLightValue(
					blockAccess, par2, par3, par4 + 1);
			aoLightValueScratchXYNP = getAmbientOcclusionLightValue(
					blockAccess, par2, par3 + 1, par4);
			aoBrightnessXYNN = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2, par3 - 1, par4);
			aoBrightnessXZNN = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2, par3, par4 - 1);
			aoBrightnessXZNP = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2, par3, par4 + 1);
			aoBrightnessXYNP = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2, par3 + 1, par4);
			var17 = Block.canBlockGrass[blockAccess.getBlockId(par2 - 1,
					par3 + 1, par4)];
			var16 = Block.canBlockGrass[blockAccess.getBlockId(par2 - 1,
					par3 - 1, par4)];
			var19 = Block.canBlockGrass[blockAccess.getBlockId(par2 - 1, par3,
					par4 - 1)];
			var18 = Block.canBlockGrass[blockAccess.getBlockId(par2 - 1, par3,
					par4 + 1)];

			if (!var19 && !var16) {
				aoLightValueScratchXYZNNN = aoLightValueScratchXZNN;
				aoBrightnessXYZNNN = aoBrightnessXZNN;
			} else {
				aoLightValueScratchXYZNNN = getAmbientOcclusionLightValue(
						blockAccess, par2, par3 - 1, par4 - 1);
				aoBrightnessXYZNNN = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2, par3 - 1, par4 - 1);
			}

			if (!var18 && !var16) {
				aoLightValueScratchXYZNNP = aoLightValueScratchXZNP;
				aoBrightnessXYZNNP = aoBrightnessXZNP;
			} else {
				aoLightValueScratchXYZNNP = getAmbientOcclusionLightValue(
						blockAccess, par2, par3 - 1, par4 + 1);
				aoBrightnessXYZNNP = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2, par3 - 1, par4 + 1);
			}

			if (!var19 && !var17) {
				aoLightValueScratchXYZNPN = aoLightValueScratchXZNN;
				aoBrightnessXYZNPN = aoBrightnessXZNN;
			} else {
				aoLightValueScratchXYZNPN = getAmbientOcclusionLightValue(
						blockAccess, par2, par3 + 1, par4 - 1);
				aoBrightnessXYZNPN = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2, par3 + 1, par4 - 1);
			}

			if (!var18 && !var17) {
				aoLightValueScratchXYZNPP = aoLightValueScratchXZNP;
				aoBrightnessXYZNPP = aoBrightnessXZNP;
			} else {
				aoLightValueScratchXYZNPP = getAmbientOcclusionLightValue(
						blockAccess, par2, par3 + 1, par4 + 1);
				aoBrightnessXYZNPP = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2, par3 + 1, par4 + 1);
			}

			if (renderMinX <= 0.0D) {
				++par2;
			}

			var21 = var14;

			if (renderMinX <= 0.0D
					|| !blockAccess.isBlockOpaqueCube(par2 - 1, par3, par4)) {
				var21 = par1Block.getMixedBrightnessForBlock(blockAccess,
						par2 - 1, par3, par4);
			}

			var20 = getAmbientOcclusionLightValue(blockAccess, par2 - 1, par3,
					par4);
			var23 = (aoLightValueScratchXYNN + aoLightValueScratchXYZNNP
					+ var20 + aoLightValueScratchXZNP) / 4.0F;
			var22 = (var20 + aoLightValueScratchXZNP + aoLightValueScratchXYNP + aoLightValueScratchXYZNPP) / 4.0F;
			var25 = (aoLightValueScratchXZNN + var20
					+ aoLightValueScratchXYZNPN + aoLightValueScratchXYNP) / 4.0F;
			var24 = (aoLightValueScratchXYZNNN + aoLightValueScratchXYNN
					+ aoLightValueScratchXZNN + var20) / 4.0F;
			var9 = (float) (var22 * renderMaxY * renderMaxZ + var25
					* renderMaxY * (1.0D - renderMaxZ) + var24
					* (1.0D - renderMaxY) * (1.0D - renderMaxZ) + var23
					* (1.0D - renderMaxY) * renderMaxZ);
			var10 = (float) (var22 * renderMaxY * renderMinZ + var25
					* renderMaxY * (1.0D - renderMinZ) + var24
					* (1.0D - renderMaxY) * (1.0D - renderMinZ) + var23
					* (1.0D - renderMaxY) * renderMinZ);
			var11 = (float) (var22 * renderMinY * renderMinZ + var25
					* renderMinY * (1.0D - renderMinZ) + var24
					* (1.0D - renderMinY) * (1.0D - renderMinZ) + var23
					* (1.0D - renderMinY) * renderMinZ);
			var12 = (float) (var22 * renderMinY * renderMaxZ + var25
					* renderMinY * (1.0D - renderMaxZ) + var24
					* (1.0D - renderMinY) * (1.0D - renderMaxZ) + var23
					* (1.0D - renderMinY) * renderMaxZ);
			var27 = getAoBrightness(aoBrightnessXYNN, aoBrightnessXYZNNP,
					aoBrightnessXZNP, var21);
			var26 = getAoBrightness(aoBrightnessXZNP, aoBrightnessXYNP,
					aoBrightnessXYZNPP, var21);
			var29 = getAoBrightness(aoBrightnessXZNN, aoBrightnessXYZNPN,
					aoBrightnessXYNP, var21);
			var28 = getAoBrightness(aoBrightnessXYZNNN, aoBrightnessXYNN,
					aoBrightnessXZNN, var21);
			brightnessTopLeft = mixAoBrightness(var26, var29, var28, var27,
					renderMaxY * renderMaxZ, renderMaxY * (1.0D - renderMaxZ),
					(1.0D - renderMaxY) * (1.0D - renderMaxZ),
					(1.0D - renderMaxY) * renderMaxZ);
			brightnessBottomLeft = mixAoBrightness(var26, var29, var28, var27,
					renderMaxY * renderMinZ, renderMaxY * (1.0D - renderMinZ),
					(1.0D - renderMaxY) * (1.0D - renderMinZ),
					(1.0D - renderMaxY) * renderMinZ);
			brightnessBottomRight = mixAoBrightness(var26, var29, var28, var27,
					renderMinY * renderMinZ, renderMinY * (1.0D - renderMinZ),
					(1.0D - renderMinY) * (1.0D - renderMinZ),
					(1.0D - renderMinY) * renderMinZ);
			brightnessTopRight = mixAoBrightness(var26, var29, var28, var27,
					renderMinY * renderMaxZ, renderMinY * (1.0D - renderMaxZ),
					(1.0D - renderMinY) * (1.0D - renderMaxZ),
					(1.0D - renderMinY) * renderMaxZ);

			if (var13) {
				colorRedTopLeft = colorRedBottomLeft = colorRedBottomRight = colorRedTopRight = par5 * 0.6F;
				colorGreenTopLeft = colorGreenBottomLeft = colorGreenBottomRight = colorGreenTopRight = par6 * 0.6F;
				colorBlueTopLeft = colorBlueBottomLeft = colorBlueBottomRight = colorBlueTopRight = par7 * 0.6F;
			} else {
				colorRedTopLeft = colorRedBottomLeft = colorRedBottomRight = colorRedTopRight = 0.6F;
				colorGreenTopLeft = colorGreenBottomLeft = colorGreenBottomRight = colorGreenTopRight = 0.6F;
				colorBlueTopLeft = colorBlueBottomLeft = colorBlueBottomRight = colorBlueTopRight = 0.6F;
			}

			colorRedTopLeft *= var9;
			colorGreenTopLeft *= var9;
			colorBlueTopLeft *= var9;
			colorRedBottomLeft *= var10;
			colorGreenBottomLeft *= var10;
			colorBlueBottomLeft *= var10;
			colorRedBottomRight *= var11;
			colorGreenBottomRight *= var11;
			colorBlueBottomRight *= var11;
			colorRedTopRight *= var12;
			colorGreenTopRight *= var12;
			colorBlueTopRight *= var12;
			var30 = this.getBlockIcon(par1Block, blockAccess, par2, par3, par4,
					4);
			renderFaceXNeg(par1Block, par2, par3, par4, var30);

			if (RenderBlocks.fancyGrass
					&& var30.getIconName().equals("grass_side")
					&& !hasOverrideBlockTexture()) {
				colorRedTopLeft *= par5;
				colorRedBottomLeft *= par5;
				colorRedBottomRight *= par5;
				colorRedTopRight *= par5;
				colorGreenTopLeft *= par6;
				colorGreenBottomLeft *= par6;
				colorGreenBottomRight *= par6;
				colorGreenTopRight *= par6;
				colorBlueTopLeft *= par7;
				colorBlueBottomLeft *= par7;
				colorBlueBottomRight *= par7;
				colorBlueTopRight *= par7;
				renderFaceXNeg(par1Block, par2, par3, par4,
						BlockGrass.getIconSideOverlay());
			}

			var8 = true;
		}

		if (renderAllFaces
				|| par1Block.shouldSideBeRendered(blockAccess, par2 + 1, par3,
						par4, 5)) {
			if (renderMaxX >= 1.0D) {
				++par2;
			}

			aoLightValueScratchXYPN = getAmbientOcclusionLightValue(
					blockAccess, par2, par3 - 1, par4);
			aoLightValueScratchXZPN = getAmbientOcclusionLightValue(
					blockAccess, par2, par3, par4 - 1);
			aoLightValueScratchXZPP = getAmbientOcclusionLightValue(
					blockAccess, par2, par3, par4 + 1);
			aoLightValueScratchXYPP = getAmbientOcclusionLightValue(
					blockAccess, par2, par3 + 1, par4);
			aoBrightnessXYPN = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2, par3 - 1, par4);
			aoBrightnessXZPN = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2, par3, par4 - 1);
			aoBrightnessXZPP = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2, par3, par4 + 1);
			aoBrightnessXYPP = par1Block.getMixedBrightnessForBlock(
					blockAccess, par2, par3 + 1, par4);
			var17 = Block.canBlockGrass[blockAccess.getBlockId(par2 + 1,
					par3 + 1, par4)];
			var16 = Block.canBlockGrass[blockAccess.getBlockId(par2 + 1,
					par3 - 1, par4)];
			var19 = Block.canBlockGrass[blockAccess.getBlockId(par2 + 1, par3,
					par4 + 1)];
			var18 = Block.canBlockGrass[blockAccess.getBlockId(par2 + 1, par3,
					par4 - 1)];

			if (!var16 && !var18) {
				aoLightValueScratchXYZPNN = aoLightValueScratchXZPN;
				aoBrightnessXYZPNN = aoBrightnessXZPN;
			} else {
				aoLightValueScratchXYZPNN = getAmbientOcclusionLightValue(
						blockAccess, par2, par3 - 1, par4 - 1);
				aoBrightnessXYZPNN = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2, par3 - 1, par4 - 1);
			}

			if (!var16 && !var19) {
				aoLightValueScratchXYZPNP = aoLightValueScratchXZPP;
				aoBrightnessXYZPNP = aoBrightnessXZPP;
			} else {
				aoLightValueScratchXYZPNP = getAmbientOcclusionLightValue(
						blockAccess, par2, par3 - 1, par4 + 1);
				aoBrightnessXYZPNP = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2, par3 - 1, par4 + 1);
			}

			if (!var17 && !var18) {
				aoLightValueScratchXYZPPN = aoLightValueScratchXZPN;
				aoBrightnessXYZPPN = aoBrightnessXZPN;
			} else {
				aoLightValueScratchXYZPPN = getAmbientOcclusionLightValue(
						blockAccess, par2, par3 + 1, par4 - 1);
				aoBrightnessXYZPPN = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2, par3 + 1, par4 - 1);
			}

			if (!var17 && !var19) {
				aoLightValueScratchXYZPPP = aoLightValueScratchXZPP;
				aoBrightnessXYZPPP = aoBrightnessXZPP;
			} else {
				aoLightValueScratchXYZPPP = getAmbientOcclusionLightValue(
						blockAccess, par2, par3 + 1, par4 + 1);
				aoBrightnessXYZPPP = par1Block.getMixedBrightnessForBlock(
						blockAccess, par2, par3 + 1, par4 + 1);
			}

			if (renderMaxX >= 1.0D) {
				--par2;
			}

			var21 = var14;

			if (renderMaxX >= 1.0D
					|| !blockAccess.isBlockOpaqueCube(par2 + 1, par3, par4)) {
				var21 = par1Block.getMixedBrightnessForBlock(blockAccess,
						par2 + 1, par3, par4);
			}

			var20 = getAmbientOcclusionLightValue(blockAccess, par2 + 1, par3,
					par4);
			var23 = (aoLightValueScratchXYPN + aoLightValueScratchXYZPNP
					+ var20 + aoLightValueScratchXZPP) / 4.0F;
			var22 = (aoLightValueScratchXYZPNN + aoLightValueScratchXYPN
					+ aoLightValueScratchXZPN + var20) / 4.0F;
			var25 = (aoLightValueScratchXZPN + var20
					+ aoLightValueScratchXYZPPN + aoLightValueScratchXYPP) / 4.0F;
			var24 = (var20 + aoLightValueScratchXZPP + aoLightValueScratchXYPP + aoLightValueScratchXYZPPP) / 4.0F;
			var9 = (float) (var23 * (1.0D - renderMinY) * renderMaxZ + var22
					* (1.0D - renderMinY) * (1.0D - renderMaxZ) + var25
					* renderMinY * (1.0D - renderMaxZ) + var24 * renderMinY
					* renderMaxZ);
			var10 = (float) (var23 * (1.0D - renderMinY) * renderMinZ + var22
					* (1.0D - renderMinY) * (1.0D - renderMinZ) + var25
					* renderMinY * (1.0D - renderMinZ) + var24 * renderMinY
					* renderMinZ);
			var11 = (float) (var23 * (1.0D - renderMaxY) * renderMinZ + var22
					* (1.0D - renderMaxY) * (1.0D - renderMinZ) + var25
					* renderMaxY * (1.0D - renderMinZ) + var24 * renderMaxY
					* renderMinZ);
			var12 = (float) (var23 * (1.0D - renderMaxY) * renderMaxZ + var22
					* (1.0D - renderMaxY) * (1.0D - renderMaxZ) + var25
					* renderMaxY * (1.0D - renderMaxZ) + var24 * renderMaxY
					* renderMaxZ);
			var27 = getAoBrightness(aoBrightnessXYPN, aoBrightnessXYZPNP,
					aoBrightnessXZPP, var21);
			var26 = getAoBrightness(aoBrightnessXZPP, aoBrightnessXYPP,
					aoBrightnessXYZPPP, var21);
			var29 = getAoBrightness(aoBrightnessXZPN, aoBrightnessXYZPPN,
					aoBrightnessXYPP, var21);
			var28 = getAoBrightness(aoBrightnessXYZPNN, aoBrightnessXYPN,
					aoBrightnessXZPN, var21);
			brightnessTopLeft = mixAoBrightness(var27, var28, var29, var26,
					(1.0D - renderMinY) * renderMaxZ, (1.0D - renderMinY)
							* (1.0D - renderMaxZ), renderMinY
							* (1.0D - renderMaxZ), renderMinY * renderMaxZ);
			brightnessBottomLeft = mixAoBrightness(var27, var28, var29, var26,
					(1.0D - renderMinY) * renderMinZ, (1.0D - renderMinY)
							* (1.0D - renderMinZ), renderMinY
							* (1.0D - renderMinZ), renderMinY * renderMinZ);
			brightnessBottomRight = mixAoBrightness(var27, var28, var29, var26,
					(1.0D - renderMaxY) * renderMinZ, (1.0D - renderMaxY)
							* (1.0D - renderMinZ), renderMaxY
							* (1.0D - renderMinZ), renderMaxY * renderMinZ);
			brightnessTopRight = mixAoBrightness(var27, var28, var29, var26,
					(1.0D - renderMaxY) * renderMaxZ, (1.0D - renderMaxY)
							* (1.0D - renderMaxZ), renderMaxY
							* (1.0D - renderMaxZ), renderMaxY * renderMaxZ);

			if (var13) {
				colorRedTopLeft = colorRedBottomLeft = colorRedBottomRight = colorRedTopRight = par5 * 0.6F;
				colorGreenTopLeft = colorGreenBottomLeft = colorGreenBottomRight = colorGreenTopRight = par6 * 0.6F;
				colorBlueTopLeft = colorBlueBottomLeft = colorBlueBottomRight = colorBlueTopRight = par7 * 0.6F;
			} else {
				colorRedTopLeft = colorRedBottomLeft = colorRedBottomRight = colorRedTopRight = 0.6F;
				colorGreenTopLeft = colorGreenBottomLeft = colorGreenBottomRight = colorGreenTopRight = 0.6F;
				colorBlueTopLeft = colorBlueBottomLeft = colorBlueBottomRight = colorBlueTopRight = 0.6F;
			}

			colorRedTopLeft *= var9;
			colorGreenTopLeft *= var9;
			colorBlueTopLeft *= var9;
			colorRedBottomLeft *= var10;
			colorGreenBottomLeft *= var10;
			colorBlueBottomLeft *= var10;
			colorRedBottomRight *= var11;
			colorGreenBottomRight *= var11;
			colorBlueBottomRight *= var11;
			colorRedTopRight *= var12;
			colorGreenTopRight *= var12;
			colorBlueTopRight *= var12;
			var30 = this.getBlockIcon(par1Block, blockAccess, par2, par3, par4,
					5);
			renderFaceXPos(par1Block, par2, par3, par4, var30);

			if (RenderBlocks.fancyGrass
					&& var30.getIconName().equals("grass_side")
					&& !hasOverrideBlockTexture()) {
				colorRedTopLeft *= par5;
				colorRedBottomLeft *= par5;
				colorRedBottomRight *= par5;
				colorRedTopRight *= par5;
				colorGreenTopLeft *= par6;
				colorGreenBottomLeft *= par6;
				colorGreenBottomRight *= par6;
				colorGreenTopRight *= par6;
				colorBlueTopLeft *= par7;
				colorBlueBottomLeft *= par7;
				colorBlueBottomRight *= par7;
				colorBlueTopRight *= par7;
				renderFaceXPos(par1Block, par2, par3, par4,
						BlockGrass.getIconSideOverlay());
			}

			var8 = true;
		}

		enableAO = false;
		return var8;
	}

	/**
	 * Get ambient occlusion brightness
	 */
	public int getAoBrightness(int par1, int par2, int par3, final int par4) {
		if (par1 == 0) {
			par1 = par4;
		}

		if (par2 == 0) {
			par2 = par4;
		}

		if (par3 == 0) {
			par3 = par4;
		}

		return par1 + par2 + par3 + par4 >> 2 & 16711935;
	}

	public int mixAoBrightness(final int par1, final int par2, final int par3,
			final int par4, final double par5, final double par7,
			final double par9, final double par11) {
		final int var13 = (int) ((par1 >> 16 & 255) * par5 + (par2 >> 16 & 255)
				* par7 + (par3 >> 16 & 255) * par9 + (par4 >> 16 & 255) * par11) & 255;
		final int var14 = (int) ((par1 & 255) * par5 + (par2 & 255) * par7
				+ (par3 & 255) * par9 + (par4 & 255) * par11) & 255;
		return var13 << 16 | var14;
	}

	/**
	 * Renders a standard cube block at the given coordinates, with a given
	 * color ratio. Args: block, x, y, z, r, g, b
	 */
	public boolean renderStandardBlockWithColorMultiplier(
			final Block par1Block, final int par2, final int par3,
			final int par4, final float par5, final float par6, final float par7) {
		enableAO = false;
		final boolean var8 = Tessellator.instance.defaultTexture;
		final boolean var9 = Config.isBetterGrass() && var8;
		final Tessellator var10 = Tessellator.instance;
		boolean var11 = false;
		int var12 = -1;
		float var13;
		float var14;
		float var15;
		float var16;

		if (renderAllFaces
				|| par1Block.shouldSideBeRendered(blockAccess, par2, par3 - 1,
						par4, 0)) {
			if (var12 < 0) {
				var12 = par1Block.getMixedBrightnessForBlock(blockAccess, par2,
						par3, par4);
			}

			var13 = 0.5F;
			var14 = var13;
			var15 = var13;
			var16 = var13;

			if (par1Block != Block.grass) {
				var14 = var13 * par5;
				var15 = var13 * par6;
				var16 = var13 * par7;
			}

			var10.setBrightness(renderMinY > 0.0D ? var12 : par1Block
					.getMixedBrightnessForBlock(blockAccess, par2, par3 - 1,
							par4));
			var10.setColorOpaque_F(var14, var15, var16);
			renderFaceYNeg(par1Block, par2, par3, par4, this.getBlockIcon(
					par1Block, blockAccess, par2, par3, par4, 0));
			var11 = true;
		}

		if (renderAllFaces
				|| par1Block.shouldSideBeRendered(blockAccess, par2, par3 + 1,
						par4, 1)) {
			if (var12 < 0) {
				var12 = par1Block.getMixedBrightnessForBlock(blockAccess, par2,
						par3, par4);
			}

			var13 = 1.0F;
			var14 = var13 * par5;
			var15 = var13 * par6;
			var16 = var13 * par7;
			var10.setBrightness(renderMaxY < 1.0D ? var12 : par1Block
					.getMixedBrightnessForBlock(blockAccess, par2, par3 + 1,
							par4));
			var10.setColorOpaque_F(var14, var15, var16);
			renderFaceYPos(par1Block, par2, par3, par4, this.getBlockIcon(
					par1Block, blockAccess, par2, par3, par4, 1));
			var11 = true;
		}

		float var17;
		Icon var18;

		if (renderAllFaces
				|| par1Block.shouldSideBeRendered(blockAccess, par2, par3,
						par4 - 1, 2)) {
			if (var12 < 0) {
				var12 = par1Block.getMixedBrightnessForBlock(blockAccess, par2,
						par3, par4);
			}

			var14 = 0.8F;
			var15 = var14;
			var16 = var14;
			var17 = var14;

			if (par1Block != Block.grass) {
				var15 = var14 * par5;
				var16 = var14 * par6;
				var17 = var14 * par7;
			}

			var10.setBrightness(renderMinZ > 0.0D ? var12 : par1Block
					.getMixedBrightnessForBlock(blockAccess, par2, par3,
							par4 - 1));
			var10.setColorOpaque_F(var15, var16, var17);
			var18 = this.getBlockIcon(par1Block, blockAccess, par2, par3, par4,
					2);

			if (var9) {
				if (var18 == TextureUtils.iconGrassSide
						|| var18 == TextureUtils.iconMycelSide) {
					var18 = Config.getSideGrassTexture(blockAccess, par2, par3,
							par4, 2, var18);

					if (var18 == TextureUtils.iconGrassTop) {
						var10.setColorOpaque_F(var15 * par5, var16 * par6,
								var17 * par7);
					}
				}

				if (var18 == TextureUtils.iconSnowSide) {
					var18 = Config.getSideSnowGrassTexture(blockAccess, par2,
							par3, par4, 2);
				}
			}

			renderFaceZNeg(par1Block, par2, par3, par4, var18);

			if (var8 && RenderBlocks.fancyGrass
					&& var18 == TextureUtils.iconGrassSide
					&& !hasOverrideBlockTexture()) {
				var10.setColorOpaque_F(var15 * par5, var16 * par6, var17 * par7);
				renderFaceZNeg(par1Block, par2, par3, par4,
						BlockGrass.getIconSideOverlay());
			}

			var11 = true;
		}

		if (renderAllFaces
				|| par1Block.shouldSideBeRendered(blockAccess, par2, par3,
						par4 + 1, 3)) {
			if (var12 < 0) {
				var12 = par1Block.getMixedBrightnessForBlock(blockAccess, par2,
						par3, par4);
			}

			var14 = 0.8F;
			var15 = var14;
			var16 = var14;
			var17 = var14;

			if (par1Block != Block.grass) {
				var15 = var14 * par5;
				var16 = var14 * par6;
				var17 = var14 * par7;
			}

			var10.setBrightness(renderMaxZ < 1.0D ? var12 : par1Block
					.getMixedBrightnessForBlock(blockAccess, par2, par3,
							par4 + 1));
			var10.setColorOpaque_F(var15, var16, var17);
			var18 = this.getBlockIcon(par1Block, blockAccess, par2, par3, par4,
					3);

			if (var9) {
				if (var18 == TextureUtils.iconGrassSide
						|| var18 == TextureUtils.iconMycelSide) {
					var18 = Config.getSideGrassTexture(blockAccess, par2, par3,
							par4, 3, var18);

					if (var18 == TextureUtils.iconGrassTop) {
						var10.setColorOpaque_F(var15 * par5, var16 * par6,
								var17 * par7);
					}
				}

				if (var18 == TextureUtils.iconSnowSide) {
					var18 = Config.getSideSnowGrassTexture(blockAccess, par2,
							par3, par4, 3);
				}
			}

			renderFaceZPos(par1Block, par2, par3, par4, var18);

			if (var8 && RenderBlocks.fancyGrass
					&& var18 == TextureUtils.iconGrassSide
					&& !hasOverrideBlockTexture()) {
				var10.setColorOpaque_F(var15 * par5, var16 * par6, var17 * par7);
				renderFaceZPos(par1Block, par2, par3, par4,
						BlockGrass.getIconSideOverlay());
			}

			var11 = true;
		}

		if (renderAllFaces
				|| par1Block.shouldSideBeRendered(blockAccess, par2 - 1, par3,
						par4, 4)) {
			if (var12 < 0) {
				var12 = par1Block.getMixedBrightnessForBlock(blockAccess, par2,
						par3, par4);
			}

			var14 = 0.6F;
			var15 = var14;
			var16 = var14;
			var17 = var14;

			if (par1Block != Block.grass) {
				var15 = var14 * par5;
				var16 = var14 * par6;
				var17 = var14 * par7;
			}

			var10.setBrightness(renderMinX > 0.0D ? var12 : par1Block
					.getMixedBrightnessForBlock(blockAccess, par2 - 1, par3,
							par4));
			var10.setColorOpaque_F(var15, var16, var17);
			var18 = this.getBlockIcon(par1Block, blockAccess, par2, par3, par4,
					4);

			if (var9) {
				if (var18 == TextureUtils.iconGrassSide
						|| var18 == TextureUtils.iconMycelSide) {
					var18 = Config.getSideGrassTexture(blockAccess, par2, par3,
							par4, 4, var18);

					if (var18 == TextureUtils.iconGrassTop) {
						var10.setColorOpaque_F(var15 * par5, var16 * par6,
								var17 * par7);
					}
				}

				if (var18 == TextureUtils.iconSnowSide) {
					var18 = Config.getSideSnowGrassTexture(blockAccess, par2,
							par3, par4, 4);
				}
			}

			renderFaceXNeg(par1Block, par2, par3, par4, var18);

			if (var8 && RenderBlocks.fancyGrass
					&& var18 == TextureUtils.iconGrassSide
					&& !hasOverrideBlockTexture()) {
				var10.setColorOpaque_F(var15 * par5, var16 * par6, var17 * par7);
				renderFaceXNeg(par1Block, par2, par3, par4,
						BlockGrass.getIconSideOverlay());
			}

			var11 = true;
		}

		if (renderAllFaces
				|| par1Block.shouldSideBeRendered(blockAccess, par2 + 1, par3,
						par4, 5)) {
			if (var12 < 0) {
				var12 = par1Block.getMixedBrightnessForBlock(blockAccess, par2,
						par3, par4);
			}

			var14 = 0.6F;
			var15 = var14;
			var16 = var14;
			var17 = var14;

			if (par1Block != Block.grass) {
				var15 = var14 * par5;
				var16 = var14 * par6;
				var17 = var14 * par7;
			}

			var10.setBrightness(renderMaxX < 1.0D ? var12 : par1Block
					.getMixedBrightnessForBlock(blockAccess, par2 + 1, par3,
							par4));
			var10.setColorOpaque_F(var15, var16, var17);
			var18 = this.getBlockIcon(par1Block, blockAccess, par2, par3, par4,
					5);

			if (var9) {
				if (var18 == TextureUtils.iconGrassSide
						|| var18 == TextureUtils.iconMycelSide) {
					var18 = Config.getSideGrassTexture(blockAccess, par2, par3,
							par4, 5, var18);

					if (var18 == TextureUtils.iconGrassTop) {
						var10.setColorOpaque_F(var15 * par5, var16 * par6,
								var17 * par7);
					}
				}

				if (var18 == TextureUtils.iconSnowSide) {
					var18 = Config.getSideSnowGrassTexture(blockAccess, par2,
							par3, par4, 5);
				}
			}

			renderFaceXPos(par1Block, par2, par3, par4, var18);

			if (var8 && RenderBlocks.fancyGrass
					&& var18 == TextureUtils.iconGrassSide
					&& !hasOverrideBlockTexture()) {
				var10.setColorOpaque_F(var15 * par5, var16 * par6, var17 * par7);
				renderFaceXPos(par1Block, par2, par3, par4,
						BlockGrass.getIconSideOverlay());
			}

			var11 = true;
		}

		return var11;
	}

	/**
	 * Renders a Cocoa block at the given coordinates
	 */
	public boolean renderBlockCocoa(final BlockCocoa par1BlockCocoa,
			final int par2, final int par3, final int par4) {
		final Tessellator var5 = Tessellator.instance;
		var5.setBrightness(par1BlockCocoa.getMixedBrightnessForBlock(
				blockAccess, par2, par3, par4));
		var5.setColorOpaque_F(1.0F, 1.0F, 1.0F);
		final int var6 = blockAccess.getBlockMetadata(par2, par3, par4);
		final int var7 = BlockDirectional.getDirection(var6);
		final int var8 = BlockCocoa.func_72219_c(var6);
		final Icon var9 = par1BlockCocoa.func_94468_i_(var8);
		final int var10 = 4 + var8 * 2;
		final int var11 = 5 + var8 * 2;
		final double var12 = 15.0D - var10;
		final double var14 = 15.0D;
		final double var16 = 4.0D;
		final double var18 = 4.0D + var11;
		double var20 = var9.getInterpolatedU(var12);
		double var22 = var9.getInterpolatedU(var14);
		double var24 = var9.getInterpolatedV(var16);
		double var26 = var9.getInterpolatedV(var18);
		double var28 = 0.0D;
		double var30 = 0.0D;

		switch (var7) {
		case 0:
			var28 = 8.0D - var10 / 2;
			var30 = 15.0D - var10;
			break;

		case 1:
			var28 = 1.0D;
			var30 = 8.0D - var10 / 2;
			break;

		case 2:
			var28 = 8.0D - var10 / 2;
			var30 = 1.0D;
			break;

		case 3:
			var28 = 15.0D - var10;
			var30 = 8.0D - var10 / 2;
		}

		double var32 = par2 + var28 / 16.0D;
		double var34 = par2 + (var28 + var10) / 16.0D;
		double var36 = par3 + (12.0D - var11) / 16.0D;
		double var38 = par3 + 0.75D;
		double var40 = par4 + var30 / 16.0D;
		double var42 = par4 + (var30 + var10) / 16.0D;
		var5.addVertexWithUV(var32, var36, var40, var20, var26);
		var5.addVertexWithUV(var32, var36, var42, var22, var26);
		var5.addVertexWithUV(var32, var38, var42, var22, var24);
		var5.addVertexWithUV(var32, var38, var40, var20, var24);
		var5.addVertexWithUV(var34, var36, var42, var20, var26);
		var5.addVertexWithUV(var34, var36, var40, var22, var26);
		var5.addVertexWithUV(var34, var38, var40, var22, var24);
		var5.addVertexWithUV(var34, var38, var42, var20, var24);
		var5.addVertexWithUV(var34, var36, var40, var20, var26);
		var5.addVertexWithUV(var32, var36, var40, var22, var26);
		var5.addVertexWithUV(var32, var38, var40, var22, var24);
		var5.addVertexWithUV(var34, var38, var40, var20, var24);
		var5.addVertexWithUV(var32, var36, var42, var20, var26);
		var5.addVertexWithUV(var34, var36, var42, var22, var26);
		var5.addVertexWithUV(var34, var38, var42, var22, var24);
		var5.addVertexWithUV(var32, var38, var42, var20, var24);
		int var44 = var10;

		if (var8 >= 2) {
			var44 = var10 - 1;
		}

		var20 = var9.getMinU();
		var22 = var9.getInterpolatedU(var44);
		var24 = var9.getMinV();
		var26 = var9.getInterpolatedV(var44);
		var5.addVertexWithUV(var32, var38, var42, var20, var26);
		var5.addVertexWithUV(var34, var38, var42, var22, var26);
		var5.addVertexWithUV(var34, var38, var40, var22, var24);
		var5.addVertexWithUV(var32, var38, var40, var20, var24);
		var5.addVertexWithUV(var32, var36, var40, var20, var24);
		var5.addVertexWithUV(var34, var36, var40, var22, var24);
		var5.addVertexWithUV(var34, var36, var42, var22, var26);
		var5.addVertexWithUV(var32, var36, var42, var20, var26);
		var20 = var9.getInterpolatedU(12.0D);
		var22 = var9.getMaxU();
		var24 = var9.getMinV();
		var26 = var9.getInterpolatedV(4.0D);
		var28 = 8.0D;
		var30 = 0.0D;
		double var45;

		switch (var7) {
		case 0:
			var28 = 8.0D;
			var30 = 12.0D;
			var45 = var20;
			var20 = var22;
			var22 = var45;
			break;

		case 1:
			var28 = 0.0D;
			var30 = 8.0D;
			break;

		case 2:
			var28 = 8.0D;
			var30 = 0.0D;
			break;

		case 3:
			var28 = 12.0D;
			var30 = 8.0D;
			var45 = var20;
			var20 = var22;
			var22 = var45;
		}

		var32 = par2 + var28 / 16.0D;
		var34 = par2 + (var28 + 4.0D) / 16.0D;
		var36 = par3 + 0.75D;
		var38 = par3 + 1.0D;
		var40 = par4 + var30 / 16.0D;
		var42 = par4 + (var30 + 4.0D) / 16.0D;

		if (var7 != 2 && var7 != 0) {
			if (var7 == 1 || var7 == 3) {
				var5.addVertexWithUV(var34, var36, var40, var20, var26);
				var5.addVertexWithUV(var32, var36, var40, var22, var26);
				var5.addVertexWithUV(var32, var38, var40, var22, var24);
				var5.addVertexWithUV(var34, var38, var40, var20, var24);
				var5.addVertexWithUV(var32, var36, var40, var22, var26);
				var5.addVertexWithUV(var34, var36, var40, var20, var26);
				var5.addVertexWithUV(var34, var38, var40, var20, var24);
				var5.addVertexWithUV(var32, var38, var40, var22, var24);
			}
		} else {
			var5.addVertexWithUV(var32, var36, var40, var22, var26);
			var5.addVertexWithUV(var32, var36, var42, var20, var26);
			var5.addVertexWithUV(var32, var38, var42, var20, var24);
			var5.addVertexWithUV(var32, var38, var40, var22, var24);
			var5.addVertexWithUV(var32, var36, var42, var20, var26);
			var5.addVertexWithUV(var32, var36, var40, var22, var26);
			var5.addVertexWithUV(var32, var38, var40, var22, var24);
			var5.addVertexWithUV(var32, var38, var42, var20, var24);
		}

		return true;
	}

	/**
	 * Renders beacon block
	 */
	public boolean renderBlockBeacon(final BlockBeacon par1BlockBeacon,
			final int par2, final int par3, final int par4) {
		final float var5 = 0.1875F;
		setOverrideBlockTexture(this.getBlockIcon(Block.obsidian));
		setRenderBounds(0.125D, 0.0062500000931322575D, 0.125D, 0.875D, var5,
				0.875D);
		renderStandardBlock(par1BlockBeacon, par2, par3, par4);
		setOverrideBlockTexture(this.getBlockIcon(Block.glass));
		setRenderBounds(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
		renderStandardBlock(par1BlockBeacon, par2, par3, par4);
		setOverrideBlockTexture(par1BlockBeacon.getBeaconIcon());
		setRenderBounds(0.1875D, var5, 0.1875D, 0.8125D, 0.875D, 0.8125D);
		renderStandardBlock(par1BlockBeacon, par2, par3, par4);
		clearOverrideBlockTexture();
		return true;
	}

	/**
	 * Renders a cactus block at the given coordinates
	 */
	public boolean renderBlockCactus(final Block par1Block, final int par2,
			final int par3, final int par4) {
		final int var5 = par1Block.colorMultiplier(blockAccess, par2, par3,
				par4);
		float var6 = (var5 >> 16 & 255) / 255.0F;
		float var7 = (var5 >> 8 & 255) / 255.0F;
		float var8 = (var5 & 255) / 255.0F;

		if (EntityRenderer.anaglyphEnable) {
			final float var9 = (var6 * 30.0F + var7 * 59.0F + var8 * 11.0F) / 100.0F;
			final float var10 = (var6 * 30.0F + var7 * 70.0F) / 100.0F;
			final float var11 = (var6 * 30.0F + var8 * 70.0F) / 100.0F;
			var6 = var9;
			var7 = var10;
			var8 = var11;
		}

		return renderBlockCactusImpl(par1Block, par2, par3, par4, var6, var7,
				var8);
	}

	/**
	 * Render block cactus implementation
	 */
	public boolean renderBlockCactusImpl(final Block par1Block, final int par2,
			final int par3, final int par4, final float par5, final float par6,
			final float par7) {
		final Tessellator var8 = Tessellator.instance;
		boolean var9 = false;
		final float var10 = 0.5F;
		final float var11 = 1.0F;
		final float var12 = 0.8F;
		final float var13 = 0.6F;
		final float var14 = var10 * par5;
		final float var15 = var11 * par5;
		final float var16 = var12 * par5;
		final float var17 = var13 * par5;
		final float var18 = var10 * par6;
		final float var19 = var11 * par6;
		final float var20 = var12 * par6;
		final float var21 = var13 * par6;
		final float var22 = var10 * par7;
		final float var23 = var11 * par7;
		final float var24 = var12 * par7;
		final float var25 = var13 * par7;
		final float var26 = 0.0625F;
		final int var27 = par1Block.getMixedBrightnessForBlock(blockAccess,
				par2, par3, par4);

		if (renderAllFaces
				|| par1Block.shouldSideBeRendered(blockAccess, par2, par3 - 1,
						par4, 0)) {
			var8.setBrightness(renderMinY > 0.0D ? var27 : par1Block
					.getMixedBrightnessForBlock(blockAccess, par2, par3 - 1,
							par4));
			var8.setColorOpaque_F(var14, var18, var22);
			renderFaceYNeg(par1Block, par2, par3, par4, this.getBlockIcon(
					par1Block, blockAccess, par2, par3, par4, 0));
			var9 = true;
		}

		if (renderAllFaces
				|| par1Block.shouldSideBeRendered(blockAccess, par2, par3 + 1,
						par4, 1)) {
			var8.setBrightness(renderMaxY < 1.0D ? var27 : par1Block
					.getMixedBrightnessForBlock(blockAccess, par2, par3 + 1,
							par4));
			var8.setColorOpaque_F(var15, var19, var23);
			renderFaceYPos(par1Block, par2, par3, par4, this.getBlockIcon(
					par1Block, blockAccess, par2, par3, par4, 1));
			var9 = true;
		}

		if (renderAllFaces
				|| par1Block.shouldSideBeRendered(blockAccess, par2, par3,
						par4 - 1, 2)) {
			var8.setBrightness(renderMinZ > 0.0D ? var27 : par1Block
					.getMixedBrightnessForBlock(blockAccess, par2, par3,
							par4 - 1));
			var8.setColorOpaque_F(var16, var20, var24);
			var8.addTranslation(0.0F, 0.0F, var26);
			renderFaceZNeg(par1Block, par2, par3, par4, this.getBlockIcon(
					par1Block, blockAccess, par2, par3, par4, 2));
			var8.addTranslation(0.0F, 0.0F, -var26);
			var9 = true;
		}

		if (renderAllFaces
				|| par1Block.shouldSideBeRendered(blockAccess, par2, par3,
						par4 + 1, 3)) {
			var8.setBrightness(renderMaxZ < 1.0D ? var27 : par1Block
					.getMixedBrightnessForBlock(blockAccess, par2, par3,
							par4 + 1));
			var8.setColorOpaque_F(var16, var20, var24);
			var8.addTranslation(0.0F, 0.0F, -var26);
			renderFaceZPos(par1Block, par2, par3, par4, this.getBlockIcon(
					par1Block, blockAccess, par2, par3, par4, 3));
			var8.addTranslation(0.0F, 0.0F, var26);
			var9 = true;
		}

		if (renderAllFaces
				|| par1Block.shouldSideBeRendered(blockAccess, par2 - 1, par3,
						par4, 4)) {
			var8.setBrightness(renderMinX > 0.0D ? var27 : par1Block
					.getMixedBrightnessForBlock(blockAccess, par2 - 1, par3,
							par4));
			var8.setColorOpaque_F(var17, var21, var25);
			var8.addTranslation(var26, 0.0F, 0.0F);
			renderFaceXNeg(par1Block, par2, par3, par4, this.getBlockIcon(
					par1Block, blockAccess, par2, par3, par4, 4));
			var8.addTranslation(-var26, 0.0F, 0.0F);
			var9 = true;
		}

		if (renderAllFaces
				|| par1Block.shouldSideBeRendered(blockAccess, par2 + 1, par3,
						par4, 5)) {
			var8.setBrightness(renderMaxX < 1.0D ? var27 : par1Block
					.getMixedBrightnessForBlock(blockAccess, par2 + 1, par3,
							par4));
			var8.setColorOpaque_F(var17, var21, var25);
			var8.addTranslation(-var26, 0.0F, 0.0F);
			renderFaceXPos(par1Block, par2, par3, par4, this.getBlockIcon(
					par1Block, blockAccess, par2, par3, par4, 5));
			var8.addTranslation(var26, 0.0F, 0.0F);
			var9 = true;
		}

		return var9;
	}

	public boolean renderBlockFence(final BlockFence par1BlockFence,
			final int par2, final int par3, final int par4) {
		boolean var5 = false;
		float var6 = 0.375F;
		float var7 = 0.625F;
		setRenderBounds(var6, 0.0D, var6, var7, 1.0D, var7);
		renderStandardBlock(par1BlockFence, par2, par3, par4);
		var5 = true;
		boolean var8 = false;
		boolean var9 = false;

		if (par1BlockFence.canConnectFenceTo(blockAccess, par2 - 1, par3, par4)
				|| par1BlockFence.canConnectFenceTo(blockAccess, par2 + 1,
						par3, par4)) {
			var8 = true;
		}

		if (par1BlockFence.canConnectFenceTo(blockAccess, par2, par3, par4 - 1)
				|| par1BlockFence.canConnectFenceTo(blockAccess, par2, par3,
						par4 + 1)) {
			var9 = true;
		}

		final boolean var10 = par1BlockFence.canConnectFenceTo(blockAccess,
				par2 - 1, par3, par4);
		final boolean var11 = par1BlockFence.canConnectFenceTo(blockAccess,
				par2 + 1, par3, par4);
		final boolean var12 = par1BlockFence.canConnectFenceTo(blockAccess,
				par2, par3, par4 - 1);
		final boolean var13 = par1BlockFence.canConnectFenceTo(blockAccess,
				par2, par3, par4 + 1);

		if (!var8 && !var9) {
			var8 = true;
		}

		var6 = 0.4375F;
		var7 = 0.5625F;
		float var14 = 0.75F;
		float var15 = 0.9375F;
		final float var16 = var10 ? 0.0F : var6;
		final float var17 = var11 ? 1.0F : var7;
		final float var18 = var12 ? 0.0F : var6;
		final float var19 = var13 ? 1.0F : var7;

		if (var8) {
			setRenderBounds(var16, var14, var6, var17, var15, var7);
			renderStandardBlock(par1BlockFence, par2, par3, par4);
			var5 = true;
		}

		if (var9) {
			setRenderBounds(var6, var14, var18, var7, var15, var19);
			renderStandardBlock(par1BlockFence, par2, par3, par4);
			var5 = true;
		}

		var14 = 0.375F;
		var15 = 0.5625F;

		if (var8) {
			setRenderBounds(var16, var14, var6, var17, var15, var7);
			renderStandardBlock(par1BlockFence, par2, par3, par4);
			var5 = true;
		}

		if (var9) {
			setRenderBounds(var6, var14, var18, var7, var15, var19);
			renderStandardBlock(par1BlockFence, par2, par3, par4);
			var5 = true;
		}

		par1BlockFence
				.setBlockBoundsBasedOnState(blockAccess, par2, par3, par4);

		if (Config.isBetterSnow() && hasSnowNeighbours(par2, par3, par4)) {
			renderSnow(par2, par3, par4, Block.snow.maxY);
		}

		return var5;
	}

	/**
	 * Renders wall block
	 */
	public boolean renderBlockWall(final BlockWall par1BlockWall,
			final int par2, final int par3, final int par4) {
		final boolean var5 = par1BlockWall.canConnectWallTo(blockAccess,
				par2 - 1, par3, par4);
		final boolean var6 = par1BlockWall.canConnectWallTo(blockAccess,
				par2 + 1, par3, par4);
		final boolean var7 = par1BlockWall.canConnectWallTo(blockAccess, par2,
				par3, par4 - 1);
		final boolean var8 = par1BlockWall.canConnectWallTo(blockAccess, par2,
				par3, par4 + 1);
		final boolean var9 = var7 && var8 && !var5 && !var6;
		final boolean var10 = !var7 && !var8 && var5 && var6;
		final boolean var11 = blockAccess.isAirBlock(par2, par3 + 1, par4);

		if ((var9 || var10) && var11) {
			if (var9) {
				setRenderBounds(0.3125D, 0.0D, 0.0D, 0.6875D, 0.8125D, 1.0D);
				renderStandardBlock(par1BlockWall, par2, par3, par4);
			} else {
				setRenderBounds(0.0D, 0.0D, 0.3125D, 1.0D, 0.8125D, 0.6875D);
				renderStandardBlock(par1BlockWall, par2, par3, par4);
			}
		} else {
			setRenderBounds(0.25D, 0.0D, 0.25D, 0.75D, 1.0D, 0.75D);
			renderStandardBlock(par1BlockWall, par2, par3, par4);

			if (var5) {
				setRenderBounds(0.0D, 0.0D, 0.3125D, 0.25D, 0.8125D, 0.6875D);
				renderStandardBlock(par1BlockWall, par2, par3, par4);
			}

			if (var6) {
				setRenderBounds(0.75D, 0.0D, 0.3125D, 1.0D, 0.8125D, 0.6875D);
				renderStandardBlock(par1BlockWall, par2, par3, par4);
			}

			if (var7) {
				setRenderBounds(0.3125D, 0.0D, 0.0D, 0.6875D, 0.8125D, 0.25D);
				renderStandardBlock(par1BlockWall, par2, par3, par4);
			}

			if (var8) {
				setRenderBounds(0.3125D, 0.0D, 0.75D, 0.6875D, 0.8125D, 1.0D);
				renderStandardBlock(par1BlockWall, par2, par3, par4);
			}
		}

		par1BlockWall.setBlockBoundsBasedOnState(blockAccess, par2, par3, par4);

		if (Config.isBetterSnow() && hasSnowNeighbours(par2, par3, par4)) {
			renderSnow(par2, par3, par4, Block.snow.maxY);
		}

		return true;
	}

	public boolean renderBlockDragonEgg(
			final BlockDragonEgg par1BlockDragonEgg, final int par2,
			final int par3, final int par4) {
		boolean var5 = false;
		int var6 = 0;

		for (int var7 = 0; var7 < 8; ++var7) {
			byte var8 = 0;
			byte var9 = 1;

			if (var7 == 0) {
				var8 = 2;
			}

			if (var7 == 1) {
				var8 = 3;
			}

			if (var7 == 2) {
				var8 = 4;
			}

			if (var7 == 3) {
				var8 = 5;
				var9 = 2;
			}

			if (var7 == 4) {
				var8 = 6;
				var9 = 3;
			}

			if (var7 == 5) {
				var8 = 7;
				var9 = 5;
			}

			if (var7 == 6) {
				var8 = 6;
				var9 = 2;
			}

			if (var7 == 7) {
				var8 = 3;
			}

			final float var10 = var8 / 16.0F;
			final float var11 = 1.0F - var6 / 16.0F;
			final float var12 = 1.0F - (var6 + var9) / 16.0F;
			var6 += var9;
			setRenderBounds(0.5F - var10, var12, 0.5F - var10, 0.5F + var10,
					var11, 0.5F + var10);
			renderStandardBlock(par1BlockDragonEgg, par2, par3, par4);
		}

		var5 = true;
		setRenderBounds(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
		return var5;
	}

	/**
	 * Render block fence gate
	 */
	public boolean renderBlockFenceGate(
			final BlockFenceGate par1BlockFenceGate, final int par2,
			final int par3, final int par4) {
		final boolean var5 = true;
		final int var6 = blockAccess.getBlockMetadata(par2, par3, par4);
		final boolean var7 = BlockFenceGate.isFenceGateOpen(var6);
		final int var8 = BlockDirectional.getDirection(var6);
		float var9 = 0.375F;
		float var10 = 0.5625F;
		float var11 = 0.75F;
		float var12 = 0.9375F;
		float var13 = 0.3125F;
		float var14 = 1.0F;

		if ((var8 == 2 || var8 == 0)
				&& blockAccess.getBlockId(par2 - 1, par3, par4) == Block.cobblestoneWall.blockID
				&& blockAccess.getBlockId(par2 + 1, par3, par4) == Block.cobblestoneWall.blockID
				|| (var8 == 3 || var8 == 1)
				&& blockAccess.getBlockId(par2, par3, par4 - 1) == Block.cobblestoneWall.blockID
				&& blockAccess.getBlockId(par2, par3, par4 + 1) == Block.cobblestoneWall.blockID) {
			var9 -= 0.1875F;
			var10 -= 0.1875F;
			var11 -= 0.1875F;
			var12 -= 0.1875F;
			var13 -= 0.1875F;
			var14 -= 0.1875F;
		}

		renderAllFaces = true;
		float var15;
		float var17;
		float var16;
		float var18;

		if (var8 != 3 && var8 != 1) {
			var15 = 0.0F;
			var17 = 0.125F;
			var16 = 0.4375F;
			var18 = 0.5625F;
			setRenderBounds(var15, var13, var16, var17, var14, var18);
			renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
			var15 = 0.875F;
			var17 = 1.0F;
			setRenderBounds(var15, var13, var16, var17, var14, var18);
			renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
		} else {
			uvRotateTop = 1;
			var15 = 0.4375F;
			var17 = 0.5625F;
			var16 = 0.0F;
			var18 = 0.125F;
			setRenderBounds(var15, var13, var16, var17, var14, var18);
			renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
			var16 = 0.875F;
			var18 = 1.0F;
			setRenderBounds(var15, var13, var16, var17, var14, var18);
			renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
			uvRotateTop = 0;
		}

		if (var7) {
			if (var8 == 2 || var8 == 0) {
				uvRotateTop = 1;
			}

			if (var8 == 3) {
				setRenderBounds(0.8125D, var9, 0.0D, 0.9375D, var12, 0.125D);
				renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
				setRenderBounds(0.8125D, var9, 0.875D, 0.9375D, var12, 1.0D);
				renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
				setRenderBounds(0.5625D, var9, 0.0D, 0.8125D, var10, 0.125D);
				renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
				setRenderBounds(0.5625D, var9, 0.875D, 0.8125D, var10, 1.0D);
				renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
				setRenderBounds(0.5625D, var11, 0.0D, 0.8125D, var12, 0.125D);
				renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
				setRenderBounds(0.5625D, var11, 0.875D, 0.8125D, var12, 1.0D);
				renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
			} else if (var8 == 1) {
				setRenderBounds(0.0625D, var9, 0.0D, 0.1875D, var12, 0.125D);
				renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
				setRenderBounds(0.0625D, var9, 0.875D, 0.1875D, var12, 1.0D);
				renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
				setRenderBounds(0.1875D, var9, 0.0D, 0.4375D, var10, 0.125D);
				renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
				setRenderBounds(0.1875D, var9, 0.875D, 0.4375D, var10, 1.0D);
				renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
				setRenderBounds(0.1875D, var11, 0.0D, 0.4375D, var12, 0.125D);
				renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
				setRenderBounds(0.1875D, var11, 0.875D, 0.4375D, var12, 1.0D);
				renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
			} else if (var8 == 0) {
				setRenderBounds(0.0D, var9, 0.8125D, 0.125D, var12, 0.9375D);
				renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
				setRenderBounds(0.875D, var9, 0.8125D, 1.0D, var12, 0.9375D);
				renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
				setRenderBounds(0.0D, var9, 0.5625D, 0.125D, var10, 0.8125D);
				renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
				setRenderBounds(0.875D, var9, 0.5625D, 1.0D, var10, 0.8125D);
				renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
				setRenderBounds(0.0D, var11, 0.5625D, 0.125D, var12, 0.8125D);
				renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
				setRenderBounds(0.875D, var11, 0.5625D, 1.0D, var12, 0.8125D);
				renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
			} else if (var8 == 2) {
				setRenderBounds(0.0D, var9, 0.0625D, 0.125D, var12, 0.1875D);
				renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
				setRenderBounds(0.875D, var9, 0.0625D, 1.0D, var12, 0.1875D);
				renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
				setRenderBounds(0.0D, var9, 0.1875D, 0.125D, var10, 0.4375D);
				renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
				setRenderBounds(0.875D, var9, 0.1875D, 1.0D, var10, 0.4375D);
				renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
				setRenderBounds(0.0D, var11, 0.1875D, 0.125D, var12, 0.4375D);
				renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
				setRenderBounds(0.875D, var11, 0.1875D, 1.0D, var12, 0.4375D);
				renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
			}
		} else if (var8 != 3 && var8 != 1) {
			var15 = 0.375F;
			var17 = 0.5F;
			var16 = 0.4375F;
			var18 = 0.5625F;
			setRenderBounds(var15, var9, var16, var17, var12, var18);
			renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
			var15 = 0.5F;
			var17 = 0.625F;
			setRenderBounds(var15, var9, var16, var17, var12, var18);
			renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
			var15 = 0.625F;
			var17 = 0.875F;
			setRenderBounds(var15, var9, var16, var17, var10, var18);
			renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
			setRenderBounds(var15, var11, var16, var17, var12, var18);
			renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
			var15 = 0.125F;
			var17 = 0.375F;
			setRenderBounds(var15, var9, var16, var17, var10, var18);
			renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
			setRenderBounds(var15, var11, var16, var17, var12, var18);
			renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
		} else {
			uvRotateTop = 1;
			var15 = 0.4375F;
			var17 = 0.5625F;
			var16 = 0.375F;
			var18 = 0.5F;
			setRenderBounds(var15, var9, var16, var17, var12, var18);
			renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
			var16 = 0.5F;
			var18 = 0.625F;
			setRenderBounds(var15, var9, var16, var17, var12, var18);
			renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
			var16 = 0.625F;
			var18 = 0.875F;
			setRenderBounds(var15, var9, var16, var17, var10, var18);
			renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
			setRenderBounds(var15, var11, var16, var17, var12, var18);
			renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
			var16 = 0.125F;
			var18 = 0.375F;
			setRenderBounds(var15, var9, var16, var17, var10, var18);
			renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
			setRenderBounds(var15, var11, var16, var17, var12, var18);
			renderStandardBlock(par1BlockFenceGate, par2, par3, par4);
		}

		if (Config.isBetterSnow() && hasSnowNeighbours(par2, par3, par4)) {
			renderSnow(par2, par3, par4, Block.snow.maxY);
		}

		renderAllFaces = false;
		uvRotateTop = 0;
		setRenderBounds(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
		return var5;
	}

	public boolean renderBlockHopper(final BlockHopper par1BlockHopper,
			final int par2, final int par3, final int par4) {
		final Tessellator var5 = Tessellator.instance;
		var5.setBrightness(par1BlockHopper.getMixedBrightnessForBlock(
				blockAccess, par2, par3, par4));
		final float var6 = 1.0F;
		final int var7 = par1BlockHopper.colorMultiplier(blockAccess, par2,
				par3, par4);
		float var8 = (var7 >> 16 & 255) / 255.0F;
		float var9 = (var7 >> 8 & 255) / 255.0F;
		float var10 = (var7 & 255) / 255.0F;

		if (EntityRenderer.anaglyphEnable) {
			final float var11 = (var8 * 30.0F + var9 * 59.0F + var10 * 11.0F) / 100.0F;
			final float var12 = (var8 * 30.0F + var9 * 70.0F) / 100.0F;
			final float var13 = (var8 * 30.0F + var10 * 70.0F) / 100.0F;
			var8 = var11;
			var9 = var12;
			var10 = var13;
		}

		var5.setColorOpaque_F(var6 * var8, var6 * var9, var6 * var10);
		return renderBlockHopperMetadata(par1BlockHopper, par2, par3, par4,
				blockAccess.getBlockMetadata(par2, par3, par4), false);
	}

	public boolean renderBlockHopperMetadata(final BlockHopper par1BlockHopper,
			final int par2, final int par3, final int par4, final int par5,
			final boolean par6) {
		final Tessellator var7 = Tessellator.instance;
		final int var8 = BlockHopper.getDirectionFromMetadata(par5);
		final double var9 = 0.625D;
		setRenderBounds(0.0D, var9, 0.0D, 1.0D, 1.0D, 1.0D);

		if (par6) {
			var7.startDrawingQuads();
			var7.setNormal(0.0F, -1.0F, 0.0F);
			renderFaceYNeg(par1BlockHopper, 0.0D, 0.0D, 0.0D,
					getBlockIconFromSideAndMetadata(par1BlockHopper, 0, par5));
			var7.draw();
			var7.startDrawingQuads();
			var7.setNormal(0.0F, 1.0F, 0.0F);
			renderFaceYPos(par1BlockHopper, 0.0D, 0.0D, 0.0D,
					getBlockIconFromSideAndMetadata(par1BlockHopper, 1, par5));
			var7.draw();
			var7.startDrawingQuads();
			var7.setNormal(0.0F, 0.0F, -1.0F);
			renderFaceZNeg(par1BlockHopper, 0.0D, 0.0D, 0.0D,
					getBlockIconFromSideAndMetadata(par1BlockHopper, 2, par5));
			var7.draw();
			var7.startDrawingQuads();
			var7.setNormal(0.0F, 0.0F, 1.0F);
			renderFaceZPos(par1BlockHopper, 0.0D, 0.0D, 0.0D,
					getBlockIconFromSideAndMetadata(par1BlockHopper, 3, par5));
			var7.draw();
			var7.startDrawingQuads();
			var7.setNormal(-1.0F, 0.0F, 0.0F);
			renderFaceXNeg(par1BlockHopper, 0.0D, 0.0D, 0.0D,
					getBlockIconFromSideAndMetadata(par1BlockHopper, 4, par5));
			var7.draw();
			var7.startDrawingQuads();
			var7.setNormal(1.0F, 0.0F, 0.0F);
			renderFaceXPos(par1BlockHopper, 0.0D, 0.0D, 0.0D,
					getBlockIconFromSideAndMetadata(par1BlockHopper, 5, par5));
			var7.draw();
		} else {
			renderStandardBlock(par1BlockHopper, par2, par3, par4);
		}

		float var11;

		if (!par6) {
			var7.setBrightness(par1BlockHopper.getMixedBrightnessForBlock(
					blockAccess, par2, par3, par4));
			final float var12 = 1.0F;
			final int var13 = par1BlockHopper.colorMultiplier(blockAccess,
					par2, par3, par4);
			var11 = (var13 >> 16 & 255) / 255.0F;
			float var14 = (var13 >> 8 & 255) / 255.0F;
			float var15 = (var13 & 255) / 255.0F;

			if (EntityRenderer.anaglyphEnable) {
				final float var16 = (var11 * 30.0F + var14 * 59.0F + var15 * 11.0F) / 100.0F;
				final float var17 = (var11 * 30.0F + var14 * 70.0F) / 100.0F;
				final float var18 = (var11 * 30.0F + var15 * 70.0F) / 100.0F;
				var11 = var16;
				var14 = var17;
				var15 = var18;
			}

			var7.setColorOpaque_F(var12 * var11, var12 * var14, var12 * var15);
		}

		final Icon var22 = BlockHopper.getHopperIcon("hopper");
		final Icon var23 = BlockHopper.getHopperIcon("hopper_inside");
		var11 = 0.125F;

		if (par6) {
			var7.startDrawingQuads();
			var7.setNormal(1.0F, 0.0F, 0.0F);
			renderFaceXPos(par1BlockHopper, -1.0F + var11, 0.0D, 0.0D, var22);
			var7.draw();
			var7.startDrawingQuads();
			var7.setNormal(-1.0F, 0.0F, 0.0F);
			renderFaceXNeg(par1BlockHopper, 1.0F - var11, 0.0D, 0.0D, var22);
			var7.draw();
			var7.startDrawingQuads();
			var7.setNormal(0.0F, 0.0F, 1.0F);
			renderFaceZPos(par1BlockHopper, 0.0D, 0.0D, -1.0F + var11, var22);
			var7.draw();
			var7.startDrawingQuads();
			var7.setNormal(0.0F, 0.0F, -1.0F);
			renderFaceZNeg(par1BlockHopper, 0.0D, 0.0D, 1.0F - var11, var22);
			var7.draw();
			var7.startDrawingQuads();
			var7.setNormal(0.0F, 1.0F, 0.0F);
			renderFaceYPos(par1BlockHopper, 0.0D, -1.0D + var9, 0.0D, var23);
			var7.draw();
		} else {
			renderFaceXPos(par1BlockHopper, par2 - 1.0F + var11, par3, par4,
					var22);
			renderFaceXNeg(par1BlockHopper, par2 + 1.0F - var11, par3, par4,
					var22);
			renderFaceZPos(par1BlockHopper, par2, par3, par4 - 1.0F + var11,
					var22);
			renderFaceZNeg(par1BlockHopper, par2, par3, par4 + 1.0F - var11,
					var22);
			renderFaceYPos(par1BlockHopper, par2, par3 - 1.0F + var9, par4,
					var23);
		}

		setOverrideBlockTexture(var22);
		final double var24 = 0.25D;
		final double var25 = 0.25D;
		setRenderBounds(var24, var25, var24, 1.0D - var24, var9 - 0.002D,
				1.0D - var24);

		if (par6) {
			var7.startDrawingQuads();
			var7.setNormal(1.0F, 0.0F, 0.0F);
			renderFaceXPos(par1BlockHopper, 0.0D, 0.0D, 0.0D, var22);
			var7.draw();
			var7.startDrawingQuads();
			var7.setNormal(-1.0F, 0.0F, 0.0F);
			renderFaceXNeg(par1BlockHopper, 0.0D, 0.0D, 0.0D, var22);
			var7.draw();
			var7.startDrawingQuads();
			var7.setNormal(0.0F, 0.0F, 1.0F);
			renderFaceZPos(par1BlockHopper, 0.0D, 0.0D, 0.0D, var22);
			var7.draw();
			var7.startDrawingQuads();
			var7.setNormal(0.0F, 0.0F, -1.0F);
			renderFaceZNeg(par1BlockHopper, 0.0D, 0.0D, 0.0D, var22);
			var7.draw();
			var7.startDrawingQuads();
			var7.setNormal(0.0F, 1.0F, 0.0F);
			renderFaceYPos(par1BlockHopper, 0.0D, 0.0D, 0.0D, var22);
			var7.draw();
			var7.startDrawingQuads();
			var7.setNormal(0.0F, -1.0F, 0.0F);
			renderFaceYNeg(par1BlockHopper, 0.0D, 0.0D, 0.0D, var22);
			var7.draw();
		} else {
			renderStandardBlock(par1BlockHopper, par2, par3, par4);
		}

		if (!par6) {
			final double var26 = 0.375D;
			final double var20 = 0.25D;
			setOverrideBlockTexture(var22);

			if (var8 == 0) {
				setRenderBounds(var26, 0.0D, var26, 1.0D - var26, 0.25D,
						1.0D - var26);
				renderStandardBlock(par1BlockHopper, par2, par3, par4);
			}

			if (var8 == 2) {
				setRenderBounds(var26, var25, 0.0D, 1.0D - var26,
						var25 + var20, var24);
				renderStandardBlock(par1BlockHopper, par2, par3, par4);
			}

			if (var8 == 3) {
				setRenderBounds(var26, var25, 1.0D - var24, 1.0D - var26, var25
						+ var20, 1.0D);
				renderStandardBlock(par1BlockHopper, par2, par3, par4);
			}

			if (var8 == 4) {
				setRenderBounds(0.0D, var25, var26, var24, var25 + var20,
						1.0D - var26);
				renderStandardBlock(par1BlockHopper, par2, par3, par4);
			}

			if (var8 == 5) {
				setRenderBounds(1.0D - var24, var25, var26, 1.0D,
						var25 + var20, 1.0D - var26);
				renderStandardBlock(par1BlockHopper, par2, par3, par4);
			}
		}

		clearOverrideBlockTexture();
		return true;
	}

	/**
	 * Renders a stair block at the given coordinates
	 */
	public boolean renderBlockStairs(final BlockStairs par1BlockStairs,
			final int par2, final int par3, final int par4) {
		par1BlockStairs.func_82541_d(blockAccess, par2, par3, par4);
		setRenderBoundsFromBlock(par1BlockStairs);
		renderStandardBlock(par1BlockStairs, par2, par3, par4);
		final boolean var5 = par1BlockStairs.func_82542_g(blockAccess, par2,
				par3, par4);
		setRenderBoundsFromBlock(par1BlockStairs);
		renderStandardBlock(par1BlockStairs, par2, par3, par4);

		if (var5 && par1BlockStairs.func_82544_h(blockAccess, par2, par3, par4)) {
			setRenderBoundsFromBlock(par1BlockStairs);
			renderStandardBlock(par1BlockStairs, par2, par3, par4);
		}

		return true;
	}

	/**
	 * Renders a door block at the given coordinates
	 */
	public boolean renderBlockDoor(final Block par1Block, final int par2,
			final int par3, final int par4) {
		final Tessellator var5 = Tessellator.instance;
		final int var6 = blockAccess.getBlockMetadata(par2, par3, par4);

		if ((var6 & 8) != 0) {
			if (blockAccess.getBlockId(par2, par3 - 1, par4) != par1Block.blockID) {
				return false;
			}
		} else if (blockAccess.getBlockId(par2, par3 + 1, par4) != par1Block.blockID) {
			return false;
		}

		boolean var7 = false;
		final float var8 = 0.5F;
		final float var9 = 1.0F;
		final float var10 = 0.8F;
		final float var11 = 0.6F;
		final int var12 = par1Block.getMixedBrightnessForBlock(blockAccess,
				par2, par3, par4);
		var5.setBrightness(renderMinY > 0.0D ? var12 : par1Block
				.getMixedBrightnessForBlock(blockAccess, par2, par3 - 1, par4));
		var5.setColorOpaque_F(var8, var8, var8);
		renderFaceYNeg(par1Block, par2, par3, par4,
				this.getBlockIcon(par1Block, blockAccess, par2, par3, par4, 0));
		var7 = true;
		var5.setBrightness(renderMaxY < 1.0D ? var12 : par1Block
				.getMixedBrightnessForBlock(blockAccess, par2, par3 + 1, par4));
		var5.setColorOpaque_F(var9, var9, var9);
		renderFaceYPos(par1Block, par2, par3, par4,
				this.getBlockIcon(par1Block, blockAccess, par2, par3, par4, 1));
		var7 = true;
		var5.setBrightness(renderMinZ > 0.0D ? var12 : par1Block
				.getMixedBrightnessForBlock(blockAccess, par2, par3, par4 - 1));
		var5.setColorOpaque_F(var10, var10, var10);
		Icon var13 = this.getBlockIcon(par1Block, blockAccess, par2, par3,
				par4, 2);
		renderFaceZNeg(par1Block, par2, par3, par4, var13);
		var7 = true;
		flipTexture = false;
		var5.setBrightness(renderMaxZ < 1.0D ? var12 : par1Block
				.getMixedBrightnessForBlock(blockAccess, par2, par3, par4 + 1));
		var5.setColorOpaque_F(var10, var10, var10);
		var13 = this.getBlockIcon(par1Block, blockAccess, par2, par3, par4, 3);
		renderFaceZPos(par1Block, par2, par3, par4, var13);
		var7 = true;
		flipTexture = false;
		var5.setBrightness(renderMinX > 0.0D ? var12 : par1Block
				.getMixedBrightnessForBlock(blockAccess, par2 - 1, par3, par4));
		var5.setColorOpaque_F(var11, var11, var11);
		var13 = this.getBlockIcon(par1Block, blockAccess, par2, par3, par4, 4);
		renderFaceXNeg(par1Block, par2, par3, par4, var13);
		var7 = true;
		flipTexture = false;
		var5.setBrightness(renderMaxX < 1.0D ? var12 : par1Block
				.getMixedBrightnessForBlock(blockAccess, par2 + 1, par3, par4));
		var5.setColorOpaque_F(var11, var11, var11);
		var13 = this.getBlockIcon(par1Block, blockAccess, par2, par3, par4, 5);
		renderFaceXPos(par1Block, par2, par3, par4, var13);
		var7 = true;
		flipTexture = false;
		return var7;
	}

	/**
	 * Renders the given texture to the bottom face of the block. Args: block,
	 * x, y, z, texture
	 */
	public void renderFaceYNeg(final Block par1Block, final double par2,
			final double par4, final double par6, Icon par8Icon) {
		final Tessellator var9 = Tessellator.instance;

		if (hasOverrideBlockTexture()) {
			par8Icon = overrideBlockTexture;
		}

		if (Config.isConnectedTextures() && overrideBlockTexture == null
				&& uvRotateBottom == 0) {
			par8Icon = ConnectedTextures.getConnectedTexture(blockAccess,
					par1Block, (int) par2, (int) par4, (int) par6, 0, par8Icon);
		}

		boolean var10 = false;

		if (Config.isNaturalTextures() && overrideBlockTexture == null
				&& uvRotateBottom == 0) {
			final NaturalProperties var11 = NaturalTextures
					.getNaturalProperties(par8Icon);

			if (var11 != null) {
				final int var12 = Config.getRandom((int) par2, (int) par4,
						(int) par6, 0);

				if (var11.rotation > 1) {
					uvRotateBottom = var12 & 3;
				}

				if (var11.rotation == 2) {
					uvRotateBottom = uvRotateBottom / 2 * 3;
				}

				if (var11.flip) {
					flipTexture = (var12 & 4) != 0;
				}

				var10 = true;
			}
		}

		double var37 = par8Icon.getInterpolatedU(renderMinX * 16.0D);
		double var13 = par8Icon.getInterpolatedU(renderMaxX * 16.0D);
		double var15 = par8Icon.getInterpolatedV(renderMinZ * 16.0D);
		double var17 = par8Icon.getInterpolatedV(renderMaxZ * 16.0D);

		if (renderMinX < 0.0D || renderMaxX > 1.0D) {
			var37 = par8Icon.getMinU();
			var13 = par8Icon.getMaxU();
		}

		if (renderMinZ < 0.0D || renderMaxZ > 1.0D) {
			var15 = par8Icon.getMinV();
			var17 = par8Icon.getMaxV();
		}

		double var19;

		if (flipTexture) {
			var19 = var37;
			var37 = var13;
			var13 = var19;
		}

		var19 = var13;
		double var21 = var37;
		double var23 = var15;
		double var25 = var17;
		double var27;

		if (uvRotateBottom == 2) {
			var37 = par8Icon.getInterpolatedU(renderMinZ * 16.0D);
			var15 = par8Icon.getInterpolatedV(16.0D - renderMaxX * 16.0D);
			var13 = par8Icon.getInterpolatedU(renderMaxZ * 16.0D);
			var17 = par8Icon.getInterpolatedV(16.0D - renderMinX * 16.0D);

			if (flipTexture) {
				var27 = var37;
				var37 = var13;
				var13 = var27;
			}

			var23 = var15;
			var25 = var17;
			var19 = var37;
			var21 = var13;
			var15 = var17;
			var17 = var23;
		} else if (uvRotateBottom == 1) {
			var37 = par8Icon.getInterpolatedU(16.0D - renderMaxZ * 16.0D);
			var15 = par8Icon.getInterpolatedV(renderMinX * 16.0D);
			var13 = par8Icon.getInterpolatedU(16.0D - renderMinZ * 16.0D);
			var17 = par8Icon.getInterpolatedV(renderMaxX * 16.0D);

			if (flipTexture) {
				var27 = var37;
				var37 = var13;
				var13 = var27;
			}

			var19 = var13;
			var21 = var37;
			var37 = var13;
			var13 = var21;
			var23 = var17;
			var25 = var15;
		} else if (uvRotateBottom == 3) {
			var37 = par8Icon.getInterpolatedU(16.0D - renderMinX * 16.0D);
			var13 = par8Icon.getInterpolatedU(16.0D - renderMaxX * 16.0D);
			var15 = par8Icon.getInterpolatedV(16.0D - renderMinZ * 16.0D);
			var17 = par8Icon.getInterpolatedV(16.0D - renderMaxZ * 16.0D);

			if (flipTexture) {
				var27 = var37;
				var37 = var13;
				var13 = var27;
			}

			var19 = var13;
			var21 = var37;
			var23 = var15;
			var25 = var17;
		}

		if (var10) {
			uvRotateBottom = 0;
			flipTexture = false;
		}

		var27 = par2 + renderMinX;
		final double var29 = par2 + renderMaxX;
		final double var31 = par4 + renderMinY;
		final double var33 = par6 + renderMinZ;
		final double var35 = par6 + renderMaxZ;

		if (enableAO) {
			var9.setColorOpaque_F(colorRedTopLeft, colorGreenTopLeft,
					colorBlueTopLeft);
			var9.setBrightness(brightnessTopLeft);
			var9.addVertexWithUV(var27, var31, var35, var21, var25);
			var9.setColorOpaque_F(colorRedBottomLeft, colorGreenBottomLeft,
					colorBlueBottomLeft);
			var9.setBrightness(brightnessBottomLeft);
			var9.addVertexWithUV(var27, var31, var33, var37, var15);
			var9.setColorOpaque_F(colorRedBottomRight, colorGreenBottomRight,
					colorBlueBottomRight);
			var9.setBrightness(brightnessBottomRight);
			var9.addVertexWithUV(var29, var31, var33, var19, var23);
			var9.setColorOpaque_F(colorRedTopRight, colorGreenTopRight,
					colorBlueTopRight);
			var9.setBrightness(brightnessTopRight);
			var9.addVertexWithUV(var29, var31, var35, var13, var17);
		} else {
			var9.addVertexWithUV(var27, var31, var35, var21, var25);
			var9.addVertexWithUV(var27, var31, var33, var37, var15);
			var9.addVertexWithUV(var29, var31, var33, var19, var23);
			var9.addVertexWithUV(var29, var31, var35, var13, var17);
		}
	}

	/**
	 * Renders the given texture to the top face of the block. Args: block, x,
	 * y, z, texture
	 */
	public void renderFaceYPos(final Block par1Block, final double par2,
			final double par4, final double par6, Icon par8Icon) {
		final Tessellator var9 = Tessellator.instance;

		if (hasOverrideBlockTexture()) {
			par8Icon = overrideBlockTexture;
		}

		if (Config.isConnectedTextures() && overrideBlockTexture == null
				&& uvRotateTop == 0) {
			par8Icon = ConnectedTextures.getConnectedTexture(blockAccess,
					par1Block, (int) par2, (int) par4, (int) par6, 1, par8Icon);
		}

		boolean var10 = false;

		if (Config.isNaturalTextures() && overrideBlockTexture == null
				&& uvRotateTop == 0) {
			final NaturalProperties var11 = NaturalTextures
					.getNaturalProperties(par8Icon);

			if (var11 != null) {
				final int var12 = Config.getRandom((int) par2, (int) par4,
						(int) par6, 1);

				if (var11.rotation > 1) {
					uvRotateTop = var12 & 3;
				}

				if (var11.rotation == 2) {
					uvRotateTop = uvRotateTop / 2 * 3;
				}

				if (var11.flip) {
					flipTexture = (var12 & 4) != 0;
				}

				var10 = true;
			}
		}

		double var37 = par8Icon.getInterpolatedU(renderMinX * 16.0D);
		double var13 = par8Icon.getInterpolatedU(renderMaxX * 16.0D);
		double var15 = par8Icon.getInterpolatedV(renderMinZ * 16.0D);
		double var17 = par8Icon.getInterpolatedV(renderMaxZ * 16.0D);
		double var19;

		if (flipTexture) {
			var19 = var37;
			var37 = var13;
			var13 = var19;
		}

		if (renderMinX < 0.0D || renderMaxX > 1.0D) {
			var37 = par8Icon.getMinU();
			var13 = par8Icon.getMaxU();
		}

		if (renderMinZ < 0.0D || renderMaxZ > 1.0D) {
			var15 = par8Icon.getMinV();
			var17 = par8Icon.getMaxV();
		}

		var19 = var13;
		double var21 = var37;
		double var23 = var15;
		double var25 = var17;
		double var27;

		if (uvRotateTop == 1) {
			var37 = par8Icon.getInterpolatedU(renderMinZ * 16.0D);
			var15 = par8Icon.getInterpolatedV(16.0D - renderMaxX * 16.0D);
			var13 = par8Icon.getInterpolatedU(renderMaxZ * 16.0D);
			var17 = par8Icon.getInterpolatedV(16.0D - renderMinX * 16.0D);

			if (flipTexture) {
				var27 = var37;
				var37 = var13;
				var13 = var27;
			}

			var23 = var15;
			var25 = var17;
			var19 = var37;
			var21 = var13;
			var15 = var17;
			var17 = var23;
		} else if (uvRotateTop == 2) {
			var37 = par8Icon.getInterpolatedU(16.0D - renderMaxZ * 16.0D);
			var15 = par8Icon.getInterpolatedV(renderMinX * 16.0D);
			var13 = par8Icon.getInterpolatedU(16.0D - renderMinZ * 16.0D);
			var17 = par8Icon.getInterpolatedV(renderMaxX * 16.0D);

			if (flipTexture) {
				var27 = var37;
				var37 = var13;
				var13 = var27;
			}

			var19 = var13;
			var21 = var37;
			var37 = var13;
			var13 = var21;
			var23 = var17;
			var25 = var15;
		} else if (uvRotateTop == 3) {
			var37 = par8Icon.getInterpolatedU(16.0D - renderMinX * 16.0D);
			var13 = par8Icon.getInterpolatedU(16.0D - renderMaxX * 16.0D);
			var15 = par8Icon.getInterpolatedV(16.0D - renderMinZ * 16.0D);
			var17 = par8Icon.getInterpolatedV(16.0D - renderMaxZ * 16.0D);

			if (flipTexture) {
				var27 = var37;
				var37 = var13;
				var13 = var27;
			}

			var19 = var13;
			var21 = var37;
			var23 = var15;
			var25 = var17;
		}

		if (var10) {
			uvRotateTop = 0;
			flipTexture = false;
		}

		var27 = par2 + renderMinX;
		final double var29 = par2 + renderMaxX;
		final double var31 = par4 + renderMaxY;
		final double var33 = par6 + renderMinZ;
		final double var35 = par6 + renderMaxZ;

		if (enableAO) {
			var9.setColorOpaque_F(colorRedTopLeft, colorGreenTopLeft,
					colorBlueTopLeft);
			var9.setBrightness(brightnessTopLeft);
			var9.addVertexWithUV(var29, var31, var35, var13, var17);
			var9.setColorOpaque_F(colorRedBottomLeft, colorGreenBottomLeft,
					colorBlueBottomLeft);
			var9.setBrightness(brightnessBottomLeft);
			var9.addVertexWithUV(var29, var31, var33, var19, var23);
			var9.setColorOpaque_F(colorRedBottomRight, colorGreenBottomRight,
					colorBlueBottomRight);
			var9.setBrightness(brightnessBottomRight);
			var9.addVertexWithUV(var27, var31, var33, var37, var15);
			var9.setColorOpaque_F(colorRedTopRight, colorGreenTopRight,
					colorBlueTopRight);
			var9.setBrightness(brightnessTopRight);
			var9.addVertexWithUV(var27, var31, var35, var21, var25);
		} else {
			var9.addVertexWithUV(var29, var31, var35, var13, var17);
			var9.addVertexWithUV(var29, var31, var33, var19, var23);
			var9.addVertexWithUV(var27, var31, var33, var37, var15);
			var9.addVertexWithUV(var27, var31, var35, var21, var25);
		}
	}

	/**
	 * Renders the given texture to the north (z-negative) face of the block.
	 * Args: block, x, y, z, texture
	 */
	public void renderFaceZNeg(final Block par1Block, final double par2,
			final double par4, final double par6, Icon par8Icon) {
		final Tessellator var9 = Tessellator.instance;

		if (hasOverrideBlockTexture()) {
			par8Icon = overrideBlockTexture;
		}

		if (Config.isConnectedTextures() && overrideBlockTexture == null
				&& uvRotateEast == 0) {
			par8Icon = ConnectedTextures.getConnectedTexture(blockAccess,
					par1Block, (int) par2, (int) par4, (int) par6, 2, par8Icon);
		}

		boolean var10 = false;

		if (Config.isNaturalTextures() && overrideBlockTexture == null
				&& uvRotateEast == 0) {
			final NaturalProperties var11 = NaturalTextures
					.getNaturalProperties(par8Icon);

			if (var11 != null) {
				final int var12 = Config.getRandom((int) par2, (int) par4,
						(int) par6, 2);

				if (var11.rotation > 1) {
					uvRotateEast = var12 & 3;
				}

				if (var11.rotation == 2) {
					uvRotateEast = uvRotateEast / 2 * 3;
				}

				if (var11.flip) {
					flipTexture = (var12 & 4) != 0;
				}

				var10 = true;
			}
		}

		double var37 = par8Icon.getInterpolatedU(renderMinX * 16.0D);
		double var13 = par8Icon.getInterpolatedU(renderMaxX * 16.0D);
		double var15 = par8Icon.getInterpolatedV(16.0D - renderMaxY * 16.0D);
		double var17 = par8Icon.getInterpolatedV(16.0D - renderMinY * 16.0D);
		double var19;

		if (flipTexture) {
			var19 = var37;
			var37 = var13;
			var13 = var19;
		}

		if (renderMinX < 0.0D || renderMaxX > 1.0D) {
			var37 = par8Icon.getMinU();
			var13 = par8Icon.getMaxU();
		}

		if (renderMinY < 0.0D || renderMaxY > 1.0D) {
			var15 = par8Icon.getMinV();
			var17 = par8Icon.getMaxV();
		}

		var19 = var13;
		double var21 = var37;
		double var23 = var15;
		double var25 = var17;
		double var27;

		if (uvRotateEast == 2) {
			var37 = par8Icon.getInterpolatedU(renderMinY * 16.0D);
			var15 = par8Icon.getInterpolatedV(16.0D - renderMinX * 16.0D);
			var13 = par8Icon.getInterpolatedU(renderMaxY * 16.0D);
			var17 = par8Icon.getInterpolatedV(16.0D - renderMaxX * 16.0D);

			if (flipTexture) {
				var27 = var37;
				var37 = var13;
				var13 = var27;
			}

			var23 = var15;
			var25 = var17;
			var19 = var37;
			var21 = var13;
			var15 = var17;
			var17 = var23;
		} else if (uvRotateEast == 1) {
			var37 = par8Icon.getInterpolatedU(16.0D - renderMaxY * 16.0D);
			var15 = par8Icon.getInterpolatedV(renderMaxX * 16.0D);
			var13 = par8Icon.getInterpolatedU(16.0D - renderMinY * 16.0D);
			var17 = par8Icon.getInterpolatedV(renderMinX * 16.0D);

			if (flipTexture) {
				var27 = var37;
				var37 = var13;
				var13 = var27;
			}

			var19 = var13;
			var21 = var37;
			var37 = var13;
			var13 = var21;
			var23 = var17;
			var25 = var15;
		} else if (uvRotateEast == 3) {
			var37 = par8Icon.getInterpolatedU(16.0D - renderMinX * 16.0D);
			var13 = par8Icon.getInterpolatedU(16.0D - renderMaxX * 16.0D);
			var15 = par8Icon.getInterpolatedV(renderMaxY * 16.0D);
			var17 = par8Icon.getInterpolatedV(renderMinY * 16.0D);

			if (flipTexture) {
				var27 = var37;
				var37 = var13;
				var13 = var27;
			}

			var19 = var13;
			var21 = var37;
			var23 = var15;
			var25 = var17;
		}

		if (var10) {
			uvRotateEast = 0;
			flipTexture = false;
		}

		var27 = par2 + renderMinX;
		final double var29 = par2 + renderMaxX;
		final double var31 = par4 + renderMinY;
		final double var33 = par4 + renderMaxY;
		final double var35 = par6 + renderMinZ;

		if (enableAO) {
			var9.setColorOpaque_F(colorRedTopLeft, colorGreenTopLeft,
					colorBlueTopLeft);
			var9.setBrightness(brightnessTopLeft);
			var9.addVertexWithUV(var27, var33, var35, var19, var23);
			var9.setColorOpaque_F(colorRedBottomLeft, colorGreenBottomLeft,
					colorBlueBottomLeft);
			var9.setBrightness(brightnessBottomLeft);
			var9.addVertexWithUV(var29, var33, var35, var37, var15);
			var9.setColorOpaque_F(colorRedBottomRight, colorGreenBottomRight,
					colorBlueBottomRight);
			var9.setBrightness(brightnessBottomRight);
			var9.addVertexWithUV(var29, var31, var35, var21, var25);
			var9.setColorOpaque_F(colorRedTopRight, colorGreenTopRight,
					colorBlueTopRight);
			var9.setBrightness(brightnessTopRight);
			var9.addVertexWithUV(var27, var31, var35, var13, var17);
		} else {
			var9.addVertexWithUV(var27, var33, var35, var19, var23);
			var9.addVertexWithUV(var29, var33, var35, var37, var15);
			var9.addVertexWithUV(var29, var31, var35, var21, var25);
			var9.addVertexWithUV(var27, var31, var35, var13, var17);
		}
	}

	/**
	 * Renders the given texture to the south (z-positive) face of the block.
	 * Args: block, x, y, z, texture
	 */
	public void renderFaceZPos(final Block par1Block, final double par2,
			final double par4, final double par6, Icon par8Icon) {
		final Tessellator var9 = Tessellator.instance;

		if (hasOverrideBlockTexture()) {
			par8Icon = overrideBlockTexture;
		}

		if (Config.isConnectedTextures() && overrideBlockTexture == null
				&& uvRotateWest == 0) {
			par8Icon = ConnectedTextures.getConnectedTexture(blockAccess,
					par1Block, (int) par2, (int) par4, (int) par6, 3, par8Icon);
		}

		boolean var10 = false;

		if (Config.isNaturalTextures() && overrideBlockTexture == null
				&& uvRotateWest == 0) {
			final NaturalProperties var11 = NaturalTextures
					.getNaturalProperties(par8Icon);

			if (var11 != null) {
				final int var12 = Config.getRandom((int) par2, (int) par4,
						(int) par6, 3);

				if (var11.rotation > 1) {
					uvRotateWest = var12 & 3;
				}

				if (var11.rotation == 2) {
					uvRotateWest = uvRotateWest / 2 * 3;
				}

				if (var11.flip) {
					flipTexture = (var12 & 4) != 0;
				}

				var10 = true;
			}
		}

		double var37 = par8Icon.getInterpolatedU(renderMinX * 16.0D);
		double var13 = par8Icon.getInterpolatedU(renderMaxX * 16.0D);
		double var15 = par8Icon.getInterpolatedV(16.0D - renderMaxY * 16.0D);
		double var17 = par8Icon.getInterpolatedV(16.0D - renderMinY * 16.0D);
		double var19;

		if (flipTexture) {
			var19 = var37;
			var37 = var13;
			var13 = var19;
		}

		if (renderMinX < 0.0D || renderMaxX > 1.0D) {
			var37 = par8Icon.getMinU();
			var13 = par8Icon.getMaxU();
		}

		if (renderMinY < 0.0D || renderMaxY > 1.0D) {
			var15 = par8Icon.getMinV();
			var17 = par8Icon.getMaxV();
		}

		var19 = var13;
		double var21 = var37;
		double var23 = var15;
		double var25 = var17;
		double var27;

		if (uvRotateWest == 1) {
			var37 = par8Icon.getInterpolatedU(renderMinY * 16.0D);
			var17 = par8Icon.getInterpolatedV(16.0D - renderMinX * 16.0D);
			var13 = par8Icon.getInterpolatedU(renderMaxY * 16.0D);
			var15 = par8Icon.getInterpolatedV(16.0D - renderMaxX * 16.0D);

			if (flipTexture) {
				var27 = var37;
				var37 = var13;
				var13 = var27;
			}

			var23 = var15;
			var25 = var17;
			var19 = var37;
			var21 = var13;
			var15 = var17;
			var17 = var23;
		} else if (uvRotateWest == 2) {
			var37 = par8Icon.getInterpolatedU(16.0D - renderMaxY * 16.0D);
			var15 = par8Icon.getInterpolatedV(renderMinX * 16.0D);
			var13 = par8Icon.getInterpolatedU(16.0D - renderMinY * 16.0D);
			var17 = par8Icon.getInterpolatedV(renderMaxX * 16.0D);

			if (flipTexture) {
				var27 = var37;
				var37 = var13;
				var13 = var27;
			}

			var19 = var13;
			var21 = var37;
			var37 = var13;
			var13 = var21;
			var23 = var17;
			var25 = var15;
		} else if (uvRotateWest == 3) {
			var37 = par8Icon.getInterpolatedU(16.0D - renderMinX * 16.0D);
			var13 = par8Icon.getInterpolatedU(16.0D - renderMaxX * 16.0D);
			var15 = par8Icon.getInterpolatedV(renderMaxY * 16.0D);
			var17 = par8Icon.getInterpolatedV(renderMinY * 16.0D);

			if (flipTexture) {
				var27 = var37;
				var37 = var13;
				var13 = var27;
			}

			var19 = var13;
			var21 = var37;
			var23 = var15;
			var25 = var17;
		}

		if (var10) {
			uvRotateWest = 0;
			flipTexture = false;
		}

		var27 = par2 + renderMinX;
		final double var29 = par2 + renderMaxX;
		final double var31 = par4 + renderMinY;
		final double var33 = par4 + renderMaxY;
		final double var35 = par6 + renderMaxZ;

		if (enableAO) {
			var9.setColorOpaque_F(colorRedTopLeft, colorGreenTopLeft,
					colorBlueTopLeft);
			var9.setBrightness(brightnessTopLeft);
			var9.addVertexWithUV(var27, var33, var35, var37, var15);
			var9.setColorOpaque_F(colorRedBottomLeft, colorGreenBottomLeft,
					colorBlueBottomLeft);
			var9.setBrightness(brightnessBottomLeft);
			var9.addVertexWithUV(var27, var31, var35, var21, var25);
			var9.setColorOpaque_F(colorRedBottomRight, colorGreenBottomRight,
					colorBlueBottomRight);
			var9.setBrightness(brightnessBottomRight);
			var9.addVertexWithUV(var29, var31, var35, var13, var17);
			var9.setColorOpaque_F(colorRedTopRight, colorGreenTopRight,
					colorBlueTopRight);
			var9.setBrightness(brightnessTopRight);
			var9.addVertexWithUV(var29, var33, var35, var19, var23);
		} else {
			var9.addVertexWithUV(var27, var33, var35, var37, var15);
			var9.addVertexWithUV(var27, var31, var35, var21, var25);
			var9.addVertexWithUV(var29, var31, var35, var13, var17);
			var9.addVertexWithUV(var29, var33, var35, var19, var23);
		}
	}

	/**
	 * Renders the given texture to the west (x-negative) face of the block.
	 * Args: block, x, y, z, texture
	 */
	public void renderFaceXNeg(final Block par1Block, final double par2,
			final double par4, final double par6, Icon par8Icon) {
		final Tessellator var9 = Tessellator.instance;

		if (hasOverrideBlockTexture()) {
			par8Icon = overrideBlockTexture;
		}

		if (Config.isConnectedTextures() && overrideBlockTexture == null
				&& uvRotateNorth == 0) {
			par8Icon = ConnectedTextures.getConnectedTexture(blockAccess,
					par1Block, (int) par2, (int) par4, (int) par6, 4, par8Icon);
		}

		boolean var10 = false;

		if (Config.isNaturalTextures() && overrideBlockTexture == null
				&& uvRotateNorth == 0) {
			final NaturalProperties var11 = NaturalTextures
					.getNaturalProperties(par8Icon);

			if (var11 != null) {
				final int var12 = Config.getRandom((int) par2, (int) par4,
						(int) par6, 4);

				if (var11.rotation > 1) {
					uvRotateNorth = var12 & 3;
				}

				if (var11.rotation == 2) {
					uvRotateNorth = uvRotateNorth / 2 * 3;
				}

				if (var11.flip) {
					flipTexture = (var12 & 4) != 0;
				}

				var10 = true;
			}
		}

		double var37 = par8Icon.getInterpolatedU(renderMinZ * 16.0D);
		double var13 = par8Icon.getInterpolatedU(renderMaxZ * 16.0D);
		double var15 = par8Icon.getInterpolatedV(16.0D - renderMaxY * 16.0D);
		double var17 = par8Icon.getInterpolatedV(16.0D - renderMinY * 16.0D);
		double var19;

		if (flipTexture) {
			var19 = var37;
			var37 = var13;
			var13 = var19;
		}

		if (renderMinZ < 0.0D || renderMaxZ > 1.0D) {
			var37 = par8Icon.getMinU();
			var13 = par8Icon.getMaxU();
		}

		if (renderMinY < 0.0D || renderMaxY > 1.0D) {
			var15 = par8Icon.getMinV();
			var17 = par8Icon.getMaxV();
		}

		var19 = var13;
		double var21 = var37;
		double var23 = var15;
		double var25 = var17;
		double var27;

		if (uvRotateNorth == 1) {
			var37 = par8Icon.getInterpolatedU(renderMinY * 16.0D);
			var15 = par8Icon.getInterpolatedV(16.0D - renderMaxZ * 16.0D);
			var13 = par8Icon.getInterpolatedU(renderMaxY * 16.0D);
			var17 = par8Icon.getInterpolatedV(16.0D - renderMinZ * 16.0D);

			if (flipTexture) {
				var27 = var37;
				var37 = var13;
				var13 = var27;
			}

			var23 = var15;
			var25 = var17;
			var19 = var37;
			var21 = var13;
			var15 = var17;
			var17 = var23;
		} else if (uvRotateNorth == 2) {
			var37 = par8Icon.getInterpolatedU(16.0D - renderMaxY * 16.0D);
			var15 = par8Icon.getInterpolatedV(renderMinZ * 16.0D);
			var13 = par8Icon.getInterpolatedU(16.0D - renderMinY * 16.0D);
			var17 = par8Icon.getInterpolatedV(renderMaxZ * 16.0D);

			if (flipTexture) {
				var27 = var37;
				var37 = var13;
				var13 = var27;
			}

			var19 = var13;
			var21 = var37;
			var37 = var13;
			var13 = var21;
			var23 = var17;
			var25 = var15;
		} else if (uvRotateNorth == 3) {
			var37 = par8Icon.getInterpolatedU(16.0D - renderMinZ * 16.0D);
			var13 = par8Icon.getInterpolatedU(16.0D - renderMaxZ * 16.0D);
			var15 = par8Icon.getInterpolatedV(renderMaxY * 16.0D);
			var17 = par8Icon.getInterpolatedV(renderMinY * 16.0D);

			if (flipTexture) {
				var27 = var37;
				var37 = var13;
				var13 = var27;
			}

			var19 = var13;
			var21 = var37;
			var23 = var15;
			var25 = var17;
		}

		if (var10) {
			uvRotateNorth = 0;
			flipTexture = false;
		}

		var27 = par2 + renderMinX;
		final double var29 = par4 + renderMinY;
		final double var31 = par4 + renderMaxY;
		final double var33 = par6 + renderMinZ;
		final double var35 = par6 + renderMaxZ;

		if (enableAO) {
			var9.setColorOpaque_F(colorRedTopLeft, colorGreenTopLeft,
					colorBlueTopLeft);
			var9.setBrightness(brightnessTopLeft);
			var9.addVertexWithUV(var27, var31, var35, var19, var23);
			var9.setColorOpaque_F(colorRedBottomLeft, colorGreenBottomLeft,
					colorBlueBottomLeft);
			var9.setBrightness(brightnessBottomLeft);
			var9.addVertexWithUV(var27, var31, var33, var37, var15);
			var9.setColorOpaque_F(colorRedBottomRight, colorGreenBottomRight,
					colorBlueBottomRight);
			var9.setBrightness(brightnessBottomRight);
			var9.addVertexWithUV(var27, var29, var33, var21, var25);
			var9.setColorOpaque_F(colorRedTopRight, colorGreenTopRight,
					colorBlueTopRight);
			var9.setBrightness(brightnessTopRight);
			var9.addVertexWithUV(var27, var29, var35, var13, var17);
		} else {
			var9.addVertexWithUV(var27, var31, var35, var19, var23);
			var9.addVertexWithUV(var27, var31, var33, var37, var15);
			var9.addVertexWithUV(var27, var29, var33, var21, var25);
			var9.addVertexWithUV(var27, var29, var35, var13, var17);
		}
	}

	/**
	 * Renders the given texture to the east (x-positive) face of the block.
	 * Args: block, x, y, z, texture
	 */
	public void renderFaceXPos(final Block par1Block, final double par2,
			final double par4, final double par6, Icon par8Icon) {
		final Tessellator var9 = Tessellator.instance;

		if (hasOverrideBlockTexture()) {
			par8Icon = overrideBlockTexture;
		}

		if (Config.isConnectedTextures() && overrideBlockTexture == null
				&& uvRotateSouth == 0) {
			par8Icon = ConnectedTextures.getConnectedTexture(blockAccess,
					par1Block, (int) par2, (int) par4, (int) par6, 5, par8Icon);
		}

		boolean var10 = false;

		if (Config.isNaturalTextures() && overrideBlockTexture == null
				&& uvRotateSouth == 0) {
			final NaturalProperties var11 = NaturalTextures
					.getNaturalProperties(par8Icon);

			if (var11 != null) {
				final int var12 = Config.getRandom((int) par2, (int) par4,
						(int) par6, 5);

				if (var11.rotation > 1) {
					uvRotateSouth = var12 & 3;
				}

				if (var11.rotation == 2) {
					uvRotateSouth = uvRotateSouth / 2 * 3;
				}

				if (var11.flip) {
					flipTexture = (var12 & 4) != 0;
				}

				var10 = true;
			}
		}

		double var37 = par8Icon.getInterpolatedU(renderMinZ * 16.0D);
		double var13 = par8Icon.getInterpolatedU(renderMaxZ * 16.0D);
		double var15 = par8Icon.getInterpolatedV(16.0D - renderMaxY * 16.0D);
		double var17 = par8Icon.getInterpolatedV(16.0D - renderMinY * 16.0D);
		double var19;

		if (flipTexture) {
			var19 = var37;
			var37 = var13;
			var13 = var19;
		}

		if (renderMinZ < 0.0D || renderMaxZ > 1.0D) {
			var37 = par8Icon.getMinU();
			var13 = par8Icon.getMaxU();
		}

		if (renderMinY < 0.0D || renderMaxY > 1.0D) {
			var15 = par8Icon.getMinV();
			var17 = par8Icon.getMaxV();
		}

		var19 = var13;
		double var21 = var37;
		double var23 = var15;
		double var25 = var17;
		double var27;

		if (uvRotateSouth == 2) {
			var37 = par8Icon.getInterpolatedU(renderMinY * 16.0D);
			var15 = par8Icon.getInterpolatedV(16.0D - renderMinZ * 16.0D);
			var13 = par8Icon.getInterpolatedU(renderMaxY * 16.0D);
			var17 = par8Icon.getInterpolatedV(16.0D - renderMaxZ * 16.0D);

			if (flipTexture) {
				var27 = var37;
				var37 = var13;
				var13 = var27;
			}

			var23 = var15;
			var25 = var17;
			var19 = var37;
			var21 = var13;
			var15 = var17;
			var17 = var23;
		} else if (uvRotateSouth == 1) {
			var37 = par8Icon.getInterpolatedU(16.0D - renderMaxY * 16.0D);
			var15 = par8Icon.getInterpolatedV(renderMaxZ * 16.0D);
			var13 = par8Icon.getInterpolatedU(16.0D - renderMinY * 16.0D);
			var17 = par8Icon.getInterpolatedV(renderMinZ * 16.0D);

			if (flipTexture) {
				var27 = var37;
				var37 = var13;
				var13 = var27;
			}

			var19 = var13;
			var21 = var37;
			var37 = var13;
			var13 = var21;
			var23 = var17;
			var25 = var15;
		} else if (uvRotateSouth == 3) {
			var37 = par8Icon.getInterpolatedU(16.0D - renderMinZ * 16.0D);
			var13 = par8Icon.getInterpolatedU(16.0D - renderMaxZ * 16.0D);
			var15 = par8Icon.getInterpolatedV(renderMaxY * 16.0D);
			var17 = par8Icon.getInterpolatedV(renderMinY * 16.0D);

			if (flipTexture) {
				var27 = var37;
				var37 = var13;
				var13 = var27;
			}

			var19 = var13;
			var21 = var37;
			var23 = var15;
			var25 = var17;
		}

		if (var10) {
			uvRotateSouth = 0;
			flipTexture = false;
		}

		var27 = par2 + renderMaxX;
		final double var29 = par4 + renderMinY;
		final double var31 = par4 + renderMaxY;
		final double var33 = par6 + renderMinZ;
		final double var35 = par6 + renderMaxZ;

		if (enableAO) {
			var9.setColorOpaque_F(colorRedTopLeft, colorGreenTopLeft,
					colorBlueTopLeft);
			var9.setBrightness(brightnessTopLeft);
			var9.addVertexWithUV(var27, var29, var35, var21, var25);
			var9.setColorOpaque_F(colorRedBottomLeft, colorGreenBottomLeft,
					colorBlueBottomLeft);
			var9.setBrightness(brightnessBottomLeft);
			var9.addVertexWithUV(var27, var29, var33, var13, var17);
			var9.setColorOpaque_F(colorRedBottomRight, colorGreenBottomRight,
					colorBlueBottomRight);
			var9.setBrightness(brightnessBottomRight);
			var9.addVertexWithUV(var27, var31, var33, var19, var23);
			var9.setColorOpaque_F(colorRedTopRight, colorGreenTopRight,
					colorBlueTopRight);
			var9.setBrightness(brightnessTopRight);
			var9.addVertexWithUV(var27, var31, var35, var37, var15);
		} else {
			var9.addVertexWithUV(var27, var29, var35, var21, var25);
			var9.addVertexWithUV(var27, var29, var33, var13, var17);
			var9.addVertexWithUV(var27, var31, var33, var19, var23);
			var9.addVertexWithUV(var27, var31, var35, var37, var15);
		}
	}

	/**
	 * Is called to render the image of a block on an inventory, as a held item,
	 * or as a an item on the ground
	 */
	public void renderBlockAsItem(final Block par1Block, int par2,
			final float par3) {
		final Tessellator var4 = Tessellator.instance;
		final boolean var5 = par1Block.blockID == Block.grass.blockID;

		if (par1Block == Block.dispenser || par1Block == Block.dropper
				|| par1Block == Block.furnaceIdle) {
			par2 = 3;
		}

		int var6;
		float var7;
		float var8;
		float var9;

		if (useInventoryTint) {
			var6 = par1Block.getRenderColor(par2);

			if (var5) {
				var6 = 16777215;
			}

			var7 = (var6 >> 16 & 255) / 255.0F;
			var8 = (var6 >> 8 & 255) / 255.0F;
			var9 = (var6 & 255) / 255.0F;
			GL11.glColor4f(var7 * par3, var8 * par3, var9 * par3, 1.0F);
		}

		var6 = par1Block.getRenderType();
		setRenderBoundsFromBlock(par1Block);
		int var10;

		if (var6 != 0 && var6 != 31 && var6 != 39 && var6 != 16 && var6 != 26) {
			if (var6 == 1) {
				var4.startDrawingQuads();
				var4.setNormal(0.0F, -1.0F, 0.0F);
				drawCrossedSquares(par1Block, par2, -0.5D, -0.5D, -0.5D, 1.0F);
				var4.draw();
			} else if (var6 == 19) {
				var4.startDrawingQuads();
				var4.setNormal(0.0F, -1.0F, 0.0F);
				par1Block.setBlockBoundsForItemRender();
				renderBlockStemSmall(par1Block, par2, renderMaxY, -0.5D, -0.5D,
						-0.5D);
				var4.draw();
			} else if (var6 == 23) {
				var4.startDrawingQuads();
				var4.setNormal(0.0F, -1.0F, 0.0F);
				par1Block.setBlockBoundsForItemRender();
				var4.draw();
			} else if (var6 == 13) {
				par1Block.setBlockBoundsForItemRender();
				GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
				var7 = 0.0625F;
				var4.startDrawingQuads();
				var4.setNormal(0.0F, -1.0F, 0.0F);
				renderFaceYNeg(par1Block, 0.0D, 0.0D, 0.0D,
						getBlockIconFromSide(par1Block, 0));
				var4.draw();
				var4.startDrawingQuads();
				var4.setNormal(0.0F, 1.0F, 0.0F);
				renderFaceYPos(par1Block, 0.0D, 0.0D, 0.0D,
						getBlockIconFromSide(par1Block, 1));
				var4.draw();
				var4.startDrawingQuads();
				var4.setNormal(0.0F, 0.0F, -1.0F);
				var4.addTranslation(0.0F, 0.0F, var7);
				renderFaceZNeg(par1Block, 0.0D, 0.0D, 0.0D,
						getBlockIconFromSide(par1Block, 2));
				var4.addTranslation(0.0F, 0.0F, -var7);
				var4.draw();
				var4.startDrawingQuads();
				var4.setNormal(0.0F, 0.0F, 1.0F);
				var4.addTranslation(0.0F, 0.0F, -var7);
				renderFaceZPos(par1Block, 0.0D, 0.0D, 0.0D,
						getBlockIconFromSide(par1Block, 3));
				var4.addTranslation(0.0F, 0.0F, var7);
				var4.draw();
				var4.startDrawingQuads();
				var4.setNormal(-1.0F, 0.0F, 0.0F);
				var4.addTranslation(var7, 0.0F, 0.0F);
				renderFaceXNeg(par1Block, 0.0D, 0.0D, 0.0D,
						getBlockIconFromSide(par1Block, 4));
				var4.addTranslation(-var7, 0.0F, 0.0F);
				var4.draw();
				var4.startDrawingQuads();
				var4.setNormal(1.0F, 0.0F, 0.0F);
				var4.addTranslation(-var7, 0.0F, 0.0F);
				renderFaceXPos(par1Block, 0.0D, 0.0D, 0.0D,
						getBlockIconFromSide(par1Block, 5));
				var4.addTranslation(var7, 0.0F, 0.0F);
				var4.draw();
				GL11.glTranslatef(0.5F, 0.5F, 0.5F);
			} else if (var6 == 22) {
				GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
				GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
				ChestItemRenderHelper.instance.renderChest(par1Block, par2,
						par3);
				GL11.glEnable(GL12.GL_RESCALE_NORMAL);
			} else if (var6 == 6) {
				var4.startDrawingQuads();
				var4.setNormal(0.0F, -1.0F, 0.0F);
				renderBlockCropsImpl(par1Block, par2, -0.5D, -0.5D, -0.5D);
				var4.draw();
			} else if (var6 == 2) {
				var4.startDrawingQuads();
				var4.setNormal(0.0F, -1.0F, 0.0F);
				renderTorchAtAngle(par1Block, -0.5D, -0.5D, -0.5D, 0.0D, 0.0D,
						0);
				var4.draw();
			} else if (var6 == 10) {
				for (var10 = 0; var10 < 2; ++var10) {
					if (var10 == 0) {
						setRenderBounds(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 0.5D);
					}

					if (var10 == 1) {
						setRenderBounds(0.0D, 0.0D, 0.5D, 1.0D, 0.5D, 1.0D);
					}

					GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
					var4.startDrawingQuads();
					var4.setNormal(0.0F, -1.0F, 0.0F);
					renderFaceYNeg(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSide(par1Block, 0));
					var4.draw();
					var4.startDrawingQuads();
					var4.setNormal(0.0F, 1.0F, 0.0F);
					renderFaceYPos(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSide(par1Block, 1));
					var4.draw();
					var4.startDrawingQuads();
					var4.setNormal(0.0F, 0.0F, -1.0F);
					renderFaceZNeg(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSide(par1Block, 2));
					var4.draw();
					var4.startDrawingQuads();
					var4.setNormal(0.0F, 0.0F, 1.0F);
					renderFaceZPos(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSide(par1Block, 3));
					var4.draw();
					var4.startDrawingQuads();
					var4.setNormal(-1.0F, 0.0F, 0.0F);
					renderFaceXNeg(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSide(par1Block, 4));
					var4.draw();
					var4.startDrawingQuads();
					var4.setNormal(1.0F, 0.0F, 0.0F);
					renderFaceXPos(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSide(par1Block, 5));
					var4.draw();
					GL11.glTranslatef(0.5F, 0.5F, 0.5F);
				}
			} else if (var6 == 27) {
				var10 = 0;
				GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
				var4.startDrawingQuads();

				for (int var17 = 0; var17 < 8; ++var17) {
					byte var12 = 0;
					byte var13 = 1;

					if (var17 == 0) {
						var12 = 2;
					}

					if (var17 == 1) {
						var12 = 3;
					}

					if (var17 == 2) {
						var12 = 4;
					}

					if (var17 == 3) {
						var12 = 5;
						var13 = 2;
					}

					if (var17 == 4) {
						var12 = 6;
						var13 = 3;
					}

					if (var17 == 5) {
						var12 = 7;
						var13 = 5;
					}

					if (var17 == 6) {
						var12 = 6;
						var13 = 2;
					}

					if (var17 == 7) {
						var12 = 3;
					}

					final float var14 = var12 / 16.0F;
					final float var15 = 1.0F - var10 / 16.0F;
					final float var16 = 1.0F - (var10 + var13) / 16.0F;
					var10 += var13;
					setRenderBounds(0.5F - var14, var16, 0.5F - var14,
							0.5F + var14, var15, 0.5F + var14);
					var4.setNormal(0.0F, -1.0F, 0.0F);
					renderFaceYNeg(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSide(par1Block, 0));
					var4.setNormal(0.0F, 1.0F, 0.0F);
					renderFaceYPos(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSide(par1Block, 1));
					var4.setNormal(0.0F, 0.0F, -1.0F);
					renderFaceZNeg(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSide(par1Block, 2));
					var4.setNormal(0.0F, 0.0F, 1.0F);
					renderFaceZPos(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSide(par1Block, 3));
					var4.setNormal(-1.0F, 0.0F, 0.0F);
					renderFaceXNeg(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSide(par1Block, 4));
					var4.setNormal(1.0F, 0.0F, 0.0F);
					renderFaceXPos(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSide(par1Block, 5));
				}

				var4.draw();
				GL11.glTranslatef(0.5F, 0.5F, 0.5F);
				setRenderBounds(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
			} else if (var6 == 11) {
				for (var10 = 0; var10 < 4; ++var10) {
					var8 = 0.125F;

					if (var10 == 0) {
						setRenderBounds(0.5F - var8, 0.0D, 0.0D, 0.5F + var8,
								1.0D, var8 * 2.0F);
					}

					if (var10 == 1) {
						setRenderBounds(0.5F - var8, 0.0D, 1.0F - var8 * 2.0F,
								0.5F + var8, 1.0D, 1.0D);
					}

					var8 = 0.0625F;

					if (var10 == 2) {
						setRenderBounds(0.5F - var8, 1.0F - var8 * 3.0F,
								-var8 * 2.0F, 0.5F + var8, 1.0F - var8,
								1.0F + var8 * 2.0F);
					}

					if (var10 == 3) {
						setRenderBounds(0.5F - var8, 0.5F - var8 * 3.0F,
								-var8 * 2.0F, 0.5F + var8, 0.5F - var8,
								1.0F + var8 * 2.0F);
					}

					GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
					var4.startDrawingQuads();
					var4.setNormal(0.0F, -1.0F, 0.0F);
					renderFaceYNeg(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSide(par1Block, 0));
					var4.draw();
					var4.startDrawingQuads();
					var4.setNormal(0.0F, 1.0F, 0.0F);
					renderFaceYPos(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSide(par1Block, 1));
					var4.draw();
					var4.startDrawingQuads();
					var4.setNormal(0.0F, 0.0F, -1.0F);
					renderFaceZNeg(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSide(par1Block, 2));
					var4.draw();
					var4.startDrawingQuads();
					var4.setNormal(0.0F, 0.0F, 1.0F);
					renderFaceZPos(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSide(par1Block, 3));
					var4.draw();
					var4.startDrawingQuads();
					var4.setNormal(-1.0F, 0.0F, 0.0F);
					renderFaceXNeg(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSide(par1Block, 4));
					var4.draw();
					var4.startDrawingQuads();
					var4.setNormal(1.0F, 0.0F, 0.0F);
					renderFaceXPos(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSide(par1Block, 5));
					var4.draw();
					GL11.glTranslatef(0.5F, 0.5F, 0.5F);
				}

				setRenderBounds(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
			} else if (var6 == 21) {
				for (var10 = 0; var10 < 3; ++var10) {
					var8 = 0.0625F;

					if (var10 == 0) {
						setRenderBounds(0.5F - var8, 0.30000001192092896D,
								0.0D, 0.5F + var8, 1.0D, var8 * 2.0F);
					}

					if (var10 == 1) {
						setRenderBounds(0.5F - var8, 0.30000001192092896D,
								1.0F - var8 * 2.0F, 0.5F + var8, 1.0D, 1.0D);
					}

					var8 = 0.0625F;

					if (var10 == 2) {
						setRenderBounds(0.5F - var8, 0.5D, 0.0D, 0.5F + var8,
								1.0F - var8, 1.0D);
					}

					GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
					var4.startDrawingQuads();
					var4.setNormal(0.0F, -1.0F, 0.0F);
					renderFaceYNeg(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSide(par1Block, 0));
					var4.draw();
					var4.startDrawingQuads();
					var4.setNormal(0.0F, 1.0F, 0.0F);
					renderFaceYPos(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSide(par1Block, 1));
					var4.draw();
					var4.startDrawingQuads();
					var4.setNormal(0.0F, 0.0F, -1.0F);
					renderFaceZNeg(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSide(par1Block, 2));
					var4.draw();
					var4.startDrawingQuads();
					var4.setNormal(0.0F, 0.0F, 1.0F);
					renderFaceZPos(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSide(par1Block, 3));
					var4.draw();
					var4.startDrawingQuads();
					var4.setNormal(-1.0F, 0.0F, 0.0F);
					renderFaceXNeg(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSide(par1Block, 4));
					var4.draw();
					var4.startDrawingQuads();
					var4.setNormal(1.0F, 0.0F, 0.0F);
					renderFaceXPos(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSide(par1Block, 5));
					var4.draw();
					GL11.glTranslatef(0.5F, 0.5F, 0.5F);
				}
			} else if (var6 == 32) {
				for (var10 = 0; var10 < 2; ++var10) {
					if (var10 == 0) {
						setRenderBounds(0.0D, 0.0D, 0.3125D, 1.0D, 0.8125D,
								0.6875D);
					}

					if (var10 == 1) {
						setRenderBounds(0.25D, 0.0D, 0.25D, 0.75D, 1.0D, 0.75D);
					}

					GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
					var4.startDrawingQuads();
					var4.setNormal(0.0F, -1.0F, 0.0F);
					renderFaceYNeg(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSideAndMetadata(par1Block, 0, par2));
					var4.draw();
					var4.startDrawingQuads();
					var4.setNormal(0.0F, 1.0F, 0.0F);
					renderFaceYPos(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSideAndMetadata(par1Block, 1, par2));
					var4.draw();
					var4.startDrawingQuads();
					var4.setNormal(0.0F, 0.0F, -1.0F);
					renderFaceZNeg(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSideAndMetadata(par1Block, 2, par2));
					var4.draw();
					var4.startDrawingQuads();
					var4.setNormal(0.0F, 0.0F, 1.0F);
					renderFaceZPos(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSideAndMetadata(par1Block, 3, par2));
					var4.draw();
					var4.startDrawingQuads();
					var4.setNormal(-1.0F, 0.0F, 0.0F);
					renderFaceXNeg(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSideAndMetadata(par1Block, 4, par2));
					var4.draw();
					var4.startDrawingQuads();
					var4.setNormal(1.0F, 0.0F, 0.0F);
					renderFaceXPos(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSideAndMetadata(par1Block, 5, par2));
					var4.draw();
					GL11.glTranslatef(0.5F, 0.5F, 0.5F);
				}

				setRenderBounds(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
			} else if (var6 == 35) {
				GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
				renderBlockAnvilOrient((BlockAnvil) par1Block, 0, 0, 0, par2,
						true);
				GL11.glTranslatef(0.5F, 0.5F, 0.5F);
			} else if (var6 == 34) {
				for (var10 = 0; var10 < 3; ++var10) {
					if (var10 == 0) {
						setRenderBounds(0.125D, 0.0D, 0.125D, 0.875D, 0.1875D,
								0.875D);
						setOverrideBlockTexture(this
								.getBlockIcon(Block.obsidian));
					} else if (var10 == 1) {
						setRenderBounds(0.1875D, 0.1875D, 0.1875D, 0.8125D,
								0.875D, 0.8125D);
						setOverrideBlockTexture(Block.beacon.getBeaconIcon());
					} else if (var10 == 2) {
						setRenderBounds(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
						setOverrideBlockTexture(this.getBlockIcon(Block.glass));
					}

					GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
					var4.startDrawingQuads();
					var4.setNormal(0.0F, -1.0F, 0.0F);
					renderFaceYNeg(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSideAndMetadata(par1Block, 0, par2));
					var4.draw();
					var4.startDrawingQuads();
					var4.setNormal(0.0F, 1.0F, 0.0F);
					renderFaceYPos(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSideAndMetadata(par1Block, 1, par2));
					var4.draw();
					var4.startDrawingQuads();
					var4.setNormal(0.0F, 0.0F, -1.0F);
					renderFaceZNeg(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSideAndMetadata(par1Block, 2, par2));
					var4.draw();
					var4.startDrawingQuads();
					var4.setNormal(0.0F, 0.0F, 1.0F);
					renderFaceZPos(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSideAndMetadata(par1Block, 3, par2));
					var4.draw();
					var4.startDrawingQuads();
					var4.setNormal(-1.0F, 0.0F, 0.0F);
					renderFaceXNeg(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSideAndMetadata(par1Block, 4, par2));
					var4.draw();
					var4.startDrawingQuads();
					var4.setNormal(1.0F, 0.0F, 0.0F);
					renderFaceXPos(par1Block, 0.0D, 0.0D, 0.0D,
							getBlockIconFromSideAndMetadata(par1Block, 5, par2));
					var4.draw();
					GL11.glTranslatef(0.5F, 0.5F, 0.5F);
				}

				setRenderBounds(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
				clearOverrideBlockTexture();
			} else if (var6 == 38) {
				GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
				renderBlockHopperMetadata((BlockHopper) par1Block, 0, 0, 0, 0,
						true);
				GL11.glTranslatef(0.5F, 0.5F, 0.5F);
			} else if (Reflector.ModLoader.exists()) {
				Reflector.callVoid(Reflector.ModLoader_renderInvBlock,
						new Object[] { this, par1Block, Integer.valueOf(par2),
								Integer.valueOf(var6) });
			} else if (Reflector.FMLRenderAccessLibrary.exists()) {
				Reflector.callVoid(
						Reflector.FMLRenderAccessLibrary_renderInventoryBlock,
						new Object[] { this, par1Block, Integer.valueOf(par2),
								Integer.valueOf(var6) });
			}
		} else {
			if (var6 == 16) {
				par2 = 1;
			}

			par1Block.setBlockBoundsForItemRender();
			setRenderBoundsFromBlock(par1Block);
			GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
			GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
			var4.startDrawingQuads();
			var4.setNormal(0.0F, -1.0F, 0.0F);
			renderFaceYNeg(par1Block, 0.0D, 0.0D, 0.0D,
					getBlockIconFromSideAndMetadata(par1Block, 0, par2));
			var4.draw();

			if (var5 && useInventoryTint) {
				var10 = par1Block.getRenderColor(par2);
				var8 = (var10 >> 16 & 255) / 255.0F;
				var9 = (var10 >> 8 & 255) / 255.0F;
				final float var11 = (var10 & 255) / 255.0F;
				GL11.glColor4f(var8 * par3, var9 * par3, var11 * par3, 1.0F);
			}

			var4.startDrawingQuads();
			var4.setNormal(0.0F, 1.0F, 0.0F);
			renderFaceYPos(par1Block, 0.0D, 0.0D, 0.0D,
					getBlockIconFromSideAndMetadata(par1Block, 1, par2));
			var4.draw();

			if (var5 && useInventoryTint) {
				GL11.glColor4f(par3, par3, par3, 1.0F);
			}

			var4.startDrawingQuads();
			var4.setNormal(0.0F, 0.0F, -1.0F);
			renderFaceZNeg(par1Block, 0.0D, 0.0D, 0.0D,
					getBlockIconFromSideAndMetadata(par1Block, 2, par2));
			var4.draw();
			var4.startDrawingQuads();
			var4.setNormal(0.0F, 0.0F, 1.0F);
			renderFaceZPos(par1Block, 0.0D, 0.0D, 0.0D,
					getBlockIconFromSideAndMetadata(par1Block, 3, par2));
			var4.draw();
			var4.startDrawingQuads();
			var4.setNormal(-1.0F, 0.0F, 0.0F);
			renderFaceXNeg(par1Block, 0.0D, 0.0D, 0.0D,
					getBlockIconFromSideAndMetadata(par1Block, 4, par2));
			var4.draw();
			var4.startDrawingQuads();
			var4.setNormal(1.0F, 0.0F, 0.0F);
			renderFaceXPos(par1Block, 0.0D, 0.0D, 0.0D,
					getBlockIconFromSideAndMetadata(par1Block, 5, par2));
			var4.draw();
			GL11.glTranslatef(0.5F, 0.5F, 0.5F);
		}
	}

	/**
	 * Checks to see if the item's render type indicates that it should be
	 * rendered as a regular block or not.
	 */
	public static boolean renderItemIn3d(final int par0) {
		switch (par0) {
		case 0:
		case 10:
		case 11:
		case 13:
		case 16:
		case 21:
		case 22:
		case 26:
		case 27:
		case 31:
		case 32:
		case 34:
		case 35:
		case 39:
			return true;

		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
		case 8:
		case 9:
		case 12:
		case 14:
		case 15:
		case 17:
		case 18:
		case 19:
		case 20:
		case 23:
		case 24:
		case 25:
		case 28:
		case 29:
		case 30:
		case 33:
		case 36:
		case 37:
		case 38:
		default:
			return Reflector.ModLoader.exists() ? Reflector.callBoolean(
					Reflector.ModLoader_renderBlockIsItemFull3D,
					new Object[] { Integer.valueOf(par0) })
					: Reflector.FMLRenderAccessLibrary.exists() ? Reflector
							.callBoolean(
									Reflector.FMLRenderAccessLibrary_renderItemAsFull3DBlock,
									new Object[] { Integer.valueOf(par0) })
							: false;
		}
	}

	public Icon getBlockIcon(final Block par1Block,
			final IBlockAccess par2IBlockAccess, final int par3,
			final int par4, final int par5, final int par6) {
		return getIconSafe(par1Block.getBlockTexture(par2IBlockAccess, par3,
				par4, par5, par6));
	}

	public Icon getBlockIconFromSideAndMetadata(final Block par1Block,
			final int par2, final int par3) {
		return getIconSafe(par1Block.getIcon(par2, par3));
	}

	public Icon getBlockIconFromSide(final Block par1Block, final int par2) {
		return getIconSafe(par1Block.getBlockTextureFromSide(par2));
	}

	public Icon getBlockIcon(final Block par1Block) {
		return getIconSafe(par1Block.getBlockTextureFromSide(1));
	}

	public Icon getIconSafe(final Icon par1Icon) {
		return par1Icon == null ? minecraftRB.renderEngine.getMissingIcon(0)
				: par1Icon;
	}

	private float getAmbientOcclusionLightValue(final IBlockAccess var1,
			final int var2, final int var3, final int var4) {
		final Block var5 = Block.blocksList[var1.getBlockId(var2, var3, var4)];

		if (var5 == null) {
			return 1.0F;
		} else {
			final boolean var6 = var5.blockMaterial.blocksMovement()
					&& var5.renderAsNormalBlock();
			return var6 ? aoLightValueOpaque : 1.0F;
		}
	}

	private Icon fixAoSideGrassTexture(Icon var1, final int var2,
			final int var3, final int var4, final int var5, final float var6,
			final float var7, final float var8) {
		if (var1 == TextureUtils.iconGrassSide
				|| var1 == TextureUtils.iconMycelSide) {
			var1 = Config.getSideGrassTexture(blockAccess, var2, var3, var4,
					var5, var1);

			if (var1 == TextureUtils.iconGrassTop) {
				colorRedTopLeft *= var6;
				colorRedBottomLeft *= var6;
				colorRedBottomRight *= var6;
				colorRedTopRight *= var6;
				colorGreenTopLeft *= var7;
				colorGreenBottomLeft *= var7;
				colorGreenBottomRight *= var7;
				colorGreenTopRight *= var7;
				colorBlueTopLeft *= var8;
				colorBlueBottomLeft *= var8;
				colorBlueBottomRight *= var8;
				colorBlueTopRight *= var8;
			}
		}

		if (var1 == TextureUtils.iconSnowSide) {
			var1 = Config.getSideSnowGrassTexture(blockAccess, var2, var3,
					var4, var5);
		}

		return var1;
	}

	private boolean hasSnowNeighbours(final int var1, final int var2,
			final int var3) {
		final int var4 = Block.snow.blockID;
		return blockAccess.getBlockId(var1 - 1, var2, var3) != var4
				&& blockAccess.getBlockId(var1 + 1, var2, var3) != var4
				&& blockAccess.getBlockId(var1, var2, var3 - 1) != var4
				&& blockAccess.getBlockId(var1, var2, var3 + 1) != var4 ? false
				: blockAccess.isBlockOpaqueCube(var1, var2 - 1, var3);
	}

	private void renderSnow(final int var1, final int var2, final int var3,
			final double var4) {
		setRenderBoundsFromBlock(Block.snow);
		renderMaxY = var4;
		renderStandardBlock(Block.snow, var1, var2, var3);
	}
}
