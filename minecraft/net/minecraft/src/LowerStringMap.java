package net.minecraft.src;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class LowerStringMap implements Map {
	private final Map internalMap = new LinkedHashMap();

	@Override
	public int size() {
		return internalMap.size();
	}

	@Override
	public boolean isEmpty() {
		return internalMap.isEmpty();
	}

	@Override
	public boolean containsKey(final Object par1Obj) {
		return internalMap.containsKey(par1Obj.toString().toLowerCase());
	}

	@Override
	public boolean containsValue(final Object par1Obj) {
		return internalMap.containsKey(par1Obj);
	}

	@Override
	public Object get(final Object par1Obj) {
		return internalMap.get(par1Obj.toString().toLowerCase());
	}

	/**
	 * a map already defines a general put
	 */
	public Object putLower(final String par1Str, final Object par2Obj) {
		return internalMap.put(par1Str.toLowerCase(), par2Obj);
	}

	@Override
	public Object remove(final Object par1Obj) {
		return internalMap.remove(par1Obj.toString().toLowerCase());
	}

	@Override
	public void putAll(final Map par1Map) {
		final Iterator var2 = par1Map.entrySet().iterator();

		while (var2.hasNext()) {
			final Entry var3 = (Entry) var2.next();
			putLower((String) var3.getKey(), var3.getValue());
		}
	}

	@Override
	public void clear() {
		internalMap.clear();
	}

	@Override
	public Set keySet() {
		return internalMap.keySet();
	}

	@Override
	public Collection values() {
		return internalMap.values();
	}

	@Override
	public Set entrySet() {
		return internalMap.entrySet();
	}

	@Override
	public Object put(final Object par1Obj, final Object par2Obj) {
		return putLower((String) par1Obj, par2Obj);
	}
}
