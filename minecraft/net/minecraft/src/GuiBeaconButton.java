package net.minecraft.src;

import net.minecraft.client.Minecraft;

import org.lwjgl.opengl.GL11;

class GuiBeaconButton extends GuiButton {
	/** Texture for this button. */
	private final String buttonTexture;
	private final int field_82257_l;
	private final int field_82258_m;
	private boolean field_82256_n;

	protected GuiBeaconButton(final int par1, final int par2, final int par3,
			final String par4Str, final int par5, final int par6) {
		super(par1, par2, par3, 22, 22, "");
		buttonTexture = par4Str;
		field_82257_l = par5;
		field_82258_m = par6;
	}

	/**
	 * Draws this button to the screen.
	 */
	@Override
	public void drawButton(final Minecraft par1Minecraft, final int par2,
			final int par3) {
		if (drawButton) {
			par1Minecraft.renderEngine.bindTexture("/gui/beacon.png");
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			field_82253_i = par2 >= xPosition && par3 >= yPosition
					&& par2 < xPosition + width && par3 < yPosition + height;
			final short var4 = 219;
			int var5 = 0;

			if (!enabled) {
				var5 += width * 2;
			} else if (field_82256_n) {
				var5 += width * 1;
			} else if (field_82253_i) {
				var5 += width * 3;
			}

			drawTexturedModalRect(xPosition, yPosition, var5, var4, width,
					height);

			if (!"/gui/beacon.png".equals(buttonTexture)) {
				par1Minecraft.renderEngine.bindTexture(buttonTexture);
			}

			drawTexturedModalRect(xPosition + 2, yPosition + 2, field_82257_l,
					field_82258_m, 18, 18);
		}
	}

	public boolean func_82255_b() {
		return field_82256_n;
	}

	public void func_82254_b(final boolean par1) {
		field_82256_n = par1;
	}
}
