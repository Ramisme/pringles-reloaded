package net.minecraft.src;

import net.minecraft.client.Minecraft;

import org.lwjgl.opengl.GL11;

public class GuiSlider extends GuiButton {
	/** The value of this slider control. */
	public float sliderValue = 1.0F;

	/** Is this slider control being dragged. */
	public boolean dragging = false;

	/** Additional ID for this slider control. */
	private EnumOptions idFloat = null;

	public GuiSlider(final int par1, final int par2, final int par3,
			final EnumOptions par4EnumOptions, final String par5Str,
			final float par6) {
		super(par1, par2, par3, 150, 20, par5Str);
		idFloat = par4EnumOptions;
		sliderValue = par6;
	}

	/**
	 * Returns 0 if the button is disabled, 1 if the mouse is NOT hovering over
	 * this button and 2 if it IS hovering over this button.
	 */
	@Override
	protected int getHoverState(final boolean par1) {
		return 0;
	}

	/**
	 * Fired when the mouse button is dragged. Equivalent of
	 * MouseListener.mouseDragged(MouseEvent e).
	 */
	@Override
	protected void mouseDragged(final Minecraft par1Minecraft, final int par2,
			final int par3) {
		if (drawButton) {
			if (dragging) {
				sliderValue = (float) (par2 - (xPosition + 4))
						/ (float) (width - 8);

				if (sliderValue < 0.0F) {
					sliderValue = 0.0F;
				}

				if (sliderValue > 1.0F) {
					sliderValue = 1.0F;
				}

				par1Minecraft.gameSettings.setOptionFloatValue(idFloat,
						sliderValue);
				displayString = par1Minecraft.gameSettings
						.getKeyBinding(idFloat);
			}

			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			drawTexturedModalRect(
					xPosition + (int) (sliderValue * (width - 8)), yPosition,
					0, 66, 4, 20);
			drawTexturedModalRect(xPosition + (int) (sliderValue * (width - 8))
					+ 4, yPosition, 196, 66, 4, 20);
		}
	}

	/**
	 * Returns true if the mouse has been pressed on this control. Equivalent of
	 * MouseListener.mousePressed(MouseEvent e).
	 */
	@Override
	public boolean mousePressed(final Minecraft par1Minecraft, final int par2,
			final int par3) {
		if (super.mousePressed(par1Minecraft, par2, par3)) {
			sliderValue = (float) (par2 - (xPosition + 4))
					/ (float) (width - 8);

			if (sliderValue < 0.0F) {
				sliderValue = 0.0F;
			}

			if (sliderValue > 1.0F) {
				sliderValue = 1.0F;
			}

			par1Minecraft.gameSettings
					.setOptionFloatValue(idFloat, sliderValue);
			displayString = par1Minecraft.gameSettings.getKeyBinding(idFloat);
			dragging = true;
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Fired when the mouse button is released. Equivalent of
	 * MouseListener.mouseReleased(MouseEvent e).
	 */
	@Override
	public void mouseReleased(final int par1, final int par2) {
		dragging = false;
	}
}
