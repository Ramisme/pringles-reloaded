package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet31RelEntityMove extends Packet30Entity {
	public Packet31RelEntityMove() {
	}

	public Packet31RelEntityMove(final int par1, final byte par2,
			final byte par3, final byte par4) {
		super(par1);
		xPosition = par2;
		yPosition = par3;
		zPosition = par4;
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		super.readPacketData(par1DataInputStream);
		xPosition = par1DataInputStream.readByte();
		yPosition = par1DataInputStream.readByte();
		zPosition = par1DataInputStream.readByte();
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		super.writePacketData(par1DataOutputStream);
		par1DataOutputStream.writeByte(xPosition);
		par1DataOutputStream.writeByte(yPosition);
		par1DataOutputStream.writeByte(zPosition);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 7;
	}
}
