package net.minecraft.src;

public class BiomeGenEnd extends BiomeGenBase {
	public BiomeGenEnd(final int par1) {
		super(par1);
		spawnableMonsterList.clear();
		spawnableCreatureList.clear();
		spawnableWaterCreatureList.clear();
		spawnableCaveCreatureList.clear();
		spawnableMonsterList.add(new SpawnListEntry(EntityEnderman.class, 10,
				4, 4));
		topBlock = (byte) Block.dirt.blockID;
		fillerBlock = (byte) Block.dirt.blockID;
		theBiomeDecorator = new BiomeEndDecorator(this);
	}

	/**
	 * takes temperature, returns color
	 */
	@Override
	public int getSkyColorByTemp(final float par1) {
		return 0;
	}
}
