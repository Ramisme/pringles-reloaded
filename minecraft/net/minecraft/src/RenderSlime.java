package net.minecraft.src;

import org.lwjgl.opengl.GL11;

public class RenderSlime extends RenderLiving {
	private final ModelBase scaleAmount;

	public RenderSlime(final ModelBase par1ModelBase,
			final ModelBase par2ModelBase, final float par3) {
		super(par1ModelBase, par3);
		scaleAmount = par2ModelBase;
	}

	/**
	 * Determines whether Slime Render should pass or not.
	 */
	protected int shouldSlimeRenderPass(final EntitySlime par1EntitySlime,
			final int par2, final float par3) {
		if (par1EntitySlime.isInvisible()) {
			return 0;
		} else if (par2 == 0) {
			setRenderPassModel(scaleAmount);
			GL11.glEnable(GL11.GL_NORMALIZE);
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
			return 1;
		} else {
			if (par2 == 1) {
				GL11.glDisable(GL11.GL_BLEND);
				GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			}

			return -1;
		}
	}

	/**
	 * sets the scale for the slime based on getSlimeSize in EntitySlime
	 */
	protected void scaleSlime(final EntitySlime par1EntitySlime,
			final float par2) {
		final float var3 = par1EntitySlime.getSlimeSize();
		final float var4 = (par1EntitySlime.field_70812_c + (par1EntitySlime.field_70811_b - par1EntitySlime.field_70812_c)
				* par2)
				/ (var3 * 0.5F + 1.0F);
		final float var5 = 1.0F / (var4 + 1.0F);
		GL11.glScalef(var5 * var3, 1.0F / var5 * var3, var5 * var3);
	}

	/**
	 * Allows the render to do any OpenGL state modifications necessary before
	 * the model is rendered. Args: entityLiving, partialTickTime
	 */
	@Override
	protected void preRenderCallback(final EntityLiving par1EntityLiving,
			final float par2) {
		scaleSlime((EntitySlime) par1EntityLiving, par2);
	}

	/**
	 * Queries whether should render the specified pass or not.
	 */
	@Override
	protected int shouldRenderPass(final EntityLiving par1EntityLiving,
			final int par2, final float par3) {
		return shouldSlimeRenderPass((EntitySlime) par1EntityLiving, par2, par3);
	}
}
