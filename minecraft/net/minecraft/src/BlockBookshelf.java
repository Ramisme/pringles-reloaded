package net.minecraft.src;

import java.util.Random;

public class BlockBookshelf extends Block {
	public BlockBookshelf(final int par1) {
		super(par1, Material.wood);
		setCreativeTab(CreativeTabs.tabBlock);
	}

	/**
	 * From the specified side and block metadata retrieves the blocks texture.
	 * Args: side, metadata
	 */
	@Override
	public Icon getIcon(final int par1, final int par2) {
		return par1 != 1 && par1 != 0 ? super.getIcon(par1, par2)
				: Block.planks.getBlockTextureFromSide(par1);
	}

	/**
	 * Returns the quantity of items to drop on block destruction.
	 */
	@Override
	public int quantityDropped(final Random par1Random) {
		return 3;
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return Item.book.itemID;
	}
}
