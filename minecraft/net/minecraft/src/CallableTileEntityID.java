package net.minecraft.src;

import java.util.concurrent.Callable;

class CallableTileEntityID implements Callable {
	final TileEntity theTileEntity;

	CallableTileEntityID(final TileEntity par1TileEntity) {
		theTileEntity = par1TileEntity;
	}

	public String callTileEntityID() {
		final int var1 = theTileEntity.worldObj.getBlockId(
				theTileEntity.xCoord, theTileEntity.yCoord,
				theTileEntity.zCoord);

		try {
			return String.format("ID #%d (%s // %s)",
					new Object[] {
							Integer.valueOf(var1),
							Block.blocksList[var1].getUnlocalizedName(),
							Block.blocksList[var1].getClass()
									.getCanonicalName() });
		} catch (final Throwable var3) {
			return "ID #" + var1;
		}
	}

	@Override
	public Object call() {
		return callTileEntityID();
	}
}
