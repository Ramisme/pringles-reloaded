package net.minecraft.src;

import net.minecraft.client.Minecraft;

public class StatStringFormatKeyInv implements IStatStringFormat {
	/** Minecraft instance */
	final Minecraft mc;

	public StatStringFormatKeyInv(final Minecraft par1Minecraft) {
		mc = par1Minecraft;
	}

	/**
	 * Formats the strings based on 'IStatStringFormat' interface.
	 */
	@Override
	public String formatString(final String par1Str) {
		try {
			return String
					.format(par1Str,
							new Object[] { GameSettings
									.getKeyDisplayString(mc.gameSettings.keyBindInventory.keyCode) });
		} catch (final Exception var3) {
			return "Error: " + var3.getLocalizedMessage();
		}
	}
}
