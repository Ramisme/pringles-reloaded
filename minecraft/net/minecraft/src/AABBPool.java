package net.minecraft.src;

import java.util.ArrayList;
import java.util.List;

public class AABBPool {
	/**
	 * Maximum number of times the pool can be "cleaned" before the list is
	 * shrunk
	 */
	private final int maxNumCleans;

	/**
	 * Number of Pool entries to remove when cleanPool is called maxNumCleans
	 * times.
	 */
	private final int numEntriesToRemove;

	/** List of AABB stored in this Pool */
	private final List listAABB = new ArrayList();

	/** Next index to use when adding a Pool Entry. */
	private int nextPoolIndex = 0;

	/**
	 * Largest index reached by this Pool (can be reset to 0 upon calling
	 * cleanPool)
	 */
	private int maxPoolIndex = 0;

	/** Number of times this Pool has been cleaned */
	private int numCleans = 0;

	public AABBPool(final int par1, final int par2) {
		maxNumCleans = par1;
		numEntriesToRemove = par2;
	}

	/**
	 * Creates a new AABB, or reuses one that's no longer in use. Parameters:
	 * minX, minY, minZ, maxX, maxY, maxZ. AABBs returned from this function
	 * should only be used for one frame or tick, as after that they will be
	 * reused.
	 */
	public AxisAlignedBB getAABB(final double par1, final double par3,
			final double par5, final double par7, final double par9,
			final double par11) {
		AxisAlignedBB var13;

		if (nextPoolIndex >= listAABB.size()) {
			var13 = new AxisAlignedBB(par1, par3, par5, par7, par9, par11);
			listAABB.add(var13);
		} else {
			var13 = (AxisAlignedBB) listAABB.get(nextPoolIndex);
			var13.setBounds(par1, par3, par5, par7, par9, par11);
		}

		++nextPoolIndex;
		return var13;
	}

	/**
	 * Marks the pool as "empty", starting over when adding new entries. If this
	 * is called maxNumCleans times, the list size is reduced
	 */
	public void cleanPool() {
		if (nextPoolIndex > maxPoolIndex) {
			maxPoolIndex = nextPoolIndex;
		}

		if (numCleans++ == maxNumCleans) {
			final int var1 = Math.max(maxPoolIndex, listAABB.size()
					- numEntriesToRemove);

			while (listAABB.size() > var1) {
				listAABB.remove(var1);
			}

			maxPoolIndex = 0;
			numCleans = 0;
		}

		nextPoolIndex = 0;
	}

	/**
	 * Clears the AABBPool
	 */
	public void clearPool() {
		nextPoolIndex = 0;
		listAABB.clear();
	}

	public int getlistAABBsize() {
		return listAABB.size();
	}

	public int getnextPoolIndex() {
		return nextPoolIndex;
	}
}
