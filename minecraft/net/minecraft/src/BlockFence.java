package net.minecraft.src;

import java.util.List;

public class BlockFence extends Block {
	private final String field_94464_a;

	public BlockFence(final int par1, final String par2Str,
			final Material par3Material) {
		super(par1, par3Material);
		field_94464_a = par2Str;
		setCreativeTab(CreativeTabs.tabDecorations);
	}

	/**
	 * Adds all intersecting collision boxes to a list. (Be sure to only add
	 * boxes to the list if they intersect the mask.) Parameters: World, X, Y,
	 * Z, mask, list, colliding entity
	 */
	@Override
	public void addCollisionBoxesToList(final World par1World, final int par2,
			final int par3, final int par4,
			final AxisAlignedBB par5AxisAlignedBB, final List par6List,
			final Entity par7Entity) {
		final boolean var8 = canConnectFenceTo(par1World, par2, par3, par4 - 1);
		final boolean var9 = canConnectFenceTo(par1World, par2, par3, par4 + 1);
		final boolean var10 = canConnectFenceTo(par1World, par2 - 1, par3, par4);
		final boolean var11 = canConnectFenceTo(par1World, par2 + 1, par3, par4);
		float var12 = 0.375F;
		float var13 = 0.625F;
		float var14 = 0.375F;
		float var15 = 0.625F;

		if (var8) {
			var14 = 0.0F;
		}

		if (var9) {
			var15 = 1.0F;
		}

		if (var8 || var9) {
			setBlockBounds(var12, 0.0F, var14, var13, 1.5F, var15);
			super.addCollisionBoxesToList(par1World, par2, par3, par4,
					par5AxisAlignedBB, par6List, par7Entity);
		}

		var14 = 0.375F;
		var15 = 0.625F;

		if (var10) {
			var12 = 0.0F;
		}

		if (var11) {
			var13 = 1.0F;
		}

		if (var10 || var11 || !var8 && !var9) {
			setBlockBounds(var12, 0.0F, var14, var13, 1.5F, var15);
			super.addCollisionBoxesToList(par1World, par2, par3, par4,
					par5AxisAlignedBB, par6List, par7Entity);
		}

		if (var8) {
			var14 = 0.0F;
		}

		if (var9) {
			var15 = 1.0F;
		}

		setBlockBounds(var12, 0.0F, var14, var13, 1.0F, var15);
	}

	/**
	 * Updates the blocks bounds based on its current state. Args: world, x, y,
	 * z
	 */
	@Override
	public void setBlockBoundsBasedOnState(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		final boolean var5 = canConnectFenceTo(par1IBlockAccess, par2, par3,
				par4 - 1);
		final boolean var6 = canConnectFenceTo(par1IBlockAccess, par2, par3,
				par4 + 1);
		final boolean var7 = canConnectFenceTo(par1IBlockAccess, par2 - 1,
				par3, par4);
		final boolean var8 = canConnectFenceTo(par1IBlockAccess, par2 + 1,
				par3, par4);
		float var9 = 0.375F;
		float var10 = 0.625F;
		float var11 = 0.375F;
		float var12 = 0.625F;

		if (var5) {
			var11 = 0.0F;
		}

		if (var6) {
			var12 = 1.0F;
		}

		if (var7) {
			var9 = 0.0F;
		}

		if (var8) {
			var10 = 1.0F;
		}

		setBlockBounds(var9, 0.0F, var11, var10, 1.0F, var12);
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	@Override
	public boolean getBlocksMovement(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		return false;
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return 11;
	}

	/**
	 * Returns true if the specified block can be connected by a fence
	 */
	public boolean canConnectFenceTo(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		final int var5 = par1IBlockAccess.getBlockId(par2, par3, par4);

		if (var5 != blockID && var5 != Block.fenceGate.blockID) {
			final Block var6 = Block.blocksList[var5];
			return var6 != null && var6.blockMaterial.isOpaque()
					&& var6.renderAsNormalBlock() ? var6.blockMaterial != Material.pumpkin
					: false;
		} else {
			return true;
		}
	}

	public static boolean isIdAFence(final int par0) {
		return par0 == Block.fence.blockID || par0 == Block.netherFence.blockID;
	}

	/**
	 * Returns true if the given side of this block type should be rendered, if
	 * the adjacent block is at the given coordinates. Args: blockAccess, x, y,
	 * z, side
	 */
	@Override
	public boolean shouldSideBeRendered(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		return true;
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		blockIcon = par1IconRegister.registerIcon(field_94464_a);
	}
}
