package net.minecraft.src;

public class GuiYesNo extends GuiScreen {
	/**
	 * A reference to the screen object that created this. Used for navigating
	 * between screens.
	 */
	protected GuiScreen parentScreen;

	/** First line of text. */
	protected String message1;

	/** Second line of text. */
	private final String message2;

	/** The text shown for the first button in GuiYesNo */
	protected String buttonText1;

	/** The text shown for the second button in GuiYesNo */
	protected String buttonText2;

	/** World number to be deleted. */
	protected int worldNumber;

	public GuiYesNo(final GuiScreen par1GuiScreen, final String par2Str,
			final String par3Str, final int par4) {
		parentScreen = par1GuiScreen;
		message1 = par2Str;
		message2 = par3Str;
		worldNumber = par4;
		final StringTranslate var5 = StringTranslate.getInstance();
		buttonText1 = var5.translateKey("gui.yes");
		buttonText2 = var5.translateKey("gui.no");
	}

	public GuiYesNo(final GuiScreen par1GuiScreen, final String par2Str,
			final String par3Str, final String par4Str, final String par5Str,
			final int par6) {
		parentScreen = par1GuiScreen;
		message1 = par2Str;
		message2 = par3Str;
		buttonText1 = par4Str;
		buttonText2 = par5Str;
		worldNumber = par6;
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui() {
		buttonList.add(new GuiSmallButton(0, width / 2 - 155, height / 6 + 96,
				buttonText1));
		buttonList.add(new GuiSmallButton(1, width / 2 - 155 + 160,
				height / 6 + 96, buttonText2));
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	@Override
	protected void actionPerformed(final GuiButton par1GuiButton) {
		parentScreen.confirmClicked(par1GuiButton.id == 0, worldNumber);
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(final int par1, final int par2, final float par3) {
		drawDefaultBackground();
		drawCenteredString(fontRenderer, message1, width / 2, 70, 16777215);
		drawCenteredString(fontRenderer, message2, width / 2, 90, 16777215);
		super.drawScreen(par1, par2, par3);
	}
}
