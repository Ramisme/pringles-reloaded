package net.minecraft.src;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

public class TexturePackCustom extends TexturePackImplementation {
	/** ZipFile object used to access the texture pack file's contents. */
	private ZipFile texturePackZipFile;

	public TexturePackCustom(final String par1Str, final File par2File,
			final ITexturePack par3ITexturePack) {
		super(par1Str, par2File, par2File.getName(), par3ITexturePack);
	}

	/**
	 * Delete the OpenGL texture id of the pack's thumbnail image, and close the
	 * zip file in case of TexturePackCustom.
	 */
	@Override
	public void deleteTexturePack(final RenderEngine par1RenderEngine) {
		super.deleteTexturePack(par1RenderEngine);

		try {
			if (texturePackZipFile != null) {
				texturePackZipFile.close();
			}
		} catch (final IOException var3) {
			;
		}

		texturePackZipFile = null;
	}

	@Override
	protected InputStream func_98139_b(final String par1Str) throws IOException {
		openTexturePackFile();
		final ZipEntry var2 = texturePackZipFile.getEntry(par1Str.substring(1));

		if (var2 == null) {
			throw new FileNotFoundException(par1Str);
		} else {
			return texturePackZipFile.getInputStream(var2);
		}
	}

	@Override
	public boolean func_98140_c(final String par1Str) {
		try {
			openTexturePackFile();
			return texturePackZipFile.getEntry(par1Str.substring(1)) != null;
		} catch (final Exception var3) {
			return false;
		}
	}

	/**
	 * Open the texture pack's file and initialize texturePackZipFile
	 */
	private void openTexturePackFile() throws IOException, ZipException {
		if (texturePackZipFile == null) {
			texturePackZipFile = new ZipFile(texturePackFile);
		}
	}

	@Override
	public boolean isCompatible() {
		try {
			openTexturePackFile();
			final Enumeration var1 = texturePackZipFile.entries();

			while (var1.hasMoreElements()) {
				final ZipEntry var2 = (ZipEntry) var1.nextElement();

				if (var2.getName().startsWith("textures/")) {
					return true;
				}
			}
		} catch (final Exception var3) {
			;
		}

		final boolean var4 = func_98140_c("terrain.png")
				|| func_98140_c("gui/items.png");
		return !var4;
	}
}
