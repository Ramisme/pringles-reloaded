package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet62LevelSound extends Packet {
	/** e.g. step.grass */
	private String soundName;

	/** Effect X multiplied by 8 */
	private int effectX;

	/** Effect Y multiplied by 8 */
	private int effectY = Integer.MAX_VALUE;

	/** Effect Z multiplied by 8 */
	private int effectZ;

	/** 1 is 100%. Can be more. */
	private float volume;

	/** 63 is 100%. Can be more. */
	private int pitch;

	public Packet62LevelSound() {
	}

	public Packet62LevelSound(final String par1Str, final double par2,
			final double par4, final double par6, final float par8,
			final float par9) {
		soundName = par1Str;
		effectX = (int) (par2 * 8.0D);
		effectY = (int) (par4 * 8.0D);
		effectZ = (int) (par6 * 8.0D);
		volume = par8;
		pitch = (int) (par9 * 63.0F);

		if (pitch < 0) {
			pitch = 0;
		}

		if (pitch > 255) {
			pitch = 255;
		}
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		soundName = Packet.readString(par1DataInputStream, 32);
		effectX = par1DataInputStream.readInt();
		effectY = par1DataInputStream.readInt();
		effectZ = par1DataInputStream.readInt();
		volume = par1DataInputStream.readFloat();
		pitch = par1DataInputStream.readUnsignedByte();
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		Packet.writeString(soundName, par1DataOutputStream);
		par1DataOutputStream.writeInt(effectX);
		par1DataOutputStream.writeInt(effectY);
		par1DataOutputStream.writeInt(effectZ);
		par1DataOutputStream.writeFloat(volume);
		par1DataOutputStream.writeByte(pitch);
	}

	public String getSoundName() {
		return soundName;
	}

	public double getEffectX() {
		return effectX / 8.0F;
	}

	public double getEffectY() {
		return effectY / 8.0F;
	}

	public double getEffectZ() {
		return effectZ / 8.0F;
	}

	public float getVolume() {
		return volume;
	}

	/**
	 * Gets the pitch divided by 63 (63 is 100%)
	 */
	public float getPitch() {
		return pitch / 63.0F;
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleLevelSound(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 24;
	}
}
