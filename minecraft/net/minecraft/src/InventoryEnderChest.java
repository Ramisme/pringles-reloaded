package net.minecraft.src;

public class InventoryEnderChest extends InventoryBasic {
	private TileEntityEnderChest associatedChest;

	public InventoryEnderChest() {
		super("container.enderchest", false, 27);
	}

	public void setAssociatedChest(
			final TileEntityEnderChest par1TileEntityEnderChest) {
		associatedChest = par1TileEntityEnderChest;
	}

	public void loadInventoryFromNBT(final NBTTagList par1NBTTagList) {
		int var2;

		for (var2 = 0; var2 < getSizeInventory(); ++var2) {
			setInventorySlotContents(var2, (ItemStack) null);
		}

		for (var2 = 0; var2 < par1NBTTagList.tagCount(); ++var2) {
			final NBTTagCompound var3 = (NBTTagCompound) par1NBTTagList
					.tagAt(var2);
			final int var4 = var3.getByte("Slot") & 255;

			if (var4 >= 0 && var4 < getSizeInventory()) {
				setInventorySlotContents(var4,
						ItemStack.loadItemStackFromNBT(var3));
			}
		}
	}

	public NBTTagList saveInventoryToNBT() {
		final NBTTagList var1 = new NBTTagList("EnderItems");

		for (int var2 = 0; var2 < getSizeInventory(); ++var2) {
			final ItemStack var3 = getStackInSlot(var2);

			if (var3 != null) {
				final NBTTagCompound var4 = new NBTTagCompound();
				var4.setByte("Slot", (byte) var2);
				var3.writeToNBT(var4);
				var1.appendTag(var4);
			}
		}

		return var1;
	}

	/**
	 * Do not make give this method the name canInteractWith because it clashes
	 * with Container
	 */
	@Override
	public boolean isUseableByPlayer(final EntityPlayer par1EntityPlayer) {
		return associatedChest != null
				&& !associatedChest.isUseableByPlayer(par1EntityPlayer) ? false
				: super.isUseableByPlayer(par1EntityPlayer);
	}

	@Override
	public void openChest() {
		if (associatedChest != null) {
			associatedChest.openChest();
		}

		super.openChest();
	}

	@Override
	public void closeChest() {
		if (associatedChest != null) {
			associatedChest.closeChest();
		}

		super.closeChest();
		associatedChest = null;
	}

	/**
	 * Returns true if automation is allowed to insert the given stack (ignoring
	 * stack size) into the given slot.
	 */
	@Override
	public boolean isStackValidForSlot(final int par1,
			final ItemStack par2ItemStack) {
		return true;
	}
}
