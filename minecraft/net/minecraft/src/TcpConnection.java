package net.minecraft.src;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import javax.crypto.SecretKey;

import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.events.packet.PacketReadEvent;

public class TcpConnection implements INetworkManager {
	public static AtomicInteger field_74471_a = new AtomicInteger();
	public static AtomicInteger field_74469_b = new AtomicInteger();

	/** The object used for synchronization on the send queue. */
	private final Object sendQueueLock;
	private final ILogAgent field_98215_i;

	/** The socket used by this network manager. */
	private Socket networkSocket;

	/** The InetSocketAddress of the remote endpoint */
	private final SocketAddress remoteSocketAddress;

	/** The input stream connected to the socket. */
	private volatile DataInputStream socketInputStream;

	/** The output stream connected to the socket. */
	private volatile DataOutputStream socketOutputStream;

	/** Whether the network is currently operational. */
	private volatile boolean isRunning;

	/**
	 * Whether this network manager is currently terminating (and should ignore
	 * further errors).
	 */
	private volatile boolean isTerminating;

	/**
	 * Linked list of packets that have been read and are awaiting processing.
	 */
	private final List readPackets;

	/** Linked list of packets awaiting sending. */
	private final List dataPackets;

	/** Linked list of packets with chunk data that are awaiting sending. */
	private final List chunkDataPackets;

	/** A reference to the NetHandler object. */
	private NetHandler theNetHandler;

	/**
	 * Whether this server is currently terminating. If this is a client, this
	 * is always false.
	 */
	private boolean isServerTerminating;

	/** The thread used for writing. */
	private Thread writeThread;

	/** The thread used for reading. */
	private Thread readThread;

	/** A String indicating why the network has shutdown. */
	private String terminationReason;
	private Object[] field_74480_w;
	private int field_74490_x;

	/**
	 * The length in bytes of the packets in both send queues (data and
	 * chunkData).
	 */
	private int sendQueueByteLength;
	public static int[] field_74470_c = new int[256];
	public static int[] field_74467_d = new int[256];
	public int field_74468_e;
	boolean isInputBeingDecrypted;
	boolean isOutputEncrypted;
	private SecretKey sharedKeyForEncryption;
	private PrivateKey field_74463_A;

	/**
	 * Delay for sending pending chunk data packets (as opposed to pending
	 * non-chunk data packets)
	 */
	private int chunkDataPacketsDelay;

	public TcpConnection(final ILogAgent par1ILogAgent,
			final Socket par2Socket, final String par3Str,
			final NetHandler par4NetHandler) throws IOException {
		this(par1ILogAgent, par2Socket, par3Str, par4NetHandler,
				(PrivateKey) null);
	}

	public TcpConnection(final ILogAgent par1ILogAgent,
			final Socket par2Socket, final String par3Str,
			final NetHandler par4NetHandler, final PrivateKey par5PrivateKey)
			throws IOException {
		sendQueueLock = new Object();
		isRunning = true;
		isTerminating = false;
		readPackets = Collections.synchronizedList(new ArrayList());
		dataPackets = Collections.synchronizedList(new ArrayList());
		chunkDataPackets = Collections.synchronizedList(new ArrayList());
		isServerTerminating = false;
		terminationReason = "";
		field_74490_x = 0;
		sendQueueByteLength = 0;
		field_74468_e = 0;
		isInputBeingDecrypted = false;
		isOutputEncrypted = false;
		sharedKeyForEncryption = null;
		field_74463_A = null;
		chunkDataPacketsDelay = 50;
		field_74463_A = par5PrivateKey;
		networkSocket = par2Socket;
		field_98215_i = par1ILogAgent;
		remoteSocketAddress = par2Socket.getRemoteSocketAddress();
		theNetHandler = par4NetHandler;

		try {
			par2Socket.setSoTimeout(30000);
			par2Socket.setTrafficClass(24);
		} catch (final SocketException var7) {
			System.err.println(var7.getMessage());
		}

		socketInputStream = new DataInputStream(par2Socket.getInputStream());
		socketOutputStream = new DataOutputStream(new BufferedOutputStream(
				par2Socket.getOutputStream(), 5120));
		readThread = new TcpReaderThread(this, par3Str + " read thread");
		writeThread = new TcpWriterThread(this, par3Str + " write thread");
		readThread.start();
		writeThread.start();
	}

	@Override
	public void closeConnections() {
		wakeThreads();
		writeThread = null;
		readThread = null;
	}

	/**
	 * Sets the NetHandler for this NetworkManager. Server-only.
	 */
	@Override
	public void setNetHandler(final NetHandler par1NetHandler) {
		theNetHandler = par1NetHandler;
	}

	/**
	 * Adds the packet to the correct send queue (chunk data packets go to a
	 * separate queue).
	 */
	@Override
	public void addToSendQueue(final Packet par1Packet) {
		if (!isServerTerminating) {
			synchronized (sendQueueLock) {
				sendQueueByteLength += par1Packet.getPacketSize() + 1;
				dataPackets.add(par1Packet);
			}
		}
	}

	/**
	 * Sends a data packet if there is one to send, or sends a chunk data packet
	 * if there is one and the counter is up, or does nothing.
	 */
	private boolean sendPacket() {
		boolean var1 = false;

		try {
			Packet var2;
			int var10001;
			int[] var10000;

			if (field_74468_e == 0
					|| !dataPackets.isEmpty()
					&& System.currentTimeMillis()
							- ((Packet) dataPackets.get(0)).creationTimeMillis >= field_74468_e) {
				var2 = func_74460_a(false);

				if (var2 != null) {
					Packet.writePacket(var2, socketOutputStream);

					if (var2 instanceof Packet252SharedKey
							&& !isOutputEncrypted) {
						if (!theNetHandler.isServerHandler()) {
							sharedKeyForEncryption = ((Packet252SharedKey) var2)
									.getSharedKey();
						}

						encryptOuputStream();
					}

					var10000 = TcpConnection.field_74467_d;
					var10001 = var2.getPacketId();
					var10000[var10001] += var2.getPacketSize() + 1;
					var1 = true;
				}
			}

			if (chunkDataPacketsDelay-- <= 0
					&& (field_74468_e == 0 || !chunkDataPackets.isEmpty()
							&& System.currentTimeMillis()
									- ((Packet) chunkDataPackets.get(0)).creationTimeMillis >= field_74468_e)) {
				var2 = func_74460_a(true);

				if (var2 != null) {
					Packet.writePacket(var2, socketOutputStream);
					var10000 = TcpConnection.field_74467_d;
					var10001 = var2.getPacketId();
					var10000[var10001] += var2.getPacketSize() + 1;
					chunkDataPacketsDelay = 0;
					var1 = true;
				}
			}

			return var1;
		} catch (final Exception var3) {
			if (!isTerminating) {
				onNetworkError(var3);
			}

			return false;
		}
	}

	private Packet func_74460_a(final boolean par1) {
		Packet var2 = null;
		final List var3 = par1 ? chunkDataPackets : dataPackets;
		synchronized (sendQueueLock) {
			while (!var3.isEmpty() && var2 == null) {
				var2 = (Packet) var3.remove(0);
				sendQueueByteLength -= var2.getPacketSize() + 1;

				if (func_74454_a(var2, par1)) {
					var2 = null;
				}
			}

			return var2;
		}
	}

	private boolean func_74454_a(final Packet par1Packet, final boolean par2) {
		if (!par1Packet.isRealPacket()) {
			return false;
		} else {
			final List var3 = par2 ? chunkDataPackets : dataPackets;
			final Iterator var4 = var3.iterator();
			Packet var5;

			do {
				if (!var4.hasNext()) {
					return false;
				}

				var5 = (Packet) var4.next();
			} while (var5.getPacketId() != par1Packet.getPacketId());

			return par1Packet.containsSameEntityIDAs(var5);
		}
	}

	/**
	 * Wakes reader and writer threads
	 */
	@Override
	public void wakeThreads() {
		if (readThread != null) {
			readThread.interrupt();
		}

		if (writeThread != null) {
			writeThread.interrupt();
		}
	}

	/**
	 * Reads a single packet from the input stream and adds it to the read
	 * queue. If no packet is read, it shuts down the network.
	 */
	private boolean readPacket() {
		boolean var1 = false;

		try {
			final Packet var2 = Packet.readPacket(field_98215_i,
					socketInputStream, theNetHandler.isServerHandler(),
					networkSocket);

			// TODO: TcpConnection#readPacket
			final PacketReadEvent event = new PacketReadEvent(var2);
			Pringles.getInstance().getFactory().getEventManager()
					.sendEvent(event);

			if (var2 != null) {
				if (var2 instanceof Packet252SharedKey
						&& !isInputBeingDecrypted) {
					if (theNetHandler.isServerHandler()) {
						sharedKeyForEncryption = ((Packet252SharedKey) var2)
								.getSharedKey(field_74463_A);
					}

					decryptInputStream();
				}

				final int[] var10000 = TcpConnection.field_74470_c;
				final int var10001 = var2.getPacketId();
				var10000[var10001] += var2.getPacketSize() + 1;

				if (!isServerTerminating) {
					if (var2.canProcessAsync()
							&& theNetHandler.canProcessPacketsAsync()) {
						field_74490_x = 0;
						var2.processPacket(theNetHandler);
					} else {
						readPackets.add(var2);
					}
				}

				var1 = true;
			} else {
				networkShutdown("disconnect.endOfStream", new Object[0]);
			}

			return var1;
		} catch (final Exception var3) {
			if (!isTerminating) {
				onNetworkError(var3);
			}

			return false;
		}
	}

	/**
	 * Used to report network errors and causes a network shutdown.
	 */
	private void onNetworkError(final Exception par1Exception) {
		par1Exception.printStackTrace();
		networkShutdown(
				"disconnect.genericReason",
				new Object[] { "Internal exception: "
						+ par1Exception.toString() });
	}

	/**
	 * Shuts down the network with the specified reason. Closes all streams and
	 * sockets, spawns NetworkMasterThread to stop reading and writing threads.
	 */
	@Override
	public void networkShutdown(final String par1Str,
			final Object... par2ArrayOfObj) {
		if (isRunning) {
			isTerminating = true;
			terminationReason = par1Str;
			field_74480_w = par2ArrayOfObj;
			isRunning = false;
			new TcpMasterThread(this).start();

			try {
				socketInputStream.close();
			} catch (final Throwable var6) {
				;
			}

			try {
				socketOutputStream.close();
			} catch (final Throwable var5) {
				;
			}

			try {
				networkSocket.close();
			} catch (final Throwable var4) {
				;
			}

			socketInputStream = null;
			socketOutputStream = null;
			networkSocket = null;
		}
	}

	/**
	 * Checks timeouts and processes all pending read packets.
	 */
	@Override
	public void processReadPackets() {
		if (sendQueueByteLength > 2097152) {
			networkShutdown("disconnect.overflow", new Object[0]);
		}

		if (readPackets.isEmpty()) {
			if (field_74490_x++ == 1200) {
				networkShutdown("disconnect.timeout", new Object[0]);
			}
		} else {
			field_74490_x = 0;
		}

		int var1 = 1000;

		while (!readPackets.isEmpty() && var1-- >= 0) {
			final Packet var2 = (Packet) readPackets.remove(0);
			var2.processPacket(theNetHandler);
		}

		wakeThreads();

		if (isTerminating && readPackets.isEmpty()) {
			theNetHandler.handleErrorMessage(terminationReason, field_74480_w);
		}
	}

	/**
	 * Return the InetSocketAddress of the remote endpoint
	 */
	@Override
	public SocketAddress getSocketAddress() {
		return remoteSocketAddress;
	}

	/**
	 * Shuts down the server. (Only actually used on the server)
	 */
	@Override
	public void serverShutdown() {
		if (!isServerTerminating) {
			wakeThreads();
			isServerTerminating = true;
			readThread.interrupt();
			new TcpMonitorThread(this).start();
		}
	}

	private void decryptInputStream() throws IOException {
		isInputBeingDecrypted = true;
		final InputStream var1 = networkSocket.getInputStream();
		socketInputStream = new DataInputStream(
				CryptManager.decryptInputStream(sharedKeyForEncryption, var1));
	}

	/**
	 * flushes the stream and replaces it with an encryptedOutputStream
	 */
	private void encryptOuputStream() throws IOException {
		socketOutputStream.flush();
		isOutputEncrypted = true;
		final BufferedOutputStream var1 = new BufferedOutputStream(
				CryptManager.encryptOuputStream(sharedKeyForEncryption,
						networkSocket.getOutputStream()), 5120);
		socketOutputStream = new DataOutputStream(var1);
	}

	/**
	 * returns 0 for memoryConnections
	 */
	@Override
	public int packetSize() {
		return chunkDataPackets.size();
	}

	public Socket getSocket() {
		return networkSocket;
	}

	/**
	 * Whether the network is operational.
	 */
	static boolean isRunning(final TcpConnection par0TcpConnection) {
		return par0TcpConnection.isRunning;
	}

	/**
	 * Is the server terminating? Client side aways returns false.
	 */
	static boolean isServerTerminating(final TcpConnection par0TcpConnection) {
		return par0TcpConnection.isServerTerminating;
	}

	/**
	 * Static accessor to readPacket.
	 */
	static boolean readNetworkPacket(final TcpConnection par0TcpConnection) {
		return par0TcpConnection.readPacket();
	}

	/**
	 * Static accessor to sendPacket.
	 */
	static boolean sendNetworkPacket(final TcpConnection par0TcpConnection) {
		return par0TcpConnection.sendPacket();
	}

	static DataOutputStream getOutputStream(
			final TcpConnection par0TcpConnection) {
		return par0TcpConnection.socketOutputStream;
	}

	/**
	 * Gets whether the Network manager is terminating.
	 */
	static boolean isTerminating(final TcpConnection par0TcpConnection) {
		return par0TcpConnection.isTerminating;
	}

	/**
	 * Sends the network manager an error
	 */
	static void sendError(final TcpConnection par0TcpConnection,
			final Exception par1Exception) {
		par0TcpConnection.onNetworkError(par1Exception);
	}

	/**
	 * Returns the read thread.
	 */
	static Thread getReadThread(final TcpConnection par0TcpConnection) {
		return par0TcpConnection.readThread;
	}

	/**
	 * Returns the write thread.
	 */
	static Thread getWriteThread(final TcpConnection par0TcpConnection) {
		return par0TcpConnection.writeThread;
	}
}
