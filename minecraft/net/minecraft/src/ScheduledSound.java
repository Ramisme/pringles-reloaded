package net.minecraft.src;

class ScheduledSound {
	String field_92069_a;
	float field_92067_b;
	float field_92068_c;
	float field_92065_d;
	float field_92066_e;
	float field_92063_f;
	int field_92064_g;

	public ScheduledSound(final String par1Str, final float par2,
			final float par3, final float par4, final float par5,
			final float par6, final int par7) {
		field_92069_a = par1Str;
		field_92067_b = par2;
		field_92068_c = par3;
		field_92065_d = par4;
		field_92066_e = par5;
		field_92063_f = par6;
		field_92064_g = par7;
	}
}
