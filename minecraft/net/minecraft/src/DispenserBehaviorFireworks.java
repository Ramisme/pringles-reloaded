package net.minecraft.src;

final class DispenserBehaviorFireworks extends BehaviorDefaultDispenseItem {
	/**
	 * Dispense the specified stack, play the dispense sound and spawn
	 * particles.
	 */
	@Override
	public ItemStack dispenseStack(final IBlockSource par1IBlockSource,
			final ItemStack par2ItemStack) {
		final EnumFacing var3 = BlockDispenser.getFacing(par1IBlockSource
				.getBlockMetadata());
		final double var4 = par1IBlockSource.getX() + var3.getFrontOffsetX();
		final double var6 = par1IBlockSource.getYInt() + 0.2F;
		final double var8 = par1IBlockSource.getZ() + var3.getFrontOffsetZ();
		final EntityFireworkRocket var10 = new EntityFireworkRocket(
				par1IBlockSource.getWorld(), var4, var6, var8, par2ItemStack);
		par1IBlockSource.getWorld().spawnEntityInWorld(var10);
		par2ItemStack.splitStack(1);
		return par2ItemStack;
	}

	/**
	 * Play the dispense sound from the specified block.
	 */
	@Override
	protected void playDispenseSound(final IBlockSource par1IBlockSource) {
		par1IBlockSource.getWorld().playAuxSFX(1002,
				par1IBlockSource.getXInt(), par1IBlockSource.getYInt(),
				par1IBlockSource.getZInt(), 0);
	}
}
