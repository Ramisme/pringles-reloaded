package net.minecraft.src;

public class IconFlipped implements Icon {
	private final Icon baseIcon;
	private final boolean flipU;
	private final boolean flipV;

	public IconFlipped(final Icon par1Icon, final boolean par2,
			final boolean par3) {
		baseIcon = par1Icon;
		flipU = par2;
		flipV = par3;
	}

	/**
	 * Returns the X position of this icon on its texture sheet, in pixels.
	 */
	@Override
	public int getOriginX() {
		return baseIcon.getOriginX();
	}

	/**
	 * Returns the Y position of this icon on its texture sheet, in pixels.
	 */
	@Override
	public int getOriginY() {
		return baseIcon.getOriginY();
	}

	/**
	 * Returns the minimum U coordinate to use when rendering with this icon.
	 */
	@Override
	public float getMinU() {
		return flipU ? baseIcon.getMaxU() : baseIcon.getMinU();
	}

	/**
	 * Returns the maximum U coordinate to use when rendering with this icon.
	 */
	@Override
	public float getMaxU() {
		return flipU ? baseIcon.getMinU() : baseIcon.getMaxU();
	}

	/**
	 * Gets a U coordinate on the icon. 0 returns uMin and 16 returns uMax.
	 * Other arguments return in-between values.
	 */
	@Override
	public float getInterpolatedU(final double par1) {
		final float var3 = getMaxU() - getMinU();
		return getMinU() + var3 * ((float) par1 / 16.0F);
	}

	/**
	 * Returns the minimum V coordinate to use when rendering with this icon.
	 */
	@Override
	public float getMinV() {
		return flipV ? baseIcon.getMinV() : baseIcon.getMinV();
	}

	/**
	 * Returns the maximum V coordinate to use when rendering with this icon.
	 */
	@Override
	public float getMaxV() {
		return flipV ? baseIcon.getMinV() : baseIcon.getMaxV();
	}

	/**
	 * Gets a V coordinate on the icon. 0 returns vMin and 16 returns vMax.
	 * Other arguments return in-between values.
	 */
	@Override
	public float getInterpolatedV(final double par1) {
		final float var3 = getMaxV() - getMinV();
		return getMinV() + var3 * ((float) par1 / 16.0F);
	}

	@Override
	public String getIconName() {
		return baseIcon.getIconName();
	}

	/**
	 * Returns the width of the texture sheet this icon is on, in pixels.
	 */
	@Override
	public int getSheetWidth() {
		return baseIcon.getSheetWidth();
	}

	/**
	 * Returns the height of the texture sheet this icon is on, in pixels.
	 */
	@Override
	public int getSheetHeight() {
		return baseIcon.getSheetHeight();
	}
}
