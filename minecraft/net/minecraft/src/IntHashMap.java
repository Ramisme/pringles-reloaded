package net.minecraft.src;

import java.util.HashSet;
import java.util.Set;

public class IntHashMap {
	/** An array of HashEntries representing the heads of hash slot lists */
	private transient IntHashMapEntry[] slots = new IntHashMapEntry[16];

	/** The number of items stored in this map */
	private transient int count;

	/** The grow threshold */
	private int threshold = 12;

	/** The scale factor used to determine when to grow the table */
	private final float growFactor = 0.75F;

	/** The set of all the keys stored in this MCHash object */
	private final Set keySet = new HashSet();

	/**
	 * Makes the passed in integer suitable for hashing by a number of shifts
	 */
	private static int computeHash(int par0) {
		par0 ^= par0 >>> 20 ^ par0 >>> 12;
		return par0 ^ par0 >>> 7 ^ par0 >>> 4;
	}

	/**
	 * Computes the index of the slot for the hash and slot count passed in.
	 */
	private static int getSlotIndex(final int par0, final int par1) {
		return par0 & par1 - 1;
	}

	/**
	 * Returns the object associated to a key
	 */
	public Object lookup(final int par1) {
		final int var2 = IntHashMap.computeHash(par1);

		for (IntHashMapEntry var3 = slots[IntHashMap.getSlotIndex(var2,
				slots.length)]; var3 != null; var3 = var3.nextEntry) {
			if (var3.hashEntry == par1) {
				return var3.valueEntry;
			}
		}

		return null;
	}

	/**
	 * Return true if an object is associated with the given key
	 */
	public boolean containsItem(final int par1) {
		return lookupEntry(par1) != null;
	}

	/**
	 * Returns the key/object mapping for a given key as a MCHashEntry
	 */
	final IntHashMapEntry lookupEntry(final int par1) {
		final int var2 = IntHashMap.computeHash(par1);

		for (IntHashMapEntry var3 = slots[IntHashMap.getSlotIndex(var2,
				slots.length)]; var3 != null; var3 = var3.nextEntry) {
			if (var3.hashEntry == par1) {
				return var3;
			}
		}

		return null;
	}

	/**
	 * Adds a key and associated value to this map
	 */
	public void addKey(final int par1, final Object par2Obj) {
		keySet.add(Integer.valueOf(par1));
		final int var3 = IntHashMap.computeHash(par1);
		final int var4 = IntHashMap.getSlotIndex(var3, slots.length);

		for (IntHashMapEntry var5 = slots[var4]; var5 != null; var5 = var5.nextEntry) {
			if (var5.hashEntry == par1) {
				var5.valueEntry = par2Obj;
				return;
			}
		}

		insert(var3, par1, par2Obj, var4);
	}

	/**
	 * Increases the number of hash slots
	 */
	private void grow(final int par1) {
		final IntHashMapEntry[] var2 = slots;
		final int var3 = var2.length;

		if (var3 == 1073741824) {
			threshold = Integer.MAX_VALUE;
		} else {
			final IntHashMapEntry[] var4 = new IntHashMapEntry[par1];
			copyTo(var4);
			slots = var4;
			threshold = (int) (par1 * growFactor);
		}
	}

	/**
	 * Copies the hash slots to a new array
	 */
	private void copyTo(final IntHashMapEntry[] par1ArrayOfIntHashMapEntry) {
		final IntHashMapEntry[] var2 = slots;
		final int var3 = par1ArrayOfIntHashMapEntry.length;

		for (int var4 = 0; var4 < var2.length; ++var4) {
			IntHashMapEntry var5 = var2[var4];

			if (var5 != null) {
				var2[var4] = null;
				IntHashMapEntry var6;

				do {
					var6 = var5.nextEntry;
					final int var7 = IntHashMap.getSlotIndex(var5.slotHash,
							var3);
					var5.nextEntry = par1ArrayOfIntHashMapEntry[var7];
					par1ArrayOfIntHashMapEntry[var7] = var5;
					var5 = var6;
				} while (var6 != null);
			}
		}
	}

	/**
	 * Removes the specified object from the map and returns it
	 */
	public Object removeObject(final int par1) {
		keySet.remove(Integer.valueOf(par1));
		final IntHashMapEntry var2 = removeEntry(par1);
		return var2 == null ? null : var2.valueEntry;
	}

	/**
	 * Removes the specified entry from the map and returns it
	 */
	final IntHashMapEntry removeEntry(final int par1) {
		final int var2 = IntHashMap.computeHash(par1);
		final int var3 = IntHashMap.getSlotIndex(var2, slots.length);
		IntHashMapEntry var4 = slots[var3];
		IntHashMapEntry var5;
		IntHashMapEntry var6;

		for (var5 = var4; var5 != null; var5 = var6) {
			var6 = var5.nextEntry;

			if (var5.hashEntry == par1) {
				--count;

				if (var4 == var5) {
					slots[var3] = var6;
				} else {
					var4.nextEntry = var6;
				}

				return var5;
			}

			var4 = var5;
		}

		return var5;
	}

	/**
	 * Removes all entries from the map
	 */
	public void clearMap() {
		final IntHashMapEntry[] var1 = slots;

		for (int var2 = 0; var2 < var1.length; ++var2) {
			var1[var2] = null;
		}

		count = 0;
	}

	/**
	 * Adds an object to a slot
	 */
	private void insert(final int par1, final int par2, final Object par3Obj,
			final int par4) {
		final IntHashMapEntry var5 = slots[par4];
		slots[par4] = new IntHashMapEntry(par1, par2, par3Obj, var5);

		if (count++ >= threshold) {
			grow(2 * slots.length);
		}
	}

	/**
	 * Return the Set of all keys stored in this MCHash object
	 */
	public Set getKeySet() {
		return keySet;
	}

	/**
	 * Returns the hash code for a key
	 */
	static int getHash(final int par0) {
		return IntHashMap.computeHash(par0);
	}
}
