package net.minecraft.src;

public class EntityMinecartTNT extends EntityMinecart {
	private int minecartTNTFuse = -1;

	public EntityMinecartTNT(final World par1) {
		super(par1);
	}

	public EntityMinecartTNT(final World par1, final double par2,
			final double par4, final double par6) {
		super(par1, par2, par4, par6);
	}

	@Override
	public int getMinecartType() {
		return 3;
	}

	@Override
	public Block getDefaultDisplayTile() {
		return Block.tnt;
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate() {
		super.onUpdate();

		if (minecartTNTFuse > 0) {
			--minecartTNTFuse;
			worldObj.spawnParticle("smoke", posX, posY + 0.5D, posZ, 0.0D,
					0.0D, 0.0D);
		} else if (minecartTNTFuse == 0) {
			explodeCart(motionX * motionX + motionZ * motionZ);
		}

		if (isCollidedHorizontally) {
			final double var1 = motionX * motionX + motionZ * motionZ;

			if (var1 >= 0.009999999776482582D) {
				explodeCart(var1);
			}
		}
	}

	@Override
	public void killMinecart(final DamageSource par1DamageSource) {
		super.killMinecart(par1DamageSource);
		final double var2 = motionX * motionX + motionZ * motionZ;

		if (!par1DamageSource.isExplosion()) {
			entityDropItem(new ItemStack(Block.tnt, 1), 0.0F);
		}

		if (par1DamageSource.isFireDamage() || par1DamageSource.isExplosion()
				|| var2 >= 0.009999999776482582D) {
			explodeCart(var2);
		}
	}

	/**
	 * Makes the minecart explode.
	 */
	protected void explodeCart(final double par1) {
		if (!worldObj.isRemote) {
			double var3 = Math.sqrt(par1);

			if (var3 > 5.0D) {
				var3 = 5.0D;
			}

			worldObj.createExplosion(this, posX, posY, posZ,
					(float) (4.0D + rand.nextDouble() * 1.5D * var3), true);
			setDead();
		}
	}

	/**
	 * Called when the mob is falling. Calculates and applies fall damage.
	 */
	@Override
	protected void fall(final float par1) {
		if (par1 >= 3.0F) {
			final float var2 = par1 / 10.0F;
			explodeCart(var2 * var2);
		}

		super.fall(par1);
	}

	/**
	 * Called every tick the minecart is on an activator rail.
	 */
	@Override
	public void onActivatorRailPass(final int par1, final int par2,
			final int par3, final boolean par4) {
		if (par4 && minecartTNTFuse < 0) {
			ignite();
		}
	}

	@Override
	public void handleHealthUpdate(final byte par1) {
		if (par1 == 10) {
			ignite();
		} else {
			super.handleHealthUpdate(par1);
		}
	}

	/**
	 * Ignites this TNT cart.
	 */
	public void ignite() {
		minecartTNTFuse = 80;

		if (!worldObj.isRemote) {
			worldObj.setEntityState(this, (byte) 10);
			worldObj.playSoundAtEntity(this, "random.fuse", 1.0F, 1.0F);
		}
	}

	public int func_94104_d() {
		return minecartTNTFuse;
	}

	/**
	 * Returns true if the TNT minecart is ignited.
	 */
	public boolean isIgnited() {
		return minecartTNTFuse > -1;
	}

	@Override
	public float func_82146_a(final Explosion par1Explosion,
			final World par2World, final int par3, final int par4,
			final int par5, final Block par6Block) {
		return isIgnited()
				&& (BlockRailBase.isRailBlock(par6Block.blockID) || BlockRailBase
						.isRailBlockAt(par2World, par3, par4 + 1, par5)) ? 0.0F
				: super.func_82146_a(par1Explosion, par2World, par3, par4,
						par5, par6Block);
	}

	@Override
	public boolean func_96091_a(final Explosion par1Explosion,
			final World par2World, final int par3, final int par4,
			final int par5, final int par6, final float par7) {
		return isIgnited()
				&& (BlockRailBase.isRailBlock(par6) || BlockRailBase
						.isRailBlockAt(par2World, par3, par4 + 1, par5)) ? false
				: super.func_96091_a(par1Explosion, par2World, par3, par4,
						par5, par6, par7);
	}

	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	@Override
	protected void readEntityFromNBT(final NBTTagCompound par1NBTTagCompound) {
		super.readEntityFromNBT(par1NBTTagCompound);

		if (par1NBTTagCompound.hasKey("TNTFuse")) {
			minecartTNTFuse = par1NBTTagCompound.getInteger("TNTFuse");
		}
	}

	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	@Override
	protected void writeEntityToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeEntityToNBT(par1NBTTagCompound);
		par1NBTTagCompound.setInteger("TNTFuse", minecartTNTFuse);
	}
}
