package net.minecraft.src;

public class RConConsoleSource implements ICommandSender {
	/** only ever used by MinecraftServer.executeCommand */
	public static final RConConsoleSource consoleBuffer = new RConConsoleSource();

	/** RCon string buffer for log. */
	private final StringBuffer buffer = new StringBuffer();

	/**
	 * Clears the RCon log
	 */
	public void resetLog() {
		buffer.setLength(0);
	}

	public String getChatBuffer() {
		return buffer.toString();
	}

	/**
	 * Gets the name of this command sender (usually username, but possibly
	 * "Rcon")
	 */
	@Override
	public String getCommandSenderName() {
		return "Rcon";
	}

	@Override
	public void sendChatToPlayer(final String par1Str) {
		buffer.append(par1Str);
	}

	/**
	 * Returns true if the command sender is allowed to use the given command.
	 */
	@Override
	public boolean canCommandSenderUseCommand(final int par1,
			final String par2Str) {
		return true;
	}

	/**
	 * Translates and formats the given string key with the given arguments.
	 */
	@Override
	public String translateString(final String par1Str,
			final Object... par2ArrayOfObj) {
		return StringTranslate.getInstance().translateKeyFormat(par1Str,
				par2ArrayOfObj);
	}

	/**
	 * Return the position for this command sender.
	 */
	@Override
	public ChunkCoordinates getPlayerCoordinates() {
		return new ChunkCoordinates(0, 0, 0);
	}
}
