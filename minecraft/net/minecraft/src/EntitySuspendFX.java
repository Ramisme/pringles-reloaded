package net.minecraft.src;

public class EntitySuspendFX extends EntityFX {
	public EntitySuspendFX(final World par1World, final double par2,
			final double par4, final double par6, final double par8,
			final double par10, final double par12) {
		super(par1World, par2, par4 - 0.125D, par6, par8, par10, par12);
		particleRed = 0.4F;
		particleGreen = 0.4F;
		particleBlue = 0.7F;
		setParticleTextureIndex(0);
		setSize(0.01F, 0.01F);
		particleScale *= rand.nextFloat() * 0.6F + 0.2F;
		motionX = par8 * 0.0D;
		motionY = par10 * 0.0D;
		motionZ = par12 * 0.0D;
		particleMaxAge = (int) (16.0D / (Math.random() * 0.8D + 0.2D));
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate() {
		prevPosX = posX;
		prevPosY = posY;
		prevPosZ = posZ;
		moveEntity(motionX, motionY, motionZ);

		if (worldObj.getBlockMaterial(MathHelper.floor_double(posX),
				MathHelper.floor_double(posY), MathHelper.floor_double(posZ)) != Material.water) {
			setDead();
		}

		if (particleMaxAge-- <= 0) {
			setDead();
		}
	}
}
