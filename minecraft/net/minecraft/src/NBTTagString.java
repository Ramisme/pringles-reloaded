package net.minecraft.src;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class NBTTagString extends NBTBase {
	/** The string value for the tag (cannot be empty). */
	public String data;

	public NBTTagString(final String par1Str) {
		super(par1Str);
	}

	public NBTTagString(final String par1Str, final String par2Str) {
		super(par1Str);
		data = par2Str;

		if (par2Str == null) {
			throw new IllegalArgumentException("Empty string not allowed");
		}
	}

	/**
	 * Write the actual data contents of the tag, implemented in NBT extension
	 * classes
	 */
	@Override
	void write(final DataOutput par1DataOutput) throws IOException {
		par1DataOutput.writeUTF(data);
	}

	/**
	 * Read the actual data contents of the tag, implemented in NBT extension
	 * classes
	 */
	@Override
	void load(final DataInput par1DataInput) throws IOException {
		data = par1DataInput.readUTF();
	}

	/**
	 * Gets the type byte for the tag.
	 */
	@Override
	public byte getId() {
		return (byte) 8;
	}

	@Override
	public String toString() {
		return "" + data;
	}

	/**
	 * Creates a clone of the tag.
	 */
	@Override
	public NBTBase copy() {
		return new NBTTagString(getName(), data);
	}

	@Override
	public boolean equals(final Object par1Obj) {
		if (!super.equals(par1Obj)) {
			return false;
		} else {
			final NBTTagString var2 = (NBTTagString) par1Obj;
			return data == null && var2.data == null || data != null
					&& data.equals(var2.data);
		}
	}

	@Override
	public int hashCode() {
		return super.hashCode() ^ data.hashCode();
	}
}
