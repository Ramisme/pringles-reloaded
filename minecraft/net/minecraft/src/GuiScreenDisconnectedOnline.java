package net.minecraft.src;

import java.util.Iterator;
import java.util.List;

public class GuiScreenDisconnectedOnline extends GuiScreen {
	private final String field_98113_a;
	private final String field_98111_b;
	private final Object[] field_98112_c;
	private List field_98110_d;
	private final GuiScreen field_98114_n;

	public GuiScreenDisconnectedOnline(final GuiScreen par1GuiScreen,
			final String par2Str, final String par3Str,
			final Object... par4ArrayOfObj) {
		final StringTranslate var5 = StringTranslate.getInstance();
		field_98114_n = par1GuiScreen;
		field_98113_a = var5.translateKey(par2Str);
		field_98111_b = par3Str;
		field_98112_c = par4ArrayOfObj;
	}

	/**
	 * Fired when a key is typed. This is the equivalent of
	 * KeyListener.keyTyped(KeyEvent e).
	 */
	@Override
	protected void keyTyped(final char par1, final int par2) {
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui() {
		final StringTranslate var1 = StringTranslate.getInstance();
		buttonList.clear();
		buttonList.add(new GuiButton(0, width / 2 - 100, height / 4 + 120 + 12,
				var1.translateKey("gui.back")));

		if (field_98112_c != null) {
			field_98110_d = fontRenderer.listFormattedStringToWidth(
					var1.translateKeyFormat(field_98111_b, field_98112_c),
					width - 50);
		} else {
			field_98110_d = fontRenderer.listFormattedStringToWidth(
					var1.translateKey(field_98111_b), width - 50);
		}
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	@Override
	protected void actionPerformed(final GuiButton par1GuiButton) {
		if (par1GuiButton.id == 0) {
			mc.displayGuiScreen(field_98114_n);
		}
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(final int par1, final int par2, final float par3) {
		drawDefaultBackground();
		drawCenteredString(fontRenderer, field_98113_a, width / 2,
				height / 2 - 50, 11184810);
		int var4 = height / 2 - 30;

		if (field_98110_d != null) {
			for (final Iterator var5 = field_98110_d.iterator(); var5.hasNext(); var4 += fontRenderer.FONT_HEIGHT) {
				final String var6 = (String) var5.next();
				drawCenteredString(fontRenderer, var6, width / 2, var4,
						16777215);
			}
		}

		super.drawScreen(par1, par2, par3);
	}
}
