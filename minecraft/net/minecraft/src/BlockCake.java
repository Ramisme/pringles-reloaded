package net.minecraft.src;

import java.util.Random;

public class BlockCake extends Block {
	private Icon cakeTopIcon;
	private Icon cakeBottomIcon;
	private Icon field_94382_c;

	protected BlockCake(final int par1) {
		super(par1, Material.cake);
		setTickRandomly(true);
	}

	/**
	 * Updates the blocks bounds based on its current state. Args: world, x, y,
	 * z
	 */
	@Override
	public void setBlockBoundsBasedOnState(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		final int var5 = par1IBlockAccess.getBlockMetadata(par2, par3, par4);
		final float var6 = 0.0625F;
		final float var7 = (1 + var5 * 2) / 16.0F;
		final float var8 = 0.5F;
		setBlockBounds(var7, 0.0F, var6, 1.0F - var6, var8, 1.0F - var6);
	}

	/**
	 * Sets the block's bounds for rendering it as an item
	 */
	@Override
	public void setBlockBoundsForItemRender() {
		final float var1 = 0.0625F;
		final float var2 = 0.5F;
		setBlockBounds(var1, 0.0F, var1, 1.0F - var1, var2, 1.0F - var1);
	}

	/**
	 * Returns a bounding box from the pool of bounding boxes (this means this
	 * box can change after the pool has been cleared to be reused)
	 */
	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(final World par1World,
			final int par2, final int par3, final int par4) {
		final int var5 = par1World.getBlockMetadata(par2, par3, par4);
		final float var6 = 0.0625F;
		final float var7 = (1 + var5 * 2) / 16.0F;
		final float var8 = 0.5F;
		return AxisAlignedBB.getAABBPool().getAABB(par2 + var7, par3,
				par4 + var6, par2 + 1 - var6, par3 + var8 - var6,
				par4 + 1 - var6);
	}

	/**
	 * Returns the bounding box of the wired rectangular prism to render.
	 */
	@Override
	public AxisAlignedBB getSelectedBoundingBoxFromPool(final World par1World,
			final int par2, final int par3, final int par4) {
		final int var5 = par1World.getBlockMetadata(par2, par3, par4);
		final float var6 = 0.0625F;
		final float var7 = (1 + var5 * 2) / 16.0F;
		final float var8 = 0.5F;
		return AxisAlignedBB.getAABBPool().getAABB(par2 + var7, par3,
				par4 + var6, par2 + 1 - var6, par3 + var8, par4 + 1 - var6);
	}

	/**
	 * From the specified side and block metadata retrieves the blocks texture.
	 * Args: side, metadata
	 */
	@Override
	public Icon getIcon(final int par1, final int par2) {
		return par1 == 1 ? cakeTopIcon : par1 == 0 ? cakeBottomIcon : par2 > 0
				&& par1 == 4 ? field_94382_c : blockIcon;
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		blockIcon = par1IconRegister.registerIcon("cake_side");
		field_94382_c = par1IconRegister.registerIcon("cake_inner");
		cakeTopIcon = par1IconRegister.registerIcon("cake_top");
		cakeBottomIcon = par1IconRegister.registerIcon("cake_bottom");
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * Called upon block activation (right click on the block.)
	 */
	@Override
	public boolean onBlockActivated(final World par1World, final int par2,
			final int par3, final int par4,
			final EntityPlayer par5EntityPlayer, final int par6,
			final float par7, final float par8, final float par9) {
		eatCakeSlice(par1World, par2, par3, par4, par5EntityPlayer);
		return true;
	}

	/**
	 * Called when the block is clicked by a player. Args: x, y, z, entityPlayer
	 */
	@Override
	public void onBlockClicked(final World par1World, final int par2,
			final int par3, final int par4, final EntityPlayer par5EntityPlayer) {
		eatCakeSlice(par1World, par2, par3, par4, par5EntityPlayer);
	}

	/**
	 * Heals the player and removes a slice from the cake.
	 */
	private void eatCakeSlice(final World par1World, final int par2,
			final int par3, final int par4, final EntityPlayer par5EntityPlayer) {
		if (par5EntityPlayer.canEat(false)) {
			par5EntityPlayer.getFoodStats().addStats(2, 0.1F);
			final int var6 = par1World.getBlockMetadata(par2, par3, par4) + 1;

			if (var6 >= 6) {
				par1World.setBlockToAir(par2, par3, par4);
			} else {
				par1World.setBlockMetadataWithNotify(par2, par3, par4, var6, 2);
			}
		}
	}

	/**
	 * Checks to see if its valid to put this block at the specified
	 * coordinates. Args: world, x, y, z
	 */
	@Override
	public boolean canPlaceBlockAt(final World par1World, final int par2,
			final int par3, final int par4) {
		return !super.canPlaceBlockAt(par1World, par2, par3, par4) ? false
				: canBlockStay(par1World, par2, par3, par4);
	}

	/**
	 * Lets the block know when one of its neighbor changes. Doesn't know which
	 * neighbor changed (coordinates passed are their own) Args: x, y, z,
	 * neighbor blockID
	 */
	@Override
	public void onNeighborBlockChange(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		if (!canBlockStay(par1World, par2, par3, par4)) {
			par1World.setBlockToAir(par2, par3, par4);
		}
	}

	/**
	 * Can this block stay at this position. Similar to canPlaceBlockAt except
	 * gets checked often with plants.
	 */
	@Override
	public boolean canBlockStay(final World par1World, final int par2,
			final int par3, final int par4) {
		return par1World.getBlockMaterial(par2, par3 - 1, par4).isSolid();
	}

	/**
	 * Returns the quantity of items to drop on block destruction.
	 */
	@Override
	public int quantityDropped(final Random par1Random) {
		return 0;
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return 0;
	}

	/**
	 * only called by clickMiddleMouseButton , and passed to
	 * inventory.setCurrentItem (along with isCreative)
	 */
	@Override
	public int idPicked(final World par1World, final int par2, final int par3,
			final int par4) {
		return Item.cake.itemID;
	}
}
