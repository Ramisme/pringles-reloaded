package net.minecraft.src;

import java.net.URL;

public class SoundPoolEntry {
	public String soundName;
	public URL soundUrl;

	public SoundPoolEntry(final String par1Str, final URL par2URL) {
		soundName = par1Str;
		soundUrl = par2URL;
	}
}
