package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet63WorldParticles extends Packet {
	/**
	 * The name of the particle to create. A list can be found at
	 * https://gist.github.com/thinkofdeath/5110835
	 */
	private String particleName;

	/** X position of the particle. */
	private float posX;

	/** Y position of the particle. */
	private float posY;

	/** Z position of the particle. */
	private float posZ;

	/**
	 * This is added to the X position after being multiplied by
	 * random.nextGaussian()
	 */
	private float offsetX;

	/**
	 * This is added to the Y position after being multiplied by
	 * random.nextGaussian()
	 */
	private float offsetY;

	/**
	 * This is added to the Z position after being multiplied by
	 * random.nextGaussian()
	 */
	private float offsetZ;

	/** The speed of each particle. */
	private float speed;

	/** The number of particles to create. */
	private int quantity;

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		particleName = Packet.readString(par1DataInputStream, 64);
		posX = par1DataInputStream.readFloat();
		posY = par1DataInputStream.readFloat();
		posZ = par1DataInputStream.readFloat();
		offsetX = par1DataInputStream.readFloat();
		offsetY = par1DataInputStream.readFloat();
		offsetZ = par1DataInputStream.readFloat();
		speed = par1DataInputStream.readFloat();
		quantity = par1DataInputStream.readInt();
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		Packet.writeString(particleName, par1DataOutputStream);
		par1DataOutputStream.writeFloat(posX);
		par1DataOutputStream.writeFloat(posY);
		par1DataOutputStream.writeFloat(posZ);
		par1DataOutputStream.writeFloat(offsetX);
		par1DataOutputStream.writeFloat(offsetY);
		par1DataOutputStream.writeFloat(offsetZ);
		par1DataOutputStream.writeFloat(speed);
		par1DataOutputStream.writeInt(quantity);
	}

	public String getParticleName() {
		return particleName;
	}

	/**
	 * Gets the X position of the particle.
	 */
	public double getPositionX() {
		return posX;
	}

	/**
	 * Gets the Y position of the particle.
	 */
	public double getPositionY() {
		return posY;
	}

	/**
	 * Gets the Z position of the particle.
	 */
	public double getPositionZ() {
		return posZ;
	}

	/**
	 * This is added to the X position after being multiplied by
	 * random.nextGaussian()
	 */
	public float getOffsetX() {
		return offsetX;
	}

	/**
	 * This is added to the Y position after being multiplied by
	 * random.nextGaussian()
	 */
	public float getOffsetY() {
		return offsetY;
	}

	/**
	 * This is added to the Z position after being multiplied by
	 * random.nextGaussian()
	 */
	public float getOffsetZ() {
		return offsetZ;
	}

	/**
	 * Gets the speed of the particles.
	 */
	public float getSpeed() {
		return speed;
	}

	/**
	 * Gets the number of particles to create.
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleWorldParticles(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 64;
	}
}
