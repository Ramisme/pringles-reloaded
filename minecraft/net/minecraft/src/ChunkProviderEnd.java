package net.minecraft.src;

import java.util.List;
import java.util.Random;

public class ChunkProviderEnd implements IChunkProvider {
	private final Random endRNG;
	private final NoiseGeneratorOctaves noiseGen1;
	private final NoiseGeneratorOctaves noiseGen2;
	private final NoiseGeneratorOctaves noiseGen3;
	public NoiseGeneratorOctaves noiseGen4;
	public NoiseGeneratorOctaves noiseGen5;
	private final World endWorld;
	private double[] densities;

	/** The biomes that are used to generate the chunk */
	private BiomeGenBase[] biomesForGeneration;
	double[] noiseData1;
	double[] noiseData2;
	double[] noiseData3;
	double[] noiseData4;
	double[] noiseData5;
	int[][] field_73203_h = new int[32][32];

	public ChunkProviderEnd(final World par1World, final long par2) {
		endWorld = par1World;
		endRNG = new Random(par2);
		noiseGen1 = new NoiseGeneratorOctaves(endRNG, 16);
		noiseGen2 = new NoiseGeneratorOctaves(endRNG, 16);
		noiseGen3 = new NoiseGeneratorOctaves(endRNG, 8);
		noiseGen4 = new NoiseGeneratorOctaves(endRNG, 10);
		noiseGen5 = new NoiseGeneratorOctaves(endRNG, 16);
	}

	public void generateTerrain(final int par1, final int par2,
			final byte[] par3ArrayOfByte,
			final BiomeGenBase[] par4ArrayOfBiomeGenBase) {
		final byte var5 = 2;
		final int var6 = var5 + 1;
		final byte var7 = 33;
		final int var8 = var5 + 1;
		densities = initializeNoiseField(densities, par1 * var5, 0,
				par2 * var5, var6, var7, var8);

		for (int var9 = 0; var9 < var5; ++var9) {
			for (int var10 = 0; var10 < var5; ++var10) {
				for (int var11 = 0; var11 < 32; ++var11) {
					final double var12 = 0.25D;
					double var14 = densities[((var9 + 0) * var8 + var10 + 0)
							* var7 + var11 + 0];
					double var16 = densities[((var9 + 0) * var8 + var10 + 1)
							* var7 + var11 + 0];
					double var18 = densities[((var9 + 1) * var8 + var10 + 0)
							* var7 + var11 + 0];
					double var20 = densities[((var9 + 1) * var8 + var10 + 1)
							* var7 + var11 + 0];
					final double var22 = (densities[((var9 + 0) * var8 + var10 + 0)
							* var7 + var11 + 1] - var14)
							* var12;
					final double var24 = (densities[((var9 + 0) * var8 + var10 + 1)
							* var7 + var11 + 1] - var16)
							* var12;
					final double var26 = (densities[((var9 + 1) * var8 + var10 + 0)
							* var7 + var11 + 1] - var18)
							* var12;
					final double var28 = (densities[((var9 + 1) * var8 + var10 + 1)
							* var7 + var11 + 1] - var20)
							* var12;

					for (int var30 = 0; var30 < 4; ++var30) {
						final double var31 = 0.125D;
						double var33 = var14;
						double var35 = var16;
						final double var37 = (var18 - var14) * var31;
						final double var39 = (var20 - var16) * var31;

						for (int var41 = 0; var41 < 8; ++var41) {
							int var42 = var41 + var9 * 8 << 11
									| 0 + var10 * 8 << 7 | var11 * 4 + var30;
							final short var43 = 128;
							final double var44 = 0.125D;
							double var46 = var33;
							final double var48 = (var35 - var33) * var44;

							for (int var50 = 0; var50 < 8; ++var50) {
								int var51 = 0;

								if (var46 > 0.0D) {
									var51 = Block.whiteStone.blockID;
								}

								par3ArrayOfByte[var42] = (byte) var51;
								var42 += var43;
								var46 += var48;
							}

							var33 += var37;
							var35 += var39;
						}

						var14 += var22;
						var16 += var24;
						var18 += var26;
						var20 += var28;
					}
				}
			}
		}
	}

	public void replaceBlocksForBiome(final int par1, final int par2,
			final byte[] par3ArrayOfByte,
			final BiomeGenBase[] par4ArrayOfBiomeGenBase) {
		for (int var5 = 0; var5 < 16; ++var5) {
			for (int var6 = 0; var6 < 16; ++var6) {
				final byte var7 = 1;
				int var8 = -1;
				byte var9 = (byte) Block.whiteStone.blockID;
				byte var10 = (byte) Block.whiteStone.blockID;

				for (int var11 = 127; var11 >= 0; --var11) {
					final int var12 = (var6 * 16 + var5) * 128 + var11;
					final byte var13 = par3ArrayOfByte[var12];

					if (var13 == 0) {
						var8 = -1;
					} else if (var13 == Block.stone.blockID) {
						if (var8 == -1) {
							var8 = var7;

							if (var11 >= 0) {
								par3ArrayOfByte[var12] = var9;
							} else {
								par3ArrayOfByte[var12] = var10;
							}
						} else if (var8 > 0) {
							--var8;
							par3ArrayOfByte[var12] = var10;
						}
					}
				}
			}
		}
	}

	/**
	 * loads or generates the chunk at the chunk location specified
	 */
	@Override
	public Chunk loadChunk(final int par1, final int par2) {
		return provideChunk(par1, par2);
	}

	/**
	 * Will return back a chunk, if it doesn't exist and its not a MP client it
	 * will generates all the blocks for the specified chunk from the map seed
	 * and chunk seed
	 */
	@Override
	public Chunk provideChunk(final int par1, final int par2) {
		endRNG.setSeed(par1 * 341873128712L + par2 * 132897987541L);
		final byte[] var3 = new byte[32768];
		biomesForGeneration = endWorld.getWorldChunkManager()
				.loadBlockGeneratorData(biomesForGeneration, par1 * 16,
						par2 * 16, 16, 16);
		generateTerrain(par1, par2, var3, biomesForGeneration);
		replaceBlocksForBiome(par1, par2, var3, biomesForGeneration);
		final Chunk var4 = new Chunk(endWorld, var3, par1, par2);
		final byte[] var5 = var4.getBiomeArray();

		for (int var6 = 0; var6 < var5.length; ++var6) {
			var5[var6] = (byte) biomesForGeneration[var6].biomeID;
		}

		var4.generateSkylightMap();
		return var4;
	}

	/**
	 * generates a subset of the level's terrain data. Takes 7 arguments: the
	 * [empty] noise array, the position, and the size.
	 */
	private double[] initializeNoiseField(double[] par1ArrayOfDouble,
			final int par2, final int par3, final int par4, final int par5,
			final int par6, final int par7) {
		if (par1ArrayOfDouble == null) {
			par1ArrayOfDouble = new double[par5 * par6 * par7];
		}

		double var8 = 684.412D;
		final double var10 = 684.412D;
		noiseData4 = noiseGen4.generateNoiseOctaves(noiseData4, par2, par4,
				par5, par7, 1.121D, 1.121D, 0.5D);
		noiseData5 = noiseGen5.generateNoiseOctaves(noiseData5, par2, par4,
				par5, par7, 200.0D, 200.0D, 0.5D);
		var8 *= 2.0D;
		noiseData1 = noiseGen3.generateNoiseOctaves(noiseData1, par2, par3,
				par4, par5, par6, par7, var8 / 80.0D, var10 / 160.0D,
				var8 / 80.0D);
		noiseData2 = noiseGen1.generateNoiseOctaves(noiseData2, par2, par3,
				par4, par5, par6, par7, var8, var10, var8);
		noiseData3 = noiseGen2.generateNoiseOctaves(noiseData3, par2, par3,
				par4, par5, par6, par7, var8, var10, var8);
		int var12 = 0;
		int var13 = 0;

		for (int var14 = 0; var14 < par5; ++var14) {
			for (int var15 = 0; var15 < par7; ++var15) {
				double var16 = (noiseData4[var13] + 256.0D) / 512.0D;

				if (var16 > 1.0D) {
					var16 = 1.0D;
				}

				double var18 = noiseData5[var13] / 8000.0D;

				if (var18 < 0.0D) {
					var18 = -var18 * 0.3D;
				}

				var18 = var18 * 3.0D - 2.0D;
				final float var20 = (var14 + par2 - 0) / 1.0F;
				final float var21 = (var15 + par4 - 0) / 1.0F;
				float var22 = 100.0F - MathHelper.sqrt_float(var20 * var20
						+ var21 * var21) * 8.0F;

				if (var22 > 80.0F) {
					var22 = 80.0F;
				}

				if (var22 < -100.0F) {
					var22 = -100.0F;
				}

				if (var18 > 1.0D) {
					var18 = 1.0D;
				}

				var18 /= 8.0D;
				var18 = 0.0D;

				if (var16 < 0.0D) {
					var16 = 0.0D;
				}

				var16 += 0.5D;
				var18 = var18 * par6 / 16.0D;
				++var13;
				final double var23 = par6 / 2.0D;

				for (int var25 = 0; var25 < par6; ++var25) {
					double var26 = 0.0D;
					double var28 = (var25 - var23) * 8.0D / var16;

					if (var28 < 0.0D) {
						var28 *= -1.0D;
					}

					final double var30 = noiseData2[var12] / 512.0D;
					final double var32 = noiseData3[var12] / 512.0D;
					final double var34 = (noiseData1[var12] / 10.0D + 1.0D) / 2.0D;

					if (var34 < 0.0D) {
						var26 = var30;
					} else if (var34 > 1.0D) {
						var26 = var32;
					} else {
						var26 = var30 + (var32 - var30) * var34;
					}

					var26 -= 8.0D;
					var26 += var22;
					byte var36 = 2;
					double var37;

					if (var25 > par6 / 2 - var36) {
						var37 = (var25 - (par6 / 2 - var36)) / 64.0F;

						if (var37 < 0.0D) {
							var37 = 0.0D;
						}

						if (var37 > 1.0D) {
							var37 = 1.0D;
						}

						var26 = var26 * (1.0D - var37) + -3000.0D * var37;
					}

					var36 = 8;

					if (var25 < var36) {
						var37 = (var36 - var25) / (var36 - 1.0F);
						var26 = var26 * (1.0D - var37) + -30.0D * var37;
					}

					par1ArrayOfDouble[var12] = var26;
					++var12;
				}
			}
		}

		return par1ArrayOfDouble;
	}

	/**
	 * Checks to see if a chunk exists at x, y
	 */
	@Override
	public boolean chunkExists(final int par1, final int par2) {
		return true;
	}

	/**
	 * Populates chunk with ores etc etc
	 */
	@Override
	public void populate(final IChunkProvider par1IChunkProvider,
			final int par2, final int par3) {
		BlockSand.fallInstantly = true;
		final int var4 = par2 * 16;
		final int var5 = par3 * 16;
		final BiomeGenBase var6 = endWorld.getBiomeGenForCoords(var4 + 16,
				var5 + 16);
		var6.decorate(endWorld, endWorld.rand, var4, var5);
		BlockSand.fallInstantly = false;
	}

	/**
	 * Two modes of operation: if passed true, save all Chunks in one go. If
	 * passed false, save up to two chunks. Return true if all chunks have been
	 * saved.
	 */
	@Override
	public boolean saveChunks(final boolean par1,
			final IProgressUpdate par2IProgressUpdate) {
		return true;
	}

	@Override
	public void func_104112_b() {
	}

	/**
	 * Unloads chunks that are marked to be unloaded. This is not guaranteed to
	 * unload every such chunk.
	 */
	@Override
	public boolean unloadQueuedChunks() {
		return false;
	}

	/**
	 * Returns if the IChunkProvider supports saving.
	 */
	@Override
	public boolean canSave() {
		return true;
	}

	/**
	 * Converts the instance data to a readable string.
	 */
	@Override
	public String makeString() {
		return "RandomLevelSource";
	}

	/**
	 * Returns a list of creatures of the specified type that can spawn at the
	 * given location.
	 */
	@Override
	public List getPossibleCreatures(
			final EnumCreatureType par1EnumCreatureType, final int par2,
			final int par3, final int par4) {
		final BiomeGenBase var5 = endWorld.getBiomeGenForCoords(par2, par4);
		return var5 == null ? null : var5
				.getSpawnableList(par1EnumCreatureType);
	}

	/**
	 * Returns the location of the closest structure of the specified type. If
	 * not found returns null.
	 */
	@Override
	public ChunkPosition findClosestStructure(final World par1World,
			final String par2Str, final int par3, final int par4, final int par5) {
		return null;
	}

	@Override
	public int getLoadedChunkCount() {
		return 0;
	}

	@Override
	public void recreateStructures(final int par1, final int par2) {
	}
}
