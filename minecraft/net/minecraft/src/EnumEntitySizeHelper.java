package net.minecraft.src;

class EnumEntitySizeHelper {
	static final int[] field_96565_a = new int[EnumEntitySize.values().length];

	static {
		try {
			EnumEntitySizeHelper.field_96565_a[EnumEntitySize.SIZE_1.ordinal()] = 1;
		} catch (final NoSuchFieldError var6) {
			;
		}

		try {
			EnumEntitySizeHelper.field_96565_a[EnumEntitySize.SIZE_2.ordinal()] = 2;
		} catch (final NoSuchFieldError var5) {
			;
		}

		try {
			EnumEntitySizeHelper.field_96565_a[EnumEntitySize.SIZE_3.ordinal()] = 3;
		} catch (final NoSuchFieldError var4) {
			;
		}

		try {
			EnumEntitySizeHelper.field_96565_a[EnumEntitySize.SIZE_4.ordinal()] = 4;
		} catch (final NoSuchFieldError var3) {
			;
		}

		try {
			EnumEntitySizeHelper.field_96565_a[EnumEntitySize.SIZE_5.ordinal()] = 5;
		} catch (final NoSuchFieldError var2) {
			;
		}

		try {
			EnumEntitySizeHelper.field_96565_a[EnumEntitySize.SIZE_6.ordinal()] = 6;
		} catch (final NoSuchFieldError var1) {
			;
		}
	}
}
