package net.minecraft.src;

import net.minecraft.client.Minecraft;

import org.lwjgl.opengl.GL11;

public class RenderMagmaCube extends RenderLiving {
	private int field_77120_a;

	public RenderMagmaCube() {
		super(new ModelMagmaCube(), 0.25F);
		field_77120_a = ((ModelMagmaCube) mainModel).func_78107_a();
	}

	public void renderMagmaCube(final EntityMagmaCube par1EntityMagmaCube,
			final double par2, final double par4, final double par6,
			final float par8, final float par9) {
		final int var10 = ((ModelMagmaCube) mainModel).func_78107_a();

		if (var10 != field_77120_a) {
			field_77120_a = var10;
			mainModel = new ModelMagmaCube();
			Minecraft.getMinecraft().getLogAgent()
					.logInfo("Loaded new lava slime model");
		}

		super.doRenderLiving(par1EntityMagmaCube, par2, par4, par6, par8, par9);
	}

	protected void scaleMagmaCube(final EntityMagmaCube par1EntityMagmaCube,
			final float par2) {
		final int var3 = par1EntityMagmaCube.getSlimeSize();
		final float var4 = (par1EntityMagmaCube.field_70812_c + (par1EntityMagmaCube.field_70811_b - par1EntityMagmaCube.field_70812_c)
				* par2)
				/ (var3 * 0.5F + 1.0F);
		final float var5 = 1.0F / (var4 + 1.0F);
		final float var6 = var3;
		GL11.glScalef(var5 * var6, 1.0F / var5 * var6, var5 * var6);
	}

	/**
	 * Allows the render to do any OpenGL state modifications necessary before
	 * the model is rendered. Args: entityLiving, partialTickTime
	 */
	@Override
	protected void preRenderCallback(final EntityLiving par1EntityLiving,
			final float par2) {
		scaleMagmaCube((EntityMagmaCube) par1EntityLiving, par2);
	}

	@Override
	public void doRenderLiving(final EntityLiving par1EntityLiving,
			final double par2, final double par4, final double par6,
			final float par8, final float par9) {
		renderMagmaCube((EntityMagmaCube) par1EntityLiving, par2, par4, par6,
				par8, par9);
	}

	/**
	 * Actually renders the given argument. This is a synthetic bridge method,
	 * always casting down its argument and then handing it off to a worker
	 * function which does the actual work. In all probabilty, the class Render
	 * is generic (Render<T extends Entity) and this method has signature public
	 * void doRender(T entity, double d, double d1, double d2, float f, float
	 * f1). But JAD is pre 1.5 so doesn't do that.
	 */
	@Override
	public void doRender(final Entity par1Entity, final double par2,
			final double par4, final double par6, final float par8,
			final float par9) {
		renderMagmaCube((EntityMagmaCube) par1Entity, par2, par4, par6, par8,
				par9);
	}
}
