package net.minecraft.src;

import java.util.Random;

public class BlockCocoa extends BlockDirectional {
	public static final String[] cocoaIcons = new String[] { "cocoa_0",
			"cocoa_1", "cocoa_2" };
	private Icon[] iconArray;

	public BlockCocoa(final int par1) {
		super(par1, Material.plants);
		setTickRandomly(true);
	}

	/**
	 * From the specified side and block metadata retrieves the blocks texture.
	 * Args: side, metadata
	 */
	@Override
	public Icon getIcon(final int par1, final int par2) {
		return iconArray[2];
	}

	public Icon func_94468_i_(int par1) {
		if (par1 < 0 || par1 >= iconArray.length) {
			par1 = iconArray.length - 1;
		}

		return iconArray[par1];
	}

	/**
	 * Ticks the block if it's been scheduled
	 */
	@Override
	public void updateTick(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		if (!canBlockStay(par1World, par2, par3, par4)) {
			dropBlockAsItem(par1World, par2, par3, par4,
					par1World.getBlockMetadata(par2, par3, par4), 0);
			par1World.setBlockToAir(par2, par3, par4);
		} else if (par1World.rand.nextInt(5) == 0) {
			final int var6 = par1World.getBlockMetadata(par2, par3, par4);
			int var7 = BlockCocoa.func_72219_c(var6);

			if (var7 < 2) {
				++var7;
				par1World.setBlockMetadataWithNotify(par2, par3, par4,
						var7 << 2 | BlockDirectional.getDirection(var6), 2);
			}
		}
	}

	/**
	 * Can this block stay at this position. Similar to canPlaceBlockAt except
	 * gets checked often with plants.
	 */
	@Override
	public boolean canBlockStay(final World par1World, int par2,
			final int par3, int par4) {
		final int var5 = BlockDirectional.getDirection(par1World
				.getBlockMetadata(par2, par3, par4));
		par2 += Direction.offsetX[var5];
		par4 += Direction.offsetZ[var5];
		final int var6 = par1World.getBlockId(par2, par3, par4);
		return var6 == Block.wood.blockID
				&& BlockLog.limitToValidMetadata(par1World.getBlockMetadata(
						par2, par3, par4)) == 3;
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return 28;
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * Returns a bounding box from the pool of bounding boxes (this means this
	 * box can change after the pool has been cleared to be reused)
	 */
	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(final World par1World,
			final int par2, final int par3, final int par4) {
		setBlockBoundsBasedOnState(par1World, par2, par3, par4);
		return super.getCollisionBoundingBoxFromPool(par1World, par2, par3,
				par4);
	}

	/**
	 * Returns the bounding box of the wired rectangular prism to render.
	 */
	@Override
	public AxisAlignedBB getSelectedBoundingBoxFromPool(final World par1World,
			final int par2, final int par3, final int par4) {
		setBlockBoundsBasedOnState(par1World, par2, par3, par4);
		return super
				.getSelectedBoundingBoxFromPool(par1World, par2, par3, par4);
	}

	/**
	 * Updates the blocks bounds based on its current state. Args: world, x, y,
	 * z
	 */
	@Override
	public void setBlockBoundsBasedOnState(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		final int var5 = par1IBlockAccess.getBlockMetadata(par2, par3, par4);
		final int var6 = BlockDirectional.getDirection(var5);
		final int var7 = BlockCocoa.func_72219_c(var5);
		final int var8 = 4 + var7 * 2;
		final int var9 = 5 + var7 * 2;
		final float var10 = var8 / 2.0F;

		switch (var6) {
		case 0:
			setBlockBounds((8.0F - var10) / 16.0F, (12.0F - var9) / 16.0F,
					(15.0F - var8) / 16.0F, (8.0F + var10) / 16.0F, 0.75F,
					0.9375F);
			break;

		case 1:
			setBlockBounds(0.0625F, (12.0F - var9) / 16.0F,
					(8.0F - var10) / 16.0F, (1.0F + var8) / 16.0F, 0.75F,
					(8.0F + var10) / 16.0F);
			break;

		case 2:
			setBlockBounds((8.0F - var10) / 16.0F, (12.0F - var9) / 16.0F,
					0.0625F, (8.0F + var10) / 16.0F, 0.75F,
					(1.0F + var8) / 16.0F);
			break;

		case 3:
			setBlockBounds((15.0F - var8) / 16.0F, (12.0F - var9) / 16.0F,
					(8.0F - var10) / 16.0F, 0.9375F, 0.75F,
					(8.0F + var10) / 16.0F);
		}
	}

	/**
	 * Called when the block is placed in the world.
	 */
	@Override
	public void onBlockPlacedBy(final World par1World, final int par2,
			final int par3, final int par4,
			final EntityLiving par5EntityLiving, final ItemStack par6ItemStack) {
		final int var7 = ((MathHelper
				.floor_double(par5EntityLiving.rotationYaw * 4.0F / 360.0F + 0.5D) & 3) + 0) % 4;
		par1World.setBlockMetadataWithNotify(par2, par3, par4, var7, 2);
	}

	/**
	 * Called when a block is placed using its ItemBlock. Args: World, X, Y, Z,
	 * side, hitX, hitY, hitZ, block metadata
	 */
	@Override
	public int onBlockPlaced(final World par1World, final int par2,
			final int par3, final int par4, int par5, final float par6,
			final float par7, final float par8, final int par9) {
		if (par5 == 1 || par5 == 0) {
			par5 = 2;
		}

		return Direction.rotateOpposite[Direction.facingToDirection[par5]];
	}

	/**
	 * Lets the block know when one of its neighbor changes. Doesn't know which
	 * neighbor changed (coordinates passed are their own) Args: x, y, z,
	 * neighbor blockID
	 */
	@Override
	public void onNeighborBlockChange(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		if (!canBlockStay(par1World, par2, par3, par4)) {
			dropBlockAsItem(par1World, par2, par3, par4,
					par1World.getBlockMetadata(par2, par3, par4), 0);
			par1World.setBlockToAir(par2, par3, par4);
		}
	}

	public static int func_72219_c(final int par0) {
		return (par0 & 12) >> 2;
	}

	/**
	 * Drops the block items with a specified chance of dropping the specified
	 * items
	 */
	@Override
	public void dropBlockAsItemWithChance(final World par1World,
			final int par2, final int par3, final int par4, final int par5,
			final float par6, final int par7) {
		final int var8 = BlockCocoa.func_72219_c(par5);
		byte var9 = 1;

		if (var8 >= 2) {
			var9 = 3;
		}

		for (int var10 = 0; var10 < var9; ++var10) {
			dropBlockAsItem_do(par1World, par2, par3, par4, new ItemStack(
					Item.dyePowder, 1, 3));
		}
	}

	/**
	 * only called by clickMiddleMouseButton , and passed to
	 * inventory.setCurrentItem (along with isCreative)
	 */
	@Override
	public int idPicked(final World par1World, final int par2, final int par3,
			final int par4) {
		return Item.dyePowder.itemID;
	}

	/**
	 * Get the block's damage value (for use with pick block).
	 */
	@Override
	public int getDamageValue(final World par1World, final int par2,
			final int par3, final int par4) {
		return 3;
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		iconArray = new Icon[BlockCocoa.cocoaIcons.length];

		for (int var2 = 0; var2 < iconArray.length; ++var2) {
			iconArray[var2] = par1IconRegister
					.registerIcon(BlockCocoa.cocoaIcons[var2]);
		}
	}
}
