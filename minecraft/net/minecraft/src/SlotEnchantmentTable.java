package net.minecraft.src;

class SlotEnchantmentTable extends InventoryBasic {
	/** The brewing stand this slot belongs to. */
	final ContainerEnchantment container;

	SlotEnchantmentTable(final ContainerEnchantment par1ContainerEnchantment,
			final String par2Str, final boolean par3, final int par4) {
		super(par2Str, par3, par4);
		container = par1ContainerEnchantment;
	}

	/**
	 * Returns the maximum stack size for a inventory slot. Seems to always be
	 * 64, possibly will be extended. *Isn't this more of a set than a get?*
	 */
	@Override
	public int getInventoryStackLimit() {
		return 1;
	}

	/**
	 * Called when an the contents of an Inventory change, usually
	 */
	@Override
	public void onInventoryChanged() {
		super.onInventoryChanged();
		container.onCraftMatrixChanged(this);
	}

	/**
	 * Returns true if automation is allowed to insert the given stack (ignoring
	 * stack size) into the given slot.
	 */
	@Override
	public boolean isStackValidForSlot(final int par1,
			final ItemStack par2ItemStack) {
		return true;
	}
}
