package net.minecraft.src;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

public class TileEntitySkullRenderer extends TileEntitySpecialRenderer {
	public static TileEntitySkullRenderer skullRenderer;
	private final ModelSkeletonHead field_82396_c = new ModelSkeletonHead(0, 0,
			64, 32);
	private final ModelSkeletonHead field_82395_d = new ModelSkeletonHead(0, 0,
			64, 64);

	/**
	 * Render a skull tile entity.
	 */
	public void renderTileEntitySkullAt(
			final TileEntitySkull par1TileEntitySkull, final double par2,
			final double par4, final double par6, final float par8) {
		func_82393_a((float) par2, (float) par4, (float) par6,
				par1TileEntitySkull.getBlockMetadata() & 7,
				par1TileEntitySkull.func_82119_b() * 360 / 16.0F,
				par1TileEntitySkull.getSkullType(),
				par1TileEntitySkull.getExtraType());
	}

	/**
	 * Associate a TileEntityRenderer with this TileEntitySpecialRenderer
	 */
	@Override
	public void setTileEntityRenderer(
			final TileEntityRenderer par1TileEntityRenderer) {
		super.setTileEntityRenderer(par1TileEntityRenderer);
		TileEntitySkullRenderer.skullRenderer = this;
	}

	public void func_82393_a(final float par1, final float par2,
			final float par3, final int par4, float par5, final int par6,
			final String par7Str) {
		ModelSkeletonHead var8 = field_82396_c;

		switch (par6) {
		case 0:
		default:
			bindTextureByName("/mob/skeleton.png");
			break;

		case 1:
			bindTextureByName("/mob/skeleton_wither.png");
			break;

		case 2:
			bindTextureByName("/mob/zombie.png");
			var8 = field_82395_d;
			break;

		case 3:
			if (par7Str != null && par7Str.length() > 0) {
				final String var9 = "http://skins.minecraft.net/MinecraftSkins/"
						+ StringUtils.stripControlCodes(par7Str) + ".png";

				if (!TileEntitySkullRenderer.skullRenderer.tileEntityRenderer.renderEngine
						.hasImageData(var9)) {
					TileEntitySkullRenderer.skullRenderer.tileEntityRenderer.renderEngine
							.obtainImageData(var9, new ImageBufferDownload());
				}

				bindTextureByURL(var9, "/mob/char.png");
			} else {
				bindTextureByName("/mob/char.png");
			}

			break;

		case 4:
			bindTextureByName("/mob/creeper.png");
		}

		GL11.glPushMatrix();
		GL11.glDisable(GL11.GL_CULL_FACE);

		if (par4 != 1) {
			switch (par4) {
			case 2:
				GL11.glTranslatef(par1 + 0.5F, par2 + 0.25F, par3 + 0.74F);
				break;

			case 3:
				GL11.glTranslatef(par1 + 0.5F, par2 + 0.25F, par3 + 0.26F);
				par5 = 180.0F;
				break;

			case 4:
				GL11.glTranslatef(par1 + 0.74F, par2 + 0.25F, par3 + 0.5F);
				par5 = 270.0F;
				break;

			case 5:
			default:
				GL11.glTranslatef(par1 + 0.26F, par2 + 0.25F, par3 + 0.5F);
				par5 = 90.0F;
			}
		} else {
			GL11.glTranslatef(par1 + 0.5F, par2, par3 + 0.5F);
		}

		final float var10 = 0.0625F;
		GL11.glEnable(GL12.GL_RESCALE_NORMAL);
		GL11.glScalef(-1.0F, -1.0F, 1.0F);
		GL11.glEnable(GL11.GL_ALPHA_TEST);
		var8.render((Entity) null, 0.0F, 0.0F, 0.0F, par5, 0.0F, var10);
		GL11.glPopMatrix();
	}

	@Override
	public void renderTileEntityAt(final TileEntity par1TileEntity,
			final double par2, final double par4, final double par6,
			final float par8) {
		renderTileEntitySkullAt((TileEntitySkull) par1TileEntity, par2, par4,
				par6, par8);
	}
}
