package net.minecraft.src;

import java.util.List;
import java.util.Random;

public class ComponentVillageTorch extends ComponentVillage {
	private int averageGroundLevel = -1;

	public ComponentVillageTorch(
			final ComponentVillageStartPiece par1ComponentVillageStartPiece,
			final int par2, final Random par3Random,
			final StructureBoundingBox par4StructureBoundingBox, final int par5) {
		super(par1ComponentVillageStartPiece, par2);
		coordBaseMode = par5;
		boundingBox = par4StructureBoundingBox;
	}

	public static StructureBoundingBox func_74904_a(
			final ComponentVillageStartPiece par0ComponentVillageStartPiece,
			final List par1List, final Random par2Random, final int par3,
			final int par4, final int par5, final int par6) {
		final StructureBoundingBox var7 = StructureBoundingBox
				.getComponentToAddBoundingBox(par3, par4, par5, 0, 0, 0, 3, 4,
						2, par6);
		return StructureComponent.findIntersecting(par1List, var7) != null ? null
				: var7;
	}

	/**
	 * second Part of Structure generating, this for example places Spiderwebs,
	 * Mob Spawners, it closes Mineshafts at the end, it adds Fences...
	 */
	@Override
	public boolean addComponentParts(final World par1World,
			final Random par2Random,
			final StructureBoundingBox par3StructureBoundingBox) {
		if (averageGroundLevel < 0) {
			averageGroundLevel = getAverageGroundLevel(par1World,
					par3StructureBoundingBox);

			if (averageGroundLevel < 0) {
				return true;
			}

			boundingBox.offset(0,
					averageGroundLevel - boundingBox.maxY + 4 - 1, 0);
		}

		fillWithBlocks(par1World, par3StructureBoundingBox, 0, 0, 0, 2, 3, 1,
				0, 0, false);
		placeBlockAtCurrentPosition(par1World, Block.fence.blockID, 0, 1, 0, 0,
				par3StructureBoundingBox);
		placeBlockAtCurrentPosition(par1World, Block.fence.blockID, 0, 1, 1, 0,
				par3StructureBoundingBox);
		placeBlockAtCurrentPosition(par1World, Block.fence.blockID, 0, 1, 2, 0,
				par3StructureBoundingBox);
		placeBlockAtCurrentPosition(par1World, Block.cloth.blockID, 15, 1, 3,
				0, par3StructureBoundingBox);
		placeBlockAtCurrentPosition(par1World, Block.torchWood.blockID, 0, 0,
				3, 0, par3StructureBoundingBox);
		placeBlockAtCurrentPosition(par1World, Block.torchWood.blockID, 0, 1,
				3, 1, par3StructureBoundingBox);
		placeBlockAtCurrentPosition(par1World, Block.torchWood.blockID, 0, 2,
				3, 0, par3StructureBoundingBox);
		placeBlockAtCurrentPosition(par1World, Block.torchWood.blockID, 0, 1,
				3, -1, par3StructureBoundingBox);
		return true;
	}
}
