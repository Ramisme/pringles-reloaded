package net.minecraft.src;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.lwjgl.input.Keyboard;

public class GuiFlatPresets extends GuiScreen {
	/** RenderItem instance used to render preset icons. */
	private static RenderItem presetIconRenderer = new RenderItem();

	/** List of defined flat world presets. */
	private static final List presets = new ArrayList();
	private final GuiCreateFlatWorld createFlatWorldGui;
	private String field_82300_d;
	private String field_82308_m;
	private String field_82306_n;
	private GuiFlatPresetsListSlot theFlatPresetsListSlot;
	private GuiButton theButton;
	private GuiTextField theTextField;

	public GuiFlatPresets(final GuiCreateFlatWorld par1) {
		createFlatWorldGui = par1;
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui() {
		buttonList.clear();
		Keyboard.enableRepeatEvents(true);
		field_82300_d = StatCollector
				.translateToLocal("createWorld.customize.presets.title");
		field_82308_m = StatCollector
				.translateToLocal("createWorld.customize.presets.share");
		field_82306_n = StatCollector
				.translateToLocal("createWorld.customize.presets.list");
		theTextField = new GuiTextField(fontRenderer, 50, 40, width - 100, 20);
		theFlatPresetsListSlot = new GuiFlatPresetsListSlot(this);
		theTextField.setMaxStringLength(1230);
		theTextField.setText(createFlatWorldGui.getFlatGeneratorInfo());
		buttonList
				.add(theButton = new GuiButton(
						0,
						width / 2 - 155,
						height - 28,
						150,
						20,
						StatCollector
								.translateToLocal("createWorld.customize.presets.select")));
		buttonList.add(new GuiButton(1, width / 2 + 5, height - 28, 150, 20,
				StatCollector.translateToLocal("gui.cancel")));
		func_82296_g();
	}

	/**
	 * Called when the screen is unloaded. Used to disable keyboard repeat
	 * events
	 */
	@Override
	public void onGuiClosed() {
		Keyboard.enableRepeatEvents(false);
	}

	/**
	 * Called when the mouse is clicked.
	 */
	@Override
	protected void mouseClicked(final int par1, final int par2, final int par3) {
		theTextField.mouseClicked(par1, par2, par3);
		super.mouseClicked(par1, par2, par3);
	}

	/**
	 * Fired when a key is typed. This is the equivalent of
	 * KeyListener.keyTyped(KeyEvent e).
	 */
	@Override
	protected void keyTyped(final char par1, final int par2) {
		if (!theTextField.textboxKeyTyped(par1, par2)) {
			super.keyTyped(par1, par2);
		}
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	@Override
	protected void actionPerformed(final GuiButton par1GuiButton) {
		if (par1GuiButton.id == 0 && func_82293_j()) {
			createFlatWorldGui.setFlatGeneratorInfo(theTextField.getText());
			mc.displayGuiScreen(createFlatWorldGui);
		} else if (par1GuiButton.id == 1) {
			mc.displayGuiScreen(createFlatWorldGui);
		}
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(final int par1, final int par2, final float par3) {
		drawDefaultBackground();
		theFlatPresetsListSlot.drawScreen(par1, par2, par3);
		drawCenteredString(fontRenderer, field_82300_d, width / 2, 8, 16777215);
		drawString(fontRenderer, field_82308_m, 50, 30, 10526880);
		drawString(fontRenderer, field_82306_n, 50, 70, 10526880);
		theTextField.drawTextBox();
		super.drawScreen(par1, par2, par3);
	}

	/**
	 * Called from the main game loop to update the screen.
	 */
	@Override
	public void updateScreen() {
		theTextField.updateCursorCounter();
		super.updateScreen();
	}

	public void func_82296_g() {
		final boolean var1 = func_82293_j();
		theButton.enabled = var1;
	}

	private boolean func_82293_j() {
		return theFlatPresetsListSlot.field_82459_a > -1
				&& theFlatPresetsListSlot.field_82459_a < GuiFlatPresets.presets
						.size() || theTextField.getText().length() > 1;
	}

	/**
	 * Add a flat world preset with no world features.
	 */
	private static void addPresetNoFeatures(final String par0Str,
			final int par1, final BiomeGenBase par2BiomeGenBase,
			final FlatLayerInfo... par3ArrayOfFlatLayerInfo) {
		GuiFlatPresets.addPreset(par0Str, par1, par2BiomeGenBase, (List) null,
				par3ArrayOfFlatLayerInfo);
	}

	/**
	 * Add a flat world preset.
	 */
	private static void addPreset(final String par0Str, final int par1,
			final BiomeGenBase par2BiomeGenBase, final List par3List,
			final FlatLayerInfo... par4ArrayOfFlatLayerInfo) {
		final FlatGeneratorInfo var5 = new FlatGeneratorInfo();

		for (int var6 = par4ArrayOfFlatLayerInfo.length - 1; var6 >= 0; --var6) {
			var5.getFlatLayers().add(par4ArrayOfFlatLayerInfo[var6]);
		}

		var5.setBiome(par2BiomeGenBase.biomeID);
		var5.func_82645_d();

		if (par3List != null) {
			final Iterator var8 = par3List.iterator();

			while (var8.hasNext()) {
				final String var7 = (String) var8.next();
				var5.getWorldFeatures().put(var7, new HashMap());
			}
		}

		GuiFlatPresets.presets.add(new GuiFlatPresetsItem(par1, par0Str, var5
				.toString()));
	}

	/**
	 * Return the RenderItem instance used to render preset icons.
	 */
	static RenderItem getPresetIconRenderer() {
		return GuiFlatPresets.presetIconRenderer;
	}

	/**
	 * Return the list of defined flat world presets.
	 */
	static List getPresets() {
		return GuiFlatPresets.presets;
	}

	static GuiFlatPresetsListSlot func_82292_a(
			final GuiFlatPresets par0GuiFlatPresets) {
		return par0GuiFlatPresets.theFlatPresetsListSlot;
	}

	static GuiTextField func_82298_b(final GuiFlatPresets par0GuiFlatPresets) {
		return par0GuiFlatPresets.theTextField;
	}

	static {
		GuiFlatPresets.addPreset("Classic Flat", Block.grass.blockID,
				BiomeGenBase.plains, Arrays.asList(new String[] { "village" }),
				new FlatLayerInfo[] {
						new FlatLayerInfo(1, Block.grass.blockID),
						new FlatLayerInfo(2, Block.dirt.blockID),
						new FlatLayerInfo(1, Block.bedrock.blockID) });
		GuiFlatPresets.addPreset(
				"Tunnelers\' Dream",
				Block.stone.blockID,
				BiomeGenBase.extremeHills,
				Arrays.asList(new String[] { "biome_1", "dungeon",
						"decoration", "stronghold", "mineshaft" }),
				new FlatLayerInfo[] {
						new FlatLayerInfo(1, Block.grass.blockID),
						new FlatLayerInfo(5, Block.dirt.blockID),
						new FlatLayerInfo(230, Block.stone.blockID),
						new FlatLayerInfo(1, Block.bedrock.blockID) });
		GuiFlatPresets.addPreset("Water World", Block.waterMoving.blockID,
				BiomeGenBase.plains,
				Arrays.asList(new String[] { "village", "biome_1" }),
				new FlatLayerInfo[] {
						new FlatLayerInfo(90, Block.waterStill.blockID),
						new FlatLayerInfo(5, Block.sand.blockID),
						new FlatLayerInfo(5, Block.dirt.blockID),
						new FlatLayerInfo(5, Block.stone.blockID),
						new FlatLayerInfo(1, Block.bedrock.blockID) });
		GuiFlatPresets.addPreset(
				"Overworld",
				Block.tallGrass.blockID,
				BiomeGenBase.plains,
				Arrays.asList(new String[] { "village", "biome_1",
						"decoration", "stronghold", "mineshaft", "dungeon",
						"lake", "lava_lake" }), new FlatLayerInfo[] {
						new FlatLayerInfo(1, Block.grass.blockID),
						new FlatLayerInfo(3, Block.dirt.blockID),
						new FlatLayerInfo(59, Block.stone.blockID),
						new FlatLayerInfo(1, Block.bedrock.blockID) });
		GuiFlatPresets.addPreset("Snowy Kingdom", Block.snow.blockID,
				BiomeGenBase.icePlains,
				Arrays.asList(new String[] { "village", "biome_1" }),
				new FlatLayerInfo[] { new FlatLayerInfo(1, Block.snow.blockID),
						new FlatLayerInfo(1, Block.grass.blockID),
						new FlatLayerInfo(3, Block.dirt.blockID),
						new FlatLayerInfo(59, Block.stone.blockID),
						new FlatLayerInfo(1, Block.bedrock.blockID) });
		GuiFlatPresets.addPreset("Bottomless Pit", Item.feather.itemID,
				BiomeGenBase.plains,
				Arrays.asList(new String[] { "village", "biome_1" }),
				new FlatLayerInfo[] {
						new FlatLayerInfo(1, Block.grass.blockID),
						new FlatLayerInfo(3, Block.dirt.blockID),
						new FlatLayerInfo(2, Block.cobblestone.blockID) });
		GuiFlatPresets.addPreset(
				"Desert",
				Block.sand.blockID,
				BiomeGenBase.desert,
				Arrays.asList(new String[] { "village", "biome_1",
						"decoration", "stronghold", "mineshaft", "dungeon" }),
				new FlatLayerInfo[] { new FlatLayerInfo(8, Block.sand.blockID),
						new FlatLayerInfo(52, Block.sandStone.blockID),
						new FlatLayerInfo(3, Block.stone.blockID),
						new FlatLayerInfo(1, Block.bedrock.blockID) });
		GuiFlatPresets.addPresetNoFeatures("Redstone Ready",
				Item.redstone.itemID, BiomeGenBase.desert, new FlatLayerInfo[] {
						new FlatLayerInfo(52, Block.sandStone.blockID),
						new FlatLayerInfo(3, Block.stone.blockID),
						new FlatLayerInfo(1, Block.bedrock.blockID) });
	}
}
