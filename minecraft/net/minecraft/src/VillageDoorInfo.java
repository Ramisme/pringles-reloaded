package net.minecraft.src;

public class VillageDoorInfo {
	public final int posX;
	public final int posY;
	public final int posZ;
	public final int insideDirectionX;
	public final int insideDirectionZ;
	public int lastActivityTimestamp;
	public boolean isDetachedFromVillageFlag = false;
	private int doorOpeningRestrictionCounter = 0;

	public VillageDoorInfo(final int par1, final int par2, final int par3,
			final int par4, final int par5, final int par6) {
		posX = par1;
		posY = par2;
		posZ = par3;
		insideDirectionX = par4;
		insideDirectionZ = par5;
		lastActivityTimestamp = par6;
	}

	/**
	 * Returns the squared distance between this door and the given coordinate.
	 */
	public int getDistanceSquared(final int par1, final int par2, final int par3) {
		final int var4 = par1 - posX;
		final int var5 = par2 - posY;
		final int var6 = par3 - posZ;
		return var4 * var4 + var5 * var5 + var6 * var6;
	}

	/**
	 * Get the square of the distance from a location 2 blocks away from the
	 * door considered 'inside' and the given arguments
	 */
	public int getInsideDistanceSquare(final int par1, final int par2,
			final int par3) {
		final int var4 = par1 - posX - insideDirectionX;
		final int var5 = par2 - posY;
		final int var6 = par3 - posZ - insideDirectionZ;
		return var4 * var4 + var5 * var5 + var6 * var6;
	}

	public int getInsidePosX() {
		return posX + insideDirectionX;
	}

	public int getInsidePosY() {
		return posY;
	}

	public int getInsidePosZ() {
		return posZ + insideDirectionZ;
	}

	public boolean isInside(final int par1, final int par2) {
		final int var3 = par1 - posX;
		final int var4 = par2 - posZ;
		return var3 * insideDirectionX + var4 * insideDirectionZ >= 0;
	}

	public void resetDoorOpeningRestrictionCounter() {
		doorOpeningRestrictionCounter = 0;
	}

	public void incrementDoorOpeningRestrictionCounter() {
		++doorOpeningRestrictionCounter;
	}

	public int getDoorOpeningRestrictionCounter() {
		return doorOpeningRestrictionCounter;
	}
}
