package net.minecraft.src;

public class BlockCarrot extends BlockCrops {
	private Icon[] iconArray;

	public BlockCarrot(final int par1) {
		super(par1);
	}

	/**
	 * From the specified side and block metadata retrieves the blocks texture.
	 * Args: side, metadata
	 */
	@Override
	public Icon getIcon(final int par1, int par2) {
		if (par2 < 7) {
			if (par2 == 6) {
				par2 = 5;
			}

			return iconArray[par2 >> 1];
		} else {
			return iconArray[3];
		}
	}

	/**
	 * Generate a seed ItemStack for this crop.
	 */
	@Override
	protected int getSeedItem() {
		return Item.carrot.itemID;
	}

	/**
	 * Generate a crop produce ItemStack for this crop.
	 */
	@Override
	protected int getCropItem() {
		return Item.carrot.itemID;
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		iconArray = new Icon[4];

		for (int var2 = 0; var2 < iconArray.length; ++var2) {
			iconArray[var2] = par1IconRegister.registerIcon("carrots_" + var2);
		}
	}
}
