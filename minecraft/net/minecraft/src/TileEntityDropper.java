package net.minecraft.src;

public class TileEntityDropper extends TileEntityDispenser {
	/**
	 * Returns the name of the inventory.
	 */
	@Override
	public String getInvName() {
		return isInvNameLocalized() ? customName : "container.dropper";
	}
}
