package net.minecraft.src;

import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.Random;
import java.util.Set;

public class CustomColorizer {
	private static int[] grassColors = null;
	private static int[] waterColors = null;
	private static int[] foliageColors = null;
	private static int[] foliagePineColors = null;
	private static int[] foliageBirchColors = null;
	private static int[] swampFoliageColors = null;
	private static int[] swampGrassColors = null;
	private static int[][] blockPalettes = null;
	private static int[][] paletteColors = null;
	private static int[] skyColors = null;
	private static int[] fogColors = null;
	private static int[] underwaterColors = null;
	private static float[][][] lightMapsColorsRgb = null;
	private static int[] lightMapsHeight = null;
	private static float[][] sunRgbs = new float[16][3];
	private static float[][] torchRgbs = new float[16][3];
	private static int[] redstoneColors = null;
	private static int[] stemColors = null;
	private static int[] myceliumParticleColors = null;
	private static boolean useDefaultColorMultiplier = true;
	private static int particleWaterColor = -1;
	private static int particlePortalColor = -1;
	private static int lilyPadColor = -1;
	private static Vec3 fogColorNether = null;
	private static Vec3 fogColorEnd = null;
	private static Vec3 skyColorEnd = null;
	private static Random random = new Random();

	public static void update(final RenderEngine var0) {
		CustomColorizer.grassColors = null;
		CustomColorizer.waterColors = null;
		CustomColorizer.foliageColors = null;
		CustomColorizer.foliageBirchColors = null;
		CustomColorizer.foliagePineColors = null;
		CustomColorizer.swampGrassColors = null;
		CustomColorizer.swampFoliageColors = null;
		CustomColorizer.skyColors = null;
		CustomColorizer.fogColors = null;
		CustomColorizer.underwaterColors = null;
		CustomColorizer.redstoneColors = null;
		CustomColorizer.stemColors = null;
		CustomColorizer.myceliumParticleColors = null;
		CustomColorizer.lightMapsColorsRgb = null;
		CustomColorizer.lightMapsHeight = null;
		CustomColorizer.lilyPadColor = -1;
		CustomColorizer.particleWaterColor = -1;
		CustomColorizer.particlePortalColor = -1;
		CustomColorizer.fogColorNether = null;
		CustomColorizer.fogColorEnd = null;
		CustomColorizer.skyColorEnd = null;
		CustomColorizer.blockPalettes = null;
		CustomColorizer.paletteColors = null;
		CustomColorizer.useDefaultColorMultiplier = true;
		CustomColorizer.grassColors = CustomColorizer.getCustomColors(
				"/misc/grasscolor.png", var0, 65536);
		CustomColorizer.foliageColors = CustomColorizer.getCustomColors(
				"/misc/foliagecolor.png", var0, 65536);
		CustomColorizer.waterColors = CustomColorizer.getCustomColors(
				"/misc/watercolorX.png", var0, 65536);

		if (Config.isCustomColors()) {
			CustomColorizer.foliagePineColors = CustomColorizer
					.getCustomColors("/misc/pinecolor.png", var0, 65536);
			CustomColorizer.foliageBirchColors = CustomColorizer
					.getCustomColors("/misc/birchcolor.png", var0, 65536);
			CustomColorizer.swampGrassColors = CustomColorizer.getCustomColors(
					"/misc/swampgrasscolor.png", var0, 65536);
			CustomColorizer.swampFoliageColors = CustomColorizer
					.getCustomColors("/misc/swampfoliagecolor.png", var0, 65536);
			CustomColorizer.skyColors = CustomColorizer.getCustomColors(
					"/misc/skycolor0.png", var0, 65536);
			CustomColorizer.fogColors = CustomColorizer.getCustomColors(
					"/misc/fogcolor0.png", var0, 65536);
			CustomColorizer.underwaterColors = CustomColorizer.getCustomColors(
					"/misc/underwatercolor.png", var0, 65536);
			CustomColorizer.redstoneColors = CustomColorizer.getCustomColors(
					"/misc/redstonecolor.png", var0, 16);
			CustomColorizer.stemColors = CustomColorizer.getCustomColors(
					"/misc/stemcolor.png", var0, 8);
			CustomColorizer.myceliumParticleColors = CustomColorizer
					.getCustomColors("/misc/myceliumparticlecolor.png", var0,
							-1);
			final int[][] var1 = new int[3][];
			CustomColorizer.lightMapsColorsRgb = new float[3][][];
			CustomColorizer.lightMapsHeight = new int[3];

			for (int var2 = 0; var2 < var1.length; ++var2) {
				final String var3 = "/environment/lightmap" + (var2 - 1)
						+ ".png";
				var1[var2] = CustomColorizer.getCustomColors(var3, var0, -1);

				if (var1[var2] != null) {
					CustomColorizer.lightMapsColorsRgb[var2] = CustomColorizer
							.toRgb(var1[var2]);
				}

				CustomColorizer.lightMapsHeight[var2] = CustomColorizer
						.getTextureHeight(var0, var3, 32);
			}

			CustomColorizer.readColorProperties("/color.properties", var0);
			CustomColorizer.updateUseDefaultColorMultiplier();
		}
	}

	private static int getTextureHeight(final RenderEngine var0,
			final String var1, final int var2) {
		try {
			final BufferedImage var3 = var0.readTextureImage(var1);
			return var3 == null ? var2 : var3.getHeight();
		} catch (final IOException var4) {
			return var2;
		}
	}

	private static float[][] toRgb(final int[] var0) {
		final float[][] var1 = new float[var0.length][3];

		for (int var2 = 0; var2 < var0.length; ++var2) {
			final int var3 = var0[var2];
			final float var4 = (var3 >> 16 & 255) / 255.0F;
			final float var5 = (var3 >> 8 & 255) / 255.0F;
			final float var6 = (var3 & 255) / 255.0F;
			final float[] var7 = var1[var2];
			var7[0] = var4;
			var7[1] = var5;
			var7[2] = var6;
		}

		return var1;
	}

	private static void readColorProperties(final String var0,
			final RenderEngine var1) {
		try {
			final InputStream var2 = var1.getTexturePack()
					.getSelectedTexturePack().getResourceAsStream(var0);

			if (var2 == null) {
				return;
			}

			Config.log("Loading " + var0);
			final Properties var3 = new Properties();
			var3.load(var2);
			CustomColorizer.lilyPadColor = CustomColorizer.readColor(var3,
					"lilypad");
			CustomColorizer.particleWaterColor = CustomColorizer.readColor(
					var3, new String[] { "particle.water", "drop.water" });
			CustomColorizer.particlePortalColor = CustomColorizer.readColor(
					var3, "particle.portal");
			CustomColorizer.fogColorNether = CustomColorizer.readColorVec3(
					var3, "fog.nether");
			CustomColorizer.fogColorEnd = CustomColorizer.readColorVec3(var3,
					"fog.end");
			CustomColorizer.skyColorEnd = CustomColorizer.readColorVec3(var3,
					"sky.end");
			CustomColorizer.readCustomPalettes(var3, var1);
		} catch (final FileNotFoundException var4) {
			return;
		} catch (final IOException var5) {
			var5.printStackTrace();
		}
	}

	private static void readCustomPalettes(final Properties var0,
			final RenderEngine var1) {
		CustomColorizer.blockPalettes = new int[256][1];

		for (int var2 = 0; var2 < 256; ++var2) {
			CustomColorizer.blockPalettes[var2][0] = -1;
		}

		final String var17 = "palette.block.";
		final HashMap var3 = new HashMap();
		final Set var4 = var0.keySet();
		final Iterator var5 = var4.iterator();
		String var7;

		while (var5.hasNext()) {
			final String var6 = (String) var5.next();
			var7 = var0.getProperty(var6);

			if (var6.startsWith(var17)) {
				var3.put(var6, var7);
			}
		}

		final String[] var18 = (String[]) var3.keySet().toArray(
				new String[var3.size()]);
		CustomColorizer.paletteColors = new int[var18.length][];

		for (int var19 = 0; var19 < var18.length; ++var19) {
			var7 = var18[var19];
			final String var8 = var0.getProperty(var7);
			Config.log("Block palette: " + var7 + " = " + var8);
			final String var9 = var7.substring(var17.length());
			final int[] var10 = CustomColorizer.getCustomColors(var9, var1,
					65536);
			CustomColorizer.paletteColors[var19] = var10;
			final String[] var11 = Config.tokenize(var8, " ,;");

			for (final String element : var11) {
				String var13 = element;
				int var14 = -1;

				if (var13.contains(":")) {
					final String[] var15 = Config.tokenize(var13, ":");
					var13 = var15[0];
					final String var16 = var15[1];
					var14 = Config.parseInt(var16, -1);

					if (var14 < 0 || var14 > 15) {
						Config.log("Invalid block metadata: " + var13
								+ " in palette: " + var7);
						continue;
					}
				}

				final int var20 = Config.parseInt(var13, -1);

				if (var20 >= 0 && var20 <= 255) {
					if (var20 != Block.grass.blockID
							&& var20 != Block.tallGrass.blockID
							&& var20 != Block.leaves.blockID
							&& var20 != Block.vine.blockID) {
						if (var14 == -1) {
							CustomColorizer.blockPalettes[var20][0] = var19;
						} else {
							if (CustomColorizer.blockPalettes[var20].length < 16) {
								CustomColorizer.blockPalettes[var20] = new int[16];
								Arrays.fill(
										CustomColorizer.blockPalettes[var20],
										-1);
							}

							CustomColorizer.blockPalettes[var20][var14] = var19;
						}
					}
				} else {
					Config.log("Invalid block index: " + var20
							+ " in palette: " + var7);
				}
			}
		}
	}

	private static int readColor(final Properties var0, final String[] var1) {
		for (final String var3 : var1) {
			final int var4 = CustomColorizer.readColor(var0, var3);

			if (var4 >= 0) {
				return var4;
			}
		}

		return -1;
	}

	private static int readColor(final Properties var0, final String var1) {
		final String var2 = var0.getProperty(var1);

		if (var2 == null) {
			return -1;
		} else {
			try {
				final int var3 = Integer.parseInt(var2, 16) & 16777215;
				Config.log("Custom color: " + var1 + " = " + var2);
				return var3;
			} catch (final NumberFormatException var4) {
				Config.log("Invalid custom color: " + var1 + " = " + var2);
				return -1;
			}
		}
	}

	private static Vec3 readColorVec3(final Properties var0, final String var1) {
		final int var2 = CustomColorizer.readColor(var0, var1);

		if (var2 < 0) {
			return null;
		} else {
			final int var3 = var2 >> 16 & 255;
			final int var4 = var2 >> 8 & 255;
			final int var5 = var2 & 255;
			final float var6 = var3 / 255.0F;
			final float var7 = var4 / 255.0F;
			final float var8 = var5 / 255.0F;
			return Vec3.createVectorHelper(var6, var7, var8);
		}
	}

	private static int[] getCustomColors(final String var0,
			final RenderEngine var1, final int var2) {
		try {
			final InputStream var3 = var1.getTexturePack()
					.getSelectedTexturePack().getResourceAsStream(var0);

			if (var3 == null) {
				return null;
			} else {
				final int[] var4 = var1.getTextureContents(var0);

				if (var4 == null) {
					return null;
				} else if (var2 > 0 && var4.length != var2) {
					Config.log("Invalid custom colors length: " + var4.length
							+ ", path: " + var0);
					return null;
				} else {
					Config.log("Loading custom colors: " + var0);
					return var4;
				}
			}
		} catch (final FileNotFoundException var5) {
			return null;
		} catch (final IOException var6) {
			var6.printStackTrace();
			return null;
		}
	}

	public static void updateUseDefaultColorMultiplier() {
		CustomColorizer.useDefaultColorMultiplier = CustomColorizer.foliageBirchColors == null
				&& CustomColorizer.foliagePineColors == null
				&& CustomColorizer.swampGrassColors == null
				&& CustomColorizer.swampFoliageColors == null
				&& CustomColorizer.blockPalettes == null
				&& Config.isSwampColors() && Config.isSmoothBiomes();
	}

	public static int getColorMultiplier(final Block var0,
			final IBlockAccess var1, final int var2, final int var3,
			final int var4) {
		if (CustomColorizer.useDefaultColorMultiplier) {
			return var0.colorMultiplier(var1, var2, var3, var4);
		} else {
			int[] var5 = null;
			int[] var6 = null;
			int var10;

			if (CustomColorizer.blockPalettes != null) {
				final int var7 = var0.blockID;

				if (var7 >= 0 && var7 < 256) {
					final int[] var8 = CustomColorizer.blockPalettes[var7];
					int var13;

					if (var8.length > 1) {
						var10 = var1.getBlockMetadata(var2, var3, var4);
						var13 = var8[var10];
					} else {
						var13 = var8[0];
					}

					if (var13 >= 0) {
						var5 = CustomColorizer.paletteColors[var13];
					}
				}

				if (var5 != null) {
					if (Config.isSmoothBiomes()) {
						return CustomColorizer.getSmoothColorMultiplier(var0,
								var1, var2, var3, var4, var5, var5, 0, 0);
					}

					return CustomColorizer.getCustomColor(var5, var1, var2,
							var3, var4);
				}
			}

			final boolean var11 = Config.isSwampColors();
			boolean var12 = false;
			byte var14 = 0;
			var10 = 0;

			if (var0 != Block.grass && var0 != Block.tallGrass) {
				if (var0 == Block.leaves) {
					var14 = 2;
					var12 = Config.isSmoothBiomes();
					var10 = var1.getBlockMetadata(var2, var3, var4);

					if ((var10 & 3) == 1) {
						var5 = CustomColorizer.foliagePineColors;
					} else if ((var10 & 3) == 2) {
						var5 = CustomColorizer.foliageBirchColors;
					} else {
						var5 = CustomColorizer.foliageColors;

						if (var11) {
							var6 = CustomColorizer.swampFoliageColors;
						} else {
							var6 = var5;
						}
					}
				} else if (var0 == Block.vine) {
					var14 = 2;
					var12 = Config.isSmoothBiomes();
					var5 = CustomColorizer.foliageColors;

					if (var11) {
						var6 = CustomColorizer.swampFoliageColors;
					} else {
						var6 = var5;
					}
				}
			} else {
				var14 = 1;
				var12 = Config.isSmoothBiomes();
				var5 = CustomColorizer.grassColors;

				if (var11) {
					var6 = CustomColorizer.swampGrassColors;
				} else {
					var6 = var5;
				}
			}

			if (var12) {
				return CustomColorizer.getSmoothColorMultiplier(var0, var1,
						var2, var3, var4, var5, var6, var14, var10);
			} else {
				if (var6 != var5
						&& var1.getBiomeGenForCoords(var2, var4) == BiomeGenBase.swampland) {
					var5 = var6;
				}

				return var5 != null ? CustomColorizer.getCustomColor(var5,
						var1, var2, var3, var4) : var0.colorMultiplier(var1,
						var2, var3, var4);
			}
		}
	}

	private static int getSmoothColorMultiplier(final Block var0,
			final IBlockAccess var1, final int var2, final int var3,
			final int var4, final int[] var5, final int[] var6, final int var7,
			final int var8) {
		int var9 = 0;
		int var10 = 0;
		int var11 = 0;
		int var12;
		int var13;

		for (var12 = var2 - 1; var12 <= var2 + 1; ++var12) {
			for (var13 = var4 - 1; var13 <= var4 + 1; ++var13) {
				int[] var14 = var5;

				if (var6 != var5
						&& var1.getBiomeGenForCoords(var12, var13) == BiomeGenBase.swampland) {
					var14 = var6;
				}

				int var17;

				if (var14 == null) {
					switch (var7) {
					case 1:
						var17 = var1.getBiomeGenForCoords(var12, var13)
								.getBiomeGrassColor();
						break;

					case 2:
						if ((var8 & 3) == 1) {
							var17 = ColorizerFoliage.getFoliageColorPine();
						} else if ((var8 & 3) == 2) {
							var17 = ColorizerFoliage.getFoliageColorBirch();
						} else {
							var17 = var1.getBiomeGenForCoords(var12, var13)
									.getBiomeFoliageColor();
						}

						break;

					default:
						var17 = var0.colorMultiplier(var1, var12, var3, var13);
					}
				} else {
					var17 = CustomColorizer.getCustomColor(var14, var1, var12,
							var3, var13);
				}

				var9 += var17 >> 16 & 255;
				var10 += var17 >> 8 & 255;
				var11 += var17 & 255;
			}
		}

		var12 = var9 / 9;
		var13 = var10 / 9;
		final int var16 = var11 / 9;
		return var12 << 16 | var13 << 8 | var16;
	}

	public static int getFluidColor(final Block var0, final IBlockAccess var1,
			final int var2, final int var3, final int var4) {
		return var0.blockMaterial != Material.water ? var0.colorMultiplier(
				var1, var2, var3, var4)
				: CustomColorizer.waterColors != null ? Config.isSmoothBiomes() ? CustomColorizer
						.getSmoothColor(CustomColorizer.waterColors, var1,
								var2, var3, var4, 3, 1) : CustomColorizer
						.getCustomColor(CustomColorizer.waterColors, var1,
								var2, var3, var4)
						: !Config.isSwampColors() ? 16777215 : var0
								.colorMultiplier(var1, var2, var3, var4);
	}

	private static int getCustomColor(final int[] var0,
			final IBlockAccess var1, final int var2, final int var3,
			final int var4) {
		final BiomeGenBase var5 = var1.getBiomeGenForCoords(var2, var4);
		final double var6 = MathHelper.clamp_float(var5.getFloatTemperature(),
				0.0F, 1.0F);
		double var8 = MathHelper.clamp_float(var5.getFloatRainfall(), 0.0F,
				1.0F);
		var8 *= var6;
		final int var10 = (int) ((1.0D - var6) * 255.0D);
		final int var11 = (int) ((1.0D - var8) * 255.0D);
		return var0[var11 << 8 | var10] & 16777215;
	}

	public static void updatePortalFX(final EntityFX var0) {
		if (CustomColorizer.particlePortalColor >= 0) {
			final int var1 = CustomColorizer.particlePortalColor;
			final int var2 = var1 >> 16 & 255;
			final int var3 = var1 >> 8 & 255;
			final int var4 = var1 & 255;
			final float var5 = var2 / 255.0F;
			final float var6 = var3 / 255.0F;
			final float var7 = var4 / 255.0F;
			var0.particleRed = var5;
			var0.particleGreen = var6;
			var0.particleBlue = var7;
		}
	}

	public static void updateMyceliumFX(final EntityFX var0) {
		if (CustomColorizer.myceliumParticleColors != null) {
			final int var1 = CustomColorizer.myceliumParticleColors[CustomColorizer.random
					.nextInt(CustomColorizer.myceliumParticleColors.length)];
			final int var2 = var1 >> 16 & 255;
			final int var3 = var1 >> 8 & 255;
			final int var4 = var1 & 255;
			final float var5 = var2 / 255.0F;
			final float var6 = var3 / 255.0F;
			final float var7 = var4 / 255.0F;
			var0.particleRed = var5;
			var0.particleGreen = var6;
			var0.particleBlue = var7;
		}
	}

	public static void updateReddustFX(final EntityFX var0,
			final IBlockAccess var1, final double var2, final double var4,
			final double var6) {
		if (CustomColorizer.redstoneColors != null) {
			final int var8 = var1.getBlockMetadata((int) var2, (int) var4,
					(int) var6);
			final int var9 = CustomColorizer.getRedstoneColor(var8);

			if (var9 != -1) {
				final int var10 = var9 >> 16 & 255;
				final int var11 = var9 >> 8 & 255;
				final int var12 = var9 & 255;
				final float var13 = var10 / 255.0F;
				final float var14 = var11 / 255.0F;
				final float var15 = var12 / 255.0F;
				var0.particleRed = var13;
				var0.particleGreen = var14;
				var0.particleBlue = var15;
			}
		}
	}

	public static int getRedstoneColor(final int var0) {
		return CustomColorizer.redstoneColors == null ? -1 : var0 >= 0
				&& var0 <= 15 ? CustomColorizer.redstoneColors[var0] & 16777215
				: -1;
	}

	public static void updateWaterFX(final EntityFX var0,
			final IBlockAccess var1) {
		if (CustomColorizer.waterColors != null) {
			final int var2 = (int) var0.posX;
			final int var3 = (int) var0.posY;
			final int var4 = (int) var0.posZ;
			final int var5 = CustomColorizer.getFluidColor(Block.waterStill,
					var1, var2, var3, var4);
			final int var6 = var5 >> 16 & 255;
			final int var7 = var5 >> 8 & 255;
			final int var8 = var5 & 255;
			float var9 = var6 / 255.0F;
			float var10 = var7 / 255.0F;
			float var11 = var8 / 255.0F;

			if (CustomColorizer.particleWaterColor >= 0) {
				final int var12 = CustomColorizer.particleWaterColor >> 16 & 255;
				final int var13 = CustomColorizer.particleWaterColor >> 8 & 255;
				final int var14 = CustomColorizer.particleWaterColor & 255;
				var9 *= var12 / 255.0F;
				var10 *= var13 / 255.0F;
				var11 *= var14 / 255.0F;
			}

			var0.particleRed = var9;
			var0.particleGreen = var10;
			var0.particleBlue = var11;
		}
	}

	public static int getLilypadColor() {
		return CustomColorizer.lilyPadColor < 0 ? Block.waterlily
				.getBlockColor() : CustomColorizer.lilyPadColor;
	}

	public static Vec3 getFogColorNether(final Vec3 var0) {
		return CustomColorizer.fogColorNether == null ? var0
				: CustomColorizer.fogColorNether;
	}

	public static Vec3 getFogColorEnd(final Vec3 var0) {
		return CustomColorizer.fogColorEnd == null ? var0
				: CustomColorizer.fogColorEnd;
	}

	public static Vec3 getSkyColorEnd(final Vec3 var0) {
		return CustomColorizer.skyColorEnd == null ? var0
				: CustomColorizer.skyColorEnd;
	}

	public static Vec3 getSkyColor(final Vec3 var0, final IBlockAccess var1,
			final double var2, final double var4, final double var6) {
		if (CustomColorizer.skyColors == null) {
			return var0;
		} else {
			final int var8 = CustomColorizer.getSmoothColor(
					CustomColorizer.skyColors, var1, var2, var4, var6, 10, 1);
			final int var9 = var8 >> 16 & 255;
			final int var10 = var8 >> 8 & 255;
			final int var11 = var8 & 255;
			float var12 = var9 / 255.0F;
			float var13 = var10 / 255.0F;
			float var14 = var11 / 255.0F;
			final float var15 = (float) var0.xCoord / 0.5F;
			final float var16 = (float) var0.yCoord / 0.66275F;
			final float var17 = (float) var0.zCoord;
			var12 *= var15;
			var13 *= var16;
			var14 *= var17;
			return Vec3.createVectorHelper(var12, var13, var14);
		}
	}

	public static Vec3 getFogColor(final Vec3 var0, final IBlockAccess var1,
			final double var2, final double var4, final double var6) {
		if (CustomColorizer.fogColors == null) {
			return var0;
		} else {
			final int var8 = CustomColorizer.getSmoothColor(
					CustomColorizer.fogColors, var1, var2, var4, var6, 10, 1);
			final int var9 = var8 >> 16 & 255;
			final int var10 = var8 >> 8 & 255;
			final int var11 = var8 & 255;
			float var12 = var9 / 255.0F;
			float var13 = var10 / 255.0F;
			float var14 = var11 / 255.0F;
			final float var15 = (float) var0.xCoord / 0.753F;
			final float var16 = (float) var0.yCoord / 0.8471F;
			final float var17 = (float) var0.zCoord;
			var12 *= var15;
			var13 *= var16;
			var14 *= var17;
			return Vec3.createVectorHelper(var12, var13, var14);
		}
	}

	public static Vec3 getUnderwaterColor(final IBlockAccess var0,
			final double var1, final double var3, final double var5) {
		if (CustomColorizer.underwaterColors == null) {
			return null;
		} else {
			final int var7 = CustomColorizer.getSmoothColor(
					CustomColorizer.underwaterColors, var0, var1, var3, var5,
					10, 1);
			final int var8 = var7 >> 16 & 255;
			final int var9 = var7 >> 8 & 255;
			final int var10 = var7 & 255;
			final float var11 = var8 / 255.0F;
			final float var12 = var9 / 255.0F;
			final float var13 = var10 / 255.0F;
			return Vec3.createVectorHelper(var11, var12, var13);
		}
	}

	public static int getSmoothColor(final int[] var0, final IBlockAccess var1,
			final double var2, final double var4, final double var6,
			final int var8, final int var9) {
		if (var0 == null) {
			return -1;
		} else {
			final int var10 = (int) Math.floor(var2);
			final int var11 = (int) Math.floor(var4);
			final int var12 = (int) Math.floor(var6);
			final int var13 = var8 * var9 / 2;
			int var14 = 0;
			int var15 = 0;
			int var16 = 0;
			int var17 = 0;
			int var19;
			int var18;
			int var20;

			for (var18 = var10 - var13; var18 <= var10 + var13; var18 += var9) {
				for (var19 = var12 - var13; var19 <= var12 + var13; var19 += var9) {
					var20 = CustomColorizer.getCustomColor(var0, var1, var18,
							var11, var19);
					var14 += var20 >> 16 & 255;
					var15 += var20 >> 8 & 255;
					var16 += var20 & 255;
					++var17;
				}
			}

			var18 = var14 / var17;
			var19 = var15 / var17;
			var20 = var16 / var17;
			return var18 << 16 | var19 << 8 | var20;
		}
	}

	public static int mixColors(final int var0, final int var1, final float var2) {
		if (var2 <= 0.0F) {
			return var1;
		} else if (var2 >= 1.0F) {
			return var0;
		} else {
			final float var3 = 1.0F - var2;
			final int var4 = var0 >> 16 & 255;
			final int var5 = var0 >> 8 & 255;
			final int var6 = var0 & 255;
			final int var7 = var1 >> 16 & 255;
			final int var8 = var1 >> 8 & 255;
			final int var9 = var1 & 255;
			final int var10 = (int) (var4 * var2 + var7 * var3);
			final int var11 = (int) (var5 * var2 + var8 * var3);
			final int var12 = (int) (var6 * var2 + var9 * var3);
			return var10 << 16 | var11 << 8 | var12;
		}
	}

	public static int getStemColorMultiplier(final BlockStem var0,
			final IBlockAccess var1, final int var2, final int var3,
			final int var4) {
		if (CustomColorizer.stemColors == null) {
			return var0.colorMultiplier(var1, var2, var3, var4);
		} else {
			int var5 = var1.getBlockMetadata(var2, var3, var4);

			if (var5 < 0) {
				var5 = 0;
			}

			if (var5 >= CustomColorizer.stemColors.length) {
				var5 = CustomColorizer.stemColors.length - 1;
			}

			return CustomColorizer.stemColors[var5];
		}
	}

	public static boolean updateLightmap(final World var0,
			final EntityRenderer var1, final int[] var2, final boolean var3) {
		if (var0 == null) {
			return false;
		} else if (CustomColorizer.lightMapsColorsRgb == null) {
			return false;
		} else if (!Config.isCustomColors()) {
			return false;
		} else {
			final int var4 = var0.provider.dimensionId;

			if (var4 >= -1 && var4 <= 1) {
				final int var5 = var4 + 1;
				final float[][] var6 = CustomColorizer.lightMapsColorsRgb[var5];

				if (var6 == null) {
					return false;
				} else {
					final int var7 = CustomColorizer.lightMapsHeight[var5];

					if (var3 && var7 < 64) {
						return false;
					} else {
						final int var8 = var6.length / var7;

						if (var8 < 16) {
							Config.dbg("Invalid lightmap width: " + var8
									+ " for: /environment/lightmap" + var4
									+ ".png");
							CustomColorizer.lightMapsColorsRgb[var5] = null;
							return false;
						} else {
							int var9 = 0;

							if (var3) {
								var9 = var8 * 16 * 2;
							}

							float var10 = 1.1666666F * (var0
									.getSunBrightness(1.0F) - 0.2F);

							if (var0.lastLightningBolt > 0) {
								var10 = 1.0F;
							}

							var10 = Config.limitTo1(var10);
							final float var11 = var10 * (var8 - 1);
							final float var12 = Config
									.limitTo1(var1.torchFlickerX + 0.5F)
									* (var8 - 1);
							final float var13 = Config.limitTo1(Config
									.getGameSettings().gammaSetting);
							final boolean var14 = var13 > 1.0E-4F;
							CustomColorizer.getLightMapColumn(var6, var11,
									var9, var8, CustomColorizer.sunRgbs);
							CustomColorizer.getLightMapColumn(var6, var12, var9
									+ 16 * var8, var8,
									CustomColorizer.torchRgbs);
							final float[] var15 = new float[3];

							for (int var16 = 0; var16 < 16; ++var16) {
								for (int var17 = 0; var17 < 16; ++var17) {
									int var18;

									for (var18 = 0; var18 < 3; ++var18) {
										float var19 = Config
												.limitTo1(CustomColorizer.sunRgbs[var16][var18]
														+ CustomColorizer.torchRgbs[var17][var18]);

										if (var14) {
											float var20 = 1.0F - var19;
											var20 = 1.0F - var20 * var20
													* var20 * var20;
											var19 = var13 * var20
													+ (1.0F - var13) * var19;
										}

										var15[var18] = var19;
									}

									var18 = (int) (var15[0] * 255.0F);
									final int var22 = (int) (var15[1] * 255.0F);
									final int var21 = (int) (var15[2] * 255.0F);
									var2[var16 * 16 + var17] = -16777216
											| var18 << 16 | var22 << 8 | var21;
								}
							}

							return true;
						}
					}
				}
			} else {
				return false;
			}
		}
	}

	private static void getLightMapColumn(final float[][] var0,
			final float var1, final int var2, final int var3,
			final float[][] var4) {
		final int var5 = (int) Math.floor(var1);
		final int var6 = (int) Math.ceil(var1);

		if (var5 == var6) {
			for (int var14 = 0; var14 < 16; ++var14) {
				final float[] var15 = var0[var2 + var14 * var3 + var5];
				final float[] var16 = var4[var14];

				for (int var17 = 0; var17 < 3; ++var17) {
					var16[var17] = var15[var17];
				}
			}
		} else {
			final float var7 = 1.0F - (var1 - var5);
			final float var8 = 1.0F - (var6 - var1);

			for (int var9 = 0; var9 < 16; ++var9) {
				final float[] var10 = var0[var2 + var9 * var3 + var5];
				final float[] var11 = var0[var2 + var9 * var3 + var6];
				final float[] var12 = var4[var9];

				for (int var13 = 0; var13 < 3; ++var13) {
					var12[var13] = var10[var13] * var7 + var11[var13] * var8;
				}
			}
		}
	}
}
