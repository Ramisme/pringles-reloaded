package net.minecraft.src;

public class EntityReddustFX extends EntityFX {
	float reddustParticleScale;

	public EntityReddustFX(final World par1World, final double par2,
			final double par4, final double par6, final float par8,
			final float par9, final float par10) {
		this(par1World, par2, par4, par6, 1.0F, par8, par9, par10);
	}

	public EntityReddustFX(final World par1World, final double par2,
			final double par4, final double par6, final float par8, float par9,
			final float par10, final float par11) {
		super(par1World, par2, par4, par6, 0.0D, 0.0D, 0.0D);
		motionX *= 0.10000000149011612D;
		motionY *= 0.10000000149011612D;
		motionZ *= 0.10000000149011612D;

		if (par9 == 0.0F) {
			par9 = 1.0F;
		}

		final float var12 = (float) Math.random() * 0.4F + 0.6F;
		particleRed = ((float) (Math.random() * 0.20000000298023224D) + 0.8F)
				* par9 * var12;
		particleGreen = ((float) (Math.random() * 0.20000000298023224D) + 0.8F)
				* par10 * var12;
		particleBlue = ((float) (Math.random() * 0.20000000298023224D) + 0.8F)
				* par11 * var12;
		particleScale *= 0.75F;
		particleScale *= par8;
		reddustParticleScale = particleScale;
		particleMaxAge = (int) (8.0D / (Math.random() * 0.8D + 0.2D));
		particleMaxAge = (int) (particleMaxAge * par8);
		noClip = false;
	}

	@Override
	public void renderParticle(final Tessellator par1Tessellator,
			final float par2, final float par3, final float par4,
			final float par5, final float par6, final float par7) {
		float var8 = (particleAge + par2) / particleMaxAge * 32.0F;

		if (var8 < 0.0F) {
			var8 = 0.0F;
		}

		if (var8 > 1.0F) {
			var8 = 1.0F;
		}

		particleScale = reddustParticleScale * var8;
		super.renderParticle(par1Tessellator, par2, par3, par4, par5, par6,
				par7);
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate() {
		prevPosX = posX;
		prevPosY = posY;
		prevPosZ = posZ;

		if (particleAge++ >= particleMaxAge) {
			setDead();
		}

		setParticleTextureIndex(7 - particleAge * 8 / particleMaxAge);
		moveEntity(motionX, motionY, motionZ);

		if (posY == prevPosY) {
			motionX *= 1.1D;
			motionZ *= 1.1D;
		}

		motionX *= 0.9599999785423279D;
		motionY *= 0.9599999785423279D;
		motionZ *= 0.9599999785423279D;

		if (onGround) {
			motionX *= 0.699999988079071D;
			motionZ *= 0.699999988079071D;
		}
	}
}
