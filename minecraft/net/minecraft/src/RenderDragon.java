package net.minecraft.src;

import java.util.Random;

import org.lwjgl.opengl.GL11;

public class RenderDragon extends RenderLiving {
	/**
	 * Reloads the dragon model if not equal to 4. Presumably a leftover
	 * debugging field.
	 */
	private static int updateModelState = 0;

	/** An instance of the dragon model in RenderDragon */
	protected ModelDragon modelDragon;

	public RenderDragon() {
		super(new ModelDragon(0.0F), 0.5F);
		modelDragon = (ModelDragon) mainModel;
		setRenderPassModel(mainModel);
	}

	/**
	 * Used to rotate the dragon as a whole in RenderDragon. It's called in the
	 * rotateCorpse method.
	 */
	protected void rotateDragonBody(final EntityDragon par1EntityDragon,
			final float par2, final float par3, final float par4) {
		final float var5 = (float) par1EntityDragon.getMovementOffsets(7, par4)[0];
		final float var6 = (float) (par1EntityDragon
				.getMovementOffsets(5, par4)[1] - par1EntityDragon
				.getMovementOffsets(10, par4)[1]);
		GL11.glRotatef(-var5, 0.0F, 1.0F, 0.0F);
		GL11.glRotatef(var6 * 10.0F, 1.0F, 0.0F, 0.0F);
		GL11.glTranslatef(0.0F, 0.0F, 1.0F);

		if (par1EntityDragon.deathTime > 0) {
			float var7 = (par1EntityDragon.deathTime + par4 - 1.0F) / 20.0F * 1.6F;
			var7 = MathHelper.sqrt_float(var7);

			if (var7 > 1.0F) {
				var7 = 1.0F;
			}

			GL11.glRotatef(var7 * getDeathMaxRotation(par1EntityDragon), 0.0F,
					0.0F, 1.0F);
		}
	}

	/**
	 * Renders the dragon model. Called by renderModel.
	 */
	protected void renderDragonModel(final EntityDragon par1EntityDragon,
			final float par2, final float par3, final float par4,
			final float par5, final float par6, final float par7) {
		if (par1EntityDragon.deathTicks > 0) {
			final float var8 = par1EntityDragon.deathTicks / 200.0F;
			GL11.glDepthFunc(GL11.GL_LEQUAL);
			GL11.glEnable(GL11.GL_ALPHA_TEST);
			GL11.glAlphaFunc(GL11.GL_GREATER, var8);
			loadTexture("/mob/enderdragon/shuffle.png");
			mainModel.render(par1EntityDragon, par2, par3, par4, par5, par6,
					par7);
			GL11.glAlphaFunc(GL11.GL_GREATER, 0.1F);
			GL11.glDepthFunc(GL11.GL_EQUAL);
		}

		loadTexture(par1EntityDragon.getTexture());
		mainModel.render(par1EntityDragon, par2, par3, par4, par5, par6, par7);

		if (par1EntityDragon.hurtTime > 0) {
			GL11.glDepthFunc(GL11.GL_EQUAL);
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
			GL11.glColor4f(1.0F, 0.0F, 0.0F, 0.5F);
			mainModel.render(par1EntityDragon, par2, par3, par4, par5, par6,
					par7);
			GL11.glEnable(GL11.GL_TEXTURE_2D);
			GL11.glDisable(GL11.GL_BLEND);
			GL11.glDepthFunc(GL11.GL_LEQUAL);
		}
	}

	/**
	 * Renders the dragon, along with its dying animation
	 */
	public void renderDragon(final EntityDragon par1EntityDragon,
			final double par2, final double par4, final double par6,
			final float par8, final float par9) {
		BossStatus.func_82824_a(par1EntityDragon, false);

		if (RenderDragon.updateModelState != 4) {
			mainModel = new ModelDragon(0.0F);
			RenderDragon.updateModelState = 4;
		}

		super.doRenderLiving(par1EntityDragon, par2, par4, par6, par8, par9);

		if (par1EntityDragon.healingEnderCrystal != null) {
			final float var10 = par1EntityDragon.healingEnderCrystal.innerRotation
					+ par9;
			float var11 = MathHelper.sin(var10 * 0.2F) / 2.0F + 0.5F;
			var11 = (var11 * var11 + var11) * 0.2F;
			final float var12 = (float) (par1EntityDragon.healingEnderCrystal.posX
					- par1EntityDragon.posX - (par1EntityDragon.prevPosX - par1EntityDragon.posX)
					* (1.0F - par9));
			final float var13 = (float) (var11
					+ par1EntityDragon.healingEnderCrystal.posY - 1.0D
					- par1EntityDragon.posY - (par1EntityDragon.prevPosY - par1EntityDragon.posY)
					* (1.0F - par9));
			final float var14 = (float) (par1EntityDragon.healingEnderCrystal.posZ
					- par1EntityDragon.posZ - (par1EntityDragon.prevPosZ - par1EntityDragon.posZ)
					* (1.0F - par9));
			final float var15 = MathHelper.sqrt_float(var12 * var12 + var14
					* var14);
			final float var16 = MathHelper.sqrt_float(var12 * var12 + var13
					* var13 + var14 * var14);
			GL11.glPushMatrix();
			GL11.glTranslatef((float) par2, (float) par4 + 2.0F, (float) par6);
			GL11.glRotatef((float) -Math.atan2(var14, var12) * 180.0F
					/ (float) Math.PI - 90.0F, 0.0F, 1.0F, 0.0F);
			GL11.glRotatef((float) -Math.atan2(var15, var13) * 180.0F
					/ (float) Math.PI - 90.0F, 1.0F, 0.0F, 0.0F);
			final Tessellator var17 = Tessellator.instance;
			RenderHelper.disableStandardItemLighting();
			GL11.glDisable(GL11.GL_CULL_FACE);
			loadTexture("/mob/enderdragon/beam.png");
			GL11.glShadeModel(GL11.GL_SMOOTH);
			final float var18 = 0.0F - (par1EntityDragon.ticksExisted + par9) * 0.01F;
			final float var19 = MathHelper.sqrt_float(var12 * var12 + var13
					* var13 + var14 * var14)
					/ 32.0F - (par1EntityDragon.ticksExisted + par9) * 0.01F;
			var17.startDrawing(5);
			final byte var20 = 8;

			for (int var21 = 0; var21 <= var20; ++var21) {
				final float var22 = MathHelper.sin(var21 % var20
						* (float) Math.PI * 2.0F / var20) * 0.75F;
				final float var23 = MathHelper.cos(var21 % var20
						* (float) Math.PI * 2.0F / var20) * 0.75F;
				final float var24 = var21 % var20 * 1.0F / var20;
				var17.setColorOpaque_I(0);
				var17.addVertexWithUV(var22 * 0.2F, var23 * 0.2F, 0.0D, var24,
						var19);
				var17.setColorOpaque_I(16777215);
				var17.addVertexWithUV(var22, var23, var16, var24, var18);
			}

			var17.draw();
			GL11.glEnable(GL11.GL_CULL_FACE);
			GL11.glShadeModel(GL11.GL_FLAT);
			RenderHelper.enableStandardItemLighting();
			GL11.glPopMatrix();
		}
	}

	/**
	 * Renders the animation for when an enderdragon dies
	 */
	protected void renderDragonDying(final EntityDragon par1EntityDragon,
			final float par2) {
		super.renderEquippedItems(par1EntityDragon, par2);
		final Tessellator var3 = Tessellator.instance;

		if (par1EntityDragon.deathTicks > 0) {
			RenderHelper.disableStandardItemLighting();
			final float var4 = (par1EntityDragon.deathTicks + par2) / 200.0F;
			float var5 = 0.0F;

			if (var4 > 0.8F) {
				var5 = (var4 - 0.8F) / 0.2F;
			}

			final Random var6 = new Random(432L);
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			GL11.glShadeModel(GL11.GL_SMOOTH);
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE);
			GL11.glDisable(GL11.GL_ALPHA_TEST);
			GL11.glEnable(GL11.GL_CULL_FACE);
			GL11.glDepthMask(false);
			GL11.glPushMatrix();
			GL11.glTranslatef(0.0F, -1.0F, -2.0F);

			for (int var7 = 0; var7 < (var4 + var4 * var4) / 2.0F * 60.0F; ++var7) {
				GL11.glRotatef(var6.nextFloat() * 360.0F, 1.0F, 0.0F, 0.0F);
				GL11.glRotatef(var6.nextFloat() * 360.0F, 0.0F, 1.0F, 0.0F);
				GL11.glRotatef(var6.nextFloat() * 360.0F, 0.0F, 0.0F, 1.0F);
				GL11.glRotatef(var6.nextFloat() * 360.0F, 1.0F, 0.0F, 0.0F);
				GL11.glRotatef(var6.nextFloat() * 360.0F, 0.0F, 1.0F, 0.0F);
				GL11.glRotatef(var6.nextFloat() * 360.0F + var4 * 90.0F, 0.0F,
						0.0F, 1.0F);
				var3.startDrawing(6);
				final float var8 = var6.nextFloat() * 20.0F + 5.0F + var5
						* 10.0F;
				final float var9 = var6.nextFloat() * 2.0F + 1.0F + var5 * 2.0F;
				var3.setColorRGBA_I(16777215, (int) (255.0F * (1.0F - var5)));
				var3.addVertex(0.0D, 0.0D, 0.0D);
				var3.setColorRGBA_I(16711935, 0);
				var3.addVertex(-0.866D * var9, var8, -0.5F * var9);
				var3.addVertex(0.866D * var9, var8, -0.5F * var9);
				var3.addVertex(0.0D, var8, 1.0F * var9);
				var3.addVertex(-0.866D * var9, var8, -0.5F * var9);
				var3.draw();
			}

			GL11.glPopMatrix();
			GL11.glDepthMask(true);
			GL11.glDisable(GL11.GL_CULL_FACE);
			GL11.glDisable(GL11.GL_BLEND);
			GL11.glShadeModel(GL11.GL_FLAT);
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			GL11.glEnable(GL11.GL_TEXTURE_2D);
			GL11.glEnable(GL11.GL_ALPHA_TEST);
			RenderHelper.enableStandardItemLighting();
		}
	}

	/**
	 * Renders the overlay for glowing eyes and the mouth. Called by
	 * shouldRenderPass.
	 */
	protected int renderGlow(final EntityDragon par1EntityDragon,
			final int par2, final float par3) {
		if (par2 == 1) {
			GL11.glDepthFunc(GL11.GL_LEQUAL);
		}

		if (par2 != 0) {
			return -1;
		} else {
			loadTexture("/mob/enderdragon/ender_eyes.png");
			final float var4 = 1.0F;
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glDisable(GL11.GL_ALPHA_TEST);
			GL11.glBlendFunc(GL11.GL_ONE, GL11.GL_ONE);
			GL11.glDisable(GL11.GL_LIGHTING);
			GL11.glDepthFunc(GL11.GL_EQUAL);
			final char var5 = 61680;
			final int var6 = var5 % 65536;
			final int var7 = var5 / 65536;
			OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit,
					var6 / 1.0F, var7 / 1.0F);
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			GL11.glEnable(GL11.GL_LIGHTING);
			GL11.glColor4f(1.0F, 1.0F, 1.0F, var4);
			return 1;
		}
	}

	/**
	 * Queries whether should render the specified pass or not.
	 */
	@Override
	protected int shouldRenderPass(final EntityLiving par1EntityLiving,
			final int par2, final float par3) {
		return renderGlow((EntityDragon) par1EntityLiving, par2, par3);
	}

	@Override
	protected void renderEquippedItems(final EntityLiving par1EntityLiving,
			final float par2) {
		renderDragonDying((EntityDragon) par1EntityLiving, par2);
	}

	@Override
	protected void rotateCorpse(final EntityLiving par1EntityLiving,
			final float par2, final float par3, final float par4) {
		rotateDragonBody((EntityDragon) par1EntityLiving, par2, par3, par4);
	}

	/**
	 * Renders the model in RenderLiving
	 */
	@Override
	protected void renderModel(final EntityLiving par1EntityLiving,
			final float par2, final float par3, final float par4,
			final float par5, final float par6, final float par7) {
		renderDragonModel((EntityDragon) par1EntityLiving, par2, par3, par4,
				par5, par6, par7);
	}

	@Override
	public void doRenderLiving(final EntityLiving par1EntityLiving,
			final double par2, final double par4, final double par6,
			final float par8, final float par9) {
		renderDragon((EntityDragon) par1EntityLiving, par2, par4, par6, par8,
				par9);
	}

	/**
	 * Actually renders the given argument. This is a synthetic bridge method,
	 * always casting down its argument and then handing it off to a worker
	 * function which does the actual work. In all probabilty, the class Render
	 * is generic (Render<T extends Entity) and this method has signature public
	 * void doRender(T entity, double d, double d1, double d2, float f, float
	 * f1). But JAD is pre 1.5 so doesn't do that.
	 */
	@Override
	public void doRender(final Entity par1Entity, final double par2,
			final double par4, final double par6, final float par8,
			final float par9) {
		renderDragon((EntityDragon) par1Entity, par2, par4, par6, par8, par9);
	}
}
