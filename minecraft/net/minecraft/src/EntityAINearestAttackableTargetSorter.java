package net.minecraft.src;

import java.util.Comparator;

public class EntityAINearestAttackableTargetSorter implements Comparator {
	private final Entity theEntity;

	final EntityAINearestAttackableTarget parent;

	public EntityAINearestAttackableTargetSorter(
			final EntityAINearestAttackableTarget par1EntityAINearestAttackableTarget,
			final Entity par2Entity) {
		parent = par1EntityAINearestAttackableTarget;
		theEntity = par2Entity;
	}

	public int compareDistanceSq(final Entity par1Entity,
			final Entity par2Entity) {
		final double var3 = theEntity.getDistanceSqToEntity(par1Entity);
		final double var5 = theEntity.getDistanceSqToEntity(par2Entity);
		return var3 < var5 ? -1 : var3 > var5 ? 1 : 0;
	}

	@Override
	public int compare(final Object par1Obj, final Object par2Obj) {
		return compareDistanceSq((Entity) par1Obj, (Entity) par2Obj);
	}
}
