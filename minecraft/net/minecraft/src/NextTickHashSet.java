package net.minecraft.src;

import java.util.AbstractSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class NextTickHashSet extends AbstractSet {
	private final LongHashMap longHashMap = new LongHashMap();
	private int size = 0;
	private final HashSet emptySet = new HashSet();

	public NextTickHashSet(final Set var1) {
		addAll(var1);
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean contains(final Object var1) {
		if (!(var1 instanceof NextTickListEntry)) {
			return false;
		} else {
			final NextTickListEntry var2 = (NextTickListEntry) var1;

			final long var3 = ChunkCoordIntPair.chunkXZ2Int(var2.xCoord >> 4,
					var2.zCoord >> 4);
			final HashSet var5 = (HashSet) longHashMap.getValueByKey(var3);
			return var5 == null ? false : var5.contains(var2);
		}
	}

	@Override
	public boolean add(final Object var1) {
		if (!(var1 instanceof NextTickListEntry)) {
			return false;
		} else {
			final NextTickListEntry var2 = (NextTickListEntry) var1;

			final long var3 = ChunkCoordIntPair.chunkXZ2Int(var2.xCoord >> 4,
					var2.zCoord >> 4);
			HashSet var5 = (HashSet) longHashMap.getValueByKey(var3);

			if (var5 == null) {
				var5 = new HashSet();
				longHashMap.add(var3, var5);
			}

			final boolean var6 = var5.add(var2);

			if (var6) {
				++size;
			}

			return var6;
		}
	}

	@Override
	public boolean remove(final Object var1) {
		if (!(var1 instanceof NextTickListEntry)) {
			return false;
		} else {
			final NextTickListEntry var2 = (NextTickListEntry) var1;

			final long var3 = ChunkCoordIntPair.chunkXZ2Int(var2.xCoord >> 4,
					var2.zCoord >> 4);
			final HashSet var5 = (HashSet) longHashMap.getValueByKey(var3);

			if (var5 == null) {
				return false;
			} else {
				final boolean var6 = var5.remove(var2);

				if (var6) {
					--size;
				}

				return var6;
			}
		}
	}

	public Iterator getNextTickEntries(final int var1, final int var2) {
		final long var3 = ChunkCoordIntPair.chunkXZ2Int(var1, var2);
		HashSet var5 = (HashSet) longHashMap.getValueByKey(var3);

		if (var5 == null) {
			var5 = emptySet;
		}

		return var5.iterator();
	}

	@Override
	public Iterator iterator() {
		throw new UnsupportedOperationException("Not implemented");
	}
}
