package net.minecraft.src;

import java.util.List;
import java.util.Random;

public class BlockCauldron extends Block {
	private Icon field_94378_a;
	private Icon cauldronTopIcon;
	private Icon cauldronBottomIcon;

	public BlockCauldron(final int par1) {
		super(par1, Material.iron);
	}

	/**
	 * From the specified side and block metadata retrieves the blocks texture.
	 * Args: side, metadata
	 */
	@Override
	public Icon getIcon(final int par1, final int par2) {
		return par1 == 1 ? cauldronTopIcon : par1 == 0 ? cauldronBottomIcon
				: blockIcon;
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		field_94378_a = par1IconRegister.registerIcon("cauldron_inner");
		cauldronTopIcon = par1IconRegister.registerIcon("cauldron_top");
		cauldronBottomIcon = par1IconRegister.registerIcon("cauldron_bottom");
		blockIcon = par1IconRegister.registerIcon("cauldron_side");
	}

	public static Icon func_94375_b(final String par0Str) {
		return par0Str == "cauldron_inner" ? Block.cauldron.field_94378_a
				: par0Str == "cauldron_bottom" ? Block.cauldron.cauldronBottomIcon
						: null;
	}

	/**
	 * Adds all intersecting collision boxes to a list. (Be sure to only add
	 * boxes to the list if they intersect the mask.) Parameters: World, X, Y,
	 * Z, mask, list, colliding entity
	 */
	@Override
	public void addCollisionBoxesToList(final World par1World, final int par2,
			final int par3, final int par4,
			final AxisAlignedBB par5AxisAlignedBB, final List par6List,
			final Entity par7Entity) {
		setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.3125F, 1.0F);
		super.addCollisionBoxesToList(par1World, par2, par3, par4,
				par5AxisAlignedBB, par6List, par7Entity);
		final float var8 = 0.125F;
		setBlockBounds(0.0F, 0.0F, 0.0F, var8, 1.0F, 1.0F);
		super.addCollisionBoxesToList(par1World, par2, par3, par4,
				par5AxisAlignedBB, par6List, par7Entity);
		setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, var8);
		super.addCollisionBoxesToList(par1World, par2, par3, par4,
				par5AxisAlignedBB, par6List, par7Entity);
		setBlockBounds(1.0F - var8, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
		super.addCollisionBoxesToList(par1World, par2, par3, par4,
				par5AxisAlignedBB, par6List, par7Entity);
		setBlockBounds(0.0F, 0.0F, 1.0F - var8, 1.0F, 1.0F, 1.0F);
		super.addCollisionBoxesToList(par1World, par2, par3, par4,
				par5AxisAlignedBB, par6List, par7Entity);
		setBlockBoundsForItemRender();
	}

	/**
	 * Sets the block's bounds for rendering it as an item
	 */
	@Override
	public void setBlockBoundsForItemRender() {
		setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return 24;
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	/**
	 * Called upon block activation (right click on the block.)
	 */
	@Override
	public boolean onBlockActivated(final World par1World, final int par2,
			final int par3, final int par4,
			final EntityPlayer par5EntityPlayer, final int par6,
			final float par7, final float par8, final float par9) {
		if (par1World.isRemote) {
			return true;
		} else {
			final ItemStack var10 = par5EntityPlayer.inventory.getCurrentItem();

			if (var10 == null) {
				return true;
			} else {
				final int var11 = par1World.getBlockMetadata(par2, par3, par4);

				if (var10.itemID == Item.bucketWater.itemID) {
					if (var11 < 3) {
						if (!par5EntityPlayer.capabilities.isCreativeMode) {
							par5EntityPlayer.inventory
									.setInventorySlotContents(
											par5EntityPlayer.inventory.currentItem,
											new ItemStack(Item.bucketEmpty));
						}

						par1World.setBlockMetadataWithNotify(par2, par3, par4,
								3, 2);
					}

					return true;
				} else {
					if (var10.itemID == Item.glassBottle.itemID) {
						if (var11 > 0) {
							final ItemStack var12 = new ItemStack(Item.potion,
									1, 0);

							if (!par5EntityPlayer.inventory
									.addItemStackToInventory(var12)) {
								par1World.spawnEntityInWorld(new EntityItem(
										par1World, par2 + 0.5D, par3 + 1.5D,
										par4 + 0.5D, var12));
							} else if (par5EntityPlayer instanceof EntityPlayerMP) {
								((EntityPlayerMP) par5EntityPlayer)
										.sendContainerToPlayer(par5EntityPlayer.inventoryContainer);
							}

							--var10.stackSize;

							if (var10.stackSize <= 0) {
								par5EntityPlayer.inventory
										.setInventorySlotContents(
												par5EntityPlayer.inventory.currentItem,
												(ItemStack) null);
							}

							par1World.setBlockMetadataWithNotify(par2, par3,
									par4, var11 - 1, 2);
						}
					} else if (var11 > 0
							&& var10.getItem() instanceof ItemArmor
							&& ((ItemArmor) var10.getItem()).getArmorMaterial() == EnumArmorMaterial.CLOTH) {
						final ItemArmor var13 = (ItemArmor) var10.getItem();
						var13.removeColor(var10);
						par1World.setBlockMetadataWithNotify(par2, par3, par4,
								var11 - 1, 2);
						return true;
					}

					return true;
				}
			}
		}
	}

	/**
	 * currently only used by BlockCauldron to incrament meta-data during rain
	 */
	@Override
	public void fillWithRain(final World par1World, final int par2,
			final int par3, final int par4) {
		if (par1World.rand.nextInt(20) == 1) {
			final int var5 = par1World.getBlockMetadata(par2, par3, par4);

			if (var5 < 3) {
				par1World.setBlockMetadataWithNotify(par2, par3, par4,
						var5 + 1, 2);
			}
		}
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return Item.cauldron.itemID;
	}

	/**
	 * only called by clickMiddleMouseButton , and passed to
	 * inventory.setCurrentItem (along with isCreative)
	 */
	@Override
	public int idPicked(final World par1World, final int par2, final int par3,
			final int par4) {
		return Item.cauldron.itemID;
	}
}
