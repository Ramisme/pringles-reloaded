package net.minecraft.src;

import java.util.concurrent.Callable;

class CallableScreenName implements Callable {
	final EntityRenderer entityRender;

	CallableScreenName(final EntityRenderer par1EntityRenderer) {
		entityRender = par1EntityRenderer;
	}

	public String callScreenName() {
		return EntityRenderer.getRendererMinecraft(entityRender).currentScreen
				.getClass().getCanonicalName();
	}

	@Override
	public Object call() {
		return callScreenName();
	}
}
