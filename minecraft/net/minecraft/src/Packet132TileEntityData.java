package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet132TileEntityData extends Packet {
	/** The X position of the tile entity to update. */
	public int xPosition;

	/** The Y position of the tile entity to update. */
	public int yPosition;

	/** The Z position of the tile entity to update. */
	public int zPosition;

	/** The type of update to perform on the tile entity. */
	public int actionType;

	/** Custom parameter 1 passed to the tile entity on update. */
	public NBTTagCompound customParam1;

	public Packet132TileEntityData() {
		isChunkDataPacket = true;
	}

	public Packet132TileEntityData(final int par1, final int par2,
			final int par3, final int par4,
			final NBTTagCompound par5NBTTagCompound) {
		isChunkDataPacket = true;
		xPosition = par1;
		yPosition = par2;
		zPosition = par3;
		actionType = par4;
		customParam1 = par5NBTTagCompound;
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		xPosition = par1DataInputStream.readInt();
		yPosition = par1DataInputStream.readShort();
		zPosition = par1DataInputStream.readInt();
		actionType = par1DataInputStream.readByte();
		customParam1 = Packet.readNBTTagCompound(par1DataInputStream);
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		par1DataOutputStream.writeInt(xPosition);
		par1DataOutputStream.writeShort(yPosition);
		par1DataOutputStream.writeInt(zPosition);
		par1DataOutputStream.writeByte((byte) actionType);
		Packet.writeNBTTagCompound(customParam1, par1DataOutputStream);
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleTileEntityData(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 25;
	}
}
