package net.minecraft.src;

import java.util.Random;

import net.minecraft.client.Minecraft;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.ramisme.pringles.Wrapper;
import org.ramisme.pringles.handlers.FriendsHandler;

public class RenderLiving extends Render {
	protected ModelBase mainModel;

	/** The model to be used during the render passes. */
	protected ModelBase renderPassModel;

	public RenderLiving(final ModelBase par1ModelBase, final float par2) {
		mainModel = par1ModelBase;
		shadowSize = par2;
	}

	/**
	 * Sets the model to be used in the current render pass (the first render
	 * pass is done after the primary model is rendered) Args: model
	 */
	public void setRenderPassModel(final ModelBase par1ModelBase) {
		renderPassModel = par1ModelBase;
	}

	/**
	 * Returns a rotation angle that is inbetween two other rotation angles.
	 * par1 and par2 are the angles between which to interpolate, par3 is
	 * probably a float between 0.0 and 1.0 that tells us where "between" the
	 * two angles we are. Example: par1 = 30, par2 = 50, par3 = 0.5, then return
	 * = 40
	 */
	private float interpolateRotation(final float par1, final float par2,
			final float par3) {
		float var4;

		for (var4 = par2 - par1; var4 < -180.0F; var4 += 360.0F) {
			;
		}

		while (var4 >= 180.0F) {
			var4 -= 360.0F;
		}

		return par1 + par3 * var4;
	}

	public void doRenderLiving(final EntityLiving par1EntityLiving,
			final double par2, final double par4, final double par6,
			final float par8, final float par9) {
		GL11.glPushMatrix();
		GL11.glDisable(GL11.GL_CULL_FACE);
		mainModel.onGround = renderSwingProgress(par1EntityLiving, par9);

		if (renderPassModel != null) {
			renderPassModel.onGround = mainModel.onGround;
		}

		mainModel.isRiding = par1EntityLiving.isRiding();

		if (renderPassModel != null) {
			renderPassModel.isRiding = mainModel.isRiding;
		}

		mainModel.isChild = par1EntityLiving.isChild();

		if (renderPassModel != null) {
			renderPassModel.isChild = mainModel.isChild;
		}

		try {
			final float var10 = interpolateRotation(
					par1EntityLiving.prevRenderYawOffset,
					par1EntityLiving.renderYawOffset, par9);
			final float var11 = interpolateRotation(
					par1EntityLiving.prevRotationYawHead,
					par1EntityLiving.rotationYawHead, par9);
			final float var12 = par1EntityLiving.prevRotationPitch
					+ (par1EntityLiving.rotationPitch - par1EntityLiving.prevRotationPitch)
					* par9;
			renderLivingAt(par1EntityLiving, par2, par4, par6);
			final float var13 = handleRotationFloat(par1EntityLiving, par9);
			rotateCorpse(par1EntityLiving, var13, var10, par9);
			final float var14 = 0.0625F;
			GL11.glEnable(GL12.GL_RESCALE_NORMAL);
			GL11.glScalef(-1.0F, -1.0F, 1.0F);
			preRenderCallback(par1EntityLiving, par9);
			GL11.glTranslatef(0.0F, -24.0F * var14 - 0.0078125F, 0.0F);
			float var15 = par1EntityLiving.prevLimbYaw
					+ (par1EntityLiving.limbYaw - par1EntityLiving.prevLimbYaw)
					* par9;
			float var16 = par1EntityLiving.limbSwing - par1EntityLiving.limbYaw
					* (1.0F - par9);

			if (par1EntityLiving.isChild()) {
				var16 *= 3.0F;
			}

			if (var15 > 1.0F) {
				var15 = 1.0F;
			}

			GL11.glEnable(GL11.GL_ALPHA_TEST);
			mainModel.setLivingAnimations(par1EntityLiving, var16, var15, par9);
			renderModel(par1EntityLiving, var16, var15, var13, var11 - var10,
					var12, var14);
			float var19;
			int var18;
			float var20;
			float var22;

			for (int var17 = 0; var17 < 4; ++var17) {
				var18 = shouldRenderPass(par1EntityLiving, var17, par9);

				if (var18 > 0) {
					renderPassModel.setLivingAnimations(par1EntityLiving,
							var16, var15, par9);
					renderPassModel.render(par1EntityLiving, var16, var15,
							var13, var11 - var10, var12, var14);

					if ((var18 & 240) == 16) {
						func_82408_c(par1EntityLiving, var17, par9);
						renderPassModel.render(par1EntityLiving, var16, var15,
								var13, var11 - var10, var12, var14);
					}

					if ((var18 & 15) == 15) {
						var19 = par1EntityLiving.ticksExisted + par9;
						loadTexture("%blur%/misc/glint.png");
						GL11.glEnable(GL11.GL_BLEND);
						var20 = 0.5F;
						GL11.glColor4f(var20, var20, var20, 1.0F);
						GL11.glDepthFunc(GL11.GL_EQUAL);
						GL11.glDepthMask(false);

						for (int var21 = 0; var21 < 2; ++var21) {
							GL11.glDisable(GL11.GL_LIGHTING);
							var22 = 0.76F;
							GL11.glColor4f(0.5F * var22, 0.25F * var22,
									0.8F * var22, 1.0F);
							GL11.glBlendFunc(GL11.GL_SRC_COLOR, GL11.GL_ONE);
							GL11.glMatrixMode(GL11.GL_TEXTURE);
							GL11.glLoadIdentity();
							final float var23 = var19
									* (0.001F + var21 * 0.003F) * 20.0F;
							final float var24 = 0.33333334F;
							GL11.glScalef(var24, var24, var24);
							GL11.glRotatef(30.0F - var21 * 60.0F, 0.0F, 0.0F,
									1.0F);
							GL11.glTranslatef(0.0F, var23, 0.0F);
							GL11.glMatrixMode(GL11.GL_MODELVIEW);
							renderPassModel.render(par1EntityLiving, var16,
									var15, var13, var11 - var10, var12, var14);
						}

						GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
						GL11.glMatrixMode(GL11.GL_TEXTURE);
						GL11.glDepthMask(true);
						GL11.glLoadIdentity();
						GL11.glMatrixMode(GL11.GL_MODELVIEW);
						GL11.glEnable(GL11.GL_LIGHTING);
						GL11.glDisable(GL11.GL_BLEND);
						GL11.glDepthFunc(GL11.GL_LEQUAL);
					}

					GL11.glDisable(GL11.GL_BLEND);
					GL11.glEnable(GL11.GL_ALPHA_TEST);
				}
			}

			GL11.glDepthMask(true);
			renderEquippedItems(par1EntityLiving, par9);
			final float var26 = par1EntityLiving.getBrightness(par9);
			var18 = getColorMultiplier(par1EntityLiving, var26, par9);
			OpenGlHelper.setActiveTexture(OpenGlHelper.lightmapTexUnit);
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			OpenGlHelper.setActiveTexture(OpenGlHelper.defaultTexUnit);

			if ((var18 >> 24 & 255) > 0 || par1EntityLiving.hurtTime > 0
					|| par1EntityLiving.deathTime > 0) {
				GL11.glDisable(GL11.GL_TEXTURE_2D);
				GL11.glDisable(GL11.GL_ALPHA_TEST);
				GL11.glEnable(GL11.GL_BLEND);
				GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
				GL11.glDepthFunc(GL11.GL_EQUAL);

				if (par1EntityLiving.hurtTime > 0
						|| par1EntityLiving.deathTime > 0) {
					GL11.glColor4f(var26, 0.0F, 0.0F, 0.4F);
					mainModel.render(par1EntityLiving, var16, var15, var13,
							var11 - var10, var12, var14);

					for (int var27 = 0; var27 < 4; ++var27) {
						if (inheritRenderPass(par1EntityLiving, var27, par9) >= 0) {
							GL11.glColor4f(var26, 0.0F, 0.0F, 0.4F);
							renderPassModel.render(par1EntityLiving, var16,
									var15, var13, var11 - var10, var12, var14);
						}
					}
				}

				if ((var18 >> 24 & 255) > 0) {
					var19 = (var18 >> 16 & 255) / 255.0F;
					var20 = (var18 >> 8 & 255) / 255.0F;
					final float var29 = (var18 & 255) / 255.0F;
					var22 = (var18 >> 24 & 255) / 255.0F;
					GL11.glColor4f(var19, var20, var29, var22);
					mainModel.render(par1EntityLiving, var16, var15, var13,
							var11 - var10, var12, var14);

					for (int var28 = 0; var28 < 4; ++var28) {
						if (inheritRenderPass(par1EntityLiving, var28, par9) >= 0) {
							GL11.glColor4f(var19, var20, var29, var22);
							renderPassModel.render(par1EntityLiving, var16,
									var15, var13, var11 - var10, var12, var14);
						}
					}
				}

				GL11.glDepthFunc(GL11.GL_LEQUAL);
				GL11.glDisable(GL11.GL_BLEND);
				GL11.glEnable(GL11.GL_ALPHA_TEST);
				GL11.glEnable(GL11.GL_TEXTURE_2D);
			}

			GL11.glDisable(GL12.GL_RESCALE_NORMAL);
		} catch (final Exception var25) {
			var25.printStackTrace();
		}

		OpenGlHelper.setActiveTexture(OpenGlHelper.lightmapTexUnit);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		OpenGlHelper.setActiveTexture(OpenGlHelper.defaultTexUnit);
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glPopMatrix();
		passSpecialRender(par1EntityLiving, par2, par4, par6);
	}

	/**
	 * Renders the model in RenderLiving
	 */
	protected void renderModel(final EntityLiving par1EntityLiving,
			final float par2, final float par3, final float par4,
			final float par5, final float par6, final float par7) {
		func_98190_a(par1EntityLiving);

		if (!par1EntityLiving.isInvisible()) {
			mainModel.render(par1EntityLiving, par2, par3, par4, par5, par6,
					par7);
		} else if (!par1EntityLiving
				.func_98034_c(Minecraft.getMinecraft().thePlayer)) {
			GL11.glPushMatrix();
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 0.15F);
			GL11.glDepthMask(false);
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
			GL11.glAlphaFunc(GL11.GL_GREATER, 0.003921569F);
			mainModel.render(par1EntityLiving, par2, par3, par4, par5, par6,
					par7);
			GL11.glDisable(GL11.GL_BLEND);
			GL11.glAlphaFunc(GL11.GL_GREATER, 0.1F);
			GL11.glPopMatrix();
			GL11.glDepthMask(true);
		} else {
			mainModel.setRotationAngles(par2, par3, par4, par5, par6, par7,
					par1EntityLiving);
		}
	}

	protected void func_98190_a(final EntityLiving par1EntityLiving) {
		loadTexture(par1EntityLiving.getTexture());
	}

	/**
	 * Sets a simple glTranslate on a LivingEntity.
	 */
	protected void renderLivingAt(final EntityLiving par1EntityLiving,
			final double par2, final double par4, final double par6) {
		GL11.glTranslatef((float) par2, (float) par4, (float) par6);
	}

	protected void rotateCorpse(final EntityLiving par1EntityLiving,
			final float par2, final float par3, final float par4) {
		GL11.glRotatef(180.0F - par3, 0.0F, 1.0F, 0.0F);

		if (par1EntityLiving.deathTime > 0) {
			float var5 = (par1EntityLiving.deathTime + par4 - 1.0F) / 20.0F * 1.6F;
			var5 = MathHelper.sqrt_float(var5);

			if (var5 > 1.0F) {
				var5 = 1.0F;
			}

			GL11.glRotatef(var5 * getDeathMaxRotation(par1EntityLiving), 0.0F,
					0.0F, 1.0F);
		}
	}

	protected float renderSwingProgress(final EntityLiving par1EntityLiving,
			final float par2) {
		return par1EntityLiving.getSwingProgress(par2);
	}

	/**
	 * Defines what float the third param in setRotationAngles of ModelBase is
	 */
	protected float handleRotationFloat(final EntityLiving par1EntityLiving,
			final float par2) {
		return par1EntityLiving.ticksExisted + par2;
	}

	protected void renderEquippedItems(final EntityLiving par1EntityLiving,
			final float par2) {
	}

	/**
	 * renders arrows the Entity has been attacked with, attached to it
	 */
	protected void renderArrowsStuckInEntity(
			final EntityLiving par1EntityLiving, final float par2) {
		final int var3 = par1EntityLiving.getArrowCountInEntity();

		if (var3 > 0) {
			final EntityArrow var4 = new EntityArrow(par1EntityLiving.worldObj,
					par1EntityLiving.posX, par1EntityLiving.posY,
					par1EntityLiving.posZ);
			final Random var5 = new Random(par1EntityLiving.entityId);
			RenderHelper.disableStandardItemLighting();

			for (int var6 = 0; var6 < var3; ++var6) {
				GL11.glPushMatrix();
				final ModelRenderer var7 = mainModel.getRandomModelBox(var5);
				final ModelBox var8 = (ModelBox) var7.cubeList.get(var5
						.nextInt(var7.cubeList.size()));
				var7.postRender(0.0625F);
				float var9 = var5.nextFloat();
				float var10 = var5.nextFloat();
				float var11 = var5.nextFloat();
				final float var12 = (var8.posX1 + (var8.posX2 - var8.posX1)
						* var9) / 16.0F;
				final float var13 = (var8.posY1 + (var8.posY2 - var8.posY1)
						* var10) / 16.0F;
				final float var14 = (var8.posZ1 + (var8.posZ2 - var8.posZ1)
						* var11) / 16.0F;
				GL11.glTranslatef(var12, var13, var14);
				var9 = var9 * 2.0F - 1.0F;
				var10 = var10 * 2.0F - 1.0F;
				var11 = var11 * 2.0F - 1.0F;
				var9 *= -1.0F;
				var10 *= -1.0F;
				var11 *= -1.0F;
				final float var15 = MathHelper.sqrt_float(var9 * var9 + var11
						* var11);
				var4.prevRotationYaw = var4.rotationYaw = (float) (Math.atan2(
						var9, var11) * 180.0D / Math.PI);
				var4.prevRotationPitch = var4.rotationPitch = (float) (Math
						.atan2(var10, var15) * 180.0D / Math.PI);
				final double var16 = 0.0D;
				final double var18 = 0.0D;
				final double var20 = 0.0D;
				final float var22 = 0.0F;
				renderManager.renderEntityWithPosYaw(var4, var16, var18, var20,
						var22, par2);
				GL11.glPopMatrix();
			}

			RenderHelper.enableStandardItemLighting();
		}
	}

	protected int inheritRenderPass(final EntityLiving par1EntityLiving,
			final int par2, final float par3) {
		return shouldRenderPass(par1EntityLiving, par2, par3);
	}

	/**
	 * Queries whether should render the specified pass or not.
	 */
	protected int shouldRenderPass(final EntityLiving par1EntityLiving,
			final int par2, final float par3) {
		return -1;
	}

	protected void func_82408_c(final EntityLiving par1EntityLiving,
			final int par2, final float par3) {
	}

	protected float getDeathMaxRotation(final EntityLiving par1EntityLiving) {
		return 90.0F;
	}

	/**
	 * Returns an ARGB int color back. Args: entityLiving, lightBrightness,
	 * partialTickTime
	 */
	protected int getColorMultiplier(final EntityLiving par1EntityLiving,
			final float par2, final float par3) {
		return 0;
	}

	/**
	 * Allows the render to do any OpenGL state modifications necessary before
	 * the model is rendered. Args: entityLiving, partialTickTime
	 */
	protected void preRenderCallback(final EntityLiving par1EntityLiving,
			final float par2) {
	}

	/**
	 * Passes the specialRender and renders it
	 */
	protected void passSpecialRender(final EntityLiving par1EntityLiving,
			final double par2, final double par4, final double par6) {
		if (Minecraft.isGuiEnabled()
				&& par1EntityLiving != renderManager.livingPlayer
				&& !par1EntityLiving
						.func_98034_c(Minecraft.getMinecraft().thePlayer)
				&& (par1EntityLiving.func_94059_bO() || par1EntityLiving
						.func_94056_bM()
						&& par1EntityLiving == renderManager.field_96451_i)) {
			final float var8 = 1.6F;
			final float var9 = 0.016666668F * var8;
			final double var10 = par1EntityLiving
					.getDistanceSqToEntity(renderManager.livingPlayer);
			final float var12 = par1EntityLiving.isSneaking() ? 32.0F : 64.0F;

			final String var13 = par1EntityLiving.getTranslatedEntityName();
			func_96449_a(par1EntityLiving, par2, par4, par6, var13, var9, 200);
		}
	}

	protected void func_96449_a(final EntityLiving par1EntityLiving,
			final double par2, final double par4, final double par6,
			final String par8Str, final float par9, final double par10) {
		if (par1EntityLiving.isPlayerSleeping()) {
			renderLivingLabel(par1EntityLiving, par8Str, par2, par4 - 1.5D,
					par6, 200);
		} else {
			renderLivingLabel(par1EntityLiving, par8Str, par2, par4, par6, 200);
		}
	}

	/**
	 * Draws the debug or playername text above a living
	 */
	protected void renderLivingLabel(final EntityLiving par1EntityLiving,
			final String par2Str, final double par3, final double par5,
			final double par7, final int par9) {
		if (!(par1EntityLiving instanceof EntityPlayer)) {
			return;
		}

		final EntityPlayer par1EntityPlayer = (EntityPlayer) par1EntityLiving;
		final float distance = Wrapper.getInstance().getPlayer()
				.getDistanceToEntity(par1EntityPlayer);
		final FontRenderer fontRenderer = Wrapper.getInstance().getMinecraft().fontRenderer;
		final int scale = 240;
		final byte height = -5;

		final String rawUsername = (par1EntityPlayer.username);
		String username = StringUtils.stripControlCodes(rawUsername);

		username = FriendsHandler.getInstance().isFriend(username) ? FriendsHandler
				.getInstance().getFriendAlias(username) : rawUsername;
		if (username == null) {
			return;
		}

		final int color = FriendsHandler.getInstance().isFriend(
				par1EntityPlayer) ? 0xFF5BD7ED
				: par1EntityPlayer.isSneaking() ? 0xFFFF0000
						: distance > 64 ? 0xFF00CC00 : -1;

		GL11.glPushMatrix();
		//Wrapper.getInstance().getMinecraft().entityRenderer.disableLightmap(0);
		GL11.glTranslated(par3, par5 + 2.3F, par7);
		GL11.glNormal3f(0.0F, 1.0F, 0.0F);
		GL11.glRotatef(-RenderManager.instance.playerViewY, 0.0F, 1.0F, 0.0F);
		GL11.glRotatef(RenderManager.instance.playerViewX, 1.0F, 0.0F, 0.0F);
		GL11.glScalef(-((distance + 5) / scale), -((distance + 5) / scale),
				-((distance + 5) / scale));
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glDepthMask(false);
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

		GL11.glDepthFunc(GL11.GL_LEQUAL);
		GL11.glHint(GL11.GL_PERSPECTIVE_CORRECTION_HINT, GL11.GL_FASTEST);
		final Tessellator var15 = Tessellator.instance;

		final int nameWidth = fontRenderer.getStringWidth(username) / 2;
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		var15.startDrawingQuads();
		var15.setColorRGBA_F(0.0F, 0.0F, 0.0F, 0.25F);
		var15.addVertex((double) (-nameWidth - 1), (double) (-1 + height), 0.0D);
		var15.addVertex((double) (-nameWidth - 1), (double) (9 + height), 0.0D);
		var15.addVertex((double) (nameWidth + 2), (double) (9 + height), 0.0D);
		var15.addVertex((double) (nameWidth + 2), (double) (-1 + height), 0.0D);
		var15.draw();

		GL11.glEnable(GL11.GL_TEXTURE_2D);
		fontRenderer.drawStringWithShadow(username, -nameWidth, height, color);

		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glDepthMask(true);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glColor4f(1F, 1F, 1F, 1F);
		//Wrapper.getInstance().getMinecraft().entityRenderer.enableLightmap(0);
		GL11.glPopMatrix();
	}

	/**
	 * Actually renders the given argument. This is a synthetic bridge method,
	 * always casting down its argument and then handing it off to a worker
	 * function which does the actual work. In all probabilty, the class Render
	 * is generic (Render<T extends Entity) and this method has signature public
	 * void doRender(T entity, double d, double d1, double d2, float f, float
	 * f1). But JAD is pre 1.5 so doesn't do that.
	 */
	@Override
	public void doRender(final Entity par1Entity, final double par2,
			final double par4, final double par6, final float par8,
			final float par9) {
		doRenderLiving((EntityLiving) par1Entity, par2, par4, par6, par8, par9);
	}
}
