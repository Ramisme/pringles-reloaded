package net.minecraft.src;

public class EntityAIOcelotSit extends EntityAIBase {
	private final EntityOcelot theOcelot;
	private final float field_75404_b;

	/** Tracks for how long the task has been executing */
	private int currentTick = 0;
	private int field_75402_d = 0;

	/** For how long the Ocelot should be sitting */
	private int maxSittingTicks = 0;

	/** X Coordinate of a nearby sitable block */
	private int sitableBlockX = 0;

	/** Y Coordinate of a nearby sitable block */
	private int sitableBlockY = 0;

	/** Z Coordinate of a nearby sitable block */
	private int sitableBlockZ = 0;

	public EntityAIOcelotSit(final EntityOcelot par1EntityOcelot,
			final float par2) {
		theOcelot = par1EntityOcelot;
		field_75404_b = par2;
		setMutexBits(5);
	}

	/**
	 * Returns whether the EntityAIBase should begin execution.
	 */
	@Override
	public boolean shouldExecute() {
		return theOcelot.isTamed() && !theOcelot.isSitting()
				&& theOcelot.getRNG().nextDouble() <= 0.006500000134110451D
				&& getNearbySitableBlockDistance();
	}

	/**
	 * Returns whether an in-progress EntityAIBase should continue executing
	 */
	@Override
	public boolean continueExecuting() {
		return currentTick <= maxSittingTicks
				&& field_75402_d <= 60
				&& isSittableBlock(theOcelot.worldObj, sitableBlockX,
						sitableBlockY, sitableBlockZ);
	}

	/**
	 * Execute a one shot task or start executing a continuous task
	 */
	@Override
	public void startExecuting() {
		theOcelot.getNavigator().tryMoveToXYZ(sitableBlockX + 0.5D,
				sitableBlockY + 1, sitableBlockZ + 0.5D, field_75404_b);
		currentTick = 0;
		field_75402_d = 0;
		maxSittingTicks = theOcelot.getRNG().nextInt(
				theOcelot.getRNG().nextInt(1200) + 1200) + 1200;
		theOcelot.func_70907_r().setSitting(false);
	}

	/**
	 * Resets the task
	 */
	@Override
	public void resetTask() {
		theOcelot.setSitting(false);
	}

	/**
	 * Updates the task
	 */
	@Override
	public void updateTask() {
		++currentTick;
		theOcelot.func_70907_r().setSitting(false);

		if (theOcelot.getDistanceSq(sitableBlockX, sitableBlockY + 1,
				sitableBlockZ) > 1.0D) {
			theOcelot.setSitting(false);
			theOcelot.getNavigator().tryMoveToXYZ(sitableBlockX + 0.5D,
					sitableBlockY + 1, sitableBlockZ + 0.5D, field_75404_b);
			++field_75402_d;
		} else if (!theOcelot.isSitting()) {
			theOcelot.setSitting(true);
		} else {
			--field_75402_d;
		}
	}

	/**
	 * Searches for a block to sit on within a 8 block range, returns 0 if none
	 * found
	 */
	private boolean getNearbySitableBlockDistance() {
		final int var1 = (int) theOcelot.posY;
		double var2 = 2.147483647E9D;

		for (int var4 = (int) theOcelot.posX - 8; var4 < theOcelot.posX + 8.0D; ++var4) {
			for (int var5 = (int) theOcelot.posZ - 8; var5 < theOcelot.posZ + 8.0D; ++var5) {
				if (isSittableBlock(theOcelot.worldObj, var4, var1, var5)
						&& theOcelot.worldObj.isAirBlock(var4, var1 + 1, var5)) {
					final double var6 = theOcelot.getDistanceSq(var4, var1,
							var5);

					if (var6 < var2) {
						sitableBlockX = var4;
						sitableBlockY = var1;
						sitableBlockZ = var5;
						var2 = var6;
					}
				}
			}
		}

		return var2 < 2.147483647E9D;
	}

	/**
	 * Determines whether the Ocelot wants to sit on the block at given
	 * coordinate
	 */
	private boolean isSittableBlock(final World par1World, final int par2,
			final int par3, final int par4) {
		final int var5 = par1World.getBlockId(par2, par3, par4);
		final int var6 = par1World.getBlockMetadata(par2, par3, par4);

		if (var5 == Block.chest.blockID) {
			final TileEntityChest var7 = (TileEntityChest) par1World
					.getBlockTileEntity(par2, par3, par4);

			if (var7.numUsingPlayers < 1) {
				return true;
			}
		} else {
			if (var5 == Block.furnaceBurning.blockID) {
				return true;
			}

			if (var5 == Block.bed.blockID && !BlockBed.isBlockHeadOfBed(var6)) {
				return true;
			}
		}

		return false;
	}
}
