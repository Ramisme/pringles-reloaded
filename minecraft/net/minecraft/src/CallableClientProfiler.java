package net.minecraft.src;

import java.util.concurrent.Callable;

import net.minecraft.client.Minecraft;

public class CallableClientProfiler implements Callable {
	final Minecraft theMinecraft;

	public CallableClientProfiler(final Minecraft par1Minecraft) {
		theMinecraft = par1Minecraft;
	}

	public String callClientProfilerInfo() {
		return theMinecraft.mcProfiler.profilingEnabled ? theMinecraft.mcProfiler
				.getNameOfLastSection() : "N/A (disabled)";
	}

	@Override
	public Object call() {
		return callClientProfilerInfo();
	}
}
