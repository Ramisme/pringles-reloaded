package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.security.PrivateKey;
import java.security.PublicKey;

import javax.crypto.SecretKey;

public class Packet252SharedKey extends Packet {
	private byte[] sharedSecret = new byte[0];
	private byte[] verifyToken = new byte[0];

	/**
	 * Secret AES key decrypted from sharedSecret via the server's private RSA
	 * key
	 */
	private SecretKey sharedKey;

	public Packet252SharedKey() {
	}

	public Packet252SharedKey(final SecretKey par1SecretKey,
			final PublicKey par2PublicKey, final byte[] par3ArrayOfByte) {
		sharedKey = par1SecretKey;
		sharedSecret = CryptManager.encryptData(par2PublicKey,
				par1SecretKey.getEncoded());
		verifyToken = CryptManager.encryptData(par2PublicKey, par3ArrayOfByte);
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		sharedSecret = Packet.readBytesFromStream(par1DataInputStream);
		verifyToken = Packet.readBytesFromStream(par1DataInputStream);
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		Packet.writeByteArray(par1DataOutputStream, sharedSecret);
		Packet.writeByteArray(par1DataOutputStream, verifyToken);
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleSharedKey(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 2 + sharedSecret.length + 2 + verifyToken.length;
	}

	/**
	 * Return secretKey, decrypting it from the sharedSecret byte array if
	 * needed
	 */
	public SecretKey getSharedKey(final PrivateKey par1PrivateKey) {
		return par1PrivateKey == null ? sharedKey : (sharedKey = CryptManager
				.decryptSharedKey(par1PrivateKey, sharedSecret));
	}

	/**
	 * Return the secret AES sharedKey (used by client only)
	 */
	public SecretKey getSharedKey() {
		return this.getSharedKey((PrivateKey) null);
	}

	/**
	 * Return verifyToken
	 */
	public byte[] getVerifyToken(final PrivateKey par1PrivateKey) {
		return par1PrivateKey == null ? verifyToken : CryptManager.decryptData(
				par1PrivateKey, verifyToken);
	}
}
