package net.minecraft.src;

import java.util.concurrent.Callable;

final class CallableBlockLocation implements Callable {
	final int blockXCoord;

	final int blockYCoord;

	final int blockZCoord;

	CallableBlockLocation(final int par1, final int par2, final int par3) {
		blockXCoord = par1;
		blockYCoord = par2;
		blockZCoord = par3;
	}

	public String callBlockLocationInfo() {
		return CrashReportCategory.getLocationInfo(blockXCoord, blockYCoord,
				blockZCoord);
	}

	@Override
	public Object call() {
		return callBlockLocationInfo();
	}
}
