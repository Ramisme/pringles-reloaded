package net.minecraft.src;

import java.util.Random;

public class StructureMineshaftStart extends StructureStart {
	public StructureMineshaftStart(final World par1World,
			final Random par2Random, final int par3, final int par4) {
		final ComponentMineshaftRoom var5 = new ComponentMineshaftRoom(0,
				par2Random, (par3 << 4) + 2, (par4 << 4) + 2);
		components.add(var5);
		var5.buildComponent(var5, components, par2Random);
		updateBoundingBox();
		markAvailableHeight(par1World, par2Random, 10);
	}
}
