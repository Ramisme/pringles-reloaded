package net.minecraft.src;

import java.util.concurrent.Callable;

class CallableMinecraftVersion implements Callable {
	/** Reference to the CrashReport object. */
	final CrashReport theCrashReport;

	CallableMinecraftVersion(final CrashReport par1CrashReport) {
		theCrashReport = par1CrashReport;
	}

	/**
	 * The current version of Minecraft
	 */
	public String minecraftVersion() {
		return "1.5.2";
	}

	@Override
	public Object call() {
		return minecraftVersion();
	}
}
