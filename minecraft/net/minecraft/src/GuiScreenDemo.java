package net.minecraft.src;

import java.net.URI;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

public class GuiScreenDemo extends GuiScreen {
	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui() {
		buttonList.clear();
		final byte var1 = -16;
		buttonList.add(new GuiButton(1, width / 2 - 116,
				height / 2 + 62 + var1, 114, 20, StatCollector
						.translateToLocal("demo.help.buy")));
		buttonList.add(new GuiButton(2, width / 2 + 2, height / 2 + 62 + var1,
				114, 20, StatCollector.translateToLocal("demo.help.later")));
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	@Override
	protected void actionPerformed(final GuiButton par1GuiButton) {
		switch (par1GuiButton.id) {
		case 1:
			par1GuiButton.enabled = false;

			try {
				final Class var2 = Class.forName("java.awt.Desktop");
				final Object var3 = var2.getMethod("getDesktop", new Class[0])
						.invoke((Object) null, new Object[0]);
				var2.getMethod("browse", new Class[] { URI.class })
						.invoke(var3,
								new Object[] { new URI(
										"http://www.minecraft.net/store?source=demo") });
			} catch (final Throwable var4) {
				var4.printStackTrace();
			}

			break;

		case 2:
			mc.displayGuiScreen((GuiScreen) null);
			mc.setIngameFocus();
		}
	}

	/**
	 * Called from the main game loop to update the screen.
	 */
	@Override
	public void updateScreen() {
		super.updateScreen();
	}

	/**
	 * Draws either a gradient over the background screen (when it exists) or a
	 * flat gradient over background.png
	 */
	@Override
	public void drawDefaultBackground() {
		super.drawDefaultBackground();
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		mc.renderEngine.bindTexture("/gui/demo_bg.png");
		final int var1 = (width - 248) / 2;
		final int var2 = (height - 166) / 2;
		drawTexturedModalRect(var1, var2, 0, 0, 248, 166);
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(final int par1, final int par2, final float par3) {
		drawDefaultBackground();
		final int var4 = (width - 248) / 2 + 10;
		int var5 = (height - 166) / 2 + 8;
		fontRenderer.drawString(
				StatCollector.translateToLocal("demo.help.title"), var4, var5,
				2039583);
		var5 += 12;
		final GameSettings var7 = mc.gameSettings;
		String var6 = StatCollector.translateToLocal("demo.help.movementShort");
		var6 = String.format(
				var6,
				new Object[] {
						Keyboard.getKeyName(var7.keyBindForward.keyCode),
						Keyboard.getKeyName(var7.keyBindLeft.keyCode),
						Keyboard.getKeyName(var7.keyBindBack.keyCode),
						Keyboard.getKeyName(var7.keyBindRight.keyCode) });
		fontRenderer.drawString(var6, var4, var5, 5197647);
		var6 = StatCollector.translateToLocal("demo.help.movementMouse");
		fontRenderer.drawString(var6, var4, var5 + 12, 5197647);
		var6 = StatCollector.translateToLocal("demo.help.jump");
		var6 = String.format(var6,
				new Object[] { Keyboard.getKeyName(var7.keyBindJump.keyCode) });
		fontRenderer.drawString(var6, var4, var5 + 24, 5197647);
		var6 = StatCollector.translateToLocal("demo.help.inventory");
		var6 = String.format(var6, new Object[] { Keyboard
				.getKeyName(var7.keyBindInventory.keyCode) });
		fontRenderer.drawString(var6, var4, var5 + 36, 5197647);
		fontRenderer.drawSplitString(
				StatCollector.translateToLocal("demo.help.fullWrapped"), var4,
				var5 + 68, 218, 2039583);
		super.drawScreen(par1, par2, par3);
	}
}
