package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet39AttachEntity extends Packet {
	public int entityId;
	public int vehicleEntityId;

	public Packet39AttachEntity() {
	}

	public Packet39AttachEntity(final Entity par1Entity, final Entity par2Entity) {
		entityId = par1Entity.entityId;
		vehicleEntityId = par2Entity != null ? par2Entity.entityId : -1;
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 8;
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		entityId = par1DataInputStream.readInt();
		vehicleEntityId = par1DataInputStream.readInt();
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		par1DataOutputStream.writeInt(entityId);
		par1DataOutputStream.writeInt(vehicleEntityId);
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleAttachEntity(this);
	}

	/**
	 * only false for the abstract Packet class, all real packets return true
	 */
	@Override
	public boolean isRealPacket() {
		return true;
	}

	/**
	 * eg return packet30entity.entityId == entityId; WARNING : will throw if
	 * you compare a packet to a different packet class
	 */
	@Override
	public boolean containsSameEntityIDAs(final Packet par1Packet) {
		final Packet39AttachEntity var2 = (Packet39AttachEntity) par1Packet;
		return var2.entityId == entityId;
	}
}
