package net.minecraft.src;

public class BlockPotato extends BlockCrops {
	private Icon[] iconArray;

	public BlockPotato(final int par1) {
		super(par1);
	}

	/**
	 * From the specified side and block metadata retrieves the blocks texture.
	 * Args: side, metadata
	 */
	@Override
	public Icon getIcon(final int par1, int par2) {
		if (par2 < 7) {
			if (par2 == 6) {
				par2 = 5;
			}

			return iconArray[par2 >> 1];
		} else {
			return iconArray[3];
		}
	}

	/**
	 * Generate a seed ItemStack for this crop.
	 */
	@Override
	protected int getSeedItem() {
		return Item.potato.itemID;
	}

	/**
	 * Generate a crop produce ItemStack for this crop.
	 */
	@Override
	protected int getCropItem() {
		return Item.potato.itemID;
	}

	/**
	 * Drops the block items with a specified chance of dropping the specified
	 * items
	 */
	@Override
	public void dropBlockAsItemWithChance(final World par1World,
			final int par2, final int par3, final int par4, final int par5,
			final float par6, final int par7) {
		super.dropBlockAsItemWithChance(par1World, par2, par3, par4, par5,
				par6, par7);

		if (!par1World.isRemote) {
			if (par5 >= 7 && par1World.rand.nextInt(50) == 0) {
				dropBlockAsItem_do(par1World, par2, par3, par4, new ItemStack(
						Item.poisonousPotato));
			}
		}
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		iconArray = new Icon[4];

		for (int var2 = 0; var2 < iconArray.length; ++var2) {
			iconArray[var2] = par1IconRegister.registerIcon("potatoes_" + var2);
		}
	}
}
