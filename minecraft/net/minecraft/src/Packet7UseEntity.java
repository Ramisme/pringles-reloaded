package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet7UseEntity extends Packet {
	/** The entity of the player (ignored by the server) */
	public int playerEntityId;

	/** The entity the player is interacting with */
	public int targetEntity;

	/**
	 * Seems to be true when the player is pointing at an entity and
	 * left-clicking and false when right-clicking.
	 */
	public int isLeftClick;

	public Packet7UseEntity() {
	}

	public Packet7UseEntity(final int par1, final int par2, final int par3) {
		playerEntityId = par1;
		targetEntity = par2;
		isLeftClick = par3;
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		playerEntityId = par1DataInputStream.readInt();
		targetEntity = par1DataInputStream.readInt();
		isLeftClick = par1DataInputStream.readByte();
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		par1DataOutputStream.writeInt(playerEntityId);
		par1DataOutputStream.writeInt(targetEntity);
		par1DataOutputStream.writeByte(isLeftClick);
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleUseEntity(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 9;
	}
}
