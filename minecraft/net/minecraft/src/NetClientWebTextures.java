package net.minecraft.src;

import net.minecraft.client.Minecraft;

class NetClientWebTextures extends GuiScreen {
	/** The Texture Pack's name. */
	final String texturePackName;

	/** Initialises Web Textures? */
	final NetClientHandler netClientHandlerWebTextures;

	NetClientWebTextures(final NetClientHandler par1NetClientHandler,
			final String par2Str) {
		netClientHandlerWebTextures = par1NetClientHandler;
		texturePackName = par2Str;
	}

	@Override
	public void confirmClicked(final boolean par1, final int par2) {
		mc = Minecraft.getMinecraft();

		if (mc.getServerData() != null) {
			mc.getServerData().setAcceptsTextures(par1);
			ServerList.func_78852_b(mc.getServerData());
		}

		if (par1) {
			mc.texturePackList.requestDownloadOfTexture(texturePackName);
		}

		mc.displayGuiScreen((GuiScreen) null);
	}
}
