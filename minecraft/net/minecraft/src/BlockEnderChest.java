package net.minecraft.src;

import java.util.Random;

public class BlockEnderChest extends BlockContainer {
	protected BlockEnderChest(final int par1) {
		super(par1, Material.rock);
		setCreativeTab(CreativeTabs.tabDecorations);
		setBlockBounds(0.0625F, 0.0F, 0.0625F, 0.9375F, 0.875F, 0.9375F);
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return 22;
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return Block.obsidian.blockID;
	}

	/**
	 * Returns the quantity of items to drop on block destruction.
	 */
	@Override
	public int quantityDropped(final Random par1Random) {
		return 8;
	}

	/**
	 * Return true if a player with Silk Touch can harvest this block directly,
	 * and not its normal drops.
	 */
	@Override
	protected boolean canSilkHarvest() {
		return true;
	}

	/**
	 * Called when the block is placed in the world.
	 */
	@Override
	public void onBlockPlacedBy(final World par1World, final int par2,
			final int par3, final int par4,
			final EntityLiving par5EntityLiving, final ItemStack par6ItemStack) {
		byte var7 = 0;
		final int var8 = MathHelper
				.floor_double(par5EntityLiving.rotationYaw * 4.0F / 360.0F + 0.5D) & 3;

		if (var8 == 0) {
			var7 = 2;
		}

		if (var8 == 1) {
			var7 = 5;
		}

		if (var8 == 2) {
			var7 = 3;
		}

		if (var8 == 3) {
			var7 = 4;
		}

		par1World.setBlockMetadataWithNotify(par2, par3, par4, var7, 2);
	}

	/**
	 * Called upon block activation (right click on the block.)
	 */
	@Override
	public boolean onBlockActivated(final World par1World, final int par2,
			final int par3, final int par4,
			final EntityPlayer par5EntityPlayer, final int par6,
			final float par7, final float par8, final float par9) {
		final InventoryEnderChest var10 = par5EntityPlayer
				.getInventoryEnderChest();
		final TileEntityEnderChest var11 = (TileEntityEnderChest) par1World
				.getBlockTileEntity(par2, par3, par4);

		if (var10 != null && var11 != null) {
			if (par1World.isBlockNormalCube(par2, par3 + 1, par4)) {
				return true;
			} else if (par1World.isRemote) {
				return true;
			} else {
				var10.setAssociatedChest(var11);
				par5EntityPlayer.displayGUIChest(var10);
				return true;
			}
		} else {
			return true;
		}
	}

	/**
	 * Returns a new instance of a block's tile entity class. Called on placing
	 * the block.
	 */
	@Override
	public TileEntity createNewTileEntity(final World par1World) {
		return new TileEntityEnderChest();
	}

	/**
	 * A randomly called display update to be able to add particles or other
	 * items for display
	 */
	@Override
	public void randomDisplayTick(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		for (int var6 = 0; var6 < 3; ++var6) {
			double var10000 = par2 + par5Random.nextFloat();
			final double var9 = par3 + par5Random.nextFloat();
			var10000 = par4 + par5Random.nextFloat();
			double var13 = 0.0D;
			double var15 = 0.0D;
			double var17 = 0.0D;
			final int var19 = par5Random.nextInt(2) * 2 - 1;
			final int var20 = par5Random.nextInt(2) * 2 - 1;
			var13 = (par5Random.nextFloat() - 0.5D) * 0.125D;
			var15 = (par5Random.nextFloat() - 0.5D) * 0.125D;
			var17 = (par5Random.nextFloat() - 0.5D) * 0.125D;
			final double var11 = par4 + 0.5D + 0.25D * var20;
			var17 = par5Random.nextFloat() * 1.0F * var20;
			final double var7 = par2 + 0.5D + 0.25D * var19;
			var13 = par5Random.nextFloat() * 1.0F * var19;
			par1World.spawnParticle("portal", var7, var9, var11, var13, var15,
					var17);
		}
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		blockIcon = par1IconRegister.registerIcon("obsidian");
	}
}
