package net.minecraft.src;

import java.util.concurrent.Callable;

import net.minecraft.client.Minecraft;

import org.lwjgl.opengl.GL11;

public class CallableGLInfo implements Callable {
	/** The Minecraft instance. */
	final Minecraft mc;

	public CallableGLInfo(final Minecraft par1Minecraft) {
		mc = par1Minecraft;
	}

	public String getTexturePack() {
		return GL11.glGetString(GL11.GL_RENDERER) + " GL version "
				+ GL11.glGetString(GL11.GL_VERSION) + ", "
				+ GL11.glGetString(GL11.GL_VENDOR);
	}

	@Override
	public Object call() {
		return getTexturePack();
	}
}
