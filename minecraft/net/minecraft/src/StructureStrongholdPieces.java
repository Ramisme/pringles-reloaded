package net.minecraft.src;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class StructureStrongholdPieces {
	private static final StructureStrongholdPieceWeight[] pieceWeightArray = new StructureStrongholdPieceWeight[] {
			new StructureStrongholdPieceWeight(
					ComponentStrongholdStraight.class, 40, 0),
			new StructureStrongholdPieceWeight(ComponentStrongholdPrison.class,
					5, 5),
			new StructureStrongholdPieceWeight(
					ComponentStrongholdLeftTurn.class, 20, 0),
			new StructureStrongholdPieceWeight(
					ComponentStrongholdRightTurn.class, 20, 0),
			new StructureStrongholdPieceWeight(
					ComponentStrongholdRoomCrossing.class, 10, 6),
			new StructureStrongholdPieceWeight(
					ComponentStrongholdStairsStraight.class, 5, 5),
			new StructureStrongholdPieceWeight(ComponentStrongholdStairs.class,
					5, 5),
			new StructureStrongholdPieceWeight(
					ComponentStrongholdCrossing.class, 5, 4),
			new StructureStrongholdPieceWeight(
					ComponentStrongholdChestCorridor.class, 5, 4),
			new StructureStrongholdPieceWeight2(
					ComponentStrongholdLibrary.class, 10, 2),
			new StructureStrongholdPieceWeight3(
					ComponentStrongholdPortalRoom.class, 20, 1) };
	private static List structurePieceList;
	private static Class strongComponentType;
	static int totalWeight = 0;
	private static final StructureStrongholdStones strongholdStones = new StructureStrongholdStones(
			(StructureStrongholdPieceWeight2) null);

	/**
	 * sets up Arrays with the Structure pieces and their weights
	 */
	public static void prepareStructurePieces() {
		StructureStrongholdPieces.structurePieceList = new ArrayList();
		final StructureStrongholdPieceWeight[] var0 = StructureStrongholdPieces.pieceWeightArray;
		final int var1 = var0.length;

		for (int var2 = 0; var2 < var1; ++var2) {
			final StructureStrongholdPieceWeight var3 = var0[var2];
			var3.instancesSpawned = 0;
			StructureStrongholdPieces.structurePieceList.add(var3);
		}

		StructureStrongholdPieces.strongComponentType = null;
	}

	private static boolean canAddStructurePieces() {
		boolean var0 = false;
		StructureStrongholdPieces.totalWeight = 0;
		StructureStrongholdPieceWeight var2;

		for (final Iterator var1 = StructureStrongholdPieces.structurePieceList
				.iterator(); var1.hasNext(); StructureStrongholdPieces.totalWeight += var2.pieceWeight) {
			var2 = (StructureStrongholdPieceWeight) var1.next();

			if (var2.instancesLimit > 0
					&& var2.instancesSpawned < var2.instancesLimit) {
				var0 = true;
			}
		}

		return var0;
	}

	/**
	 * translates the PieceWeight class to the Component class
	 */
	private static ComponentStronghold getStrongholdComponentFromWeightedPiece(
			final Class par0Class, final List par1List,
			final Random par2Random, final int par3, final int par4,
			final int par5, final int par6, final int par7) {
		Object var8 = null;

		if (par0Class == ComponentStrongholdStraight.class) {
			var8 = ComponentStrongholdStraight.findValidPlacement(par1List,
					par2Random, par3, par4, par5, par6, par7);
		} else if (par0Class == ComponentStrongholdPrison.class) {
			var8 = ComponentStrongholdPrison.findValidPlacement(par1List,
					par2Random, par3, par4, par5, par6, par7);
		} else if (par0Class == ComponentStrongholdLeftTurn.class) {
			var8 = ComponentStrongholdLeftTurn.findValidPlacement(par1List,
					par2Random, par3, par4, par5, par6, par7);
		} else if (par0Class == ComponentStrongholdRightTurn.class) {
			var8 = ComponentStrongholdLeftTurn.findValidPlacement(par1List,
					par2Random, par3, par4, par5, par6, par7);
		} else if (par0Class == ComponentStrongholdRoomCrossing.class) {
			var8 = ComponentStrongholdRoomCrossing.findValidPlacement(par1List,
					par2Random, par3, par4, par5, par6, par7);
		} else if (par0Class == ComponentStrongholdStairsStraight.class) {
			var8 = ComponentStrongholdStairsStraight.findValidPlacement(
					par1List, par2Random, par3, par4, par5, par6, par7);
		} else if (par0Class == ComponentStrongholdStairs.class) {
			var8 = ComponentStrongholdStairs.getStrongholdStairsComponent(
					par1List, par2Random, par3, par4, par5, par6, par7);
		} else if (par0Class == ComponentStrongholdCrossing.class) {
			var8 = ComponentStrongholdCrossing.findValidPlacement(par1List,
					par2Random, par3, par4, par5, par6, par7);
		} else if (par0Class == ComponentStrongholdChestCorridor.class) {
			var8 = ComponentStrongholdChestCorridor.findValidPlacement(
					par1List, par2Random, par3, par4, par5, par6, par7);
		} else if (par0Class == ComponentStrongholdLibrary.class) {
			var8 = ComponentStrongholdLibrary.findValidPlacement(par1List,
					par2Random, par3, par4, par5, par6, par7);
		} else if (par0Class == ComponentStrongholdPortalRoom.class) {
			var8 = ComponentStrongholdPortalRoom.findValidPlacement(par1List,
					par2Random, par3, par4, par5, par6, par7);
		}

		return (ComponentStronghold) var8;
	}

	private static ComponentStronghold getNextComponent(
			final ComponentStrongholdStairs2 par0ComponentStrongholdStairs2,
			final List par1List, final Random par2Random, final int par3,
			final int par4, final int par5, final int par6, final int par7) {
		if (!StructureStrongholdPieces.canAddStructurePieces()) {
			return null;
		} else {
			if (StructureStrongholdPieces.strongComponentType != null) {
				final ComponentStronghold var8 = StructureStrongholdPieces
						.getStrongholdComponentFromWeightedPiece(
								StructureStrongholdPieces.strongComponentType,
								par1List, par2Random, par3, par4, par5, par6,
								par7);
				StructureStrongholdPieces.strongComponentType = null;

				if (var8 != null) {
					return var8;
				}
			}

			int var13 = 0;

			while (var13 < 5) {
				++var13;
				int var9 = par2Random
						.nextInt(StructureStrongholdPieces.totalWeight);
				final Iterator var10 = StructureStrongholdPieces.structurePieceList
						.iterator();

				while (var10.hasNext()) {
					final StructureStrongholdPieceWeight var11 = (StructureStrongholdPieceWeight) var10
							.next();
					var9 -= var11.pieceWeight;

					if (var9 < 0) {
						if (!var11.canSpawnMoreStructuresOfType(par7)
								|| var11 == par0ComponentStrongholdStairs2.strongholdPieceWeight) {
							break;
						}

						final ComponentStronghold var12 = StructureStrongholdPieces
								.getStrongholdComponentFromWeightedPiece(
										var11.pieceClass, par1List, par2Random,
										par3, par4, par5, par6, par7);

						if (var12 != null) {
							++var11.instancesSpawned;
							par0ComponentStrongholdStairs2.strongholdPieceWeight = var11;

							if (!var11.canSpawnMoreStructures()) {
								StructureStrongholdPieces.structurePieceList
										.remove(var11);
							}

							return var12;
						}
					}
				}
			}

			final StructureBoundingBox var14 = ComponentStrongholdCorridor
					.func_74992_a(par1List, par2Random, par3, par4, par5, par6);

			if (var14 != null && var14.minY > 1) {
				return new ComponentStrongholdCorridor(par7, par2Random, var14,
						par6);
			} else {
				return null;
			}
		}
	}

	private static StructureComponent getNextValidComponent(
			final ComponentStrongholdStairs2 par0ComponentStrongholdStairs2,
			final List par1List, final Random par2Random, final int par3,
			final int par4, final int par5, final int par6, final int par7) {
		if (par7 > 50) {
			return null;
		} else if (Math.abs(par3
				- par0ComponentStrongholdStairs2.getBoundingBox().minX) <= 112
				&& Math.abs(par5
						- par0ComponentStrongholdStairs2.getBoundingBox().minZ) <= 112) {
			final ComponentStronghold var8 = StructureStrongholdPieces
					.getNextComponent(par0ComponentStrongholdStairs2, par1List,
							par2Random, par3, par4, par5, par6, par7 + 1);

			if (var8 != null) {
				par1List.add(var8);
				par0ComponentStrongholdStairs2.field_75026_c.add(var8);
			}

			return var8;
		} else {
			return null;
		}
	}

	static StructureComponent getNextValidComponentAccess(
			final ComponentStrongholdStairs2 par0ComponentStrongholdStairs2,
			final List par1List, final Random par2Random, final int par3,
			final int par4, final int par5, final int par6, final int par7) {
		return StructureStrongholdPieces.getNextValidComponent(
				par0ComponentStrongholdStairs2, par1List, par2Random, par3,
				par4, par5, par6, par7);
	}

	static Class setComponentType(final Class par0Class) {
		StructureStrongholdPieces.strongComponentType = par0Class;
		return par0Class;
	}

	static StructureStrongholdStones getStrongholdStones() {
		return StructureStrongholdPieces.strongholdStones;
	}
}
