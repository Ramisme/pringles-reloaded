package net.minecraft.src;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public abstract class ValueObject {
	@Override
	public String toString() {
		final StringBuilder var1 = new StringBuilder("{");
		final Field[] var2 = this.getClass().getFields();
		final int var3 = var2.length;

		for (int var4 = 0; var4 < var3; ++var4) {
			final Field var5 = var2[var4];

			if (!ValueObject.func_96394_a(var5)) {
				try {
					var1.append(var5.getName()).append("=")
							.append(var5.get(this)).append(" ");
				} catch (final IllegalAccessException var7) {
					;
				}
			}
		}

		var1.deleteCharAt(var1.length() - 1);
		var1.append('}');
		return var1.toString();
	}

	private static boolean func_96394_a(final Field par0Field) {
		return Modifier.isStatic(par0Field.getModifiers());
	}
}
