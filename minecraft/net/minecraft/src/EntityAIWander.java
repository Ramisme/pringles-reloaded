package net.minecraft.src;

public class EntityAIWander extends EntityAIBase {
	private final EntityCreature entity;
	private double xPosition;
	private double yPosition;
	private double zPosition;
	private final float speed;

	public EntityAIWander(final EntityCreature par1EntityCreature,
			final float par2) {
		entity = par1EntityCreature;
		speed = par2;
		setMutexBits(1);
	}

	/**
	 * Returns whether the EntityAIBase should begin execution.
	 */
	@Override
	public boolean shouldExecute() {
		if (entity.getAge() >= 100) {
			return false;
		} else if (entity.getRNG().nextInt(120) != 0) {
			return false;
		} else {
			final Vec3 var1 = RandomPositionGenerator.findRandomTarget(entity,
					10, 7);

			if (var1 == null) {
				return false;
			} else {
				xPosition = var1.xCoord;
				yPosition = var1.yCoord;
				zPosition = var1.zCoord;
				return true;
			}
		}
	}

	/**
	 * Returns whether an in-progress EntityAIBase should continue executing
	 */
	@Override
	public boolean continueExecuting() {
		return !entity.getNavigator().noPath();
	}

	/**
	 * Execute a one shot task or start executing a continuous task
	 */
	@Override
	public void startExecuting() {
		entity.getNavigator().tryMoveToXYZ(xPosition, yPosition, zPosition,
				speed);
	}
}
