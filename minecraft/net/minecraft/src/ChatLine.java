package net.minecraft.src;

public class ChatLine {
	/** GUI Update Counter value this Line was created at */
	private final int updateCounterCreated;
	private final String lineString;

	/**
	 * int value to refer to existing Chat Lines, can be 0 which means
	 * unreferrable
	 */
	private final int chatLineID;

	public ChatLine(final int par1, final String par2Str, final int par3) {
		lineString = par2Str;
		updateCounterCreated = par1;
		chatLineID = par3;
	}

	public String getChatLineString() {
		return lineString;
	}

	public int getUpdatedCounter() {
		return updateCounterCreated;
	}

	public int getChatLineID() {
		return chatLineID;
	}
}
