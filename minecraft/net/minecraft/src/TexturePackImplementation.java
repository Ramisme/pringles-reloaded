package net.minecraft.src;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.imageio.ImageIO;

import org.lwjgl.opengl.GL11;

public abstract class TexturePackImplementation implements ITexturePack {
	/**
	 * Texture pack ID as returnd by generateTexturePackID(). Used only
	 * internally and not visible to the user.
	 */
	private final String texturePackID;

	/**
	 * The name of the texture pack's zip file/directory or "Default" for the
	 * builtin texture pack. Shown in the GUI.
	 */
	private final String texturePackFileName;

	/**
	 * File object for the texture pack's zip file in TexturePackCustom or the
	 * directory in TexturePackFolder.
	 */
	protected final File texturePackFile;

	/**
	 * First line of texture pack description (from /pack.txt) displayed in the
	 * GUI
	 */
	protected String firstDescriptionLine;

	/**
	 * Second line of texture pack description (from /pack.txt) displayed in the
	 * GUI
	 */
	protected String secondDescriptionLine;
	private final ITexturePack field_98141_g;

	/** The texture pack's thumbnail image loaded from the /pack.png file. */
	protected BufferedImage thumbnailImage;

	/** The texture id for this pcak's thumbnail image. */
	private int thumbnailTextureName = -1;

	protected TexturePackImplementation(final String par1, final File par2File,
			final String par3Str, final ITexturePack par4ITexturePack) {
		texturePackID = par1;
		texturePackFileName = par3Str;
		texturePackFile = par2File;
		field_98141_g = par4ITexturePack;
		loadThumbnailImage();
		loadDescription();
	}

	/**
	 * Truncate strings to at most 34 characters. Truncates description lines
	 */
	private static String trimStringToGUIWidth(String par0Str) {
		if (par0Str != null && par0Str.length() > 34) {
			par0Str = par0Str.substring(0, 34);
		}

		return par0Str;
	}

	/**
	 * Load and initialize thumbnailImage from the the /pack.png file.
	 */
	private void loadThumbnailImage() {
		InputStream var1 = null;

		try {
			var1 = func_98137_a("/pack.png", false);
			thumbnailImage = ImageIO.read(var1);
		} catch (final IOException var11) {
			;
		} finally {
			try {
				if (var1 != null) {
					var1.close();
				}
			} catch (final IOException var10) {
				;
			}
		}
	}

	/**
	 * Load texture pack description from /pack.txt file in the texture pack
	 */
	protected void loadDescription() {
		InputStream var1 = null;
		BufferedReader var2 = null;

		try {
			var1 = func_98139_b("/pack.txt");
			var2 = new BufferedReader(new InputStreamReader(var1));
			firstDescriptionLine = TexturePackImplementation
					.trimStringToGUIWidth(var2.readLine());
			secondDescriptionLine = TexturePackImplementation
					.trimStringToGUIWidth(var2.readLine());
		} catch (final IOException var12) {
			;
		} finally {
			try {
				if (var2 != null) {
					var2.close();
				}

				if (var1 != null) {
					var1.close();
				}
			} catch (final IOException var11) {
				;
			}
		}
	}

	@Override
	public InputStream func_98137_a(final String par1Str, final boolean par2)
			throws IOException {
		try {
			return func_98139_b(par1Str);
		} catch (final IOException var4) {
			if (field_98141_g != null && par2) {
				return field_98141_g.func_98137_a(par1Str, true);
			} else {
				throw var4;
			}
		}
	}

	/**
	 * Gives a texture resource as InputStream.
	 */
	@Override
	public InputStream getResourceAsStream(final String par1Str)
			throws IOException {
		return func_98137_a(par1Str, true);
	}

	protected abstract InputStream func_98139_b(String var1) throws IOException;

	/**
	 * Delete the OpenGL texture id of the pack's thumbnail image, and close the
	 * zip file in case of TexturePackCustom.
	 */
	@Override
	public void deleteTexturePack(final RenderEngine par1RenderEngine) {
		if (thumbnailImage != null && thumbnailTextureName != -1) {
			par1RenderEngine.deleteTexture(thumbnailTextureName);
		}
	}

	/**
	 * Bind the texture id of the pack's thumbnail image, loading it if
	 * necessary.
	 */
	@Override
	public void bindThumbnailTexture(final RenderEngine par1RenderEngine) {
		if (thumbnailImage != null) {
			if (thumbnailTextureName == -1) {
				thumbnailTextureName = par1RenderEngine
						.allocateAndSetupTexture(thumbnailImage);
			}

			GL11.glBindTexture(GL11.GL_TEXTURE_2D, thumbnailTextureName);
			par1RenderEngine.resetBoundTexture();
		} else {
			par1RenderEngine.bindTexture("/gui/unknown_pack.png");
		}
	}

	@Override
	public boolean func_98138_b(final String par1Str, final boolean par2) {
		final boolean var3 = func_98140_c(par1Str);
		return !var3 && par2 && field_98141_g != null ? field_98141_g
				.func_98138_b(par1Str, par2) : var3;
	}

	public abstract boolean func_98140_c(String var1);

	/**
	 * Get the texture pack ID
	 */
	@Override
	public String getTexturePackID() {
		return texturePackID;
	}

	/**
	 * Get the file name of the texture pack, or Default if not from a custom
	 * texture pack
	 */
	@Override
	public String getTexturePackFileName() {
		return texturePackFileName;
	}

	/**
	 * Get the first line of the texture pack description (read from the
	 * pack.txt file)
	 */
	@Override
	public String getFirstDescriptionLine() {
		return firstDescriptionLine;
	}

	/**
	 * Get the second line of the texture pack description (read from the
	 * pack.txt file)
	 */
	@Override
	public String getSecondDescriptionLine() {
		return secondDescriptionLine;
	}
}
