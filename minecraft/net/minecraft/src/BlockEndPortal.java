package net.minecraft.src;

import java.util.List;
import java.util.Random;

public class BlockEndPortal extends BlockContainer {
	/**
	 * true if the enderdragon has been killed - allows end portal blocks to be
	 * created in the end
	 */
	public static boolean bossDefeated = false;

	protected BlockEndPortal(final int par1, final Material par2Material) {
		super(par1, par2Material);
		setLightValue(1.0F);
	}

	/**
	 * Returns a new instance of a block's tile entity class. Called on placing
	 * the block.
	 */
	@Override
	public TileEntity createNewTileEntity(final World par1World) {
		return new TileEntityEndPortal();
	}

	/**
	 * Updates the blocks bounds based on its current state. Args: world, x, y,
	 * z
	 */
	@Override
	public void setBlockBoundsBasedOnState(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		final float var5 = 0.0625F;
		setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, var5, 1.0F);
	}

	/**
	 * Returns true if the given side of this block type should be rendered, if
	 * the adjacent block is at the given coordinates. Args: blockAccess, x, y,
	 * z, side
	 */
	@Override
	public boolean shouldSideBeRendered(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		return par5 != 0 ? false : super.shouldSideBeRendered(par1IBlockAccess,
				par2, par3, par4, par5);
	}

	/**
	 * Adds all intersecting collision boxes to a list. (Be sure to only add
	 * boxes to the list if they intersect the mask.) Parameters: World, X, Y,
	 * Z, mask, list, colliding entity
	 */
	@Override
	public void addCollisionBoxesToList(final World par1World, final int par2,
			final int par3, final int par4,
			final AxisAlignedBB par5AxisAlignedBB, final List par6List,
			final Entity par7Entity) {
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	/**
	 * Returns the quantity of items to drop on block destruction.
	 */
	@Override
	public int quantityDropped(final Random par1Random) {
		return 0;
	}

	/**
	 * Triggered whenever an entity collides with this block (enters into the
	 * block). Args: world, x, y, z, entity
	 */
	@Override
	public void onEntityCollidedWithBlock(final World par1World,
			final int par2, final int par3, final int par4,
			final Entity par5Entity) {
		if (par5Entity.ridingEntity == null
				&& par5Entity.riddenByEntity == null && !par1World.isRemote) {
			par5Entity.travelToDimension(1);
		}
	}

	/**
	 * A randomly called display update to be able to add particles or other
	 * items for display
	 */
	@Override
	public void randomDisplayTick(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		final double var6 = par2 + par5Random.nextFloat();
		final double var8 = par3 + 0.8F;
		final double var10 = par4 + par5Random.nextFloat();
		final double var12 = 0.0D;
		final double var14 = 0.0D;
		final double var16 = 0.0D;
		par1World
				.spawnParticle("smoke", var6, var8, var10, var12, var14, var16);
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return -1;
	}

	/**
	 * Called whenever the block is added into the world. Args: world, x, y, z
	 */
	@Override
	public void onBlockAdded(final World par1World, final int par2,
			final int par3, final int par4) {
		if (!BlockEndPortal.bossDefeated) {
			if (par1World.provider.dimensionId != 0) {
				par1World.setBlockToAir(par2, par3, par4);
			}
		}
	}

	/**
	 * only called by clickMiddleMouseButton , and passed to
	 * inventory.setCurrentItem (along with isCreative)
	 */
	@Override
	public int idPicked(final World par1World, final int par2, final int par3,
			final int par4) {
		return 0;
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		blockIcon = par1IconRegister.registerIcon("portal");
	}
}
