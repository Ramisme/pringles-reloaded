package net.minecraft.src;

public class ItemTool extends Item {
	/** Array of blocks the tool has extra effect against. */
	private final Block[] blocksEffectiveAgainst;
	protected float efficiencyOnProperMaterial = 4.0F;

	/** Damage versus entities. */
	private final int damageVsEntity;

	/** The material this tool is made from. */
	protected EnumToolMaterial toolMaterial;

	protected ItemTool(final int par1, final int par2,
			final EnumToolMaterial par3EnumToolMaterial,
			final Block[] par4ArrayOfBlock) {
		super(par1);
		toolMaterial = par3EnumToolMaterial;
		blocksEffectiveAgainst = par4ArrayOfBlock;
		maxStackSize = 1;
		setMaxDamage(par3EnumToolMaterial.getMaxUses());
		efficiencyOnProperMaterial = par3EnumToolMaterial
				.getEfficiencyOnProperMaterial();
		damageVsEntity = par2 + par3EnumToolMaterial.getDamageVsEntity();
		setCreativeTab(CreativeTabs.tabTools);
	}

	/**
	 * Returns the strength of the stack against a given block. 1.0F base,
	 * (Quality+1)*2 if correct blocktype, 1.5F if sword
	 */
	@Override
	public float getStrVsBlock(final ItemStack par1ItemStack,
			final Block par2Block) {
		for (final Block element : blocksEffectiveAgainst) {
			if (element == par2Block) {
				return efficiencyOnProperMaterial;
			}
		}

		return 1.0F;
	}

	/**
	 * Current implementations of this method in child classes do not use the
	 * entry argument beside ev. They just raise the damage on the stack.
	 */
	@Override
	public boolean hitEntity(final ItemStack par1ItemStack,
			final EntityLiving par2EntityLiving,
			final EntityLiving par3EntityLiving) {
		par1ItemStack.damageItem(2, par3EntityLiving);
		return true;
	}

	@Override
	public boolean onBlockDestroyed(final ItemStack par1ItemStack,
			final World par2World, final int par3, final int par4,
			final int par5, final int par6, final EntityLiving par7EntityLiving) {
		if (Block.blocksList[par3]
				.getBlockHardness(par2World, par4, par5, par6) != 0.0D) {
			par1ItemStack.damageItem(1, par7EntityLiving);
		}

		return true;
	}

	/**
	 * Returns the damage against a given entity.
	 */
	@Override
	public int getDamageVsEntity(final Entity par1Entity) {
		return damageVsEntity;
	}

	/**
	 * Returns True is the item is renderer in full 3D when hold.
	 */
	@Override
	public boolean isFull3D() {
		return true;
	}

	/**
	 * Return the enchantability factor of the item, most of the time is based
	 * on material.
	 */
	@Override
	public int getItemEnchantability() {
		return toolMaterial.getEnchantability();
	}

	/**
	 * Return the name for this tool's material.
	 */
	public String getToolMaterialName() {
		return toolMaterial.toString();
	}

	/**
	 * Return whether this item is repairable in an anvil.
	 */
	@Override
	public boolean getIsRepairable(final ItemStack par1ItemStack,
			final ItemStack par2ItemStack) {
		return toolMaterial.getToolCraftingMaterial() == par2ItemStack.itemID ? true
				: super.getIsRepairable(par1ItemStack, par2ItemStack);
	}
}
