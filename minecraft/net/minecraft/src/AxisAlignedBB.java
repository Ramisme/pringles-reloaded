package net.minecraft.src;

public class AxisAlignedBB {
	/** ThreadLocal AABBPool */
	private static final ThreadLocal theAABBLocalPool = new AABBLocalPool();
	public double minX;
	public double minY;
	public double minZ;
	public double maxX;
	public double maxY;
	public double maxZ;

	/**
	 * Returns a bounding box with the specified bounds. Args: minX, minY, minZ,
	 * maxX, maxY, maxZ
	 */
	public static AxisAlignedBB getBoundingBox(final double par0,
			final double par2, final double par4, final double par6,
			final double par8, final double par10) {
		return new AxisAlignedBB(par0, par2, par4, par6, par8, par10);
	}

	/**
	 * Gets the ThreadLocal AABBPool
	 */
	public static AABBPool getAABBPool() {
		return (AABBPool) AxisAlignedBB.theAABBLocalPool.get();
	}

	public AxisAlignedBB(final double par1, final double par3,
			final double par5, final double par7, final double par9,
			final double par11) {
		minX = par1;
		minY = par3;
		minZ = par5;
		maxX = par7;
		maxY = par9;
		maxZ = par11;
	}

	/**
	 * Sets the bounds of the bounding box. Args: minX, minY, minZ, maxX, maxY,
	 * maxZ
	 */
	public AxisAlignedBB setBounds(final double par1, final double par3,
			final double par5, final double par7, final double par9,
			final double par11) {
		minX = par1;
		minY = par3;
		minZ = par5;
		maxX = par7;
		maxY = par9;
		maxZ = par11;
		return this;
	}

	/**
	 * Adds the coordinates to the bounding box extending it if the point lies
	 * outside the current ranges. Args: x, y, z
	 */
	public AxisAlignedBB addCoord(final double par1, final double par3,
			final double par5) {
		double var7 = minX;
		double var9 = minY;
		double var11 = minZ;
		double var13 = maxX;
		double var15 = maxY;
		double var17 = maxZ;

		if (par1 < 0.0D) {
			var7 += par1;
		}

		if (par1 > 0.0D) {
			var13 += par1;
		}

		if (par3 < 0.0D) {
			var9 += par3;
		}

		if (par3 > 0.0D) {
			var15 += par3;
		}

		if (par5 < 0.0D) {
			var11 += par5;
		}

		if (par5 > 0.0D) {
			var17 += par5;
		}

		return AxisAlignedBB.getAABBPool().getAABB(var7, var9, var11, var13,
				var15, var17);
	}

	/**
	 * Returns a bounding box expanded by the specified vector (if negative
	 * numbers are given it will shrink). Args: x, y, z
	 */
	public AxisAlignedBB expand(final double par1, final double par3,
			final double par5) {
		final double var7 = minX - par1;
		final double var9 = minY - par3;
		final double var11 = minZ - par5;
		final double var13 = maxX + par1;
		final double var15 = maxY + par3;
		final double var17 = maxZ + par5;
		return AxisAlignedBB.getAABBPool().getAABB(var7, var9, var11, var13,
				var15, var17);
	}

	/**
	 * Returns a bounding box offseted by the specified vector (if negative
	 * numbers are given it will shrink). Args: x, y, z
	 */
	public AxisAlignedBB getOffsetBoundingBox(final double par1,
			final double par3, final double par5) {
		return AxisAlignedBB.getAABBPool().getAABB(minX + par1, minY + par3,
				minZ + par5, maxX + par1, maxY + par3, maxZ + par5);
	}

	/**
	 * if instance and the argument bounding boxes overlap in the Y and Z
	 * dimensions, calculate the offset between them in the X dimension. return
	 * var2 if the bounding boxes do not overlap or if var2 is closer to 0 then
	 * the calculated offset. Otherwise return the calculated offset.
	 */
	public double calculateXOffset(final AxisAlignedBB par1AxisAlignedBB,
			double par2) {
		if (par1AxisAlignedBB.maxY > minY && par1AxisAlignedBB.minY < maxY) {
			if (par1AxisAlignedBB.maxZ > minZ && par1AxisAlignedBB.minZ < maxZ) {
				double var4;

				if (par2 > 0.0D && par1AxisAlignedBB.maxX <= minX) {
					var4 = minX - par1AxisAlignedBB.maxX;

					if (var4 < par2) {
						par2 = var4;
					}
				}

				if (par2 < 0.0D && par1AxisAlignedBB.minX >= maxX) {
					var4 = maxX - par1AxisAlignedBB.minX;

					if (var4 > par2) {
						par2 = var4;
					}
				}

				return par2;
			} else {
				return par2;
			}
		} else {
			return par2;
		}
	}

	/**
	 * if instance and the argument bounding boxes overlap in the X and Z
	 * dimensions, calculate the offset between them in the Y dimension. return
	 * var2 if the bounding boxes do not overlap or if var2 is closer to 0 then
	 * the calculated offset. Otherwise return the calculated offset.
	 */
	public double calculateYOffset(final AxisAlignedBB par1AxisAlignedBB,
			double par2) {
		if (par1AxisAlignedBB.maxX > minX && par1AxisAlignedBB.minX < maxX) {
			if (par1AxisAlignedBB.maxZ > minZ && par1AxisAlignedBB.minZ < maxZ) {
				double var4;

				if (par2 > 0.0D && par1AxisAlignedBB.maxY <= minY) {
					var4 = minY - par1AxisAlignedBB.maxY;

					if (var4 < par2) {
						par2 = var4;
					}
				}

				if (par2 < 0.0D && par1AxisAlignedBB.minY >= maxY) {
					var4 = maxY - par1AxisAlignedBB.minY;

					if (var4 > par2) {
						par2 = var4;
					}
				}

				return par2;
			} else {
				return par2;
			}
		} else {
			return par2;
		}
	}

	/**
	 * if instance and the argument bounding boxes overlap in the Y and X
	 * dimensions, calculate the offset between them in the Z dimension. return
	 * var2 if the bounding boxes do not overlap or if var2 is closer to 0 then
	 * the calculated offset. Otherwise return the calculated offset.
	 */
	public double calculateZOffset(final AxisAlignedBB par1AxisAlignedBB,
			double par2) {
		if (par1AxisAlignedBB.maxX > minX && par1AxisAlignedBB.minX < maxX) {
			if (par1AxisAlignedBB.maxY > minY && par1AxisAlignedBB.minY < maxY) {
				double var4;

				if (par2 > 0.0D && par1AxisAlignedBB.maxZ <= minZ) {
					var4 = minZ - par1AxisAlignedBB.maxZ;

					if (var4 < par2) {
						par2 = var4;
					}
				}

				if (par2 < 0.0D && par1AxisAlignedBB.minZ >= maxZ) {
					var4 = maxZ - par1AxisAlignedBB.minZ;

					if (var4 > par2) {
						par2 = var4;
					}
				}

				return par2;
			} else {
				return par2;
			}
		} else {
			return par2;
		}
	}

	/**
	 * Returns whether the given bounding box intersects with this one. Args:
	 * axisAlignedBB
	 */
	public boolean intersectsWith(final AxisAlignedBB par1AxisAlignedBB) {
		return par1AxisAlignedBB.maxX > minX && par1AxisAlignedBB.minX < maxX ? par1AxisAlignedBB.maxY > minY
				&& par1AxisAlignedBB.minY < maxY ? par1AxisAlignedBB.maxZ > minZ
				&& par1AxisAlignedBB.minZ < maxZ
				: false
				: false;
	}

	/**
	 * Offsets the current bounding box by the specified coordinates. Args: x,
	 * y, z
	 */
	public AxisAlignedBB offset(final double par1, final double par3,
			final double par5) {
		minX += par1;
		minY += par3;
		minZ += par5;
		maxX += par1;
		maxY += par3;
		maxZ += par5;
		return this;
	}

	/**
	 * Returns if the supplied Vec3D is completely inside the bounding box
	 */
	public boolean isVecInside(final Vec3 par1Vec3) {
		return par1Vec3.xCoord > minX && par1Vec3.xCoord < maxX ? par1Vec3.yCoord > minY
				&& par1Vec3.yCoord < maxY ? par1Vec3.zCoord > minZ
				&& par1Vec3.zCoord < maxZ : false
				: false;
	}

	/**
	 * Returns the average length of the edges of the bounding box.
	 */
	public double getAverageEdgeLength() {
		final double var1 = maxX - minX;
		final double var3 = maxY - minY;
		final double var5 = maxZ - minZ;
		return (var1 + var3 + var5) / 3.0D;
	}

	/**
	 * Returns a bounding box that is inset by the specified amounts
	 */
	public AxisAlignedBB contract(final double par1, final double par3,
			final double par5) {
		final double var7 = minX + par1;
		final double var9 = minY + par3;
		final double var11 = minZ + par5;
		final double var13 = maxX - par1;
		final double var15 = maxY - par3;
		final double var17 = maxZ - par5;
		return AxisAlignedBB.getAABBPool().getAABB(var7, var9, var11, var13,
				var15, var17);
	}

	/**
	 * Returns a copy of the bounding box.
	 */
	public AxisAlignedBB copy() {
		return AxisAlignedBB.getAABBPool().getAABB(minX, minY, minZ, maxX,
				maxY, maxZ);
	}

	public MovingObjectPosition calculateIntercept(final Vec3 par1Vec3,
			final Vec3 par2Vec3) {
		Vec3 var3 = par1Vec3.getIntermediateWithXValue(par2Vec3, minX);
		Vec3 var4 = par1Vec3.getIntermediateWithXValue(par2Vec3, maxX);
		Vec3 var5 = par1Vec3.getIntermediateWithYValue(par2Vec3, minY);
		Vec3 var6 = par1Vec3.getIntermediateWithYValue(par2Vec3, maxY);
		Vec3 var7 = par1Vec3.getIntermediateWithZValue(par2Vec3, minZ);
		Vec3 var8 = par1Vec3.getIntermediateWithZValue(par2Vec3, maxZ);

		if (!isVecInYZ(var3)) {
			var3 = null;
		}

		if (!isVecInYZ(var4)) {
			var4 = null;
		}

		if (!isVecInXZ(var5)) {
			var5 = null;
		}

		if (!isVecInXZ(var6)) {
			var6 = null;
		}

		if (!isVecInXY(var7)) {
			var7 = null;
		}

		if (!isVecInXY(var8)) {
			var8 = null;
		}

		Vec3 var9 = null;

		if (var3 != null
				&& (var9 == null || par1Vec3.squareDistanceTo(var3) < par1Vec3
						.squareDistanceTo(var9))) {
			var9 = var3;
		}

		if (var4 != null
				&& (var9 == null || par1Vec3.squareDistanceTo(var4) < par1Vec3
						.squareDistanceTo(var9))) {
			var9 = var4;
		}

		if (var5 != null
				&& (var9 == null || par1Vec3.squareDistanceTo(var5) < par1Vec3
						.squareDistanceTo(var9))) {
			var9 = var5;
		}

		if (var6 != null
				&& (var9 == null || par1Vec3.squareDistanceTo(var6) < par1Vec3
						.squareDistanceTo(var9))) {
			var9 = var6;
		}

		if (var7 != null
				&& (var9 == null || par1Vec3.squareDistanceTo(var7) < par1Vec3
						.squareDistanceTo(var9))) {
			var9 = var7;
		}

		if (var8 != null
				&& (var9 == null || par1Vec3.squareDistanceTo(var8) < par1Vec3
						.squareDistanceTo(var9))) {
			var9 = var8;
		}

		if (var9 == null) {
			return null;
		} else {
			byte var10 = -1;

			if (var9 == var3) {
				var10 = 4;
			}

			if (var9 == var4) {
				var10 = 5;
			}

			if (var9 == var5) {
				var10 = 0;
			}

			if (var9 == var6) {
				var10 = 1;
			}

			if (var9 == var7) {
				var10 = 2;
			}

			if (var9 == var8) {
				var10 = 3;
			}

			return new MovingObjectPosition(0, 0, 0, var10, var9);
		}
	}

	/**
	 * Checks if the specified vector is within the YZ dimensions of the
	 * bounding box. Args: Vec3D
	 */
	private boolean isVecInYZ(final Vec3 par1Vec3) {
		return par1Vec3 == null ? false : par1Vec3.yCoord >= minY
				&& par1Vec3.yCoord <= maxY && par1Vec3.zCoord >= minZ
				&& par1Vec3.zCoord <= maxZ;
	}

	/**
	 * Checks if the specified vector is within the XZ dimensions of the
	 * bounding box. Args: Vec3D
	 */
	private boolean isVecInXZ(final Vec3 par1Vec3) {
		return par1Vec3 == null ? false : par1Vec3.xCoord >= minX
				&& par1Vec3.xCoord <= maxX && par1Vec3.zCoord >= minZ
				&& par1Vec3.zCoord <= maxZ;
	}

	/**
	 * Checks if the specified vector is within the XY dimensions of the
	 * bounding box. Args: Vec3D
	 */
	private boolean isVecInXY(final Vec3 par1Vec3) {
		return par1Vec3 == null ? false : par1Vec3.xCoord >= minX
				&& par1Vec3.xCoord <= maxX && par1Vec3.yCoord >= minY
				&& par1Vec3.yCoord <= maxY;
	}

	/**
	 * Sets the bounding box to the same bounds as the bounding box passed in.
	 * Args: axisAlignedBB
	 */
	public void setBB(final AxisAlignedBB par1AxisAlignedBB) {
		minX = par1AxisAlignedBB.minX;
		minY = par1AxisAlignedBB.minY;
		minZ = par1AxisAlignedBB.minZ;
		maxX = par1AxisAlignedBB.maxX;
		maxY = par1AxisAlignedBB.maxY;
		maxZ = par1AxisAlignedBB.maxZ;
	}

	@Override
	public String toString() {
		return "box[" + minX + ", " + minY + ", " + minZ + " -> " + maxX + ", "
				+ maxY + ", " + maxZ + "]";
	}
}
