package net.minecraft.src;

import java.util.List;
import java.util.Random;

public class ComponentMineshaftCorridor extends StructureComponent {
	private final boolean hasRails;
	private final boolean hasSpiders;
	private boolean spawnerPlaced;

	/**
	 * A count of the different sections of this mine. The space between ceiling
	 * supports.
	 */
	private int sectionCount;

	public ComponentMineshaftCorridor(final int par1, final Random par2Random,
			final StructureBoundingBox par3StructureBoundingBox, final int par4) {
		super(par1);
		coordBaseMode = par4;
		boundingBox = par3StructureBoundingBox;
		hasRails = par2Random.nextInt(3) == 0;
		hasSpiders = !hasRails && par2Random.nextInt(23) == 0;

		if (coordBaseMode != 2 && coordBaseMode != 0) {
			sectionCount = par3StructureBoundingBox.getXSize() / 5;
		} else {
			sectionCount = par3StructureBoundingBox.getZSize() / 5;
		}
	}

	public static StructureBoundingBox findValidPlacement(final List par0List,
			final Random par1Random, final int par2, final int par3,
			final int par4, final int par5) {
		final StructureBoundingBox var6 = new StructureBoundingBox(par2, par3,
				par4, par2, par3 + 2, par4);
		int var7;

		for (var7 = par1Random.nextInt(3) + 2; var7 > 0; --var7) {
			final int var8 = var7 * 5;

			switch (par5) {
			case 0:
				var6.maxX = par2 + 2;
				var6.maxZ = par4 + var8 - 1;
				break;

			case 1:
				var6.minX = par2 - (var8 - 1);
				var6.maxZ = par4 + 2;
				break;

			case 2:
				var6.maxX = par2 + 2;
				var6.minZ = par4 - (var8 - 1);
				break;

			case 3:
				var6.maxX = par2 + var8 - 1;
				var6.maxZ = par4 + 2;
			}

			if (StructureComponent.findIntersecting(par0List, var6) == null) {
				break;
			}
		}

		return var7 > 0 ? var6 : null;
	}

	/**
	 * Initiates construction of the Structure Component picked, at the current
	 * Location of StructGen
	 */
	@Override
	public void buildComponent(final StructureComponent par1StructureComponent,
			final List par2List, final Random par3Random) {
		final int var4 = getComponentType();
		final int var5 = par3Random.nextInt(4);

		switch (coordBaseMode) {
		case 0:
			if (var5 <= 1) {
				StructureMineshaftPieces.getNextComponent(
						par1StructureComponent, par2List, par3Random,
						boundingBox.minX,
						boundingBox.minY - 1 + par3Random.nextInt(3),
						boundingBox.maxZ + 1, coordBaseMode, var4);
			} else if (var5 == 2) {
				StructureMineshaftPieces.getNextComponent(
						par1StructureComponent, par2List, par3Random,
						boundingBox.minX - 1,
						boundingBox.minY - 1 + par3Random.nextInt(3),
						boundingBox.maxZ - 3, 1, var4);
			} else {
				StructureMineshaftPieces.getNextComponent(
						par1StructureComponent, par2List, par3Random,
						boundingBox.maxX + 1,
						boundingBox.minY - 1 + par3Random.nextInt(3),
						boundingBox.maxZ - 3, 3, var4);
			}

			break;

		case 1:
			if (var5 <= 1) {
				StructureMineshaftPieces.getNextComponent(
						par1StructureComponent, par2List, par3Random,
						boundingBox.minX - 1,
						boundingBox.minY - 1 + par3Random.nextInt(3),
						boundingBox.minZ, coordBaseMode, var4);
			} else if (var5 == 2) {
				StructureMineshaftPieces.getNextComponent(
						par1StructureComponent, par2List, par3Random,
						boundingBox.minX,
						boundingBox.minY - 1 + par3Random.nextInt(3),
						boundingBox.minZ - 1, 2, var4);
			} else {
				StructureMineshaftPieces.getNextComponent(
						par1StructureComponent, par2List, par3Random,
						boundingBox.minX,
						boundingBox.minY - 1 + par3Random.nextInt(3),
						boundingBox.maxZ + 1, 0, var4);
			}

			break;

		case 2:
			if (var5 <= 1) {
				StructureMineshaftPieces.getNextComponent(
						par1StructureComponent, par2List, par3Random,
						boundingBox.minX,
						boundingBox.minY - 1 + par3Random.nextInt(3),
						boundingBox.minZ - 1, coordBaseMode, var4);
			} else if (var5 == 2) {
				StructureMineshaftPieces.getNextComponent(
						par1StructureComponent, par2List, par3Random,
						boundingBox.minX - 1,
						boundingBox.minY - 1 + par3Random.nextInt(3),
						boundingBox.minZ, 1, var4);
			} else {
				StructureMineshaftPieces.getNextComponent(
						par1StructureComponent, par2List, par3Random,
						boundingBox.maxX + 1,
						boundingBox.minY - 1 + par3Random.nextInt(3),
						boundingBox.minZ, 3, var4);
			}

			break;

		case 3:
			if (var5 <= 1) {
				StructureMineshaftPieces.getNextComponent(
						par1StructureComponent, par2List, par3Random,
						boundingBox.maxX + 1,
						boundingBox.minY - 1 + par3Random.nextInt(3),
						boundingBox.minZ, coordBaseMode, var4);
			} else if (var5 == 2) {
				StructureMineshaftPieces.getNextComponent(
						par1StructureComponent, par2List, par3Random,
						boundingBox.maxX - 3,
						boundingBox.minY - 1 + par3Random.nextInt(3),
						boundingBox.minZ - 1, 2, var4);
			} else {
				StructureMineshaftPieces.getNextComponent(
						par1StructureComponent, par2List, par3Random,
						boundingBox.maxX - 3,
						boundingBox.minY - 1 + par3Random.nextInt(3),
						boundingBox.maxZ + 1, 0, var4);
			}
		}

		if (var4 < 8) {
			int var6;
			int var7;

			if (coordBaseMode != 2 && coordBaseMode != 0) {
				for (var6 = boundingBox.minX + 3; var6 + 3 <= boundingBox.maxX; var6 += 5) {
					var7 = par3Random.nextInt(5);

					if (var7 == 0) {
						StructureMineshaftPieces.getNextComponent(
								par1StructureComponent, par2List, par3Random,
								var6, boundingBox.minY, boundingBox.minZ - 1,
								2, var4 + 1);
					} else if (var7 == 1) {
						StructureMineshaftPieces.getNextComponent(
								par1StructureComponent, par2List, par3Random,
								var6, boundingBox.minY, boundingBox.maxZ + 1,
								0, var4 + 1);
					}
				}
			} else {
				for (var6 = boundingBox.minZ + 3; var6 + 3 <= boundingBox.maxZ; var6 += 5) {
					var7 = par3Random.nextInt(5);

					if (var7 == 0) {
						StructureMineshaftPieces.getNextComponent(
								par1StructureComponent, par2List, par3Random,
								boundingBox.minX - 1, boundingBox.minY, var6,
								1, var4 + 1);
					} else if (var7 == 1) {
						StructureMineshaftPieces.getNextComponent(
								par1StructureComponent, par2List, par3Random,
								boundingBox.maxX + 1, boundingBox.minY, var6,
								3, var4 + 1);
					}
				}
			}
		}
	}

	/**
	 * Used to generate chests with items in it. ex: Temple Chests, Village
	 * Blacksmith Chests, Mineshaft Chests.
	 */
	@Override
	protected boolean generateStructureChestContents(
			final World par1World,
			final StructureBoundingBox par2StructureBoundingBox,
			final Random par3Random,
			final int par4,
			final int par5,
			final int par6,
			final WeightedRandomChestContent[] par7ArrayOfWeightedRandomChestContent,
			final int par8) {
		final int var9 = getXWithOffset(par4, par6);
		final int var10 = getYWithOffset(par5);
		final int var11 = getZWithOffset(par4, par6);

		if (par2StructureBoundingBox.isVecInside(var9, var10, var11)
				&& par1World.getBlockId(var9, var10, var11) == 0) {
			par1World.setBlock(
					var9,
					var10,
					var11,
					Block.rail.blockID,
					getMetadataWithOffset(Block.rail.blockID,
							par3Random.nextBoolean() ? 1 : 0), 2);
			final EntityMinecartChest var12 = new EntityMinecartChest(
					par1World, var9 + 0.5F, var10 + 0.5F, var11 + 0.5F);
			WeightedRandomChestContent.generateChestContents(par3Random,
					par7ArrayOfWeightedRandomChestContent, var12, par8);
			par1World.spawnEntityInWorld(var12);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * second Part of Structure generating, this for example places Spiderwebs,
	 * Mob Spawners, it closes Mineshafts at the end, it adds Fences...
	 */
	@Override
	public boolean addComponentParts(final World par1World,
			final Random par2Random,
			final StructureBoundingBox par3StructureBoundingBox) {
		if (isLiquidInStructureBoundingBox(par1World, par3StructureBoundingBox)) {
			return false;
		} else {
			final int var8 = sectionCount * 5 - 1;
			fillWithBlocks(par1World, par3StructureBoundingBox, 0, 0, 0, 2, 1,
					var8, 0, 0, false);
			randomlyFillWithBlocks(par1World, par3StructureBoundingBox,
					par2Random, 0.8F, 0, 2, 0, 2, 2, var8, 0, 0, false);

			if (hasSpiders) {
				randomlyFillWithBlocks(par1World, par3StructureBoundingBox,
						par2Random, 0.6F, 0, 0, 0, 2, 1, var8,
						Block.web.blockID, 0, false);
			}

			int var9;
			int var10;
			int var11;

			for (var9 = 0; var9 < sectionCount; ++var9) {
				var10 = 2 + var9 * 5;
				fillWithBlocks(par1World, par3StructureBoundingBox, 0, 0,
						var10, 0, 1, var10, Block.fence.blockID, 0, false);
				fillWithBlocks(par1World, par3StructureBoundingBox, 2, 0,
						var10, 2, 1, var10, Block.fence.blockID, 0, false);

				if (par2Random.nextInt(4) == 0) {
					fillWithBlocks(par1World, par3StructureBoundingBox, 0, 2,
							var10, 0, 2, var10, Block.planks.blockID, 0, false);
					fillWithBlocks(par1World, par3StructureBoundingBox, 2, 2,
							var10, 2, 2, var10, Block.planks.blockID, 0, false);
				} else {
					fillWithBlocks(par1World, par3StructureBoundingBox, 0, 2,
							var10, 2, 2, var10, Block.planks.blockID, 0, false);
				}

				randomlyPlaceBlock(par1World, par3StructureBoundingBox,
						par2Random, 0.1F, 0, 2, var10 - 1, Block.web.blockID, 0);
				randomlyPlaceBlock(par1World, par3StructureBoundingBox,
						par2Random, 0.1F, 2, 2, var10 - 1, Block.web.blockID, 0);
				randomlyPlaceBlock(par1World, par3StructureBoundingBox,
						par2Random, 0.1F, 0, 2, var10 + 1, Block.web.blockID, 0);
				randomlyPlaceBlock(par1World, par3StructureBoundingBox,
						par2Random, 0.1F, 2, 2, var10 + 1, Block.web.blockID, 0);
				randomlyPlaceBlock(par1World, par3StructureBoundingBox,
						par2Random, 0.05F, 0, 2, var10 - 2, Block.web.blockID,
						0);
				randomlyPlaceBlock(par1World, par3StructureBoundingBox,
						par2Random, 0.05F, 2, 2, var10 - 2, Block.web.blockID,
						0);
				randomlyPlaceBlock(par1World, par3StructureBoundingBox,
						par2Random, 0.05F, 0, 2, var10 + 2, Block.web.blockID,
						0);
				randomlyPlaceBlock(par1World, par3StructureBoundingBox,
						par2Random, 0.05F, 2, 2, var10 + 2, Block.web.blockID,
						0);
				randomlyPlaceBlock(par1World, par3StructureBoundingBox,
						par2Random, 0.05F, 1, 2, var10 - 1,
						Block.torchWood.blockID, 0);
				randomlyPlaceBlock(par1World, par3StructureBoundingBox,
						par2Random, 0.05F, 1, 2, var10 + 1,
						Block.torchWood.blockID, 0);

				if (par2Random.nextInt(100) == 0) {
					generateStructureChestContents(
							par1World,
							par3StructureBoundingBox,
							par2Random,
							2,
							0,
							var10 - 1,
							WeightedRandomChestContent.func_92080_a(
									StructureMineshaftPieces.func_78816_a(),
									new WeightedRandomChestContent[] { Item.enchantedBook
											.func_92114_b(par2Random) }),
							3 + par2Random.nextInt(4));
				}

				if (par2Random.nextInt(100) == 0) {
					generateStructureChestContents(
							par1World,
							par3StructureBoundingBox,
							par2Random,
							0,
							0,
							var10 + 1,
							WeightedRandomChestContent.func_92080_a(
									StructureMineshaftPieces.func_78816_a(),
									new WeightedRandomChestContent[] { Item.enchantedBook
											.func_92114_b(par2Random) }),
							3 + par2Random.nextInt(4));
				}

				if (hasSpiders && !spawnerPlaced) {
					var11 = getYWithOffset(0);
					int var12 = var10 - 1 + par2Random.nextInt(3);
					final int var13 = getXWithOffset(1, var12);
					var12 = getZWithOffset(1, var12);

					if (par3StructureBoundingBox.isVecInside(var13, var11,
							var12)) {
						spawnerPlaced = true;
						par1World.setBlock(var13, var11, var12,
								Block.mobSpawner.blockID, 0, 2);
						final TileEntityMobSpawner var14 = (TileEntityMobSpawner) par1World
								.getBlockTileEntity(var13, var11, var12);

						if (var14 != null) {
							var14.func_98049_a().setMobID("CaveSpider");
						}
					}
				}
			}

			for (var9 = 0; var9 <= 2; ++var9) {
				for (var10 = 0; var10 <= var8; ++var10) {
					var11 = getBlockIdAtCurrentPosition(par1World, var9, -1,
							var10, par3StructureBoundingBox);

					if (var11 == 0) {
						placeBlockAtCurrentPosition(par1World,
								Block.planks.blockID, 0, var9, -1, var10,
								par3StructureBoundingBox);
					}
				}
			}

			if (hasRails) {
				for (var9 = 0; var9 <= var8; ++var9) {
					var10 = getBlockIdAtCurrentPosition(par1World, 1, -1, var9,
							par3StructureBoundingBox);

					if (var10 > 0 && Block.opaqueCubeLookup[var10]) {
						randomlyPlaceBlock(par1World, par3StructureBoundingBox,
								par2Random, 0.7F, 1, 0, var9,
								Block.rail.blockID,
								getMetadataWithOffset(Block.rail.blockID, 0));
					}
				}
			}

			return true;
		}
	}
}
