package net.minecraft.src;

import java.io.IOException;

class ThreadTitleScreen extends Thread {
	final StringTranslate field_98135_a;

	final int field_98133_b;

	final int field_98134_c;

	final GuiMainMenu field_98132_d;

	ThreadTitleScreen(final GuiMainMenu par1GuiMainMenu,
			final StringTranslate par2StringTranslate, final int par3,
			final int par4) {
		field_98132_d = par1GuiMainMenu;
		field_98135_a = par2StringTranslate;
		field_98133_b = par3;
		field_98134_c = par4;
	}

	@Override
	public void run() {
		final McoClient var1 = new McoClient(
				GuiMainMenu.func_98058_a(field_98132_d).session);
		boolean var2 = false;

		for (int var3 = 0; var3 < 3; ++var3) {
			try {
				final Boolean var4 = var1.func_96375_b();

				if (var4.booleanValue()) {
					GuiMainMenu.func_98061_a(field_98132_d, field_98135_a,
							field_98133_b, field_98134_c);
				}

				GuiMainMenu.func_98059_a(var4.booleanValue());
			} catch (final ExceptionRetryCall var6) {
				var2 = true;
			} catch (final ExceptionMcoService var7) {
				;
			} catch (final IOException var8) {
				;
			}

			if (!var2) {
				break;
			}

			try {
				Thread.sleep(10000L);
			} catch (final InterruptedException var5) {
				Thread.currentThread().interrupt();
			}
		}
	}
}
