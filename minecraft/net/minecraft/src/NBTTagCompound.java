package net.minecraft.src;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class NBTTagCompound extends NBTBase {
	/**
	 * The key-value pairs for the tag. Each key is a UTF string, each value is
	 * a tag.
	 */
	private final Map tagMap = new HashMap();

	public NBTTagCompound() {
		super("");
	}

	public NBTTagCompound(final String par1Str) {
		super(par1Str);
	}

	/**
	 * Write the actual data contents of the tag, implemented in NBT extension
	 * classes
	 */
	@Override
	void write(final DataOutput par1DataOutput) throws IOException {
		final Iterator var2 = tagMap.values().iterator();

		while (var2.hasNext()) {
			final NBTBase var3 = (NBTBase) var2.next();
			NBTBase.writeNamedTag(var3, par1DataOutput);
		}

		par1DataOutput.writeByte(0);
	}

	/**
	 * Read the actual data contents of the tag, implemented in NBT extension
	 * classes
	 */
	@Override
	void load(final DataInput par1DataInput) throws IOException {
		tagMap.clear();
		NBTBase var2;

		while ((var2 = NBTBase.readNamedTag(par1DataInput)).getId() != 0) {
			tagMap.put(var2.getName(), var2);
		}
	}

	/**
	 * Returns all the values in the tagMap HashMap.
	 */
	public Collection getTags() {
		return tagMap.values();
	}

	/**
	 * Gets the type byte for the tag.
	 */
	@Override
	public byte getId() {
		return (byte) 10;
	}

	/**
	 * Stores the given tag into the map with the given string key. This is
	 * mostly used to store tag lists.
	 */
	public void setTag(final String par1Str, final NBTBase par2NBTBase) {
		tagMap.put(par1Str, par2NBTBase.setName(par1Str));
	}

	/**
	 * Stores a new NBTTagByte with the given byte value into the map with the
	 * given string key.
	 */
	public void setByte(final String par1Str, final byte par2) {
		tagMap.put(par1Str, new NBTTagByte(par1Str, par2));
	}

	/**
	 * Stores a new NBTTagShort with the given short value into the map with the
	 * given string key.
	 */
	public void setShort(final String par1Str, final short par2) {
		tagMap.put(par1Str, new NBTTagShort(par1Str, par2));
	}

	/**
	 * Stores a new NBTTagInt with the given integer value into the map with the
	 * given string key.
	 */
	public void setInteger(final String par1Str, final int par2) {
		tagMap.put(par1Str, new NBTTagInt(par1Str, par2));
	}

	/**
	 * Stores a new NBTTagLong with the given long value into the map with the
	 * given string key.
	 */
	public void setLong(final String par1Str, final long par2) {
		tagMap.put(par1Str, new NBTTagLong(par1Str, par2));
	}

	/**
	 * Stores a new NBTTagFloat with the given float value into the map with the
	 * given string key.
	 */
	public void setFloat(final String par1Str, final float par2) {
		tagMap.put(par1Str, new NBTTagFloat(par1Str, par2));
	}

	/**
	 * Stores a new NBTTagDouble with the given double value into the map with
	 * the given string key.
	 */
	public void setDouble(final String par1Str, final double par2) {
		tagMap.put(par1Str, new NBTTagDouble(par1Str, par2));
	}

	/**
	 * Stores a new NBTTagString with the given string value into the map with
	 * the given string key.
	 */
	public void setString(final String par1Str, final String par2Str) {
		tagMap.put(par1Str, new NBTTagString(par1Str, par2Str));
	}

	/**
	 * Stores a new NBTTagByteArray with the given array as data into the map
	 * with the given string key.
	 */
	public void setByteArray(final String par1Str, final byte[] par2ArrayOfByte) {
		tagMap.put(par1Str, new NBTTagByteArray(par1Str, par2ArrayOfByte));
	}

	/**
	 * Stores a new NBTTagIntArray with the given array as data into the map
	 * with the given string key.
	 */
	public void setIntArray(final String par1Str, final int[] par2ArrayOfInteger) {
		tagMap.put(par1Str, new NBTTagIntArray(par1Str, par2ArrayOfInteger));
	}

	/**
	 * Stores the given NBTTagCompound into the map with the given string key.
	 */
	public void setCompoundTag(final String par1Str,
			final NBTTagCompound par2NBTTagCompound) {
		tagMap.put(par1Str, par2NBTTagCompound.setName(par1Str));
	}

	/**
	 * Stores the given boolean value as a NBTTagByte, storing 1 for true and 0
	 * for false, using the given string key.
	 */
	public void setBoolean(final String par1Str, final boolean par2) {
		setByte(par1Str, (byte) (par2 ? 1 : 0));
	}

	/**
	 * gets a generic tag with the specified name
	 */
	public NBTBase getTag(final String par1Str) {
		return (NBTBase) tagMap.get(par1Str);
	}

	/**
	 * Returns whether the given string has been previously stored as a key in
	 * the map.
	 */
	public boolean hasKey(final String par1Str) {
		return tagMap.containsKey(par1Str);
	}

	/**
	 * Retrieves a byte value using the specified key, or 0 if no such key was
	 * stored.
	 */
	public byte getByte(final String par1Str) {
		try {
			return !tagMap.containsKey(par1Str) ? 0 : ((NBTTagByte) tagMap
					.get(par1Str)).data;
		} catch (final ClassCastException var3) {
			throw new ReportedException(createCrashReport(par1Str, 1, var3));
		}
	}

	/**
	 * Retrieves a short value using the specified key, or 0 if no such key was
	 * stored.
	 */
	public short getShort(final String par1Str) {
		try {
			return !tagMap.containsKey(par1Str) ? 0 : ((NBTTagShort) tagMap
					.get(par1Str)).data;
		} catch (final ClassCastException var3) {
			throw new ReportedException(createCrashReport(par1Str, 2, var3));
		}
	}

	/**
	 * Retrieves an integer value using the specified key, or 0 if no such key
	 * was stored.
	 */
	public int getInteger(final String par1Str) {
		try {
			return !tagMap.containsKey(par1Str) ? 0 : ((NBTTagInt) tagMap
					.get(par1Str)).data;
		} catch (final ClassCastException var3) {
			throw new ReportedException(createCrashReport(par1Str, 3, var3));
		}
	}

	/**
	 * Retrieves a long value using the specified key, or 0 if no such key was
	 * stored.
	 */
	public long getLong(final String par1Str) {
		try {
			return !tagMap.containsKey(par1Str) ? 0L : ((NBTTagLong) tagMap
					.get(par1Str)).data;
		} catch (final ClassCastException var3) {
			throw new ReportedException(createCrashReport(par1Str, 4, var3));
		}
	}

	/**
	 * Retrieves a float value using the specified key, or 0 if no such key was
	 * stored.
	 */
	public float getFloat(final String par1Str) {
		try {
			return !tagMap.containsKey(par1Str) ? 0.0F : ((NBTTagFloat) tagMap
					.get(par1Str)).data;
		} catch (final ClassCastException var3) {
			throw new ReportedException(createCrashReport(par1Str, 5, var3));
		}
	}

	/**
	 * Retrieves a double value using the specified key, or 0 if no such key was
	 * stored.
	 */
	public double getDouble(final String par1Str) {
		try {
			return !tagMap.containsKey(par1Str) ? 0.0D : ((NBTTagDouble) tagMap
					.get(par1Str)).data;
		} catch (final ClassCastException var3) {
			throw new ReportedException(createCrashReport(par1Str, 6, var3));
		}
	}

	/**
	 * Retrieves a string value using the specified key, or an empty string if
	 * no such key was stored.
	 */
	public String getString(final String par1Str) {
		try {
			return !tagMap.containsKey(par1Str) ? "" : ((NBTTagString) tagMap
					.get(par1Str)).data;
		} catch (final ClassCastException var3) {
			throw new ReportedException(createCrashReport(par1Str, 8, var3));
		}
	}

	/**
	 * Retrieves a byte array using the specified key, or a zero-length array if
	 * no such key was stored.
	 */
	public byte[] getByteArray(final String par1Str) {
		try {
			return !tagMap.containsKey(par1Str) ? new byte[0]
					: ((NBTTagByteArray) tagMap.get(par1Str)).byteArray;
		} catch (final ClassCastException var3) {
			throw new ReportedException(createCrashReport(par1Str, 7, var3));
		}
	}

	/**
	 * Retrieves an int array using the specified key, or a zero-length array if
	 * no such key was stored.
	 */
	public int[] getIntArray(final String par1Str) {
		try {
			return !tagMap.containsKey(par1Str) ? new int[0]
					: ((NBTTagIntArray) tagMap.get(par1Str)).intArray;
		} catch (final ClassCastException var3) {
			throw new ReportedException(createCrashReport(par1Str, 11, var3));
		}
	}

	/**
	 * Retrieves a NBTTagCompound subtag matching the specified key, or a new
	 * empty NBTTagCompound if no such key was stored.
	 */
	public NBTTagCompound getCompoundTag(final String par1Str) {
		try {
			return !tagMap.containsKey(par1Str) ? new NBTTagCompound(par1Str)
					: (NBTTagCompound) tagMap.get(par1Str);
		} catch (final ClassCastException var3) {
			throw new ReportedException(createCrashReport(par1Str, 10, var3));
		}
	}

	/**
	 * Retrieves a NBTTagList subtag matching the specified key, or a new empty
	 * NBTTagList if no such key was stored.
	 */
	public NBTTagList getTagList(final String par1Str) {
		try {
			return !tagMap.containsKey(par1Str) ? new NBTTagList(par1Str)
					: (NBTTagList) tagMap.get(par1Str);
		} catch (final ClassCastException var3) {
			throw new ReportedException(createCrashReport(par1Str, 9, var3));
		}
	}

	/**
	 * Retrieves a boolean value using the specified key, or false if no such
	 * key was stored. This uses the getByte method.
	 */
	public boolean getBoolean(final String par1Str) {
		return getByte(par1Str) != 0;
	}

	/**
	 * Remove the specified tag.
	 */
	public void removeTag(final String par1Str) {
		tagMap.remove(par1Str);
	}

	@Override
	public String toString() {
		String var1 = getName() + ":[";
		String var3;

		for (final Iterator var2 = tagMap.keySet().iterator(); var2.hasNext(); var1 = var1
				+ var3 + ":" + tagMap.get(var3) + ",") {
			var3 = (String) var2.next();
		}

		return var1 + "]";
	}

	/**
	 * Return whether this compound has no tags.
	 */
	public boolean hasNoTags() {
		return tagMap.isEmpty();
	}

	/**
	 * Create a crash report which indicates a NBT read error.
	 */
	private CrashReport createCrashReport(final String par1Str, final int par2,
			final ClassCastException par3ClassCastException) {
		final CrashReport var4 = CrashReport.makeCrashReport(
				par3ClassCastException, "Reading NBT data");
		final CrashReportCategory var5 = var4.makeCategoryDepth(
				"Corrupt NBT tag", 1);
		var5.addCrashSectionCallable("Tag type found",
				new CallableTagCompound1(this, par1Str));
		var5.addCrashSectionCallable("Tag type expected",
				new CallableTagCompound2(this, par2));
		var5.addCrashSection("Tag name", par1Str);

		if (getName() != null && getName().length() > 0) {
			var5.addCrashSection("Tag parent", getName());
		}

		return var4;
	}

	/**
	 * Creates a clone of the tag.
	 */
	@Override
	public NBTBase copy() {
		final NBTTagCompound var1 = new NBTTagCompound(getName());
		final Iterator var2 = tagMap.keySet().iterator();

		while (var2.hasNext()) {
			final String var3 = (String) var2.next();
			var1.setTag(var3, ((NBTBase) tagMap.get(var3)).copy());
		}

		return var1;
	}

	@Override
	public boolean equals(final Object par1Obj) {
		if (super.equals(par1Obj)) {
			final NBTTagCompound var2 = (NBTTagCompound) par1Obj;
			return tagMap.entrySet().equals(var2.tagMap.entrySet());
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return super.hashCode() ^ tagMap.hashCode();
	}

	/**
	 * Return the tag map for this compound.
	 */
	static Map getTagMap(final NBTTagCompound par0NBTTagCompound) {
		return par0NBTTagCompound.tagMap;
	}
}
