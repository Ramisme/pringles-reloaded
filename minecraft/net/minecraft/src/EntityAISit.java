package net.minecraft.src;

public class EntityAISit extends EntityAIBase {
	private final EntityTameable theEntity;

	/** If the EntityTameable is sitting. */
	private boolean isSitting = false;

	public EntityAISit(final EntityTameable par1EntityTameable) {
		theEntity = par1EntityTameable;
		setMutexBits(5);
	}

	/**
	 * Returns whether the EntityAIBase should begin execution.
	 */
	@Override
	public boolean shouldExecute() {
		if (!theEntity.isTamed()) {
			return false;
		} else if (theEntity.isInWater()) {
			return false;
		} else if (!theEntity.onGround) {
			return false;
		} else {
			final EntityLiving var1 = theEntity.getOwner();
			return var1 == null ? true
					: theEntity.getDistanceSqToEntity(var1) < 144.0D
							&& var1.getAITarget() != null ? false : isSitting;
		}
	}

	/**
	 * Execute a one shot task or start executing a continuous task
	 */
	@Override
	public void startExecuting() {
		theEntity.getNavigator().clearPathEntity();
		theEntity.setSitting(true);
	}

	/**
	 * Resets the task
	 */
	@Override
	public void resetTask() {
		theEntity.setSitting(false);
	}

	/**
	 * Sets the sitting flag.
	 */
	public void setSitting(final boolean par1) {
		isSitting = par1;
	}
}
