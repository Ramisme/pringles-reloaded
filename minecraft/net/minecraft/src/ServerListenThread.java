package net.minecraft.src;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class ServerListenThread extends Thread {
	private final List pendingConnections = Collections
			.synchronizedList(new ArrayList());

	/**
	 * This map stores a list of InetAddresses and the last time which they
	 * connected at
	 */
	private final HashMap recentConnections = new HashMap();
	private int connectionCounter = 0;
	private final ServerSocket myServerSocket;
	private final NetworkListenThread myNetworkListenThread;
	private final InetAddress myServerAddress;
	private final int myPort;

	public ServerListenThread(
			final NetworkListenThread par1NetworkListenThread,
			final InetAddress par2InetAddress, final int par3)
			throws IOException {
		super("Listen thread");
		myNetworkListenThread = par1NetworkListenThread;
		myPort = par3;
		myServerSocket = new ServerSocket(par3, 0, par2InetAddress);
		myServerAddress = par2InetAddress == null ? myServerSocket
				.getInetAddress() : par2InetAddress;
		myServerSocket.setPerformancePreferences(0, 2, 1);
	}

	public void processPendingConnections() {
		synchronized (pendingConnections) {
			for (int var2 = 0; var2 < pendingConnections.size(); ++var2) {
				final NetLoginHandler var3 = (NetLoginHandler) pendingConnections
						.get(var2);

				try {
					var3.tryLogin();
				} catch (final Exception var6) {
					var3.raiseErrorAndDisconnect("Internal server error");
					myNetworkListenThread
							.getServer()
							.getLogAgent()
							.logWarningException(
									"Failed to handle packet for "
											+ var3.getUsernameAndAddress()
											+ ": " + var6, var6);
				}

				if (var3.connectionComplete) {
					pendingConnections.remove(var2--);
				}

				var3.myTCPConnection.wakeThreads();
			}
		}
	}

	@Override
	public void run() {
		while (myNetworkListenThread.isListening) {
			try {
				final Socket var1 = myServerSocket.accept();
				final NetLoginHandler var2 = new NetLoginHandler(
						myNetworkListenThread.getServer(), var1, "Connection #"
								+ connectionCounter++);
				addPendingConnection(var2);
			} catch (final IOException var3) {
				var3.printStackTrace();
			}
		}

		myNetworkListenThread.getServer().getLogAgent()
				.logInfo("Closing listening thread");
	}

	private void addPendingConnection(final NetLoginHandler par1NetLoginHandler) {
		if (par1NetLoginHandler == null) {
			throw new IllegalArgumentException("Got null pendingconnection!");
		} else {
			synchronized (pendingConnections) {
				pendingConnections.add(par1NetLoginHandler);
			}
		}
	}

	public void func_71769_a(final InetAddress par1InetAddress) {
		if (par1InetAddress != null) {
			synchronized (recentConnections) {
				recentConnections.remove(par1InetAddress);
			}
		}
	}

	public void func_71768_b() {
		try {
			myServerSocket.close();
		} catch (final Throwable var2) {
			;
		}
	}

	public InetAddress getInetAddress() {
		return myServerAddress;
	}

	public int getMyPort() {
		return myPort;
	}
}
