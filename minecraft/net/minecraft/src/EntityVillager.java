package net.minecraft.src;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

public class EntityVillager extends EntityAgeable implements INpc, IMerchant {
	private int randomTickDivider;
	private boolean isMating;
	private boolean isPlaying;
	Village villageObj;

	/** This villager's current customer. */
	private EntityPlayer buyingPlayer;

	/** Initialises the MerchantRecipeList.java */
	private MerchantRecipeList buyingList;
	private int timeUntilReset;

	/** addDefaultEquipmentAndRecipies is called if this is true */
	private boolean needsInitilization;
	private int wealth;

	/** Last player to trade with this villager, used for aggressivity. */
	private String lastBuyingPlayer;
	private boolean field_82190_bM;
	private float field_82191_bN;

	/**
	 * a villagers recipe list is intialized off this list ; the 2 params are
	 * min/max amount they will trade for 1 emerald
	 */
	private static final Map villagerStockList = new HashMap();

	/**
	 * Selling list of Blacksmith items. negative numbers mean 1 emerald for n
	 * items, positive numbers are n emeralds for 1 item
	 */
	private static final Map blacksmithSellingList = new HashMap();

	public EntityVillager(final World par1World) {
		this(par1World, 0);
	}

	public EntityVillager(final World par1World, final int par2) {
		super(par1World);
		randomTickDivider = 0;
		isMating = false;
		isPlaying = false;
		villageObj = null;
		setProfession(par2);
		texture = "/mob/villager/villager.png";
		moveSpeed = 0.5F;
		setSize(0.6F, 1.8F);
		getNavigator().setBreakDoors(true);
		getNavigator().setAvoidsWater(true);
		tasks.addTask(0, new EntityAISwimming(this));
		tasks.addTask(1, new EntityAIAvoidEntity(this, EntityZombie.class,
				8.0F, 0.3F, 0.35F));
		tasks.addTask(1, new EntityAITradePlayer(this));
		tasks.addTask(1, new EntityAILookAtTradePlayer(this));
		tasks.addTask(2, new EntityAIMoveIndoors(this));
		tasks.addTask(3, new EntityAIRestrictOpenDoor(this));
		tasks.addTask(4, new EntityAIOpenDoor(this, true));
		tasks.addTask(5, new EntityAIMoveTwardsRestriction(this, 0.3F));
		tasks.addTask(6, new EntityAIVillagerMate(this));
		tasks.addTask(7, new EntityAIFollowGolem(this));
		tasks.addTask(8, new EntityAIPlay(this, 0.32F));
		tasks.addTask(9, new EntityAIWatchClosest2(this, EntityPlayer.class,
				3.0F, 1.0F));
		tasks.addTask(9, new EntityAIWatchClosest2(this, EntityVillager.class,
				5.0F, 0.02F));
		tasks.addTask(9, new EntityAIWander(this, 0.3F));
		tasks.addTask(10, new EntityAIWatchClosest(this, EntityLiving.class,
				8.0F));
	}

	/**
	 * Returns true if the newer Entity AI code should be run
	 */
	@Override
	public boolean isAIEnabled() {
		return true;
	}

	/**
	 * main AI tick function, replaces updateEntityActionState
	 */
	@Override
	protected void updateAITick() {
		if (--randomTickDivider <= 0) {
			worldObj.villageCollectionObj.addVillagerPosition(
					MathHelper.floor_double(posX),
					MathHelper.floor_double(posY),
					MathHelper.floor_double(posZ));
			randomTickDivider = 70 + rand.nextInt(50);
			villageObj = worldObj.villageCollectionObj.findNearestVillage(
					MathHelper.floor_double(posX),
					MathHelper.floor_double(posY),
					MathHelper.floor_double(posZ), 32);

			if (villageObj == null) {
				detachHome();
			} else {
				final ChunkCoordinates var1 = villageObj.getCenter();
				setHomeArea(var1.posX, var1.posY, var1.posZ,
						(int) (villageObj.getVillageRadius() * 0.6F));

				if (field_82190_bM) {
					field_82190_bM = false;
					villageObj.func_82683_b(5);
				}
			}
		}

		if (!isTrading() && timeUntilReset > 0) {
			--timeUntilReset;

			if (timeUntilReset <= 0) {
				if (needsInitilization) {
					if (buyingList.size() > 1) {
						final Iterator var3 = buyingList.iterator();

						while (var3.hasNext()) {
							final MerchantRecipe var2 = (MerchantRecipe) var3
									.next();

							if (var2.func_82784_g()) {
								var2.func_82783_a(rand.nextInt(6)
										+ rand.nextInt(6) + 2);
							}
						}
					}

					addDefaultEquipmentAndRecipies(1);
					needsInitilization = false;

					if (villageObj != null && lastBuyingPlayer != null) {
						worldObj.setEntityState(this, (byte) 14);
						villageObj.setReputationForPlayer(lastBuyingPlayer, 1);
					}
				}

				addPotionEffect(new PotionEffect(Potion.regeneration.id, 200, 0));
			}
		}

		super.updateAITick();
	}

	/**
	 * Called when a player interacts with a mob. e.g. gets milk from a cow,
	 * gets into the saddle on a pig.
	 */
	@Override
	public boolean interact(final EntityPlayer par1EntityPlayer) {
		final ItemStack var2 = par1EntityPlayer.inventory.getCurrentItem();
		final boolean var3 = var2 != null
				&& var2.itemID == Item.monsterPlacer.itemID;

		if (!var3 && isEntityAlive() && !isTrading() && !isChild()) {
			if (!worldObj.isRemote) {
				setCustomer(par1EntityPlayer);
				par1EntityPlayer.displayGUIMerchant(this, func_94057_bL());
			}

			return true;
		} else {
			return super.interact(par1EntityPlayer);
		}
	}

	@Override
	protected void entityInit() {
		super.entityInit();
		dataWatcher.addObject(16, Integer.valueOf(0));
	}

	@Override
	public int getMaxHealth() {
		return 20;
	}

	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	@Override
	public void writeEntityToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeEntityToNBT(par1NBTTagCompound);
		par1NBTTagCompound.setInteger("Profession", getProfession());
		par1NBTTagCompound.setInteger("Riches", wealth);

		if (buyingList != null) {
			par1NBTTagCompound.setCompoundTag("Offers",
					buyingList.getRecipiesAsTags());
		}
	}

	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	@Override
	public void readEntityFromNBT(final NBTTagCompound par1NBTTagCompound) {
		super.readEntityFromNBT(par1NBTTagCompound);
		setProfession(par1NBTTagCompound.getInteger("Profession"));
		wealth = par1NBTTagCompound.getInteger("Riches");

		if (par1NBTTagCompound.hasKey("Offers")) {
			final NBTTagCompound var2 = par1NBTTagCompound
					.getCompoundTag("Offers");
			buyingList = new MerchantRecipeList(var2);
		}
	}

	/**
	 * Returns the texture's file path as a String.
	 */
	@Override
	public String getTexture() {
		switch (getProfession()) {
		case 0:
			return "/mob/villager/farmer.png";

		case 1:
			return "/mob/villager/librarian.png";

		case 2:
			return "/mob/villager/priest.png";

		case 3:
			return "/mob/villager/smith.png";

		case 4:
			return "/mob/villager/butcher.png";

		default:
			return super.getTexture();
		}
	}

	/**
	 * Determines if an entity can be despawned, used on idle far away entities
	 */
	@Override
	protected boolean canDespawn() {
		return false;
	}

	/**
	 * Returns the sound this mob makes while it's alive.
	 */
	@Override
	protected String getLivingSound() {
		return "mob.villager.default";
	}

	/**
	 * Returns the sound this mob makes when it is hurt.
	 */
	@Override
	protected String getHurtSound() {
		return "mob.villager.defaulthurt";
	}

	/**
	 * Returns the sound this mob makes on death.
	 */
	@Override
	protected String getDeathSound() {
		return "mob.villager.defaultdeath";
	}

	public void setProfession(final int par1) {
		dataWatcher.updateObject(16, Integer.valueOf(par1));
	}

	public int getProfession() {
		return dataWatcher.getWatchableObjectInt(16);
	}

	public boolean isMating() {
		return isMating;
	}

	public void setMating(final boolean par1) {
		isMating = par1;
	}

	public void setPlaying(final boolean par1) {
		isPlaying = par1;
	}

	public boolean isPlaying() {
		return isPlaying;
	}

	@Override
	public void setRevengeTarget(final EntityLiving par1EntityLiving) {
		super.setRevengeTarget(par1EntityLiving);

		if (villageObj != null && par1EntityLiving != null) {
			villageObj.addOrRenewAgressor(par1EntityLiving);

			if (par1EntityLiving instanceof EntityPlayer) {
				byte var2 = -1;

				if (isChild()) {
					var2 = -3;
				}

				villageObj.setReputationForPlayer(
						((EntityPlayer) par1EntityLiving)
								.getCommandSenderName(), var2);

				if (isEntityAlive()) {
					worldObj.setEntityState(this, (byte) 13);
				}
			}
		}
	}

	/**
	 * Called when the mob's health reaches 0.
	 */
	@Override
	public void onDeath(final DamageSource par1DamageSource) {
		if (villageObj != null) {
			final Entity var2 = par1DamageSource.getEntity();

			if (var2 != null) {
				if (var2 instanceof EntityPlayer) {
					villageObj.setReputationForPlayer(
							((EntityPlayer) var2).getCommandSenderName(), -2);
				} else if (var2 instanceof IMob) {
					villageObj.endMatingSeason();
				}
			} else if (var2 == null) {
				final EntityPlayer var3 = worldObj.getClosestPlayerToEntity(
						this, 16.0D);

				if (var3 != null) {
					villageObj.endMatingSeason();
				}
			}
		}

		super.onDeath(par1DamageSource);
	}

	@Override
	public void setCustomer(final EntityPlayer par1EntityPlayer) {
		buyingPlayer = par1EntityPlayer;
	}

	@Override
	public EntityPlayer getCustomer() {
		return buyingPlayer;
	}

	public boolean isTrading() {
		return buyingPlayer != null;
	}

	@Override
	public void useRecipe(final MerchantRecipe par1MerchantRecipe) {
		par1MerchantRecipe.incrementToolUses();

		if (par1MerchantRecipe.hasSameIDsAs((MerchantRecipe) buyingList
				.get(buyingList.size() - 1))) {
			timeUntilReset = 40;
			needsInitilization = true;

			if (buyingPlayer != null) {
				lastBuyingPlayer = buyingPlayer.getCommandSenderName();
			} else {
				lastBuyingPlayer = null;
			}
		}

		if (par1MerchantRecipe.getItemToBuy().itemID == Item.emerald.itemID) {
			wealth += par1MerchantRecipe.getItemToBuy().stackSize;
		}
	}

	@Override
	public MerchantRecipeList getRecipes(final EntityPlayer par1EntityPlayer) {
		if (buyingList == null) {
			addDefaultEquipmentAndRecipies(1);
		}

		return buyingList;
	}

	private float func_82188_j(final float par1) {
		final float var2 = par1 + field_82191_bN;
		return var2 > 0.9F ? 0.9F - (var2 - 0.9F) : var2;
	}

	/**
	 * based on the villagers profession add items, equipment, and recipies adds
	 * par1 random items to the list of things that the villager wants to buy.
	 * (at most 1 of each wanted type is added)
	 */
	private void addDefaultEquipmentAndRecipies(final int par1) {
		if (buyingList != null) {
			field_82191_bN = MathHelper.sqrt_float(buyingList.size()) * 0.2F;
		} else {
			field_82191_bN = 0.0F;
		}

		MerchantRecipeList var2;
		var2 = new MerchantRecipeList();
		int var6;
		label50:

		switch (getProfession()) {
		case 0:
			EntityVillager.addMerchantItem(var2, Item.wheat.itemID, rand,
					func_82188_j(0.9F));
			EntityVillager.addMerchantItem(var2, Block.cloth.blockID, rand,
					func_82188_j(0.5F));
			EntityVillager.addMerchantItem(var2, Item.chickenRaw.itemID, rand,
					func_82188_j(0.5F));
			EntityVillager.addMerchantItem(var2, Item.fishCooked.itemID, rand,
					func_82188_j(0.4F));
			EntityVillager.addBlacksmithItem(var2, Item.bread.itemID, rand,
					func_82188_j(0.9F));
			EntityVillager.addBlacksmithItem(var2, Item.melon.itemID, rand,
					func_82188_j(0.3F));
			EntityVillager.addBlacksmithItem(var2, Item.appleRed.itemID, rand,
					func_82188_j(0.3F));
			EntityVillager.addBlacksmithItem(var2, Item.cookie.itemID, rand,
					func_82188_j(0.3F));
			EntityVillager.addBlacksmithItem(var2, Item.shears.itemID, rand,
					func_82188_j(0.3F));
			EntityVillager.addBlacksmithItem(var2, Item.flintAndSteel.itemID,
					rand, func_82188_j(0.3F));
			EntityVillager.addBlacksmithItem(var2, Item.chickenCooked.itemID,
					rand, func_82188_j(0.3F));
			EntityVillager.addBlacksmithItem(var2, Item.arrow.itemID, rand,
					func_82188_j(0.5F));

			if (rand.nextFloat() < func_82188_j(0.5F)) {
				var2.add(new MerchantRecipe(new ItemStack(Block.gravel, 10),
						new ItemStack(Item.emerald), new ItemStack(
								Item.flint.itemID, 4 + rand.nextInt(2), 0)));
			}

			break;

		case 1:
			EntityVillager.addMerchantItem(var2, Item.paper.itemID, rand,
					func_82188_j(0.8F));
			EntityVillager.addMerchantItem(var2, Item.book.itemID, rand,
					func_82188_j(0.8F));
			EntityVillager.addMerchantItem(var2, Item.writtenBook.itemID, rand,
					func_82188_j(0.3F));
			EntityVillager.addBlacksmithItem(var2, Block.bookShelf.blockID,
					rand, func_82188_j(0.8F));
			EntityVillager.addBlacksmithItem(var2, Block.glass.blockID, rand,
					func_82188_j(0.2F));
			EntityVillager.addBlacksmithItem(var2, Item.compass.itemID, rand,
					func_82188_j(0.2F));
			EntityVillager.addBlacksmithItem(var2, Item.pocketSundial.itemID,
					rand, func_82188_j(0.2F));

			if (rand.nextFloat() < func_82188_j(0.07F)) {
				final Enchantment var8 = Enchantment.field_92090_c[rand
						.nextInt(Enchantment.field_92090_c.length)];
				final int var10 = MathHelper.getRandomIntegerInRange(rand,
						var8.getMinLevel(), var8.getMaxLevel());
				final ItemStack var11 = Item.enchantedBook
						.func_92111_a(new EnchantmentData(var8, var10));
				var6 = 2 + rand.nextInt(5 + var10 * 10) + 3 * var10;
				var2.add(new MerchantRecipe(new ItemStack(Item.book),
						new ItemStack(Item.emerald, var6), var11));
			}

			break;

		case 2:
			EntityVillager.addBlacksmithItem(var2, Item.eyeOfEnder.itemID,
					rand, func_82188_j(0.3F));
			EntityVillager.addBlacksmithItem(var2, Item.expBottle.itemID, rand,
					func_82188_j(0.2F));
			EntityVillager.addBlacksmithItem(var2, Item.redstone.itemID, rand,
					func_82188_j(0.4F));
			EntityVillager.addBlacksmithItem(var2, Block.glowStone.blockID,
					rand, func_82188_j(0.3F));
			final int[] var3 = new int[] { Item.swordIron.itemID,
					Item.swordDiamond.itemID, Item.plateIron.itemID,
					Item.plateDiamond.itemID, Item.axeIron.itemID,
					Item.axeDiamond.itemID, Item.pickaxeIron.itemID,
					Item.pickaxeDiamond.itemID };
			final int[] var4 = var3;
			final int var5 = var3.length;
			var6 = 0;

			while (true) {
				if (var6 >= var5) {
					break label50;
				}

				final int var7 = var4[var6];

				if (rand.nextFloat() < func_82188_j(0.05F)) {
					var2.add(new MerchantRecipe(
							new ItemStack(var7, 1, 0),
							new ItemStack(Item.emerald, 2 + rand.nextInt(3), 0),
							EnchantmentHelper.addRandomEnchantment(rand,
									new ItemStack(var7, 1, 0),
									5 + rand.nextInt(15))));
				}

				++var6;
			}

		case 3:
			EntityVillager.addMerchantItem(var2, Item.coal.itemID, rand,
					func_82188_j(0.7F));
			EntityVillager.addMerchantItem(var2, Item.ingotIron.itemID, rand,
					func_82188_j(0.5F));
			EntityVillager.addMerchantItem(var2, Item.ingotGold.itemID, rand,
					func_82188_j(0.5F));
			EntityVillager.addMerchantItem(var2, Item.diamond.itemID, rand,
					func_82188_j(0.5F));
			EntityVillager.addBlacksmithItem(var2, Item.swordIron.itemID, rand,
					func_82188_j(0.5F));
			EntityVillager.addBlacksmithItem(var2, Item.swordDiamond.itemID,
					rand, func_82188_j(0.5F));
			EntityVillager.addBlacksmithItem(var2, Item.axeIron.itemID, rand,
					func_82188_j(0.3F));
			EntityVillager.addBlacksmithItem(var2, Item.axeDiamond.itemID,
					rand, func_82188_j(0.3F));
			EntityVillager.addBlacksmithItem(var2, Item.pickaxeIron.itemID,
					rand, func_82188_j(0.5F));
			EntityVillager.addBlacksmithItem(var2, Item.pickaxeDiamond.itemID,
					rand, func_82188_j(0.5F));
			EntityVillager.addBlacksmithItem(var2, Item.shovelIron.itemID,
					rand, func_82188_j(0.2F));
			EntityVillager.addBlacksmithItem(var2, Item.shovelDiamond.itemID,
					rand, func_82188_j(0.2F));
			EntityVillager.addBlacksmithItem(var2, Item.hoeIron.itemID, rand,
					func_82188_j(0.2F));
			EntityVillager.addBlacksmithItem(var2, Item.hoeDiamond.itemID,
					rand, func_82188_j(0.2F));
			EntityVillager.addBlacksmithItem(var2, Item.bootsIron.itemID, rand,
					func_82188_j(0.2F));
			EntityVillager.addBlacksmithItem(var2, Item.bootsDiamond.itemID,
					rand, func_82188_j(0.2F));
			EntityVillager.addBlacksmithItem(var2, Item.helmetIron.itemID,
					rand, func_82188_j(0.2F));
			EntityVillager.addBlacksmithItem(var2, Item.helmetDiamond.itemID,
					rand, func_82188_j(0.2F));
			EntityVillager.addBlacksmithItem(var2, Item.plateIron.itemID, rand,
					func_82188_j(0.2F));
			EntityVillager.addBlacksmithItem(var2, Item.plateDiamond.itemID,
					rand, func_82188_j(0.2F));
			EntityVillager.addBlacksmithItem(var2, Item.legsIron.itemID, rand,
					func_82188_j(0.2F));
			EntityVillager.addBlacksmithItem(var2, Item.legsDiamond.itemID,
					rand, func_82188_j(0.2F));
			EntityVillager.addBlacksmithItem(var2, Item.bootsChain.itemID,
					rand, func_82188_j(0.1F));
			EntityVillager.addBlacksmithItem(var2, Item.helmetChain.itemID,
					rand, func_82188_j(0.1F));
			EntityVillager.addBlacksmithItem(var2, Item.plateChain.itemID,
					rand, func_82188_j(0.1F));
			EntityVillager.addBlacksmithItem(var2, Item.legsChain.itemID, rand,
					func_82188_j(0.1F));
			break;

		case 4:
			EntityVillager.addMerchantItem(var2, Item.coal.itemID, rand,
					func_82188_j(0.7F));
			EntityVillager.addMerchantItem(var2, Item.porkRaw.itemID, rand,
					func_82188_j(0.5F));
			EntityVillager.addMerchantItem(var2, Item.beefRaw.itemID, rand,
					func_82188_j(0.5F));
			EntityVillager.addBlacksmithItem(var2, Item.saddle.itemID, rand,
					func_82188_j(0.1F));
			EntityVillager.addBlacksmithItem(var2, Item.plateLeather.itemID,
					rand, func_82188_j(0.3F));
			EntityVillager.addBlacksmithItem(var2, Item.bootsLeather.itemID,
					rand, func_82188_j(0.3F));
			EntityVillager.addBlacksmithItem(var2, Item.helmetLeather.itemID,
					rand, func_82188_j(0.3F));
			EntityVillager.addBlacksmithItem(var2, Item.legsLeather.itemID,
					rand, func_82188_j(0.3F));
			EntityVillager.addBlacksmithItem(var2, Item.porkCooked.itemID,
					rand, func_82188_j(0.3F));
			EntityVillager.addBlacksmithItem(var2, Item.beefCooked.itemID,
					rand, func_82188_j(0.3F));
		}

		if (var2.isEmpty()) {
			EntityVillager.addMerchantItem(var2, Item.ingotGold.itemID, rand,
					1.0F);
		}

		Collections.shuffle(var2);

		if (buyingList == null) {
			buyingList = new MerchantRecipeList();
		}

		for (int var9 = 0; var9 < par1 && var9 < var2.size(); ++var9) {
			buyingList.addToListWithCheck((MerchantRecipe) var2.get(var9));
		}
	}

	@Override
	public void setRecipes(final MerchantRecipeList par1MerchantRecipeList) {
	}

	/**
	 * each recipie takes a random stack from villagerStockList and offers it
	 * for 1 emerald
	 */
	private static void addMerchantItem(
			final MerchantRecipeList par0MerchantRecipeList, final int par1,
			final Random par2Random, final float par3) {
		if (par2Random.nextFloat() < par3) {
			par0MerchantRecipeList.add(new MerchantRecipe(EntityVillager
					.getRandomSizedStack(par1, par2Random), Item.emerald));
		}
	}

	private static ItemStack getRandomSizedStack(final int par0,
			final Random par1Random) {
		return new ItemStack(par0, EntityVillager.getRandomCountForItem(par0,
				par1Random), 0);
	}

	/**
	 * default to 1, and villagerStockList contains a min/max amount for each
	 * index
	 */
	private static int getRandomCountForItem(final int par0,
			final Random par1Random) {
		final Tuple var2 = (Tuple) EntityVillager.villagerStockList.get(Integer
				.valueOf(par0));
		return var2 == null ? 1
				: ((Integer) var2.getFirst()).intValue() >= ((Integer) var2
						.getSecond()).intValue() ? ((Integer) var2.getFirst())
						.intValue() : ((Integer) var2.getFirst()).intValue()
						+ par1Random.nextInt(((Integer) var2.getSecond())
								.intValue()
								- ((Integer) var2.getFirst()).intValue());
	}

	private static void addBlacksmithItem(
			final MerchantRecipeList par0MerchantRecipeList, final int par1,
			final Random par2Random, final float par3) {
		if (par2Random.nextFloat() < par3) {
			final int var4 = EntityVillager.getRandomCountForBlacksmithItem(
					par1, par2Random);
			ItemStack var5;
			ItemStack var6;

			if (var4 < 0) {
				var5 = new ItemStack(Item.emerald.itemID, 1, 0);
				var6 = new ItemStack(par1, -var4, 0);
			} else {
				var5 = new ItemStack(Item.emerald.itemID, var4, 0);
				var6 = new ItemStack(par1, 1, 0);
			}

			par0MerchantRecipeList.add(new MerchantRecipe(var5, var6));
		}
	}

	private static int getRandomCountForBlacksmithItem(final int par0,
			final Random par1Random) {
		final Tuple var2 = (Tuple) EntityVillager.blacksmithSellingList
				.get(Integer.valueOf(par0));
		return var2 == null ? 1
				: ((Integer) var2.getFirst()).intValue() >= ((Integer) var2
						.getSecond()).intValue() ? ((Integer) var2.getFirst())
						.intValue() : ((Integer) var2.getFirst()).intValue()
						+ par1Random.nextInt(((Integer) var2.getSecond())
								.intValue()
								- ((Integer) var2.getFirst()).intValue());
	}

	@Override
	public void handleHealthUpdate(final byte par1) {
		if (par1 == 12) {
			generateRandomParticles("heart");
		} else if (par1 == 13) {
			generateRandomParticles("angryVillager");
		} else if (par1 == 14) {
			generateRandomParticles("happyVillager");
		} else {
			super.handleHealthUpdate(par1);
		}
	}

	/**
	 * par1 is the particleName
	 */
	private void generateRandomParticles(final String par1Str) {
		for (int var2 = 0; var2 < 5; ++var2) {
			final double var3 = rand.nextGaussian() * 0.02D;
			final double var5 = rand.nextGaussian() * 0.02D;
			final double var7 = rand.nextGaussian() * 0.02D;
			worldObj.spawnParticle(par1Str, posX + rand.nextFloat() * width
					* 2.0F - width, posY + 1.0D + rand.nextFloat() * height,
					posZ + rand.nextFloat() * width * 2.0F - width, var3, var5,
					var7);
		}
	}

	/**
	 * Initialize this creature.
	 */
	@Override
	public void initCreature() {
		setProfession(worldObj.rand.nextInt(5));
	}

	public void func_82187_q() {
		field_82190_bM = true;
	}

	public EntityVillager func_90012_b(final EntityAgeable par1EntityAgeable) {
		final EntityVillager var2 = new EntityVillager(worldObj);
		var2.initCreature();
		return var2;
	}

	@Override
	public EntityAgeable createChild(final EntityAgeable par1EntityAgeable) {
		return func_90012_b(par1EntityAgeable);
	}

	static {
		EntityVillager.villagerStockList.put(Integer.valueOf(Item.coal.itemID),
				new Tuple(Integer.valueOf(16), Integer.valueOf(24)));
		EntityVillager.villagerStockList.put(
				Integer.valueOf(Item.ingotIron.itemID),
				new Tuple(Integer.valueOf(8), Integer.valueOf(10)));
		EntityVillager.villagerStockList.put(
				Integer.valueOf(Item.ingotGold.itemID),
				new Tuple(Integer.valueOf(8), Integer.valueOf(10)));
		EntityVillager.villagerStockList.put(
				Integer.valueOf(Item.diamond.itemID),
				new Tuple(Integer.valueOf(4), Integer.valueOf(6)));
		EntityVillager.villagerStockList.put(
				Integer.valueOf(Item.paper.itemID),
				new Tuple(Integer.valueOf(24), Integer.valueOf(36)));
		EntityVillager.villagerStockList.put(Integer.valueOf(Item.book.itemID),
				new Tuple(Integer.valueOf(11), Integer.valueOf(13)));
		EntityVillager.villagerStockList.put(
				Integer.valueOf(Item.writtenBook.itemID),
				new Tuple(Integer.valueOf(1), Integer.valueOf(1)));
		EntityVillager.villagerStockList.put(
				Integer.valueOf(Item.enderPearl.itemID),
				new Tuple(Integer.valueOf(3), Integer.valueOf(4)));
		EntityVillager.villagerStockList.put(
				Integer.valueOf(Item.eyeOfEnder.itemID),
				new Tuple(Integer.valueOf(2), Integer.valueOf(3)));
		EntityVillager.villagerStockList.put(
				Integer.valueOf(Item.porkRaw.itemID),
				new Tuple(Integer.valueOf(14), Integer.valueOf(18)));
		EntityVillager.villagerStockList.put(
				Integer.valueOf(Item.beefRaw.itemID),
				new Tuple(Integer.valueOf(14), Integer.valueOf(18)));
		EntityVillager.villagerStockList.put(
				Integer.valueOf(Item.chickenRaw.itemID),
				new Tuple(Integer.valueOf(14), Integer.valueOf(18)));
		EntityVillager.villagerStockList.put(
				Integer.valueOf(Item.fishCooked.itemID),
				new Tuple(Integer.valueOf(9), Integer.valueOf(13)));
		EntityVillager.villagerStockList.put(
				Integer.valueOf(Item.seeds.itemID),
				new Tuple(Integer.valueOf(34), Integer.valueOf(48)));
		EntityVillager.villagerStockList.put(
				Integer.valueOf(Item.melonSeeds.itemID),
				new Tuple(Integer.valueOf(30), Integer.valueOf(38)));
		EntityVillager.villagerStockList.put(
				Integer.valueOf(Item.pumpkinSeeds.itemID),
				new Tuple(Integer.valueOf(30), Integer.valueOf(38)));
		EntityVillager.villagerStockList.put(
				Integer.valueOf(Item.wheat.itemID),
				new Tuple(Integer.valueOf(18), Integer.valueOf(22)));
		EntityVillager.villagerStockList.put(
				Integer.valueOf(Block.cloth.blockID),
				new Tuple(Integer.valueOf(14), Integer.valueOf(22)));
		EntityVillager.villagerStockList.put(
				Integer.valueOf(Item.rottenFlesh.itemID),
				new Tuple(Integer.valueOf(36), Integer.valueOf(64)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.flintAndSteel.itemID),
				new Tuple(Integer.valueOf(3), Integer.valueOf(4)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.shears.itemID),
				new Tuple(Integer.valueOf(3), Integer.valueOf(4)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.swordIron.itemID),
				new Tuple(Integer.valueOf(7), Integer.valueOf(11)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.swordDiamond.itemID),
				new Tuple(Integer.valueOf(12), Integer.valueOf(14)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.axeIron.itemID),
				new Tuple(Integer.valueOf(6), Integer.valueOf(8)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.axeDiamond.itemID),
				new Tuple(Integer.valueOf(9), Integer.valueOf(12)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.pickaxeIron.itemID),
				new Tuple(Integer.valueOf(7), Integer.valueOf(9)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.pickaxeDiamond.itemID),
				new Tuple(Integer.valueOf(10), Integer.valueOf(12)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.shovelIron.itemID),
				new Tuple(Integer.valueOf(4), Integer.valueOf(6)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.shovelDiamond.itemID),
				new Tuple(Integer.valueOf(7), Integer.valueOf(8)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.hoeIron.itemID),
				new Tuple(Integer.valueOf(4), Integer.valueOf(6)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.hoeDiamond.itemID),
				new Tuple(Integer.valueOf(7), Integer.valueOf(8)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.bootsIron.itemID),
				new Tuple(Integer.valueOf(4), Integer.valueOf(6)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.bootsDiamond.itemID),
				new Tuple(Integer.valueOf(7), Integer.valueOf(8)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.helmetIron.itemID),
				new Tuple(Integer.valueOf(4), Integer.valueOf(6)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.helmetDiamond.itemID),
				new Tuple(Integer.valueOf(7), Integer.valueOf(8)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.plateIron.itemID),
				new Tuple(Integer.valueOf(10), Integer.valueOf(14)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.plateDiamond.itemID),
				new Tuple(Integer.valueOf(16), Integer.valueOf(19)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.legsIron.itemID),
				new Tuple(Integer.valueOf(8), Integer.valueOf(10)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.legsDiamond.itemID),
				new Tuple(Integer.valueOf(11), Integer.valueOf(14)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.bootsChain.itemID),
				new Tuple(Integer.valueOf(5), Integer.valueOf(7)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.helmetChain.itemID),
				new Tuple(Integer.valueOf(5), Integer.valueOf(7)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.plateChain.itemID),
				new Tuple(Integer.valueOf(11), Integer.valueOf(15)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.legsChain.itemID),
				new Tuple(Integer.valueOf(9), Integer.valueOf(11)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.bread.itemID),
				new Tuple(Integer.valueOf(-4), Integer.valueOf(-2)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.melon.itemID),
				new Tuple(Integer.valueOf(-8), Integer.valueOf(-4)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.appleRed.itemID),
				new Tuple(Integer.valueOf(-8), Integer.valueOf(-4)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.cookie.itemID),
				new Tuple(Integer.valueOf(-10), Integer.valueOf(-7)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Block.glass.blockID),
				new Tuple(Integer.valueOf(-5), Integer.valueOf(-3)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Block.bookShelf.blockID),
				new Tuple(Integer.valueOf(3), Integer.valueOf(4)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.plateLeather.itemID),
				new Tuple(Integer.valueOf(4), Integer.valueOf(5)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.bootsLeather.itemID),
				new Tuple(Integer.valueOf(2), Integer.valueOf(4)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.helmetLeather.itemID),
				new Tuple(Integer.valueOf(2), Integer.valueOf(4)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.legsLeather.itemID),
				new Tuple(Integer.valueOf(2), Integer.valueOf(4)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.saddle.itemID),
				new Tuple(Integer.valueOf(6), Integer.valueOf(8)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.expBottle.itemID),
				new Tuple(Integer.valueOf(-4), Integer.valueOf(-1)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.redstone.itemID),
				new Tuple(Integer.valueOf(-4), Integer.valueOf(-1)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.compass.itemID),
				new Tuple(Integer.valueOf(10), Integer.valueOf(12)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.pocketSundial.itemID),
				new Tuple(Integer.valueOf(10), Integer.valueOf(12)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Block.glowStone.blockID),
				new Tuple(Integer.valueOf(-3), Integer.valueOf(-1)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.porkCooked.itemID),
				new Tuple(Integer.valueOf(-7), Integer.valueOf(-5)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.beefCooked.itemID),
				new Tuple(Integer.valueOf(-7), Integer.valueOf(-5)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.chickenCooked.itemID),
				new Tuple(Integer.valueOf(-8), Integer.valueOf(-6)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.eyeOfEnder.itemID),
				new Tuple(Integer.valueOf(7), Integer.valueOf(11)));
		EntityVillager.blacksmithSellingList.put(
				Integer.valueOf(Item.arrow.itemID),
				new Tuple(Integer.valueOf(-12), Integer.valueOf(-8)));
	}
}
