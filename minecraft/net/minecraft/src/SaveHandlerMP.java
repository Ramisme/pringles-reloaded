package net.minecraft.src;

import java.io.File;

public class SaveHandlerMP implements ISaveHandler {
	/**
	 * Loads and returns the world info
	 */
	@Override
	public WorldInfo loadWorldInfo() {
		return null;
	}

	/**
	 * Checks the session lock to prevent save collisions
	 */
	@Override
	public void checkSessionLock() throws MinecraftException {
	}

	/**
	 * Returns the chunk loader with the provided world provider
	 */
	@Override
	public IChunkLoader getChunkLoader(final WorldProvider par1WorldProvider) {
		return null;
	}

	/**
	 * Saves the given World Info with the given NBTTagCompound as the Player.
	 */
	@Override
	public void saveWorldInfoWithPlayer(final WorldInfo par1WorldInfo,
			final NBTTagCompound par2NBTTagCompound) {
	}

	/**
	 * Saves the passed in world info.
	 */
	@Override
	public void saveWorldInfo(final WorldInfo par1WorldInfo) {
	}

	/**
	 * returns null if no saveHandler is relevent (eg. SMP)
	 */
	@Override
	public IPlayerFileData getSaveHandler() {
		return null;
	}

	/**
	 * Called to flush all changes to disk, waiting for them to complete.
	 */
	@Override
	public void flush() {
	}

	/**
	 * Gets the file location of the given map
	 */
	@Override
	public File getMapFileFromName(final String par1Str) {
		return null;
	}

	/**
	 * Returns the name of the directory where world information is saved.
	 */
	@Override
	public String getWorldDirectoryName() {
		return "none";
	}
}
