package net.minecraft.src;

import java.util.concurrent.Callable;

class CallableEntityName implements Callable {
	final Entity theEntity;

	CallableEntityName(final Entity par1Entity) {
		theEntity = par1Entity;
	}

	public String callEntityName() {
		return theEntity.getEntityName();
	}

	@Override
	public Object call() {
		return callEntityName();
	}
}
