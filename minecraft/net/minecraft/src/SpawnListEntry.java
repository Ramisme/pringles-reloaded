package net.minecraft.src;

public class SpawnListEntry extends WeightedRandomItem {
	/** Holds the class of the entity to be spawned. */
	public Class entityClass;
	public int minGroupCount;
	public int maxGroupCount;

	public SpawnListEntry(final Class par1Class, final int par2,
			final int par3, final int par4) {
		super(par2);
		entityClass = par1Class;
		minGroupCount = par3;
		maxGroupCount = par4;
	}
}
