package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet22Collect extends Packet {
	/** The entity on the ground that was picked up. */
	public int collectedEntityId;

	/** The entity that picked up the one from the ground. */
	public int collectorEntityId;

	public Packet22Collect() {
	}

	public Packet22Collect(final int par1, final int par2) {
		collectedEntityId = par1;
		collectorEntityId = par2;
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		collectedEntityId = par1DataInputStream.readInt();
		collectorEntityId = par1DataInputStream.readInt();
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		par1DataOutputStream.writeInt(collectedEntityId);
		par1DataOutputStream.writeInt(collectorEntityId);
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleCollect(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 8;
	}
}
