package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class Packet209SetPlayerTeam extends Packet {
	/** A unique name for the team. */
	public String teamName = "";

	/** Only if mode = 0 or 2. */
	public String teamDisplayName = "";

	/**
	 * Only if mode = 0 or 2. Displayed before the players' name that are part
	 * of this team.
	 */
	public String teamPrefix = "";

	/**
	 * Only if mode = 0 or 2. Displayed after the players' name that are part of
	 * this team.
	 */
	public String teamSuffix = "";

	/** Only if mode = 0 or 3 or 4. Players to be added/remove from the team. */
	public Collection playerNames = new ArrayList();

	/**
	 * If 0 then the team is created. If 1 then the team is removed. If 2 the
	 * team team information is updated. If 3 then new players are added to the
	 * team. If 4 then players are removed from the team.
	 */
	public int mode = 0;

	/** Only if mode = 0 or 2. */
	public int friendlyFire;

	public Packet209SetPlayerTeam() {
	}

	public Packet209SetPlayerTeam(final ScorePlayerTeam par1, final int par2) {
		teamName = par1.func_96661_b();
		mode = par2;

		if (par2 == 0 || par2 == 2) {
			teamDisplayName = par1.func_96669_c();
			teamPrefix = par1.func_96668_e();
			teamSuffix = par1.func_96663_f();
			friendlyFire = par1.func_98299_i();
		}

		if (par2 == 0) {
			playerNames.addAll(par1.getMembershipCollection());
		}
	}

	public Packet209SetPlayerTeam(final ScorePlayerTeam par1ScorePlayerTeam,
			final Collection par2Collection, final int par3) {
		if (par3 != 3 && par3 != 4) {
			throw new IllegalArgumentException(
					"Method must be join or leave for player constructor");
		} else if (par2Collection != null && !par2Collection.isEmpty()) {
			mode = par3;
			teamName = par1ScorePlayerTeam.func_96661_b();
			playerNames.addAll(par2Collection);
		} else {
			throw new IllegalArgumentException("Players cannot be null/empty");
		}
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		teamName = Packet.readString(par1DataInputStream, 16);
		mode = par1DataInputStream.readByte();

		if (mode == 0 || mode == 2) {
			teamDisplayName = Packet.readString(par1DataInputStream, 32);
			teamPrefix = Packet.readString(par1DataInputStream, 16);
			teamSuffix = Packet.readString(par1DataInputStream, 16);
			friendlyFire = par1DataInputStream.readByte();
		}

		if (mode == 0 || mode == 3 || mode == 4) {
			final short var2 = par1DataInputStream.readShort();

			for (int var3 = 0; var3 < var2; ++var3) {
				playerNames.add(Packet.readString(par1DataInputStream, 16));
			}
		}
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		Packet.writeString(teamName, par1DataOutputStream);
		par1DataOutputStream.writeByte(mode);

		if (mode == 0 || mode == 2) {
			Packet.writeString(teamDisplayName, par1DataOutputStream);
			Packet.writeString(teamPrefix, par1DataOutputStream);
			Packet.writeString(teamSuffix, par1DataOutputStream);
			par1DataOutputStream.writeByte(friendlyFire);
		}

		if (mode == 0 || mode == 3 || mode == 4) {
			par1DataOutputStream.writeShort(playerNames.size());
			final Iterator var2 = playerNames.iterator();

			while (var2.hasNext()) {
				final String var3 = (String) var2.next();
				Packet.writeString(var3, par1DataOutputStream);
			}
		}
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleSetPlayerTeam(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 3 + teamName.length();
	}
}
