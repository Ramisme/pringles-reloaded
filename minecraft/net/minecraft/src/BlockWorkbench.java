package net.minecraft.src;

public class BlockWorkbench extends Block {
	private Icon workbenchIconTop;
	private Icon workbenchIconFront;

	protected BlockWorkbench(final int par1) {
		super(par1, Material.wood);
		setCreativeTab(CreativeTabs.tabDecorations);
	}

	/**
	 * From the specified side and block metadata retrieves the blocks texture.
	 * Args: side, metadata
	 */
	@Override
	public Icon getIcon(final int par1, final int par2) {
		return par1 == 1 ? workbenchIconTop : par1 == 0 ? Block.planks
				.getBlockTextureFromSide(par1)
				: par1 != 2 && par1 != 4 ? blockIcon : workbenchIconFront;
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		blockIcon = par1IconRegister.registerIcon("workbench_side");
		workbenchIconTop = par1IconRegister.registerIcon("workbench_top");
		workbenchIconFront = par1IconRegister.registerIcon("workbench_front");
	}

	/**
	 * Called upon block activation (right click on the block.)
	 */
	@Override
	public boolean onBlockActivated(final World par1World, final int par2,
			final int par3, final int par4,
			final EntityPlayer par5EntityPlayer, final int par6,
			final float par7, final float par8, final float par9) {
		if (par1World.isRemote) {
			return true;
		} else {
			par5EntityPlayer.displayGUIWorkbench(par2, par3, par4);
			return true;
		}
	}
}
