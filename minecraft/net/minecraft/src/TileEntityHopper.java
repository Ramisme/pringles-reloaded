package net.minecraft.src;

import java.util.List;

public class TileEntityHopper extends TileEntity implements Hopper {
	private ItemStack[] hopperItemStacks = new ItemStack[5];

	/** The name that is displayed if the hopper was renamed */
	private String inventoryName;
	private int transferCooldown = -1;

	/**
	 * Reads a tile entity from NBT.
	 */
	@Override
	public void readFromNBT(final NBTTagCompound par1NBTTagCompound) {
		super.readFromNBT(par1NBTTagCompound);
		final NBTTagList var2 = par1NBTTagCompound.getTagList("Items");
		hopperItemStacks = new ItemStack[getSizeInventory()];

		if (par1NBTTagCompound.hasKey("CustomName")) {
			inventoryName = par1NBTTagCompound.getString("CustomName");
		}

		transferCooldown = par1NBTTagCompound.getInteger("TransferCooldown");

		for (int var3 = 0; var3 < var2.tagCount(); ++var3) {
			final NBTTagCompound var4 = (NBTTagCompound) var2.tagAt(var3);
			final byte var5 = var4.getByte("Slot");

			if (var5 >= 0 && var5 < hopperItemStacks.length) {
				hopperItemStacks[var5] = ItemStack.loadItemStackFromNBT(var4);
			}
		}
	}

	/**
	 * Writes a tile entity to NBT.
	 */
	@Override
	public void writeToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeToNBT(par1NBTTagCompound);
		final NBTTagList var2 = new NBTTagList();

		for (int var3 = 0; var3 < hopperItemStacks.length; ++var3) {
			if (hopperItemStacks[var3] != null) {
				final NBTTagCompound var4 = new NBTTagCompound();
				var4.setByte("Slot", (byte) var3);
				hopperItemStacks[var3].writeToNBT(var4);
				var2.appendTag(var4);
			}
		}

		par1NBTTagCompound.setTag("Items", var2);
		par1NBTTagCompound.setInteger("TransferCooldown", transferCooldown);

		if (isInvNameLocalized()) {
			par1NBTTagCompound.setString("CustomName", inventoryName);
		}
	}

	/**
	 * Called when an the contents of an Inventory change, usually
	 */
	@Override
	public void onInventoryChanged() {
		super.onInventoryChanged();
	}

	/**
	 * Returns the number of slots in the inventory.
	 */
	@Override
	public int getSizeInventory() {
		return hopperItemStacks.length;
	}

	/**
	 * Returns the stack in slot i
	 */
	@Override
	public ItemStack getStackInSlot(final int par1) {
		return hopperItemStacks[par1];
	}

	/**
	 * Removes from an inventory slot (first arg) up to a specified number
	 * (second arg) of items and returns them in a new stack.
	 */
	@Override
	public ItemStack decrStackSize(final int par1, final int par2) {
		if (hopperItemStacks[par1] != null) {
			ItemStack var3;

			if (hopperItemStacks[par1].stackSize <= par2) {
				var3 = hopperItemStacks[par1];
				hopperItemStacks[par1] = null;
				return var3;
			} else {
				var3 = hopperItemStacks[par1].splitStack(par2);

				if (hopperItemStacks[par1].stackSize == 0) {
					hopperItemStacks[par1] = null;
				}

				return var3;
			}
		} else {
			return null;
		}
	}

	/**
	 * When some containers are closed they call this on each slot, then drop
	 * whatever it returns as an EntityItem - like when you close a workbench
	 * GUI.
	 */
	@Override
	public ItemStack getStackInSlotOnClosing(final int par1) {
		if (hopperItemStacks[par1] != null) {
			final ItemStack var2 = hopperItemStacks[par1];
			hopperItemStacks[par1] = null;
			return var2;
		} else {
			return null;
		}
	}

	/**
	 * Sets the given item stack to the specified slot in the inventory (can be
	 * crafting or armor sections).
	 */
	@Override
	public void setInventorySlotContents(final int par1,
			final ItemStack par2ItemStack) {
		hopperItemStacks[par1] = par2ItemStack;

		if (par2ItemStack != null
				&& par2ItemStack.stackSize > getInventoryStackLimit()) {
			par2ItemStack.stackSize = getInventoryStackLimit();
		}
	}

	/**
	 * Returns the name of the inventory.
	 */
	@Override
	public String getInvName() {
		return isInvNameLocalized() ? inventoryName : "container.hopper";
	}

	/**
	 * If this returns false, the inventory name will be used as an unlocalized
	 * name, and translated into the player's language. Otherwise it will be
	 * used directly.
	 */
	@Override
	public boolean isInvNameLocalized() {
		return inventoryName != null && inventoryName.length() > 0;
	}

	public void setInventoryName(final String par1Str) {
		inventoryName = par1Str;
	}

	/**
	 * Returns the maximum stack size for a inventory slot. Seems to always be
	 * 64, possibly will be extended. *Isn't this more of a set than a get?*
	 */
	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	/**
	 * Do not make give this method the name canInteractWith because it clashes
	 * with Container
	 */
	@Override
	public boolean isUseableByPlayer(final EntityPlayer par1EntityPlayer) {
		return worldObj.getBlockTileEntity(xCoord, yCoord, zCoord) != this ? false
				: par1EntityPlayer.getDistanceSq(xCoord + 0.5D, yCoord + 0.5D,
						zCoord + 0.5D) <= 64.0D;
	}

	@Override
	public void openChest() {
	}

	@Override
	public void closeChest() {
	}

	/**
	 * Returns true if automation is allowed to insert the given stack (ignoring
	 * stack size) into the given slot.
	 */
	@Override
	public boolean isStackValidForSlot(final int par1,
			final ItemStack par2ItemStack) {
		return true;
	}

	/**
	 * Allows the entity to update its state. Overridden in most subclasses,
	 * e.g. the mob spawner uses this to count ticks and creates a new spawn
	 * inside its implementation.
	 */
	@Override
	public void updateEntity() {
		if (worldObj != null && !worldObj.isRemote) {
			--transferCooldown;

			if (!isCoolingDown()) {
				setTransferCooldown(0);
				func_98045_j();
			}
		}
	}

	public boolean func_98045_j() {
		if (worldObj != null && !worldObj.isRemote) {
			if (!isCoolingDown()
					&& BlockHopper
							.getIsBlockNotPoweredFromMetadata(getBlockMetadata())) {
				final boolean var1 = insertItemToInventory()
						| TileEntityHopper.suckItemsIntoHopper(this);

				if (var1) {
					setTransferCooldown(8);
					onInventoryChanged();
					return true;
				}
			}

			return false;
		} else {
			return false;
		}
	}

	/**
	 * Inserts one item from the hopper into the inventory the hopper is
	 * pointing at.
	 */
	private boolean insertItemToInventory() {
		final IInventory var1 = getOutputInventory();

		if (var1 == null) {
			return false;
		} else {
			for (int var2 = 0; var2 < getSizeInventory(); ++var2) {
				if (getStackInSlot(var2) != null) {
					final ItemStack var3 = getStackInSlot(var2).copy();
					final ItemStack var4 = TileEntityHopper
							.insertStack(
									var1,
									decrStackSize(var2, 1),
									Facing.oppositeSide[BlockHopper
											.getDirectionFromMetadata(getBlockMetadata())]);

					if (var4 == null || var4.stackSize == 0) {
						var1.onInventoryChanged();
						return true;
					}

					setInventorySlotContents(var2, var3);
				}
			}

			return false;
		}
	}

	/**
	 * Sucks one item into the given hopper from an inventory or EntityItem
	 * above it.
	 */
	public static boolean suckItemsIntoHopper(final Hopper par0Hopper) {
		final IInventory var1 = TileEntityHopper
				.getInventoryAboveHopper(par0Hopper);

		if (var1 != null) {
			final byte var2 = 0;

			if (var1 instanceof ISidedInventory && var2 > -1) {
				final ISidedInventory var7 = (ISidedInventory) var1;
				final int[] var8 = var7.getAccessibleSlotsFromSide(var2);

				for (final int element : var8) {
					if (TileEntityHopper.func_102012_a(par0Hopper, var1,
							element, var2)) {
						return true;
					}
				}
			} else {
				final int var3 = var1.getSizeInventory();

				for (int var4 = 0; var4 < var3; ++var4) {
					if (TileEntityHopper.func_102012_a(par0Hopper, var1, var4,
							var2)) {
						return true;
					}
				}
			}
		} else {
			final EntityItem var6 = TileEntityHopper.func_96119_a(
					par0Hopper.getWorldObj(), par0Hopper.getXPos(),
					par0Hopper.getYPos() + 1.0D, par0Hopper.getZPos());

			if (var6 != null) {
				return TileEntityHopper.func_96114_a(par0Hopper, var6);
			}
		}

		return false;
	}

	private static boolean func_102012_a(final Hopper par0Hopper,
			final IInventory par1IInventory, final int par2, final int par3) {
		final ItemStack var4 = par1IInventory.getStackInSlot(par2);

		if (var4 != null
				&& TileEntityHopper.canExtractItemFromInventory(par1IInventory,
						var4, par2, par3)) {
			final ItemStack var5 = var4.copy();
			final ItemStack var6 = TileEntityHopper.insertStack(par0Hopper,
					par1IInventory.decrStackSize(par2, 1), -1);

			if (var6 == null || var6.stackSize == 0) {
				par1IInventory.onInventoryChanged();
				return true;
			}

			par1IInventory.setInventorySlotContents(par2, var5);
		}

		return false;
	}

	public static boolean func_96114_a(final IInventory par0IInventory,
			final EntityItem par1EntityItem) {
		boolean var2 = false;

		if (par1EntityItem == null) {
			return false;
		} else {
			final ItemStack var3 = par1EntityItem.getEntityItem().copy();
			final ItemStack var4 = TileEntityHopper.insertStack(par0IInventory,
					var3, -1);

			if (var4 != null && var4.stackSize != 0) {
				par1EntityItem.setEntityItemStack(var4);
			} else {
				var2 = true;
				par1EntityItem.setDead();
			}

			return var2;
		}
	}

	/**
	 * Inserts a stack into an inventory. Args: Inventory, stack, side. Returns
	 * leftover items.
	 */
	public static ItemStack insertStack(final IInventory par1IInventory,
			ItemStack par2ItemStack, final int par3) {
		if (par1IInventory instanceof ISidedInventory && par3 > -1) {
			final ISidedInventory var6 = (ISidedInventory) par1IInventory;
			final int[] var7 = var6.getAccessibleSlotsFromSide(par3);

			for (int var5 = 0; var5 < var7.length && par2ItemStack != null
					&& par2ItemStack.stackSize > 0; ++var5) {
				par2ItemStack = TileEntityHopper.func_102014_c(par1IInventory,
						par2ItemStack, var7[var5], par3);
			}
		} else {
			final int var3 = par1IInventory.getSizeInventory();

			for (int var4 = 0; var4 < var3 && par2ItemStack != null
					&& par2ItemStack.stackSize > 0; ++var4) {
				par2ItemStack = TileEntityHopper.func_102014_c(par1IInventory,
						par2ItemStack, var4, par3);
			}
		}

		if (par2ItemStack != null && par2ItemStack.stackSize == 0) {
			par2ItemStack = null;
		}

		return par2ItemStack;
	}

	private static boolean func_102015_a(final IInventory par0IInventory,
			final ItemStack par1ItemStack, final int par2, final int par3) {
		return !par0IInventory.isStackValidForSlot(par2, par1ItemStack) ? false
				: !(par0IInventory instanceof ISidedInventory)
						|| ((ISidedInventory) par0IInventory).canInsertItem(
								par2, par1ItemStack, par3);
	}

	private static boolean canExtractItemFromInventory(
			final IInventory par0IInventory, final ItemStack par1ItemStack,
			final int par2, final int par3) {
		return !(par0IInventory instanceof ISidedInventory)
				|| ((ISidedInventory) par0IInventory).canExtractItem(par2,
						par1ItemStack, par3);
	}

	private static ItemStack func_102014_c(final IInventory par0IInventory,
			ItemStack par1ItemStack, final int par2, final int par3) {
		final ItemStack var4 = par0IInventory.getStackInSlot(par2);

		if (TileEntityHopper.func_102015_a(par0IInventory, par1ItemStack, par2,
				par3)) {
			boolean var5 = false;

			if (var4 == null) {
				par0IInventory.setInventorySlotContents(par2, par1ItemStack);
				par1ItemStack = null;
				var5 = true;
			} else if (TileEntityHopper.areItemStacksEqualItem(var4,
					par1ItemStack)) {
				final int var6 = par1ItemStack.getMaxStackSize()
						- var4.stackSize;
				final int var7 = Math.min(par1ItemStack.stackSize, var6);
				par1ItemStack.stackSize -= var7;
				var4.stackSize += var7;
				var5 = var7 > 0;
			}

			if (var5) {
				if (par0IInventory instanceof TileEntityHopper) {
					((TileEntityHopper) par0IInventory).setTransferCooldown(8);
				}

				par0IInventory.onInventoryChanged();
			}
		}

		return par1ItemStack;
	}

	/**
	 * Gets the inventory the hopper is pointing at.
	 */
	private IInventory getOutputInventory() {
		final int var1 = BlockHopper
				.getDirectionFromMetadata(getBlockMetadata());
		return TileEntityHopper.getInventoryAtLocation(getWorldObj(), xCoord
				+ Facing.offsetsXForSide[var1], yCoord
				+ Facing.offsetsYForSide[var1], zCoord
				+ Facing.offsetsZForSide[var1]);
	}

	/**
	 * Looks for anything, that can hold items (like chests, furnaces, etc.) one
	 * block above the given hopper.
	 */
	public static IInventory getInventoryAboveHopper(final Hopper par0Hopper) {
		return TileEntityHopper.getInventoryAtLocation(
				par0Hopper.getWorldObj(), par0Hopper.getXPos(),
				par0Hopper.getYPos() + 1.0D, par0Hopper.getZPos());
	}

	public static EntityItem func_96119_a(final World par0World,
			final double par1, final double par3, final double par5) {
		final List var7 = par0World.selectEntitiesWithinAABB(
				EntityItem.class,
				AxisAlignedBB.getAABBPool().getAABB(par1, par3, par5,
						par1 + 1.0D, par3 + 1.0D, par5 + 1.0D),
				IEntitySelector.selectAnything);
		return var7.size() > 0 ? (EntityItem) var7.get(0) : null;
	}

	/**
	 * Gets an inventory at the given location to extract items into or take
	 * items from. Can find either a tile entity or regular entity implementing
	 * IInventory.
	 */
	public static IInventory getInventoryAtLocation(final World par0World,
			final double par1, final double par3, final double par5) {
		IInventory var7 = null;
		final int var8 = MathHelper.floor_double(par1);
		final int var9 = MathHelper.floor_double(par3);
		final int var10 = MathHelper.floor_double(par5);
		final TileEntity var11 = par0World
				.getBlockTileEntity(var8, var9, var10);

		if (var11 != null && var11 instanceof IInventory) {
			var7 = (IInventory) var11;

			if (var7 instanceof TileEntityChest) {
				final int var12 = par0World.getBlockId(var8, var9, var10);
				final Block var13 = Block.blocksList[var12];

				if (var13 instanceof BlockChest) {
					var7 = ((BlockChest) var13).getInventory(par0World, var8,
							var9, var10);
				}
			}
		}

		if (var7 == null) {
			final List var14 = par0World.getEntitiesWithinAABBExcludingEntity(
					(Entity) null,
					AxisAlignedBB.getAABBPool().getAABB(par1, par3, par5,
							par1 + 1.0D, par3 + 1.0D, par5 + 1.0D),
					IEntitySelector.selectInventories);

			if (var14 != null && var14.size() > 0) {
				var7 = (IInventory) var14.get(par0World.rand.nextInt(var14
						.size()));
			}
		}

		return var7;
	}

	private static boolean areItemStacksEqualItem(
			final ItemStack par1ItemStack, final ItemStack par2ItemStack) {
		return par1ItemStack.itemID != par2ItemStack.itemID ? false
				: par1ItemStack.getItemDamage() != par2ItemStack
						.getItemDamage() ? false
						: par1ItemStack.stackSize > par1ItemStack
								.getMaxStackSize() ? false : ItemStack
								.areItemStackTagsEqual(par1ItemStack,
										par2ItemStack);
	}

	/**
	 * Gets the world X position for this hopper entity.
	 */
	@Override
	public double getXPos() {
		return xCoord;
	}

	/**
	 * Gets the world Y position for this hopper entity.
	 */
	@Override
	public double getYPos() {
		return yCoord;
	}

	/**
	 * Gets the world Z position for this hopper entity.
	 */
	@Override
	public double getZPos() {
		return zCoord;
	}

	public void setTransferCooldown(final int par1) {
		transferCooldown = par1;
	}

	public boolean isCoolingDown() {
		return transferCooldown > 0;
	}
}
