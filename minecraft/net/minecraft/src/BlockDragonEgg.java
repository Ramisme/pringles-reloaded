package net.minecraft.src;

import java.util.Random;

public class BlockDragonEgg extends Block {
	public BlockDragonEgg(final int par1) {
		super(par1, Material.dragonEgg);
		setBlockBounds(0.0625F, 0.0F, 0.0625F, 0.9375F, 1.0F, 0.9375F);
	}

	/**
	 * Called whenever the block is added into the world. Args: world, x, y, z
	 */
	@Override
	public void onBlockAdded(final World par1World, final int par2,
			final int par3, final int par4) {
		par1World.scheduleBlockUpdate(par2, par3, par4, blockID,
				tickRate(par1World));
	}

	/**
	 * Lets the block know when one of its neighbor changes. Doesn't know which
	 * neighbor changed (coordinates passed are their own) Args: x, y, z,
	 * neighbor blockID
	 */
	@Override
	public void onNeighborBlockChange(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		par1World.scheduleBlockUpdate(par2, par3, par4, blockID,
				tickRate(par1World));
	}

	/**
	 * Ticks the block if it's been scheduled
	 */
	@Override
	public void updateTick(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		fallIfPossible(par1World, par2, par3, par4);
	}

	/**
	 * Checks if the dragon egg can fall down, and if so, makes it fall.
	 */
	private void fallIfPossible(final World par1World, final int par2,
			int par3, final int par4) {
		if (BlockSand.canFallBelow(par1World, par2, par3 - 1, par4)
				&& par3 >= 0) {
			final byte var5 = 32;

			if (!BlockSand.fallInstantly
					&& par1World.checkChunksExist(par2 - var5, par3 - var5,
							par4 - var5, par2 + var5, par3 + var5, par4 + var5)) {
				final EntityFallingSand var6 = new EntityFallingSand(par1World,
						par2 + 0.5F, par3 + 0.5F, par4 + 0.5F, blockID);
				par1World.spawnEntityInWorld(var6);
			} else {
				par1World.setBlockToAir(par2, par3, par4);

				while (BlockSand.canFallBelow(par1World, par2, par3 - 1, par4)
						&& par3 > 0) {
					--par3;
				}

				if (par3 > 0) {
					par1World.setBlock(par2, par3, par4, blockID, 0, 2);
				}
			}
		}
	}

	/**
	 * Called upon block activation (right click on the block.)
	 */
	@Override
	public boolean onBlockActivated(final World par1World, final int par2,
			final int par3, final int par4,
			final EntityPlayer par5EntityPlayer, final int par6,
			final float par7, final float par8, final float par9) {
		teleportNearby(par1World, par2, par3, par4);
		return true;
	}

	/**
	 * Called when the block is clicked by a player. Args: x, y, z, entityPlayer
	 */
	@Override
	public void onBlockClicked(final World par1World, final int par2,
			final int par3, final int par4, final EntityPlayer par5EntityPlayer) {
		teleportNearby(par1World, par2, par3, par4);
	}

	/**
	 * Teleports the dragon egg somewhere else in a 31x19x31 area centered on
	 * the egg.
	 */
	private void teleportNearby(final World par1World, final int par2,
			final int par3, final int par4) {
		if (par1World.getBlockId(par2, par3, par4) == blockID) {
			for (int var5 = 0; var5 < 1000; ++var5) {
				final int var6 = par2 + par1World.rand.nextInt(16)
						- par1World.rand.nextInt(16);
				final int var7 = par3 + par1World.rand.nextInt(8)
						- par1World.rand.nextInt(8);
				final int var8 = par4 + par1World.rand.nextInt(16)
						- par1World.rand.nextInt(16);

				if (par1World.getBlockId(var6, var7, var8) == 0) {
					if (!par1World.isRemote) {
						par1World
								.setBlock(var6, var7, var8, blockID, par1World
										.getBlockMetadata(par2, par3, par4), 2);
						par1World.setBlockToAir(par2, par3, par4);
					} else {
						final short var9 = 128;

						for (int var10 = 0; var10 < var9; ++var10) {
							final double var11 = par1World.rand.nextDouble();
							final float var13 = (par1World.rand.nextFloat() - 0.5F) * 0.2F;
							final float var14 = (par1World.rand.nextFloat() - 0.5F) * 0.2F;
							final float var15 = (par1World.rand.nextFloat() - 0.5F) * 0.2F;
							final double var16 = var6 + (par2 - var6) * var11
									+ (par1World.rand.nextDouble() - 0.5D)
									* 1.0D + 0.5D;
							final double var18 = var7 + (par3 - var7) * var11
									+ par1World.rand.nextDouble() * 1.0D - 0.5D;
							final double var20 = var8 + (par4 - var8) * var11
									+ (par1World.rand.nextDouble() - 0.5D)
									* 1.0D + 0.5D;
							par1World.spawnParticle("portal", var16, var18,
									var20, var13, var14, var15);
						}
					}

					return;
				}
			}
		}
	}

	/**
	 * How many world ticks before ticking
	 */
	@Override
	public int tickRate(final World par1World) {
		return 5;
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	/**
	 * Returns true if the given side of this block type should be rendered, if
	 * the adjacent block is at the given coordinates. Args: blockAccess, x, y,
	 * z, side
	 */
	@Override
	public boolean shouldSideBeRendered(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		return true;
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return 27;
	}

	/**
	 * only called by clickMiddleMouseButton , and passed to
	 * inventory.setCurrentItem (along with isCreative)
	 */
	@Override
	public int idPicked(final World par1World, final int par2, final int par3,
			final int par4) {
		return 0;
	}
}
