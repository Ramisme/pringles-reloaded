package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet3Chat extends Packet {
	/** Maximum number of characters allowed in chat string in each packet. */
	public static int maxChatLength = 119;

	/** The message being sent. */
	public String message;
	private boolean isServer;

	public Packet3Chat() {
		isServer = true;
	}

	public Packet3Chat(final String par1Str) {
		this(par1Str, true);
	}

	public Packet3Chat(String par1Str, final boolean par2) {
		isServer = true;

		if (par1Str.length() > Packet3Chat.maxChatLength) {
			par1Str = par1Str.substring(0, Packet3Chat.maxChatLength);
		}

		message = par1Str;
		isServer = par2;
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		message = Packet.readString(par1DataInputStream,
				Packet3Chat.maxChatLength);
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		Packet.writeString(message, par1DataOutputStream);
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleChat(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 2 + message.length() * 2;
	}

	/**
	 * Get whether this is a server
	 */
	public boolean getIsServer() {
		return isServer;
	}

	/**
	 * If this returns true, the packet may be processed on any thread;
	 * otherwise it is queued for the main thread to handle.
	 */
	@Override
	public boolean canProcessAsync() {
		return !message.startsWith("/");
	}
}
