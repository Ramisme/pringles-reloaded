package net.minecraft.src;

import java.util.Random;

public class EntitySheep extends EntityAnimal {
	private final InventoryCrafting field_90016_e = new InventoryCrafting(
			new ContainerSheep(this), 2, 1);

	/**
	 * Holds the RGB table of the sheep colors - in OpenGL glColor3f values -
	 * used to render the sheep colored fleece.
	 */
	public static final float[][] fleeceColorTable = new float[][] {
			{ 1.0F, 1.0F, 1.0F }, { 0.85F, 0.5F, 0.2F }, { 0.7F, 0.3F, 0.85F },
			{ 0.4F, 0.6F, 0.85F }, { 0.9F, 0.9F, 0.2F }, { 0.5F, 0.8F, 0.1F },
			{ 0.95F, 0.5F, 0.65F }, { 0.3F, 0.3F, 0.3F }, { 0.6F, 0.6F, 0.6F },
			{ 0.3F, 0.5F, 0.6F }, { 0.5F, 0.25F, 0.7F }, { 0.2F, 0.3F, 0.7F },
			{ 0.4F, 0.3F, 0.2F }, { 0.4F, 0.5F, 0.2F }, { 0.6F, 0.2F, 0.2F },
			{ 0.1F, 0.1F, 0.1F } };

	/**
	 * Used to control movement as well as wool regrowth. Set to 40 on
	 * handleHealthUpdate and counts down with each tick.
	 */
	private int sheepTimer;

	/** The eat grass AI task for this mob. */
	private final EntityAIEatGrass aiEatGrass = new EntityAIEatGrass(this);

	public EntitySheep(final World par1World) {
		super(par1World);
		texture = "/mob/sheep.png";
		setSize(0.9F, 1.3F);
		final float var2 = 0.23F;
		getNavigator().setAvoidsWater(true);
		tasks.addTask(0, new EntityAISwimming(this));
		tasks.addTask(1, new EntityAIPanic(this, 0.38F));
		tasks.addTask(2, new EntityAIMate(this, var2));
		tasks.addTask(3, new EntityAITempt(this, 0.25F, Item.wheat.itemID,
				false));
		tasks.addTask(4, new EntityAIFollowParent(this, 0.25F));
		tasks.addTask(5, aiEatGrass);
		tasks.addTask(6, new EntityAIWander(this, var2));
		tasks.addTask(7, new EntityAIWatchClosest(this, EntityPlayer.class,
				6.0F));
		tasks.addTask(8, new EntityAILookIdle(this));
		field_90016_e.setInventorySlotContents(0, new ItemStack(Item.dyePowder,
				1, 0));
		field_90016_e.setInventorySlotContents(1, new ItemStack(Item.dyePowder,
				1, 0));
	}

	/**
	 * Returns true if the newer Entity AI code should be run
	 */
	@Override
	protected boolean isAIEnabled() {
		return true;
	}

	@Override
	protected void updateAITasks() {
		sheepTimer = aiEatGrass.getEatGrassTick();
		super.updateAITasks();
	}

	/**
	 * Called frequently so the entity can update its state every tick as
	 * required. For example, zombies and skeletons use this to react to
	 * sunlight and start to burn.
	 */
	@Override
	public void onLivingUpdate() {
		if (worldObj.isRemote) {
			sheepTimer = Math.max(0, sheepTimer - 1);
		}

		super.onLivingUpdate();
	}

	@Override
	public int getMaxHealth() {
		return 8;
	}

	@Override
	protected void entityInit() {
		super.entityInit();
		dataWatcher.addObject(16, new Byte((byte) 0));
	}

	/**
	 * Drop 0-2 items of this living's type. @param par1 - Whether this entity
	 * has recently been hit by a player. @param par2 - Level of Looting used to
	 * kill this mob.
	 */
	@Override
	protected void dropFewItems(final boolean par1, final int par2) {
		if (!getSheared()) {
			entityDropItem(new ItemStack(Block.cloth.blockID, 1,
					getFleeceColor()), 0.0F);
		}
	}

	/**
	 * Returns the item ID for the item the mob drops on death.
	 */
	@Override
	protected int getDropItemId() {
		return Block.cloth.blockID;
	}

	@Override
	public void handleHealthUpdate(final byte par1) {
		if (par1 == 10) {
			sheepTimer = 40;
		} else {
			super.handleHealthUpdate(par1);
		}
	}

	public float func_70894_j(final float par1) {
		return sheepTimer <= 0 ? 0.0F
				: sheepTimer >= 4 && sheepTimer <= 36 ? 1.0F
						: sheepTimer < 4 ? (sheepTimer - par1) / 4.0F
								: -(sheepTimer - 40 - par1) / 4.0F;
	}

	public float func_70890_k(final float par1) {
		if (sheepTimer > 4 && sheepTimer <= 36) {
			final float var2 = (sheepTimer - 4 - par1) / 32.0F;
			return (float) Math.PI / 5F + (float) Math.PI * 7F / 100F
					* MathHelper.sin(var2 * 28.7F);
		} else {
			return sheepTimer > 0 ? (float) Math.PI / 5F : rotationPitch
					/ (180F / (float) Math.PI);
		}
	}

	/**
	 * Called when a player interacts with a mob. e.g. gets milk from a cow,
	 * gets into the saddle on a pig.
	 */
	@Override
	public boolean interact(final EntityPlayer par1EntityPlayer) {
		final ItemStack var2 = par1EntityPlayer.inventory.getCurrentItem();

		if (var2 != null && var2.itemID == Item.shears.itemID && !getSheared()
				&& !isChild()) {
			if (!worldObj.isRemote) {
				setSheared(true);
				final int var3 = 1 + rand.nextInt(3);

				for (int var4 = 0; var4 < var3; ++var4) {
					final EntityItem var5 = entityDropItem(new ItemStack(
							Block.cloth.blockID, 1, getFleeceColor()), 1.0F);
					var5.motionY += rand.nextFloat() * 0.05F;
					var5.motionX += (rand.nextFloat() - rand.nextFloat()) * 0.1F;
					var5.motionZ += (rand.nextFloat() - rand.nextFloat()) * 0.1F;
				}
			}

			var2.damageItem(1, par1EntityPlayer);
			playSound("mob.sheep.shear", 1.0F, 1.0F);
		}

		return super.interact(par1EntityPlayer);
	}

	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	@Override
	public void writeEntityToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeEntityToNBT(par1NBTTagCompound);
		par1NBTTagCompound.setBoolean("Sheared", getSheared());
		par1NBTTagCompound.setByte("Color", (byte) getFleeceColor());
	}

	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	@Override
	public void readEntityFromNBT(final NBTTagCompound par1NBTTagCompound) {
		super.readEntityFromNBT(par1NBTTagCompound);
		setSheared(par1NBTTagCompound.getBoolean("Sheared"));
		setFleeceColor(par1NBTTagCompound.getByte("Color"));
	}

	/**
	 * Returns the sound this mob makes while it's alive.
	 */
	@Override
	protected String getLivingSound() {
		return "mob.sheep.say";
	}

	/**
	 * Returns the sound this mob makes when it is hurt.
	 */
	@Override
	protected String getHurtSound() {
		return "mob.sheep.say";
	}

	/**
	 * Returns the sound this mob makes on death.
	 */
	@Override
	protected String getDeathSound() {
		return "mob.sheep.say";
	}

	/**
	 * Plays step sound at given x, y, z for the entity
	 */
	@Override
	protected void playStepSound(final int par1, final int par2,
			final int par3, final int par4) {
		playSound("mob.sheep.step", 0.15F, 1.0F);
	}

	public int getFleeceColor() {
		return dataWatcher.getWatchableObjectByte(16) & 15;
	}

	public void setFleeceColor(final int par1) {
		final byte var2 = dataWatcher.getWatchableObjectByte(16);
		dataWatcher.updateObject(16,
				Byte.valueOf((byte) (var2 & 240 | par1 & 15)));
	}

	/**
	 * returns true if a sheeps wool has been sheared
	 */
	public boolean getSheared() {
		return (dataWatcher.getWatchableObjectByte(16) & 16) != 0;
	}

	/**
	 * make a sheep sheared if set to true
	 */
	public void setSheared(final boolean par1) {
		final byte var2 = dataWatcher.getWatchableObjectByte(16);

		if (par1) {
			dataWatcher.updateObject(16, Byte.valueOf((byte) (var2 | 16)));
		} else {
			dataWatcher.updateObject(16, Byte.valueOf((byte) (var2 & -17)));
		}
	}

	/**
	 * This method is called when a sheep spawns in the world to select the
	 * color of sheep fleece.
	 */
	public static int getRandomFleeceColor(final Random par0Random) {
		final int var1 = par0Random.nextInt(100);
		return var1 < 5 ? 15 : var1 < 10 ? 7 : var1 < 15 ? 8 : var1 < 18 ? 12
				: par0Random.nextInt(500) == 0 ? 6 : 0;
	}

	public EntitySheep func_90015_b(final EntityAgeable par1EntityAgeable) {
		final EntitySheep var2 = (EntitySheep) par1EntityAgeable;
		final EntitySheep var3 = new EntitySheep(worldObj);
		final int var4 = func_90014_a(this, var2);
		var3.setFleeceColor(15 - var4);
		return var3;
	}

	/**
	 * This function applies the benefits of growing back wool and faster
	 * growing up to the acting entity. (This function is used in the
	 * AIEatGrass)
	 */
	@Override
	public void eatGrassBonus() {
		setSheared(false);

		if (isChild()) {
			int var1 = getGrowingAge() + 1200;

			if (var1 > 0) {
				var1 = 0;
			}

			setGrowingAge(var1);
		}
	}

	/**
	 * Initialize this creature.
	 */
	@Override
	public void initCreature() {
		setFleeceColor(EntitySheep.getRandomFleeceColor(worldObj.rand));
	}

	private int func_90014_a(final EntityAnimal par1EntityAnimal,
			final EntityAnimal par2EntityAnimal) {
		final int var3 = func_90013_b(par1EntityAnimal);
		final int var4 = func_90013_b(par2EntityAnimal);
		field_90016_e.getStackInSlot(0).setItemDamage(var3);
		field_90016_e.getStackInSlot(1).setItemDamage(var4);
		final ItemStack var5 = CraftingManager.getInstance()
				.findMatchingRecipe(field_90016_e,
						((EntitySheep) par1EntityAnimal).worldObj);
		int var6;

		if (var5 != null && var5.getItem().itemID == Item.dyePowder.itemID) {
			var6 = var5.getItemDamage();
		} else {
			var6 = worldObj.rand.nextBoolean() ? var3 : var4;
		}

		return var6;
	}

	private int func_90013_b(final EntityAnimal par1EntityAnimal) {
		return 15 - ((EntitySheep) par1EntityAnimal).getFleeceColor();
	}

	@Override
	public EntityAgeable createChild(final EntityAgeable par1EntityAgeable) {
		return func_90015_b(par1EntityAgeable);
	}
}
