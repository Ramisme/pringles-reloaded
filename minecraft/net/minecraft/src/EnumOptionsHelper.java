package net.minecraft.src;

class EnumOptionsHelper {
	static final int[] enumOptionsMappingHelperArray = new int[EnumOptions
			.values().length];

	static {
		try {
			EnumOptionsHelper.enumOptionsMappingHelperArray[EnumOptions.INVERT_MOUSE
					.ordinal()] = 1;
		} catch (final NoSuchFieldError var14) {
			;
		}

		try {
			EnumOptionsHelper.enumOptionsMappingHelperArray[EnumOptions.VIEW_BOBBING
					.ordinal()] = 2;
		} catch (final NoSuchFieldError var13) {
			;
		}

		try {
			EnumOptionsHelper.enumOptionsMappingHelperArray[EnumOptions.ANAGLYPH
					.ordinal()] = 3;
		} catch (final NoSuchFieldError var12) {
			;
		}

		try {
			EnumOptionsHelper.enumOptionsMappingHelperArray[EnumOptions.ADVANCED_OPENGL
					.ordinal()] = 4;
		} catch (final NoSuchFieldError var11) {
			;
		}

		try {
			EnumOptionsHelper.enumOptionsMappingHelperArray[EnumOptions.RENDER_CLOUDS
					.ordinal()] = 5;
		} catch (final NoSuchFieldError var10) {
			;
		}

		try {
			EnumOptionsHelper.enumOptionsMappingHelperArray[EnumOptions.CHAT_COLOR
					.ordinal()] = 6;
		} catch (final NoSuchFieldError var9) {
			;
		}

		try {
			EnumOptionsHelper.enumOptionsMappingHelperArray[EnumOptions.CHAT_LINKS
					.ordinal()] = 7;
		} catch (final NoSuchFieldError var8) {
			;
		}

		try {
			EnumOptionsHelper.enumOptionsMappingHelperArray[EnumOptions.CHAT_LINKS_PROMPT
					.ordinal()] = 8;
		} catch (final NoSuchFieldError var7) {
			;
		}

		try {
			EnumOptionsHelper.enumOptionsMappingHelperArray[EnumOptions.USE_SERVER_TEXTURES
					.ordinal()] = 9;
		} catch (final NoSuchFieldError var6) {
			;
		}

		try {
			EnumOptionsHelper.enumOptionsMappingHelperArray[EnumOptions.SNOOPER_ENABLED
					.ordinal()] = 10;
		} catch (final NoSuchFieldError var5) {
			;
		}

		try {
			EnumOptionsHelper.enumOptionsMappingHelperArray[EnumOptions.USE_FULLSCREEN
					.ordinal()] = 11;
		} catch (final NoSuchFieldError var4) {
			;
		}

		try {
			EnumOptionsHelper.enumOptionsMappingHelperArray[EnumOptions.ENABLE_VSYNC
					.ordinal()] = 12;
		} catch (final NoSuchFieldError var3) {
			;
		}

		try {
			EnumOptionsHelper.enumOptionsMappingHelperArray[EnumOptions.SHOW_CAPE
					.ordinal()] = 13;
		} catch (final NoSuchFieldError var2) {
			;
		}

		try {
			EnumOptionsHelper.enumOptionsMappingHelperArray[EnumOptions.TOUCHSCREEN
					.ordinal()] = 14;
		} catch (final NoSuchFieldError var1) {
			;
		}
	}
}
