package net.minecraft.src;

public class WorldProviderEnd extends WorldProvider {
	/**
	 * creates a new world chunk manager for WorldProvider
	 */
	@Override
	public void registerWorldChunkManager() {
		worldChunkMgr = new WorldChunkManagerHell(BiomeGenBase.sky, 0.5F, 0.0F);
		dimensionId = 1;
		hasNoSky = true;
	}

	/**
	 * Returns a new chunk provider which generates chunks for this world
	 */
	@Override
	public IChunkProvider createChunkGenerator() {
		return new ChunkProviderEnd(worldObj, worldObj.getSeed());
	}

	/**
	 * Calculates the angle of sun and moon in the sky relative to a specified
	 * time (usually worldTime)
	 */
	@Override
	public float calculateCelestialAngle(final long par1, final float par3) {
		return 0.0F;
	}

	/**
	 * Returns array with sunrise/sunset colors
	 */
	@Override
	public float[] calcSunriseSunsetColors(final float par1, final float par2) {
		return null;
	}

	/**
	 * Return Vec3D with biome specific fog color
	 */
	@Override
	public Vec3 getFogColor(final float par1, final float par2) {
		final int var3 = 10518688;
		float var4 = MathHelper.cos(par1 * (float) Math.PI * 2.0F) * 2.0F + 0.5F;

		if (var4 < 0.0F) {
			var4 = 0.0F;
		}

		if (var4 > 1.0F) {
			var4 = 1.0F;
		}

		float var5 = (var3 >> 16 & 255) / 255.0F;
		float var6 = (var3 >> 8 & 255) / 255.0F;
		float var7 = (var3 & 255) / 255.0F;
		var5 *= var4 * 0.0F + 0.15F;
		var6 *= var4 * 0.0F + 0.15F;
		var7 *= var4 * 0.0F + 0.15F;
		return worldObj.getWorldVec3Pool().getVecFromPool(var5, var6, var7);
	}

	@Override
	public boolean isSkyColored() {
		return false;
	}

	/**
	 * True if the player can respawn in this dimension (true = overworld, false
	 * = nether).
	 */
	@Override
	public boolean canRespawnHere() {
		return false;
	}

	/**
	 * Returns 'true' if in the "main surface world", but 'false' if in the
	 * Nether or End dimensions.
	 */
	@Override
	public boolean isSurfaceWorld() {
		return false;
	}

	/**
	 * the y level at which clouds are rendered.
	 */
	@Override
	public float getCloudHeight() {
		return 8.0F;
	}

	/**
	 * Will check if the x, z position specified is alright to be set as the map
	 * spawn point
	 */
	@Override
	public boolean canCoordinateBeSpawn(final int par1, final int par2) {
		final int var3 = worldObj.getFirstUncoveredBlock(par1, par2);
		return var3 == 0 ? false : Block.blocksList[var3].blockMaterial
				.blocksMovement();
	}

	/**
	 * Gets the hard-coded portal location to use when entering this dimension.
	 */
	@Override
	public ChunkCoordinates getEntrancePortalLocation() {
		return new ChunkCoordinates(100, 50, 0);
	}

	@Override
	public int getAverageGroundLevel() {
		return 50;
	}

	/**
	 * Returns true if the given X,Z coordinate should show environmental fog.
	 */
	@Override
	public boolean doesXZShowFog(final int par1, final int par2) {
		return true;
	}

	/**
	 * Returns the dimension's name, e.g. "The End", "Nether", or "Overworld".
	 */
	@Override
	public String getDimensionName() {
		return "The End";
	}
}
