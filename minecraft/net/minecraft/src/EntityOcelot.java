package net.minecraft.src;

public class EntityOcelot extends EntityTameable {
	/**
	 * The tempt AI task for this mob, used to prevent taming while it is
	 * fleeing.
	 */
	private EntityAITempt aiTempt;

	public EntityOcelot(final World par1World) {
		super(par1World);
		texture = "/mob/ozelot.png";
		setSize(0.6F, 0.8F);
		getNavigator().setAvoidsWater(true);
		tasks.addTask(1, new EntityAISwimming(this));
		tasks.addTask(2, aiSit);
		tasks.addTask(3, aiTempt = new EntityAITempt(this, 0.18F,
				Item.fishRaw.itemID, true));
		tasks.addTask(4, new EntityAIAvoidEntity(this, EntityPlayer.class,
				16.0F, 0.23F, 0.4F));
		tasks.addTask(5, new EntityAIFollowOwner(this, 0.3F, 10.0F, 5.0F));
		tasks.addTask(6, new EntityAIOcelotSit(this, 0.4F));
		tasks.addTask(7, new EntityAILeapAtTarget(this, 0.3F));
		tasks.addTask(8, new EntityAIOcelotAttack(this));
		tasks.addTask(9, new EntityAIMate(this, 0.23F));
		tasks.addTask(10, new EntityAIWander(this, 0.23F));
		tasks.addTask(11, new EntityAIWatchClosest(this, EntityPlayer.class,
				10.0F));
		targetTasks.addTask(1, new EntityAITargetNonTamed(this,
				EntityChicken.class, 14.0F, 750, false));
	}

	@Override
	protected void entityInit() {
		super.entityInit();
		dataWatcher.addObject(18, Byte.valueOf((byte) 0));
	}

	/**
	 * main AI tick function, replaces updateEntityActionState
	 */
	@Override
	public void updateAITick() {
		if (getMoveHelper().isUpdating()) {
			final float var1 = getMoveHelper().getSpeed();

			if (var1 == 0.18F) {
				setSneaking(true);
				setSprinting(false);
			} else if (var1 == 0.4F) {
				setSneaking(false);
				setSprinting(true);
			} else {
				setSneaking(false);
				setSprinting(false);
			}
		} else {
			setSneaking(false);
			setSprinting(false);
		}
	}

	/**
	 * Determines if an entity can be despawned, used on idle far away entities
	 */
	@Override
	protected boolean canDespawn() {
		return !isTamed();
	}

	/**
	 * Returns the texture's file path as a String.
	 */
	@Override
	public String getTexture() {
		switch (getTameSkin()) {
		case 0:
			return "/mob/ozelot.png";

		case 1:
			return "/mob/cat_black.png";

		case 2:
			return "/mob/cat_red.png";

		case 3:
			return "/mob/cat_siamese.png";

		default:
			return super.getTexture();
		}
	}

	/**
	 * Returns true if the newer Entity AI code should be run
	 */
	@Override
	public boolean isAIEnabled() {
		return true;
	}

	@Override
	public int getMaxHealth() {
		return 10;
	}

	/**
	 * Called when the mob is falling. Calculates and applies fall damage.
	 */
	@Override
	protected void fall(final float par1) {
	}

	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	@Override
	public void writeEntityToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeEntityToNBT(par1NBTTagCompound);
		par1NBTTagCompound.setInteger("CatType", getTameSkin());
	}

	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	@Override
	public void readEntityFromNBT(final NBTTagCompound par1NBTTagCompound) {
		super.readEntityFromNBT(par1NBTTagCompound);
		setTameSkin(par1NBTTagCompound.getInteger("CatType"));
	}

	/**
	 * Returns the sound this mob makes while it's alive.
	 */
	@Override
	protected String getLivingSound() {
		return isTamed() ? isInLove() ? "mob.cat.purr"
				: rand.nextInt(4) == 0 ? "mob.cat.purreow" : "mob.cat.meow"
				: "";
	}

	/**
	 * Returns the sound this mob makes when it is hurt.
	 */
	@Override
	protected String getHurtSound() {
		return "mob.cat.hitt";
	}

	/**
	 * Returns the sound this mob makes on death.
	 */
	@Override
	protected String getDeathSound() {
		return "mob.cat.hitt";
	}

	/**
	 * Returns the volume for the sounds this mob makes.
	 */
	@Override
	protected float getSoundVolume() {
		return 0.4F;
	}

	/**
	 * Returns the item ID for the item the mob drops on death.
	 */
	@Override
	protected int getDropItemId() {
		return Item.leather.itemID;
	}

	@Override
	public boolean attackEntityAsMob(final Entity par1Entity) {
		return par1Entity
				.attackEntityFrom(DamageSource.causeMobDamage(this), 3);
	}

	/**
	 * Called when the entity is attacked.
	 */
	@Override
	public boolean attackEntityFrom(final DamageSource par1DamageSource,
			final int par2) {
		if (isEntityInvulnerable()) {
			return false;
		} else {
			aiSit.setSitting(false);
			return super.attackEntityFrom(par1DamageSource, par2);
		}
	}

	/**
	 * Drop 0-2 items of this living's type. @param par1 - Whether this entity
	 * has recently been hit by a player. @param par2 - Level of Looting used to
	 * kill this mob.
	 */
	@Override
	protected void dropFewItems(final boolean par1, final int par2) {
	}

	/**
	 * Called when a player interacts with a mob. e.g. gets milk from a cow,
	 * gets into the saddle on a pig.
	 */
	@Override
	public boolean interact(final EntityPlayer par1EntityPlayer) {
		final ItemStack var2 = par1EntityPlayer.inventory.getCurrentItem();

		if (isTamed()) {
			if (par1EntityPlayer.username.equalsIgnoreCase(getOwnerName())
					&& !worldObj.isRemote && !isBreedingItem(var2)) {
				aiSit.setSitting(!isSitting());
			}
		} else if (aiTempt.func_75277_f() && var2 != null
				&& var2.itemID == Item.fishRaw.itemID
				&& par1EntityPlayer.getDistanceSqToEntity(this) < 9.0D) {
			if (!par1EntityPlayer.capabilities.isCreativeMode) {
				--var2.stackSize;
			}

			if (var2.stackSize <= 0) {
				par1EntityPlayer.inventory.setInventorySlotContents(
						par1EntityPlayer.inventory.currentItem,
						(ItemStack) null);
			}

			if (!worldObj.isRemote) {
				if (rand.nextInt(3) == 0) {
					setTamed(true);
					setTameSkin(1 + worldObj.rand.nextInt(3));
					setOwner(par1EntityPlayer.username);
					playTameEffect(true);
					aiSit.setSitting(true);
					worldObj.setEntityState(this, (byte) 7);
				} else {
					playTameEffect(false);
					worldObj.setEntityState(this, (byte) 6);
				}
			}

			return true;
		}

		return super.interact(par1EntityPlayer);
	}

	/**
	 * This function is used when two same-species animals in 'love mode' breed
	 * to generate the new baby animal.
	 */
	public EntityOcelot spawnBabyAnimal(final EntityAgeable par1EntityAgeable) {
		final EntityOcelot var2 = new EntityOcelot(worldObj);

		if (isTamed()) {
			var2.setOwner(getOwnerName());
			var2.setTamed(true);
			var2.setTameSkin(getTameSkin());
		}

		return var2;
	}

	/**
	 * Checks if the parameter is an item which this animal can be fed to breed
	 * it (wheat, carrots or seeds depending on the animal type)
	 */
	@Override
	public boolean isBreedingItem(final ItemStack par1ItemStack) {
		return par1ItemStack != null
				&& par1ItemStack.itemID == Item.fishRaw.itemID;
	}

	/**
	 * Returns true if the mob is currently able to mate with the specified mob.
	 */
	@Override
	public boolean canMateWith(final EntityAnimal par1EntityAnimal) {
		if (par1EntityAnimal == this) {
			return false;
		} else if (!isTamed()) {
			return false;
		} else if (!(par1EntityAnimal instanceof EntityOcelot)) {
			return false;
		} else {
			final EntityOcelot var2 = (EntityOcelot) par1EntityAnimal;
			return !var2.isTamed() ? false : isInLove() && var2.isInLove();
		}
	}

	public int getTameSkin() {
		return dataWatcher.getWatchableObjectByte(18);
	}

	public void setTameSkin(final int par1) {
		dataWatcher.updateObject(18, Byte.valueOf((byte) par1));
	}

	/**
	 * Checks if the entity's current position is a valid location to spawn this
	 * entity.
	 */
	@Override
	public boolean getCanSpawnHere() {
		if (worldObj.rand.nextInt(3) == 0) {
			return false;
		} else {
			if (worldObj.checkNoEntityCollision(boundingBox)
					&& worldObj.getCollidingBoundingBoxes(this, boundingBox)
							.isEmpty() && !worldObj.isAnyLiquid(boundingBox)) {
				final int var1 = MathHelper.floor_double(posX);
				final int var2 = MathHelper.floor_double(boundingBox.minY);
				final int var3 = MathHelper.floor_double(posZ);

				if (var2 < 63) {
					return false;
				}

				final int var4 = worldObj.getBlockId(var1, var2 - 1, var3);

				if (var4 == Block.grass.blockID || var4 == Block.leaves.blockID) {
					return true;
				}
			}

			return false;
		}
	}

	/**
	 * Gets the username of the entity.
	 */
	@Override
	public String getEntityName() {
		return func_94056_bM() ? func_94057_bL()
				: isTamed() ? "entity.Cat.name" : super.getEntityName();
	}

	/**
	 * Initialize this creature.
	 */
	@Override
	public void initCreature() {
		if (worldObj.rand.nextInt(7) == 0) {
			for (int var1 = 0; var1 < 2; ++var1) {
				final EntityOcelot var2 = new EntityOcelot(worldObj);
				var2.setLocationAndAngles(posX, posY, posZ, rotationYaw, 0.0F);
				var2.setGrowingAge(-24000);
				worldObj.spawnEntityInWorld(var2);
			}
		}
	}

	@Override
	public EntityAgeable createChild(final EntityAgeable par1EntityAgeable) {
		return spawnBabyAnimal(par1EntityAgeable);
	}
}
