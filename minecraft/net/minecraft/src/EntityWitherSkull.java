package net.minecraft.src;

public class EntityWitherSkull extends EntityFireball {
	public EntityWitherSkull(final World par1World) {
		super(par1World);
		setSize(0.3125F, 0.3125F);
	}

	public EntityWitherSkull(final World par1World,
			final EntityLiving par2EntityLiving, final double par3,
			final double par5, final double par7) {
		super(par1World, par2EntityLiving, par3, par5, par7);
		setSize(0.3125F, 0.3125F);
	}

	/**
	 * Return the motion factor for this projectile. The factor is multiplied by
	 * the original motion.
	 */
	@Override
	protected float getMotionFactor() {
		return isInvulnerable() ? 0.73F : super.getMotionFactor();
	}

	public EntityWitherSkull(final World par1World, final double par2,
			final double par4, final double par6, final double par8,
			final double par10, final double par12) {
		super(par1World, par2, par4, par6, par8, par10, par12);
		setSize(0.3125F, 0.3125F);
	}

	/**
	 * Returns true if the entity is on fire. Used by render to add the fire
	 * effect on rendering.
	 */
	@Override
	public boolean isBurning() {
		return false;
	}

	@Override
	public float func_82146_a(final Explosion par1Explosion,
			final World par2World, final int par3, final int par4,
			final int par5, final Block par6Block) {
		float var7 = super.func_82146_a(par1Explosion, par2World, par3, par4,
				par5, par6Block);

		if (isInvulnerable() && par6Block != Block.bedrock
				&& par6Block != Block.endPortal
				&& par6Block != Block.endPortalFrame) {
			var7 = Math.min(0.8F, var7);
		}

		return var7;
	}

	/**
	 * Called when this EntityFireball hits a block or entity.
	 */
	@Override
	protected void onImpact(final MovingObjectPosition par1MovingObjectPosition) {
		if (!worldObj.isRemote) {
			if (par1MovingObjectPosition.entityHit != null) {
				if (shootingEntity != null) {
					if (par1MovingObjectPosition.entityHit.attackEntityFrom(
							DamageSource.causeMobDamage(shootingEntity), 8)
							&& !par1MovingObjectPosition.entityHit
									.isEntityAlive()) {
						shootingEntity.heal(5);
					}
				} else {
					par1MovingObjectPosition.entityHit.attackEntityFrom(
							DamageSource.magic, 5);
				}

				if (par1MovingObjectPosition.entityHit instanceof EntityLiving) {
					byte var2 = 0;

					if (worldObj.difficultySetting > 1) {
						if (worldObj.difficultySetting == 2) {
							var2 = 10;
						} else if (worldObj.difficultySetting == 3) {
							var2 = 40;
						}
					}

					if (var2 > 0) {
						((EntityLiving) par1MovingObjectPosition.entityHit)
								.addPotionEffect(new PotionEffect(
										Potion.wither.id, 20 * var2, 1));
					}
				}
			}

			worldObj.newExplosion(this, posX, posY, posZ, 1.0F, false, worldObj
					.getGameRules().getGameRuleBooleanValue("mobGriefing"));
			setDead();
		}
	}

	/**
	 * Returns true if other Entities should be prevented from moving through
	 * this Entity.
	 */
	@Override
	public boolean canBeCollidedWith() {
		return false;
	}

	/**
	 * Called when the entity is attacked.
	 */
	@Override
	public boolean attackEntityFrom(final DamageSource par1DamageSource,
			final int par2) {
		return false;
	}

	@Override
	protected void entityInit() {
		dataWatcher.addObject(10, Byte.valueOf((byte) 0));
	}

	/**
	 * Return whether this skull comes from an invulnerable (aura) wither boss.
	 */
	public boolean isInvulnerable() {
		return dataWatcher.getWatchableObjectByte(10) == 1;
	}

	/**
	 * Set whether this skull comes from an invulnerable (aura) wither boss.
	 */
	public void setInvulnerable(final boolean par1) {
		dataWatcher.updateObject(10, Byte.valueOf((byte) (par1 ? 1 : 0)));
	}
}
