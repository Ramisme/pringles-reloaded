package net.minecraft.src;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import net.minecraft.server.MinecraftServer;

public class WorldServer extends World {
	private final MinecraftServer mcServer;
	private final EntityTracker theEntityTracker;
	private final PlayerManager thePlayerManager;
	private Set field_73064_N;

	/** All work to do in future ticks. */
	private TreeSet pendingTickListEntries;
	public ChunkProviderServer theChunkProviderServer;

	/** set by CommandServerSave{all,Off,On} */
	public boolean canNotSave;

	/** is false if there are no players */
	private boolean allPlayersSleeping;
	private int updateEntityTick = 0;
	private final Teleporter field_85177_Q;

	/**
	 * Double buffer of ServerBlockEventList[] for holding pending
	 * BlockEventData's
	 */
	private final ServerBlockEventList[] blockEventCache = new ServerBlockEventList[] {
			new ServerBlockEventList((ServerBlockEvent) null),
			new ServerBlockEventList((ServerBlockEvent) null) };

	/**
	 * The index into the blockEventCache; either 0, or 1, toggled in
	 * sendBlockEventPackets where all BlockEvent are applied locally and send
	 * to clients.
	 */
	private int blockEventCacheIndex = 0;
	private static final WeightedRandomChestContent[] bonusChestContent = new WeightedRandomChestContent[] {
			new WeightedRandomChestContent(Item.stick.itemID, 0, 1, 3, 10),
			new WeightedRandomChestContent(Block.planks.blockID, 0, 1, 3, 10),
			new WeightedRandomChestContent(Block.wood.blockID, 0, 1, 3, 10),
			new WeightedRandomChestContent(Item.axeStone.itemID, 0, 1, 1, 3),
			new WeightedRandomChestContent(Item.axeWood.itemID, 0, 1, 1, 5),
			new WeightedRandomChestContent(Item.pickaxeStone.itemID, 0, 1, 1, 3),
			new WeightedRandomChestContent(Item.pickaxeWood.itemID, 0, 1, 1, 5),
			new WeightedRandomChestContent(Item.appleRed.itemID, 0, 2, 3, 5),
			new WeightedRandomChestContent(Item.bread.itemID, 0, 2, 3, 3) };
	private final ArrayList field_94579_S = new ArrayList();

	/** An IntHashMap of entity IDs (integers) to their Entity objects. */
	private IntHashMap entityIdMap;

	public WorldServer(final MinecraftServer par1MinecraftServer,
			final ISaveHandler par2ISaveHandler, final String par3Str,
			final int par4, final WorldSettings par5WorldSettings,
			final Profiler par6Profiler, final ILogAgent par7ILogAgent) {
		super(par2ISaveHandler, par3Str, par5WorldSettings, WorldProvider
				.getProviderForDimension(par4), par6Profiler, par7ILogAgent);
		mcServer = par1MinecraftServer;
		theEntityTracker = new EntityTracker(this);
		thePlayerManager = new PlayerManager(this, par1MinecraftServer
				.getConfigurationManager().getViewDistance());

		if (entityIdMap == null) {
			entityIdMap = new IntHashMap();
		}

		if (field_73064_N == null) {
			field_73064_N = new HashSet();
		}

		if (pendingTickListEntries == null) {
			pendingTickListEntries = new TreeSet();
		}

		field_85177_Q = new Teleporter(this);
		worldScoreboard = new ServerScoreboard(par1MinecraftServer);
		ScoreboardSaveData var8 = (ScoreboardSaveData) mapStorage.loadData(
				ScoreboardSaveData.class, "scoreboard");

		if (var8 == null) {
			var8 = new ScoreboardSaveData();
			mapStorage.setData("scoreboard", var8);
		}

		var8.func_96499_a(worldScoreboard);
		((ServerScoreboard) worldScoreboard).func_96547_a(var8);
	}

	/**
	 * Runs a single tick for the world
	 */
	@Override
	public void tick() {
		super.tick();

		if (getWorldInfo().isHardcoreModeEnabled() && difficultySetting < 3) {
			difficultySetting = 3;
		}

		provider.worldChunkMgr.cleanupCache();

		if (areAllPlayersAsleep()) {
			final boolean var1 = false;

			if (spawnHostileMobs && difficultySetting >= 1) {
				;
			}

			if (!var1) {
				final long var2 = worldInfo.getWorldTime() + 24000L;
				worldInfo.setWorldTime(var2 - var2 % 24000L);
				wakeAllPlayers();
			}
		}

		theProfiler.startSection("mobSpawner");

		if (getGameRules().getGameRuleBooleanValue("doMobSpawning")) {
			SpawnerAnimals.findChunksForSpawning(this, spawnHostileMobs,
					spawnPeacefulMobs,
					worldInfo.getWorldTotalTime() % 400L == 0L);
		}

		theProfiler.endStartSection("chunkSource");
		chunkProvider.unloadQueuedChunks();
		final int var4 = calculateSkylightSubtracted(1.0F);

		if (var4 != skylightSubtracted) {
			skylightSubtracted = var4;
		}

		worldInfo.incrementTotalWorldTime(worldInfo.getWorldTotalTime() + 1L);
		worldInfo.setWorldTime(worldInfo.getWorldTime() + 1L);
		theProfiler.endStartSection("tickPending");
		tickUpdates(false);
		theProfiler.endStartSection("tickTiles");
		tickBlocksAndAmbiance();
		theProfiler.endStartSection("chunkMap");
		thePlayerManager.updatePlayerInstances();
		theProfiler.endStartSection("village");
		villageCollectionObj.tick();
		villageSiegeObj.tick();
		theProfiler.endStartSection("portalForcer");
		field_85177_Q.removeStalePortalLocations(getTotalWorldTime());
		theProfiler.endSection();
		sendAndApplyBlockEvents();
	}

	/**
	 * only spawns creatures allowed by the chunkProvider
	 */
	public SpawnListEntry spawnRandomCreature(
			final EnumCreatureType par1EnumCreatureType, final int par2,
			final int par3, final int par4) {
		final List var5 = getChunkProvider().getPossibleCreatures(
				par1EnumCreatureType, par2, par3, par4);
		return var5 != null && !var5.isEmpty() ? (SpawnListEntry) WeightedRandom
				.getRandomItem(rand, var5) : null;
	}

	/**
	 * Updates the flag that indicates whether or not all players in the world
	 * are sleeping.
	 */
	@Override
	public void updateAllPlayersSleepingFlag() {
		allPlayersSleeping = !playerEntities.isEmpty();
		final Iterator var1 = playerEntities.iterator();

		while (var1.hasNext()) {
			final EntityPlayer var2 = (EntityPlayer) var1.next();

			if (!var2.isPlayerSleeping()) {
				allPlayersSleeping = false;
				break;
			}
		}
	}

	protected void wakeAllPlayers() {
		allPlayersSleeping = false;
		final Iterator var1 = playerEntities.iterator();

		while (var1.hasNext()) {
			final EntityPlayer var2 = (EntityPlayer) var1.next();

			if (var2.isPlayerSleeping()) {
				var2.wakeUpPlayer(false, false, true);
			}
		}

		resetRainAndThunder();
	}

	private void resetRainAndThunder() {
		worldInfo.setRainTime(0);
		worldInfo.setRaining(false);
		worldInfo.setThunderTime(0);
		worldInfo.setThundering(false);
	}

	public boolean areAllPlayersAsleep() {
		if (allPlayersSleeping && !isRemote) {
			final Iterator var1 = playerEntities.iterator();
			EntityPlayer var2;

			do {
				if (!var1.hasNext()) {
					return true;
				}

				var2 = (EntityPlayer) var1.next();
			} while (var2.isPlayerFullyAsleep());

			return false;
		} else {
			return false;
		}
	}

	/**
	 * Sets a new spawn location by finding an uncovered block at a random (x,z)
	 * location in the chunk.
	 */
	@Override
	public void setSpawnLocation() {
		if (worldInfo.getSpawnY() <= 0) {
			worldInfo.setSpawnY(64);
		}

		int var1 = worldInfo.getSpawnX();
		int var2 = worldInfo.getSpawnZ();
		int var3 = 0;

		while (getFirstUncoveredBlock(var1, var2) == 0) {
			var1 += rand.nextInt(8) - rand.nextInt(8);
			var2 += rand.nextInt(8) - rand.nextInt(8);
			++var3;

			if (var3 == 10000) {
				break;
			}
		}

		worldInfo.setSpawnX(var1);
		worldInfo.setSpawnZ(var2);
	}

	/**
	 * plays random cave ambient sounds and runs updateTick on random blocks
	 * within each chunk in the vacinity of a player
	 */
	@Override
	protected void tickBlocksAndAmbiance() {
		super.tickBlocksAndAmbiance();
		final Iterator var3 = activeChunkSet.iterator();

		while (var3.hasNext()) {
			final ChunkCoordIntPair var4 = (ChunkCoordIntPair) var3.next();
			final int var5 = var4.chunkXPos * 16;
			final int var6 = var4.chunkZPos * 16;
			theProfiler.startSection("getChunk");
			final Chunk var7 = getChunkFromChunkCoords(var4.chunkXPos,
					var4.chunkZPos);
			moodSoundAndLightCheck(var5, var6, var7);
			theProfiler.endStartSection("tickChunk");
			var7.updateSkylight();
			theProfiler.endStartSection("thunder");
			int var8;
			int var9;
			int var10;
			int var11;

			if (rand.nextInt(100000) == 0 && isRaining() && isThundering()) {
				updateLCG = updateLCG * 3 + 1013904223;
				var8 = updateLCG >> 2;
				var9 = var5 + (var8 & 15);
				var10 = var6 + (var8 >> 8 & 15);
				var11 = getPrecipitationHeight(var9, var10);

				if (canLightningStrikeAt(var9, var11, var10)) {
					addWeatherEffect(new EntityLightningBolt(this, var9, var11,
							var10));
				}
			}

			theProfiler.endStartSection("iceandsnow");
			int var13;

			if (rand.nextInt(16) == 0) {
				updateLCG = updateLCG * 3 + 1013904223;
				var8 = updateLCG >> 2;
				var9 = var8 & 15;
				var10 = var8 >> 8 & 15;
				var11 = getPrecipitationHeight(var9 + var5, var10 + var6);

				if (isBlockFreezableNaturally(var9 + var5, var11 - 1, var10
						+ var6)) {
					this.setBlock(var9 + var5, var11 - 1, var10 + var6,
							Block.ice.blockID);
				}

				if (isRaining() && canSnowAt(var9 + var5, var11, var10 + var6)) {
					this.setBlock(var9 + var5, var11, var10 + var6,
							Block.snow.blockID);
				}

				if (isRaining()) {
					final BiomeGenBase var12 = getBiomeGenForCoords(
							var9 + var5, var10 + var6);

					if (var12.canSpawnLightningBolt()) {
						var13 = getBlockId(var9 + var5, var11 - 1, var10 + var6);

						if (var13 != 0) {
							Block.blocksList[var13].fillWithRain(this, var9
									+ var5, var11 - 1, var10 + var6);
						}
					}
				}
			}

			theProfiler.endStartSection("tickTiles");
			final ExtendedBlockStorage[] var19 = var7.getBlockStorageArray();
			var9 = var19.length;

			for (var10 = 0; var10 < var9; ++var10) {
				final ExtendedBlockStorage var21 = var19[var10];

				if (var21 != null && var21.getNeedsRandomTick()) {
					for (int var20 = 0; var20 < 3; ++var20) {
						updateLCG = updateLCG * 3 + 1013904223;
						var13 = updateLCG >> 2;
						final int var14 = var13 & 15;
						final int var15 = var13 >> 8 & 15;
						final int var16 = var13 >> 16 & 15;
						final int var17 = var21.getExtBlockID(var14, var16,
								var15);
						final Block var18 = Block.blocksList[var17];

						if (var18 != null && var18.getTickRandomly()) {
							var18.updateTick(this, var14 + var5,
									var16 + var21.getYLocation(), var15 + var6,
									rand);
						}
					}
				}
			}

			theProfiler.endSection();
		}
	}

	/**
	 * Returns true if the given block will receive a scheduled tick in the
	 * future. Args: X, Y, Z, blockID
	 */
	@Override
	public boolean isBlockTickScheduled(final int par1, final int par2,
			final int par3, final int par4) {
		final NextTickListEntry var5 = new NextTickListEntry(par1, par2, par3,
				par4);
		return field_94579_S.contains(var5);
	}

	/**
	 * Schedules a tick to a block with a delay (Most commonly the tick rate)
	 */
	@Override
	public void scheduleBlockUpdate(final int par1, final int par2,
			final int par3, final int par4, final int par5) {
		func_82740_a(par1, par2, par3, par4, par5, 0);
	}

	@Override
	public void func_82740_a(final int par1, final int par2, final int par3,
			final int par4, int par5, final int par6) {
		final NextTickListEntry var7 = new NextTickListEntry(par1, par2, par3,
				par4);
		final byte var8 = 0;

		if (scheduledUpdatesAreImmediate && par4 > 0) {
			if (Block.blocksList[par4].func_82506_l()) {
				if (checkChunksExist(var7.xCoord - var8, var7.yCoord - var8,
						var7.zCoord - var8, var7.xCoord + var8, var7.yCoord
								+ var8, var7.zCoord + var8)) {
					final int var9 = getBlockId(var7.xCoord, var7.yCoord,
							var7.zCoord);

					if (var9 == var7.blockID && var9 > 0) {
						Block.blocksList[var9].updateTick(this, var7.xCoord,
								var7.yCoord, var7.zCoord, rand);
					}
				}

				return;
			}

			par5 = 1;
		}

		if (checkChunksExist(par1 - var8, par2 - var8, par3 - var8,
				par1 + var8, par2 + var8, par3 + var8)) {
			if (par4 > 0) {
				var7.setScheduledTime(par5 + worldInfo.getWorldTotalTime());
				var7.func_82753_a(par6);
			}

			if (!field_73064_N.contains(var7)) {
				field_73064_N.add(var7);
				pendingTickListEntries.add(var7);
			}
		}
	}

	/**
	 * Schedules a block update from the saved information in a chunk. Called
	 * when the chunk is loaded.
	 */
	@Override
	public void scheduleBlockUpdateFromLoad(final int par1, final int par2,
			final int par3, final int par4, final int par5, final int par6) {
		final NextTickListEntry var7 = new NextTickListEntry(par1, par2, par3,
				par4);
		var7.func_82753_a(par6);

		if (par4 > 0) {
			var7.setScheduledTime(par5 + worldInfo.getWorldTotalTime());
		}

		if (!field_73064_N.contains(var7)) {
			field_73064_N.add(var7);
			pendingTickListEntries.add(var7);
		}
	}

	/**
	 * Updates (and cleans up) entities and tile entities
	 */
	@Override
	public void updateEntities() {
		if (playerEntities.isEmpty()) {
			if (updateEntityTick++ >= 1200) {
				return;
			}
		} else {
			resetUpdateEntityTick();
		}

		super.updateEntities();
	}

	/**
	 * Resets the updateEntityTick field to 0
	 */
	public void resetUpdateEntityTick() {
		updateEntityTick = 0;
	}

	/**
	 * Runs through the list of updates to run and ticks them
	 */
	@Override
	public boolean tickUpdates(final boolean par1) {
		int var2 = pendingTickListEntries.size();

		if (var2 != field_73064_N.size()) {
			throw new IllegalStateException("TickNextTick list out of synch");
		} else {
			if (var2 > 1000) {
				var2 = 1000;
			}

			theProfiler.startSection("cleaning");
			NextTickListEntry var4;

			for (int var3 = 0; var3 < var2; ++var3) {
				var4 = (NextTickListEntry) pendingTickListEntries.first();

				if (!par1 && var4.scheduledTime > worldInfo.getWorldTotalTime()) {
					break;
				}

				pendingTickListEntries.remove(var4);
				field_73064_N.remove(var4);
				field_94579_S.add(var4);
			}

			theProfiler.endSection();
			theProfiler.startSection("ticking");
			final Iterator var14 = field_94579_S.iterator();

			while (var14.hasNext()) {
				var4 = (NextTickListEntry) var14.next();
				var14.remove();
				final byte var5 = 0;

				if (checkChunksExist(var4.xCoord - var5, var4.yCoord - var5,
						var4.zCoord - var5, var4.xCoord + var5, var4.yCoord
								+ var5, var4.zCoord + var5)) {
					final int var6 = getBlockId(var4.xCoord, var4.yCoord,
							var4.zCoord);

					if (var6 > 0
							&& Block.isAssociatedBlockID(var6, var4.blockID)) {
						try {
							Block.blocksList[var6]
									.updateTick(this, var4.xCoord, var4.yCoord,
											var4.zCoord, rand);
						} catch (final Throwable var13) {
							final CrashReport var8 = CrashReport
									.makeCrashReport(var13,
											"Exception while ticking a block");
							final CrashReportCategory var9 = var8
									.makeCategory("Block being ticked");
							int var10;

							try {
								var10 = getBlockMetadata(var4.xCoord,
										var4.yCoord, var4.zCoord);
							} catch (final Throwable var12) {
								var10 = -1;
							}

							CrashReportCategory.func_85068_a(var9, var4.xCoord,
									var4.yCoord, var4.zCoord, var6, var10);
							throw new ReportedException(var8);
						}
					}
				} else {
					scheduleBlockUpdate(var4.xCoord, var4.yCoord, var4.zCoord,
							var4.blockID, 0);
				}
			}

			theProfiler.endSection();
			field_94579_S.clear();
			return !pendingTickListEntries.isEmpty();
		}
	}

	@Override
	public List getPendingBlockUpdates(final Chunk par1Chunk, final boolean par2) {
		ArrayList var3 = null;
		final ChunkCoordIntPair var4 = par1Chunk.getChunkCoordIntPair();
		final int var5 = (var4.chunkXPos << 4) - 2;
		final int var6 = var5 + 16 + 2;
		final int var7 = (var4.chunkZPos << 4) - 2;
		final int var8 = var7 + 16 + 2;

		for (int var9 = 0; var9 < 2; ++var9) {
			Iterator var10;

			if (var9 == 0) {
				var10 = pendingTickListEntries.iterator();
			} else {
				var10 = field_94579_S.iterator();

				if (!field_94579_S.isEmpty()) {
					System.out.println(field_94579_S.size());
				}
			}

			while (var10.hasNext()) {
				final NextTickListEntry var11 = (NextTickListEntry) var10
						.next();

				if (var11.xCoord >= var5 && var11.xCoord < var6
						&& var11.zCoord >= var7 && var11.zCoord < var8) {
					if (par2) {
						field_73064_N.remove(var11);
						var10.remove();
					}

					if (var3 == null) {
						var3 = new ArrayList();
					}

					var3.add(var11);
				}
			}
		}

		return var3;
	}

	/**
	 * Will update the entity in the world if the chunk the entity is in is
	 * currently loaded or its forced to update. Args: entity, forceUpdate
	 */
	@Override
	public void updateEntityWithOptionalForce(final Entity par1Entity,
			final boolean par2) {
		if (!mcServer.getCanSpawnAnimals()
				&& (par1Entity instanceof EntityAnimal || par1Entity instanceof EntityWaterMob)) {
			par1Entity.setDead();
		}

		if (!mcServer.getCanSpawnNPCs() && par1Entity instanceof INpc) {
			par1Entity.setDead();
		}

		if (!(par1Entity.riddenByEntity instanceof EntityPlayer)) {
			super.updateEntityWithOptionalForce(par1Entity, par2);
		}
	}

	/**
	 * direct call to super.updateEntityWithOptionalForce
	 */
	public void uncheckedUpdateEntity(final Entity par1Entity,
			final boolean par2) {
		super.updateEntityWithOptionalForce(par1Entity, par2);
	}

	/**
	 * Creates the chunk provider for this world. Called in the constructor.
	 * Retrieves provider from worldProvider?
	 */
	@Override
	protected IChunkProvider createChunkProvider() {
		final IChunkLoader var1 = saveHandler.getChunkLoader(provider);
		theChunkProviderServer = new ChunkProviderServer(this, var1,
				provider.createChunkGenerator());
		return theChunkProviderServer;
	}

	/**
	 * pars: min x,y,z , max x,y,z
	 */
	public List getAllTileEntityInBox(final int par1, final int par2,
			final int par3, final int par4, final int par5, final int par6) {
		final ArrayList var7 = new ArrayList();

		for (int var8 = 0; var8 < loadedTileEntityList.size(); ++var8) {
			final TileEntity var9 = (TileEntity) loadedTileEntityList.get(var8);

			if (var9.xCoord >= par1 && var9.yCoord >= par2
					&& var9.zCoord >= par3 && var9.xCoord < par4
					&& var9.yCoord < par5 && var9.zCoord < par6) {
				var7.add(var9);
			}
		}

		return var7;
	}

	/**
	 * Called when checking if a certain block can be mined or not. The 'spawn
	 * safe zone' check is located here.
	 */
	@Override
	public boolean canMineBlock(final EntityPlayer par1EntityPlayer,
			final int par2, final int par3, final int par4) {
		return !mcServer.func_96290_a(this, par2, par3, par4, par1EntityPlayer);
	}

	@Override
	protected void initialize(final WorldSettings par1WorldSettings) {
		if (entityIdMap == null) {
			entityIdMap = new IntHashMap();
		}

		if (field_73064_N == null) {
			field_73064_N = new HashSet();
		}

		if (pendingTickListEntries == null) {
			pendingTickListEntries = new TreeSet();
		}

		createSpawnPosition(par1WorldSettings);
		super.initialize(par1WorldSettings);
	}

	/**
	 * creates a spawn position at random within 256 blocks of 0,0
	 */
	protected void createSpawnPosition(final WorldSettings par1WorldSettings) {
		if (!provider.canRespawnHere()) {
			worldInfo.setSpawnPosition(0, provider.getAverageGroundLevel(), 0);
		} else {
			findingSpawnPoint = true;
			final WorldChunkManager var2 = provider.worldChunkMgr;
			final List var3 = var2.getBiomesToSpawnIn();
			final Random var4 = new Random(getSeed());
			final ChunkPosition var5 = var2.findBiomePosition(0, 0, 256, var3,
					var4);
			int var6 = 0;
			final int var7 = provider.getAverageGroundLevel();
			int var8 = 0;

			if (var5 != null) {
				var6 = var5.x;
				var8 = var5.z;
			} else {
				getWorldLogAgent().logWarning("Unable to find spawn biome");
			}

			int var9 = 0;

			while (!provider.canCoordinateBeSpawn(var6, var8)) {
				var6 += var4.nextInt(64) - var4.nextInt(64);
				var8 += var4.nextInt(64) - var4.nextInt(64);
				++var9;

				if (var9 == 1000) {
					break;
				}
			}

			worldInfo.setSpawnPosition(var6, var7, var8);
			findingSpawnPoint = false;

			if (par1WorldSettings.isBonusChestEnabled()) {
				createBonusChest();
			}
		}
	}

	/**
	 * Creates the bonus chest in the world.
	 */
	protected void createBonusChest() {
		final WorldGeneratorBonusChest var1 = new WorldGeneratorBonusChest(
				WorldServer.bonusChestContent, 10);

		for (int var2 = 0; var2 < 10; ++var2) {
			final int var3 = worldInfo.getSpawnX() + rand.nextInt(6)
					- rand.nextInt(6);
			final int var4 = worldInfo.getSpawnZ() + rand.nextInt(6)
					- rand.nextInt(6);
			final int var5 = getTopSolidOrLiquidBlock(var3, var4) + 1;

			if (var1.generate(this, rand, var3, var5, var4)) {
				break;
			}
		}
	}

	/**
	 * Gets the hard-coded portal location to use when entering this dimension.
	 */
	public ChunkCoordinates getEntrancePortalLocation() {
		return provider.getEntrancePortalLocation();
	}

	/**
	 * Saves all chunks to disk while updating progress bar.
	 */
	public void saveAllChunks(final boolean par1,
			final IProgressUpdate par2IProgressUpdate)
			throws MinecraftException {
		if (chunkProvider.canSave()) {
			if (par2IProgressUpdate != null) {
				par2IProgressUpdate.displayProgressMessage("Saving level");
			}

			saveLevel();

			if (par2IProgressUpdate != null) {
				par2IProgressUpdate
						.resetProgresAndWorkingMessage("Saving chunks");
			}

			chunkProvider.saveChunks(par1, par2IProgressUpdate);
		}
	}

	public void func_104140_m() {
		if (chunkProvider.canSave()) {
			chunkProvider.func_104112_b();
		}
	}

	/**
	 * Saves the chunks to disk.
	 */
	protected void saveLevel() throws MinecraftException {
		checkSessionLock();
		saveHandler.saveWorldInfoWithPlayer(worldInfo, mcServer
				.getConfigurationManager().getHostPlayerData());
		mapStorage.saveAllData();
	}

	/**
	 * Start the skin for this entity downloading, if necessary, and increment
	 * its reference counter
	 */
	@Override
	protected void obtainEntitySkin(final Entity par1Entity) {
		super.obtainEntitySkin(par1Entity);
		entityIdMap.addKey(par1Entity.entityId, par1Entity);
		final Entity[] var2 = par1Entity.getParts();

		if (var2 != null) {
			for (final Entity element : var2) {
				entityIdMap.addKey(element.entityId, element);
			}
		}
	}

	/**
	 * Decrement the reference counter for this entity's skin image data
	 */
	@Override
	protected void releaseEntitySkin(final Entity par1Entity) {
		super.releaseEntitySkin(par1Entity);
		entityIdMap.removeObject(par1Entity.entityId);
		final Entity[] var2 = par1Entity.getParts();

		if (var2 != null) {
			for (final Entity element : var2) {
				entityIdMap.removeObject(element.entityId);
			}
		}
	}

	/**
	 * Returns the Entity with the given ID, or null if it doesn't exist in this
	 * World.
	 */
	@Override
	public Entity getEntityByID(final int par1) {
		return (Entity) entityIdMap.lookup(par1);
	}

	/**
	 * adds a lightning bolt to the list of lightning bolts in this world.
	 */
	@Override
	public boolean addWeatherEffect(final Entity par1Entity) {
		if (super.addWeatherEffect(par1Entity)) {
			mcServer.getConfigurationManager().sendToAllNear(par1Entity.posX,
					par1Entity.posY, par1Entity.posZ, 512.0D,
					provider.dimensionId, new Packet71Weather(par1Entity));
			return true;
		} else {
			return false;
		}
	}

	/**
	 * sends a Packet 38 (Entity Status) to all tracked players of that entity
	 */
	@Override
	public void setEntityState(final Entity par1Entity, final byte par2) {
		final Packet38EntityStatus var3 = new Packet38EntityStatus(
				par1Entity.entityId, par2);
		getEntityTracker().sendPacketToAllAssociatedPlayers(par1Entity, var3);
	}

	/**
	 * returns a new explosion. Does initiation (at time of writing Explosion is
	 * not finished)
	 */
	@Override
	public Explosion newExplosion(final Entity par1Entity, final double par2,
			final double par4, final double par6, final float par8,
			final boolean par9, final boolean par10) {
		final Explosion var11 = new Explosion(this, par1Entity, par2, par4,
				par6, par8);
		var11.isFlaming = par9;
		var11.isSmoking = par10;
		var11.doExplosionA();
		var11.doExplosionB(false);

		if (!par10) {
			var11.affectedBlockPositions.clear();
		}

		final Iterator var12 = playerEntities.iterator();

		while (var12.hasNext()) {
			final EntityPlayer var13 = (EntityPlayer) var12.next();

			if (var13.getDistanceSq(par2, par4, par6) < 4096.0D) {
				((EntityPlayerMP) var13).playerNetServerHandler
						.sendPacketToPlayer(new Packet60Explosion(par2, par4,
								par6, par8, var11.affectedBlockPositions,
								(Vec3) var11.func_77277_b().get(var13)));
			}
		}

		return var11;
	}

	/**
	 * Adds a block event with the given Args to the blockEventCache. During the
	 * next tick(), the block specified will have its onBlockEvent handler
	 * called with the given parameters. Args: X,Y,Z, BlockID, EventID,
	 * EventParameter
	 */
	@Override
	public void addBlockEvent(final int par1, final int par2, final int par3,
			final int par4, final int par5, final int par6) {
		final BlockEventData var7 = new BlockEventData(par1, par2, par3, par4,
				par5, par6);
		final Iterator var8 = blockEventCache[blockEventCacheIndex].iterator();
		BlockEventData var9;

		do {
			if (!var8.hasNext()) {
				blockEventCache[blockEventCacheIndex].add(var7);
				return;
			}

			var9 = (BlockEventData) var8.next();
		} while (!var9.equals(var7));
	}

	/**
	 * Send and apply locally all pending BlockEvents to each player with 64m
	 * radius of the event.
	 */
	private void sendAndApplyBlockEvents() {
		while (!blockEventCache[blockEventCacheIndex].isEmpty()) {
			final int var1 = blockEventCacheIndex;
			blockEventCacheIndex ^= 1;
			final Iterator var2 = blockEventCache[var1].iterator();

			while (var2.hasNext()) {
				final BlockEventData var3 = (BlockEventData) var2.next();

				if (onBlockEventReceived(var3)) {
					mcServer.getConfigurationManager().sendToAllNear(
							var3.getX(),
							var3.getY(),
							var3.getZ(),
							64.0D,
							provider.dimensionId,
							new Packet54PlayNoteBlock(var3.getX(), var3.getY(),
									var3.getZ(), var3.getBlockID(), var3
											.getEventID(), var3
											.getEventParameter()));
				}
			}

			blockEventCache[var1].clear();
		}
	}

	/**
	 * Called to apply a pending BlockEvent to apply to the current world.
	 */
	private boolean onBlockEventReceived(final BlockEventData par1BlockEventData) {
		final int var2 = getBlockId(par1BlockEventData.getX(),
				par1BlockEventData.getY(), par1BlockEventData.getZ());
		return var2 == par1BlockEventData.getBlockID() ? Block.blocksList[var2]
				.onBlockEventReceived(this, par1BlockEventData.getX(),
						par1BlockEventData.getY(), par1BlockEventData.getZ(),
						par1BlockEventData.getEventID(),
						par1BlockEventData.getEventParameter()) : false;
	}

	/**
	 * Syncs all changes to disk and wait for completion.
	 */
	public void flush() {
		saveHandler.flush();
	}

	/**
	 * Updates all weather states.
	 */
	@Override
	protected void updateWeather() {
		final boolean var1 = isRaining();
		super.updateWeather();

		if (var1 != isRaining()) {
			if (var1) {
				mcServer.getConfigurationManager().sendPacketToAllPlayers(
						new Packet70GameEvent(2, 0));
			} else {
				mcServer.getConfigurationManager().sendPacketToAllPlayers(
						new Packet70GameEvent(1, 0));
			}
		}
	}

	/**
	 * Gets the MinecraftServer.
	 */
	public MinecraftServer getMinecraftServer() {
		return mcServer;
	}

	/**
	 * Gets the EntityTracker
	 */
	public EntityTracker getEntityTracker() {
		return theEntityTracker;
	}

	public PlayerManager getPlayerManager() {
		return thePlayerManager;
	}

	public Teleporter getDefaultTeleporter() {
		return field_85177_Q;
	}
}
