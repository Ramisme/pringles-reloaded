package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet30Entity extends Packet {
	/** The ID of this entity. */
	public int entityId;

	/** The X axis relative movement. */
	public byte xPosition;

	/** The Y axis relative movement. */
	public byte yPosition;

	/** The Z axis relative movement. */
	public byte zPosition;

	/** The X axis rotation. */
	public byte yaw;

	/** The Y axis rotation. */
	public byte pitch;

	/** Boolean set to true if the entity is rotating. */
	public boolean rotating = false;

	public Packet30Entity() {
	}

	public Packet30Entity(final int par1) {
		entityId = par1;
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		entityId = par1DataInputStream.readInt();
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		par1DataOutputStream.writeInt(entityId);
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleEntity(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 4;
	}

	@Override
	public String toString() {
		return "Entity_" + super.toString();
	}

	/**
	 * only false for the abstract Packet class, all real packets return true
	 */
	@Override
	public boolean isRealPacket() {
		return true;
	}

	/**
	 * eg return packet30entity.entityId == entityId; WARNING : will throw if
	 * you compare a packet to a different packet class
	 */
	@Override
	public boolean containsSameEntityIDAs(final Packet par1Packet) {
		final Packet30Entity var2 = (Packet30Entity) par1Packet;
		return var2.entityId == entityId;
	}
}
