package net.minecraft.src;

public class DerivedWorldInfo extends WorldInfo {
	/** Instance of WorldInfo. */
	private final WorldInfo theWorldInfo;

	public DerivedWorldInfo(final WorldInfo par1WorldInfo) {
		theWorldInfo = par1WorldInfo;
	}

	/**
	 * Gets the NBTTagCompound for the worldInfo
	 */
	@Override
	public NBTTagCompound getNBTTagCompound() {
		return theWorldInfo.getNBTTagCompound();
	}

	/**
	 * Creates a new NBTTagCompound for the world, with the given NBTTag as the
	 * "Player"
	 */
	@Override
	public NBTTagCompound cloneNBTCompound(
			final NBTTagCompound par1NBTTagCompound) {
		return theWorldInfo.cloneNBTCompound(par1NBTTagCompound);
	}

	/**
	 * Returns the seed of current world.
	 */
	@Override
	public long getSeed() {
		return theWorldInfo.getSeed();
	}

	/**
	 * Returns the x spawn position
	 */
	@Override
	public int getSpawnX() {
		return theWorldInfo.getSpawnX();
	}

	/**
	 * Return the Y axis spawning point of the player.
	 */
	@Override
	public int getSpawnY() {
		return theWorldInfo.getSpawnY();
	}

	/**
	 * Returns the z spawn position
	 */
	@Override
	public int getSpawnZ() {
		return theWorldInfo.getSpawnZ();
	}

	@Override
	public long getWorldTotalTime() {
		return theWorldInfo.getWorldTotalTime();
	}

	/**
	 * Get current world time
	 */
	@Override
	public long getWorldTime() {
		return theWorldInfo.getWorldTime();
	}

	@Override
	public long getSizeOnDisk() {
		return theWorldInfo.getSizeOnDisk();
	}

	/**
	 * Returns the player's NBTTagCompound to be loaded
	 */
	@Override
	public NBTTagCompound getPlayerNBTTagCompound() {
		return theWorldInfo.getPlayerNBTTagCompound();
	}

	@Override
	public int getDimension() {
		return theWorldInfo.getDimension();
	}

	/**
	 * Get current world name
	 */
	@Override
	public String getWorldName() {
		return theWorldInfo.getWorldName();
	}

	/**
	 * Returns the save version of this world
	 */
	@Override
	public int getSaveVersion() {
		return theWorldInfo.getSaveVersion();
	}

	/**
	 * Return the last time the player was in this world.
	 */
	@Override
	public long getLastTimePlayed() {
		return theWorldInfo.getLastTimePlayed();
	}

	/**
	 * Returns true if it is thundering, false otherwise.
	 */
	@Override
	public boolean isThundering() {
		return theWorldInfo.isThundering();
	}

	/**
	 * Returns the number of ticks until next thunderbolt.
	 */
	@Override
	public int getThunderTime() {
		return theWorldInfo.getThunderTime();
	}

	/**
	 * Returns true if it is raining, false otherwise.
	 */
	@Override
	public boolean isRaining() {
		return theWorldInfo.isRaining();
	}

	/**
	 * Return the number of ticks until rain.
	 */
	@Override
	public int getRainTime() {
		return theWorldInfo.getRainTime();
	}

	/**
	 * Gets the GameType.
	 */
	@Override
	public EnumGameType getGameType() {
		return theWorldInfo.getGameType();
	}

	/**
	 * Set the x spawn position to the passed in value
	 */
	@Override
	public void setSpawnX(final int par1) {
	}

	/**
	 * Sets the y spawn position
	 */
	@Override
	public void setSpawnY(final int par1) {
	}

	/**
	 * Set the z spawn position to the passed in value
	 */
	@Override
	public void setSpawnZ(final int par1) {
	}

	@Override
	public void incrementTotalWorldTime(final long par1) {
	}

	/**
	 * Set current world time
	 */
	@Override
	public void setWorldTime(final long par1) {
	}

	/**
	 * Sets the spawn zone position. Args: x, y, z
	 */
	@Override
	public void setSpawnPosition(final int par1, final int par2, final int par3) {
	}

	@Override
	public void setWorldName(final String par1Str) {
	}

	/**
	 * Sets the save version of the world
	 */
	@Override
	public void setSaveVersion(final int par1) {
	}

	/**
	 * Sets whether it is thundering or not.
	 */
	@Override
	public void setThundering(final boolean par1) {
	}

	/**
	 * Defines the number of ticks until next thunderbolt.
	 */
	@Override
	public void setThunderTime(final int par1) {
	}

	/**
	 * Sets whether it is raining or not.
	 */
	@Override
	public void setRaining(final boolean par1) {
	}

	/**
	 * Sets the number of ticks until rain.
	 */
	@Override
	public void setRainTime(final int par1) {
	}

	/**
	 * Get whether the map features (e.g. strongholds) generation is enabled or
	 * disabled.
	 */
	@Override
	public boolean isMapFeaturesEnabled() {
		return theWorldInfo.isMapFeaturesEnabled();
	}

	/**
	 * Returns true if hardcore mode is enabled, otherwise false
	 */
	@Override
	public boolean isHardcoreModeEnabled() {
		return theWorldInfo.isHardcoreModeEnabled();
	}

	@Override
	public WorldType getTerrainType() {
		return theWorldInfo.getTerrainType();
	}

	@Override
	public void setTerrainType(final WorldType par1WorldType) {
	}

	/**
	 * Returns true if commands are allowed on this World.
	 */
	@Override
	public boolean areCommandsAllowed() {
		return theWorldInfo.areCommandsAllowed();
	}

	/**
	 * Returns true if the World is initialized.
	 */
	@Override
	public boolean isInitialized() {
		return theWorldInfo.isInitialized();
	}

	/**
	 * Sets the initialization status of the World.
	 */
	@Override
	public void setServerInitialized(final boolean par1) {
	}

	/**
	 * Gets the GameRules class Instance.
	 */
	@Override
	public GameRules getGameRulesInstance() {
		return theWorldInfo.getGameRulesInstance();
	}
}
