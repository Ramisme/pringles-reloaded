package net.minecraft.src;

class InventoryRepair extends InventoryBasic {
	/** Container of this anvil's block. */
	final ContainerRepair theContainer;

	InventoryRepair(final ContainerRepair par1ContainerRepair,
			final String par2Str, final boolean par3, final int par4) {
		super(par2Str, par3, par4);
		theContainer = par1ContainerRepair;
	}

	/**
	 * Called when an the contents of an Inventory change, usually
	 */
	@Override
	public void onInventoryChanged() {
		super.onInventoryChanged();
		theContainer.onCraftMatrixChanged(this);
	}

	/**
	 * Returns true if automation is allowed to insert the given stack (ignoring
	 * stack size) into the given slot.
	 */
	@Override
	public boolean isStackValidForSlot(final int par1,
			final ItemStack par2ItemStack) {
		return true;
	}
}
