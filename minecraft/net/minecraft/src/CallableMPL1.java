package net.minecraft.src;

import java.util.concurrent.Callable;

class CallableMPL1 implements Callable {
	/** Reference to the WorldClient object. */
	final WorldClient theWorldClient;

	CallableMPL1(final WorldClient par1WorldClient) {
		theWorldClient = par1WorldClient;
	}

	/**
	 * Returns the size and contents of the entity list.
	 */
	public String getEntityCountAndList() {
		return WorldClient.getEntityList(theWorldClient).size() + " total; "
				+ WorldClient.getEntityList(theWorldClient).toString();
	}

	@Override
	public Object call() {
		return getEntityCountAndList();
	}
}
