package net.minecraft.src;

import java.util.List;
import java.util.Random;

public class BlockBrewingStand extends BlockContainer {
	private final Random rand = new Random();
	private Icon theIcon;

	public BlockBrewingStand(final int par1) {
		super(par1, Material.iron);
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return 25;
	}

	/**
	 * Returns a new instance of a block's tile entity class. Called on placing
	 * the block.
	 */
	@Override
	public TileEntity createNewTileEntity(final World par1World) {
		return new TileEntityBrewingStand();
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	/**
	 * Adds all intersecting collision boxes to a list. (Be sure to only add
	 * boxes to the list if they intersect the mask.) Parameters: World, X, Y,
	 * Z, mask, list, colliding entity
	 */
	@Override
	public void addCollisionBoxesToList(final World par1World, final int par2,
			final int par3, final int par4,
			final AxisAlignedBB par5AxisAlignedBB, final List par6List,
			final Entity par7Entity) {
		setBlockBounds(0.4375F, 0.0F, 0.4375F, 0.5625F, 0.875F, 0.5625F);
		super.addCollisionBoxesToList(par1World, par2, par3, par4,
				par5AxisAlignedBB, par6List, par7Entity);
		setBlockBoundsForItemRender();
		super.addCollisionBoxesToList(par1World, par2, par3, par4,
				par5AxisAlignedBB, par6List, par7Entity);
	}

	/**
	 * Sets the block's bounds for rendering it as an item
	 */
	@Override
	public void setBlockBoundsForItemRender() {
		setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.125F, 1.0F);
	}

	/**
	 * Called upon block activation (right click on the block.)
	 */
	@Override
	public boolean onBlockActivated(final World par1World, final int par2,
			final int par3, final int par4,
			final EntityPlayer par5EntityPlayer, final int par6,
			final float par7, final float par8, final float par9) {
		if (par1World.isRemote) {
			return true;
		} else {
			final TileEntityBrewingStand var10 = (TileEntityBrewingStand) par1World
					.getBlockTileEntity(par2, par3, par4);

			if (var10 != null) {
				par5EntityPlayer.displayGUIBrewingStand(var10);
			}

			return true;
		}
	}

	/**
	 * Called when the block is placed in the world.
	 */
	@Override
	public void onBlockPlacedBy(final World par1World, final int par2,
			final int par3, final int par4,
			final EntityLiving par5EntityLiving, final ItemStack par6ItemStack) {
		if (par6ItemStack.hasDisplayName()) {
			((TileEntityBrewingStand) par1World.getBlockTileEntity(par2, par3,
					par4)).func_94131_a(par6ItemStack.getDisplayName());
		}
	}

	/**
	 * A randomly called display update to be able to add particles or other
	 * items for display
	 */
	@Override
	public void randomDisplayTick(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		final double var6 = par2 + 0.4F + par5Random.nextFloat() * 0.2F;
		final double var8 = par3 + 0.7F + par5Random.nextFloat() * 0.3F;
		final double var10 = par4 + 0.4F + par5Random.nextFloat() * 0.2F;
		par1World.spawnParticle("smoke", var6, var8, var10, 0.0D, 0.0D, 0.0D);
	}

	/**
	 * ejects contained items into the world, and notifies neighbours of an
	 * update, as appropriate
	 */
	@Override
	public void breakBlock(final World par1World, final int par2,
			final int par3, final int par4, final int par5, final int par6) {
		final TileEntity var7 = par1World.getBlockTileEntity(par2, par3, par4);

		if (var7 instanceof TileEntityBrewingStand) {
			final TileEntityBrewingStand var8 = (TileEntityBrewingStand) var7;

			for (int var9 = 0; var9 < var8.getSizeInventory(); ++var9) {
				final ItemStack var10 = var8.getStackInSlot(var9);

				if (var10 != null) {
					final float var11 = rand.nextFloat() * 0.8F + 0.1F;
					final float var12 = rand.nextFloat() * 0.8F + 0.1F;
					final float var13 = rand.nextFloat() * 0.8F + 0.1F;

					while (var10.stackSize > 0) {
						int var14 = rand.nextInt(21) + 10;

						if (var14 > var10.stackSize) {
							var14 = var10.stackSize;
						}

						var10.stackSize -= var14;
						final EntityItem var15 = new EntityItem(par1World, par2
								+ var11, par3 + var12, par4 + var13,
								new ItemStack(var10.itemID, var14,
										var10.getItemDamage()));
						final float var16 = 0.05F;
						var15.motionX = (float) rand.nextGaussian() * var16;
						var15.motionY = (float) rand.nextGaussian() * var16
								+ 0.2F;
						var15.motionZ = (float) rand.nextGaussian() * var16;
						par1World.spawnEntityInWorld(var15);
					}
				}
			}
		}

		super.breakBlock(par1World, par2, par3, par4, par5, par6);
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return Item.brewingStand.itemID;
	}

	/**
	 * only called by clickMiddleMouseButton , and passed to
	 * inventory.setCurrentItem (along with isCreative)
	 */
	@Override
	public int idPicked(final World par1World, final int par2, final int par3,
			final int par4) {
		return Item.brewingStand.itemID;
	}

	/**
	 * If this returns true, then comparators facing away from this block will
	 * use the value from getComparatorInputOverride instead of the actual
	 * redstone signal strength.
	 */
	@Override
	public boolean hasComparatorInputOverride() {
		return true;
	}

	/**
	 * If hasComparatorInputOverride returns true, the return value from this is
	 * used instead of the redstone signal strength when this block inputs to a
	 * comparator.
	 */
	@Override
	public int getComparatorInputOverride(final World par1World,
			final int par2, final int par3, final int par4, final int par5) {
		return Container.calcRedstoneFromInventory((IInventory) par1World
				.getBlockTileEntity(par2, par3, par4));
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		super.registerIcons(par1IconRegister);
		theIcon = par1IconRegister.registerIcon("brewingStand_base");
	}

	public Icon getBrewingStandIcon() {
		return theIcon;
	}
}
