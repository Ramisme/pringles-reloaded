package net.minecraft.src;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class EntityAINearestAttackableTarget extends EntityAITarget {
	EntityLiving targetEntity;
	Class targetClass;
	int targetChance;
	private final IEntitySelector field_82643_g;

	/** Instance of EntityAINearestAttackableTargetSorter. */
	private final EntityAINearestAttackableTargetSorter theNearestAttackableTargetSorter;

	public EntityAINearestAttackableTarget(final EntityLiving par1EntityLiving,
			final Class par2Class, final float par3, final int par4,
			final boolean par5) {
		this(par1EntityLiving, par2Class, par3, par4, par5, false);
	}

	public EntityAINearestAttackableTarget(final EntityLiving par1EntityLiving,
			final Class par2Class, final float par3, final int par4,
			final boolean par5, final boolean par6) {
		this(par1EntityLiving, par2Class, par3, par4, par5, par6,
				(IEntitySelector) null);
	}

	public EntityAINearestAttackableTarget(final EntityLiving par1,
			final Class par2, final float par3, final int par4,
			final boolean par5, final boolean par6,
			final IEntitySelector par7IEntitySelector) {
		super(par1, par3, par5, par6);
		targetClass = par2;
		targetDistance = par3;
		targetChance = par4;
		theNearestAttackableTargetSorter = new EntityAINearestAttackableTargetSorter(
				this, par1);
		field_82643_g = par7IEntitySelector;
		setMutexBits(1);
	}

	/**
	 * Returns whether the EntityAIBase should begin execution.
	 */
	@Override
	public boolean shouldExecute() {
		if (targetChance > 0 && taskOwner.getRNG().nextInt(targetChance) != 0) {
			return false;
		} else {
			if (targetClass == EntityPlayer.class) {
				final EntityPlayer var1 = taskOwner.worldObj
						.getClosestVulnerablePlayerToEntity(taskOwner,
								targetDistance);

				if (isSuitableTarget(var1, false)) {
					targetEntity = var1;
					return true;
				}
			} else {
				final List var5 = taskOwner.worldObj.selectEntitiesWithinAABB(
						targetClass, taskOwner.boundingBox.expand(
								targetDistance, 4.0D, targetDistance),
						field_82643_g);
				Collections.sort(var5, theNearestAttackableTargetSorter);
				final Iterator var2 = var5.iterator();

				while (var2.hasNext()) {
					final Entity var3 = (Entity) var2.next();
					final EntityLiving var4 = (EntityLiving) var3;

					if (isSuitableTarget(var4, false)) {
						targetEntity = var4;
						return true;
					}
				}
			}

			return false;
		}
	}

	/**
	 * Execute a one shot task or start executing a continuous task
	 */
	@Override
	public void startExecuting() {
		taskOwner.setAttackTarget(targetEntity);
		super.startExecuting();
	}
}
