package net.minecraft.src;

import java.util.Random;

final class DispenserBehaviorFireball extends BehaviorDefaultDispenseItem {
	/**
	 * Dispense the specified stack, play the dispense sound and spawn
	 * particles.
	 */
	@Override
	public ItemStack dispenseStack(final IBlockSource par1IBlockSource,
			final ItemStack par2ItemStack) {
		final EnumFacing var3 = BlockDispenser.getFacing(par1IBlockSource
				.getBlockMetadata());
		final IPosition var4 = BlockDispenser
				.getIPositionFromBlockSource(par1IBlockSource);
		final double var5 = var4.getX() + var3.getFrontOffsetX() * 0.3F;
		final double var7 = var4.getY() + var3.getFrontOffsetX() * 0.3F;
		final double var9 = var4.getZ() + var3.getFrontOffsetZ() * 0.3F;
		final World var11 = par1IBlockSource.getWorld();
		final Random var12 = var11.rand;
		final double var13 = var12.nextGaussian() * 0.05D
				+ var3.getFrontOffsetX();
		final double var15 = var12.nextGaussian() * 0.05D
				+ var3.getFrontOffsetY();
		final double var17 = var12.nextGaussian() * 0.05D
				+ var3.getFrontOffsetZ();
		var11.spawnEntityInWorld(new EntitySmallFireball(var11, var5, var7,
				var9, var13, var15, var17));
		par2ItemStack.splitStack(1);
		return par2ItemStack;
	}

	/**
	 * Play the dispense sound from the specified block.
	 */
	@Override
	protected void playDispenseSound(final IBlockSource par1IBlockSource) {
		par1IBlockSource.getWorld().playAuxSFX(1009,
				par1IBlockSource.getXInt(), par1IBlockSource.getYInt(),
				par1IBlockSource.getZInt(), 0);
	}
}
