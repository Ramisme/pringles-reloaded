package net.minecraft.src;

import java.util.Random;

public class BlockMelon extends Block {
	private Icon theIcon;

	protected BlockMelon(final int par1) {
		super(par1, Material.pumpkin);
		setCreativeTab(CreativeTabs.tabBlock);
	}

	/**
	 * From the specified side and block metadata retrieves the blocks texture.
	 * Args: side, metadata
	 */
	@Override
	public Icon getIcon(final int par1, final int par2) {
		return par1 != 1 && par1 != 0 ? blockIcon : theIcon;
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return Item.melon.itemID;
	}

	/**
	 * Returns the quantity of items to drop on block destruction.
	 */
	@Override
	public int quantityDropped(final Random par1Random) {
		return 3 + par1Random.nextInt(5);
	}

	/**
	 * Returns the usual quantity dropped by the block plus a bonus of 1 to 'i'
	 * (inclusive).
	 */
	@Override
	public int quantityDroppedWithBonus(final int par1, final Random par2Random) {
		int var3 = quantityDropped(par2Random) + par2Random.nextInt(1 + par1);

		if (var3 > 9) {
			var3 = 9;
		}

		return var3;
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		blockIcon = par1IconRegister.registerIcon("melon_side");
		theIcon = par1IconRegister.registerIcon("melon_top");
	}
}
