package net.minecraft.src;

public class TileEntityEnderChest extends TileEntity {
	/** The current angle of the chest lid (between 0 and 1) */
	public float lidAngle;

	/** The angle of the chest lid last tick */
	public float prevLidAngle;

	/** The number of players currently using this ender chest. */
	public int numUsingPlayers;

	/** Server sync counter (once per 20 ticks) */
	private int ticksSinceSync;

	/**
	 * Allows the entity to update its state. Overridden in most subclasses,
	 * e.g. the mob spawner uses this to count ticks and creates a new spawn
	 * inside its implementation.
	 */
	@Override
	public void updateEntity() {
		super.updateEntity();

		if (++ticksSinceSync % 20 * 4 == 0) {
			worldObj.addBlockEvent(xCoord, yCoord, zCoord,
					Block.enderChest.blockID, 1, numUsingPlayers);
		}

		prevLidAngle = lidAngle;
		final float var1 = 0.1F;
		double var4;

		if (numUsingPlayers > 0 && lidAngle == 0.0F) {
			final double var2 = xCoord + 0.5D;
			var4 = zCoord + 0.5D;
			worldObj.playSoundEffect(var2, yCoord + 0.5D, var4,
					"random.chestopen", 0.5F,
					worldObj.rand.nextFloat() * 0.1F + 0.9F);
		}

		if (numUsingPlayers == 0 && lidAngle > 0.0F || numUsingPlayers > 0
				&& lidAngle < 1.0F) {
			final float var8 = lidAngle;

			if (numUsingPlayers > 0) {
				lidAngle += var1;
			} else {
				lidAngle -= var1;
			}

			if (lidAngle > 1.0F) {
				lidAngle = 1.0F;
			}

			final float var3 = 0.5F;

			if (lidAngle < var3 && var8 >= var3) {
				var4 = xCoord + 0.5D;
				final double var6 = zCoord + 0.5D;
				worldObj.playSoundEffect(var4, yCoord + 0.5D, var6,
						"random.chestclosed", 0.5F,
						worldObj.rand.nextFloat() * 0.1F + 0.9F);
			}

			if (lidAngle < 0.0F) {
				lidAngle = 0.0F;
			}
		}
	}

	/**
	 * Called when a client event is received with the event number and
	 * argument, see World.sendClientEvent
	 */
	@Override
	public boolean receiveClientEvent(final int par1, final int par2) {
		if (par1 == 1) {
			numUsingPlayers = par2;
			return true;
		} else {
			return super.receiveClientEvent(par1, par2);
		}
	}

	/**
	 * invalidates a tile entity
	 */
	@Override
	public void invalidate() {
		updateContainingBlockInfo();
		super.invalidate();
	}

	public void openChest() {
		++numUsingPlayers;
		worldObj.addBlockEvent(xCoord, yCoord, zCoord,
				Block.enderChest.blockID, 1, numUsingPlayers);
	}

	public void closeChest() {
		--numUsingPlayers;
		worldObj.addBlockEvent(xCoord, yCoord, zCoord,
				Block.enderChest.blockID, 1, numUsingPlayers);
	}

	public boolean isUseableByPlayer(final EntityPlayer par1EntityPlayer) {
		return worldObj.getBlockTileEntity(xCoord, yCoord, zCoord) != this ? false
				: par1EntityPlayer.getDistanceSq(xCoord + 0.5D, yCoord + 0.5D,
						zCoord + 0.5D) <= 64.0D;
	}
}
