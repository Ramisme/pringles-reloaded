package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import net.minecraft.server.MinecraftServer;

public class AnvilSaveConverter extends SaveFormatOld {
	public AnvilSaveConverter(final File par1File) {
		super(par1File);
	}

	@Override
	public List getSaveList() throws AnvilConverterException {
		if (savesDirectory != null && savesDirectory.exists()
				&& savesDirectory.isDirectory()) {
			final ArrayList var1 = new ArrayList();
			final File[] var2 = savesDirectory.listFiles();
			final File[] var3 = var2;
			final int var4 = var2.length;

			for (int var5 = 0; var5 < var4; ++var5) {
				final File var6 = var3[var5];

				if (var6.isDirectory()) {
					final String var7 = var6.getName();
					final WorldInfo var8 = getWorldInfo(var7);

					if (var8 != null
							&& (var8.getSaveVersion() == 19132 || var8
									.getSaveVersion() == 19133)) {
						final boolean var9 = var8.getSaveVersion() != getSaveVersion();
						String var10 = var8.getWorldName();

						if (var10 == null
								|| MathHelper.stringNullOrLengthZero(var10)) {
							var10 = var7;
						}

						final long var11 = 0L;
						var1.add(new SaveFormatComparator(var7, var10, var8
								.getLastTimePlayed(), var11,
								var8.getGameType(), var9, var8
										.isHardcoreModeEnabled(), var8
										.areCommandsAllowed()));
					}
				}
			}

			return var1;
		} else {
			throw new AnvilConverterException(
					"Unable to read or access folder where game worlds are saved!");
		}
	}

	protected int getSaveVersion() {
		return 19133;
	}

	@Override
	public void flushCache() {
		RegionFileCache.clearRegionFileReferences();
	}

	/**
	 * Returns back a loader for the specified save directory
	 */
	@Override
	public ISaveHandler getSaveLoader(final String par1Str, final boolean par2) {
		return new AnvilSaveHandler(savesDirectory, par1Str, par2);
	}

	/**
	 * Checks if the save directory uses the old map format
	 */
	@Override
	public boolean isOldMapFormat(final String par1Str) {
		final WorldInfo var2 = getWorldInfo(par1Str);
		return var2 != null && var2.getSaveVersion() != getSaveVersion();
	}

	/**
	 * Converts the specified map to the new map format. Args: worldName,
	 * loadingScreen
	 */
	@Override
	public boolean convertMapFormat(final String par1Str,
			final IProgressUpdate par2IProgressUpdate) {
		par2IProgressUpdate.setLoadingProgress(0);
		final ArrayList var3 = new ArrayList();
		final ArrayList var4 = new ArrayList();
		final ArrayList var5 = new ArrayList();
		final File var6 = new File(savesDirectory, par1Str);
		final File var7 = new File(var6, "DIM-1");
		final File var8 = new File(var6, "DIM1");
		MinecraftServer.getServer().getLogAgent()
				.logInfo("Scanning folders...");
		addRegionFilesToCollection(var6, var3);

		if (var7.exists()) {
			addRegionFilesToCollection(var7, var4);
		}

		if (var8.exists()) {
			addRegionFilesToCollection(var8, var5);
		}

		final int var9 = var3.size() + var4.size() + var5.size();
		MinecraftServer.getServer().getLogAgent()
				.logInfo("Total conversion count is " + var9);
		final WorldInfo var10 = getWorldInfo(par1Str);
		Object var11 = null;

		if (var10.getTerrainType() == WorldType.FLAT) {
			var11 = new WorldChunkManagerHell(BiomeGenBase.plains, 0.5F, 0.5F);
		} else {
			var11 = new WorldChunkManager(var10.getSeed(),
					var10.getTerrainType());
		}

		convertFile(new File(var6, "region"), var3, (WorldChunkManager) var11,
				0, var9, par2IProgressUpdate);
		convertFile(new File(var7, "region"), var4, new WorldChunkManagerHell(
				BiomeGenBase.hell, 1.0F, 0.0F), var3.size(), var9,
				par2IProgressUpdate);
		convertFile(new File(var8, "region"), var5, new WorldChunkManagerHell(
				BiomeGenBase.sky, 0.5F, 0.0F), var3.size() + var4.size(), var9,
				par2IProgressUpdate);
		var10.setSaveVersion(19133);

		if (var10.getTerrainType() == WorldType.DEFAULT_1_1) {
			var10.setTerrainType(WorldType.DEFAULT);
		}

		createFile(par1Str);
		final ISaveHandler var12 = getSaveLoader(par1Str, false);
		var12.saveWorldInfo(var10);
		return true;
	}

	/**
	 * par: filename for the level.dat_mcr backup
	 */
	private void createFile(final String par1Str) {
		final File var2 = new File(savesDirectory, par1Str);

		if (!var2.exists()) {
			System.out
					.println("Warning: Unable to create level.dat_mcr backup");
		} else {
			final File var3 = new File(var2, "level.dat");

			if (!var3.exists()) {
				System.out
						.println("Warning: Unable to create level.dat_mcr backup");
			} else {
				final File var4 = new File(var2, "level.dat_mcr");

				if (!var3.renameTo(var4)) {
					System.out
							.println("Warning: Unable to create level.dat_mcr backup");
				}
			}
		}
	}

	private void convertFile(final File par1File, final Iterable par2Iterable,
			final WorldChunkManager par3WorldChunkManager, int par4,
			final int par5, final IProgressUpdate par6IProgressUpdate) {
		final Iterator var7 = par2Iterable.iterator();

		while (var7.hasNext()) {
			final File var8 = (File) var7.next();
			convertChunks(par1File, var8, par3WorldChunkManager, par4, par5,
					par6IProgressUpdate);
			++par4;
			final int var9 = (int) Math.round(100.0D * par4 / par5);
			par6IProgressUpdate.setLoadingProgress(var9);
		}
	}

	/**
	 * copies a 32x32 chunk set from par2File to par1File, via
	 * AnvilConverterData
	 */
	private void convertChunks(final File par1File, final File par2File,
			final WorldChunkManager par3WorldChunkManager, final int par4,
			final int par5, final IProgressUpdate par6IProgressUpdate) {
		try {
			final String var7 = par2File.getName();
			final RegionFile var8 = new RegionFile(par2File);
			final RegionFile var9 = new RegionFile(
					new File(par1File, var7.substring(0,
							var7.length() - ".mcr".length())
							+ ".mca"));

			for (int var10 = 0; var10 < 32; ++var10) {
				int var11;

				for (var11 = 0; var11 < 32; ++var11) {
					if (var8.isChunkSaved(var10, var11)
							&& !var9.isChunkSaved(var10, var11)) {
						final DataInputStream var12 = var8
								.getChunkDataInputStream(var10, var11);

						if (var12 == null) {
							MinecraftServer.getServer().getLogAgent()
									.logWarning("Failed to fetch input stream");
						} else {
							final NBTTagCompound var13 = CompressedStreamTools
									.read(var12);
							var12.close();
							final NBTTagCompound var14 = var13
									.getCompoundTag("Level");
							final AnvilConverterData var15 = ChunkLoader
									.load(var14);
							final NBTTagCompound var16 = new NBTTagCompound();
							final NBTTagCompound var17 = new NBTTagCompound();
							var16.setTag("Level", var17);
							ChunkLoader.convertToAnvilFormat(var15, var17,
									par3WorldChunkManager);
							final DataOutputStream var18 = var9
									.getChunkDataOutputStream(var10, var11);
							CompressedStreamTools.write(var16, var18);
							var18.close();
						}
					}
				}

				var11 = (int) Math
						.round(100.0D * (par4 * 1024) / (par5 * 1024));
				final int var20 = (int) Math.round(100.0D
						* ((var10 + 1) * 32 + par4 * 1024) / (par5 * 1024));

				if (var20 > var11) {
					par6IProgressUpdate.setLoadingProgress(var20);
				}
			}

			var8.close();
			var9.close();
		} catch (final IOException var19) {
			var19.printStackTrace();
		}
	}

	/**
	 * filters the files in the par1 directory, and adds them to the par2
	 * collections
	 */
	private void addRegionFilesToCollection(final File par1File,
			final Collection par2Collection) {
		final File var3 = new File(par1File, "region");
		final File[] var4 = var3.listFiles(new AnvilSaveConverterFileFilter(
				this));

		if (var4 != null) {
			Collections.addAll(par2Collection, var4);
		}
	}
}
