package net.minecraft.src;

public class ItemBow extends Item {
	public static final String[] bowPullIconNameArray = new String[] {
			"bow_pull_0", "bow_pull_1", "bow_pull_2" };
	private Icon[] iconArray;

	public ItemBow(final int par1) {
		super(par1);
		maxStackSize = 1;
		setMaxDamage(384);
		setCreativeTab(CreativeTabs.tabCombat);
	}

	/**
	 * called when the player releases the use item button. Args: itemstack,
	 * world, entityplayer, itemInUseCount
	 */
	@Override
	public void onPlayerStoppedUsing(final ItemStack par1ItemStack,
			final World par2World, final EntityPlayer par3EntityPlayer,
			final int par4) {
		final boolean var5 = par3EntityPlayer.capabilities.isCreativeMode
				|| EnchantmentHelper.getEnchantmentLevel(
						Enchantment.infinity.effectId, par1ItemStack) > 0;

		if (var5 || par3EntityPlayer.inventory.hasItem(Item.arrow.itemID)) {
			final int var6 = getMaxItemUseDuration(par1ItemStack) - par4;
			float var7 = var6 / 20.0F;
			var7 = (var7 * var7 + var7 * 2.0F) / 3.0F;

			if (var7 < 0.1D) {
				return;
			}

			if (var7 > 1.0F) {
				var7 = 1.0F;
			}

			final EntityArrow var8 = new EntityArrow(par2World,
					par3EntityPlayer, var7 * 2.0F);

			if (var7 == 1.0F) {
				var8.setIsCritical(true);
			}

			final int var9 = EnchantmentHelper.getEnchantmentLevel(
					Enchantment.power.effectId, par1ItemStack);

			if (var9 > 0) {
				var8.setDamage(var8.getDamage() + var9 * 0.5D + 0.5D);
			}

			final int var10 = EnchantmentHelper.getEnchantmentLevel(
					Enchantment.punch.effectId, par1ItemStack);

			if (var10 > 0) {
				var8.setKnockbackStrength(var10);
			}

			if (EnchantmentHelper.getEnchantmentLevel(
					Enchantment.flame.effectId, par1ItemStack) > 0) {
				var8.setFire(100);
			}

			par1ItemStack.damageItem(1, par3EntityPlayer);
			par2World.playSoundAtEntity(par3EntityPlayer, "random.bow", 1.0F,
					1.0F / (Item.itemRand.nextFloat() * 0.4F + 1.2F) + var7
							* 0.5F);

			if (var5) {
				var8.canBePickedUp = 2;
			} else {
				par3EntityPlayer.inventory
						.consumeInventoryItem(Item.arrow.itemID);
			}

			if (!par2World.isRemote) {
				par2World.spawnEntityInWorld(var8);
			}
		}
	}

	@Override
	public ItemStack onEaten(final ItemStack par1ItemStack,
			final World par2World, final EntityPlayer par3EntityPlayer) {
		return par1ItemStack;
	}

	/**
	 * How long it takes to use or consume an item
	 */
	@Override
	public int getMaxItemUseDuration(final ItemStack par1ItemStack) {
		return 72000;
	}

	/**
	 * returns the action that specifies what animation to play when the items
	 * is being used
	 */
	@Override
	public EnumAction getItemUseAction(final ItemStack par1ItemStack) {
		return EnumAction.bow;
	}

	/**
	 * Called whenever this item is equipped and the right mouse button is
	 * pressed. Args: itemStack, world, entityPlayer
	 */
	@Override
	public ItemStack onItemRightClick(final ItemStack par1ItemStack,
			final World par2World, final EntityPlayer par3EntityPlayer) {
		if (par3EntityPlayer.capabilities.isCreativeMode
				|| par3EntityPlayer.inventory.hasItem(Item.arrow.itemID)) {
			par3EntityPlayer.setItemInUse(par1ItemStack,
					getMaxItemUseDuration(par1ItemStack));
		}

		return par1ItemStack;
	}

	/**
	 * Return the enchantability factor of the item, most of the time is based
	 * on material.
	 */
	@Override
	public int getItemEnchantability() {
		return 1;
	}

	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		super.registerIcons(par1IconRegister);
		iconArray = new Icon[ItemBow.bowPullIconNameArray.length];

		for (int var2 = 0; var2 < iconArray.length; ++var2) {
			iconArray[var2] = par1IconRegister
					.registerIcon(ItemBow.bowPullIconNameArray[var2]);
		}
	}

	/**
	 * used to cycle through icons based on their used duration, i.e. for the
	 * bow
	 */
	public Icon getItemIconForUseDuration(final int par1) {
		return iconArray[par1];
	}
}
