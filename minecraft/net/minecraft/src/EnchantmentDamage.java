package net.minecraft.src;

public class EnchantmentDamage extends Enchantment {
	/** Holds the name to be translated of each protection type. */
	private static final String[] protectionName = new String[] { "all",
			"undead", "arthropods" };

	/**
	 * Holds the base factor of enchantability needed to be able to use the
	 * enchant.
	 */
	private static final int[] baseEnchantability = new int[] { 1, 5, 5 };

	/**
	 * Holds how much each level increased the enchantability factor to be able
	 * to use this enchant.
	 */
	private static final int[] levelEnchantability = new int[] { 11, 8, 8 };

	/**
	 * Used on the formula of base enchantability, this is the 'window' factor
	 * of values to be able to use thing enchant.
	 */
	private static final int[] thresholdEnchantability = new int[] { 20, 20, 20 };

	/**
	 * Defines the type of damage of the enchantment, 0 = all, 1 = undead, 3 =
	 * arthropods
	 */
	public final int damageType;

	public EnchantmentDamage(final int par1, final int par2, final int par3) {
		super(par1, par2, EnumEnchantmentType.weapon);
		damageType = par3;
	}

	/**
	 * Returns the minimal value of enchantability needed on the enchantment
	 * level passed.
	 */
	@Override
	public int getMinEnchantability(final int par1) {
		return EnchantmentDamage.baseEnchantability[damageType] + (par1 - 1)
				* EnchantmentDamage.levelEnchantability[damageType];
	}

	/**
	 * Returns the maximum value of enchantability nedded on the enchantment
	 * level passed.
	 */
	@Override
	public int getMaxEnchantability(final int par1) {
		return getMinEnchantability(par1)
				+ EnchantmentDamage.thresholdEnchantability[damageType];
	}

	/**
	 * Returns the maximum level that the enchantment can have.
	 */
	@Override
	public int getMaxLevel() {
		return 5;
	}

	/**
	 * Calculates de (magic) damage done by the enchantment on a living entity
	 * based on level and entity passed.
	 */
	@Override
	public int calcModifierLiving(final int par1,
			final EntityLiving par2EntityLiving) {
		return damageType == 0 ? MathHelper.floor_float(par1 * 2.75F)
				: damageType == 1
						&& par2EntityLiving.getCreatureAttribute() == EnumCreatureAttribute.UNDEAD ? MathHelper
						.floor_float(par1 * 4.5F)
						: damageType == 2
								&& par2EntityLiving.getCreatureAttribute() == EnumCreatureAttribute.ARTHROPOD ? MathHelper
								.floor_float(par1 * 4.5F) : 0;
	}

	/**
	 * Return the name of key in translation table of this enchantment.
	 */
	@Override
	public String getName() {
		return "enchantment.damage."
				+ EnchantmentDamage.protectionName[damageType];
	}

	/**
	 * Determines if the enchantment passed can be applyied together with this
	 * enchantment.
	 */
	@Override
	public boolean canApplyTogether(final Enchantment par1Enchantment) {
		return !(par1Enchantment instanceof EnchantmentDamage);
	}

	@Override
	public boolean canApply(final ItemStack par1ItemStack) {
		return par1ItemStack.getItem() instanceof ItemAxe ? true : super
				.canApply(par1ItemStack);
	}
}
