package net.minecraft.src;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.imageio.ImageIO;

public class TextureAnimations {
	private static TextureAnimation[] textureAnimations = null;
	private static RenderEngine renderEngine = null;

	public static void reset() {
		TextureAnimations.textureAnimations = null;
	}

	public static void update(final RenderEngine var0) {
		TextureAnimations.renderEngine = var0;
		final ITexturePack var1 = var0.texturePack.getSelectedTexturePack();
		TextureAnimations.textureAnimations = TextureAnimations
				.getTextureAnimations(var1);
		TextureAnimations.updateAnimations();
	}

	public static void updateCustomAnimations() {
		if (TextureAnimations.textureAnimations != null) {
			if (Config.isAnimatedTextures()) {
				TextureAnimations.updateAnimations();
			}
		}
	}

	public static void updateAnimations() {
		if (TextureAnimations.textureAnimations != null) {
			for (final TextureAnimation var1 : TextureAnimations.textureAnimations) {
				var1.updateTexture();
			}
		}
	}

	public static TextureAnimation[] getTextureAnimations(
			final ITexturePack var0) {
		if (!(var0 instanceof TexturePackImplementation)) {
			return null;
		} else {
			final TexturePackImplementation var1 = (TexturePackImplementation) var0;
			final File var2 = var1.texturePackFile;

			if (var2 == null) {
				return null;
			} else if (!var2.exists()) {
				return null;
			} else {
				String[] var3 = null;

				if (var2.isFile()) {
					var3 = TextureAnimations.getAnimationPropertiesZip(var2);
				} else {
					var3 = TextureAnimations.getAnimationPropertiesDir(var2);
				}

				if (var3 == null) {
					return null;
				} else {
					final ArrayList var4 = new ArrayList();

					for (final String var6 : var3) {
						Config.dbg("Texture animation: " + var6);

						try {
							final InputStream var7 = var1
									.getResourceAsStream(var6);
							final Properties var8 = new Properties();
							var8.load(var7);
							final TextureAnimation var9 = TextureAnimations
									.makeTextureAnimation(var8);

							if (var9 != null) {
								var4.add(var9);
							}
						} catch (final FileNotFoundException var10) {
							Config.dbg("File not found: " + var10.getMessage());
						} catch (final IOException var11) {
							var11.printStackTrace();
						}
					}

					final TextureAnimation[] var12 = (TextureAnimation[]) var4
							.toArray(new TextureAnimation[var4.size()]);
					return var12;
				}
			}
		}
	}

	public static TextureAnimation makeTextureAnimation(final Properties var0) {
		final String var1 = var0.getProperty("from");
		final String var2 = var0.getProperty("to");
		final int var3 = Config.parseInt(var0.getProperty("x"), -1);
		final int var4 = Config.parseInt(var0.getProperty("y"), -1);
		final int var5 = Config.parseInt(var0.getProperty("w"), -1);
		final int var6 = Config.parseInt(var0.getProperty("h"), -1);

		if (var1 != null && var2 != null) {
			if (var3 >= 0 && var4 >= 0 && var5 >= 0 && var6 >= 0) {
				final byte[] var7 = TextureAnimations.getCustomTextureData(
						var1, var5);

				if (var7 == null) {
					Config.dbg("TextureAnimation: Source texture not found: "
							+ var2);
					return null;
				} else if (!TextureAnimations.renderEngine.getTexturePack()
						.getSelectedTexturePack().func_98138_b(var2, true)) {
					Config.dbg("TextureAnimation: Target texture not found: "
							+ var2);
					return null;
				} else {
					final int var8 = TextureAnimations.renderEngine
							.getTexture(var2);

					if (var8 < 0) {
						Config.dbg("TextureAnimation: Target texture not found: "
								+ var2);
						return null;
					} else {
						final TextureAnimation var9 = new TextureAnimation(
								var1, var7, var2, var8, var3, var4, var5, var6,
								var0, 1);
						return var9;
					}
				}
			} else {
				Config.dbg("TextureAnimation: Invalid coordinates");
				return null;
			}
		} else {
			Config.dbg("TextureAnimation: Source or target texture not specified");
			return null;
		}
	}

	public static String[] getAnimationPropertiesDir(final File var0) {
		final File var1 = new File(var0, "anim");

		if (!var1.exists()) {
			return null;
		} else if (!var1.isDirectory()) {
			return null;
		} else {
			final File[] var2 = var1.listFiles();

			if (var2 == null) {
				return null;
			} else {
				final ArrayList var3 = new ArrayList();

				for (final File var5 : var2) {
					final String var6 = var5.getName();

					if (!var6.startsWith("custom_")
							&& var6.endsWith(".properties") && var5.isFile()
							&& var5.canRead()) {
						Config.dbg("TextureAnimation: anim/" + var5.getName());
						var3.add("/anim/" + var6);
					}
				}

				final String[] var7 = (String[]) var3.toArray(new String[var3
						.size()]);
				return var7;
			}
		}
	}

	public static String[] getAnimationPropertiesZip(final File var0) {
		try {
			final ZipFile var1 = new ZipFile(var0);
			final Enumeration var2 = var1.entries();
			final ArrayList var3 = new ArrayList();

			while (var2.hasMoreElements()) {
				final ZipEntry var4 = (ZipEntry) var2.nextElement();
				final String var5 = var4.getName();

				if (var5.startsWith("anim/")
						&& !var5.startsWith("anim/custom_")
						&& var5.endsWith(".properties")) {
					var3.add("/" + var5);
				}
			}

			var1.close();

			final String[] var7 = (String[]) var3.toArray(new String[var3
					.size()]);
			return var7;
		} catch (final IOException var6) {
			var6.printStackTrace();
			return null;
		}
	}

	public static byte[] getCustomTextureData(final String var0, final int var1) {
		byte[] var2 = TextureAnimations.loadImage(var0, var1);

		if (var2 == null) {
			var2 = TextureAnimations.loadImage("/anim" + var0, var1);
		}

		return var2;
	}

	private static byte[] loadImage(final String var0, final int var1) {
		final ITexturePack var2 = TextureAnimations.renderEngine
				.getTexturePack().getSelectedTexturePack();
		final GameSettings var3 = Config.getGameSettings();

		try {
			if (var2 == null) {
				return null;
			} else {
				final InputStream var5 = var2.getResourceAsStream(var0);

				if (var5 == null) {
					return null;
				} else {
					BufferedImage var6 = TextureAnimations
							.readTextureImage(var5);

					if (var6 == null) {
						return null;
					} else {
						if (var1 > 0 && var6.getWidth() != var1) {
							final double var7 = var6.getHeight()
									/ var6.getWidth();
							final int var9 = (int) (var1 * var7);
							var6 = TextureAnimations.scaleBufferedImage(var6,
									var1, var9);
						}

						final int var21 = var6.getWidth();
						final int var8 = var6.getHeight();
						final int[] var22 = new int[var21 * var8];
						final byte[] var10 = new byte[var21 * var8 * 4];
						var6.getRGB(0, 0, var21, var8, var22, 0, var21);

						for (int var11 = 0; var11 < var22.length; ++var11) {
							final int var12 = var22[var11] >> 24 & 255;
							int var13 = var22[var11] >> 16 & 255;
							int var14 = var22[var11] >> 8 & 255;
							int var15 = var22[var11] & 255;

							if (var3 != null && var3.anaglyph) {
								final int var16 = (var13 * 30 + var14 * 59 + var15 * 11) / 100;
								final int var17 = (var13 * 30 + var14 * 70) / 100;
								final int var18 = (var13 * 30 + var15 * 70) / 100;
								var13 = var16;
								var14 = var17;
								var15 = var18;
							}

							var10[var11 * 4 + 0] = (byte) var13;
							var10[var11 * 4 + 1] = (byte) var14;
							var10[var11 * 4 + 2] = (byte) var15;
							var10[var11 * 4 + 3] = (byte) var12;
						}

						return var10;
					}
				}
			}
		} catch (final FileNotFoundException var19) {
			return null;
		} catch (final Exception var20) {
			var20.printStackTrace();
			return null;
		}
	}

	private static BufferedImage readTextureImage(final InputStream var0)
			throws IOException {
		final BufferedImage var1 = ImageIO.read(var0);
		var0.close();
		return var1;
	}

	public static BufferedImage scaleBufferedImage(final BufferedImage var0,
			final int var1, final int var2) {
		final BufferedImage var3 = new BufferedImage(var1, var2, 2);
		final Graphics2D var4 = var3.createGraphics();
		var4.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
				RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		var4.drawImage(var0, 0, 0, var1, var2, (ImageObserver) null);
		return var3;
	}
}
