package net.minecraft.src;

public class ItemArmor extends Item {
	/** Holds the 'base' maxDamage that each armorType have. */
	private static final int[] maxDamageArray = new int[] { 11, 16, 15, 13 };
	private static final String[] field_94606_cu = new String[] {
			"helmetCloth_overlay", "chestplateCloth_overlay",
			"leggingsCloth_overlay", "bootsCloth_overlay" };
	public static final String[] field_94603_a = new String[] {
			"slot_empty_helmet", "slot_empty_chestplate",
			"slot_empty_leggings", "slot_empty_boots" };
	private static final IBehaviorDispenseItem field_96605_cw = new BehaviorDispenseArmor();

	/**
	 * Stores the armor type: 0 is helmet, 1 is plate, 2 is legs and 3 is boots
	 */
	public final int armorType;

	/** Holds the amount of damage that the armor reduces at full durability. */
	public final int damageReduceAmount;

	/**
	 * Used on RenderPlayer to select the correspondent armor to be rendered on
	 * the player: 0 is cloth, 1 is chain, 2 is iron, 3 is diamond and 4 is
	 * gold.
	 */
	public final int renderIndex;

	/** The EnumArmorMaterial used for this ItemArmor */
	private final EnumArmorMaterial material;
	private Icon field_94605_cw;
	private Icon field_94604_cx;

	public ItemArmor(final int par1,
			final EnumArmorMaterial par2EnumArmorMaterial, final int par3,
			final int par4) {
		super(par1);
		material = par2EnumArmorMaterial;
		armorType = par4;
		renderIndex = par3;
		damageReduceAmount = par2EnumArmorMaterial
				.getDamageReductionAmount(par4);
		setMaxDamage(par2EnumArmorMaterial.getDurability(par4));
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabCombat);
		BlockDispenser.dispenseBehaviorRegistry.putObject(this,
				ItemArmor.field_96605_cw);
	}

	@Override
	public int getColorFromItemStack(final ItemStack par1ItemStack,
			final int par2) {
		if (par2 > 0) {
			return 16777215;
		} else {
			int var3 = getColor(par1ItemStack);

			if (var3 < 0) {
				var3 = 16777215;
			}

			return var3;
		}
	}

	@Override
	public boolean requiresMultipleRenderPasses() {
		return material == EnumArmorMaterial.CLOTH;
	}

	/**
	 * Return the enchantability factor of the item, most of the time is based
	 * on material.
	 */
	@Override
	public int getItemEnchantability() {
		return material.getEnchantability();
	}

	/**
	 * Return the armor material for this armor item.
	 */
	public EnumArmorMaterial getArmorMaterial() {
		return material;
	}

	/**
	 * Return whether the specified armor ItemStack has a color.
	 */
	public boolean hasColor(final ItemStack par1ItemStack) {
		return material != EnumArmorMaterial.CLOTH ? false : !par1ItemStack
				.hasTagCompound() ? false : !par1ItemStack.getTagCompound()
				.hasKey("display") ? false : par1ItemStack.getTagCompound()
				.getCompoundTag("display").hasKey("color");
	}

	/**
	 * Return the color for the specified armor ItemStack.
	 */
	public int getColor(final ItemStack par1ItemStack) {
		if (material != EnumArmorMaterial.CLOTH) {
			return -1;
		} else {
			final NBTTagCompound var2 = par1ItemStack.getTagCompound();

			if (var2 == null) {
				return 10511680;
			} else {
				final NBTTagCompound var3 = var2.getCompoundTag("display");
				return var3 == null ? 10511680 : var3.hasKey("color") ? var3
						.getInteger("color") : 10511680;
			}
		}
	}

	/**
	 * Gets an icon index based on an item's damage value and the given render
	 * pass
	 */
	@Override
	public Icon getIconFromDamageForRenderPass(final int par1, final int par2) {
		return par2 == 1 ? field_94605_cw : super
				.getIconFromDamageForRenderPass(par1, par2);
	}

	/**
	 * Remove the color from the specified armor ItemStack.
	 */
	public void removeColor(final ItemStack par1ItemStack) {
		if (material == EnumArmorMaterial.CLOTH) {
			final NBTTagCompound var2 = par1ItemStack.getTagCompound();

			if (var2 != null) {
				final NBTTagCompound var3 = var2.getCompoundTag("display");

				if (var3.hasKey("color")) {
					var3.removeTag("color");
				}
			}
		}
	}

	public void func_82813_b(final ItemStack par1ItemStack, final int par2) {
		if (material != EnumArmorMaterial.CLOTH) {
			throw new UnsupportedOperationException("Can\'t dye non-leather!");
		} else {
			NBTTagCompound var3 = par1ItemStack.getTagCompound();

			if (var3 == null) {
				var3 = new NBTTagCompound();
				par1ItemStack.setTagCompound(var3);
			}

			final NBTTagCompound var4 = var3.getCompoundTag("display");

			if (!var3.hasKey("display")) {
				var3.setCompoundTag("display", var4);
			}

			var4.setInteger("color", par2);
		}
	}

	/**
	 * Return whether this item is repairable in an anvil.
	 */
	@Override
	public boolean getIsRepairable(final ItemStack par1ItemStack,
			final ItemStack par2ItemStack) {
		return material.getArmorCraftingMaterial() == par2ItemStack.itemID ? true
				: super.getIsRepairable(par1ItemStack, par2ItemStack);
	}

	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		super.registerIcons(par1IconRegister);

		if (material == EnumArmorMaterial.CLOTH) {
			field_94605_cw = par1IconRegister
					.registerIcon(ItemArmor.field_94606_cu[armorType]);
		}

		field_94604_cx = par1IconRegister
				.registerIcon(ItemArmor.field_94603_a[armorType]);
	}

	/**
	 * Called whenever this item is equipped and the right mouse button is
	 * pressed. Args: itemStack, world, entityPlayer
	 */
	@Override
	public ItemStack onItemRightClick(final ItemStack par1ItemStack,
			final World par2World, final EntityPlayer par3EntityPlayer) {
		final int var4 = EntityLiving.getArmorPosition(par1ItemStack) - 1;
		final ItemStack var5 = par3EntityPlayer.getCurrentArmor(var4);

		if (var5 == null) {
			par3EntityPlayer.setCurrentItemOrArmor(var4, par1ItemStack.copy());
			par1ItemStack.stackSize = 0;
		}

		return par1ItemStack;
	}

	public static Icon func_94602_b(final int par0) {
		switch (par0) {
		case 0:
			return Item.helmetDiamond.field_94604_cx;

		case 1:
			return Item.plateDiamond.field_94604_cx;

		case 2:
			return Item.legsDiamond.field_94604_cx;

		case 3:
			return Item.bootsDiamond.field_94604_cx;

		default:
			return null;
		}
	}

	/**
	 * Returns the 'max damage' factor array for the armor, each piece of armor
	 * have a durability factor (that gets multiplied by armor material factor)
	 */
	static int[] getMaxDamageArray() {
		return ItemArmor.maxDamageArray;
	}
}
