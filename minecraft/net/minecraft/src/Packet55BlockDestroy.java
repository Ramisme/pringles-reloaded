package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet55BlockDestroy extends Packet {
	/** Entity breaking the block */
	private int entityId;

	/** X posiiton of the block */
	private int posX;

	/** Y position of the block */
	private int posY;

	/** Z position of the block */
	private int posZ;

	/** How far destroyed this block is */
	private int destroyedStage;

	public Packet55BlockDestroy() {
	}

	public Packet55BlockDestroy(final int par1, final int par2, final int par3,
			final int par4, final int par5) {
		entityId = par1;
		posX = par2;
		posY = par3;
		posZ = par4;
		destroyedStage = par5;
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		entityId = par1DataInputStream.readInt();
		posX = par1DataInputStream.readInt();
		posY = par1DataInputStream.readInt();
		posZ = par1DataInputStream.readInt();
		destroyedStage = par1DataInputStream.read();
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		par1DataOutputStream.writeInt(entityId);
		par1DataOutputStream.writeInt(posX);
		par1DataOutputStream.writeInt(posY);
		par1DataOutputStream.writeInt(posZ);
		par1DataOutputStream.write(destroyedStage);
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleBlockDestroy(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 13;
	}

	/**
	 * Gets the ID of the entity breaking the block
	 */
	public int getEntityId() {
		return entityId;
	}

	/**
	 * Gets the X position of the block
	 */
	public int getPosX() {
		return posX;
	}

	/**
	 * Gets the Y position of the block
	 */
	public int getPosY() {
		return posY;
	}

	/**
	 * Gets the Z position of the block
	 */
	public int getPosZ() {
		return posZ;
	}

	/**
	 * Gets how far destroyed this block is
	 */
	public int getDestroyedStage() {
		return destroyedStage;
	}

	/**
	 * only false for the abstract Packet class, all real packets return true
	 */
	@Override
	public boolean isRealPacket() {
		return true;
	}

	/**
	 * eg return packet30entity.entityId == entityId; WARNING : will throw if
	 * you compare a packet to a different packet class
	 */
	@Override
	public boolean containsSameEntityIDAs(final Packet par1Packet) {
		final Packet55BlockDestroy var2 = (Packet55BlockDestroy) par1Packet;
		return var2.entityId == entityId;
	}
}
