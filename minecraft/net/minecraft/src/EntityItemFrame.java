package net.minecraft.src;

public class EntityItemFrame extends EntityHanging {
	/** Chance for this item frame's item to drop from the frame. */
	private float itemDropChance = 1.0F;

	public EntityItemFrame(final World par1World) {
		super(par1World);
	}

	public EntityItemFrame(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		super(par1World, par2, par3, par4, par5);
		setDirection(par5);
	}

	@Override
	protected void entityInit() {
		getDataWatcher().addObjectByDataType(2, 5);
		getDataWatcher().addObject(3, Byte.valueOf((byte) 0));
	}

	@Override
	public int func_82329_d() {
		return 9;
	}

	@Override
	public int func_82330_g() {
		return 9;
	}

	/**
	 * Checks if the entity is in range to render by using the past in distance
	 * and comparing it to its average edge length * 64 * renderDistanceWeight
	 * Args: distance
	 */
	@Override
	public boolean isInRangeToRenderDist(final double par1) {
		double var3 = 16.0D;
		var3 *= 64.0D * renderDistanceWeight;
		return par1 < var3 * var3;
	}

	/**
	 * Drop the item currently on this item frame.
	 */
	@Override
	public void dropItemStack() {
		entityDropItem(new ItemStack(Item.itemFrame), 0.0F);
		ItemStack var1 = getDisplayedItem();

		if (var1 != null && rand.nextFloat() < itemDropChance) {
			var1 = var1.copy();
			var1.setItemFrame((EntityItemFrame) null);
			entityDropItem(var1, 0.0F);
		}
	}

	public ItemStack getDisplayedItem() {
		return getDataWatcher().getWatchableObjectItemStack(2);
	}

	public void setDisplayedItem(ItemStack par1ItemStack) {
		par1ItemStack = par1ItemStack.copy();
		par1ItemStack.stackSize = 1;
		par1ItemStack.setItemFrame(this);
		getDataWatcher().updateObject(2, par1ItemStack);
		getDataWatcher().setObjectWatched(2);
	}

	/**
	 * Return the rotation of the item currently on this frame.
	 */
	public int getRotation() {
		return getDataWatcher().getWatchableObjectByte(3);
	}

	public void setItemRotation(final int par1) {
		getDataWatcher().updateObject(3, Byte.valueOf((byte) (par1 % 4)));
	}

	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	@Override
	public void writeEntityToNBT(final NBTTagCompound par1NBTTagCompound) {
		if (getDisplayedItem() != null) {
			par1NBTTagCompound.setCompoundTag("Item", getDisplayedItem()
					.writeToNBT(new NBTTagCompound()));
			par1NBTTagCompound.setByte("ItemRotation", (byte) getRotation());
			par1NBTTagCompound.setFloat("ItemDropChance", itemDropChance);
		}

		super.writeEntityToNBT(par1NBTTagCompound);
	}

	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	@Override
	public void readEntityFromNBT(final NBTTagCompound par1NBTTagCompound) {
		final NBTTagCompound var2 = par1NBTTagCompound.getCompoundTag("Item");

		if (var2 != null && !var2.hasNoTags()) {
			setDisplayedItem(ItemStack.loadItemStackFromNBT(var2));
			setItemRotation(par1NBTTagCompound.getByte("ItemRotation"));

			if (par1NBTTagCompound.hasKey("ItemDropChance")) {
				itemDropChance = par1NBTTagCompound.getFloat("ItemDropChance");
			}
		}

		super.readEntityFromNBT(par1NBTTagCompound);
	}

	/**
	 * Called when a player interacts with a mob. e.g. gets milk from a cow,
	 * gets into the saddle on a pig.
	 */
	@Override
	public boolean interact(final EntityPlayer par1EntityPlayer) {
		if (getDisplayedItem() == null) {
			final ItemStack var2 = par1EntityPlayer.getHeldItem();

			if (var2 != null && !worldObj.isRemote) {
				setDisplayedItem(var2);

				if (!par1EntityPlayer.capabilities.isCreativeMode
						&& --var2.stackSize <= 0) {
					par1EntityPlayer.inventory.setInventorySlotContents(
							par1EntityPlayer.inventory.currentItem,
							(ItemStack) null);
				}
			}
		} else if (!worldObj.isRemote) {
			setItemRotation(getRotation() + 1);
		}

		return true;
	}
}
