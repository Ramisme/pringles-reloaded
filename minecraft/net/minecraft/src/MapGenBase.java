package net.minecraft.src;

import java.util.Random;

public class MapGenBase {
	/** The number of Chunks to gen-check in any given direction. */
	protected int range = 8;

	/** The RNG used by the MapGen classes. */
	protected Random rand = new Random();

	/** This world object. */
	protected World worldObj;

	public void generate(final IChunkProvider par1IChunkProvider,
			final World par2World, final int par3, final int par4,
			final byte[] par5ArrayOfByte) {
		final int var6 = range;
		worldObj = par2World;
		rand.setSeed(par2World.getSeed());
		final long var7 = rand.nextLong();
		final long var9 = rand.nextLong();

		for (int var11 = par3 - var6; var11 <= par3 + var6; ++var11) {
			for (int var12 = par4 - var6; var12 <= par4 + var6; ++var12) {
				final long var13 = var11 * var7;
				final long var15 = var12 * var9;
				rand.setSeed(var13 ^ var15 ^ par2World.getSeed());
				recursiveGenerate(par2World, var11, var12, par3, par4,
						par5ArrayOfByte);
			}
		}
	}

	/**
	 * Recursively called by generate() (generate) and optionally by itself.
	 */
	protected void recursiveGenerate(final World par1World, final int par2,
			final int par3, final int par4, final int par5,
			final byte[] par6ArrayOfByte) {
	}
}
