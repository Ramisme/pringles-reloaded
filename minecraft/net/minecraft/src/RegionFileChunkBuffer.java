package net.minecraft.src;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

class RegionFileChunkBuffer extends ByteArrayOutputStream {
	private final int chunkX;
	private final int chunkZ;

	final RegionFile regionFile;

	public RegionFileChunkBuffer(final RegionFile par1RegionFile,
			final int par2, final int par3) {
		super(8096);
		regionFile = par1RegionFile;
		chunkX = par2;
		chunkZ = par3;
	}

	@Override
	public void close() throws IOException {
		regionFile.write(chunkX, chunkZ, buf, count);
	}
}
