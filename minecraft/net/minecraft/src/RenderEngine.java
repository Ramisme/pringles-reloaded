package net.minecraft.src;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import net.minecraft.client.Minecraft;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

public class RenderEngine {
	private final HashMap textureMap = new HashMap();

	/** Texture contents map (key: texture name, value: int[] contents) */
	private final HashMap textureContentsMap = new HashMap();

	/** A mapping from GL texture names (integers) to BufferedImage instances */
	private final IntHashMap textureNameToImageMap = new IntHashMap();

	/** Stores the image data for the texture. */
	private IntBuffer imageData = GLAllocation.createDirectIntBuffer(4194304);

	/** A mapping from image URLs to ThreadDownloadImageData instances */
	private final Map urlToImageDataMap = new HashMap();

	/** Reference to the GameSettings object */
	private final GameSettings options;

	/** Texture pack */
	public TexturePackList texturePack;

	/** Missing texture image */
	private final BufferedImage missingTextureImage = new BufferedImage(64, 64,
			2);
	public final TextureMap textureMapBlocks;
	public final TextureMap textureMapItems;
	public int boundTexture;
	public static Logger log = Logger.getAnonymousLogger();
	private boolean initialized = false;

	public RenderEngine(final TexturePackList par1TexturePackList,
			final GameSettings par2GameSettings) {
		texturePack = par1TexturePackList;
		options = par2GameSettings;
		final Graphics var3 = missingTextureImage.getGraphics();
		var3.setColor(Color.WHITE);
		var3.fillRect(0, 0, 64, 64);
		var3.setColor(Color.BLACK);
		int var4 = 10;
		int var5 = 0;

		while (var4 < 64) {
			final String var6 = var5++ % 2 == 0 ? "missing" : "texture";
			var3.drawString(var6, 1, var4);
			var4 += var3.getFont().getSize();

			if (var5 % 2 == 0) {
				var4 += 5;
			}
		}

		var3.dispose();
		textureMapBlocks = new TextureMap(0, "terrain", "textures/blocks/",
				missingTextureImage);
		textureMapItems = new TextureMap(1, "items", "textures/items/",
				missingTextureImage);
	}

	public int[] getTextureContents(final String par1Str) {
		final ITexturePack var2 = texturePack.getSelectedTexturePack();
		final int[] var3 = (int[]) textureContentsMap.get(par1Str);

		if (var3 != null) {
			return var3;
		} else {
			int[] var5;

			try {
				final InputStream var4 = var2.getResourceAsStream(par1Str);

				if (var4 == null) {
					var5 = getImageContentsAndAllocate(missingTextureImage);
				} else {
					var5 = getImageContentsAndAllocate(this
							.readTextureImage(var4));
				}

				textureContentsMap.put(par1Str, var5);
				return var5;
			} catch (final IOException var6) {
				var6.printStackTrace();
				var5 = getImageContentsAndAllocate(missingTextureImage);
				textureContentsMap.put(par1Str, var5);
				return var5;
			}
		}
	}

	private int[] getImageContentsAndAllocate(
			final BufferedImage par1BufferedImage) {
		return getImageContents(
				par1BufferedImage,
				new int[par1BufferedImage.getWidth()
						* par1BufferedImage.getHeight()]);
	}

	private int[] getImageContents(final BufferedImage par1BufferedImage,
			final int[] par2ArrayOfInteger) {
		final int var3 = par1BufferedImage.getWidth();
		final int var4 = par1BufferedImage.getHeight();
		par1BufferedImage.getRGB(0, 0, var3, var4, par2ArrayOfInteger, 0, var3);
		return par2ArrayOfInteger;
	}

	public void bindTexture(final String par1Str) {
		this.bindTexture(getTexture(par1Str));
	}

	public void bindTexture(final int par1) {
		if (par1 != boundTexture) {
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, par1);
			boundTexture = par1;
		}
	}

	public void resetBoundTexture() {
		boundTexture = -1;
	}

	public int getTexture(String par1Str) {
		if (Config.isRandomMobs()) {
			par1Str = RandomMobs.getTexture(par1Str);
		}

		if (par1Str.equals("/terrain.png")) {
			textureMapBlocks.getTexture().bindTexture(0);
			return textureMapBlocks.getTexture().getGlTextureId();
		} else if (par1Str.equals("/gui/items.png")) {
			textureMapItems.getTexture().bindTexture(0);
			return textureMapItems.getTexture().getGlTextureId();
		} else {
			final Integer var2 = (Integer) textureMap.get(par1Str);

			if (var2 != null) {
				return var2.intValue();
			} else {
				final String var3 = par1Str;

				try {
					Reflector.callVoid(
							Reflector.ForgeHooksClient_onTextureLoadPre,
							new Object[] { par1Str });
					final int var4 = GLAllocation.generateTextureNames();
					final boolean var9 = par1Str.startsWith("%blur%");

					if (var9) {
						par1Str = par1Str.substring(6);
					}

					final boolean var6 = par1Str.startsWith("%clamp%");

					if (var6) {
						par1Str = par1Str.substring(7);
					}

					final InputStream var7 = texturePack
							.getSelectedTexturePack().getResourceAsStream(
									par1Str);

					if (var7 == null) {
						setupTextureExt(missingTextureImage, var4, var9, var6);
					} else {
						setupTextureExt(this.readTextureImage(var7), var4,
								var9, var6);
					}

					textureMap.put(var3, Integer.valueOf(var4));
					Reflector.callVoid(
							Reflector.ForgeHooksClient_onTextureLoad,
							new Object[] { par1Str,
									texturePack.getSelectedTexturePack() });
					return var4;
				} catch (final Exception var8) {
					var8.printStackTrace();
					final int var5 = GLAllocation.generateTextureNames();
					setupTexture(missingTextureImage, var5);
					textureMap.put(par1Str, Integer.valueOf(var5));
					return var5;
				}
			}
		}
	}

	/**
	 * Copy the supplied image onto a newly-allocated OpenGL texture, returning
	 * the allocated texture name
	 */
	public int allocateAndSetupTexture(final BufferedImage par1BufferedImage) {
		final int var2 = GLAllocation.generateTextureNames();
		setupTexture(par1BufferedImage, var2);
		textureNameToImageMap.addKey(var2, par1BufferedImage);
		return var2;
	}

	/**
	 * Copy the supplied image onto the specified OpenGL texture
	 */
	public void setupTexture(final BufferedImage par1BufferedImage,
			final int par2) {
		setupTextureExt(par1BufferedImage, par2, false, false);
	}

	public void setupTextureExt(final BufferedImage par1BufferedImage,
			final int par2, final boolean par3, final boolean par4) {
		this.bindTexture(par2);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER,
				GL11.GL_NEAREST);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER,
				GL11.GL_NEAREST);

		if (par3) {
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D,
					GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D,
					GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		}

		if (par4) {
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S,
					GL11.GL_CLAMP);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T,
					GL11.GL_CLAMP);
		} else {
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S,
					GL11.GL_REPEAT);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T,
					GL11.GL_REPEAT);
		}

		final int var5 = par1BufferedImage.getWidth();
		final int var6 = par1BufferedImage.getHeight();
		int[] var7 = new int[var5 * var6];
		par1BufferedImage.getRGB(0, 0, var5, var6, var7, 0, var5);

		if (options != null && options.anaglyph) {
			var7 = colorToAnaglyph(var7);
		}

		fixTransparency(var7);
		checkImageDataSize(var7.length);
		imageData.clear();
		imageData.put(var7);
		imageData.position(0).limit(var7.length);
		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA, var5, var6, 0,
				GL12.GL_BGRA, GL12.GL_UNSIGNED_INT_8_8_8_8_REV, imageData);
	}

	private int[] colorToAnaglyph(final int[] par1ArrayOfInteger) {
		final int[] var2 = new int[par1ArrayOfInteger.length];

		for (int var3 = 0; var3 < par1ArrayOfInteger.length; ++var3) {
			final int var4 = par1ArrayOfInteger[var3] >> 24 & 255;
			final int var5 = par1ArrayOfInteger[var3] >> 16 & 255;
			final int var6 = par1ArrayOfInteger[var3] >> 8 & 255;
			final int var7 = par1ArrayOfInteger[var3] & 255;
			final int var8 = (var5 * 30 + var6 * 59 + var7 * 11) / 100;
			final int var9 = (var5 * 30 + var6 * 70) / 100;
			final int var10 = (var5 * 30 + var7 * 70) / 100;
			var2[var3] = var4 << 24 | var8 << 16 | var9 << 8 | var10;
		}

		return var2;
	}

	public void createTextureFromBytes(int[] par1ArrayOfInteger,
			final int par2, final int par3, final int par4) {
		this.bindTexture(par4);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER,
				GL11.GL_NEAREST);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER,
				GL11.GL_NEAREST);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S,
				GL11.GL_REPEAT);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T,
				GL11.GL_REPEAT);

		if (options != null && options.anaglyph) {
			par1ArrayOfInteger = colorToAnaglyph(par1ArrayOfInteger);
		}

		checkImageDataSize(par1ArrayOfInteger.length);
		imageData.clear();
		imageData.put(par1ArrayOfInteger);
		imageData.position(0).limit(par1ArrayOfInteger.length);
		GL11.glTexSubImage2D(GL11.GL_TEXTURE_2D, 0, 0, 0, par2, par3,
				GL12.GL_BGRA, GL12.GL_UNSIGNED_INT_8_8_8_8_REV, imageData);
	}

	/**
	 * Deletes a single GL texture
	 */
	public void deleteTexture(final int par1) {
		textureNameToImageMap.removeObject(par1);
		GL11.glDeleteTextures(par1);
	}

	/**
	 * Takes a URL of a downloadable image and the name of the local image to be
	 * used as a fallback. If the image has been downloaded, returns the GL
	 * texture of the downloaded image, otherwise returns the GL texture of the
	 * fallback image.
	 */
	public int getTextureForDownloadableImage(final String par1Str,
			final String par2Str) {
		final ThreadDownloadImageData var3 = (ThreadDownloadImageData) urlToImageDataMap
				.get(par1Str);

		if (var3 != null && var3.image != null && !var3.textureSetupComplete) {
			if (var3.textureName < 0) {
				var3.textureName = allocateAndSetupTexture(var3.image);
			} else {
				setupTexture(var3.image, var3.textureName);
			}

			var3.textureSetupComplete = true;
		}

		return var3 != null && var3.textureName >= 0 ? var3.textureName
				: par2Str == null ? -1 : getTexture(par2Str);
	}

	/**
	 * Checks if urlToImageDataMap has image data for the given key
	 */
	public boolean hasImageData(final String par1Str) {
		return urlToImageDataMap.containsKey(par1Str);
	}

	/**
	 * Return a ThreadDownloadImageData instance for the given URL. If it does
	 * not already exist, it is created and uses the passed ImageBuffer. If it
	 * does, its reference count is incremented.
	 */
	public ThreadDownloadImageData obtainImageData(final String par1Str,
			final IImageBuffer par2IImageBuffer) {
		if (par1Str != null && par1Str.length() > 0
				&& Character.isDigit(par1Str.charAt(0))) {
			return null;
		} else {
			final ThreadDownloadImageData var3 = (ThreadDownloadImageData) urlToImageDataMap
					.get(par1Str);

			if (var3 == null) {
				urlToImageDataMap.put(par1Str, new ThreadDownloadImageData(
						par1Str, par2IImageBuffer));
			} else {
				++var3.referenceCount;
			}

			return var3;
		}
	}

	/**
	 * Decrements the reference count for a given URL, deleting the image data
	 * if the reference count hits 0
	 */
	public void releaseImageData(final String par1Str) {
		final ThreadDownloadImageData var2 = (ThreadDownloadImageData) urlToImageDataMap
				.get(par1Str);

		if (var2 != null) {
			--var2.referenceCount;

			if (var2.referenceCount == 0) {
				if (var2.textureName >= 0) {
					deleteTexture(var2.textureName);
				}

				urlToImageDataMap.remove(par1Str);
			}
		}
	}

	public void updateDynamicTextures() {
		checkInitialized();
		textureMapBlocks.updateAnimations();
		textureMapItems.updateAnimations();
		TextureAnimations.updateCustomAnimations();
	}

	/**
	 * Call setupTexture on all currently-loaded textures again to account for
	 * changes in rendering options
	 */
	public void refreshTextures() {
		Config.dbg("*** Reloading textures ***");
		Config.log("Texture pack: \""
				+ texturePack.getSelectedTexturePack().getTexturePackFileName()
				+ "\"");
		CustomSky.reset();
		TextureAnimations.reset();
		final ITexturePack var1 = texturePack.getSelectedTexturePack();
		refreshTextureMaps();
		Iterator var2 = textureNameToImageMap.getKeySet().iterator();
		BufferedImage var3;

		while (var2.hasNext()) {
			final int var4 = ((Integer) var2.next()).intValue();
			var3 = (BufferedImage) textureNameToImageMap.lookup(var4);
			setupTexture(var3, var4);
		}

		ThreadDownloadImageData var14;

		for (var2 = urlToImageDataMap.values().iterator(); var2.hasNext(); var14.textureSetupComplete = false) {
			var14 = (ThreadDownloadImageData) var2.next();
		}

		var2 = textureMap.keySet().iterator();
		String var5;

		while (var2.hasNext()) {
			var5 = (String) var2.next();

			try {
				final int var6 = ((Integer) textureMap.get(var5)).intValue();
				final boolean var7 = var5.startsWith("%blur%");

				if (var7) {
					var5 = var5.substring(6);
				}

				final boolean var8 = var5.startsWith("%clamp%");

				if (var8) {
					var5 = var5.substring(7);
				}

				final BufferedImage var9 = this.readTextureImage(var1
						.getResourceAsStream(var5));
				setupTextureExt(var9, var6, var7, var8);
			} catch (final FileNotFoundException var12) {
				;
			} catch (final Exception var13) {
				if (!"input == null!".equals(var13.getMessage())) {
					var13.printStackTrace();
				}
			}
		}

		var2 = textureContentsMap.keySet().iterator();

		while (var2.hasNext()) {
			var5 = (String) var2.next();

			try {
				var3 = this.readTextureImage(var1.getResourceAsStream(var5));
				getImageContents(var3, (int[]) textureContentsMap.get(var5));
			} catch (final FileNotFoundException var10) {
				;
			} catch (final Exception var11) {
				if (!"input == null!".equals(var11.getMessage())) {
					var11.printStackTrace();
				}
			}
		}

		Minecraft.getMinecraft().fontRenderer.readFontData();
		Minecraft.getMinecraft().standardGalacticFontRenderer.readFontData();
		TextureAnimations.update(this);
		CustomColorizer.update(this);
		CustomSky.update(this);
		RandomMobs.resetTextures();
		Config.updateTexturePackClouds();
		updateDynamicTextures();
	}

	/**
	 * Returns a BufferedImage read off the provided input stream. Args:
	 * inputStream
	 */
	private BufferedImage readTextureImage(final InputStream par1InputStream)
			throws IOException {
		final BufferedImage var2 = ImageIO.read(par1InputStream);
		par1InputStream.close();
		return var2;
	}

	public void refreshTextureMaps() {
		textureMapBlocks.refreshTextures();
		textureMapItems.refreshTextures();
		TextureUtils.update(this);
		NaturalTextures.update(this);
		ConnectedTextures.update(this);
	}

	public Icon getMissingIcon(final int par1) {
		switch (par1) {
		case 0:
			return textureMapBlocks.getMissingIcon();

		case 1:
		default:
			return textureMapItems.getMissingIcon();
		}
	}

	protected BufferedImage readTextureImage(final String var1)
			throws IOException {
		final InputStream var2 = texturePack.getSelectedTexturePack()
				.getResourceAsStream(var1);

		if (var2 == null) {
			return null;
		} else {
			final BufferedImage var3 = ImageIO.read(var2);
			var2.close();
			return var3;
		}
	}

	public TexturePackList getTexturePack() {
		return texturePack;
	}

	public void checkInitialized() {
		if (!initialized) {
			final Minecraft var1 = Config.getMinecraft();

			if (var1 != null) {
				initialized = true;
				Config.log("Texture pack: \""
						+ texturePack.getSelectedTexturePack()
								.getTexturePackFileName() + "\"");
				CustomColorizer.update(this);
				CustomSky.update(this);
				TextureAnimations.update(this);
				Config.updateTexturePackClouds();
			}
		}
	}

	public void checkImageDataSize(int var1) {
		if (imageData == null || imageData.capacity() < var1) {
			var1 = TextureUtils.ceilPowerOfTwo(var1);
			imageData = GLAllocation.createDirectIntBuffer(var1);
		}
	}

	private void fixTransparency(final int[] var1) {
		for (int var2 = 0; var2 < var1.length; ++var2) {
			final int var3 = var1[var2] >> 24 & 255;

			if (var3 == 0) {
				var1[var2] = 0;
			}
		}
	}

	public void refreshBlockTextures() {
		Config.dbg("*** Reloading block textures ***");
		textureMapBlocks.refreshTextures();
		TextureUtils.update(this);
		NaturalTextures.update(this);
		ConnectedTextures.update(this);
		updateDynamicTextures();
	}
}
