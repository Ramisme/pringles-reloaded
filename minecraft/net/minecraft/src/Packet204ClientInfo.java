package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet204ClientInfo extends Packet {
	private String language;
	private int renderDistance;
	private int chatVisisble;
	private boolean chatColours;
	private int gameDifficulty;
	private boolean showCape;

	public Packet204ClientInfo() {
	}

	public Packet204ClientInfo(final String par1Str, final int par2,
			final int par3, final boolean par4, final int par5,
			final boolean par6) {
		language = par1Str;
		renderDistance = par2;
		chatVisisble = par3;
		chatColours = par4;
		gameDifficulty = par5;
		showCape = par6;
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		language = Packet.readString(par1DataInputStream, 7);
		renderDistance = par1DataInputStream.readByte();
		final byte var2 = par1DataInputStream.readByte();
		chatVisisble = var2 & 7;
		chatColours = (var2 & 8) == 8;
		gameDifficulty = par1DataInputStream.readByte();
		showCape = par1DataInputStream.readBoolean();
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		Packet.writeString(language, par1DataOutputStream);
		par1DataOutputStream.writeByte(renderDistance);
		par1DataOutputStream.writeByte(chatVisisble
				| (chatColours ? 1 : 0) << 3);
		par1DataOutputStream.writeByte(gameDifficulty);
		par1DataOutputStream.writeBoolean(showCape);
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleClientInfo(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 7;
	}

	public String getLanguage() {
		return language;
	}

	public int getRenderDistance() {
		return renderDistance;
	}

	public int getChatVisibility() {
		return chatVisisble;
	}

	public boolean getChatColours() {
		return chatColours;
	}

	public int getDifficulty() {
		return gameDifficulty;
	}

	public boolean getShowCape() {
		return showCape;
	}

	/**
	 * only false for the abstract Packet class, all real packets return true
	 */
	@Override
	public boolean isRealPacket() {
		return true;
	}

	/**
	 * eg return packet30entity.entityId == entityId; WARNING : will throw if
	 * you compare a packet to a different packet class
	 */
	@Override
	public boolean containsSameEntityIDAs(final Packet par1Packet) {
		return true;
	}
}
