package net.minecraft.src;

public class ItemHoe extends Item {
	protected EnumToolMaterial theToolMaterial;

	public ItemHoe(final int par1, final EnumToolMaterial par2EnumToolMaterial) {
		super(par1);
		theToolMaterial = par2EnumToolMaterial;
		maxStackSize = 1;
		setMaxDamage(par2EnumToolMaterial.getMaxUses());
		setCreativeTab(CreativeTabs.tabTools);
	}

	/**
	 * Callback for item usage. If the item does something special on right
	 * clicking, he will have one of those. Return True if something happen and
	 * false if it don't. This is for ITEMS, not BLOCKS
	 */
	@Override
	public boolean onItemUse(final ItemStack par1ItemStack,
			final EntityPlayer par2EntityPlayer, final World par3World,
			final int par4, final int par5, final int par6, final int par7,
			final float par8, final float par9, final float par10) {
		if (!par2EntityPlayer.canPlayerEdit(par4, par5, par6, par7,
				par1ItemStack)) {
			return false;
		} else {
			final int var11 = par3World.getBlockId(par4, par5, par6);
			final int var12 = par3World.getBlockId(par4, par5 + 1, par6);

			if ((par7 == 0 || var12 != 0 || var11 != Block.grass.blockID)
					&& var11 != Block.dirt.blockID) {
				return false;
			} else {
				final Block var13 = Block.tilledField;
				par3World.playSoundEffect(par4 + 0.5F, par5 + 0.5F,
						par6 + 0.5F, var13.stepSound.getStepSound(),
						(var13.stepSound.getVolume() + 1.0F) / 2.0F,
						var13.stepSound.getPitch() * 0.8F);

				if (par3World.isRemote) {
					return true;
				} else {
					par3World.setBlock(par4, par5, par6, var13.blockID);
					par1ItemStack.damageItem(1, par2EntityPlayer);
					return true;
				}
			}
		}
	}

	/**
	 * Returns True is the item is renderer in full 3D when hold.
	 */
	@Override
	public boolean isFull3D() {
		return true;
	}

	/**
	 * Returns the name of the material this tool is made from as it is declared
	 * in EnumToolMaterial (meaning diamond would return "EMERALD")
	 */
	public String getMaterialName() {
		return theToolMaterial.toString();
	}
}
