package net.minecraft.src;

public abstract class EntityAgeable extends EntityCreature {
	private float field_98056_d = -1.0F;
	private float field_98057_e;

	public EntityAgeable(final World par1World) {
		super(par1World);
	}

	public abstract EntityAgeable createChild(EntityAgeable var1);

	/**
	 * Called when a player interacts with a mob. e.g. gets milk from a cow,
	 * gets into the saddle on a pig.
	 */
	@Override
	public boolean interact(final EntityPlayer par1EntityPlayer) {
		final ItemStack var2 = par1EntityPlayer.inventory.getCurrentItem();

		if (var2 != null && var2.itemID == Item.monsterPlacer.itemID
				&& !worldObj.isRemote) {
			final Class var3 = EntityList.getClassFromID(var2.getItemDamage());

			if (var3 != null && var3.isAssignableFrom(this.getClass())) {
				final EntityAgeable var4 = createChild(this);

				if (var4 != null) {
					var4.setGrowingAge(-24000);
					var4.setLocationAndAngles(posX, posY, posZ, 0.0F, 0.0F);
					worldObj.spawnEntityInWorld(var4);

					if (var2.hasDisplayName()) {
						var4.func_94058_c(var2.getDisplayName());
					}

					if (!par1EntityPlayer.capabilities.isCreativeMode) {
						--var2.stackSize;

						if (var2.stackSize <= 0) {
							par1EntityPlayer.inventory
									.setInventorySlotContents(
											par1EntityPlayer.inventory.currentItem,
											(ItemStack) null);
						}
					}
				}
			}
		}

		return super.interact(par1EntityPlayer);
	}

	@Override
	protected void entityInit() {
		super.entityInit();
		dataWatcher.addObject(12, new Integer(0));
	}

	/**
	 * The age value may be negative or positive or zero. If it's negative, it
	 * get's incremented on each tick, if it's positive, it get's decremented
	 * each tick. Don't confuse this with EntityLiving.getAge. With a negative
	 * value the Entity is considered a child.
	 */
	public int getGrowingAge() {
		return dataWatcher.getWatchableObjectInt(12);
	}

	/**
	 * The age value may be negative or positive or zero. If it's negative, it
	 * get's incremented on each tick, if it's positive, it get's decremented
	 * each tick. With a negative value the Entity is considered a child.
	 */
	public void setGrowingAge(final int par1) {
		dataWatcher.updateObject(12, Integer.valueOf(par1));
		func_98054_a(isChild());
	}

	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	@Override
	public void writeEntityToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeEntityToNBT(par1NBTTagCompound);
		par1NBTTagCompound.setInteger("Age", getGrowingAge());
	}

	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	@Override
	public void readEntityFromNBT(final NBTTagCompound par1NBTTagCompound) {
		super.readEntityFromNBT(par1NBTTagCompound);
		setGrowingAge(par1NBTTagCompound.getInteger("Age"));
	}

	/**
	 * Called frequently so the entity can update its state every tick as
	 * required. For example, zombies and skeletons use this to react to
	 * sunlight and start to burn.
	 */
	@Override
	public void onLivingUpdate() {
		super.onLivingUpdate();

		if (worldObj.isRemote) {
			func_98054_a(isChild());
		} else {
			int var1 = getGrowingAge();

			if (var1 < 0) {
				++var1;
				setGrowingAge(var1);
			} else if (var1 > 0) {
				--var1;
				setGrowingAge(var1);
			}
		}
	}

	/**
	 * If Animal, checks if the age timer is negative
	 */
	@Override
	public boolean isChild() {
		return getGrowingAge() < 0;
	}

	public void func_98054_a(final boolean par1) {
		func_98055_j(par1 ? 0.5F : 1.0F);
	}

	/**
	 * Sets the width and height of the entity. Args: width, height
	 */
	@Override
	protected final void setSize(final float par1, final float par2) {
		final boolean var3 = field_98056_d > 0.0F;
		field_98056_d = par1;
		field_98057_e = par2;

		if (!var3) {
			func_98055_j(1.0F);
		}
	}

	private void func_98055_j(final float par1) {
		super.setSize(field_98056_d * par1, field_98057_e * par1);
	}
}
