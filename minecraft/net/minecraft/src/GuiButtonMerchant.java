package net.minecraft.src;

import net.minecraft.client.Minecraft;

import org.lwjgl.opengl.GL11;

class GuiButtonMerchant extends GuiButton {
	/**
	 * If true, then next page button will face to right, if false then next
	 * page button will face to left.
	 */
	private final boolean mirrored;

	public GuiButtonMerchant(final int par1, final int par2, final int par3,
			final boolean par4) {
		super(par1, par2, par3, 12, 19, "");
		mirrored = par4;
	}

	/**
	 * Draws this button to the screen.
	 */
	@Override
	public void drawButton(final Minecraft par1Minecraft, final int par2,
			final int par3) {
		if (drawButton) {
			par1Minecraft.renderEngine.bindTexture("/gui/trading.png");
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			final boolean var4 = par2 >= xPosition && par3 >= yPosition
					&& par2 < xPosition + width && par3 < yPosition + height;
			int var5 = 0;
			int var6 = 176;

			if (!enabled) {
				var6 += width * 2;
			} else if (var4) {
				var6 += width;
			}

			if (!mirrored) {
				var5 += height;
			}

			drawTexturedModalRect(xPosition, yPosition, var6, var5, width,
					height);
		}
	}
}
