package net.minecraft.src;

import java.util.concurrent.Callable;

class CallableEffectAmplifier implements Callable {
	final PotionEffect field_102040_a;

	final EntityLiving field_102039_b;

	CallableEffectAmplifier(final EntityLiving par1EntityLiving,
			final PotionEffect par2PotionEffect) {
		field_102039_b = par1EntityLiving;
		field_102040_a = par2PotionEffect;
	}

	public String func_102038_a() {
		return field_102040_a.getAmplifier() + "";
	}

	@Override
	public Object call() {
		return func_102038_a();
	}
}
