package net.minecraft.src;

import org.lwjgl.input.Keyboard;

public class GuiRenameWorld extends GuiScreen {
	private final GuiScreen parentGuiScreen;
	private GuiTextField theGuiTextField;
	private final String worldName;

	public GuiRenameWorld(final GuiScreen par1GuiScreen, final String par2Str) {
		parentGuiScreen = par1GuiScreen;
		worldName = par2Str;
	}

	/**
	 * Called from the main game loop to update the screen.
	 */
	@Override
	public void updateScreen() {
		theGuiTextField.updateCursorCounter();
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui() {
		final StringTranslate var1 = StringTranslate.getInstance();
		Keyboard.enableRepeatEvents(true);
		buttonList.clear();
		buttonList.add(new GuiButton(0, width / 2 - 100, height / 4 + 96 + 12,
				var1.translateKey("selectWorld.renameButton")));
		buttonList.add(new GuiButton(1, width / 2 - 100, height / 4 + 120 + 12,
				var1.translateKey("gui.cancel")));
		final ISaveFormat var2 = mc.getSaveLoader();
		final WorldInfo var3 = var2.getWorldInfo(worldName);
		final String var4 = var3.getWorldName();
		theGuiTextField = new GuiTextField(fontRenderer, width / 2 - 100, 60,
				200, 20);
		theGuiTextField.setFocused(true);
		theGuiTextField.setText(var4);
	}

	/**
	 * Called when the screen is unloaded. Used to disable keyboard repeat
	 * events
	 */
	@Override
	public void onGuiClosed() {
		Keyboard.enableRepeatEvents(false);
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	@Override
	protected void actionPerformed(final GuiButton par1GuiButton) {
		if (par1GuiButton.enabled) {
			if (par1GuiButton.id == 1) {
				mc.displayGuiScreen(parentGuiScreen);
			} else if (par1GuiButton.id == 0) {
				final ISaveFormat var2 = mc.getSaveLoader();
				var2.renameWorld(worldName, theGuiTextField.getText().trim());
				mc.displayGuiScreen(parentGuiScreen);
			}
		}
	}

	/**
	 * Fired when a key is typed. This is the equivalent of
	 * KeyListener.keyTyped(KeyEvent e).
	 */
	@Override
	protected void keyTyped(final char par1, final int par2) {
		theGuiTextField.textboxKeyTyped(par1, par2);
		((GuiButton) buttonList.get(0)).enabled = theGuiTextField.getText()
				.trim().length() > 0;

		if (par1 == 13) {
			actionPerformed((GuiButton) buttonList.get(0));
		}
	}

	/**
	 * Called when the mouse is clicked.
	 */
	@Override
	protected void mouseClicked(final int par1, final int par2, final int par3) {
		super.mouseClicked(par1, par2, par3);
		theGuiTextField.mouseClicked(par1, par2, par3);
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(final int par1, final int par2, final float par3) {
		final StringTranslate var4 = StringTranslate.getInstance();
		drawDefaultBackground();
		drawCenteredString(fontRenderer,
				var4.translateKey("selectWorld.renameTitle"), width / 2,
				height / 4 - 60 + 20, 16777215);
		drawString(fontRenderer, var4.translateKey("selectWorld.enterName"),
				width / 2 - 100, 47, 10526880);
		theGuiTextField.drawTextBox();
		super.drawScreen(par1, par2, par3);
	}
}
