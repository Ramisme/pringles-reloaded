package net.minecraft.src;

import java.util.Random;

public class BlockEnchantmentTable extends BlockContainer {
	private Icon field_94461_a;
	private Icon field_94460_b;

	protected BlockEnchantmentTable(final int par1) {
		super(par1, Material.rock);
		setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.75F, 1.0F);
		setLightOpacity(0);
		setCreativeTab(CreativeTabs.tabDecorations);
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	/**
	 * A randomly called display update to be able to add particles or other
	 * items for display
	 */
	@Override
	public void randomDisplayTick(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		super.randomDisplayTick(par1World, par2, par3, par4, par5Random);

		for (int var6 = par2 - 2; var6 <= par2 + 2; ++var6) {
			for (int var7 = par4 - 2; var7 <= par4 + 2; ++var7) {
				if (var6 > par2 - 2 && var6 < par2 + 2 && var7 == par4 - 1) {
					var7 = par4 + 2;
				}

				if (par5Random.nextInt(16) == 0) {
					for (int var8 = par3; var8 <= par3 + 1; ++var8) {
						if (par1World.getBlockId(var6, var8, var7) == Block.bookShelf.blockID) {
							if (!par1World.isAirBlock((var6 - par2) / 2 + par2,
									var8, (var7 - par4) / 2 + par4)) {
								break;
							}

							par1World
									.spawnParticle("enchantmenttable",
											par2 + 0.5D, par3 + 2.0D,
											par4 + 0.5D, var6 - par2
													+ par5Random.nextFloat()
													- 0.5D, var8 - par3
													- par5Random.nextFloat()
													- 1.0F, var7 - par4
													+ par5Random.nextFloat()
													- 0.5D);
						}
					}
				}
			}
		}
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * From the specified side and block metadata retrieves the blocks texture.
	 * Args: side, metadata
	 */
	@Override
	public Icon getIcon(final int par1, final int par2) {
		return par1 == 0 ? field_94460_b : par1 == 1 ? field_94461_a
				: blockIcon;
	}

	/**
	 * Returns a new instance of a block's tile entity class. Called on placing
	 * the block.
	 */
	@Override
	public TileEntity createNewTileEntity(final World par1World) {
		return new TileEntityEnchantmentTable();
	}

	/**
	 * Called upon block activation (right click on the block.)
	 */
	@Override
	public boolean onBlockActivated(final World par1World, final int par2,
			final int par3, final int par4,
			final EntityPlayer par5EntityPlayer, final int par6,
			final float par7, final float par8, final float par9) {
		if (par1World.isRemote) {
			return true;
		} else {
			final TileEntityEnchantmentTable var10 = (TileEntityEnchantmentTable) par1World
					.getBlockTileEntity(par2, par3, par4);
			par5EntityPlayer.displayGUIEnchantment(par2, par3, par4,
					var10.func_94135_b() ? var10.func_94133_a() : null);
			return true;
		}
	}

	/**
	 * Called when the block is placed in the world.
	 */
	@Override
	public void onBlockPlacedBy(final World par1World, final int par2,
			final int par3, final int par4,
			final EntityLiving par5EntityLiving, final ItemStack par6ItemStack) {
		super.onBlockPlacedBy(par1World, par2, par3, par4, par5EntityLiving,
				par6ItemStack);

		if (par6ItemStack.hasDisplayName()) {
			((TileEntityEnchantmentTable) par1World.getBlockTileEntity(par2,
					par3, par4)).func_94134_a(par6ItemStack.getDisplayName());
		}
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		blockIcon = par1IconRegister.registerIcon("enchantment_side");
		field_94461_a = par1IconRegister.registerIcon("enchantment_top");
		field_94460_b = par1IconRegister.registerIcon("enchantment_bottom");
	}
}
