package net.minecraft.src;

import java.util.ArrayList;
import java.util.Iterator;

public class EntityFallingSand extends Entity {
	public int blockID;
	public int metadata;

	/** How long the block has been falling for. */
	public int fallTime;
	public boolean shouldDropItem;
	private boolean isBreakingAnvil;
	private boolean isAnvil;

	/** Maximum amount of damage dealt to entities hit by falling block */
	private int fallHurtMax;

	/** Actual damage dealt to entities hit by falling block */
	private float fallHurtAmount;
	public NBTTagCompound fallingBlockTileEntityData;

	public EntityFallingSand(final World par1World) {
		super(par1World);
		fallTime = 0;
		shouldDropItem = true;
		isBreakingAnvil = false;
		isAnvil = false;
		fallHurtMax = 40;
		fallHurtAmount = 2.0F;
		fallingBlockTileEntityData = null;
	}

	public EntityFallingSand(final World par1World, final double par2,
			final double par4, final double par6, final int par8) {
		this(par1World, par2, par4, par6, par8, 0);
	}

	public EntityFallingSand(final World par1World, final double par2,
			final double par4, final double par6, final int par8, final int par9) {
		super(par1World);
		fallTime = 0;
		shouldDropItem = true;
		isBreakingAnvil = false;
		isAnvil = false;
		fallHurtMax = 40;
		fallHurtAmount = 2.0F;
		fallingBlockTileEntityData = null;
		blockID = par8;
		metadata = par9;
		preventEntitySpawning = true;
		setSize(0.98F, 0.98F);
		yOffset = height / 2.0F;
		setPosition(par2, par4, par6);
		motionX = 0.0D;
		motionY = 0.0D;
		motionZ = 0.0D;
		prevPosX = par2;
		prevPosY = par4;
		prevPosZ = par6;
	}

	/**
	 * returns if this entity triggers Block.onEntityWalking on the blocks they
	 * walk on. used for spiders and wolves to prevent them from trampling crops
	 */
	@Override
	protected boolean canTriggerWalking() {
		return false;
	}

	@Override
	protected void entityInit() {
	}

	/**
	 * Returns true if other Entities should be prevented from moving through
	 * this Entity.
	 */
	@Override
	public boolean canBeCollidedWith() {
		return !isDead;
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate() {
		if (blockID == 0) {
			setDead();
		} else {
			prevPosX = posX;
			prevPosY = posY;
			prevPosZ = posZ;
			++fallTime;
			motionY -= 0.03999999910593033D;
			moveEntity(motionX, motionY, motionZ);
			motionX *= 0.9800000190734863D;
			motionY *= 0.9800000190734863D;
			motionZ *= 0.9800000190734863D;

			if (!worldObj.isRemote) {
				final int var1 = MathHelper.floor_double(posX);
				final int var2 = MathHelper.floor_double(posY);
				final int var3 = MathHelper.floor_double(posZ);

				if (fallTime == 1) {
					if (worldObj.getBlockId(var1, var2, var3) != blockID) {
						setDead();
						return;
					}

					worldObj.setBlockToAir(var1, var2, var3);
				}

				if (onGround) {
					motionX *= 0.699999988079071D;
					motionZ *= 0.699999988079071D;
					motionY *= -0.5D;

					if (worldObj.getBlockId(var1, var2, var3) != Block.pistonMoving.blockID) {
						setDead();

						if (!isBreakingAnvil
								&& worldObj.canPlaceEntityOnSide(blockID, var1,
										var2, var3, true, 1, (Entity) null,
										(ItemStack) null)
								&& !BlockSand.canFallBelow(worldObj, var1,
										var2 - 1, var3)
								&& worldObj.setBlock(var1, var2, var3, blockID,
										metadata, 3)) {
							if (Block.blocksList[blockID] instanceof BlockSand) {
								((BlockSand) Block.blocksList[blockID])
										.onFinishFalling(worldObj, var1, var2,
												var3, metadata);
							}

							if (fallingBlockTileEntityData != null
									&& Block.blocksList[blockID] instanceof ITileEntityProvider) {
								final TileEntity var4 = worldObj
										.getBlockTileEntity(var1, var2, var3);

								if (var4 != null) {
									final NBTTagCompound var5 = new NBTTagCompound();
									var4.writeToNBT(var5);
									final Iterator var6 = fallingBlockTileEntityData
											.getTags().iterator();

									while (var6.hasNext()) {
										final NBTBase var7 = (NBTBase) var6
												.next();

										if (!var7.getName().equals("x")
												&& !var7.getName().equals("y")
												&& !var7.getName().equals("z")) {
											var5.setTag(var7.getName(),
													var7.copy());
										}
									}

									var4.readFromNBT(var5);
									var4.onInventoryChanged();
								}
							}
						} else if (shouldDropItem && !isBreakingAnvil) {
							entityDropItem(
									new ItemStack(blockID, 1,
											Block.blocksList[blockID]
													.damageDropped(metadata)),
									0.0F);
						}
					}
				} else if (fallTime > 100 && !worldObj.isRemote
						&& (var2 < 1 || var2 > 256) || fallTime > 600) {
					if (shouldDropItem) {
						entityDropItem(
								new ItemStack(blockID, 1,
										Block.blocksList[blockID]
												.damageDropped(metadata)), 0.0F);
					}

					setDead();
				}
			}
		}
	}

	/**
	 * Called when the mob is falling. Calculates and applies fall damage.
	 */
	@Override
	protected void fall(final float par1) {
		if (isAnvil) {
			final int var2 = MathHelper.ceiling_float_int(par1 - 1.0F);

			if (var2 > 0) {
				final ArrayList var3 = new ArrayList(
						worldObj.getEntitiesWithinAABBExcludingEntity(this,
								boundingBox));
				final DamageSource var4 = blockID == Block.anvil.blockID ? DamageSource.anvil
						: DamageSource.fallingBlock;
				final Iterator var5 = var3.iterator();

				while (var5.hasNext()) {
					final Entity var6 = (Entity) var5.next();
					var6.attackEntityFrom(
							var4,
							Math.min(
									MathHelper.floor_float(var2
											* fallHurtAmount), fallHurtMax));
				}

				if (blockID == Block.anvil.blockID
						&& rand.nextFloat() < 0.05000000074505806D + var2 * 0.05D) {
					int var7 = metadata >> 2;
					final int var8 = metadata & 3;
					++var7;

					if (var7 > 2) {
						isBreakingAnvil = true;
					} else {
						metadata = var8 | var7 << 2;
					}
				}
			}
		}
	}

	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	@Override
	protected void writeEntityToNBT(final NBTTagCompound par1NBTTagCompound) {
		par1NBTTagCompound.setByte("Tile", (byte) blockID);
		par1NBTTagCompound.setInteger("TileID", blockID);
		par1NBTTagCompound.setByte("Data", (byte) metadata);
		par1NBTTagCompound.setByte("Time", (byte) fallTime);
		par1NBTTagCompound.setBoolean("DropItem", shouldDropItem);
		par1NBTTagCompound.setBoolean("HurtEntities", isAnvil);
		par1NBTTagCompound.setFloat("FallHurtAmount", fallHurtAmount);
		par1NBTTagCompound.setInteger("FallHurtMax", fallHurtMax);

		if (fallingBlockTileEntityData != null) {
			par1NBTTagCompound.setCompoundTag("TileEntityData",
					fallingBlockTileEntityData);
		}
	}

	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	@Override
	protected void readEntityFromNBT(final NBTTagCompound par1NBTTagCompound) {
		if (par1NBTTagCompound.hasKey("TileID")) {
			blockID = par1NBTTagCompound.getInteger("TileID");
		} else {
			blockID = par1NBTTagCompound.getByte("Tile") & 255;
		}

		metadata = par1NBTTagCompound.getByte("Data") & 255;
		fallTime = par1NBTTagCompound.getByte("Time") & 255;

		if (par1NBTTagCompound.hasKey("HurtEntities")) {
			isAnvil = par1NBTTagCompound.getBoolean("HurtEntities");
			fallHurtAmount = par1NBTTagCompound.getFloat("FallHurtAmount");
			fallHurtMax = par1NBTTagCompound.getInteger("FallHurtMax");
		} else if (blockID == Block.anvil.blockID) {
			isAnvil = true;
		}

		if (par1NBTTagCompound.hasKey("DropItem")) {
			shouldDropItem = par1NBTTagCompound.getBoolean("DropItem");
		}

		if (par1NBTTagCompound.hasKey("TileEntityData")) {
			fallingBlockTileEntityData = par1NBTTagCompound
					.getCompoundTag("TileEntityData");
		}

		if (blockID == 0) {
			blockID = Block.sand.blockID;
		}
	}

	@Override
	public float getShadowSize() {
		return 0.0F;
	}

	public World getWorld() {
		return worldObj;
	}

	public void setIsAnvil(final boolean par1) {
		isAnvil = par1;
	}

	/**
	 * Return whether this entity should be rendered as on fire.
	 */
	@Override
	public boolean canRenderOnFire() {
		return false;
	}

	@Override
	public void func_85029_a(final CrashReportCategory par1CrashReportCategory) {
		super.func_85029_a(par1CrashReportCategory);
		par1CrashReportCategory.addCrashSection("Immitating block ID",
				Integer.valueOf(blockID));
		par1CrashReportCategory.addCrashSection("Immitating block data",
				Integer.valueOf(metadata));
	}
}
