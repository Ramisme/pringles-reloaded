package net.minecraft.src;

public class EntityCreeper extends EntityMob {
	/**
	 * Time when this creeper was last in an active state (Messed up code here,
	 * probably causes creeper animation to go weird)
	 */
	private int lastActiveTime;

	/**
	 * The amount of time since the creeper was close enough to the player to
	 * ignite
	 */
	private int timeSinceIgnited;
	private int fuseTime = 30;

	/** Explosion radius for this creeper. */
	private int explosionRadius = 3;

	public EntityCreeper(final World par1World) {
		super(par1World);
		texture = "/mob/creeper.png";
		tasks.addTask(1, new EntityAISwimming(this));
		tasks.addTask(2, new EntityAICreeperSwell(this));
		tasks.addTask(3, new EntityAIAvoidEntity(this, EntityOcelot.class,
				6.0F, 0.25F, 0.3F));
		tasks.addTask(4, new EntityAIAttackOnCollide(this, 0.25F, false));
		tasks.addTask(5, new EntityAIWander(this, 0.2F));
		tasks.addTask(6, new EntityAIWatchClosest(this, EntityPlayer.class,
				8.0F));
		tasks.addTask(6, new EntityAILookIdle(this));
		targetTasks.addTask(1, new EntityAINearestAttackableTarget(this,
				EntityPlayer.class, 16.0F, 0, true));
		targetTasks.addTask(2, new EntityAIHurtByTarget(this, false));
	}

	/**
	 * Returns true if the newer Entity AI code should be run
	 */
	@Override
	public boolean isAIEnabled() {
		return true;
	}

	@Override
	public int func_82143_as() {
		return getAttackTarget() == null ? 3 : 3 + health - 1;
	}

	/**
	 * Called when the mob is falling. Calculates and applies fall damage.
	 */
	@Override
	protected void fall(final float par1) {
		super.fall(par1);
		timeSinceIgnited = (int) (timeSinceIgnited + par1 * 1.5F);

		if (timeSinceIgnited > fuseTime - 5) {
			timeSinceIgnited = fuseTime - 5;
		}
	}

	@Override
	public int getMaxHealth() {
		return 20;
	}

	@Override
	protected void entityInit() {
		super.entityInit();
		dataWatcher.addObject(16, Byte.valueOf((byte) -1));
		dataWatcher.addObject(17, Byte.valueOf((byte) 0));
	}

	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	@Override
	public void writeEntityToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeEntityToNBT(par1NBTTagCompound);

		if (dataWatcher.getWatchableObjectByte(17) == 1) {
			par1NBTTagCompound.setBoolean("powered", true);
		}

		par1NBTTagCompound.setShort("Fuse", (short) fuseTime);
		par1NBTTagCompound.setByte("ExplosionRadius", (byte) explosionRadius);
	}

	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	@Override
	public void readEntityFromNBT(final NBTTagCompound par1NBTTagCompound) {
		super.readEntityFromNBT(par1NBTTagCompound);
		dataWatcher.updateObject(17, Byte.valueOf((byte) (par1NBTTagCompound
				.getBoolean("powered") ? 1 : 0)));

		if (par1NBTTagCompound.hasKey("Fuse")) {
			fuseTime = par1NBTTagCompound.getShort("Fuse");
		}

		if (par1NBTTagCompound.hasKey("ExplosionRadius")) {
			explosionRadius = par1NBTTagCompound.getByte("ExplosionRadius");
		}
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate() {
		if (isEntityAlive()) {
			lastActiveTime = timeSinceIgnited;
			final int var1 = getCreeperState();

			if (var1 > 0 && timeSinceIgnited == 0) {
				playSound("random.fuse", 1.0F, 0.5F);
			}

			timeSinceIgnited += var1;

			if (timeSinceIgnited < 0) {
				timeSinceIgnited = 0;
			}

			if (timeSinceIgnited >= fuseTime) {
				timeSinceIgnited = fuseTime;

				if (!worldObj.isRemote) {
					final boolean var2 = worldObj.getGameRules()
							.getGameRuleBooleanValue("mobGriefing");

					if (getPowered()) {
						worldObj.createExplosion(this, posX, posY, posZ,
								explosionRadius * 2, var2);
					} else {
						worldObj.createExplosion(this, posX, posY, posZ,
								explosionRadius, var2);
					}

					setDead();
				}
			}
		}

		super.onUpdate();
	}

	/**
	 * Returns the sound this mob makes when it is hurt.
	 */
	@Override
	protected String getHurtSound() {
		return "mob.creeper.say";
	}

	/**
	 * Returns the sound this mob makes on death.
	 */
	@Override
	protected String getDeathSound() {
		return "mob.creeper.death";
	}

	/**
	 * Called when the mob's health reaches 0.
	 */
	@Override
	public void onDeath(final DamageSource par1DamageSource) {
		super.onDeath(par1DamageSource);

		if (par1DamageSource.getEntity() instanceof EntitySkeleton) {
			final int var2 = Item.record13.itemID
					+ rand.nextInt(Item.recordWait.itemID
							- Item.record13.itemID + 1);
			dropItem(var2, 1);
		}
	}

	@Override
	public boolean attackEntityAsMob(final Entity par1Entity) {
		return true;
	}

	/**
	 * Returns true if the creeper is powered by a lightning bolt.
	 */
	public boolean getPowered() {
		return dataWatcher.getWatchableObjectByte(17) == 1;
	}

	/**
	 * Params: (Float)Render tick. Returns the intensity of the creeper's flash
	 * when it is ignited.
	 */
	public float getCreeperFlashIntensity(final float par1) {
		return (lastActiveTime + (timeSinceIgnited - lastActiveTime) * par1)
				/ (fuseTime - 2);
	}

	/**
	 * Returns the item ID for the item the mob drops on death.
	 */
	@Override
	protected int getDropItemId() {
		return Item.gunpowder.itemID;
	}

	/**
	 * Returns the current state of creeper, -1 is idle, 1 is 'in fuse'
	 */
	public int getCreeperState() {
		return dataWatcher.getWatchableObjectByte(16);
	}

	/**
	 * Sets the state of creeper, -1 to idle and 1 to be 'in fuse'
	 */
	public void setCreeperState(final int par1) {
		dataWatcher.updateObject(16, Byte.valueOf((byte) par1));
	}

	/**
	 * Called when a lightning bolt hits the entity.
	 */
	@Override
	public void onStruckByLightning(
			final EntityLightningBolt par1EntityLightningBolt) {
		super.onStruckByLightning(par1EntityLightningBolt);
		dataWatcher.updateObject(17, Byte.valueOf((byte) 1));
	}
}
