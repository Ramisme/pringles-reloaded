package net.minecraft.src;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

import net.minecraft.client.Minecraft;

public class WorldClient extends World {
	/** The packets that need to be sent to the server. */
	private final NetClientHandler sendQueue;

	/** The ChunkProviderClient instance */
	private ChunkProviderClient clientChunkProvider;

	/**
	 * The hash set of entities handled by this client. Uses the entity's ID as
	 * the hash set's key.
	 */
	private final IntHashMap entityHashSet = new IntHashMap();

	/** Contains all entities for this client, both spawned and non-spawned. */
	private final Set entityList = new HashSet();

	/**
	 * Contains all entities for this client that were not spawned due to a
	 * non-present chunk. The game will attempt to spawn up to 10 pending
	 * entities with each subsequent tick until the spawn queue is empty.
	 */
	private final Set entitySpawnQueue = new HashSet();
	private final Minecraft mc = Minecraft.getMinecraft();
	private final Set previousActiveChunkSet = new HashSet();

	public WorldClient(final NetClientHandler par1NetClientHandler,
			final WorldSettings par2WorldSettings, final int par3,
			final int par4, final Profiler par5Profiler,
			final ILogAgent par6ILogAgent) {
		super(new SaveHandlerMP(), "MpServer", WorldProvider
				.getProviderForDimension(par3), par2WorldSettings,
				par5Profiler, par6ILogAgent);
		sendQueue = par1NetClientHandler;
		difficultySetting = par4;
		this.setSpawnLocation(8, 64, 8);
		mapStorage = par1NetClientHandler.mapStorage;
	}

	/**
	 * Runs a single tick for the world
	 */
	@Override
	public void tick() {
		super.tick();
		func_82738_a(getTotalWorldTime() + 1L);
		setWorldTime(getWorldTime() + 1L);
		theProfiler.startSection("reEntryProcessing");

		for (int var1 = 0; var1 < 10 && !entitySpawnQueue.isEmpty(); ++var1) {
			final Entity var2 = (Entity) entitySpawnQueue.iterator().next();
			entitySpawnQueue.remove(var2);

			if (!loadedEntityList.contains(var2)) {
				spawnEntityInWorld(var2);
			}
		}

		theProfiler.endStartSection("connection");
		sendQueue.processReadPackets();
		theProfiler.endStartSection("chunkCache");
		clientChunkProvider.unloadQueuedChunks();
		theProfiler.endStartSection("tiles");
		tickBlocksAndAmbiance();
		theProfiler.endSection();
	}

	/**
	 * Invalidates an AABB region of blocks from the receive queue, in the event
	 * that the block has been modified client-side in the intervening 80
	 * receive ticks.
	 */
	public void invalidateBlockReceiveRegion(final int par1, final int par2,
			final int par3, final int par4, final int par5, final int par6) {
	}

	/**
	 * Creates the chunk provider for this world. Called in the constructor.
	 * Retrieves provider from worldProvider?
	 */
	@Override
	protected IChunkProvider createChunkProvider() {
		clientChunkProvider = new ChunkProviderClient(this);
		return clientChunkProvider;
	}

	/**
	 * plays random cave ambient sounds and runs updateTick on random blocks
	 * within each chunk in the vacinity of a player
	 */
	@Override
	protected void tickBlocksAndAmbiance() {
		super.tickBlocksAndAmbiance();
		previousActiveChunkSet.retainAll(activeChunkSet);

		if (previousActiveChunkSet.size() == activeChunkSet.size()) {
			previousActiveChunkSet.clear();
		}

		int var1 = 0;
		final Iterator var2 = activeChunkSet.iterator();

		while (var2.hasNext()) {
			final ChunkCoordIntPair var3 = (ChunkCoordIntPair) var2.next();

			if (!previousActiveChunkSet.contains(var3)) {
				final int var4 = var3.chunkXPos * 16;
				final int var5 = var3.chunkZPos * 16;
				theProfiler.startSection("getChunk");
				final Chunk var6 = getChunkFromChunkCoords(var3.chunkXPos,
						var3.chunkZPos);
				moodSoundAndLightCheck(var4, var5, var6);
				theProfiler.endSection();
				previousActiveChunkSet.add(var3);
				++var1;

				if (var1 >= 10) {
					return;
				}
			}
		}
	}

	public void doPreChunk(final int par1, final int par2, final boolean par3) {
		if (par3) {
			clientChunkProvider.loadChunk(par1, par2);
		} else {
			clientChunkProvider.unloadChunk(par1, par2);
		}

		if (!par3) {
			markBlockRangeForRenderUpdate(par1 * 16, 0, par2 * 16,
					par1 * 16 + 15, 256, par2 * 16 + 15);
		}
	}

	/**
	 * Called to place all entities as part of a world
	 */
	@Override
	public boolean spawnEntityInWorld(final Entity par1Entity) {
		final boolean var2 = super.spawnEntityInWorld(par1Entity);
		entityList.add(par1Entity);

		if (!var2) {
			entitySpawnQueue.add(par1Entity);
		}

		return var2;
	}

	/**
	 * Schedule the entity for removal during the next tick. Marks the entity
	 * dead in anticipation.
	 */
	@Override
	public void removeEntity(final Entity par1Entity) {
		super.removeEntity(par1Entity);
		entityList.remove(par1Entity);
	}

	/**
	 * Start the skin for this entity downloading, if necessary, and increment
	 * its reference counter
	 */
	@Override
	protected void obtainEntitySkin(final Entity par1Entity) {
		super.obtainEntitySkin(par1Entity);

		if (entitySpawnQueue.contains(par1Entity)) {
			entitySpawnQueue.remove(par1Entity);
		}
	}

	/**
	 * Decrement the reference counter for this entity's skin image data
	 */
	@Override
	protected void releaseEntitySkin(final Entity par1Entity) {
		super.releaseEntitySkin(par1Entity);

		if (entityList.contains(par1Entity)) {
			if (par1Entity.isEntityAlive()) {
				entitySpawnQueue.add(par1Entity);
			} else {
				entityList.remove(par1Entity);
			}
		}
	}

	/**
	 * Add an ID to Entity mapping to entityHashSet
	 */
	public void addEntityToWorld(final int par1, final Entity par2Entity) {
		final Entity var3 = getEntityByID(par1);

		if (var3 != null) {
			removeEntity(var3);
		}

		entityList.add(par2Entity);
		par2Entity.entityId = par1;

		if (!spawnEntityInWorld(par2Entity)) {
			entitySpawnQueue.add(par2Entity);
		}

		entityHashSet.addKey(par1, par2Entity);
	}

	/**
	 * Returns the Entity with the given ID, or null if it doesn't exist in this
	 * World.
	 */
	@Override
	public Entity getEntityByID(final int par1) {
		return par1 == mc.thePlayer.entityId ? mc.thePlayer
				: (Entity) entityHashSet.lookup(par1);
	}

	public Entity removeEntityFromWorld(final int par1) {
		final Entity var2 = (Entity) entityHashSet.removeObject(par1);

		if (var2 != null) {
			entityList.remove(var2);
			removeEntity(var2);
		}

		return var2;
	}

	public boolean setBlockAndMetadataAndInvalidate(final int par1,
			final int par2, final int par3, final int par4, final int par5) {
		invalidateBlockReceiveRegion(par1, par2, par3, par1, par2, par3);
		return super.setBlock(par1, par2, par3, par4, par5, 3);
	}

	/**
	 * If on MP, sends a quitting packet.
	 */
	@Override
	public void sendQuittingDisconnectingPacket() {
		sendQueue.quitWithPacket(new Packet255KickDisconnect("Quitting"));
	}

	@Override
	public IUpdatePlayerListBox func_82735_a(
			final EntityMinecart par1EntityMinecart) {
		return new SoundUpdaterMinecart(mc.sndManager, par1EntityMinecart,
				mc.thePlayer);
	}

	/**
	 * Updates all weather states.
	 */
	@Override
	protected void updateWeather() {
		if (!provider.hasNoSky) {
			prevRainingStrength = rainingStrength;

			if (worldInfo.isRaining()) {
				rainingStrength = (float) (rainingStrength + 0.01D);
			} else {
				rainingStrength = (float) (rainingStrength - 0.01D);
			}

			if (rainingStrength < 0.0F) {
				rainingStrength = 0.0F;
			}

			if (rainingStrength > 1.0F) {
				rainingStrength = 1.0F;
			}

			prevThunderingStrength = thunderingStrength;

			if (worldInfo.isThundering()) {
				thunderingStrength = (float) (thunderingStrength + 0.01D);
			} else {
				thunderingStrength = (float) (thunderingStrength - 0.01D);
			}

			if (thunderingStrength < 0.0F) {
				thunderingStrength = 0.0F;
			}

			if (thunderingStrength > 1.0F) {
				thunderingStrength = 1.0F;
			}
		}
	}

	public void doVoidFogParticles(final int par1, final int par2,
			final int par3) {
		final byte var4 = 16;
		final Random var5 = new Random();

		for (int var6 = 0; var6 < 1000; ++var6) {
			final int var7 = par1 + rand.nextInt(var4) - rand.nextInt(var4);
			final int var8 = par2 + rand.nextInt(var4) - rand.nextInt(var4);
			final int var9 = par3 + rand.nextInt(var4) - rand.nextInt(var4);
			final int var10 = getBlockId(var7, var8, var9);

			if (var10 == 0 && rand.nextInt(8) > var8
					&& provider.getWorldHasVoidParticles()) {
				spawnParticle("depthsuspend", var7 + rand.nextFloat(), var8
						+ rand.nextFloat(), var9 + rand.nextFloat(), 0.0D,
						0.0D, 0.0D);
			} else if (var10 > 0) {
				Block.blocksList[var10].randomDisplayTick(this, var7, var8,
						var9, var5);
			}
		}
	}

	/**
	 * also releases skins.
	 */
	public void removeAllEntities() {
		loadedEntityList.removeAll(unloadedEntityList);
		int var1;
		Entity var2;
		int var3;
		int var4;

		for (var1 = 0; var1 < unloadedEntityList.size(); ++var1) {
			var2 = (Entity) unloadedEntityList.get(var1);
			var3 = var2.chunkCoordX;
			var4 = var2.chunkCoordZ;

			if (var2.addedToChunk && chunkExists(var3, var4)) {
				getChunkFromChunkCoords(var3, var4).removeEntity(var2);
			}
		}

		for (var1 = 0; var1 < unloadedEntityList.size(); ++var1) {
			releaseEntitySkin((Entity) unloadedEntityList.get(var1));
		}

		unloadedEntityList.clear();

		for (var1 = 0; var1 < loadedEntityList.size(); ++var1) {
			var2 = (Entity) loadedEntityList.get(var1);

			if (var2.ridingEntity != null) {
				if (!var2.ridingEntity.isDead
						&& var2.ridingEntity.riddenByEntity == var2) {
					continue;
				}

				var2.ridingEntity.riddenByEntity = null;
				var2.ridingEntity = null;
			}

			if (var2.isDead) {
				var3 = var2.chunkCoordX;
				var4 = var2.chunkCoordZ;

				if (var2.addedToChunk && chunkExists(var3, var4)) {
					getChunkFromChunkCoords(var3, var4).removeEntity(var2);
				}

				loadedEntityList.remove(var1--);
				releaseEntitySkin(var2);
			}
		}
	}

	/**
	 * Adds some basic stats of the world to the given crash report.
	 */
	@Override
	public CrashReportCategory addWorldInfoToCrashReport(
			final CrashReport par1CrashReport) {
		final CrashReportCategory var2 = super
				.addWorldInfoToCrashReport(par1CrashReport);
		var2.addCrashSectionCallable("Forced entities", new CallableMPL1(this));
		var2.addCrashSectionCallable("Retry entities", new CallableMPL2(this));
		return var2;
	}

	/**
	 * par8 is loudness, all pars passed to
	 * minecraftInstance.sndManager.playSound
	 */
	@Override
	public void playSound(final double par1, final double par3,
			final double par5, final String par7Str, final float par8,
			final float par9, final boolean par10) {
		float var11 = 16.0F;

		if (par8 > 1.0F) {
			var11 *= par8;
		}

		final double var12 = mc.renderViewEntity
				.getDistanceSq(par1, par3, par5);

		if (var12 < var11 * var11) {
			if (par10 && var12 > 100.0D) {
				final double var14 = Math.sqrt(var12) / 40.0D;
				mc.sndManager.func_92070_a(par7Str, (float) par1, (float) par3,
						(float) par5, par8, par9,
						(int) Math.round(var14 * 20.0D));
			} else {
				mc.sndManager.playSound(par7Str, (float) par1, (float) par3,
						(float) par5, par8, par9);
			}
		}
	}

	@Override
	public void func_92088_a(final double par1, final double par3,
			final double par5, final double par7, final double par9,
			final double par11, final NBTTagCompound par13NBTTagCompound) {
		mc.effectRenderer.addEffect(new EntityFireworkStarterFX(this, par1,
				par3, par5, par7, par9, par11, mc.effectRenderer,
				par13NBTTagCompound));
	}

	public void func_96443_a(final Scoreboard par1Scoreboard) {
		worldScoreboard = par1Scoreboard;
	}

	static Set getEntityList(final WorldClient par0WorldClient) {
		return par0WorldClient.entityList;
	}

	static Set getEntitySpawnQueue(final WorldClient par0WorldClient) {
		return par0WorldClient.entitySpawnQueue;
	}
}
