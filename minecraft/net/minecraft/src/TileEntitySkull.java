package net.minecraft.src;

public class TileEntitySkull extends TileEntity {
	/** Entity type for this skull. */
	private int skullType;

	/** The skull's rotation. */
	private int skullRotation;

	/** Extra data for this skull, used as player username by player heads */
	private String extraType = "";

	/**
	 * Writes a tile entity to NBT.
	 */
	@Override
	public void writeToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeToNBT(par1NBTTagCompound);
		par1NBTTagCompound.setByte("SkullType", (byte) (skullType & 255));
		par1NBTTagCompound.setByte("Rot", (byte) (skullRotation & 255));
		par1NBTTagCompound.setString("ExtraType", extraType);
	}

	/**
	 * Reads a tile entity from NBT.
	 */
	@Override
	public void readFromNBT(final NBTTagCompound par1NBTTagCompound) {
		super.readFromNBT(par1NBTTagCompound);
		skullType = par1NBTTagCompound.getByte("SkullType");
		skullRotation = par1NBTTagCompound.getByte("Rot");

		if (par1NBTTagCompound.hasKey("ExtraType")) {
			extraType = par1NBTTagCompound.getString("ExtraType");
		}
	}

	/**
	 * Overriden in a sign to provide the text.
	 */
	@Override
	public Packet getDescriptionPacket() {
		final NBTTagCompound var1 = new NBTTagCompound();
		writeToNBT(var1);
		return new Packet132TileEntityData(xCoord, yCoord, zCoord, 4, var1);
	}

	/**
	 * Set the entity type for the skull
	 */
	public void setSkullType(final int par1, final String par2Str) {
		skullType = par1;
		extraType = par2Str;
	}

	/**
	 * Get the entity type for the skull
	 */
	public int getSkullType() {
		return skullType;
	}

	public int func_82119_b() {
		return skullRotation;
	}

	/**
	 * Set the skull's rotation
	 */
	public void setSkullRotation(final int par1) {
		skullRotation = par1;
	}

	/**
	 * Get the extra data foor this skull, used as player username by player
	 * heads
	 */
	public String getExtraType() {
		return extraType;
	}
}
