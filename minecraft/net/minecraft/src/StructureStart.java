package net.minecraft.src;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

public abstract class StructureStart {
	/** List of all StructureComponents that are part of this structure */
	protected LinkedList components = new LinkedList();
	protected StructureBoundingBox boundingBox;

	public StructureBoundingBox getBoundingBox() {
		return boundingBox;
	}

	public LinkedList getComponents() {
		return components;
	}

	/**
	 * Keeps iterating Structure Pieces and spawning them until the checks tell
	 * it to stop
	 */
	public void generateStructure(final World par1World,
			final Random par2Random,
			final StructureBoundingBox par3StructureBoundingBox) {
		final Iterator var4 = components.iterator();

		while (var4.hasNext()) {
			final StructureComponent var5 = (StructureComponent) var4.next();

			if (var5.getBoundingBox().intersectsWith(par3StructureBoundingBox)
					&& !var5.addComponentParts(par1World, par2Random,
							par3StructureBoundingBox)) {
				var4.remove();
			}
		}
	}

	/**
	 * Calculates total bounding box based on components' bounding boxes and
	 * saves it to boundingBox
	 */
	protected void updateBoundingBox() {
		boundingBox = StructureBoundingBox.getNewBoundingBox();
		final Iterator var1 = components.iterator();

		while (var1.hasNext()) {
			final StructureComponent var2 = (StructureComponent) var1.next();
			boundingBox.expandTo(var2.getBoundingBox());
		}
	}

	/**
	 * offsets the structure Bounding Boxes up to a certain height, typically 63
	 * - 10
	 */
	protected void markAvailableHeight(final World par1World,
			final Random par2Random, final int par3) {
		final int var4 = 63 - par3;
		int var5 = boundingBox.getYSize() + 1;

		if (var5 < var4) {
			var5 += par2Random.nextInt(var4 - var5);
		}

		final int var6 = var5 - boundingBox.maxY;
		boundingBox.offset(0, var6, 0);
		final Iterator var7 = components.iterator();

		while (var7.hasNext()) {
			final StructureComponent var8 = (StructureComponent) var7.next();
			var8.getBoundingBox().offset(0, var6, 0);
		}
	}

	protected void setRandomHeight(final World par1World,
			final Random par2Random, final int par3, final int par4) {
		final int var5 = par4 - par3 + 1 - boundingBox.getYSize();
		int var10;

		if (var5 > 1) {
			var10 = par3 + par2Random.nextInt(var5);
		} else {
			var10 = par3;
		}

		final int var7 = var10 - boundingBox.minY;
		boundingBox.offset(0, var7, 0);
		final Iterator var8 = components.iterator();

		while (var8.hasNext()) {
			final StructureComponent var9 = (StructureComponent) var8.next();
			var9.getBoundingBox().offset(0, var7, 0);
		}
	}

	/**
	 * currently only defined for Villages, returns true if Village has more
	 * than 2 non-road components
	 */
	public boolean isSizeableStructure() {
		return true;
	}
}
