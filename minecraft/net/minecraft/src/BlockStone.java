package net.minecraft.src;

import java.util.Random;

public class BlockStone extends Block {
	public BlockStone(final int par1) {
		super(par1, Material.rock);
		setCreativeTab(CreativeTabs.tabBlock);
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return Block.cobblestone.blockID;
	}
}
