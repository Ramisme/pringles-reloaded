package net.minecraft.src;

import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.Bidi;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.Set;

import javax.imageio.ImageIO;

import org.lwjgl.opengl.GL11;

public class FontRenderer {
	private int textID;
	
	/** Array of width of all the characters in default.png */
	private final float[] charWidth = new float[256];

	/** the height in pixels of default text */
	public int FONT_HEIGHT = 9;
	public Random fontRandom = new Random();

	/**
	 * Array of the start/end column (in upper/lower nibble) for every glyph in
	 * the /font directory.
	 */
	private final byte[] glyphWidth = new byte[65536];

	/**
	 * Array of RGB triplets defining the 16 standard chat colors followed by 16
	 * darker version of the same colors for drop shadows.
	 */
	private final int[] colorCode = new int[32];
	private final String fontTextureName;

	/** The RenderEngine used to load and setup glyph textures. */
	private final RenderEngine renderEngine;

	/** Current X coordinate at which to draw the next character. */
	private float posX;

	/** Current Y coordinate at which to draw the next character. */
	private float posY;

	/**
	 * If true, strings should be rendered with Unicode fonts instead of the
	 * default.png font
	 */
	private boolean unicodeFlag;

	/**
	 * If true, the Unicode Bidirectional Algorithm should be run before
	 * rendering any string.
	 */
	private boolean bidiFlag;

	/** Used to specify new red value for the current color. */
	private float red;

	/** Used to specify new blue value for the current color. */
	private float blue;

	/** Used to specify new green value for the current color. */
	private float green;

	/** Used to speify new alpha value for the current color. */
	private float alpha;

	/** Text color of the currently rendering string. */
	private int textColor;

	/** Set if the "k" style (random) is active in currently rendering string */
	private boolean randomStyle = false;

	/** Set if the "l" style (bold) is active in currently rendering string */
	private boolean boldStyle = false;

	/** Set if the "o" style (italic) is active in currently rendering string */
	private boolean italicStyle = false;

	/**
	 * Set if the "n" style (underlined) is active in currently rendering string
	 */
	private boolean underlineStyle = false;

	/**
	 * Set if the "m" style (strikethrough) is active in currently rendering
	 * string
	 */
	private boolean strikethroughStyle = false;
	public GameSettings gameSettings;
	public boolean enabled = true;

	FontRenderer() {
		renderEngine = null;
		fontTextureName = null;
	}

	public FontRenderer(final GameSettings par1GameSettings,
			final String par2Str, final RenderEngine par3RenderEngine,
			final boolean par4) {
		gameSettings = par1GameSettings;
		fontTextureName = par2Str;
		textID = par3RenderEngine.getTexture(par2Str);
		renderEngine = par3RenderEngine;
		unicodeFlag = par4;
		readFontData();
		par3RenderEngine.bindTexture(par2Str);

		for (int var5 = 0; var5 < 32; ++var5) {
			final int var6 = (var5 >> 3 & 1) * 85;
			int var7 = (var5 >> 2 & 1) * 170 + var6;
			int var8 = (var5 >> 1 & 1) * 170 + var6;
			int var9 = (var5 >> 0 & 1) * 170 + var6;

			if (var5 == 6) {
				var7 += 85;
			}

			if (par1GameSettings.anaglyph) {
				final int var10 = (var7 * 30 + var8 * 59 + var9 * 11) / 100;
				final int var11 = (var7 * 30 + var8 * 70) / 100;
				final int var12 = (var7 * 30 + var9 * 70) / 100;
				var7 = var10;
				var8 = var11;
				var9 = var12;
			}

			if (var5 >= 16) {
				var7 /= 4;
				var8 /= 4;
				var9 /= 4;
			}

			colorCode[var5] = (var7 & 255) << 16 | (var8 & 255) << 8 | var9
					& 255;
		}
	}

	public void readFontData() {
		readGlyphSizes();
		readFontTexture(fontTextureName);
	}

	private void readFontTexture(String par1Str) {
		BufferedImage var2;

		try {
			par1Str = FontRenderer.fixHdFontName(par1Str);
			var2 = ImageIO.read(FontRenderer.getFontTexturePack()
					.getResourceAsStream(par1Str));
		} catch (final IOException var19) {
			throw new RuntimeException(var19);
		}

		final int var3 = var2.getWidth();
		final int var4 = var2.getHeight();
		final int var5 = var3 / 16;
		final int var6 = var4 / 16;
		final float var7 = var3 / 128.0F;
		final int[] var8 = new int[var3 * var4];
		var2.getRGB(0, 0, var3, var4, var8, 0, var3);
		int var9 = 0;

		while (var9 < 256) {
			final int var10 = var9 % 16;
			final int var11 = var9 / 16;
			int var20 = var5 - 1;

			while (true) {
				if (var20 >= 0) {
					final int var13 = var10 * var5 + var20;
					boolean var14 = true;

					for (int var15 = 0; var15 < var6 && var14; ++var15) {
						final int var16 = (var11 * var6 + var15) * var3;
						final int var17 = var8[var13 + var16];
						final int var18 = var17 >> 24 & 255;

						if (var18 > 16) {
							var14 = false;
						}
					}

					if (var14) {
						--var20;
						continue;
					}
				}

				if (var9 == 32) {
					if (var5 <= 8) {
						var20 = (int) (2.0F * var7);
					} else {
						var20 = (int) (1.5F * var7);
					}
				}

				charWidth[var9] = (var20 + 1) / var7 + 1.0F;
				++var9;
				break;
			}
		}

		readCustomCharWidths();
	}

	private void readGlyphSizes() {
		try {
			final InputStream var1 = FontRenderer.getFontTexturePack()
					.getResourceAsStream("/font/glyph_sizes.bin");
			var1.read(glyphWidth);
		} catch (final IOException var2) {
			throw new RuntimeException(var2);
		}
	}

	/**
	 * Pick how to render a single character and return the width used.
	 */
	private float renderCharAtPos(final int par1, final char par2,
			final boolean par3) {
		return par2 == 32 ? charWidth[par2]
				: par1 > 0 && !unicodeFlag ? renderDefaultChar(par1 + 32, par3)
						: renderUnicodeChar(par2, par3);
	}

	/**
	 * Render a single character with the default.png font at current
	 * (posX,posY) location...
	 */
	private float renderDefaultChar(final int par1, final boolean par2) {
		final float var3 = par1 % 16 * 8;
		final float var4 = par1 / 16 * 8;
		final float var5 = par2 ? 1.0F : 0.0F;
		renderEngine.bindTexture(fontTextureName);
		final float var6 = charWidth[par1] - 0.01F;
		GL11.glBegin(GL11.GL_TRIANGLE_STRIP);
		GL11.glTexCoord2f(var3 / 128.0F, var4 / 128.0F);
		GL11.glVertex3f(posX + var5, posY, 0.0F);
		GL11.glTexCoord2f(var3 / 128.0F, (var4 + 7.99F) / 128.0F);
		GL11.glVertex3f(posX - var5, posY + 7.99F, 0.0F);
		GL11.glTexCoord2f((var3 + var6) / 128.0F, var4 / 128.0F);
		GL11.glVertex3f(posX + var6 + var5, posY, 0.0F);
		GL11.glTexCoord2f((var3 + var6) / 128.0F, (var4 + 7.99F) / 128.0F);
		GL11.glVertex3f(posX + var6 - var5, posY + 7.99F, 0.0F);
		GL11.glEnd();
		return charWidth[par1];
	}

	/**
	 * Load one of the /font/glyph_XX.png into a new GL texture and store the
	 * texture ID in glyphTextureName array.
	 */
	private void loadGlyphTexture(final int par1) {
		final String var2 = String.format("/font/glyph_%02X.png",
				new Object[] { Integer.valueOf(par1) });
		renderEngine.bindTexture(var2);
	}

	/**
	 * Render a single Unicode character at current (posX,posY) location using
	 * one of the /font/glyph_XX.png files...
	 */
	private float renderUnicodeChar(final char par1, final boolean par2) {
		if (glyphWidth[par1] == 0) {
			return 0.0F;
		} else {
			final int var3 = par1 / 256;
			loadGlyphTexture(var3);
			final int var4 = glyphWidth[par1] >>> 4;
			final int var5 = glyphWidth[par1] & 15;
			final float var6 = var4;
			final float var7 = var5 + 1;
			final float var8 = par1 % 16 * 16 + var6;
			final float var9 = (par1 & 255) / 16 * 16;
			final float var10 = var7 - var6 - 0.02F;
			final float var11 = par2 ? 1.0F : 0.0F;
			GL11.glBegin(GL11.GL_TRIANGLE_STRIP);
			GL11.glTexCoord2f(var8 / 256.0F, var9 / 256.0F);
			GL11.glVertex3f(posX + var11, posY, 0.0F);
			GL11.glTexCoord2f(var8 / 256.0F, (var9 + 15.98F) / 256.0F);
			GL11.glVertex3f(posX - var11, posY + 7.99F, 0.0F);
			GL11.glTexCoord2f((var8 + var10) / 256.0F, var9 / 256.0F);
			GL11.glVertex3f(posX + var10 / 2.0F + var11, posY, 0.0F);
			GL11.glTexCoord2f((var8 + var10) / 256.0F, (var9 + 15.98F) / 256.0F);
			GL11.glVertex3f(posX + var10 / 2.0F - var11, posY + 7.99F, 0.0F);
			GL11.glEnd();
			return (var7 - var6) / 2.0F + 1.0F;
		}
	}

	/**
	 * Draws the specified string with a shadow.
	 */
	public int drawStringWithShadow(final String par1Str, final int par2,
			final int par3, final int par4) {
		return this.drawString(par1Str, par2, par3, par4, true);
	}

	/**
	 * Draws the specified string.
	 */
	public int drawString(final String par1Str, final int par2, final int par3,
			final int par4) {
		return !enabled ? 0 : this.drawString(par1Str, par2, par3, par4, false);
	}

	/**
	 * Draws the specified string. Args: string, x, y, color, dropShadow
	 */
	public int drawString(String par1Str, final int par2, final int par3,
			final int par4, final boolean par5) {
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, textID);
		resetStyles();

		if (bidiFlag) {
			par1Str = bidiReorder(par1Str);
		}

		int var6;

		if (par5) {
			var6 = renderString(par1Str, par2 + 1, par3 + 1, par4, true);
			var6 = Math.max(var6,
					renderString(par1Str, par2, par3, par4, false));
		} else {
			var6 = renderString(par1Str, par2, par3, par4, false);
		}

		return var6;
	}

	/**
	 * Apply Unicode Bidirectional Algorithm to string and return a new possibly
	 * reordered string for visual rendering.
	 */
	private String bidiReorder(final String par1Str) {
		if (par1Str != null
				&& Bidi.requiresBidi(par1Str.toCharArray(), 0, par1Str.length())) {
			final Bidi var2 = new Bidi(par1Str, -2);
			final byte[] var3 = new byte[var2.getRunCount()];
			final String[] var4 = new String[var3.length];
			int var5;

			for (int var6 = 0; var6 < var3.length; ++var6) {
				final int var7 = var2.getRunStart(var6);
				var5 = var2.getRunLimit(var6);
				final int var8 = var2.getRunLevel(var6);
				final String var9 = par1Str.substring(var7, var5);
				var3[var6] = (byte) var8;
				var4[var6] = var9;
			}

			final String[] var11 = var4.clone();
			Bidi.reorderVisually(var3, 0, var4, 0, var3.length);
			final StringBuilder var12 = new StringBuilder();
			var5 = 0;

			while (var5 < var4.length) {
				byte var13 = var3[var5];
				int var14 = 0;

				while (true) {
					if (var14 < var11.length) {
						if (!var11[var14].equals(var4[var5])) {
							++var14;
							continue;
						}

						var13 = var3[var14];
					}

					if ((var13 & 1) == 0) {
						var12.append(var4[var5]);
					} else {
						for (var14 = var4[var5].length() - 1; var14 >= 0; --var14) {
							char var10 = var4[var5].charAt(var14);

							if (var10 == 40) {
								var10 = 41;
							} else if (var10 == 41) {
								var10 = 40;
							}

							var12.append(var10);
						}
					}

					++var5;
					break;
				}
			}

			return var12.toString();
		} else {
			return par1Str;
		}
	}

	/**
	 * Reset all style flag fields in the class to false; called at the start of
	 * string rendering
	 */
	private void resetStyles() {
		randomStyle = false;
		boldStyle = false;
		italicStyle = false;
		underlineStyle = false;
		strikethroughStyle = false;
	}

	/**
	 * Render a single line string at the current (posX,posY) and update posX
	 */
	private void renderStringAtPos(final String par1Str, final boolean par2) {
		for (int var3 = 0; var3 < par1Str.length(); ++var3) {
			final char var4 = par1Str.charAt(var3);
			int var5;
			int var6;

			if (var4 == 167 && var3 + 1 < par1Str.length()) {
				var5 = "0123456789abcdefklmnor".indexOf(par1Str.toLowerCase()
						.charAt(var3 + 1));

				if (var5 < 16) {
					randomStyle = false;
					boldStyle = false;
					strikethroughStyle = false;
					underlineStyle = false;
					italicStyle = false;

					if (var5 < 0 || var5 > 15) {
						var5 = 15;
					}

					if (par2) {
						var5 += 16;
					}

					var6 = colorCode[var5];
					textColor = var6;
					GL11.glColor4f((var6 >> 16) / 255.0F,
							(var6 >> 8 & 255) / 255.0F, (var6 & 255) / 255.0F,
							alpha);
				} else if (var5 == 16) {
					randomStyle = true;
				} else if (var5 == 17) {
					boldStyle = true;
				} else if (var5 == 18) {
					strikethroughStyle = true;
				} else if (var5 == 19) {
					underlineStyle = true;
				} else if (var5 == 20) {
					italicStyle = true;
				} else if (var5 == 21) {
					randomStyle = false;
					boldStyle = false;
					strikethroughStyle = false;
					underlineStyle = false;
					italicStyle = false;
					GL11.glColor4f(red, blue, green, alpha);
				}

				++var3;
			} else {
				var5 = ChatAllowedCharacters.allowedCharacters.indexOf(var4);

				if (randomStyle && var5 > 0) {
					do {
						var6 = fontRandom
								.nextInt(ChatAllowedCharacters.allowedCharacters
										.length());
					} while ((int) charWidth[var5 + 32] != (int) charWidth[var6 + 32]);

					var5 = var6;
				}

				final float var7 = unicodeFlag ? 0.5F : 1.0F;
				final boolean var8 = (var5 <= 0 || unicodeFlag) && par2;

				if (var8) {
					posX -= var7;
					posY -= var7;
				}

				float var9 = renderCharAtPos(var5, var4, italicStyle);

				if (var8) {
					posX += var7;
					posY += var7;
				}

				if (boldStyle) {
					posX += var7;

					if (var8) {
						posX -= var7;
						posY -= var7;
					}

					renderCharAtPos(var5, var4, italicStyle);
					posX -= var7;

					if (var8) {
						posX += var7;
						posY += var7;
					}

					++var9;
				}

				Tessellator var10;

				if (strikethroughStyle) {
					var10 = Tessellator.instance;
					GL11.glDisable(GL11.GL_TEXTURE_2D);
					var10.startDrawingQuads();
					var10.addVertex(posX, posY + FONT_HEIGHT / 2, 0.0D);
					var10.addVertex(posX + var9, posY + FONT_HEIGHT / 2, 0.0D);
					var10.addVertex(posX + var9, posY + FONT_HEIGHT / 2 - 1.0F,
							0.0D);
					var10.addVertex(posX, posY + FONT_HEIGHT / 2 - 1.0F, 0.0D);
					var10.draw();
					GL11.glEnable(GL11.GL_TEXTURE_2D);
				}

				if (underlineStyle) {
					var10 = Tessellator.instance;
					GL11.glDisable(GL11.GL_TEXTURE_2D);
					var10.startDrawingQuads();
					final int var11 = underlineStyle ? -1 : 0;
					var10.addVertex(posX + var11, posY + FONT_HEIGHT, 0.0D);
					var10.addVertex(posX + var9, posY + FONT_HEIGHT, 0.0D);
					var10.addVertex(posX + var9, posY + FONT_HEIGHT - 1.0F,
							0.0D);
					var10.addVertex(posX + var11, posY + FONT_HEIGHT - 1.0F,
							0.0D);
					var10.draw();
					GL11.glEnable(GL11.GL_TEXTURE_2D);
				}

				posX += var9;
			}
		}
	}

	/**
	 * Render string either left or right aligned depending on bidiFlag
	 */
	private int renderStringAligned(String par1Str, int par2, final int par3,
			final int par4, final int par5, final boolean par6) {
		if (bidiFlag) {
			par1Str = bidiReorder(par1Str);
			final int var7 = getStringWidth(par1Str);
			par2 = par2 + par4 - var7;
		}

		return renderString(par1Str, par2, par3, par5, par6);
	}

	/**
	 * Render single line string by setting GL color, current (posX,posY), and
	 * calling renderStringAtPos()
	 */
	private int renderString(final String par1Str, final int par2,
			final int par3, int par4, final boolean par5) {
		if (par1Str == null) {
			return 0;
		} else {
			if ((par4 & -67108864) == 0) {
				par4 |= -16777216;
			}

			if (par5) {
				par4 = (par4 & 16579836) >> 2 | par4 & -16777216;
			}

			red = (par4 >> 16 & 255) / 255.0F;
			blue = (par4 >> 8 & 255) / 255.0F;
			green = (par4 & 255) / 255.0F;
			alpha = (par4 >> 24 & 255) / 255.0F;
			GL11.glColor4f(red, blue, green, alpha);
			posX = par2;
			posY = par3;
			renderStringAtPos(par1Str, par5);
			return (int) posX;
		}
	}

	/**
	 * Returns the width of this string. Equivalent of
	 * FontMetrics.stringWidth(String s).
	 */
	public int getStringWidth(final String par1Str) {
		if (par1Str == null) {
			return 0;
		} else {
			float var2 = 0.0F;
			boolean var3 = false;

			for (int var4 = 0; var4 < par1Str.length(); ++var4) {
				char var5 = par1Str.charAt(var4);
				float var6 = getCharWidthFloat(var5);

				if (var6 < 0.0F && var4 < par1Str.length() - 1) {
					++var4;
					var5 = par1Str.charAt(var4);

					if (var5 != 108 && var5 != 76) {
						if (var5 == 114 || var5 == 82) {
							var3 = false;
						}
					} else {
						var3 = true;
					}

					var6 = 0.0F;
				}

				var2 += var6;

				if (var3) {
					++var2;
				}
			}

			return (int) var2;
		}
	}

	/**
	 * Returns the width of this character as rendered.
	 */
	public int getCharWidth(final char par1) {
		return Math.round(getCharWidthFloat(par1));
	}

	private float getCharWidthFloat(final char var1) {
		if (var1 == 167) {
			return -1.0F;
		} else if (var1 == 32) {
			return charWidth[32];
		} else {
			final int var2 = ChatAllowedCharacters.allowedCharacters
					.indexOf(var1);

			if (var2 >= 0 && !unicodeFlag) {
				return charWidth[var2 + 32];
			} else if (glyphWidth[var1] != 0) {
				int var3 = glyphWidth[var1] >>> 4;
				int var4 = glyphWidth[var1] & 15;

				if (var4 > 7) {
					var4 = 15;
					var3 = 0;
				}

				++var4;
				return (var4 - var3) / 2 + 1;
			} else {
				return 0.0F;
			}
		}
	}

	/**
	 * Trims a string to fit a specified Width.
	 */
	public String trimStringToWidth(final String par1Str, final int par2) {
		return this.trimStringToWidth(par1Str, par2, false);
	}

	/**
	 * Trims a string to a specified width, and will reverse it if par3 is set.
	 */
	public String trimStringToWidth(final String par1Str, final int par2,
			final boolean par3) {
		final StringBuilder var4 = new StringBuilder();
		float var5 = 0.0F;
		final int var6 = par3 ? par1Str.length() - 1 : 0;
		final int var7 = par3 ? -1 : 1;
		boolean var8 = false;
		boolean var9 = false;

		for (int var10 = var6; var10 >= 0 && var10 < par1Str.length()
				&& var5 < par2; var10 += var7) {
			final char var11 = par1Str.charAt(var10);
			final float var12 = getCharWidthFloat(var11);

			if (var8) {
				var8 = false;

				if (var11 != 108 && var11 != 76) {
					if (var11 == 114 || var11 == 82) {
						var9 = false;
					}
				} else {
					var9 = true;
				}
			} else if (var12 < 0.0F) {
				var8 = true;
			} else {
				var5 += var12;

				if (var9) {
					++var5;
				}
			}

			if (var5 > par2) {
				break;
			}

			if (par3) {
				var4.insert(0, var11);
			} else {
				var4.append(var11);
			}
		}

		return var4.toString();
	}

	/**
	 * Remove all newline characters from the end of the string
	 */
	private String trimStringNewline(String par1Str) {
		while (par1Str != null && par1Str.endsWith("\n")) {
			par1Str = par1Str.substring(0, par1Str.length() - 1);
		}

		return par1Str;
	}

	/**
	 * Splits and draws a String with wordwrap (maximum length is parameter k)
	 */
	public void drawSplitString(String par1Str, final int par2, final int par3,
			final int par4, final int par5) {
		resetStyles();
		textColor = par5;
		par1Str = trimStringNewline(par1Str);
		renderSplitString(par1Str, par2, par3, par4, false);
	}

	/**
	 * Perform actual work of rendering a multi-line string with wordwrap and
	 * with darker drop shadow color if flag is set
	 */
	private void renderSplitString(final String par1Str, final int par2,
			int par3, final int par4, final boolean par5) {
		final List var6 = listFormattedStringToWidth(par1Str, par4);

		for (final Iterator var7 = var6.iterator(); var7.hasNext(); par3 += FONT_HEIGHT) {
			final String var8 = (String) var7.next();
			renderStringAligned(var8, par2, par3, par4, textColor, par5);
		}
	}

	/**
	 * Returns the width of the wordwrapped String (maximum length is parameter
	 * k)
	 */
	public int splitStringWidth(final String par1Str, final int par2) {
		return FONT_HEIGHT * listFormattedStringToWidth(par1Str, par2).size();
	}

	/**
	 * Set unicodeFlag controlling whether strings should be rendered with
	 * Unicode fonts instead of the default.png font.
	 */
	public void setUnicodeFlag(final boolean par1) {
		unicodeFlag = par1;
	}

	/**
	 * Get unicodeFlag controlling whether strings should be rendered with
	 * Unicode fonts instead of the default.png font.
	 */
	public boolean getUnicodeFlag() {
		return unicodeFlag;
	}

	/**
	 * Set bidiFlag to control if the Unicode Bidirectional Algorithm should be
	 * run before rendering any string.
	 */
	public void setBidiFlag(final boolean par1) {
		bidiFlag = par1;
	}

	/**
	 * Breaks a string into a list of pieces that will fit a specified width.
	 */
	public List listFormattedStringToWidth(final String par1Str, final int par2) {
		return Arrays.asList(wrapFormattedStringToWidth(par1Str, par2).split(
				"\n"));
	}

	/**
	 * Inserts newline and formatting into a string to wrap it within the
	 * specified width.
	 */
	String wrapFormattedStringToWidth(final String par1Str, final int par2) {
		final int var3 = sizeStringToWidth(par1Str, par2);

		if (par1Str.length() <= var3) {
			return par1Str;
		} else {
			final String var4 = par1Str.substring(0, var3);
			final char var5 = par1Str.charAt(var3);
			final boolean var6 = var5 == 32 || var5 == 10;
			final String var7 = FontRenderer.getFormatFromString(var4)
					+ par1Str.substring(var3 + (var6 ? 1 : 0));
			return var4 + "\n" + wrapFormattedStringToWidth(var7, par2);
		}
	}

	/**
	 * Determines how many characters from the string will fit into the
	 * specified width.
	 */
	private int sizeStringToWidth(final String par1Str, final int par2) {
		final int var3 = par1Str.length();
		float var4 = 0.0F;
		int var5 = 0;
		int var6 = -1;

		for (boolean var7 = false; var5 < var3; ++var5) {
			final char var8 = par1Str.charAt(var5);

			switch (var8) {
			case 10:
				--var5;
				break;

			case 167:
				if (var5 < var3 - 1) {
					++var5;
					final char var9 = par1Str.charAt(var5);

					if (var9 != 108 && var9 != 76) {
						if (var9 == 114 || var9 == 82
								|| FontRenderer.isFormatColor(var9)) {
							var7 = false;
						}
					} else {
						var7 = true;
					}
				}

				break;

			case 32:
				var6 = var5;

			default:
				var4 += getCharWidthFloat(var8);

				if (var7) {
					++var4;
				}
			}

			if (var8 == 10) {
				++var5;
				var6 = var5;
				break;
			}

			if (var4 > par2) {
				break;
			}
		}

		return var5 != var3 && var6 != -1 && var6 < var5 ? var6 : var5;
	}

	/**
	 * Checks if the char code is a hexadecimal character, used to set colour.
	 */
	private static boolean isFormatColor(final char par0) {
		return par0 >= 48 && par0 <= 57 || par0 >= 97 && par0 <= 102
				|| par0 >= 65 && par0 <= 70;
	}

	/**
	 * Checks if the char code is O-K...lLrRk-o... used to set special
	 * formatting.
	 */
	private static boolean isFormatSpecial(final char par0) {
		return par0 >= 107 && par0 <= 111 || par0 >= 75 && par0 <= 79
				|| par0 == 114 || par0 == 82;
	}

	/**
	 * Digests a string for nonprinting formatting characters then returns a
	 * string containing only that formatting.
	 */
	private static String getFormatFromString(final String par0Str) {
		String var1 = "";
		int var2 = -1;
		final int var3 = par0Str.length();

		while ((var2 = par0Str.indexOf(167, var2 + 1)) != -1) {
			if (var2 < var3 - 1) {
				final char var4 = par0Str.charAt(var2 + 1);

				if (FontRenderer.isFormatColor(var4)) {
					var1 = "\u00a7" + var4;
				} else if (FontRenderer.isFormatSpecial(var4)) {
					var1 = var1 + "\u00a7" + var4;
				}
			}
		}

		return var1;
	}

	/**
	 * Get bidiFlag that controls if the Unicode Bidirectional Algorithm should
	 * be run before rendering any string
	 */
	public boolean getBidiFlag() {
		return bidiFlag;
	}

	private void readCustomCharWidths() {
		final String var1 = FontRenderer.fixHdFontName(fontTextureName);
		final String var2 = ".png";

		if (var1.endsWith(var2)) {
			final String var3 = var1
					.substring(0, var1.length() - var2.length())
					+ ".properties";

			try {
				final InputStream var4 = FontRenderer.getFontTexturePack()
						.getResourceAsStream(var3);

				if (var4 == null) {
					return;
				}

				Config.log("Loading " + var3);
				final Properties var5 = new Properties();
				var5.load(var4);
				final Set var6 = var5.keySet();
				final Iterator var7 = var6.iterator();

				while (var7.hasNext()) {
					final String var8 = (String) var7.next();
					final String var9 = "width.";

					if (var8.startsWith(var9)) {
						final String var10 = var8.substring(var9.length());
						final int var11 = Config.parseInt(var10, -1);

						if (var11 >= 0 && var11 < charWidth.length) {
							final String var12 = var5.getProperty(var8);
							final float var13 = Config.parseFloat(var12, -1.0F);

							if (var13 >= 0.0F) {
								charWidth[var11] = var13;
							}
						}
					}
				}
			} catch (final FileNotFoundException var14) {
				;
			} catch (final IOException var15) {
				var15.printStackTrace();
			}
		}
	}

	private static String fixHdFontName(final String var0) {
		if (!Config.isCustomFonts()) {
			return var0;
		} else if (var0 == null) {
			return var0;
		} else {
			final String var1 = ".png";

			if (!var0.endsWith(var1)) {
				return var0;
			} else {
				final String var2 = var0.substring(0,
						var0.length() - var1.length());
				final String var3 = var2 + "_hd" + var1;
				return FontRenderer.getFontTexturePack().func_98138_b(var3,
						true) ? var3 : var0;
			}
		}
	}

	private static ITexturePack getFontTexturePack() {
		return Config.isCustomFonts() ? Config.getMinecraft().texturePackList
				.getSelectedTexturePack() : (ITexturePack) Config
				.getMinecraft().texturePackList.availableTexturePacks().get(0);
	}

	public int[] getColorCode() {
		return colorCode;
	}
}
