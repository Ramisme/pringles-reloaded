package net.minecraft.src;

import java.util.Random;

public class BlockSkull extends BlockContainer {
	protected BlockSkull(final int par1) {
		super(par1, Material.circuits);
		setBlockBounds(0.25F, 0.0F, 0.25F, 0.75F, 0.5F, 0.75F);
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return -1;
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	/**
	 * Updates the blocks bounds based on its current state. Args: world, x, y,
	 * z
	 */
	@Override
	public void setBlockBoundsBasedOnState(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		final int var5 = par1IBlockAccess.getBlockMetadata(par2, par3, par4) & 7;

		switch (var5) {
		case 1:
		default:
			setBlockBounds(0.25F, 0.0F, 0.25F, 0.75F, 0.5F, 0.75F);
			break;

		case 2:
			setBlockBounds(0.25F, 0.25F, 0.5F, 0.75F, 0.75F, 1.0F);
			break;

		case 3:
			setBlockBounds(0.25F, 0.25F, 0.0F, 0.75F, 0.75F, 0.5F);
			break;

		case 4:
			setBlockBounds(0.5F, 0.25F, 0.25F, 1.0F, 0.75F, 0.75F);
			break;

		case 5:
			setBlockBounds(0.0F, 0.25F, 0.25F, 0.5F, 0.75F, 0.75F);
		}
	}

	/**
	 * Returns a bounding box from the pool of bounding boxes (this means this
	 * box can change after the pool has been cleared to be reused)
	 */
	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(final World par1World,
			final int par2, final int par3, final int par4) {
		setBlockBoundsBasedOnState(par1World, par2, par3, par4);
		return super.getCollisionBoundingBoxFromPool(par1World, par2, par3,
				par4);
	}

	/**
	 * Called when the block is placed in the world.
	 */
	@Override
	public void onBlockPlacedBy(final World par1World, final int par2,
			final int par3, final int par4,
			final EntityLiving par5EntityLiving, final ItemStack par6ItemStack) {
		final int var7 = MathHelper
				.floor_double(par5EntityLiving.rotationYaw * 4.0F / 360.0F + 2.5D) & 3;
		par1World.setBlockMetadataWithNotify(par2, par3, par4, var7, 2);
	}

	/**
	 * Returns a new instance of a block's tile entity class. Called on placing
	 * the block.
	 */
	@Override
	public TileEntity createNewTileEntity(final World par1World) {
		return new TileEntitySkull();
	}

	/**
	 * only called by clickMiddleMouseButton , and passed to
	 * inventory.setCurrentItem (along with isCreative)
	 */
	@Override
	public int idPicked(final World par1World, final int par2, final int par3,
			final int par4) {
		return Item.skull.itemID;
	}

	/**
	 * Get the block's damage value (for use with pick block).
	 */
	@Override
	public int getDamageValue(final World par1World, final int par2,
			final int par3, final int par4) {
		final TileEntity var5 = par1World.getBlockTileEntity(par2, par3, par4);
		return var5 != null && var5 instanceof TileEntitySkull ? ((TileEntitySkull) var5)
				.getSkullType() : super.getDamageValue(par1World, par2, par3,
				par4);
	}

	/**
	 * Determines the damage on the item the block drops. Used in cloth and
	 * wood.
	 */
	@Override
	public int damageDropped(final int par1) {
		return par1;
	}

	/**
	 * Drops the block items with a specified chance of dropping the specified
	 * items
	 */
	@Override
	public void dropBlockAsItemWithChance(final World par1World,
			final int par2, final int par3, final int par4, final int par5,
			final float par6, final int par7) {
	}

	/**
	 * Called when the block is attempted to be harvested
	 */
	@Override
	public void onBlockHarvested(final World par1World, final int par2,
			final int par3, final int par4, int par5,
			final EntityPlayer par6EntityPlayer) {
		if (par6EntityPlayer.capabilities.isCreativeMode) {
			par5 |= 8;
			par1World.setBlockMetadataWithNotify(par2, par3, par4, par5, 4);
		}

		super.onBlockHarvested(par1World, par2, par3, par4, par5,
				par6EntityPlayer);
	}

	/**
	 * ejects contained items into the world, and notifies neighbours of an
	 * update, as appropriate
	 */
	@Override
	public void breakBlock(final World par1World, final int par2,
			final int par3, final int par4, final int par5, final int par6) {
		if (!par1World.isRemote) {
			if ((par6 & 8) == 0) {
				final ItemStack var7 = new ItemStack(Item.skull.itemID, 1,
						getDamageValue(par1World, par2, par3, par4));
				final TileEntitySkull var8 = (TileEntitySkull) par1World
						.getBlockTileEntity(par2, par3, par4);

				if (var8.getSkullType() == 3 && var8.getExtraType() != null
						&& var8.getExtraType().length() > 0) {
					var7.setTagCompound(new NBTTagCompound());
					var7.getTagCompound().setString("SkullOwner",
							var8.getExtraType());
				}

				dropBlockAsItem_do(par1World, par2, par3, par4, var7);
			}

			super.breakBlock(par1World, par2, par3, par4, par5, par6);
		}
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return Item.skull.itemID;
	}

	/**
	 * This method attempts to create a wither at the given location and skull
	 */
	public void makeWither(final World par1World, final int par2,
			final int par3, final int par4,
			final TileEntitySkull par5TileEntitySkull) {
		if (par5TileEntitySkull.getSkullType() == 1 && par3 >= 2
				&& par1World.difficultySetting > 0 && !par1World.isRemote) {
			final int var6 = Block.slowSand.blockID;
			int var7;
			EntityWither var8;
			int var9;

			for (var7 = -2; var7 <= 0; ++var7) {
				if (par1World.getBlockId(par2, par3 - 1, par4 + var7) == var6
						&& par1World
								.getBlockId(par2, par3 - 1, par4 + var7 + 1) == var6
						&& par1World
								.getBlockId(par2, par3 - 2, par4 + var7 + 1) == var6
						&& par1World
								.getBlockId(par2, par3 - 1, par4 + var7 + 2) == var6
						&& func_82528_d(par1World, par2, par3, par4 + var7, 1)
						&& func_82528_d(par1World, par2, par3, par4 + var7 + 1,
								1)
						&& func_82528_d(par1World, par2, par3, par4 + var7 + 2,
								1)) {
					par1World.setBlockMetadataWithNotify(par2, par3, par4
							+ var7, 8, 2);
					par1World.setBlockMetadataWithNotify(par2, par3, par4
							+ var7 + 1, 8, 2);
					par1World.setBlockMetadataWithNotify(par2, par3, par4
							+ var7 + 2, 8, 2);
					par1World.setBlock(par2, par3, par4 + var7, 0, 0, 2);
					par1World.setBlock(par2, par3, par4 + var7 + 1, 0, 0, 2);
					par1World.setBlock(par2, par3, par4 + var7 + 2, 0, 0, 2);
					par1World.setBlock(par2, par3 - 1, par4 + var7, 0, 0, 2);
					par1World
							.setBlock(par2, par3 - 1, par4 + var7 + 1, 0, 0, 2);
					par1World
							.setBlock(par2, par3 - 1, par4 + var7 + 2, 0, 0, 2);
					par1World
							.setBlock(par2, par3 - 2, par4 + var7 + 1, 0, 0, 2);

					if (!par1World.isRemote) {
						var8 = new EntityWither(par1World);
						var8.setLocationAndAngles(par2 + 0.5D, par3 - 1.45D,
								par4 + var7 + 1.5D, 90.0F, 0.0F);
						var8.renderYawOffset = 90.0F;
						var8.func_82206_m();
						par1World.spawnEntityInWorld(var8);
					}

					for (var9 = 0; var9 < 120; ++var9) {
						par1World.spawnParticle("snowballpoof", par2
								+ par1World.rand.nextDouble(), par3 - 2
								+ par1World.rand.nextDouble() * 3.9D, par4
								+ var7 + 1 + par1World.rand.nextDouble(), 0.0D,
								0.0D, 0.0D);
					}

					par1World.notifyBlockChange(par2, par3, par4 + var7, 0);
					par1World.notifyBlockChange(par2, par3, par4 + var7 + 1, 0);
					par1World.notifyBlockChange(par2, par3, par4 + var7 + 2, 0);
					par1World.notifyBlockChange(par2, par3 - 1, par4 + var7, 0);
					par1World.notifyBlockChange(par2, par3 - 1,
							par4 + var7 + 1, 0);
					par1World.notifyBlockChange(par2, par3 - 1,
							par4 + var7 + 2, 0);
					par1World.notifyBlockChange(par2, par3 - 2,
							par4 + var7 + 1, 0);
					return;
				}
			}

			for (var7 = -2; var7 <= 0; ++var7) {
				if (par1World.getBlockId(par2 + var7, par3 - 1, par4) == var6
						&& par1World
								.getBlockId(par2 + var7 + 1, par3 - 1, par4) == var6
						&& par1World
								.getBlockId(par2 + var7 + 1, par3 - 2, par4) == var6
						&& par1World
								.getBlockId(par2 + var7 + 2, par3 - 1, par4) == var6
						&& func_82528_d(par1World, par2 + var7, par3, par4, 1)
						&& func_82528_d(par1World, par2 + var7 + 1, par3, par4,
								1)
						&& func_82528_d(par1World, par2 + var7 + 2, par3, par4,
								1)) {
					par1World.setBlockMetadataWithNotify(par2 + var7, par3,
							par4, 8, 2);
					par1World.setBlockMetadataWithNotify(par2 + var7 + 1, par3,
							par4, 8, 2);
					par1World.setBlockMetadataWithNotify(par2 + var7 + 2, par3,
							par4, 8, 2);
					par1World.setBlock(par2 + var7, par3, par4, 0, 0, 2);
					par1World.setBlock(par2 + var7 + 1, par3, par4, 0, 0, 2);
					par1World.setBlock(par2 + var7 + 2, par3, par4, 0, 0, 2);
					par1World.setBlock(par2 + var7, par3 - 1, par4, 0, 0, 2);
					par1World
							.setBlock(par2 + var7 + 1, par3 - 1, par4, 0, 0, 2);
					par1World
							.setBlock(par2 + var7 + 2, par3 - 1, par4, 0, 0, 2);
					par1World
							.setBlock(par2 + var7 + 1, par3 - 2, par4, 0, 0, 2);

					if (!par1World.isRemote) {
						var8 = new EntityWither(par1World);
						var8.setLocationAndAngles(par2 + var7 + 1.5D,
								par3 - 1.45D, par4 + 0.5D, 0.0F, 0.0F);
						var8.func_82206_m();
						par1World.spawnEntityInWorld(var8);
					}

					for (var9 = 0; var9 < 120; ++var9) {
						par1World
								.spawnParticle("snowballpoof", par2 + var7 + 1
										+ par1World.rand.nextDouble(), par3 - 2
										+ par1World.rand.nextDouble() * 3.9D,
										par4 + par1World.rand.nextDouble(),
										0.0D, 0.0D, 0.0D);
					}

					par1World.notifyBlockChange(par2 + var7, par3, par4, 0);
					par1World.notifyBlockChange(par2 + var7 + 1, par3, par4, 0);
					par1World.notifyBlockChange(par2 + var7 + 2, par3, par4, 0);
					par1World.notifyBlockChange(par2 + var7, par3 - 1, par4, 0);
					par1World.notifyBlockChange(par2 + var7 + 1, par3 - 1,
							par4, 0);
					par1World.notifyBlockChange(par2 + var7 + 2, par3 - 1,
							par4, 0);
					par1World.notifyBlockChange(par2 + var7 + 1, par3 - 2,
							par4, 0);
					return;
				}
			}
		}
	}

	private boolean func_82528_d(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		if (par1World.getBlockId(par2, par3, par4) != blockID) {
			return false;
		} else {
			final TileEntity var6 = par1World.getBlockTileEntity(par2, par3,
					par4);
			return var6 != null && var6 instanceof TileEntitySkull ? ((TileEntitySkull) var6)
					.getSkullType() == par5 : false;
		}
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
	}

	/**
	 * From the specified side and block metadata retrieves the blocks texture.
	 * Args: side, metadata
	 */
	@Override
	public Icon getIcon(final int par1, final int par2) {
		return Block.slowSand.getBlockTextureFromSide(par1);
	}

	/**
	 * Gets the icon name of the ItemBlock corresponding to this block. Used by
	 * hoppers.
	 */
	@Override
	public String getItemIconName() {
		return ItemSkull.field_94587_a[0];
	}
}
