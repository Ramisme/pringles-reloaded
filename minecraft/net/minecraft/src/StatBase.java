package net.minecraft.src;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class StatBase {
	/** The Stat ID */
	public final int statId;

	/** The Stat name */
	private final String statName;
	public boolean isIndependent;

	/** Holds the GUID of the stat. */
	public String statGuid;
	private final IStatType type;
	private static NumberFormat numberFormat = NumberFormat
			.getIntegerInstance(Locale.US);
	public static IStatType simpleStatType = new StatTypeSimple();
	private static DecimalFormat decimalFormat = new DecimalFormat(
			"########0.00");
	public static IStatType timeStatType = new StatTypeTime();
	public static IStatType distanceStatType = new StatTypeDistance();

	public StatBase(final int par1, final String par2Str,
			final IStatType par3IStatType) {
		isIndependent = false;
		statId = par1;
		statName = par2Str;
		type = par3IStatType;
	}

	public StatBase(final int par1, final String par2Str) {
		this(par1, par2Str, StatBase.simpleStatType);
	}

	/**
	 * Initializes the current stat as independent (i.e., lacking prerequisites
	 * for being updated) and returns the current instance.
	 */
	public StatBase initIndependentStat() {
		isIndependent = true;
		return this;
	}

	/**
	 * Register the stat into StatList.
	 */
	public StatBase registerStat() {
		if (StatList.oneShotStats.containsKey(Integer.valueOf(statId))) {
			throw new RuntimeException("Duplicate stat id: \""
					+ ((StatBase) StatList.oneShotStats.get(Integer
							.valueOf(statId))).statName + "\" and \""
					+ statName + "\" at id " + statId);
		} else {
			StatList.allStats.add(this);
			StatList.oneShotStats.put(Integer.valueOf(statId), this);
			statGuid = AchievementMap.getGuid(statId);
			return this;
		}
	}

	/**
	 * Returns whether or not the StatBase-derived class is a statistic (running
	 * counter) or an achievement (one-shot).
	 */
	public boolean isAchievement() {
		return false;
	}

	public String func_75968_a(final int par1) {
		return type.format(par1);
	}

	public String getName() {
		return statName;
	}

	@Override
	public String toString() {
		return StatCollector.translateToLocal(statName);
	}

	static NumberFormat getNumberFormat() {
		return StatBase.numberFormat;
	}

	static DecimalFormat getDecimalFormat() {
		return StatBase.decimalFormat;
	}
}
