package net.minecraft.src;

import java.lang.reflect.Field;

public class ReflectorField {
	private ReflectorClass reflectorClass = null;
	private String targetFieldName = null;
	private boolean checked = false;
	private Field targetField = null;

	public ReflectorField(final ReflectorClass var1, final String var2) {
		reflectorClass = var1;
		targetFieldName = var2;
		getTargetField();
	}

	public Field getTargetField() {
		if (checked) {
			return targetField;
		} else {
			checked = true;
			final Class var1 = reflectorClass.getTargetClass();

			if (var1 == null) {
				return null;
			} else {
				try {
					targetField = var1.getDeclaredField(targetFieldName);
				} catch (final SecurityException var3) {
					var3.printStackTrace();
				} catch (final NoSuchFieldException var4) {
					Config.log("(Reflector) Field not present: "
							+ var1.getName() + "." + targetFieldName);
				}

				return targetField;
			}
		}
	}

	public Object getValue() {
		return Reflector.getFieldValue((Object) null, this);
	}

	public void setValue(final Object var1) {
		Reflector.setFieldValue((Object) null, this, var1);
	}

	public boolean exists() {
		return getTargetField() != null;
	}
}
