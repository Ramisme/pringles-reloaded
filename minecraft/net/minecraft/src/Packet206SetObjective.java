package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet206SetObjective extends Packet {
	public String objectiveName;
	public String objectiveDisplayName;

	/**
	 * 0 to create scoreboard, 1 to remove scoreboard, 2 to update display text.
	 */
	public int change;

	public Packet206SetObjective() {
	}

	public Packet206SetObjective(final ScoreObjective par1, final int par2) {
		objectiveName = par1.getName();
		objectiveDisplayName = par1.getDisplayName();
		change = par2;
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		objectiveName = Packet.readString(par1DataInputStream, 16);
		objectiveDisplayName = Packet.readString(par1DataInputStream, 32);
		change = par1DataInputStream.readByte();
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		Packet.writeString(objectiveName, par1DataOutputStream);
		Packet.writeString(objectiveDisplayName, par1DataOutputStream);
		par1DataOutputStream.writeByte(change);
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleSetObjective(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 2 + objectiveName.length() + 2 + objectiveDisplayName.length()
				+ 1;
	}
}
