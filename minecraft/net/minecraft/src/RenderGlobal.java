package net.minecraft.src;

import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import net.minecraft.client.Minecraft;

import org.lwjgl.BufferUtils;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.ARBOcclusionQuery;
import org.lwjgl.opengl.GL11;

public class RenderGlobal implements IWorldAccess {
	public List tileEntities = new ArrayList();
	public WorldClient theWorld;

	/** The RenderEngine instance used by RenderGlobal */
	public final RenderEngine renderEngine;
	public CompactArrayList worldRenderersToUpdate = new CompactArrayList(100,
			0.8F);
	private WorldRenderer[] sortedWorldRenderers;
	private WorldRenderer[] worldRenderers;
	private int renderChunksWide;
	private int renderChunksTall;
	private int renderChunksDeep;

	/** OpenGL render lists base */
	private final int glRenderListBase;

	/** A reference to the Minecraft object. */
	public Minecraft mc;

	/** Global render blocks */
	public RenderBlocks globalRenderBlocks;

	/** OpenGL occlusion query base */
	private IntBuffer glOcclusionQueryBase;

	/** Is occlusion testing enabled */
	private boolean occlusionEnabled = false;

	/**
	 * counts the cloud render updates. Used with mod to stagger some updates
	 */
	private int cloudTickCounter = 0;

	/** The star GL Call list */
	private final int starGLCallList;

	/** OpenGL sky list */
	private final int glSkyList;

	/** OpenGL sky list 2 */
	private final int glSkyList2;

	/** Minimum block X */
	private int minBlockX;

	/** Minimum block Y */
	private int minBlockY;

	/** Minimum block Z */
	private int minBlockZ;

	/** Maximum block X */
	private int maxBlockX;

	/** Maximum block Y */
	private int maxBlockY;

	/** Maximum block Z */
	private int maxBlockZ;

	/**
	 * Stores blocks currently being broken. Key is entity ID of the thing doing
	 * the breaking. Value is a DestroyBlockProgress
	 */
	public Map damagedBlocks = new HashMap();
	private Icon[] destroyBlockIcons;
	private int renderDistance = -1;

	/** Render entities startup counter (init value=2) */
	private int renderEntitiesStartupCounter = 2;

	/** Count entities total */
	private int countEntitiesTotal;

	/** Count entities rendered */
	private int countEntitiesRendered;

	/** Count entities hidden */
	private int countEntitiesHidden;

	/** Dummy buffer (50k) not used */
	int[] dummyBuf50k = new int[50000];

	/** Occlusion query result */
	IntBuffer occlusionResult = GLAllocation.createDirectIntBuffer(64);

	/** How many renderers are loaded this frame that try to be rendered */
	private int renderersLoaded;

	/** How many renderers are being clipped by the frustrum this frame */
	private int renderersBeingClipped;

	/** How many renderers are being occluded this frame */
	private int renderersBeingOccluded;

	/** How many renderers are actually being rendered this frame */
	private int renderersBeingRendered;

	/**
	 * How many renderers are skipping rendering due to not having a render pass
	 * this frame
	 */
	private int renderersSkippingRenderPass;

	/** World renderers check index */
	private int worldRenderersCheckIndex;
	private final IntBuffer glListBuffer = BufferUtils.createIntBuffer(65536);

	/**
	 * Previous x position when the renderers were sorted. (Once the distance
	 * moves more than 4 units they will be resorted)
	 */
	double prevSortX = -9999.0D;

	/**
	 * Previous y position when the renderers were sorted. (Once the distance
	 * moves more than 4 units they will be resorted)
	 */
	double prevSortY = -9999.0D;

	/**
	 * Previous Z position when the renderers were sorted. (Once the distance
	 * moves more than 4 units they will be resorted)
	 */
	double prevSortZ = -9999.0D;

	/**
	 * The offset used to determine if a renderer is one of the sixteenth that
	 * are being updated this frame
	 */
	int frustumCheckOffset = 0;
	double prevReposX;
	double prevReposY;
	double prevReposZ;
	public Entity renderedEntity;
	private long lastMovedTime = System.currentTimeMillis();
	private long lastActionTime = System.currentTimeMillis();

	public RenderGlobal(final Minecraft par1Minecraft,
			final RenderEngine par2RenderEngine) {
		mc = par1Minecraft;
		renderEngine = par2RenderEngine;
		final byte var3 = 65;
		final byte var4 = 16;
		glRenderListBase = GLAllocation.generateDisplayLists(var3 * var3 * var4
				* 3);
		occlusionEnabled = OpenGlCapsChecker.checkARBOcclusion();

		if (occlusionEnabled) {
			occlusionResult.clear();
			glOcclusionQueryBase = GLAllocation.createDirectIntBuffer(var3
					* var3 * var4);
			glOcclusionQueryBase.clear();
			glOcclusionQueryBase.position(0);
			glOcclusionQueryBase.limit(var3 * var3 * var4);
			ARBOcclusionQuery.glGenQueriesARB(glOcclusionQueryBase);
		}

		starGLCallList = GLAllocation.generateDisplayLists(3);
		GL11.glPushMatrix();
		GL11.glNewList(starGLCallList, GL11.GL_COMPILE);
		renderStars();
		GL11.glEndList();
		GL11.glPopMatrix();
		final Tessellator var5 = Tessellator.instance;
		glSkyList = starGLCallList + 1;
		GL11.glNewList(glSkyList, GL11.GL_COMPILE);
		final byte var6 = 64;
		final int var7 = 256 / var6 + 2;
		float var8 = 16.0F;
		int var9;
		int var10;

		for (var9 = -var6 * var7; var9 <= var6 * var7; var9 += var6) {
			for (var10 = -var6 * var7; var10 <= var6 * var7; var10 += var6) {
				var5.startDrawingQuads();
				var5.addVertex(var9 + 0, var8, var10 + 0);
				var5.addVertex(var9 + var6, var8, var10 + 0);
				var5.addVertex(var9 + var6, var8, var10 + var6);
				var5.addVertex(var9 + 0, var8, var10 + var6);
				var5.draw();
			}
		}

		GL11.glEndList();
		glSkyList2 = starGLCallList + 2;
		GL11.glNewList(glSkyList2, GL11.GL_COMPILE);
		var8 = -16.0F;
		var5.startDrawingQuads();

		for (var9 = -var6 * var7; var9 <= var6 * var7; var9 += var6) {
			for (var10 = -var6 * var7; var10 <= var6 * var7; var10 += var6) {
				var5.addVertex(var9 + var6, var8, var10 + 0);
				var5.addVertex(var9 + 0, var8, var10 + 0);
				var5.addVertex(var9 + 0, var8, var10 + var6);
				var5.addVertex(var9 + var6, var8, var10 + var6);
			}
		}

		var5.draw();
		GL11.glEndList();
		renderEngine.updateDynamicTextures();
	}

	private void renderStars() {
		final Random var1 = new Random(10842L);
		final Tessellator var2 = Tessellator.instance;
		var2.startDrawingQuads();

		for (int var3 = 0; var3 < 1500; ++var3) {
			double var4 = var1.nextFloat() * 2.0F - 1.0F;
			double var6 = var1.nextFloat() * 2.0F - 1.0F;
			double var8 = var1.nextFloat() * 2.0F - 1.0F;
			final double var10 = 0.15F + var1.nextFloat() * 0.1F;
			double var12 = var4 * var4 + var6 * var6 + var8 * var8;

			if (var12 < 1.0D && var12 > 0.01D) {
				var12 = 1.0D / Math.sqrt(var12);
				var4 *= var12;
				var6 *= var12;
				var8 *= var12;
				final double var14 = var4 * 100.0D;
				final double var16 = var6 * 100.0D;
				final double var18 = var8 * 100.0D;
				final double var20 = Math.atan2(var4, var8);
				final double var22 = Math.sin(var20);
				final double var24 = Math.cos(var20);
				final double var26 = Math.atan2(
						Math.sqrt(var4 * var4 + var8 * var8), var6);
				final double var28 = Math.sin(var26);
				final double var30 = Math.cos(var26);
				final double var32 = var1.nextDouble() * Math.PI * 2.0D;
				final double var34 = Math.sin(var32);
				final double var36 = Math.cos(var32);

				for (int var38 = 0; var38 < 4; ++var38) {
					final double var39 = 0.0D;
					final double var41 = ((var38 & 2) - 1) * var10;
					final double var43 = ((var38 + 1 & 2) - 1) * var10;
					final double var45 = var41 * var36 - var43 * var34;
					final double var47 = var43 * var36 + var41 * var34;
					final double var49 = var45 * var28 + var39 * var30;
					final double var51 = var39 * var28 - var45 * var30;
					final double var53 = var51 * var22 - var47 * var24;
					final double var55 = var47 * var22 + var51 * var24;
					var2.addVertex(var14 + var53, var16 + var49, var18 + var55);
				}
			}
		}

		var2.draw();
	}

	/**
	 * set null to clear
	 */
	public void setWorldAndLoadRenderers(final WorldClient par1WorldClient) {
		if (theWorld != null) {
			theWorld.removeWorldAccess(this);
		}

		prevSortX = -9999.0D;
		prevSortY = -9999.0D;
		prevSortZ = -9999.0D;
		RenderManager.instance.set(par1WorldClient);
		theWorld = par1WorldClient;
		globalRenderBlocks = new RenderBlocks(par1WorldClient);

		if (par1WorldClient != null) {
			par1WorldClient.addWorldAccess(this);
			loadRenderers();
		}
	}

	/**
	 * Loads all the renderers and sets up the basic settings usage
	 */
	public void loadRenderers() {
		if (theWorld != null) {
			Block.leaves.setGraphicsLevel(Config.isTreesFancy());
			renderDistance = mc.gameSettings.renderDistance;
			int var1;

			if (worldRenderers != null) {
				for (var1 = 0; var1 < worldRenderers.length; ++var1) {
					worldRenderers[var1].stopRendering();
				}
			}

			var1 = 64 << 3 - renderDistance;
			final short var2 = 512;
			var1 = 2 * mc.gameSettings.ofRenderDistanceFine;

			if (Config.isLoadChunksFar() && var1 < var2) {
				var1 = var2;
			}

			var1 += Config.getPreloadedChunks() * 2 * 16;
			short var3 = 400;

			if (mc.gameSettings.ofRenderDistanceFine > 256) {
				var3 = 1024;
			}

			if (var1 > var3) {
				var1 = var3;
			}

			prevReposX = -9999.0D;
			prevReposY = -9999.0D;
			prevReposZ = -9999.0D;
			renderChunksWide = var1 / 16 + 1;
			renderChunksTall = 16;
			renderChunksDeep = var1 / 16 + 1;
			worldRenderers = new WorldRenderer[renderChunksWide
					* renderChunksTall * renderChunksDeep];
			sortedWorldRenderers = new WorldRenderer[renderChunksWide
					* renderChunksTall * renderChunksDeep];
			int var4 = 0;
			int var5 = 0;
			minBlockX = 0;
			minBlockY = 0;
			minBlockZ = 0;
			maxBlockX = renderChunksWide;
			maxBlockY = renderChunksTall;
			maxBlockZ = renderChunksDeep;
			int var7;

			for (var7 = 0; var7 < worldRenderersToUpdate.size(); ++var7) {
				final WorldRenderer var8 = (WorldRenderer) worldRenderersToUpdate
						.get(var7);

				if (var8 != null) {
					var8.needsUpdate = false;
				}
			}

			worldRenderersToUpdate.clear();
			tileEntities.clear();

			for (var7 = 0; var7 < renderChunksWide; ++var7) {
				for (int var12 = 0; var12 < renderChunksTall; ++var12) {
					for (int var9 = 0; var9 < renderChunksDeep; ++var9) {
						final int var10 = (var9 * renderChunksTall + var12)
								* renderChunksWide + var7;
						worldRenderers[var10] = new WorldRenderer(theWorld,
								tileEntities, var7 * 16, var12 * 16, var9 * 16,
								glRenderListBase + var4);

						if (occlusionEnabled) {
							worldRenderers[var10].glOcclusionQuery = glOcclusionQueryBase
									.get(var5);
						}

						worldRenderers[var10].isWaitingOnOcclusionQuery = false;
						worldRenderers[var10].isVisible = true;
						worldRenderers[var10].isInFrustum = false;
						worldRenderers[var10].chunkIndex = var5++;
						sortedWorldRenderers[var10] = worldRenderers[var10];

						if (theWorld.chunkExists(var7, var9)) {
							worldRenderers[var10].markDirty();
							worldRenderersToUpdate.add(worldRenderers[var10]);
						}

						var4 += 3;
					}
				}
			}

			if (theWorld != null) {
				Object var11 = mc.renderViewEntity;

				if (var11 == null) {
					var11 = mc.thePlayer;
				}

				if (var11 != null) {
					markRenderersForNewPosition(
							MathHelper
									.floor_double(((EntityLiving) var11).posX),
							MathHelper
									.floor_double(((EntityLiving) var11).posY),
							MathHelper
									.floor_double(((EntityLiving) var11).posZ));
					Arrays.sort(sortedWorldRenderers, new EntitySorter(
							(Entity) var11));
				}
			}

			renderEntitiesStartupCounter = 2;
		}
	}

	/**
	 * Renders all entities within range and within the frustrum. Args: pos,
	 * frustrum, partialTickTime
	 */
	public void renderEntities(final Vec3 par1Vec3, final ICamera par2ICamera,
			final float par3) {
		if (renderEntitiesStartupCounter > 0) {
			--renderEntitiesStartupCounter;
		} else {
			theWorld.theProfiler.startSection("prepare");
			TileEntityRenderer.instance.cacheActiveRenderInfo(theWorld,
					renderEngine, mc.fontRenderer, mc.renderViewEntity, par3);
			RenderManager.instance.cacheActiveRenderInfo(theWorld,
					renderEngine, mc.fontRenderer, mc.renderViewEntity,
					mc.pointedEntityLiving, mc.gameSettings, par3);
			countEntitiesTotal = 0;
			countEntitiesRendered = 0;
			countEntitiesHidden = 0;
			final EntityLiving var4 = mc.renderViewEntity;
			RenderManager.renderPosX = var4.lastTickPosX
					+ (var4.posX - var4.lastTickPosX) * par3;
			RenderManager.renderPosY = var4.lastTickPosY
					+ (var4.posY - var4.lastTickPosY) * par3;
			RenderManager.renderPosZ = var4.lastTickPosZ
					+ (var4.posZ - var4.lastTickPosZ) * par3;
			TileEntityRenderer.staticPlayerX = var4.lastTickPosX
					+ (var4.posX - var4.lastTickPosX) * par3;
			TileEntityRenderer.staticPlayerY = var4.lastTickPosY
					+ (var4.posY - var4.lastTickPosY) * par3;
			TileEntityRenderer.staticPlayerZ = var4.lastTickPosZ
					+ (var4.posZ - var4.lastTickPosZ) * par3;
			mc.entityRenderer.enableLightmap(par3);
			theWorld.theProfiler.endStartSection("global");
			final List var5 = theWorld.getLoadedEntityList();
			countEntitiesTotal = var5.size();
			int var6;
			Entity var7;

			for (var6 = 0; var6 < theWorld.weatherEffects.size(); ++var6) {
				var7 = (Entity) theWorld.weatherEffects.get(var6);
				++countEntitiesRendered;

				if (var7.isInRangeToRenderVec3D(par1Vec3)) {
					RenderManager.instance.renderEntity(var7, par3);
				}
			}

			theWorld.theProfiler.endStartSection("entities");
			final boolean var8 = mc.gameSettings.fancyGraphics;
			mc.gameSettings.fancyGraphics = Config.isDroppedItemsFancy();

			for (var6 = 0; var6 < var5.size(); ++var6) {
				var7 = (Entity) var5.get(var6);

				if (var7.isInRangeToRenderVec3D(par1Vec3)
						&& (var7.ignoreFrustumCheck
								|| par2ICamera
										.isBoundingBoxInFrustum(var7.boundingBox) || var7.riddenByEntity == mc.thePlayer)
						&& (var7 != mc.renderViewEntity
								|| mc.gameSettings.thirdPersonView != 0 || mc.renderViewEntity
									.isPlayerSleeping())
						&& theWorld.blockExists(
								MathHelper.floor_double(var7.posX), 0,
								MathHelper.floor_double(var7.posZ))) {
					++countEntitiesRendered;

					if (var7.getClass() == EntityItemFrame.class) {
						var7.renderDistanceWeight = 0.06D;
					}

					renderedEntity = var7;
					RenderManager.instance.renderEntity(var7, par3);
					renderedEntity = null;
				}
			}

			mc.gameSettings.fancyGraphics = var8;
			theWorld.theProfiler.endStartSection("tileentities");
			RenderHelper.enableStandardItemLighting();

			for (var6 = 0; var6 < tileEntities.size(); ++var6) {
				final TileEntity var9 = (TileEntity) tileEntities.get(var6);
				final Class var10 = var9.getClass();

				if (var10 == TileEntitySign.class && !Config.zoomMode) {
					final EntityClientPlayerMP var11 = mc.thePlayer;
					final double var12 = var9.getDistanceFrom(var11.posX,
							var11.posY, var11.posZ);

					if (var12 > 256.0D) {
						final FontRenderer var14 = TileEntityRenderer.instance
								.getFontRenderer();
						var14.enabled = false;
						TileEntityRenderer.instance
								.renderTileEntity(var9, par3);
						var14.enabled = true;
						continue;
					}
				}

				if (var10 == TileEntityChest.class) {
					final int var15 = theWorld.getBlockId(var9.xCoord,
							var9.yCoord, var9.zCoord);
					final Block var16 = Block.blocksList[var15];

					if (!(var16 instanceof BlockChest)) {
						continue;
					}
				}

				TileEntityRenderer.instance.renderTileEntity(var9, par3);
			}

			mc.entityRenderer.disableLightmap(par3);
			theWorld.theProfiler.endSection();
		}
	}

	/**
	 * Gets the render info for use on the Debug screen
	 */
	public String getDebugInfoRenders() {
		return "C: " + renderersBeingRendered + "/" + renderersLoaded + ". F: "
				+ renderersBeingClipped + ", O: " + renderersBeingOccluded
				+ ", E: " + renderersSkippingRenderPass;
	}

	/**
	 * Gets the entities info for use on the Debug screen
	 */
	public String getDebugInfoEntities() {
		return "E: "
				+ countEntitiesRendered
				+ "/"
				+ countEntitiesTotal
				+ ". B: "
				+ countEntitiesHidden
				+ ", I: "
				+ (countEntitiesTotal - countEntitiesHidden - countEntitiesRendered)
				+ ", " + Config.getVersion();
	}

	/**
	 * Goes through all the renderers setting new positions on them and those
	 * that have their position changed are adding to be updated
	 */
	private void markRenderersForNewPosition(int par1, int par2, int par3) {
		par1 -= 8;
		par2 -= 8;
		par3 -= 8;
		minBlockX = Integer.MAX_VALUE;
		minBlockY = Integer.MAX_VALUE;
		minBlockZ = Integer.MAX_VALUE;
		maxBlockX = Integer.MIN_VALUE;
		maxBlockY = Integer.MIN_VALUE;
		maxBlockZ = Integer.MIN_VALUE;
		final int var4 = renderChunksWide * 16;
		final int var5 = var4 / 2;

		for (int var6 = 0; var6 < renderChunksWide; ++var6) {
			int var7 = var6 * 16;
			int var8 = var7 + var5 - par1;

			if (var8 < 0) {
				var8 -= var4 - 1;
			}

			var8 /= var4;
			var7 -= var8 * var4;

			if (var7 < minBlockX) {
				minBlockX = var7;
			}

			if (var7 > maxBlockX) {
				maxBlockX = var7;
			}

			for (int var9 = 0; var9 < renderChunksDeep; ++var9) {
				int var10 = var9 * 16;
				int var11 = var10 + var5 - par3;

				if (var11 < 0) {
					var11 -= var4 - 1;
				}

				var11 /= var4;
				var10 -= var11 * var4;

				if (var10 < minBlockZ) {
					minBlockZ = var10;
				}

				if (var10 > maxBlockZ) {
					maxBlockZ = var10;
				}

				for (int var12 = 0; var12 < renderChunksTall; ++var12) {
					final int var13 = var12 * 16;

					if (var13 < minBlockY) {
						minBlockY = var13;
					}

					if (var13 > maxBlockY) {
						maxBlockY = var13;
					}

					final WorldRenderer var14 = worldRenderers[(var9
							* renderChunksTall + var12)
							* renderChunksWide + var6];
					final boolean var15 = var14.needsUpdate;
					var14.setPosition(var7, var13, var10);

					if (!var15 && var14.needsUpdate) {
						worldRenderersToUpdate.add(var14);
					}
				}
			}
		}
	}

	/**
	 * Sorts all renderers based on the passed in entity. Args: entityLiving,
	 * renderPass, partialTickTime
	 */
	public int sortAndRender(final EntityLiving par1EntityLiving,
			final int par2, final double par3) {
		final Profiler var5 = theWorld.theProfiler;
		var5.startSection("sortchunks");

		if (worldRenderersToUpdate.size() < 10) {
			final byte var6 = 10;

			for (int var7 = 0; var7 < var6; ++var7) {
				worldRenderersCheckIndex = (worldRenderersCheckIndex + 1)
						% worldRenderers.length;
				final WorldRenderer var8 = worldRenderers[worldRenderersCheckIndex];

				if (var8.needsUpdate && !worldRenderersToUpdate.contains(var8)) {
					worldRenderersToUpdate.add(var8);
				}
			}
		}

		if (mc.gameSettings.renderDistance != renderDistance
				&& !Config.isLoadChunksFar()) {
			loadRenderers();
		}

		if (par2 == 0) {
			renderersLoaded = 0;
			renderersBeingClipped = 0;
			renderersBeingOccluded = 0;
			renderersBeingRendered = 0;
			renderersSkippingRenderPass = 0;
		}

		final double var40 = par1EntityLiving.lastTickPosX
				+ (par1EntityLiving.posX - par1EntityLiving.lastTickPosX)
				* par3;
		final double var41 = par1EntityLiving.lastTickPosY
				+ (par1EntityLiving.posY - par1EntityLiving.lastTickPosY)
				* par3;
		final double var10 = par1EntityLiving.lastTickPosZ
				+ (par1EntityLiving.posZ - par1EntityLiving.lastTickPosZ)
				* par3;
		final double var12 = par1EntityLiving.posX - prevSortX;
		final double var14 = par1EntityLiving.posY - prevSortY;
		final double var16 = par1EntityLiving.posZ - prevSortZ;
		final double var18 = var12 * var12 + var14 * var14 + var16 * var16;
		int var20;

		if (var18 > 16.0D) {
			prevSortX = par1EntityLiving.posX;
			prevSortY = par1EntityLiving.posY;
			prevSortZ = par1EntityLiving.posZ;
			var20 = Config.getPreloadedChunks() * 16;
			final double var21 = par1EntityLiving.posX - prevReposX;
			final double var23 = par1EntityLiving.posY - prevReposY;
			final double var25 = par1EntityLiving.posZ - prevReposZ;
			final double var27 = var21 * var21 + var23 * var23 + var25 * var25;

			if (var27 > var20 * var20 + 16.0D) {
				prevReposX = par1EntityLiving.posX;
				prevReposY = par1EntityLiving.posY;
				prevReposZ = par1EntityLiving.posZ;
				markRenderersForNewPosition(
						MathHelper.floor_double(par1EntityLiving.posX),
						MathHelper.floor_double(par1EntityLiving.posY),
						MathHelper.floor_double(par1EntityLiving.posZ));
			}

			Arrays.sort(sortedWorldRenderers,
					new EntitySorter(par1EntityLiving));
			final int var29 = (int) par1EntityLiving.posX;
			final int var30 = (int) par1EntityLiving.posZ;
			final short var31 = 2000;

			if (Math.abs(var29 - WorldRenderer.globalChunkOffsetX) > var31
					|| Math.abs(var30 - WorldRenderer.globalChunkOffsetZ) > var31) {
				WorldRenderer.globalChunkOffsetX = var29;
				WorldRenderer.globalChunkOffsetZ = var30;
				loadRenderers();
			}
		}

		RenderHelper.disableStandardItemLighting();

		if (mc.gameSettings.ofSmoothFps && par2 == 0) {
			GL11.glFinish();
		}

		final byte var42 = 0;
		if (occlusionEnabled && mc.gameSettings.advancedOpengl
				&& !mc.gameSettings.anaglyph && par2 == 0) {
			final byte var22 = 0;
			final byte var43 = 20;
			checkOcclusionQueryResult(var22, var43, par1EntityLiving.posX,
					par1EntityLiving.posY, par1EntityLiving.posZ);
			int var24;

			for (var24 = var22; var24 < var43; ++var24) {
				sortedWorldRenderers[var24].isVisible = true;
			}

			var5.endStartSection("render");
			var20 = var42 + renderSortedRenderers(var22, var43, par2, par3);
			var24 = var43;
			int var45 = 0;
			final byte var26 = 40;
			int var28;

			for (final int var46 = renderChunksWide; var24 < sortedWorldRenderers.length; var20 += renderSortedRenderers(
					var28, var24, par2, par3)) {
				var5.endStartSection("occ");
				var28 = var24;

				if (var45 < var46) {
					++var45;
				} else {
					--var45;
				}

				var24 += var45 * var26;

				if (var24 <= var28) {
					var24 = var28 + 10;
				}

				if (var24 > sortedWorldRenderers.length) {
					var24 = sortedWorldRenderers.length;
				}

				GL11.glDisable(GL11.GL_TEXTURE_2D);
				GL11.glDisable(GL11.GL_LIGHTING);
				GL11.glDisable(GL11.GL_ALPHA_TEST);
				GL11.glDisable(GL11.GL_FOG);
				GL11.glColorMask(false, false, false, false);
				GL11.glDepthMask(false);
				var5.startSection("check");
				checkOcclusionQueryResult(var28, var24, par1EntityLiving.posX,
						par1EntityLiving.posY, par1EntityLiving.posZ);
				var5.endSection();
				GL11.glPushMatrix();
				float var49 = 0.0F;
				float var47 = 0.0F;
				float var48 = 0.0F;

				for (int var32 = var28; var32 < var24; ++var32) {
					final WorldRenderer var33 = sortedWorldRenderers[var32];

					if (var33.skipAllRenderPasses()) {
						var33.isInFrustum = false;
					} else if (var33.isUpdating) {
						var33.isVisible = true;
					} else if (var33.isInFrustum) {
						if (Config.isOcclusionFancy()
								&& !var33.isInFrustrumFully) {
							var33.isVisible = true;
						} else if (var33.isInFrustum
								&& !var33.isWaitingOnOcclusionQuery) {
							float var34;
							float var35;
							float var36;
							float var37;

							if (var33.isVisibleFromPosition) {
								var34 = Math
										.abs((float) (var33.visibleFromX - par1EntityLiving.posX));
								var35 = Math
										.abs((float) (var33.visibleFromY - par1EntityLiving.posY));
								var36 = Math
										.abs((float) (var33.visibleFromZ - par1EntityLiving.posZ));
								var37 = var34 + var35 + var36;

								if (var37 < 10.0D + var32 / 1000.0D) {
									var33.isVisible = true;
									continue;
								}

								var33.isVisibleFromPosition = false;
							}

							var34 = (float) (var33.posXMinus - var40);
							var35 = (float) (var33.posYMinus - var41);
							var36 = (float) (var33.posZMinus - var10);
							var37 = var34 - var49;
							final float var38 = var35 - var47;
							final float var39 = var36 - var48;

							if (var37 != 0.0F || var38 != 0.0F || var39 != 0.0F) {
								GL11.glTranslatef(var37, var38, var39);
								var49 += var37;
								var47 += var38;
								var48 += var39;
							}

							var5.startSection("bb");
							ARBOcclusionQuery.glBeginQueryARB(
									ARBOcclusionQuery.GL_SAMPLES_PASSED_ARB,
									var33.glOcclusionQuery);
							var33.callOcclusionQueryList();
							ARBOcclusionQuery
									.glEndQueryARB(ARBOcclusionQuery.GL_SAMPLES_PASSED_ARB);
							var5.endSection();
							var33.isWaitingOnOcclusionQuery = true;
						}
					}
				}

				GL11.glPopMatrix();

				if (mc.gameSettings.anaglyph) {
					if (EntityRenderer.anaglyphField == 0) {
						GL11.glColorMask(false, true, true, true);
					} else {
						GL11.glColorMask(true, false, false, true);
					}
				} else {
					GL11.glColorMask(true, true, true, true);
				}

				GL11.glDepthMask(true);
				GL11.glEnable(GL11.GL_TEXTURE_2D);
				GL11.glEnable(GL11.GL_ALPHA_TEST);
				GL11.glEnable(GL11.GL_FOG);
				var5.endStartSection("render");
			}
		} else {
			var5.endStartSection("render");
			var20 = var42
					+ renderSortedRenderers(0, sortedWorldRenderers.length,
							par2, par3);
		}

		var5.endSection();
		return var20;
	}

	private void checkOcclusionQueryResult(final int var1, final int var2,
			final double var3, final double var5, final double var7) {
		for (int var9 = var1; var9 < var2; ++var9) {
			final WorldRenderer var10 = sortedWorldRenderers[var9];

			if (var10.isWaitingOnOcclusionQuery) {
				occlusionResult.clear();
				ARBOcclusionQuery.glGetQueryObjectuARB(var10.glOcclusionQuery,
						ARBOcclusionQuery.GL_QUERY_RESULT_AVAILABLE_ARB,
						occlusionResult);

				if (occlusionResult.get(0) != 0) {
					var10.isWaitingOnOcclusionQuery = false;
					occlusionResult.clear();
					ARBOcclusionQuery.glGetQueryObjectuARB(
							var10.glOcclusionQuery,
							ARBOcclusionQuery.GL_QUERY_RESULT_ARB,
							occlusionResult);
					final boolean var11 = var10.isVisible;
					var10.isVisible = occlusionResult.get(0) > 0;

					if (var11 && var10.isVisible) {
						var10.isVisibleFromPosition = true;
						var10.visibleFromX = var3;
						var10.visibleFromY = var5;
						var10.visibleFromZ = var7;
					}
				}
			}
		}
	}

	/**
	 * Renders the sorted renders for the specified render pass. Args:
	 * startRenderer, numRenderers, renderPass, partialTickTime
	 */
	private int renderSortedRenderers(final int par1, final int par2,
			final int par3, final double par4) {
		glListBuffer.clear();
		int var6 = 0;

		for (int var7 = par1; var7 < par2; ++var7) {
			final WorldRenderer var8 = sortedWorldRenderers[var7];

			if (par3 == 0) {
				++renderersLoaded;

				if (var8.skipRenderPass[par3]) {
					++renderersSkippingRenderPass;
				} else if (!var8.isInFrustum) {
					++renderersBeingClipped;
				} else if (occlusionEnabled && !var8.isVisible) {
					++renderersBeingOccluded;
				} else {
					++renderersBeingRendered;
				}
			}

			if (var8.isInFrustum && !var8.skipRenderPass[par3]
					&& (!occlusionEnabled || var8.isVisible)) {
				final int var9 = var8.getGLCallListForPass(par3);

				if (var9 >= 0) {
					glListBuffer.put(var9);
					++var6;
				}
			}
		}

		if (var6 == 0) {
			return 0;
		} else {
			if (Config.isFogOff()) {
				GL11.glDisable(GL11.GL_FOG);
			}

			glListBuffer.flip();
			final EntityLiving var14 = mc.renderViewEntity;
			final double var15 = var14.lastTickPosX
					+ (var14.posX - var14.lastTickPosX) * par4
					- WorldRenderer.globalChunkOffsetX;
			final double var10 = var14.lastTickPosY
					+ (var14.posY - var14.lastTickPosY) * par4;
			final double var12 = var14.lastTickPosZ
					+ (var14.posZ - var14.lastTickPosZ) * par4
					- WorldRenderer.globalChunkOffsetZ;
			mc.entityRenderer.enableLightmap(par4);
			GL11.glTranslatef((float) -var15, (float) -var10, (float) -var12);
			GL11.glCallLists(glListBuffer);
			GL11.glTranslatef((float) var15, (float) var10, (float) var12);
			mc.entityRenderer.disableLightmap(par4);
			return var6;
		}
	}

	/**
	 * Render all render lists
	 */
	public void renderAllRenderLists(final int par1, final double par2) {
	}

	public void updateClouds() {
		++cloudTickCounter;

		if (cloudTickCounter % 20 == 0) {
			final Iterator var1 = damagedBlocks.values().iterator();

			while (var1.hasNext()) {
				final DestroyBlockProgress var2 = (DestroyBlockProgress) var1
						.next();
				final int var3 = var2.getCreationCloudUpdateTick();

				if (cloudTickCounter - var3 > 400) {
					var1.remove();
				}
			}
		}
	}

	/**
	 * Renders the sky with the partial tick time. Args: partialTickTime
	 */
	public void renderSky(final float par1) {
		if (Reflector.ForgeWorldProvider_getSkyRenderer.exists()) {
			final WorldProvider var2 = mc.theWorld.provider;
			final Object var3 = Reflector.call(var2,
					Reflector.ForgeWorldProvider_getSkyRenderer, new Object[0]);

			if (var3 != null) {
				Reflector.callVoid(var3, Reflector.IRenderHandler_render,
						new Object[] { Float.valueOf(par1), theWorld, mc });
				return;
			}
		}

		if (mc.theWorld.provider.dimensionId == 1) {
			if (!Config.isSkyEnabled()) {
				return;
			}

			GL11.glDisable(GL11.GL_FOG);
			GL11.glDisable(GL11.GL_ALPHA_TEST);
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
			RenderHelper.disableStandardItemLighting();
			GL11.glDepthMask(false);
			renderEngine.bindTexture("/misc/tunnel.png");
			final Tessellator var20 = Tessellator.instance;

			for (int var22 = 0; var22 < 6; ++var22) {
				GL11.glPushMatrix();

				if (var22 == 1) {
					GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
				}

				if (var22 == 2) {
					GL11.glRotatef(-90.0F, 1.0F, 0.0F, 0.0F);
				}

				if (var22 == 3) {
					GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
				}

				if (var22 == 4) {
					GL11.glRotatef(90.0F, 0.0F, 0.0F, 1.0F);
				}

				if (var22 == 5) {
					GL11.glRotatef(-90.0F, 0.0F, 0.0F, 1.0F);
				}

				var20.startDrawingQuads();
				var20.setColorOpaque_I(2631720);
				var20.addVertexWithUV(-100.0D, -100.0D, -100.0D, 0.0D, 0.0D);
				var20.addVertexWithUV(-100.0D, -100.0D, 100.0D, 0.0D, 16.0D);
				var20.addVertexWithUV(100.0D, -100.0D, 100.0D, 16.0D, 16.0D);
				var20.addVertexWithUV(100.0D, -100.0D, -100.0D, 16.0D, 0.0D);
				var20.draw();
				GL11.glPopMatrix();
			}

			GL11.glDepthMask(true);
			GL11.glEnable(GL11.GL_TEXTURE_2D);
			GL11.glEnable(GL11.GL_ALPHA_TEST);
		} else if (mc.theWorld.provider.isSurfaceWorld()) {
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			Vec3 var21 = theWorld.getSkyColor(mc.renderViewEntity, par1);
			var21 = CustomColorizer.getSkyColor(var21, mc.theWorld,
					mc.renderViewEntity.posX, mc.renderViewEntity.posY + 1.0D,
					mc.renderViewEntity.posZ);
			float var23 = (float) var21.xCoord;
			float var4 = (float) var21.yCoord;
			float var5 = (float) var21.zCoord;
			float var6;

			if (mc.gameSettings.anaglyph) {
				final float var7 = (var23 * 30.0F + var4 * 59.0F + var5 * 11.0F) / 100.0F;
				final float var8 = (var23 * 30.0F + var4 * 70.0F) / 100.0F;
				var6 = (var23 * 30.0F + var5 * 70.0F) / 100.0F;
				var23 = var7;
				var4 = var8;
				var5 = var6;
			}

			GL11.glColor3f(var23, var4, var5);
			final Tessellator var24 = Tessellator.instance;
			GL11.glDepthMask(false);
			GL11.glEnable(GL11.GL_FOG);
			GL11.glColor3f(var23, var4, var5);

			if (Config.isSkyEnabled()) {
				GL11.glCallList(glSkyList);
			}

			GL11.glDisable(GL11.GL_FOG);
			GL11.glDisable(GL11.GL_ALPHA_TEST);
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
			RenderHelper.disableStandardItemLighting();
			final float[] var25 = theWorld.provider.calcSunriseSunsetColors(
					theWorld.getCelestialAngle(par1), par1);
			float var9;
			float var10;
			float var11;
			float var12;
			float var13;
			int var15;
			float var17;
			float var16;

			if (var25 != null && Config.isSunMoonEnabled()) {
				GL11.glDisable(GL11.GL_TEXTURE_2D);
				GL11.glShadeModel(GL11.GL_SMOOTH);
				GL11.glPushMatrix();
				GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
				GL11.glRotatef(
						MathHelper.sin(theWorld.getCelestialAngleRadians(par1)) < 0.0F ? 180.0F
								: 0.0F, 0.0F, 0.0F, 1.0F);
				GL11.glRotatef(90.0F, 0.0F, 0.0F, 1.0F);
				var6 = var25[0];
				var9 = var25[1];
				var10 = var25[2];

				if (mc.gameSettings.anaglyph) {
					var11 = (var6 * 30.0F + var9 * 59.0F + var10 * 11.0F) / 100.0F;
					var12 = (var6 * 30.0F + var9 * 70.0F) / 100.0F;
					var13 = (var6 * 30.0F + var10 * 70.0F) / 100.0F;
					var6 = var11;
					var9 = var12;
					var10 = var13;
				}

				var24.startDrawing(6);
				var24.setColorRGBA_F(var6, var9, var10, var25[3]);
				var24.addVertex(0.0D, 100.0D, 0.0D);
				final byte var14 = 16;
				var24.setColorRGBA_F(var25[0], var25[1], var25[2], 0.0F);

				for (var15 = 0; var15 <= var14; ++var15) {
					var13 = var15 * (float) Math.PI * 2.0F / var14;
					var16 = MathHelper.sin(var13);
					var17 = MathHelper.cos(var13);
					var24.addVertex(var16 * 120.0F, var17 * 120.0F, -var17
							* 40.0F * var25[3]);
				}

				var24.draw();
				GL11.glPopMatrix();
				GL11.glShadeModel(GL11.GL_FLAT);
			}

			GL11.glEnable(GL11.GL_TEXTURE_2D);
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE);
			GL11.glPushMatrix();
			var6 = 1.0F - theWorld.getRainStrength(par1);
			var9 = 0.0F;
			var10 = 0.0F;
			var11 = 0.0F;
			GL11.glColor4f(1.0F, 1.0F, 1.0F, var6);
			GL11.glTranslatef(var9, var10, var11);
			GL11.glRotatef(-90.0F, 0.0F, 1.0F, 0.0F);
			CustomSky.renderSky(theWorld, renderEngine,
					theWorld.getCelestialAngle(par1), var6);
			GL11.glRotatef(theWorld.getCelestialAngle(par1) * 360.0F, 1.0F,
					0.0F, 0.0F);

			if (Config.isSunMoonEnabled()) {
				var12 = 30.0F;
				renderEngine.bindTexture("/environment/sun.png");
				var24.startDrawingQuads();
				var24.addVertexWithUV(-var12, 100.0D, -var12, 0.0D, 0.0D);
				var24.addVertexWithUV(var12, 100.0D, -var12, 1.0D, 0.0D);
				var24.addVertexWithUV(var12, 100.0D, var12, 1.0D, 1.0D);
				var24.addVertexWithUV(-var12, 100.0D, var12, 0.0D, 1.0D);
				var24.draw();
				var12 = 20.0F;
				renderEngine.bindTexture("/environment/moon_phases.png");
				final int var26 = theWorld.getMoonPhase();
				final int var27 = var26 % 4;
				var15 = var26 / 4 % 2;
				var16 = (var27 + 0) / 4.0F;
				var17 = (var15 + 0) / 2.0F;
				final float var18 = (var27 + 1) / 4.0F;
				final float var19 = (var15 + 1) / 2.0F;
				var24.startDrawingQuads();
				var24.addVertexWithUV(-var12, -100.0D, var12, var18, var19);
				var24.addVertexWithUV(var12, -100.0D, var12, var16, var19);
				var24.addVertexWithUV(var12, -100.0D, -var12, var16, var17);
				var24.addVertexWithUV(-var12, -100.0D, -var12, var18, var17);
				var24.draw();
			}

			GL11.glDisable(GL11.GL_TEXTURE_2D);
			var13 = theWorld.getStarBrightness(par1) * var6;

			if (var13 > 0.0F && Config.isStarsEnabled()
					&& !CustomSky.hasSkyLayers(theWorld)) {
				GL11.glColor4f(var13, var13, var13, var13);
				GL11.glCallList(starGLCallList);
			}

			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			GL11.glDisable(GL11.GL_BLEND);
			GL11.glEnable(GL11.GL_ALPHA_TEST);
			GL11.glEnable(GL11.GL_FOG);
			GL11.glPopMatrix();
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			GL11.glColor3f(0.0F, 0.0F, 0.0F);
			final double var28 = mc.thePlayer.getPosition(par1).yCoord
					- theWorld.getHorizon();

			if (var28 < 0.0D) {
				GL11.glPushMatrix();
				GL11.glTranslatef(0.0F, 12.0F, 0.0F);
				GL11.glCallList(glSkyList2);
				GL11.glPopMatrix();
				var10 = 1.0F;
				var11 = -((float) (var28 + 65.0D));
				var12 = -var10;
				var24.startDrawingQuads();
				var24.setColorRGBA_I(0, 255);
				var24.addVertex(-var10, var11, var10);
				var24.addVertex(var10, var11, var10);
				var24.addVertex(var10, var12, var10);
				var24.addVertex(-var10, var12, var10);
				var24.addVertex(-var10, var12, -var10);
				var24.addVertex(var10, var12, -var10);
				var24.addVertex(var10, var11, -var10);
				var24.addVertex(-var10, var11, -var10);
				var24.addVertex(var10, var12, -var10);
				var24.addVertex(var10, var12, var10);
				var24.addVertex(var10, var11, var10);
				var24.addVertex(var10, var11, -var10);
				var24.addVertex(-var10, var11, -var10);
				var24.addVertex(-var10, var11, var10);
				var24.addVertex(-var10, var12, var10);
				var24.addVertex(-var10, var12, -var10);
				var24.addVertex(-var10, var12, -var10);
				var24.addVertex(-var10, var12, var10);
				var24.addVertex(var10, var12, var10);
				var24.addVertex(var10, var12, -var10);
				var24.draw();
			}

			if (theWorld.provider.isSkyColored()) {
				GL11.glColor3f(var23 * 0.2F + 0.04F, var4 * 0.2F + 0.04F,
						var5 * 0.6F + 0.1F);
			} else {
				GL11.glColor3f(var23, var4, var5);
			}

			if (mc.gameSettings.ofRenderDistanceFine <= 64) {
				GL11.glColor3f(mc.entityRenderer.fogColorRed,
						mc.entityRenderer.fogColorGreen,
						mc.entityRenderer.fogColorBlue);
			}

			GL11.glPushMatrix();
			GL11.glTranslatef(0.0F, -((float) (var28 - 16.0D)), 0.0F);

			if (Config.isSkyEnabled()) {
				GL11.glCallList(glSkyList2);
			}

			GL11.glPopMatrix();
			GL11.glEnable(GL11.GL_TEXTURE_2D);
			GL11.glDepthMask(true);
		}
	}

	public void renderClouds(final float par1) {
		if (!Config.isCloudsOff()) {
			if (Reflector.ForgeWorldProvider_getCloudRenderer.exists()) {
				final WorldProvider var2 = mc.theWorld.provider;
				final Object var3 = Reflector.call(var2,
						Reflector.ForgeWorldProvider_getCloudRenderer,
						new Object[0]);

				if (var3 != null) {
					Reflector.callVoid(var3, Reflector.IRenderHandler_render,
							new Object[] { Float.valueOf(par1), theWorld, mc });
					return;
				}
			}

			if (mc.theWorld.provider.isSurfaceWorld()) {
				if (Config.isCloudsFancy()) {
					renderCloudsFancy(par1);
				} else {
					GL11.glDisable(GL11.GL_CULL_FACE);
					final float var24 = (float) (mc.renderViewEntity.lastTickPosY + (mc.renderViewEntity.posY - mc.renderViewEntity.lastTickPosY)
							* par1);
					final byte var25 = 32;
					final int var4 = 256 / var25;
					final Tessellator var5 = Tessellator.instance;
					renderEngine.bindTexture("/environment/clouds.png");
					GL11.glEnable(GL11.GL_BLEND);
					GL11.glBlendFunc(GL11.GL_SRC_ALPHA,
							GL11.GL_ONE_MINUS_SRC_ALPHA);
					final Vec3 var6 = theWorld.getCloudColour(par1);
					float var7 = (float) var6.xCoord;
					float var8 = (float) var6.yCoord;
					float var9 = (float) var6.zCoord;
					float var10;

					if (mc.gameSettings.anaglyph) {
						var10 = (var7 * 30.0F + var8 * 59.0F + var9 * 11.0F) / 100.0F;
						final float var11 = (var7 * 30.0F + var8 * 70.0F) / 100.0F;
						final float var12 = (var7 * 30.0F + var9 * 70.0F) / 100.0F;
						var7 = var10;
						var8 = var11;
						var9 = var12;
					}

					var10 = 4.8828125E-4F;
					final double var26 = cloudTickCounter + par1;
					double var13 = mc.renderViewEntity.prevPosX
							+ (mc.renderViewEntity.posX - mc.renderViewEntity.prevPosX)
							* par1 + var26 * 0.029999999329447746D;
					double var15 = mc.renderViewEntity.prevPosZ
							+ (mc.renderViewEntity.posZ - mc.renderViewEntity.prevPosZ)
							* par1;
					final int var17 = MathHelper.floor_double(var13 / 2048.0D);
					final int var18 = MathHelper.floor_double(var15 / 2048.0D);
					var13 -= var17 * 2048;
					var15 -= var18 * 2048;
					float var19 = theWorld.provider.getCloudHeight() - var24
							+ 0.33F;
					var19 += mc.gameSettings.ofCloudsHeight * 128.0F;
					final float var20 = (float) (var13 * var10);
					final float var21 = (float) (var15 * var10);
					var5.startDrawingQuads();
					var5.setColorRGBA_F(var7, var8, var9, 0.8F);

					for (int var22 = -var25 * var4; var22 < var25 * var4; var22 += var25) {
						for (int var23 = -var25 * var4; var23 < var25 * var4; var23 += var25) {
							var5.addVertexWithUV(var22 + 0, var19, var23
									+ var25, (var22 + 0) * var10 + var20,
									(var23 + var25) * var10 + var21);
							var5.addVertexWithUV(var22 + var25, var19, var23
									+ var25, (var22 + var25) * var10 + var20,
									(var23 + var25) * var10 + var21);
							var5.addVertexWithUV(var22 + var25, var19,
									var23 + 0, (var22 + var25) * var10 + var20,
									(var23 + 0) * var10 + var21);
							var5.addVertexWithUV(var22 + 0, var19, var23 + 0,
									(var22 + 0) * var10 + var20, (var23 + 0)
											* var10 + var21);
						}
					}

					var5.draw();
					GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
					GL11.glDisable(GL11.GL_BLEND);
					GL11.glEnable(GL11.GL_CULL_FACE);
				}
			}
		}
	}

	/**
	 * Checks if the given position is to be rendered with cloud fog
	 */
	public boolean hasCloudFog(final double par1, final double par3,
			final double par5, final float par7) {
		return false;
	}

	/**
	 * Renders the 3d fancy clouds
	 */
	public void renderCloudsFancy(final float par1) {
		GL11.glDisable(GL11.GL_CULL_FACE);
		final float var2 = (float) (mc.renderViewEntity.lastTickPosY + (mc.renderViewEntity.posY - mc.renderViewEntity.lastTickPosY)
				* par1);
		final Tessellator var3 = Tessellator.instance;
		final float var4 = 12.0F;
		final float var5 = 4.0F;
		final double var6 = cloudTickCounter + par1;
		double var8 = (mc.renderViewEntity.prevPosX
				+ (mc.renderViewEntity.posX - mc.renderViewEntity.prevPosX)
				* par1 + var6 * 0.029999999329447746D)
				/ var4;
		double var10 = (mc.renderViewEntity.prevPosZ + (mc.renderViewEntity.posZ - mc.renderViewEntity.prevPosZ)
				* par1)
				/ var4 + 0.33000001311302185D;
		float var12 = theWorld.provider.getCloudHeight() - var2 + 0.33F;
		var12 += mc.gameSettings.ofCloudsHeight * 128.0F;
		final int var13 = MathHelper.floor_double(var8 / 2048.0D);
		final int var14 = MathHelper.floor_double(var10 / 2048.0D);
		var8 -= var13 * 2048;
		var10 -= var14 * 2048;
		renderEngine.bindTexture("/environment/clouds.png");
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		final Vec3 var15 = theWorld.getCloudColour(par1);
		float var16 = (float) var15.xCoord;
		float var17 = (float) var15.yCoord;
		float var18 = (float) var15.zCoord;
		float var19;
		float var21;
		float var20;

		if (mc.gameSettings.anaglyph) {
			var19 = (var16 * 30.0F + var17 * 59.0F + var18 * 11.0F) / 100.0F;
			var21 = (var16 * 30.0F + var17 * 70.0F) / 100.0F;
			var20 = (var16 * 30.0F + var18 * 70.0F) / 100.0F;
			var16 = var19;
			var17 = var21;
			var18 = var20;
		}

		var19 = (float) (var8 * 0.0D);
		var21 = (float) (var10 * 0.0D);
		var20 = 0.00390625F;
		var19 = MathHelper.floor_double(var8) * var20;
		var21 = MathHelper.floor_double(var10) * var20;
		final float var22 = (float) (var8 - MathHelper.floor_double(var8));
		final float var23 = (float) (var10 - MathHelper.floor_double(var10));
		final byte var24 = 8;
		final byte var25 = 4;
		final float var26 = 9.765625E-4F;
		GL11.glScalef(var4, 1.0F, var4);

		for (int var27 = 0; var27 < 2; ++var27) {
			if (var27 == 0) {
				GL11.glColorMask(false, false, false, false);
			} else if (mc.gameSettings.anaglyph) {
				if (EntityRenderer.anaglyphField == 0) {
					GL11.glColorMask(false, true, true, true);
				} else {
					GL11.glColorMask(true, false, false, true);
				}
			} else {
				GL11.glColorMask(true, true, true, true);
			}

			for (int var28 = -var25 + 1; var28 <= var25; ++var28) {
				for (int var29 = -var25 + 1; var29 <= var25; ++var29) {
					var3.startDrawingQuads();
					final float var30 = var28 * var24;
					final float var31 = var29 * var24;
					final float var32 = var30 - var22;
					final float var33 = var31 - var23;

					if (var12 > -var5 - 1.0F) {
						var3.setColorRGBA_F(var16 * 0.7F, var17 * 0.7F,
								var18 * 0.7F, 0.8F);
						var3.setNormal(0.0F, -1.0F, 0.0F);
						var3.addVertexWithUV(var32 + 0.0F, var12 + 0.0F, var33
								+ var24, (var30 + 0.0F) * var20 + var19,
								(var31 + var24) * var20 + var21);
						var3.addVertexWithUV(var32 + var24, var12 + 0.0F, var33
								+ var24, (var30 + var24) * var20 + var19,
								(var31 + var24) * var20 + var21);
						var3.addVertexWithUV(var32 + var24, var12 + 0.0F,
								var33 + 0.0F, (var30 + var24) * var20 + var19,
								(var31 + 0.0F) * var20 + var21);
						var3.addVertexWithUV(var32 + 0.0F, var12 + 0.0F,
								var33 + 0.0F, (var30 + 0.0F) * var20 + var19,
								(var31 + 0.0F) * var20 + var21);
					}

					if (var12 <= var5 + 1.0F) {
						var3.setColorRGBA_F(var16, var17, var18, 0.8F);
						var3.setNormal(0.0F, 1.0F, 0.0F);
						var3.addVertexWithUV(var32 + 0.0F,
								var12 + var5 - var26, var33 + var24,
								(var30 + 0.0F) * var20 + var19, (var31 + var24)
										* var20 + var21);
						var3.addVertexWithUV(var32 + var24, var12 + var5
								- var26, var33 + var24, (var30 + var24) * var20
								+ var19, (var31 + var24) * var20 + var21);
						var3.addVertexWithUV(var32 + var24, var12 + var5
								- var26, var33 + 0.0F, (var30 + var24) * var20
								+ var19, (var31 + 0.0F) * var20 + var21);
						var3.addVertexWithUV(var32 + 0.0F,
								var12 + var5 - var26, var33 + 0.0F,
								(var30 + 0.0F) * var20 + var19, (var31 + 0.0F)
										* var20 + var21);
					}

					var3.setColorRGBA_F(var16 * 0.9F, var17 * 0.9F,
							var18 * 0.9F, 0.8F);
					int var34;

					if (var28 > -1) {
						var3.setNormal(-1.0F, 0.0F, 0.0F);

						for (var34 = 0; var34 < var24; ++var34) {
							var3.addVertexWithUV(var32 + var34 + 0.0F,
									var12 + 0.0F, var33 + var24,
									(var30 + var34 + 0.5F) * var20 + var19,
									(var31 + var24) * var20 + var21);
							var3.addVertexWithUV(var32 + var34 + 0.0F, var12
									+ var5, var33 + var24,
									(var30 + var34 + 0.5F) * var20 + var19,
									(var31 + var24) * var20 + var21);
							var3.addVertexWithUV(var32 + var34 + 0.0F, var12
									+ var5, var33 + 0.0F,
									(var30 + var34 + 0.5F) * var20 + var19,
									(var31 + 0.0F) * var20 + var21);
							var3.addVertexWithUV(var32 + var34 + 0.0F,
									var12 + 0.0F, var33 + 0.0F,
									(var30 + var34 + 0.5F) * var20 + var19,
									(var31 + 0.0F) * var20 + var21);
						}
					}

					if (var28 <= 1) {
						var3.setNormal(1.0F, 0.0F, 0.0F);

						for (var34 = 0; var34 < var24; ++var34) {
							var3.addVertexWithUV(var32 + var34 + 1.0F - var26,
									var12 + 0.0F, var33 + var24,
									(var30 + var34 + 0.5F) * var20 + var19,
									(var31 + var24) * var20 + var21);
							var3.addVertexWithUV(var32 + var34 + 1.0F - var26,
									var12 + var5, var33 + var24,
									(var30 + var34 + 0.5F) * var20 + var19,
									(var31 + var24) * var20 + var21);
							var3.addVertexWithUV(var32 + var34 + 1.0F - var26,
									var12 + var5, var33 + 0.0F,
									(var30 + var34 + 0.5F) * var20 + var19,
									(var31 + 0.0F) * var20 + var21);
							var3.addVertexWithUV(var32 + var34 + 1.0F - var26,
									var12 + 0.0F, var33 + 0.0F,
									(var30 + var34 + 0.5F) * var20 + var19,
									(var31 + 0.0F) * var20 + var21);
						}
					}

					var3.setColorRGBA_F(var16 * 0.8F, var17 * 0.8F,
							var18 * 0.8F, 0.8F);

					if (var29 > -1) {
						var3.setNormal(0.0F, 0.0F, -1.0F);

						for (var34 = 0; var34 < var24; ++var34) {
							var3.addVertexWithUV(var32 + 0.0F, var12 + var5,
									var33 + var34 + 0.0F, (var30 + 0.0F)
											* var20 + var19,
									(var31 + var34 + 0.5F) * var20 + var21);
							var3.addVertexWithUV(var32 + var24, var12 + var5,
									var33 + var34 + 0.0F, (var30 + var24)
											* var20 + var19,
									(var31 + var34 + 0.5F) * var20 + var21);
							var3.addVertexWithUV(var32 + var24, var12 + 0.0F,
									var33 + var34 + 0.0F, (var30 + var24)
											* var20 + var19,
									(var31 + var34 + 0.5F) * var20 + var21);
							var3.addVertexWithUV(var32 + 0.0F, var12 + 0.0F,
									var33 + var34 + 0.0F, (var30 + 0.0F)
											* var20 + var19,
									(var31 + var34 + 0.5F) * var20 + var21);
						}
					}

					if (var29 <= 1) {
						var3.setNormal(0.0F, 0.0F, 1.0F);

						for (var34 = 0; var34 < var24; ++var34) {
							var3.addVertexWithUV(var32 + 0.0F, var12 + var5,
									var33 + var34 + 1.0F - var26,
									(var30 + 0.0F) * var20 + var19, (var31
											+ var34 + 0.5F)
											* var20 + var21);
							var3.addVertexWithUV(var32 + var24, var12 + var5,
									var33 + var34 + 1.0F - var26,
									(var30 + var24) * var20 + var19, (var31
											+ var34 + 0.5F)
											* var20 + var21);
							var3.addVertexWithUV(var32 + var24, var12 + 0.0F,
									var33 + var34 + 1.0F - var26,
									(var30 + var24) * var20 + var19, (var31
											+ var34 + 0.5F)
											* var20 + var21);
							var3.addVertexWithUV(var32 + 0.0F, var12 + 0.0F,
									var33 + var34 + 1.0F - var26,
									(var30 + 0.0F) * var20 + var19, (var31
											+ var34 + 0.5F)
											* var20 + var21);
						}
					}

					var3.draw();
				}
			}
		}

		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_CULL_FACE);
	}

	/**
	 * Updates some of the renderers sorted by distance from the player
	 */
	public boolean updateRenderers(final EntityLiving par1EntityLiving,
			final boolean par2) {
		if (worldRenderersToUpdate.size() <= 0) {
			return false;
		} else {
			int var3 = 0;
			int var4 = Config.getUpdatesPerFrame();

			if (Config.isDynamicUpdates() && !isMoving(par1EntityLiving)) {
				var4 *= 3;
			}

			final byte var5 = 4;
			int var6 = 0;
			WorldRenderer var7 = null;
			float var8 = Float.MAX_VALUE;
			int var9 = -1;

			for (int var10 = 0; var10 < worldRenderersToUpdate.size(); ++var10) {
				final WorldRenderer var11 = (WorldRenderer) worldRenderersToUpdate
						.get(var10);

				if (var11 != null) {
					++var6;

					if (!var11.needsUpdate) {
						worldRenderersToUpdate.set(var10, (Object) null);
					} else {
						float var12 = var11
								.distanceToEntitySquared(par1EntityLiving);

						if (var12 <= 256.0F && isActingNow()) {
							var11.updateRenderer();
							var11.needsUpdate = false;
							worldRenderersToUpdate.set(var10, (Object) null);
							++var3;
						} else {
							if (var12 > 256.0F && var3 >= var4) {
								break;
							}

							if (!var11.isInFrustum) {
								var12 *= var5;
							}

							if (var7 == null) {
								var7 = var11;
								var8 = var12;
								var9 = var10;
							} else if (var12 < var8) {
								var7 = var11;
								var8 = var12;
								var9 = var10;
							}
						}
					}
				}
			}

			if (var7 != null) {
				var7.updateRenderer();
				var7.needsUpdate = false;
				worldRenderersToUpdate.set(var9, (Object) null);
				++var3;
				final float var16 = var8 / 5.0F;

				for (int var15 = 0; var15 < worldRenderersToUpdate.size()
						&& var3 < var4; ++var15) {
					final WorldRenderer var17 = (WorldRenderer) worldRenderersToUpdate
							.get(var15);

					if (var17 != null) {
						float var13 = var17
								.distanceToEntitySquared(par1EntityLiving);

						if (!var17.isInFrustum) {
							var13 *= var5;
						}

						final float var14 = Math.abs(var13 - var8);

						if (var14 < var16) {
							var17.updateRenderer();
							var17.needsUpdate = false;
							worldRenderersToUpdate.set(var15, (Object) null);
							++var3;
						}
					}
				}
			}

			if (var6 == 0) {
				worldRenderersToUpdate.clear();
			}

			worldRenderersToUpdate.compact();
			return true;
		}
	}

	public void drawBlockBreaking(final EntityPlayer par1EntityPlayer,
			final MovingObjectPosition par2MovingObjectPosition,
			final int par3, final ItemStack par4ItemStack, final float par5) {
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_ALPHA_TEST);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE);
		GL11.glColor4f(
				1.0F,
				1.0F,
				1.0F,
				(MathHelper.sin(Minecraft.getSystemTime() / 100.0F) * 0.2F + 0.4F) * 0.5F);

		if (par3 != 0 && par4ItemStack != null) {
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
			final float var7 = MathHelper
					.sin(Minecraft.getSystemTime() / 100.0F) * 0.2F + 0.8F;
			GL11.glColor4f(
					var7,
					var7,
					var7,
					MathHelper.sin(Minecraft.getSystemTime() / 200.0F) * 0.2F + 0.5F);
			renderEngine.bindTexture("/terrain.png");
		}

		GL11.glDisable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_ALPHA_TEST);
	}

	public void drawBlockDamageTexture(final Tessellator par1Tessellator,
			final EntityPlayer par2EntityPlayer, final float par3) {
		this.drawBlockDamageTexture(par1Tessellator, par2EntityPlayer, par3);
	}

	public void drawBlockDamageTexture(final Tessellator var1,
			final EntityLiving var2, final float var3) {
		final double var4 = var2.lastTickPosX + (var2.posX - var2.lastTickPosX)
				* var3;
		final double var6 = var2.lastTickPosY + (var2.posY - var2.lastTickPosY)
				* var3;
		final double var8 = var2.lastTickPosZ + (var2.posZ - var2.lastTickPosZ)
				* var3;

		if (!damagedBlocks.isEmpty()) {
			GL11.glBlendFunc(GL11.GL_DST_COLOR, GL11.GL_SRC_COLOR);
			renderEngine.bindTexture("/terrain.png");
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 0.5F);
			GL11.glPushMatrix();
			GL11.glDisable(GL11.GL_ALPHA_TEST);
			GL11.glPolygonOffset(-3.0F, -3.0F);
			GL11.glEnable(GL11.GL_POLYGON_OFFSET_FILL);
			GL11.glEnable(GL11.GL_ALPHA_TEST);
			var1.startDrawingQuads();
			var1.setTranslation(-var4, -var6, -var8);
			var1.disableColor();
			final Iterator var10 = damagedBlocks.values().iterator();

			while (var10.hasNext()) {
				final DestroyBlockProgress var11 = (DestroyBlockProgress) var10
						.next();
				final double var12 = var11.getPartialBlockX() - var4;
				final double var14 = var11.getPartialBlockY() - var6;
				final double var16 = var11.getPartialBlockZ() - var8;

				if (var12 * var12 + var14 * var14 + var16 * var16 > 1024.0D) {
					var10.remove();
				} else {
					final int var18 = theWorld.getBlockId(
							var11.getPartialBlockX(), var11.getPartialBlockY(),
							var11.getPartialBlockZ());
					Block var19 = var18 > 0 ? Block.blocksList[var18] : null;

					if (var19 == null) {
						var19 = Block.stone;
					}

					globalRenderBlocks.renderBlockUsingTexture(var19,
							var11.getPartialBlockX(), var11.getPartialBlockY(),
							var11.getPartialBlockZ(),
							destroyBlockIcons[var11.getPartialBlockDamage()]);
				}
			}

			var1.draw();
			var1.setTranslation(0.0D, 0.0D, 0.0D);
			GL11.glDisable(GL11.GL_ALPHA_TEST);
			GL11.glPolygonOffset(0.0F, 0.0F);
			GL11.glDisable(GL11.GL_POLYGON_OFFSET_FILL);
			GL11.glEnable(GL11.GL_ALPHA_TEST);
			GL11.glDepthMask(true);
			GL11.glPopMatrix();
		}
	}

	/**
	 * Draws the selection box for the player. Args: entityPlayer, rayTraceHit,
	 * i, itemStack, partialTickTime
	 */
	public void drawSelectionBox(final EntityPlayer par1EntityPlayer,
			final MovingObjectPosition par2MovingObjectPosition,
			final int par3, final ItemStack par4ItemStack, final float par5) {
		if (par3 == 0
				&& par2MovingObjectPosition.typeOfHit == EnumMovingObjectType.TILE) {
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
			GL11.glColor4f(0.0F, 0.0F, 0.0F, 0.4F);
			GL11.glLineWidth(2.0F);
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			GL11.glDepthMask(false);
			final float var6 = 0.002F;
			final int var7 = theWorld.getBlockId(
					par2MovingObjectPosition.blockX,
					par2MovingObjectPosition.blockY,
					par2MovingObjectPosition.blockZ);

			if (var7 > 0) {
				Block.blocksList[var7].setBlockBoundsBasedOnState(theWorld,
						par2MovingObjectPosition.blockX,
						par2MovingObjectPosition.blockY,
						par2MovingObjectPosition.blockZ);
				final double var8 = par1EntityPlayer.lastTickPosX
						+ (par1EntityPlayer.posX - par1EntityPlayer.lastTickPosX)
						* par5;
				final double var10 = par1EntityPlayer.lastTickPosY
						+ (par1EntityPlayer.posY - par1EntityPlayer.lastTickPosY)
						* par5;
				final double var12 = par1EntityPlayer.lastTickPosZ
						+ (par1EntityPlayer.posZ - par1EntityPlayer.lastTickPosZ)
						* par5;
				drawOutlinedBoundingBox(Block.blocksList[var7]
						.getSelectedBoundingBoxFromPool(theWorld,
								par2MovingObjectPosition.blockX,
								par2MovingObjectPosition.blockY,
								par2MovingObjectPosition.blockZ)
						.expand(var6, var6, var6)
						.getOffsetBoundingBox(-var8, -var10, -var12));
			}

			GL11.glDepthMask(true);
			GL11.glEnable(GL11.GL_TEXTURE_2D);
			GL11.glDisable(GL11.GL_BLEND);
		}
	}

	/**
	 * Draws lines for the edges of the bounding box.
	 */
	private void drawOutlinedBoundingBox(final AxisAlignedBB par1AxisAlignedBB) {
		final Tessellator var2 = Tessellator.instance;
		var2.startDrawing(3);
		var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.minY,
				par1AxisAlignedBB.minZ);
		var2.addVertex(par1AxisAlignedBB.maxX, par1AxisAlignedBB.minY,
				par1AxisAlignedBB.minZ);
		var2.addVertex(par1AxisAlignedBB.maxX, par1AxisAlignedBB.minY,
				par1AxisAlignedBB.maxZ);
		var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.minY,
				par1AxisAlignedBB.maxZ);
		var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.minY,
				par1AxisAlignedBB.minZ);
		var2.draw();
		var2.startDrawing(3);
		var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.maxY,
				par1AxisAlignedBB.minZ);
		var2.addVertex(par1AxisAlignedBB.maxX, par1AxisAlignedBB.maxY,
				par1AxisAlignedBB.minZ);
		var2.addVertex(par1AxisAlignedBB.maxX, par1AxisAlignedBB.maxY,
				par1AxisAlignedBB.maxZ);
		var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.maxY,
				par1AxisAlignedBB.maxZ);
		var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.maxY,
				par1AxisAlignedBB.minZ);
		var2.draw();
		var2.startDrawing(1);
		var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.minY,
				par1AxisAlignedBB.minZ);
		var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.maxY,
				par1AxisAlignedBB.minZ);
		var2.addVertex(par1AxisAlignedBB.maxX, par1AxisAlignedBB.minY,
				par1AxisAlignedBB.minZ);
		var2.addVertex(par1AxisAlignedBB.maxX, par1AxisAlignedBB.maxY,
				par1AxisAlignedBB.minZ);
		var2.addVertex(par1AxisAlignedBB.maxX, par1AxisAlignedBB.minY,
				par1AxisAlignedBB.maxZ);
		var2.addVertex(par1AxisAlignedBB.maxX, par1AxisAlignedBB.maxY,
				par1AxisAlignedBB.maxZ);
		var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.minY,
				par1AxisAlignedBB.maxZ);
		var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.maxY,
				par1AxisAlignedBB.maxZ);
		var2.draw();
	}

	/**
	 * Marks the blocks in the given range for update
	 */
	public void markBlocksForUpdate(final int par1, final int par2,
			final int par3, final int par4, final int par5, final int par6) {
		final int var7 = MathHelper.bucketInt(par1, 16);
		final int var8 = MathHelper.bucketInt(par2, 16);
		final int var9 = MathHelper.bucketInt(par3, 16);
		final int var10 = MathHelper.bucketInt(par4, 16);
		final int var11 = MathHelper.bucketInt(par5, 16);
		final int var12 = MathHelper.bucketInt(par6, 16);

		for (int var13 = var7; var13 <= var10; ++var13) {
			int var14 = var13 % renderChunksWide;

			if (var14 < 0) {
				var14 += renderChunksWide;
			}

			for (int var15 = var8; var15 <= var11; ++var15) {
				int var16 = var15 % renderChunksTall;

				if (var16 < 0) {
					var16 += renderChunksTall;
				}

				for (int var17 = var9; var17 <= var12; ++var17) {
					int var18 = var17 % renderChunksDeep;

					if (var18 < 0) {
						var18 += renderChunksDeep;
					}

					final int var19 = (var18 * renderChunksTall + var16)
							* renderChunksWide + var14;
					final WorldRenderer var20 = worldRenderers[var19];

					if (var20 != null && !var20.needsUpdate) {
						worldRenderersToUpdate.add(var20);
						var20.markDirty();
					}
				}
			}
		}
	}

	/**
	 * On the client, re-renders the block. On the server, sends the block to
	 * the client (which will re-render it), including the tile entity
	 * description packet if applicable. Args: x, y, z
	 */
	@Override
	public void markBlockForUpdate(final int par1, final int par2,
			final int par3) {
		markBlocksForUpdate(par1 - 1, par2 - 1, par3 - 1, par1 + 1, par2 + 1,
				par3 + 1);
	}

	/**
	 * On the client, re-renders this block. On the server, does nothing. Used
	 * for lighting updates.
	 */
	@Override
	public void markBlockForRenderUpdate(final int par1, final int par2,
			final int par3) {
		markBlocksForUpdate(par1 - 1, par2 - 1, par3 - 1, par1 + 1, par2 + 1,
				par3 + 1);
	}

	/**
	 * On the client, re-renders all blocks in this range, inclusive. On the
	 * server, does nothing. Args: min x, min y, min z, max x, max y, max z
	 */
	@Override
	public void markBlockRangeForRenderUpdate(final int par1, final int par2,
			final int par3, final int par4, final int par5, final int par6) {
		markBlocksForUpdate(par1 - 1, par2 - 1, par3 - 1, par4 + 1, par5 + 1,
				par6 + 1);
	}

	/**
	 * Checks all renderers that previously weren't in the frustum and 1/16th of
	 * those that previously were in the frustum for frustum clipping Args:
	 * frustum, partialTickTime
	 */
	public void clipRenderersByFrustum(final ICamera par1ICamera,
			final float par2) {
		for (int var3 = 0; var3 < worldRenderers.length; ++var3) {
			if (!worldRenderers[var3].skipAllRenderPasses()) {
				worldRenderers[var3].updateInFrustum(par1ICamera);
			}
		}

		++frustumCheckOffset;
	}

	/**
	 * Plays the specified record. Arg: recordName, x, y, z
	 */
	@Override
	public void playRecord(final String par1Str, final int par2,
			final int par3, final int par4) {
		final ItemRecord var5 = ItemRecord.getRecord(par1Str);

		if (par1Str != null && var5 != null) {
			mc.ingameGUI.setRecordPlayingMessage(var5.getRecordTitle());
		}

		mc.sndManager.playStreaming(par1Str, par2, par3, par4);
	}

	/**
	 * Plays the specified sound. Arg: soundName, x, y, z, volume, pitch
	 */
	@Override
	public void playSound(final String par1Str, final double par2,
			final double par4, final double par6, final float par8,
			final float par9) {
	}

	/**
	 * Plays sound to all near players except the player reference given
	 */
	@Override
	public void playSoundToNearExcept(final EntityPlayer par1EntityPlayer,
			final String par2Str, final double par3, final double par5,
			final double par7, final float par9, final float par10) {
	}

	/**
	 * Spawns a particle. Arg: particleType, x, y, z, velX, velY, velZ
	 */
	@Override
	public void spawnParticle(final String par1Str, final double par2,
			final double par4, final double par6, final double par8,
			final double par10, final double par12) {
		try {
			doSpawnParticle(par1Str, par2, par4, par6, par8, par10, par12);
		} catch (final Throwable var17) {
			final CrashReport var15 = CrashReport.makeCrashReport(var17,
					"Exception while adding particle");
			final CrashReportCategory var16 = var15
					.makeCategory("Particle being added");
			var16.addCrashSection("Name", par1Str);
			var16.addCrashSectionCallable("Position",
					new CallableParticlePositionInfo(this, par2, par4, par6));
			throw new ReportedException(var15);
		}
	}

	/**
	 * Spawns a particle. Arg: particleType, x, y, z, velX, velY, velZ
	 */
	public EntityFX doSpawnParticle(final String par1Str, final double par2,
			final double par4, final double par6, final double par8,
			final double par10, final double par12) {
		if (mc != null && mc.renderViewEntity != null
				&& mc.effectRenderer != null) {
			int var14 = mc.gameSettings.particleSetting;

			if (var14 == 1 && theWorld.rand.nextInt(3) == 0) {
				var14 = 2;
			}

			final double var15 = mc.renderViewEntity.posX - par2;
			final double var17 = mc.renderViewEntity.posY - par4;
			final double var19 = mc.renderViewEntity.posZ - par6;
			EntityFX var21 = null;

			if (par1Str.equals("hugeexplosion")) {
				if (Config.isAnimatedExplosion()) {
					mc.effectRenderer
							.addEffect(var21 = new EntityHugeExplodeFX(
									theWorld, par2, par4, par6, par8, par10,
									par12));
				}
			} else if (par1Str.equals("largeexplode")) {
				if (Config.isAnimatedExplosion()) {
					mc.effectRenderer
							.addEffect(var21 = new EntityLargeExplodeFX(
									renderEngine, theWorld, par2, par4, par6,
									par8, par10, par12));
				}
			} else if (par1Str.equals("fireworksSpark")) {
				mc.effectRenderer.addEffect(var21 = new EntityFireworkSparkFX(
						theWorld, par2, par4, par6, par8, par10, par12,
						mc.effectRenderer));
			}

			if (var21 != null) {
				return var21;
			} else {
				double var22 = 16.0D;
				if (par1Str.equals("crit")) {
					var22 = 196.0D;
				}

				if (var15 * var15 + var17 * var17 + var19 * var19 > var22
						* var22) {
					return null;
				} else if (var14 > 1) {
					return null;
				} else {
					if (par1Str.equals("bubble")) {
						var21 = new EntityBubbleFX(theWorld, par2, par4, par6,
								par8, par10, par12);
						CustomColorizer.updateWaterFX(var21, theWorld);
					} else if (par1Str.equals("suspended")) {
						if (Config.isWaterParticles()) {
							var21 = new EntitySuspendFX(theWorld, par2, par4,
									par6, par8, par10, par12);
						}
					} else if (par1Str.equals("depthsuspend")) {
						if (Config.isVoidParticles()) {
							var21 = new EntityAuraFX(theWorld, par2, par4,
									par6, par8, par10, par12);
						}
					} else if (par1Str.equals("townaura")) {
						var21 = new EntityAuraFX(theWorld, par2, par4, par6,
								par8, par10, par12);
						CustomColorizer.updateMyceliumFX(var21);
					} else if (par1Str.equals("crit")) {
						var21 = new EntityCritFX(theWorld, par2, par4, par6,
								par8, par10, par12);
					} else if (par1Str.equals("magicCrit")) {
						var21 = new EntityCritFX(theWorld, par2, par4, par6,
								par8, par10, par12);
						var21.setRBGColorF(var21.getRedColorF() * 0.3F,
								var21.getGreenColorF() * 0.8F,
								var21.getBlueColorF());
						var21.nextTextureIndexX();
					} else if (par1Str.equals("smoke")) {
						if (Config.isAnimatedSmoke()) {
							var21 = new EntitySmokeFX(theWorld, par2, par4,
									par6, par8, par10, par12);
						}
					} else if (par1Str.equals("mobSpell")) {
						if (Config.isPotionParticles()) {
							var21 = new EntitySpellParticleFX(theWorld, par2,
									par4, par6, 0.0D, 0.0D, 0.0D);
							var21.setRBGColorF((float) par8, (float) par10,
									(float) par12);
						}
					} else if (par1Str.equals("mobSpellAmbient")) {
						if (Config.isPotionParticles()) {
							var21 = new EntitySpellParticleFX(theWorld, par2,
									par4, par6, 0.0D, 0.0D, 0.0D);
							var21.setAlphaF(0.15F);
							var21.setRBGColorF((float) par8, (float) par10,
									(float) par12);
						}
					} else if (par1Str.equals("spell")) {
						if (Config.isPotionParticles()) {
							var21 = new EntitySpellParticleFX(theWorld, par2,
									par4, par6, par8, par10, par12);
						}
					} else if (par1Str.equals("instantSpell")) {
						if (Config.isPotionParticles()) {
							var21 = new EntitySpellParticleFX(theWorld, par2,
									par4, par6, par8, par10, par12);
							((EntitySpellParticleFX) var21)
									.setBaseSpellTextureIndex(144);
						}
					} else if (par1Str.equals("witchMagic")) {
						if (Config.isPotionParticles()) {
							var21 = new EntitySpellParticleFX(theWorld, par2,
									par4, par6, par8, par10, par12);
							((EntitySpellParticleFX) var21)
									.setBaseSpellTextureIndex(144);
							final float var26 = theWorld.rand.nextFloat() * 0.5F + 0.35F;
							var21.setRBGColorF(1.0F * var26, 0.0F * var26,
									1.0F * var26);
						}
					} else if (par1Str.equals("note")) {
						var21 = new EntityNoteFX(theWorld, par2, par4, par6,
								par8, par10, par12);
					} else if (par1Str.equals("portal")) {
						if (Config.isPortalParticles()) {
							var21 = new EntityPortalFX(theWorld, par2, par4,
									par6, par8, par10, par12);
							CustomColorizer.updatePortalFX(var21);
						}
					} else if (par1Str.equals("enchantmenttable")) {
						var21 = new EntityEnchantmentTableParticleFX(theWorld,
								par2, par4, par6, par8, par10, par12);
					} else if (par1Str.equals("explode")) {
						if (Config.isAnimatedExplosion()) {
							var21 = new EntityExplodeFX(theWorld, par2, par4,
									par6, par8, par10, par12);
						}
					} else if (par1Str.equals("flame")) {
						if (Config.isAnimatedFlame()) {
							var21 = new EntityFlameFX(theWorld, par2, par4,
									par6, par8, par10, par12);
						}
					} else if (par1Str.equals("lava")) {
						var21 = new EntityLavaFX(theWorld, par2, par4, par6);
					} else if (par1Str.equals("footstep")) {
						var21 = new EntityFootStepFX(renderEngine, theWorld,
								par2, par4, par6);
					} else if (par1Str.equals("splash")) {
						var21 = new EntitySplashFX(theWorld, par2, par4, par6,
								par8, par10, par12);
						CustomColorizer.updateWaterFX(var21, theWorld);
					} else if (par1Str.equals("largesmoke")) {
						if (Config.isAnimatedSmoke()) {
							var21 = new EntitySmokeFX(theWorld, par2, par4,
									par6, par8, par10, par12, 2.5F);
						}
					} else if (par1Str.equals("cloud")) {
						var21 = new EntityCloudFX(theWorld, par2, par4, par6,
								par8, par10, par12);
					} else if (par1Str.equals("reddust")) {
						if (Config.isAnimatedRedstone()) {
							var21 = new EntityReddustFX(theWorld, par2, par4,
									par6, (float) par8, (float) par10,
									(float) par12);
							CustomColorizer.updateReddustFX(var21, theWorld,
									var15, var17, var19);
						}
					} else if (par1Str.equals("snowballpoof")) {
						var21 = new EntityBreakingFX(theWorld, par2, par4,
								par6, Item.snowball, renderEngine);
					} else if (par1Str.equals("dripWater")) {
						if (Config.isDrippingWaterLava()) {
							var21 = new EntityDropParticleFX(theWorld, par2,
									par4, par6, Material.water);
						}
					} else if (par1Str.equals("dripLava")) {
						if (Config.isDrippingWaterLava()) {
							var21 = new EntityDropParticleFX(theWorld, par2,
									par4, par6, Material.lava);
						}
					} else if (par1Str.equals("snowshovel")) {
						var21 = new EntitySnowShovelFX(theWorld, par2, par4,
								par6, par8, par10, par12);
					} else if (par1Str.equals("slime")) {
						var21 = new EntityBreakingFX(theWorld, par2, par4,
								par6, Item.slimeBall, renderEngine);
					} else if (par1Str.equals("heart")) {
						var21 = new EntityHeartFX(theWorld, par2, par4, par6,
								par8, par10, par12);
					} else if (par1Str.equals("angryVillager")) {
						var21 = new EntityHeartFX(theWorld, par2, par4 + 0.5D,
								par6, par8, par10, par12);
						var21.setParticleTextureIndex(81);
						var21.setRBGColorF(1.0F, 1.0F, 1.0F);
					} else if (par1Str.equals("happyVillager")) {
						var21 = new EntityAuraFX(theWorld, par2, par4, par6,
								par8, par10, par12);
						var21.setParticleTextureIndex(82);
						var21.setRBGColorF(1.0F, 1.0F, 1.0F);
					} else if (par1Str.startsWith("iconcrack_")) {
						final int var30 = Integer.parseInt(par1Str
								.substring(par1Str.indexOf("_") + 1));
						var21 = new EntityBreakingFX(theWorld, par2, par4,
								par6, par8, par10, par12,
								Item.itemsList[var30], renderEngine);
					} else if (par1Str.startsWith("tilecrack_")) {
						final String[] var29 = par1Str.split("_", 3);
						final int var27 = Integer.parseInt(var29[1]);
						final int var28 = Integer.parseInt(var29[2]);
						var21 = new EntityDiggingFX(theWorld, par2, par4, par6,
								par8, par10, par12, Block.blocksList[var27], 0,
								var28, renderEngine).applyRenderColor(var28);
					}

					if (var21 != null) {
						mc.effectRenderer.addEffect(var21);
					}

					return var21;
				}
			}
		} else {
			return null;
		}
	}

	/**
	 * Called on all IWorldAccesses when an entity is created or loaded. On
	 * client worlds, starts downloading any necessary textures. On server
	 * worlds, adds the entity to the entity tracker.
	 */
	@Override
	public void onEntityCreate(final Entity par1Entity) {
		par1Entity.updateCloak();

		if (par1Entity.skinUrl != null) {
			renderEngine.obtainImageData(par1Entity.skinUrl,
					new ImageBufferDownload());
		}

		if (par1Entity.cloakUrl != null) {
			renderEngine.obtainImageData(par1Entity.cloakUrl,
					new ImageBufferDownload());

			if (par1Entity instanceof EntityPlayer) {
				final EntityPlayer var2 = (EntityPlayer) par1Entity;
				final ThreadDownloadImageData var3 = renderEngine
						.obtainImageData(var2.cloakUrl,
								new ImageBufferDownload());
				renderEngine.releaseImageData(var2.cloakUrl);
				final String var4 = "http://s.optifine.net/capes/"
						+ StringUtils.stripControlCodes(var2.username) + ".png";
				final ThreadDownloadImage var5 = new ThreadDownloadImage(var3,
						var4, new ImageBufferDownload());
				var5.start();

				if (!Config.isShowCapes()) {
					var2.cloakUrl = "";
				}
			}
		}

		if (Config.isRandomMobs()) {
			RandomMobs.entityLoaded(par1Entity);
		}
	}

	/**
	 * Called on all IWorldAccesses when an entity is unloaded or destroyed. On
	 * client worlds, releases any downloaded textures. On server worlds,
	 * removes the entity from the entity tracker.
	 */
	@Override
	public void onEntityDestroy(final Entity par1Entity) {
		if (par1Entity.skinUrl != null) {
			renderEngine.releaseImageData(par1Entity.skinUrl);
		}

		if (par1Entity.cloakUrl != null) {
			renderEngine.releaseImageData(par1Entity.cloakUrl);
		}
	}

	/**
	 * Deletes all display lists
	 */
	public void deleteAllDisplayLists() {
		GLAllocation.deleteDisplayLists(glRenderListBase);
	}

	@Override
	public void broadcastSound(final int par1, final int par2, final int par3,
			final int par4, final int par5) {
		switch (par1) {
		case 1013:
		case 1018:
			if (mc.renderViewEntity != null) {
				final double var7 = par2 - mc.renderViewEntity.posX;
				final double var9 = par3 - mc.renderViewEntity.posY;
				final double var11 = par4 - mc.renderViewEntity.posZ;
				final double var13 = Math.sqrt(var7 * var7 + var9 * var9
						+ var11 * var11);
				double var15 = mc.renderViewEntity.posX;
				double var17 = mc.renderViewEntity.posY;
				double var19 = mc.renderViewEntity.posZ;

				if (var13 > 0.0D) {
					var15 += var7 / var13 * 2.0D;
					var17 += var9 / var13 * 2.0D;
					var19 += var11 / var13 * 2.0D;
				}

				if (par1 == 1013) {
					theWorld.playSound(var15, var17, var19, "mob.wither.spawn",
							1.0F, 1.0F, false);
				} else if (par1 == 1018) {
					theWorld.playSound(var15, var17, var19,
							"mob.enderdragon.end", 5.0F, 1.0F, false);
				}
			}

		default:
		}
	}

	/**
	 * Plays a pre-canned sound effect along with potentially auxiliary
	 * data-driven one-shot behaviour (particles, etc).
	 */
	@Override
	public void playAuxSFX(final EntityPlayer par1EntityPlayer, final int par2,
			final int par3, final int par4, final int par5, final int par6) {
		final Random var7 = theWorld.rand;
		double var8;
		double var10;
		double var12;
		String var14;
		int var15;
		double var17;
		int var16;
		double var19;
		double var21;
		double var23;
		double var25;

		switch (par2) {
		case 1000:
			theWorld.playSound(par3, par4, par5, "random.click", 1.0F, 1.0F,
					false);
			break;

		case 1001:
			theWorld.playSound(par3, par4, par5, "random.click", 1.0F, 1.2F,
					false);
			break;

		case 1002:
			theWorld.playSound(par3, par4, par5, "random.bow", 1.0F, 1.2F,
					false);
			break;

		case 1003:
			if (Math.random() < 0.5D) {
				theWorld.playSound(par3 + 0.5D, par4 + 0.5D, par5 + 0.5D,
						"random.door_open", 1.0F,
						theWorld.rand.nextFloat() * 0.1F + 0.9F, false);
			} else {
				theWorld.playSound(par3 + 0.5D, par4 + 0.5D, par5 + 0.5D,
						"random.door_close", 1.0F,
						theWorld.rand.nextFloat() * 0.1F + 0.9F, false);
			}

			break;

		case 1004:
			theWorld.playSound(par3 + 0.5F, par4 + 0.5F, par5 + 0.5F,
					"random.fizz", 0.5F,
					2.6F + (var7.nextFloat() - var7.nextFloat()) * 0.8F, false);
			break;

		case 1005:
			if (Item.itemsList[par6] instanceof ItemRecord) {
				theWorld.playRecord(
						((ItemRecord) Item.itemsList[par6]).recordName, par3,
						par4, par5);
			} else {
				theWorld.playRecord((String) null, par3, par4, par5);
			}

			break;

		case 1007:
			theWorld.playSound(par3 + 0.5D, par4 + 0.5D, par5 + 0.5D,
					"mob.ghast.charge", 10.0F,
					(var7.nextFloat() - var7.nextFloat()) * 0.2F + 1.0F, false);
			break;

		case 1008:
			theWorld.playSound(par3 + 0.5D, par4 + 0.5D, par5 + 0.5D,
					"mob.ghast.fireball", 10.0F,
					(var7.nextFloat() - var7.nextFloat()) * 0.2F + 1.0F, false);
			break;

		case 1009:
			theWorld.playSound(par3 + 0.5D, par4 + 0.5D, par5 + 0.5D,
					"mob.ghast.fireball", 2.0F,
					(var7.nextFloat() - var7.nextFloat()) * 0.2F + 1.0F, false);
			break;

		case 1010:
			theWorld.playSound(par3 + 0.5D, par4 + 0.5D, par5 + 0.5D,
					"mob.zombie.wood", 2.0F,
					(var7.nextFloat() - var7.nextFloat()) * 0.2F + 1.0F, false);
			break;

		case 1011:
			theWorld.playSound(par3 + 0.5D, par4 + 0.5D, par5 + 0.5D,
					"mob.zombie.metal", 2.0F,
					(var7.nextFloat() - var7.nextFloat()) * 0.2F + 1.0F, false);
			break;

		case 1012:
			theWorld.playSound(par3 + 0.5D, par4 + 0.5D, par5 + 0.5D,
					"mob.zombie.woodbreak", 2.0F,
					(var7.nextFloat() - var7.nextFloat()) * 0.2F + 1.0F, false);
			break;

		case 1014:
			theWorld.playSound(par3 + 0.5D, par4 + 0.5D, par5 + 0.5D,
					"mob.wither.shoot", 2.0F,
					(var7.nextFloat() - var7.nextFloat()) * 0.2F + 1.0F, false);
			break;

		case 1015:
			theWorld.playSound(par3 + 0.5D, par4 + 0.5D, par5 + 0.5D,
					"mob.bat.takeoff", 0.05F,
					(var7.nextFloat() - var7.nextFloat()) * 0.2F + 1.0F, false);
			break;

		case 1016:
			theWorld.playSound(par3 + 0.5D, par4 + 0.5D, par5 + 0.5D,
					"mob.zombie.infect", 2.0F,
					(var7.nextFloat() - var7.nextFloat()) * 0.2F + 1.0F, false);
			break;

		case 1017:
			theWorld.playSound(par3 + 0.5D, par4 + 0.5D, par5 + 0.5D,
					"mob.zombie.unfect", 2.0F,
					(var7.nextFloat() - var7.nextFloat()) * 0.2F + 1.0F, false);
			break;

		case 1020:
			theWorld.playSound(par3 + 0.5F, par4 + 0.5F, par5 + 0.5F,
					"random.anvil_break", 1.0F,
					theWorld.rand.nextFloat() * 0.1F + 0.9F, false);
			break;

		case 1021:
			theWorld.playSound(par3 + 0.5F, par4 + 0.5F, par5 + 0.5F,
					"random.anvil_use", 1.0F,
					theWorld.rand.nextFloat() * 0.1F + 0.9F, false);
			break;

		case 1022:
			theWorld.playSound(par3 + 0.5F, par4 + 0.5F, par5 + 0.5F,
					"random.anvil_land", 0.3F,
					theWorld.rand.nextFloat() * 0.1F + 0.9F, false);
			break;

		case 2000:
			final int var27 = par6 % 3 - 1;
			final int var28 = par6 / 3 % 3 - 1;
			var10 = par3 + var27 * 0.6D + 0.5D;
			var12 = par4 + 0.5D;
			final double var29 = par5 + var28 * 0.6D + 0.5D;

			for (int var44 = 0; var44 < 10; ++var44) {
				final double var43 = var7.nextDouble() * 0.2D + 0.01D;
				final double var48 = var10 + var27 * 0.01D
						+ (var7.nextDouble() - 0.5D) * var28 * 0.5D;
				var25 = var12 + (var7.nextDouble() - 0.5D) * 0.5D;
				var17 = var29 + var28 * 0.01D + (var7.nextDouble() - 0.5D)
						* var27 * 0.5D;
				var19 = var27 * var43 + var7.nextGaussian() * 0.01D;
				var21 = -0.03D + var7.nextGaussian() * 0.01D;
				var23 = var28 * var43 + var7.nextGaussian() * 0.01D;
				spawnParticle("smoke", var48, var25, var17, var19, var21, var23);
			}

			return;

		case 2001:
			var16 = par6 & 4095;

			if (var16 > 0) {
				final Block var42 = Block.blocksList[var16];
				mc.sndManager.playSound(var42.stepSound.getBreakSound(),
						par3 + 0.5F, par4 + 0.5F, par5 + 0.5F,
						(var42.stepSound.getVolume() + 1.0F) / 2.0F,
						var42.stepSound.getPitch() * 0.8F);
			}

			mc.effectRenderer.addBlockDestroyEffects(par3, par4, par5,
					par6 & 4095, par6 >> 12 & 255);
			break;

		case 2002:
			var8 = par3;
			var10 = par4;
			var12 = par5;
			var14 = "iconcrack_" + Item.potion.itemID;

			for (var15 = 0; var15 < 8; ++var15) {
				spawnParticle(var14, var8, var10, var12,
						var7.nextGaussian() * 0.15D, var7.nextDouble() * 0.2D,
						var7.nextGaussian() * 0.15D);
			}

			var15 = Item.potion.getColorFromDamage(par6);
			final float var31 = (var15 >> 16 & 255) / 255.0F;
			final float var32 = (var15 >> 8 & 255) / 255.0F;
			final float var33 = (var15 >> 0 & 255) / 255.0F;
			String var34 = "spell";

			if (Item.potion.isEffectInstant(par6)) {
				var34 = "instantSpell";
			}

			for (var16 = 0; var16 < 100; ++var16) {
				var25 = var7.nextDouble() * 4.0D;
				var17 = var7.nextDouble() * Math.PI * 2.0D;
				var19 = Math.cos(var17) * var25;
				var21 = 0.01D + var7.nextDouble() * 0.5D;
				var23 = Math.sin(var17) * var25;
				final EntityFX var45 = doSpawnParticle(var34, var8 + var19
						* 0.1D, var10 + 0.3D, var12 + var23 * 0.1D, var19,
						var21, var23);

				if (var45 != null) {
					final float var46 = 0.75F + var7.nextFloat() * 0.25F;
					var45.setRBGColorF(var31 * var46, var32 * var46, var33
							* var46);
					var45.multiplyVelocity((float) var25);
				}
			}

			theWorld.playSound(par3 + 0.5D, par4 + 0.5D, par5 + 0.5D,
					"random.glass", 1.0F,
					theWorld.rand.nextFloat() * 0.1F + 0.9F, false);
			break;

		case 2003:
			var8 = par3 + 0.5D;
			var10 = par4;
			var12 = par5 + 0.5D;
			var14 = "iconcrack_" + Item.eyeOfEnder.itemID;

			for (var15 = 0; var15 < 8; ++var15) {
				spawnParticle(var14, var8, var10, var12,
						var7.nextGaussian() * 0.15D, var7.nextDouble() * 0.2D,
						var7.nextGaussian() * 0.15D);
			}

			for (double var47 = 0.0D; var47 < Math.PI * 2D; var47 += 0.15707963267948966D) {
				spawnParticle("portal", var8 + Math.cos(var47) * 5.0D,
						var10 - 0.4D, var12 + Math.sin(var47) * 5.0D,
						Math.cos(var47) * -5.0D, 0.0D, Math.sin(var47) * -5.0D);
				spawnParticle("portal", var8 + Math.cos(var47) * 5.0D,
						var10 - 0.4D, var12 + Math.sin(var47) * 5.0D,
						Math.cos(var47) * -7.0D, 0.0D, Math.sin(var47) * -7.0D);
			}

			return;

		case 2004:
			for (int var35 = 0; var35 < 20; ++var35) {
				final double var36 = par3 + 0.5D
						+ (theWorld.rand.nextFloat() - 0.5D) * 2.0D;
				final double var38 = par4 + 0.5D
						+ (theWorld.rand.nextFloat() - 0.5D) * 2.0D;
				final double var40 = par5 + 0.5D
						+ (theWorld.rand.nextFloat() - 0.5D) * 2.0D;
				theWorld.spawnParticle("smoke", var36, var38, var40, 0.0D,
						0.0D, 0.0D);
				theWorld.spawnParticle("flame", var36, var38, var40, 0.0D,
						0.0D, 0.0D);
			}

			return;

		case 2005:
			ItemDye.func_96603_a(theWorld, par3, par4, par5, par6);
		}
	}

	/**
	 * Starts (or continues) destroying a block with given ID at the given
	 * coordinates for the given partially destroyed value
	 */
	@Override
	public void destroyBlockPartially(final int par1, final int par2,
			final int par3, final int par4, final int par5) {
		if (par5 >= 0 && par5 < 10) {
			DestroyBlockProgress var6 = (DestroyBlockProgress) damagedBlocks
					.get(Integer.valueOf(par1));

			if (var6 == null || var6.getPartialBlockX() != par2
					|| var6.getPartialBlockY() != par3
					|| var6.getPartialBlockZ() != par4) {
				var6 = new DestroyBlockProgress(par1, par2, par3, par4);
				damagedBlocks.put(Integer.valueOf(par1), var6);
			}

			var6.setPartialBlockDamage(par5);
			var6.setCloudUpdateTick(cloudTickCounter);
		} else {
			damagedBlocks.remove(Integer.valueOf(par1));
		}
	}

	public void registerDestroyBlockIcons(final IconRegister par1IconRegister) {
		destroyBlockIcons = new Icon[10];

		for (int var2 = 0; var2 < destroyBlockIcons.length; ++var2) {
			destroyBlockIcons[var2] = par1IconRegister.registerIcon("destroy_"
					+ var2);
		}
	}

	public void setAllRenderersVisible() {
		if (worldRenderers != null) {
			for (int var1 = 0; var1 < worldRenderers.length; ++var1) {
				worldRenderers[var1].isVisible = true;
			}
		}
	}

	public boolean isMoving(final EntityLiving var1) {
		final boolean var2 = isMovingNow(var1);

		if (var2) {
			lastMovedTime = System.currentTimeMillis();
			return true;
		} else {
			return System.currentTimeMillis() - lastMovedTime < 2000L;
		}
	}

	private boolean isMovingNow(final EntityLiving var1) {
		final double var2 = 0.001D;
		return var1.isJumping ? true
				: var1.isSneaking() ? true
						: var1.prevSwingProgress > var2 ? true
								: mc.mouseHelper.deltaX != 0 ? true
										: mc.mouseHelper.deltaY != 0 ? true
												: Math.abs(var1.posX
														- var1.prevPosX) > var2 ? true
														: Math.abs(var1.posY
																- var1.prevPosY) > var2 ? true
																: Math.abs(var1.posZ
																		- var1.prevPosZ) > var2;
	}

	public boolean isActing() {
		final boolean var1 = isActingNow();

		if (var1) {
			lastActionTime = System.currentTimeMillis();
			return true;
		} else {
			return System.currentTimeMillis() - lastActionTime < 500L;
		}
	}

	public boolean isActingNow() {
		return Mouse.isButtonDown(0) ? true : Mouse.isButtonDown(1);
	}

	public int renderAllSortedRenderers(final int var1, final double var2) {
		return renderSortedRenderers(0, sortedWorldRenderers.length, var1, var2);
	}

	public void updateCapes() {
		if (theWorld != null) {
			final boolean var1 = Config.isShowCapes();
			final List var2 = theWorld.playerEntities;

			for (int var3 = 0; var3 < var2.size(); ++var3) {
				final Entity var4 = (Entity) var2.get(var3);

				if (var4 instanceof EntityPlayer) {
					final EntityPlayer var5 = (EntityPlayer) var4;

					if (var1) {
						var5.cloakUrl = var5.cloakUrl;
					} else {
						var5.cloakUrl = "";
					}
				}
			}
		}
	}
}
