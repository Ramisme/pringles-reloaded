package net.minecraft.src;

public class TileEntitySign extends TileEntity {
	/** An array of four strings storing the lines of text on the sign. */
	public String[] signText = new String[] { "", "", "", "" };

	/**
	 * The index of the line currently being edited. Only used on client side,
	 * but defined on both. Note this is only really used when the > < are going
	 * to be visible.
	 */
	public int lineBeingEdited = -1;
	private boolean isEditable = true;

	/**
	 * Writes a tile entity to NBT.
	 */
	@Override
	public void writeToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeToNBT(par1NBTTagCompound);
		par1NBTTagCompound.setString("Text1", signText[0]);
		par1NBTTagCompound.setString("Text2", signText[1]);
		par1NBTTagCompound.setString("Text3", signText[2]);
		par1NBTTagCompound.setString("Text4", signText[3]);
	}

	/**
	 * Reads a tile entity from NBT.
	 */
	@Override
	public void readFromNBT(final NBTTagCompound par1NBTTagCompound) {
		isEditable = false;
		super.readFromNBT(par1NBTTagCompound);

		for (int var2 = 0; var2 < 4; ++var2) {
			signText[var2] = par1NBTTagCompound.getString("Text" + (var2 + 1));

			if (signText[var2].length() > 15) {
				signText[var2] = signText[var2].substring(0, 15);
			}
		}
	}

	/**
	 * Overriden in a sign to provide the text.
	 */
	@Override
	public Packet getDescriptionPacket() {
		final String[] var1 = new String[4];
		System.arraycopy(signText, 0, var1, 0, 4);
		return new Packet130UpdateSign(xCoord, yCoord, zCoord, var1);
	}

	public boolean isEditable() {
		return isEditable;
	}

	/**
	 * Sets the sign's isEditable flag to the specified parameter.
	 */
	public void setEditable(final boolean par1) {
		isEditable = par1;
	}
}
