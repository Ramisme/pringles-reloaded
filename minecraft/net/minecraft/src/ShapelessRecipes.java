package net.minecraft.src;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ShapelessRecipes implements IRecipe {
	/** Is the ItemStack that you get when craft the recipe. */
	private final ItemStack recipeOutput;

	/** Is a List of ItemStack that composes the recipe. */
	private final List recipeItems;

	public ShapelessRecipes(final ItemStack par1ItemStack, final List par2List) {
		recipeOutput = par1ItemStack;
		recipeItems = par2List;
	}

	@Override
	public ItemStack getRecipeOutput() {
		return recipeOutput;
	}

	/**
	 * Used to check if a recipe matches current crafting inventory
	 */
	@Override
	public boolean matches(final InventoryCrafting par1InventoryCrafting,
			final World par2World) {
		final ArrayList var3 = new ArrayList(recipeItems);

		for (int var4 = 0; var4 < 3; ++var4) {
			for (int var5 = 0; var5 < 3; ++var5) {
				final ItemStack var6 = par1InventoryCrafting
						.getStackInRowAndColumn(var5, var4);

				if (var6 != null) {
					boolean var7 = false;
					final Iterator var8 = var3.iterator();

					while (var8.hasNext()) {
						final ItemStack var9 = (ItemStack) var8.next();

						if (var6.itemID == var9.itemID
								&& (var9.getItemDamage() == 32767 || var6
										.getItemDamage() == var9
										.getItemDamage())) {
							var7 = true;
							var3.remove(var9);
							break;
						}
					}

					if (!var7) {
						return false;
					}
				}
			}
		}

		return var3.isEmpty();
	}

	/**
	 * Returns an Item that is the result of this recipe
	 */
	@Override
	public ItemStack getCraftingResult(
			final InventoryCrafting par1InventoryCrafting) {
		return recipeOutput.copy();
	}

	/**
	 * Returns the size of the recipe area
	 */
	@Override
	public int getRecipeSize() {
		return recipeItems.size();
	}
}
