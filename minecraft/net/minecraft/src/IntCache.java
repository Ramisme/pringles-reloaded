package net.minecraft.src;

import java.util.ArrayList;
import java.util.List;

public class IntCache {
	private static int intCacheSize = 256;

	/**
	 * A list of pre-allocated int[256] arrays that are currently unused and can
	 * be returned by getIntCache()
	 */
	private static List freeSmallArrays = new ArrayList();

	/**
	 * A list of pre-allocated int[256] arrays that were previously returned by
	 * getIntCache() and which will not be re- used again until resetIntCache()
	 * is called.
	 */
	private static List inUseSmallArrays = new ArrayList();

	/**
	 * A list of pre-allocated int[cacheSize] arrays that are currently unused
	 * and can be returned by getIntCache()
	 */
	private static List freeLargeArrays = new ArrayList();

	/**
	 * A list of pre-allocated int[cacheSize] arrays that were previously
	 * returned by getIntCache() and which will not be re-used again until
	 * resetIntCache() is called.
	 */
	private static List inUseLargeArrays = new ArrayList();

	public static synchronized int[] getIntCache(final int par0) {
		int[] var1;

		if (par0 <= 256) {
			if (IntCache.freeSmallArrays.isEmpty()) {
				var1 = new int[256];
				IntCache.inUseSmallArrays.add(var1);
				return var1;
			} else {
				var1 = (int[]) IntCache.freeSmallArrays
						.remove(IntCache.freeSmallArrays.size() - 1);
				IntCache.inUseSmallArrays.add(var1);
				return var1;
			}
		} else if (par0 > IntCache.intCacheSize) {
			IntCache.intCacheSize = par0;
			IntCache.freeLargeArrays.clear();
			IntCache.inUseLargeArrays.clear();
			var1 = new int[IntCache.intCacheSize];
			IntCache.inUseLargeArrays.add(var1);
			return var1;
		} else if (IntCache.freeLargeArrays.isEmpty()) {
			var1 = new int[IntCache.intCacheSize];
			IntCache.inUseLargeArrays.add(var1);
			return var1;
		} else {
			var1 = (int[]) IntCache.freeLargeArrays
					.remove(IntCache.freeLargeArrays.size() - 1);
			IntCache.inUseLargeArrays.add(var1);
			return var1;
		}
	}

	/**
	 * Mark all pre-allocated arrays as available for re-use by moving them to
	 * the appropriate free lists.
	 */
	public static synchronized void resetIntCache() {
		if (!IntCache.freeLargeArrays.isEmpty()) {
			IntCache.freeLargeArrays
					.remove(IntCache.freeLargeArrays.size() - 1);
		}

		if (!IntCache.freeSmallArrays.isEmpty()) {
			IntCache.freeSmallArrays
					.remove(IntCache.freeSmallArrays.size() - 1);
		}

		IntCache.freeLargeArrays.addAll(IntCache.inUseLargeArrays);
		IntCache.freeSmallArrays.addAll(IntCache.inUseSmallArrays);
		IntCache.inUseLargeArrays.clear();
		IntCache.inUseSmallArrays.clear();
	}

	public static synchronized String func_85144_b() {
		return "cache: " + IntCache.freeLargeArrays.size() + ", tcache: "
				+ IntCache.freeSmallArrays.size() + ", allocated: "
				+ IntCache.inUseLargeArrays.size() + ", tallocated: "
				+ IntCache.inUseSmallArrays.size();
	}
}
