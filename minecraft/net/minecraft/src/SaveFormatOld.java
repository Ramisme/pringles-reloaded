package net.minecraft.src;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

public class SaveFormatOld implements ISaveFormat {
	/**
	 * Reference to the File object representing the directory for the world
	 * saves
	 */
	protected final File savesDirectory;

	public SaveFormatOld(final File par1File) {
		if (!par1File.exists()) {
			par1File.mkdirs();
		}

		savesDirectory = par1File;
	}

	@Override
	public List getSaveList() throws AnvilConverterException {
		final ArrayList var1 = new ArrayList();

		for (int var2 = 0; var2 < 5; ++var2) {
			final String var3 = "World" + (var2 + 1);
			final WorldInfo var4 = getWorldInfo(var3);

			if (var4 != null) {
				var1.add(new SaveFormatComparator(var3, "", var4
						.getLastTimePlayed(), var4.getSizeOnDisk(), var4
						.getGameType(), false, var4.isHardcoreModeEnabled(),
						var4.areCommandsAllowed()));
			}
		}

		return var1;
	}

	@Override
	public void flushCache() {
	}

	/**
	 * gets the world info
	 */
	@Override
	public WorldInfo getWorldInfo(final String par1Str) {
		final File var2 = new File(savesDirectory, par1Str);

		if (!var2.exists()) {
			return null;
		} else {
			File var3 = new File(var2, "level.dat");
			NBTTagCompound var4;
			NBTTagCompound var5;

			if (var3.exists()) {
				try {
					var4 = CompressedStreamTools
							.readCompressed(new FileInputStream(var3));
					var5 = var4.getCompoundTag("Data");
					return new WorldInfo(var5);
				} catch (final Exception var7) {
					var7.printStackTrace();
				}
			}

			var3 = new File(var2, "level.dat_old");

			if (var3.exists()) {
				try {
					var4 = CompressedStreamTools
							.readCompressed(new FileInputStream(var3));
					var5 = var4.getCompoundTag("Data");
					return new WorldInfo(var5);
				} catch (final Exception var6) {
					var6.printStackTrace();
				}
			}

			return null;
		}
	}

	/**
	 * @args: Takes two arguments - first the name of the directory containing
	 *        the world and second the new name for that world. @desc: Renames
	 *        the world by storing the new name in level.dat. It does *not*
	 *        rename the directory containing the world data.
	 */
	@Override
	public void renameWorld(final String par1Str, final String par2Str) {
		final File var3 = new File(savesDirectory, par1Str);

		if (var3.exists()) {
			final File var4 = new File(var3, "level.dat");

			if (var4.exists()) {
				try {
					final NBTTagCompound var5 = CompressedStreamTools
							.readCompressed(new FileInputStream(var4));
					final NBTTagCompound var6 = var5.getCompoundTag("Data");
					var6.setString("LevelName", par2Str);
					CompressedStreamTools.writeCompressed(var5,
							new FileOutputStream(var4));
				} catch (final Exception var7) {
					var7.printStackTrace();
				}
			}
		}
	}

	/**
	 * @args: Takes one argument - the name of the directory of the world to
	 *        delete. @desc: Delete the world by deleting the associated
	 *        directory recursively.
	 */
	@Override
	public boolean deleteWorldDirectory(final String par1Str) {
		final File var2 = new File(savesDirectory, par1Str);

		if (!var2.exists()) {
			return true;
		} else {
			System.out.println("Deleting level " + par1Str);

			for (int var3 = 1; var3 <= 5; ++var3) {
				System.out.println("Attempt " + var3 + "...");

				if (SaveFormatOld.deleteFiles(var2.listFiles())) {
					break;
				}

				System.out.println("Unsuccessful in deleting contents.");

				if (var3 < 5) {
					try {
						Thread.sleep(500L);
					} catch (final InterruptedException var5) {
						;
					}
				}
			}

			return var2.delete();
		}
	}

	/**
	 * @args: Takes one argument - the list of files and directories to delete.
	 *        @desc: Deletes the files and directory listed in the list
	 *        recursively.
	 */
	protected static boolean deleteFiles(final File[] par0ArrayOfFile) {
		for (final File element : par0ArrayOfFile) {
			final File var2 = element;
			System.out.println("Deleting " + var2);

			if (var2.isDirectory()
					&& !SaveFormatOld.deleteFiles(var2.listFiles())) {
				System.out.println("Couldn\'t delete directory " + var2);
				return false;
			}

			if (!var2.delete()) {
				System.out.println("Couldn\'t delete file " + var2);
				return false;
			}
		}

		return true;
	}

	/**
	 * Returns back a loader for the specified save directory
	 */
	@Override
	public ISaveHandler getSaveLoader(final String par1Str, final boolean par2) {
		return new SaveHandler(savesDirectory, par1Str, par2);
	}

	/**
	 * Checks if the save directory uses the old map format
	 */
	@Override
	public boolean isOldMapFormat(final String par1Str) {
		return false;
	}

	/**
	 * Converts the specified map to the new map format. Args: worldName,
	 * loadingScreen
	 */
	@Override
	public boolean convertMapFormat(final String par1Str,
			final IProgressUpdate par2IProgressUpdate) {
		return false;
	}

	/**
	 * Return whether the given world can be loaded.
	 */
	@Override
	public boolean canLoadWorld(final String par1Str) {
		final File var2 = new File(savesDirectory, par1Str);
		return var2.isDirectory();
	}
}
