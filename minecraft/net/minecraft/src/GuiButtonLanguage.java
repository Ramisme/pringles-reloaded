package net.minecraft.src;

import net.minecraft.client.Minecraft;

import org.lwjgl.opengl.GL11;

public class GuiButtonLanguage extends GuiButton {
	public GuiButtonLanguage(final int par1, final int par2, final int par3) {
		super(par1, par2, par3, 20, 20, "");
	}

	/**
	 * Draws this button to the screen.
	 */
	@Override
	public void drawButton(final Minecraft par1Minecraft, final int par2,
			final int par3) {
		if (drawButton) {
			par1Minecraft.renderEngine.bindTexture("/gui/gui.png");
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			final boolean var4 = par2 >= xPosition && par3 >= yPosition
					&& par2 < xPosition + width && par3 < yPosition + height;
			int var5 = 106;

			if (var4) {
				var5 += height;
			}

			drawTexturedModalRect(xPosition, yPosition, 0, var5, width, height);
		}
	}
}
