package net.minecraft.src;

public class MaterialPortal extends Material {
	public MaterialPortal(final MapColor par1MapColor) {
		super(par1MapColor);
	}

	@Override
	public boolean isSolid() {
		return false;
	}

	/**
	 * Will prevent grass from growing on dirt underneath and kill any grass
	 * below it if it returns true
	 */
	@Override
	public boolean getCanBlockGrass() {
		return false;
	}

	/**
	 * Returns if this material is considered solid or not
	 */
	@Override
	public boolean blocksMovement() {
		return false;
	}
}
