package net.minecraft.src;

import org.lwjgl.opengl.GL11;

public class RenderCreeper extends RenderLiving {
	/** The creeper model. */
	private final ModelBase creeperModel = new ModelCreeper(2.0F);

	public RenderCreeper() {
		super(new ModelCreeper(), 0.5F);
	}

	/**
	 * Updates creeper scale in prerender callback
	 */
	protected void updateCreeperScale(final EntityCreeper par1EntityCreeper,
			final float par2) {
		float var4 = par1EntityCreeper.getCreeperFlashIntensity(par2);
		final float var5 = 1.0F + MathHelper.sin(var4 * 100.0F) * var4 * 0.01F;

		if (var4 < 0.0F) {
			var4 = 0.0F;
		}

		if (var4 > 1.0F) {
			var4 = 1.0F;
		}

		var4 *= var4;
		var4 *= var4;
		final float var6 = (1.0F + var4 * 0.4F) * var5;
		final float var7 = (1.0F + var4 * 0.1F) / var5;
		GL11.glScalef(var6, var7, var6);
	}

	/**
	 * Updates color multiplier based on creeper state called by
	 * getColorMultiplier
	 */
	protected int updateCreeperColorMultiplier(
			final EntityCreeper par1EntityCreeper, final float par2,
			final float par3) {
		final float var5 = par1EntityCreeper.getCreeperFlashIntensity(par3);

		if ((int) (var5 * 10.0F) % 2 == 0) {
			return 0;
		} else {
			int var6 = (int) (var5 * 0.2F * 255.0F);

			if (var6 < 0) {
				var6 = 0;
			}

			if (var6 > 255) {
				var6 = 255;
			}

			final short var7 = 255;
			final short var8 = 255;
			final short var9 = 255;
			return var6 << 24 | var7 << 16 | var8 << 8 | var9;
		}
	}

	/**
	 * A method used to render a creeper's powered form as a pass model.
	 */
	protected int renderCreeperPassModel(final EntityCreeper par1EntityCreeper,
			final int par2, final float par3) {
		if (par1EntityCreeper.getPowered()) {
			if (par1EntityCreeper.isInvisible()) {
				GL11.glDepthMask(false);
			} else {
				GL11.glDepthMask(true);
			}

			if (par2 == 1) {
				final float var4 = par1EntityCreeper.ticksExisted + par3;
				loadTexture("/armor/power.png");
				GL11.glMatrixMode(GL11.GL_TEXTURE);
				GL11.glLoadIdentity();
				final float var5 = var4 * 0.01F;
				final float var6 = var4 * 0.01F;
				GL11.glTranslatef(var5, var6, 0.0F);
				setRenderPassModel(creeperModel);
				GL11.glMatrixMode(GL11.GL_MODELVIEW);
				GL11.glEnable(GL11.GL_BLEND);
				final float var7 = 0.5F;
				GL11.glColor4f(var7, var7, var7, 1.0F);
				GL11.glDisable(GL11.GL_LIGHTING);
				GL11.glBlendFunc(GL11.GL_ONE, GL11.GL_ONE);
				return 1;
			}

			if (par2 == 2) {
				GL11.glMatrixMode(GL11.GL_TEXTURE);
				GL11.glLoadIdentity();
				GL11.glMatrixMode(GL11.GL_MODELVIEW);
				GL11.glEnable(GL11.GL_LIGHTING);
				GL11.glDisable(GL11.GL_BLEND);
			}
		}

		return -1;
	}

	protected int func_77061_b(final EntityCreeper par1EntityCreeper,
			final int par2, final float par3) {
		return -1;
	}

	/**
	 * Allows the render to do any OpenGL state modifications necessary before
	 * the model is rendered. Args: entityLiving, partialTickTime
	 */
	@Override
	protected void preRenderCallback(final EntityLiving par1EntityLiving,
			final float par2) {
		updateCreeperScale((EntityCreeper) par1EntityLiving, par2);
	}

	/**
	 * Returns an ARGB int color back. Args: entityLiving, lightBrightness,
	 * partialTickTime
	 */
	@Override
	protected int getColorMultiplier(final EntityLiving par1EntityLiving,
			final float par2, final float par3) {
		return updateCreeperColorMultiplier((EntityCreeper) par1EntityLiving,
				par2, par3);
	}

	/**
	 * Queries whether should render the specified pass or not.
	 */
	@Override
	protected int shouldRenderPass(final EntityLiving par1EntityLiving,
			final int par2, final float par3) {
		return renderCreeperPassModel((EntityCreeper) par1EntityLiving, par2,
				par3);
	}

	@Override
	protected int inheritRenderPass(final EntityLiving par1EntityLiving,
			final int par2, final float par3) {
		return func_77061_b((EntityCreeper) par1EntityLiving, par2, par3);
	}
}
