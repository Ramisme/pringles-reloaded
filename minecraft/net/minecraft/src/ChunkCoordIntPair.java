package net.minecraft.src;

public class ChunkCoordIntPair {
	/** The X position of this Chunk Coordinate Pair */
	public final int chunkXPos;

	/** The Z position of this Chunk Coordinate Pair */
	public final int chunkZPos;

	public ChunkCoordIntPair(final int par1, final int par2) {
		chunkXPos = par1;
		chunkZPos = par2;
	}

	/**
	 * converts a chunk coordinate pair to an integer (suitable for hashing)
	 */
	public static long chunkXZ2Int(final int par0, final int par1) {
		return par0 & 4294967295L | (par1 & 4294967295L) << 32;
	}

	@Override
	public int hashCode() {
		final long var1 = ChunkCoordIntPair.chunkXZ2Int(chunkXPos, chunkZPos);
		final int var3 = (int) var1;
		final int var4 = (int) (var1 >> 32);
		return var3 ^ var4;
	}

	@Override
	public boolean equals(final Object par1Obj) {
		final ChunkCoordIntPair var2 = (ChunkCoordIntPair) par1Obj;
		return var2.chunkXPos == chunkXPos && var2.chunkZPos == chunkZPos;
	}

	public int getCenterXPos() {
		return (chunkXPos << 4) + 8;
	}

	public int getCenterZPosition() {
		return (chunkZPos << 4) + 8;
	}

	public ChunkPosition getChunkPosition(final int par1) {
		return new ChunkPosition(getCenterXPos(), par1, getCenterZPosition());
	}

	@Override
	public String toString() {
		return "[" + chunkXPos + ", " + chunkZPos + "]";
	}
}
