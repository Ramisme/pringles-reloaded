package net.minecraft.src;

import java.util.List;
import java.util.Random;

abstract class ComponentStronghold extends StructureComponent {
	protected ComponentStronghold(final int par1) {
		super(par1);
	}

	/**
	 * builds a door of the enumerated types (empty opening is a door)
	 */
	protected void placeDoor(final World par1World, final Random par2Random,
			final StructureBoundingBox par3StructureBoundingBox,
			final EnumDoor par4EnumDoor, final int par5, final int par6,
			final int par7) {
		switch (EnumDoorHelper.doorEnum[par4EnumDoor.ordinal()]) {
		case 1:
		default:
			fillWithBlocks(par1World, par3StructureBoundingBox, par5, par6,
					par7, par5 + 3 - 1, par6 + 3 - 1, par7, 0, 0, false);
			break;

		case 2:
			placeBlockAtCurrentPosition(par1World, Block.stoneBrick.blockID, 0,
					par5, par6, par7, par3StructureBoundingBox);
			placeBlockAtCurrentPosition(par1World, Block.stoneBrick.blockID, 0,
					par5, par6 + 1, par7, par3StructureBoundingBox);
			placeBlockAtCurrentPosition(par1World, Block.stoneBrick.blockID, 0,
					par5, par6 + 2, par7, par3StructureBoundingBox);
			placeBlockAtCurrentPosition(par1World, Block.stoneBrick.blockID, 0,
					par5 + 1, par6 + 2, par7, par3StructureBoundingBox);
			placeBlockAtCurrentPosition(par1World, Block.stoneBrick.blockID, 0,
					par5 + 2, par6 + 2, par7, par3StructureBoundingBox);
			placeBlockAtCurrentPosition(par1World, Block.stoneBrick.blockID, 0,
					par5 + 2, par6 + 1, par7, par3StructureBoundingBox);
			placeBlockAtCurrentPosition(par1World, Block.stoneBrick.blockID, 0,
					par5 + 2, par6, par7, par3StructureBoundingBox);
			placeBlockAtCurrentPosition(par1World, Block.doorWood.blockID, 0,
					par5 + 1, par6, par7, par3StructureBoundingBox);
			placeBlockAtCurrentPosition(par1World, Block.doorWood.blockID, 8,
					par5 + 1, par6 + 1, par7, par3StructureBoundingBox);
			break;

		case 3:
			placeBlockAtCurrentPosition(par1World, 0, 0, par5 + 1, par6, par7,
					par3StructureBoundingBox);
			placeBlockAtCurrentPosition(par1World, 0, 0, par5 + 1, par6 + 1,
					par7, par3StructureBoundingBox);
			placeBlockAtCurrentPosition(par1World, Block.fenceIron.blockID, 0,
					par5, par6, par7, par3StructureBoundingBox);
			placeBlockAtCurrentPosition(par1World, Block.fenceIron.blockID, 0,
					par5, par6 + 1, par7, par3StructureBoundingBox);
			placeBlockAtCurrentPosition(par1World, Block.fenceIron.blockID, 0,
					par5, par6 + 2, par7, par3StructureBoundingBox);
			placeBlockAtCurrentPosition(par1World, Block.fenceIron.blockID, 0,
					par5 + 1, par6 + 2, par7, par3StructureBoundingBox);
			placeBlockAtCurrentPosition(par1World, Block.fenceIron.blockID, 0,
					par5 + 2, par6 + 2, par7, par3StructureBoundingBox);
			placeBlockAtCurrentPosition(par1World, Block.fenceIron.blockID, 0,
					par5 + 2, par6 + 1, par7, par3StructureBoundingBox);
			placeBlockAtCurrentPosition(par1World, Block.fenceIron.blockID, 0,
					par5 + 2, par6, par7, par3StructureBoundingBox);
			break;

		case 4:
			placeBlockAtCurrentPosition(par1World, Block.stoneBrick.blockID, 0,
					par5, par6, par7, par3StructureBoundingBox);
			placeBlockAtCurrentPosition(par1World, Block.stoneBrick.blockID, 0,
					par5, par6 + 1, par7, par3StructureBoundingBox);
			placeBlockAtCurrentPosition(par1World, Block.stoneBrick.blockID, 0,
					par5, par6 + 2, par7, par3StructureBoundingBox);
			placeBlockAtCurrentPosition(par1World, Block.stoneBrick.blockID, 0,
					par5 + 1, par6 + 2, par7, par3StructureBoundingBox);
			placeBlockAtCurrentPosition(par1World, Block.stoneBrick.blockID, 0,
					par5 + 2, par6 + 2, par7, par3StructureBoundingBox);
			placeBlockAtCurrentPosition(par1World, Block.stoneBrick.blockID, 0,
					par5 + 2, par6 + 1, par7, par3StructureBoundingBox);
			placeBlockAtCurrentPosition(par1World, Block.stoneBrick.blockID, 0,
					par5 + 2, par6, par7, par3StructureBoundingBox);
			placeBlockAtCurrentPosition(par1World, Block.doorIron.blockID, 0,
					par5 + 1, par6, par7, par3StructureBoundingBox);
			placeBlockAtCurrentPosition(par1World, Block.doorIron.blockID, 8,
					par5 + 1, par6 + 1, par7, par3StructureBoundingBox);
			placeBlockAtCurrentPosition(par1World, Block.stoneButton.blockID,
					getMetadataWithOffset(Block.stoneButton.blockID, 4),
					par5 + 2, par6 + 1, par7 + 1, par3StructureBoundingBox);
			placeBlockAtCurrentPosition(par1World, Block.stoneButton.blockID,
					getMetadataWithOffset(Block.stoneButton.blockID, 3),
					par5 + 2, par6 + 1, par7 - 1, par3StructureBoundingBox);
		}
	}

	protected EnumDoor getRandomDoor(final Random par1Random) {
		final int var2 = par1Random.nextInt(5);

		switch (var2) {
		case 0:
		case 1:
		default:
			return EnumDoor.OPENING;

		case 2:
			return EnumDoor.WOOD_DOOR;

		case 3:
			return EnumDoor.GRATES;

		case 4:
			return EnumDoor.IRON_DOOR;
		}
	}

	/**
	 * Gets the next component in any cardinal direction
	 */
	protected StructureComponent getNextComponentNormal(
			final ComponentStrongholdStairs2 par1ComponentStrongholdStairs2,
			final List par2List, final Random par3Random, final int par4,
			final int par5) {
		switch (coordBaseMode) {
		case 0:
			return StructureStrongholdPieces.getNextValidComponentAccess(
					par1ComponentStrongholdStairs2, par2List, par3Random,
					boundingBox.minX + par4, boundingBox.minY + par5,
					boundingBox.maxZ + 1, coordBaseMode, getComponentType());

		case 1:
			return StructureStrongholdPieces.getNextValidComponentAccess(
					par1ComponentStrongholdStairs2, par2List, par3Random,
					boundingBox.minX - 1, boundingBox.minY + par5,
					boundingBox.minZ + par4, coordBaseMode, getComponentType());

		case 2:
			return StructureStrongholdPieces.getNextValidComponentAccess(
					par1ComponentStrongholdStairs2, par2List, par3Random,
					boundingBox.minX + par4, boundingBox.minY + par5,
					boundingBox.minZ - 1, coordBaseMode, getComponentType());

		case 3:
			return StructureStrongholdPieces.getNextValidComponentAccess(
					par1ComponentStrongholdStairs2, par2List, par3Random,
					boundingBox.maxX + 1, boundingBox.minY + par5,
					boundingBox.minZ + par4, coordBaseMode, getComponentType());

		default:
			return null;
		}
	}

	/**
	 * Gets the next component in the +/- X direction
	 */
	protected StructureComponent getNextComponentX(
			final ComponentStrongholdStairs2 par1ComponentStrongholdStairs2,
			final List par2List, final Random par3Random, final int par4,
			final int par5) {
		switch (coordBaseMode) {
		case 0:
			return StructureStrongholdPieces.getNextValidComponentAccess(
					par1ComponentStrongholdStairs2, par2List, par3Random,
					boundingBox.minX - 1, boundingBox.minY + par4,
					boundingBox.minZ + par5, 1, getComponentType());

		case 1:
			return StructureStrongholdPieces.getNextValidComponentAccess(
					par1ComponentStrongholdStairs2, par2List, par3Random,
					boundingBox.minX + par5, boundingBox.minY + par4,
					boundingBox.minZ - 1, 2, getComponentType());

		case 2:
			return StructureStrongholdPieces.getNextValidComponentAccess(
					par1ComponentStrongholdStairs2, par2List, par3Random,
					boundingBox.minX - 1, boundingBox.minY + par4,
					boundingBox.minZ + par5, 1, getComponentType());

		case 3:
			return StructureStrongholdPieces.getNextValidComponentAccess(
					par1ComponentStrongholdStairs2, par2List, par3Random,
					boundingBox.minX + par5, boundingBox.minY + par4,
					boundingBox.minZ - 1, 2, getComponentType());

		default:
			return null;
		}
	}

	/**
	 * Gets the next component in the +/- Z direction
	 */
	protected StructureComponent getNextComponentZ(
			final ComponentStrongholdStairs2 par1ComponentStrongholdStairs2,
			final List par2List, final Random par3Random, final int par4,
			final int par5) {
		switch (coordBaseMode) {
		case 0:
			return StructureStrongholdPieces.getNextValidComponentAccess(
					par1ComponentStrongholdStairs2, par2List, par3Random,
					boundingBox.maxX + 1, boundingBox.minY + par4,
					boundingBox.minZ + par5, 3, getComponentType());

		case 1:
			return StructureStrongholdPieces.getNextValidComponentAccess(
					par1ComponentStrongholdStairs2, par2List, par3Random,
					boundingBox.minX + par5, boundingBox.minY + par4,
					boundingBox.maxZ + 1, 0, getComponentType());

		case 2:
			return StructureStrongholdPieces.getNextValidComponentAccess(
					par1ComponentStrongholdStairs2, par2List, par3Random,
					boundingBox.maxX + 1, boundingBox.minY + par4,
					boundingBox.minZ + par5, 3, getComponentType());

		case 3:
			return StructureStrongholdPieces.getNextValidComponentAccess(
					par1ComponentStrongholdStairs2, par2List, par3Random,
					boundingBox.minX + par5, boundingBox.minY + par4,
					boundingBox.maxZ + 1, 0, getComponentType());

		default:
			return null;
		}
	}

	/**
	 * returns false if the Structure Bounding Box goes below 10
	 */
	protected static boolean canStrongholdGoDeeper(
			final StructureBoundingBox par0StructureBoundingBox) {
		return par0StructureBoundingBox != null
				&& par0StructureBoundingBox.minY > 10;
	}
}
