package net.minecraft.src;

import java.util.List;

public abstract class EntityThrowable extends Entity implements IProjectile {
	private int xTile = -1;
	private int yTile = -1;
	private int zTile = -1;
	private int inTile = 0;
	protected boolean inGround = false;
	public int throwableShake = 0;

	/**
	 * Is the entity that throws this 'thing' (snowball, ender pearl, eye of
	 * ender or potion)
	 */
	private EntityLiving thrower;
	private String throwerName = null;
	private int ticksInGround;
	private int ticksInAir = 0;

	public EntityThrowable(final World par1World) {
		super(par1World);
		setSize(0.25F, 0.25F);
	}

	@Override
	protected void entityInit() {
	}

	/**
	 * Checks if the entity is in range to render by using the past in distance
	 * and comparing it to its average edge length * 64 * renderDistanceWeight
	 * Args: distance
	 */
	@Override
	public boolean isInRangeToRenderDist(final double par1) {
		double var3 = boundingBox.getAverageEdgeLength() * 4.0D;
		var3 *= 64.0D;
		return par1 < var3 * var3;
	}

	public EntityThrowable(final World par1World,
			final EntityLiving par2EntityLiving) {
		super(par1World);
		thrower = par2EntityLiving;
		setSize(0.25F, 0.25F);
		setLocationAndAngles(par2EntityLiving.posX, par2EntityLiving.posY
				+ par2EntityLiving.getEyeHeight(), par2EntityLiving.posZ,
				par2EntityLiving.rotationYaw, par2EntityLiving.rotationPitch);
		posX -= MathHelper.cos(rotationYaw / 180.0F * (float) Math.PI) * 0.16F;
		posY -= 0.10000000149011612D;
		posZ -= MathHelper.sin(rotationYaw / 180.0F * (float) Math.PI) * 0.16F;
		setPosition(posX, posY, posZ);
		yOffset = 0.0F;
		final float var3 = 0.4F;
		motionX = -MathHelper.sin(rotationYaw / 180.0F * (float) Math.PI)
				* MathHelper.cos(rotationPitch / 180.0F * (float) Math.PI)
				* var3;
		motionZ = MathHelper.cos(rotationYaw / 180.0F * (float) Math.PI)
				* MathHelper.cos(rotationPitch / 180.0F * (float) Math.PI)
				* var3;
		motionY = -MathHelper.sin((rotationPitch + func_70183_g()) / 180.0F
				* (float) Math.PI)
				* var3;
		setThrowableHeading(motionX, motionY, motionZ, func_70182_d(), 1.0F);
	}

	public EntityThrowable(final World par1World, final double par2,
			final double par4, final double par6) {
		super(par1World);
		ticksInGround = 0;
		setSize(0.25F, 0.25F);
		setPosition(par2, par4, par6);
		yOffset = 0.0F;
	}

	protected float func_70182_d() {
		return 1.5F;
	}

	protected float func_70183_g() {
		return 0.0F;
	}

	/**
	 * Similar to setArrowHeading, it's point the throwable entity to a x, y, z
	 * direction.
	 */
	@Override
	public void setThrowableHeading(double par1, double par3, double par5,
			final float par7, final float par8) {
		final float var9 = MathHelper.sqrt_double(par1 * par1 + par3 * par3
				+ par5 * par5);
		par1 /= var9;
		par3 /= var9;
		par5 /= var9;
		par1 += rand.nextGaussian() * 0.007499999832361937D * par8;
		par3 += rand.nextGaussian() * 0.007499999832361937D * par8;
		par5 += rand.nextGaussian() * 0.007499999832361937D * par8;
		par1 *= par7;
		par3 *= par7;
		par5 *= par7;
		motionX = par1;
		motionY = par3;
		motionZ = par5;
		final float var10 = MathHelper.sqrt_double(par1 * par1 + par5 * par5);
		prevRotationYaw = rotationYaw = (float) (Math.atan2(par1, par5) * 180.0D / Math.PI);
		prevRotationPitch = rotationPitch = (float) (Math.atan2(par3, var10) * 180.0D / Math.PI);
		ticksInGround = 0;
	}

	/**
	 * Sets the velocity to the args. Args: x, y, z
	 */
	@Override
	public void setVelocity(final double par1, final double par3,
			final double par5) {
		motionX = par1;
		motionY = par3;
		motionZ = par5;

		if (prevRotationPitch == 0.0F && prevRotationYaw == 0.0F) {
			final float var7 = MathHelper
					.sqrt_double(par1 * par1 + par5 * par5);
			prevRotationYaw = rotationYaw = (float) (Math.atan2(par1, par5) * 180.0D / Math.PI);
			prevRotationPitch = rotationPitch = (float) (Math.atan2(par3, var7) * 180.0D / Math.PI);
		}
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate() {
		lastTickPosX = posX;
		lastTickPosY = posY;
		lastTickPosZ = posZ;
		super.onUpdate();

		if (throwableShake > 0) {
			--throwableShake;
		}

		if (inGround) {
			final int var1 = worldObj.getBlockId(xTile, yTile, zTile);

			if (var1 == inTile) {
				++ticksInGround;

				if (ticksInGround == 1200) {
					setDead();
				}

				return;
			}

			inGround = false;
			motionX *= rand.nextFloat() * 0.2F;
			motionY *= rand.nextFloat() * 0.2F;
			motionZ *= rand.nextFloat() * 0.2F;
			ticksInGround = 0;
			ticksInAir = 0;
		} else {
			++ticksInAir;
		}

		Vec3 var16 = worldObj.getWorldVec3Pool().getVecFromPool(posX, posY,
				posZ);
		Vec3 var2 = worldObj.getWorldVec3Pool().getVecFromPool(posX + motionX,
				posY + motionY, posZ + motionZ);
		MovingObjectPosition var3 = worldObj.rayTraceBlocks(var16, var2);
		var16 = worldObj.getWorldVec3Pool().getVecFromPool(posX, posY, posZ);
		var2 = worldObj.getWorldVec3Pool().getVecFromPool(posX + motionX,
				posY + motionY, posZ + motionZ);

		if (var3 != null) {
			var2 = worldObj.getWorldVec3Pool().getVecFromPool(
					var3.hitVec.xCoord, var3.hitVec.yCoord, var3.hitVec.zCoord);
		}

		if (!worldObj.isRemote) {
			Entity var4 = null;
			final List var5 = worldObj.getEntitiesWithinAABBExcludingEntity(
					this, boundingBox.addCoord(motionX, motionY, motionZ)
							.expand(1.0D, 1.0D, 1.0D));
			double var6 = 0.0D;
			final EntityLiving var8 = getThrower();

			for (int var9 = 0; var9 < var5.size(); ++var9) {
				final Entity var10 = (Entity) var5.get(var9);

				if (var10.canBeCollidedWith()
						&& (var10 != var8 || ticksInAir >= 5)) {
					final float var11 = 0.3F;
					final AxisAlignedBB var12 = var10.boundingBox.expand(var11,
							var11, var11);
					final MovingObjectPosition var13 = var12
							.calculateIntercept(var16, var2);

					if (var13 != null) {
						final double var14 = var16.distanceTo(var13.hitVec);

						if (var14 < var6 || var6 == 0.0D) {
							var4 = var10;
							var6 = var14;
						}
					}
				}
			}

			if (var4 != null) {
				var3 = new MovingObjectPosition(var4);
			}
		}

		if (var3 != null) {
			if (var3.typeOfHit == EnumMovingObjectType.TILE
					&& worldObj.getBlockId(var3.blockX, var3.blockY,
							var3.blockZ) == Block.portal.blockID) {
				setInPortal();
			} else {
				onImpact(var3);
			}
		}

		posX += motionX;
		posY += motionY;
		posZ += motionZ;
		final float var17 = MathHelper.sqrt_double(motionX * motionX + motionZ
				* motionZ);
		rotationYaw = (float) (Math.atan2(motionX, motionZ) * 180.0D / Math.PI);

		for (rotationPitch = (float) (Math.atan2(motionY, var17) * 180.0D / Math.PI); rotationPitch
				- prevRotationPitch < -180.0F; prevRotationPitch -= 360.0F) {
			;
		}

		while (rotationPitch - prevRotationPitch >= 180.0F) {
			prevRotationPitch += 360.0F;
		}

		while (rotationYaw - prevRotationYaw < -180.0F) {
			prevRotationYaw -= 360.0F;
		}

		while (rotationYaw - prevRotationYaw >= 180.0F) {
			prevRotationYaw += 360.0F;
		}

		rotationPitch = prevRotationPitch + (rotationPitch - prevRotationPitch)
				* 0.2F;
		rotationYaw = prevRotationYaw + (rotationYaw - prevRotationYaw) * 0.2F;
		float var18 = 0.99F;
		final float var19 = getGravityVelocity();

		if (isInWater()) {
			for (int var7 = 0; var7 < 4; ++var7) {
				final float var20 = 0.25F;
				worldObj.spawnParticle("bubble", posX - motionX * var20, posY
						- motionY * var20, posZ - motionZ * var20, motionX,
						motionY, motionZ);
			}

			var18 = 0.8F;
		}

		motionX *= var18;
		motionY *= var18;
		motionZ *= var18;
		motionY -= var19;
		setPosition(posX, posY, posZ);
	}

	/**
	 * Gets the amount of gravity to apply to the thrown entity with each tick.
	 */
	protected float getGravityVelocity() {
		return 0.03F;
	}

	/**
	 * Called when this EntityThrowable hits a block or entity.
	 */
	protected abstract void onImpact(MovingObjectPosition var1);

	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	@Override
	public void writeEntityToNBT(final NBTTagCompound par1NBTTagCompound) {
		par1NBTTagCompound.setShort("xTile", (short) xTile);
		par1NBTTagCompound.setShort("yTile", (short) yTile);
		par1NBTTagCompound.setShort("zTile", (short) zTile);
		par1NBTTagCompound.setByte("inTile", (byte) inTile);
		par1NBTTagCompound.setByte("shake", (byte) throwableShake);
		par1NBTTagCompound.setByte("inGround", (byte) (inGround ? 1 : 0));

		if ((throwerName == null || throwerName.length() == 0)
				&& thrower != null && thrower instanceof EntityPlayer) {
			throwerName = thrower.getEntityName();
		}

		par1NBTTagCompound.setString("ownerName", throwerName == null ? ""
				: throwerName);
	}

	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	@Override
	public void readEntityFromNBT(final NBTTagCompound par1NBTTagCompound) {
		xTile = par1NBTTagCompound.getShort("xTile");
		yTile = par1NBTTagCompound.getShort("yTile");
		zTile = par1NBTTagCompound.getShort("zTile");
		inTile = par1NBTTagCompound.getByte("inTile") & 255;
		throwableShake = par1NBTTagCompound.getByte("shake") & 255;
		inGround = par1NBTTagCompound.getByte("inGround") == 1;
		throwerName = par1NBTTagCompound.getString("ownerName");

		if (throwerName != null && throwerName.length() == 0) {
			throwerName = null;
		}
	}

	@Override
	public float getShadowSize() {
		return 0.0F;
	}

	public EntityLiving getThrower() {
		if (thrower == null && throwerName != null && throwerName.length() > 0) {
			thrower = worldObj.getPlayerEntityByName(throwerName);
		}

		return thrower;
	}
}
