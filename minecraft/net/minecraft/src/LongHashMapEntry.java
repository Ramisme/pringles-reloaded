package net.minecraft.src;

class LongHashMapEntry {
	/**
	 * the key as a long (for playerInstances it is the x in the most
	 * significant 32 bits and then y)
	 */
	final long key;

	/** the value held by the hash at the specified key */
	Object value;

	/** the next hashentry in the table */
	LongHashMapEntry nextEntry;
	final int hash;

	LongHashMapEntry(final int par1, final long par2, final Object par4Obj,
			final LongHashMapEntry par5LongHashMapEntry) {
		value = par4Obj;
		nextEntry = par5LongHashMapEntry;
		key = par2;
		hash = par1;
	}

	public final long getKey() {
		return key;
	}

	public final Object getValue() {
		return value;
	}

	@Override
	public final boolean equals(final Object par1Obj) {
		if (!(par1Obj instanceof LongHashMapEntry)) {
			return false;
		} else {
			final LongHashMapEntry var2 = (LongHashMapEntry) par1Obj;
			final Long var3 = Long.valueOf(getKey());
			final Long var4 = Long.valueOf(var2.getKey());

			if (var3 == var4 || var3 != null && var3.equals(var4)) {
				final Object var5 = getValue();
				final Object var6 = var2.getValue();

				if (var5 == var6 || var5 != null && var5.equals(var6)) {
					return true;
				}
			}

			return false;
		}
	}

	@Override
	public final int hashCode() {
		return LongHashMap.getHashCode(key);
	}

	@Override
	public final String toString() {
		return getKey() + "=" + getValue();
	}
}
