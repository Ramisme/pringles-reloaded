package net.minecraft.src;

import java.util.concurrent.Callable;

class CallablePacketClass implements Callable {
	final Packet thePacket;

	final NetServerHandler theNetServerHandler;

	CallablePacketClass(final NetServerHandler par1NetServerHandler,
			final Packet par2Packet) {
		theNetServerHandler = par1NetServerHandler;
		thePacket = par2Packet;
	}

	public String getPacketClass() {
		return thePacket.getClass().getCanonicalName();
	}

	@Override
	public Object call() {
		return getPacketClass();
	}
}
