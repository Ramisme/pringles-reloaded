package net.minecraft.src;

import java.io.IOException;
import java.net.InetAddress;

import net.minecraft.server.MinecraftServer;

public class DedicatedServerListenThread extends NetworkListenThread {
	/** Instance of ServerListenThread. */
	private final ServerListenThread theServerListenThread;

	public DedicatedServerListenThread(
			final MinecraftServer par1MinecraftServer,
			final InetAddress par2InetAddress, final int par3)
			throws IOException {
		super(par1MinecraftServer);
		theServerListenThread = new ServerListenThread(this, par2InetAddress,
				par3);
		theServerListenThread.start();
	}

	@Override
	public void stopListening() {
		super.stopListening();
		theServerListenThread.func_71768_b();
		theServerListenThread.interrupt();
	}

	/**
	 * processes packets and pending connections
	 */
	@Override
	public void networkTick() {
		theServerListenThread.processPendingConnections();
		super.networkTick();
	}

	public DedicatedServer getDedicatedServer() {
		return (DedicatedServer) super.getServer();
	}

	public void func_71761_a(final InetAddress par1InetAddress) {
		theServerListenThread.func_71769_a(par1InetAddress);
	}

	@Override
	public MinecraftServer getServer() {
		return getDedicatedServer();
	}
}
