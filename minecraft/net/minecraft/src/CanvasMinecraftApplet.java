package net.minecraft.src;

import java.awt.Canvas;

import net.minecraft.client.MinecraftApplet;

public class CanvasMinecraftApplet extends Canvas {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7925850237633896902L;
	/** Reference to the MinecraftApplet object. */
	final MinecraftApplet mcApplet;

	public CanvasMinecraftApplet(final MinecraftApplet par1MinecraftApplet) {
		mcApplet = par1MinecraftApplet;
	}

	@Override
	public synchronized void addNotify() {
		super.addNotify();
		mcApplet.startMainThread();
	}

	@Override
	public synchronized void removeNotify() {
		mcApplet.shutdown();
		super.removeNotify();
	}
}
