package net.minecraft.src;

import java.util.ArrayList;
import java.util.List;

public class PlayerManager {
	private final WorldServer theWorldServer;

	/** players in the current instance */
	private final List players = new ArrayList();

	/**
	 * A map of chunk position (two ints concatenated into a long) to
	 * PlayerInstance
	 */
	private final LongHashMap playerInstances = new LongHashMap();

	/**
	 * contains a PlayerInstance for every chunk they can see. the
	 * "player instance" cotains a list of all players who can also that chunk
	 */
	private final List chunkWatcherWithPlayers = new ArrayList();
	public CompactArrayList chunkCoordsNotLoaded = new CompactArrayList(100,
			0.8F);

	/**
	 * Number of chunks the server sends to the client. Valid 3<=x<=15. In
	 * server.properties.
	 */
	private int playerViewRadius;

	/** x, z direction vectors: east, south, west, north */
	private final int[][] xzDirectionsConst = new int[][] { { 1, 0 }, { 0, 1 },
			{ -1, 0 }, { 0, -1 } };

	public PlayerManager(final WorldServer par1WorldServer, final int par2) {
		if (par2 > 15) {
			throw new IllegalArgumentException("Too big view radius!");
		} else if (par2 < 3) {
			throw new IllegalArgumentException("Too small view radius!");
		} else {
			playerViewRadius = Config.getChunkViewDistance();
			Config.dbg("ViewRadius: " + playerViewRadius + ", for: " + this
					+ " (constructor)");
			theWorldServer = par1WorldServer;
		}
	}

	public WorldServer getWorldServer() {
		return theWorldServer;
	}

	/**
	 * updates all the player instances that need to be updated
	 */
	public void updatePlayerInstances() {
		int var1;

		for (var1 = 0; var1 < chunkWatcherWithPlayers.size(); ++var1) {
			((PlayerInstance) chunkWatcherWithPlayers.get(var1))
					.sendChunkUpdate();
		}

		chunkWatcherWithPlayers.clear();

		if (players.isEmpty()) {
			final WorldProvider var18 = theWorldServer.provider;

			if (!var18.canRespawnHere()) {
				theWorldServer.theChunkProviderServer.unloadAllChunks();
			}
		}

		if (playerViewRadius != Config.getChunkViewDistance()) {
			setPlayerViewRadius(Config.getChunkViewDistance());
		}

		if (chunkCoordsNotLoaded.size() > 0) {
			for (var1 = 0; var1 < players.size(); ++var1) {
				final EntityPlayerMP var2 = (EntityPlayerMP) players.get(var1);
				final int var3 = var2.chunkCoordX;
				final int var4 = var2.chunkCoordZ;
				final int var5 = playerViewRadius + 1;
				final int var6 = var5 / 2;
				final int var7 = var5 * var5 + var6 * var6;
				int var8 = var7;
				int var9 = -1;
				PlayerInstance var10 = null;
				ChunkCoordIntPair var11 = null;

				for (int var12 = 0; var12 < chunkCoordsNotLoaded.size(); ++var12) {
					final ChunkCoordIntPair var13 = (ChunkCoordIntPair) chunkCoordsNotLoaded
							.get(var12);

					if (var13 != null) {
						final PlayerInstance var14 = this
								.getOrCreateChunkWatcher(var13.chunkXPos,
										var13.chunkZPos, false);

						if (var14 != null && !var14.chunkLoaded) {
							final int var15 = var3 - var13.chunkXPos;
							final int var16 = var4 - var13.chunkZPos;
							final int var17 = var15 * var15 + var16 * var16;

							if (var17 < var8) {
								var8 = var17;
								var9 = var12;
								var10 = var14;
								var11 = var13;
							}
						} else {
							chunkCoordsNotLoaded.set(var12, (Object) null);
						}
					}
				}

				if (var9 >= 0) {
					chunkCoordsNotLoaded.set(var9, (Object) null);
				}

				if (var10 != null) {
					var10.chunkLoaded = true;
					this.getWorldServer().theChunkProviderServer.loadChunk(
							var11.chunkXPos, var11.chunkZPos);
					var10.sendThisChunkToAllPlayers();
					break;
				}
			}

			chunkCoordsNotLoaded.compact();
		}
	}

	private PlayerInstance getOrCreateChunkWatcher(final int par1,
			final int par2, final boolean par3) {
		return this.getOrCreateChunkWatcher(par1, par2, par3, false);
	}

	private PlayerInstance getOrCreateChunkWatcher(final int var1,
			final int var2, final boolean var3, final boolean var4) {
		final long var5 = var1 + 2147483647L | var2 + 2147483647L << 32;
		PlayerInstance var7 = (PlayerInstance) playerInstances
				.getValueByKey(var5);

		if (var7 == null && var3) {
			var7 = new PlayerInstance(this, var1, var2, var4);
			playerInstances.add(var5, var7);
		}

		return var7;
	}

	/**
	 * the "PlayerInstance"/ chunkWatcher will send this chunk to all players
	 * who are in line of sight
	 */
	public void flagChunkForUpdate(final int par1, final int par2,
			final int par3) {
		final int var4 = par1 >> 4;
		final int var5 = par3 >> 4;
		final PlayerInstance var6 = this.getOrCreateChunkWatcher(var4, var5,
				false);

		if (var6 != null) {
			var6.flagChunkForUpdate(par1 & 15, par2, par3 & 15);
		}
	}

	/**
	 * Adds an EntityPlayerMP to the PlayerManager.
	 */
	public void addPlayer(final EntityPlayerMP par1EntityPlayerMP) {
		final int var2 = (int) par1EntityPlayerMP.posX >> 4;
		final int var3 = (int) par1EntityPlayerMP.posZ >> 4;
		par1EntityPlayerMP.managedPosX = par1EntityPlayerMP.posX;
		par1EntityPlayerMP.managedPosZ = par1EntityPlayerMP.posZ;
		final ArrayList var4 = new ArrayList(1);

		for (int var5 = var2 - playerViewRadius; var5 <= var2
				+ playerViewRadius; ++var5) {
			for (int var6 = var3 - playerViewRadius; var6 <= var3
					+ playerViewRadius; ++var6) {
				this.getOrCreateChunkWatcher(var5, var6, true)
						.addPlayerToChunkWatchingList(par1EntityPlayerMP);

				if (var5 >= var2 - 1 && var5 <= var2 + 1 && var6 >= var3 - 1
						&& var6 <= var3 + 1) {
					final Chunk var7 = this.getWorldServer().theChunkProviderServer
							.loadChunk(var5, var6);
					var4.add(var7);
				}
			}
		}

		par1EntityPlayerMP.playerNetServerHandler
				.sendPacketToPlayer(new Packet56MapChunks(var4));
		players.add(par1EntityPlayerMP);
		filterChunkLoadQueue(par1EntityPlayerMP);
	}

	/**
	 * Removes all chunks from the given player's chunk load queue that are not
	 * in viewing range of the player.
	 */
	public void filterChunkLoadQueue(final EntityPlayerMP par1EntityPlayerMP) {
		final ArrayList var2 = new ArrayList(par1EntityPlayerMP.loadedChunks);
		int var3 = 0;
		final int var4 = playerViewRadius;
		final int var5 = (int) par1EntityPlayerMP.posX >> 4;
		final int var6 = (int) par1EntityPlayerMP.posZ >> 4;
		int var7 = 0;
		int var8 = 0;
		ChunkCoordIntPair var9 = PlayerInstance.getChunkLocation(this
				.getOrCreateChunkWatcher(var5, var6, true));
		par1EntityPlayerMP.loadedChunks.clear();

		if (var2.contains(var9)) {
			par1EntityPlayerMP.loadedChunks.add(var9);
		}

		int var10;

		for (var10 = 1; var10 <= var4 * 2; ++var10) {
			for (int var11 = 0; var11 < 2; ++var11) {
				final int[] var12 = xzDirectionsConst[var3++ % 4];

				for (int var13 = 0; var13 < var10; ++var13) {
					var7 += var12[0];
					var8 += var12[1];
					var9 = PlayerInstance.getChunkLocation(this
							.getOrCreateChunkWatcher(var5 + var7, var6 + var8,
									true));

					if (var2.contains(var9)) {
						par1EntityPlayerMP.loadedChunks.add(var9);
					}
				}
			}
		}

		var3 %= 4;

		for (var10 = 0; var10 < var4 * 2; ++var10) {
			var7 += xzDirectionsConst[var3][0];
			var8 += xzDirectionsConst[var3][1];
			var9 = PlayerInstance.getChunkLocation(this
					.getOrCreateChunkWatcher(var5 + var7, var6 + var8, true));

			if (var2.contains(var9)) {
				par1EntityPlayerMP.loadedChunks.add(var9);
			}
		}
	}

	/**
	 * Removes an EntityPlayerMP from the PlayerManager.
	 */
	public void removePlayer(final EntityPlayerMP par1EntityPlayerMP) {
		final int var2 = (int) par1EntityPlayerMP.managedPosX >> 4;
		final int var3 = (int) par1EntityPlayerMP.managedPosZ >> 4;

		for (int var4 = var2 - playerViewRadius; var4 <= var2
				+ playerViewRadius; ++var4) {
			for (int var5 = var3 - playerViewRadius; var5 <= var3
					+ playerViewRadius; ++var5) {
				final PlayerInstance var6 = this.getOrCreateChunkWatcher(var4,
						var5, false);

				if (var6 != null) {
					var6.sendThisChunkToPlayer(par1EntityPlayerMP, false);
				}
			}
		}

		players.remove(par1EntityPlayerMP);
	}

	private boolean func_72684_a(final int par1, final int par2,
			final int par3, final int par4, final int par5) {
		final int var6 = par1 - par3;
		final int var7 = par2 - par4;
		return var6 >= -par5 && var6 <= par5 ? var7 >= -par5 && var7 <= par5
				: false;
	}

	/**
	 * update chunks around a player being moved by server logic (e.g. cart,
	 * boat)
	 */
	public void updateMountedMovingPlayer(
			final EntityPlayerMP par1EntityPlayerMP) {
		final int var2 = (int) par1EntityPlayerMP.posX >> 4;
		final int var3 = (int) par1EntityPlayerMP.posZ >> 4;
		final double var4 = par1EntityPlayerMP.managedPosX
				- par1EntityPlayerMP.posX;
		final double var6 = par1EntityPlayerMP.managedPosZ
				- par1EntityPlayerMP.posZ;
		final double var8 = var4 * var4 + var6 * var6;

		if (var8 >= 64.0D) {
			final int var10 = (int) par1EntityPlayerMP.managedPosX >> 4;
			final int var11 = (int) par1EntityPlayerMP.managedPosZ >> 4;
			final int var12 = playerViewRadius;
			final int var13 = var2 - var10;
			final int var14 = var3 - var11;

			if (var13 != 0 || var14 != 0) {
				for (int var15 = var2 - var12; var15 <= var2 + var12; ++var15) {
					for (int var16 = var3 - var12; var16 <= var3 + var12; ++var16) {
						if (!func_72684_a(var15, var16, var10, var11, var12)) {
							this.getOrCreateChunkWatcher(var15, var16, true,
									true).addPlayerToChunkWatchingList(
									par1EntityPlayerMP);
						}

						if (!func_72684_a(var15 - var13, var16 - var14, var2,
								var3, var12)) {
							final PlayerInstance var17 = this
									.getOrCreateChunkWatcher(var15 - var13,
											var16 - var14, false);

							if (var17 != null) {
								var17.sendThisChunkToPlayer(par1EntityPlayerMP);
							}
						}
					}
				}

				filterChunkLoadQueue(par1EntityPlayerMP);
				par1EntityPlayerMP.managedPosX = par1EntityPlayerMP.posX;
				par1EntityPlayerMP.managedPosZ = par1EntityPlayerMP.posZ;
			}
		}
	}

	public boolean isPlayerWatchingChunk(
			final EntityPlayerMP par1EntityPlayerMP, final int par2,
			final int par3) {
		final PlayerInstance var4 = this.getOrCreateChunkWatcher(par2, par3,
				false);
		return var4 == null ? false : PlayerInstance.getPlayersInChunk(var4)
				.contains(par1EntityPlayerMP)
				&& !par1EntityPlayerMP.loadedChunks.contains(PlayerInstance
						.getChunkLocation(var4));
	}

	/**
	 * Get the furthest viewable block given player's view distance
	 */
	public static int getFurthestViewableBlock(final int par0) {
		return par0 * 16 - 16;
	}

	static WorldServer getWorldServer(final PlayerManager par0PlayerManager) {
		return par0PlayerManager.theWorldServer;
	}

	static LongHashMap getChunkWatchers(final PlayerManager par0PlayerManager) {
		return par0PlayerManager.playerInstances;
	}

	static List getChunkWatchersWithPlayers(
			final PlayerManager par0PlayerManager) {
		return par0PlayerManager.chunkWatcherWithPlayers;
	}

	private void setPlayerViewRadius(final int var1) {
		if (playerViewRadius != var1) {
			final EntityPlayerMP[] var2 = (EntityPlayerMP[]) players
					.toArray(new EntityPlayerMP[players.size()]);
			int var3;
			EntityPlayerMP var4;

			for (var3 = 0; var3 < var2.length; ++var3) {
				var4 = var2[var3];
				removePlayer(var4);
			}

			playerViewRadius = var1;

			for (var3 = 0; var3 < var2.length; ++var3) {
				var4 = var2[var3];
				addPlayer(var4);
			}

			Config.dbg("ViewRadius: " + playerViewRadius + ", for: " + this
					+ " (detect)");
		}
	}
}
