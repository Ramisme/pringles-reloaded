package net.minecraft.src;

public class CustomAnimationFrame {
	public int index = 0;
	public int duration = 0;
	public int counter = 0;

	public CustomAnimationFrame(final int var1, final int var2) {
		index = var1;
		duration = var2;
	}
}
