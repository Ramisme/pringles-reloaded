package net.minecraft.src;

public class ItemSword extends Item {
	private final int weaponDamage;
	private final EnumToolMaterial toolMaterial;

	public ItemSword(final int par1, final EnumToolMaterial par2EnumToolMaterial) {
		super(par1);
		toolMaterial = par2EnumToolMaterial;
		maxStackSize = 1;
		setMaxDamage(par2EnumToolMaterial.getMaxUses());
		setCreativeTab(CreativeTabs.tabCombat);
		weaponDamage = 4 + par2EnumToolMaterial.getDamageVsEntity();
	}

	public int func_82803_g() {
		return toolMaterial.getDamageVsEntity();
	}

	/**
	 * Returns the strength of the stack against a given block. 1.0F base,
	 * (Quality+1)*2 if correct blocktype, 1.5F if sword
	 */
	@Override
	public float getStrVsBlock(final ItemStack par1ItemStack,
			final Block par2Block) {
		if (par2Block.blockID == Block.web.blockID) {
			return 15.0F;
		} else {
			final Material var3 = par2Block.blockMaterial;
			return var3 != Material.plants && var3 != Material.vine
					&& var3 != Material.coral && var3 != Material.leaves
					&& var3 != Material.pumpkin ? 1.0F : 1.5F;
		}
	}

	/**
	 * Current implementations of this method in child classes do not use the
	 * entry argument beside ev. They just raise the damage on the stack.
	 */
	@Override
	public boolean hitEntity(final ItemStack par1ItemStack,
			final EntityLiving par2EntityLiving,
			final EntityLiving par3EntityLiving) {
		par1ItemStack.damageItem(1, par3EntityLiving);
		return true;
	}

	@Override
	public boolean onBlockDestroyed(final ItemStack par1ItemStack,
			final World par2World, final int par3, final int par4,
			final int par5, final int par6, final EntityLiving par7EntityLiving) {
		if (Block.blocksList[par3]
				.getBlockHardness(par2World, par4, par5, par6) != 0.0D) {
			par1ItemStack.damageItem(2, par7EntityLiving);
		}

		return true;
	}

	/**
	 * Returns the damage against a given entity.
	 */
	@Override
	public int getDamageVsEntity(final Entity par1Entity) {
		return weaponDamage;
	}

	/**
	 * Returns True is the item is renderer in full 3D when hold.
	 */
	@Override
	public boolean isFull3D() {
		return true;
	}

	/**
	 * returns the action that specifies what animation to play when the items
	 * is being used
	 */
	@Override
	public EnumAction getItemUseAction(final ItemStack par1ItemStack) {
		return EnumAction.block;
	}

	/**
	 * How long it takes to use or consume an item
	 */
	@Override
	public int getMaxItemUseDuration(final ItemStack par1ItemStack) {
		return 72000;
	}

	/**
	 * Called whenever this item is equipped and the right mouse button is
	 * pressed. Args: itemStack, world, entityPlayer
	 */
	@Override
	public ItemStack onItemRightClick(final ItemStack par1ItemStack,
			final World par2World, final EntityPlayer par3EntityPlayer) {
		par3EntityPlayer.setItemInUse(par1ItemStack,
				getMaxItemUseDuration(par1ItemStack));
		return par1ItemStack;
	}

	/**
	 * Returns if the item (tool) can harvest results from the block type.
	 */
	@Override
	public boolean canHarvestBlock(final Block par1Block) {
		return par1Block.blockID == Block.web.blockID;
	}

	/**
	 * Return the enchantability factor of the item, most of the time is based
	 * on material.
	 */
	@Override
	public int getItemEnchantability() {
		return toolMaterial.getEnchantability();
	}

	/**
	 * Return the name for this tool's material.
	 */
	public String getToolMaterialName() {
		return toolMaterial.toString();
	}

	/**
	 * Return whether this item is repairable in an anvil.
	 */
	@Override
	public boolean getIsRepairable(final ItemStack par1ItemStack,
			final ItemStack par2ItemStack) {
		return toolMaterial.getToolCraftingMaterial() == par2ItemStack.itemID ? true
				: super.getIsRepairable(par1ItemStack, par2ItemStack);
	}
}
