package net.minecraft.src;

import java.util.ArrayList;
import java.util.Collections;

import net.minecraft.client.Minecraft;

import org.lwjgl.input.Keyboard;

public class GuiScreenCreateOnlineWorld extends GuiScreen {
	private final GuiScreen field_96260_a;
	private GuiTextField field_96257_c;
	private GuiTextField field_96255_b;
	private String field_98108_c;
	private String field_98109_n;
	private static int field_96253_d = 0;
	private static int field_96261_n = 1;
	private boolean field_96256_r = false;
	private final String field_96254_s = "You must enter a name!";

	public GuiScreenCreateOnlineWorld(final GuiScreen par1GuiScreen) {
		super.buttonList = Collections.synchronizedList(new ArrayList());
		field_96260_a = par1GuiScreen;
	}

	/**
	 * Called from the main game loop to update the screen.
	 */
	@Override
	public void updateScreen() {
		field_96257_c.updateCursorCounter();
		field_98108_c = field_96257_c.getText();
		field_96255_b.updateCursorCounter();
		field_98109_n = field_96255_b.getText();
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui() {
		final StringTranslate var1 = StringTranslate.getInstance();
		Keyboard.enableRepeatEvents(true);
		buttonList.clear();
		buttonList.add(new GuiButton(GuiScreenCreateOnlineWorld.field_96253_d,
				width / 2 - 100, height / 4 + 120 + 17, 97, 20, var1
						.translateKey("mco.create.world")));
		buttonList.add(new GuiButton(GuiScreenCreateOnlineWorld.field_96261_n,
				width / 2 + 5, height / 4 + 120 + 17, 95, 20, var1
						.translateKey("gui.cancel")));
		field_96257_c = new GuiTextField(fontRenderer, width / 2 - 100, 65,
				200, 20);
		field_96257_c.setFocused(true);

		if (field_98108_c != null) {
			field_96257_c.setText(field_98108_c);
		}

		field_96255_b = new GuiTextField(fontRenderer, width / 2 - 100, 111,
				200, 20);

		if (field_98109_n != null) {
			field_96255_b.setText(field_98109_n);
		}
	}

	/**
	 * Called when the screen is unloaded. Used to disable keyboard repeat
	 * events
	 */
	@Override
	public void onGuiClosed() {
		Keyboard.enableRepeatEvents(false);
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	@Override
	protected void actionPerformed(final GuiButton par1GuiButton) {
		if (par1GuiButton.enabled) {
			if (par1GuiButton.id == GuiScreenCreateOnlineWorld.field_96261_n) {
				mc.displayGuiScreen(field_96260_a);
			} else if (par1GuiButton.id == GuiScreenCreateOnlineWorld.field_96253_d) {
				func_96252_h();
			}
		}
	}

	private void func_96252_h() {
		if (func_96249_i()) {
			final TaskWorldCreation var1 = new TaskWorldCreation(this,
					field_96257_c.getText(), "Minecraft Realms Server",
					"NO_LOCATION", field_98109_n);
			final GuiScreenLongRunningTask var2 = new GuiScreenLongRunningTask(
					mc, field_96260_a, var1);
			var2.func_98117_g();
			mc.displayGuiScreen(var2);
		}
	}

	private boolean func_96249_i() {
		field_96256_r = field_96257_c.getText() == null
				|| field_96257_c.getText().trim().equals("");
		return !field_96256_r;
	}

	/**
	 * Fired when a key is typed. This is the equivalent of
	 * KeyListener.keyTyped(KeyEvent e).
	 */
	@Override
	protected void keyTyped(final char par1, final int par2) {
		field_96257_c.textboxKeyTyped(par1, par2);
		field_96255_b.textboxKeyTyped(par1, par2);

		if (par1 == 9) {
			field_96257_c.setFocused(!field_96257_c.isFocused());
			field_96255_b.setFocused(!field_96255_b.isFocused());
		}

		if (par1 == 13) {
			actionPerformed((GuiButton) buttonList.get(0));
		}
	}

	/**
	 * Called when the mouse is clicked.
	 */
	@Override
	protected void mouseClicked(final int par1, final int par2, final int par3) {
		super.mouseClicked(par1, par2, par3);
		field_96257_c.mouseClicked(par1, par2, par3);
		field_96255_b.mouseClicked(par1, par2, par3);
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(final int par1, final int par2, final float par3) {
		final StringTranslate var4 = StringTranslate.getInstance();
		drawDefaultBackground();
		drawCenteredString(fontRenderer,
				var4.translateKey("mco.selectServer.create"), width / 2, 11,
				16777215);
		drawString(fontRenderer, var4.translateKey("mco.configure.world.name"),
				width / 2 - 100, 52, 10526880);
		drawString(fontRenderer, var4.translateKey("mco.configure.world.seed"),
				width / 2 - 100, 98, 10526880);

		if (field_96256_r) {
			drawCenteredString(fontRenderer, field_96254_s, width / 2, 167,
					16711680);
		}

		field_96257_c.drawTextBox();
		field_96255_b.drawTextBox();
		super.drawScreen(par1, par2, par3);
	}

	static Minecraft func_96248_a(
			final GuiScreenCreateOnlineWorld par0GuiScreenCreateOnlineWorld) {
		return par0GuiScreenCreateOnlineWorld.mc;
	}

	static GuiScreen func_96247_b(
			final GuiScreenCreateOnlineWorld par0GuiScreenCreateOnlineWorld) {
		return par0GuiScreenCreateOnlineWorld.field_96260_a;
	}

	static Minecraft func_96246_c(
			final GuiScreenCreateOnlineWorld par0GuiScreenCreateOnlineWorld) {
		return par0GuiScreenCreateOnlineWorld.mc;
	}
}
