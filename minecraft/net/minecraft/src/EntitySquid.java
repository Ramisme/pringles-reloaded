package net.minecraft.src;

public class EntitySquid extends EntityWaterMob {
	public float squidPitch = 0.0F;
	public float prevSquidPitch = 0.0F;
	public float squidYaw = 0.0F;
	public float prevSquidYaw = 0.0F;
	public float field_70867_h = 0.0F;
	public float field_70868_i = 0.0F;

	/** angle of the tentacles in radians */
	public float tentacleAngle = 0.0F;

	/** the last calculated angle of the tentacles in radians */
	public float prevTentacleAngle = 0.0F;
	private float randomMotionSpeed = 0.0F;
	private float field_70864_bA = 0.0F;
	private float field_70871_bB = 0.0F;
	private float randomMotionVecX = 0.0F;
	private float randomMotionVecY = 0.0F;
	private float randomMotionVecZ = 0.0F;

	public EntitySquid(final World par1World) {
		super(par1World);
		texture = "/mob/squid.png";
		setSize(0.95F, 0.95F);
		field_70864_bA = 1.0F / (rand.nextFloat() + 1.0F) * 0.2F;
	}

	@Override
	public int getMaxHealth() {
		return 10;
	}

	/**
	 * Returns the sound this mob makes while it's alive.
	 */
	@Override
	protected String getLivingSound() {
		return null;
	}

	/**
	 * Returns the sound this mob makes when it is hurt.
	 */
	@Override
	protected String getHurtSound() {
		return null;
	}

	/**
	 * Returns the sound this mob makes on death.
	 */
	@Override
	protected String getDeathSound() {
		return null;
	}

	/**
	 * Returns the volume for the sounds this mob makes.
	 */
	@Override
	protected float getSoundVolume() {
		return 0.4F;
	}

	/**
	 * Returns the item ID for the item the mob drops on death.
	 */
	@Override
	protected int getDropItemId() {
		return 0;
	}

	/**
	 * Drop 0-2 items of this living's type. @param par1 - Whether this entity
	 * has recently been hit by a player. @param par2 - Level of Looting used to
	 * kill this mob.
	 */
	@Override
	protected void dropFewItems(final boolean par1, final int par2) {
		final int var3 = rand.nextInt(3 + par2) + 1;

		for (int var4 = 0; var4 < var3; ++var4) {
			entityDropItem(new ItemStack(Item.dyePowder, 1, 0), 0.0F);
		}
	}

	/**
	 * Checks if this entity is inside water (if inWater field is true as a
	 * result of handleWaterMovement() returning true)
	 */
	@Override
	public boolean isInWater() {
		return worldObj.handleMaterialAcceleration(
				boundingBox.expand(0.0D, -0.6000000238418579D, 0.0D),
				Material.water, this);
	}

	/**
	 * Called frequently so the entity can update its state every tick as
	 * required. For example, zombies and skeletons use this to react to
	 * sunlight and start to burn.
	 */
	@Override
	public void onLivingUpdate() {
		super.onLivingUpdate();
		prevSquidPitch = squidPitch;
		prevSquidYaw = squidYaw;
		field_70868_i = field_70867_h;
		prevTentacleAngle = tentacleAngle;
		field_70867_h += field_70864_bA;

		if (field_70867_h > (float) Math.PI * 2F) {
			field_70867_h -= (float) Math.PI * 2F;

			if (rand.nextInt(10) == 0) {
				field_70864_bA = 1.0F / (rand.nextFloat() + 1.0F) * 0.2F;
			}
		}

		if (isInWater()) {
			float var1;

			if (field_70867_h < (float) Math.PI) {
				var1 = field_70867_h / (float) Math.PI;
				tentacleAngle = MathHelper.sin(var1 * var1 * (float) Math.PI)
						* (float) Math.PI * 0.25F;

				if (var1 > 0.75D) {
					randomMotionSpeed = 1.0F;
					field_70871_bB = 1.0F;
				} else {
					field_70871_bB *= 0.8F;
				}
			} else {
				tentacleAngle = 0.0F;
				randomMotionSpeed *= 0.9F;
				field_70871_bB *= 0.99F;
			}

			if (!worldObj.isRemote) {
				motionX = randomMotionVecX * randomMotionSpeed;
				motionY = randomMotionVecY * randomMotionSpeed;
				motionZ = randomMotionVecZ * randomMotionSpeed;
			}

			var1 = MathHelper
					.sqrt_double(motionX * motionX + motionZ * motionZ);
			renderYawOffset += (-((float) Math.atan2(motionX, motionZ))
					* 180.0F / (float) Math.PI - renderYawOffset) * 0.1F;
			rotationYaw = renderYawOffset;
			squidYaw += (float) Math.PI * field_70871_bB * 1.5F;
			squidPitch += (-((float) Math.atan2(var1, motionY)) * 180.0F
					/ (float) Math.PI - squidPitch) * 0.1F;
		} else {
			tentacleAngle = MathHelper.abs(MathHelper.sin(field_70867_h))
					* (float) Math.PI * 0.25F;

			if (!worldObj.isRemote) {
				motionX = 0.0D;
				motionY -= 0.08D;
				motionY *= 0.9800000190734863D;
				motionZ = 0.0D;
			}

			squidPitch = (float) (squidPitch + (-90.0F - squidPitch) * 0.02D);
		}
	}

	/**
	 * Moves the entity based on the specified heading. Args: strafe, forward
	 */
	@Override
	public void moveEntityWithHeading(final float par1, final float par2) {
		moveEntity(motionX, motionY, motionZ);
	}

	@Override
	protected void updateEntityActionState() {
		++entityAge;

		if (entityAge > 100) {
			randomMotionVecX = randomMotionVecY = randomMotionVecZ = 0.0F;
		} else if (rand.nextInt(50) == 0 || !inWater
				|| randomMotionVecX == 0.0F && randomMotionVecY == 0.0F
				&& randomMotionVecZ == 0.0F) {
			final float var1 = rand.nextFloat() * (float) Math.PI * 2.0F;
			randomMotionVecX = MathHelper.cos(var1) * 0.2F;
			randomMotionVecY = -0.1F + rand.nextFloat() * 0.2F;
			randomMotionVecZ = MathHelper.sin(var1) * 0.2F;
		}

		despawnEntity();
	}

	/**
	 * Checks if the entity's current position is a valid location to spawn this
	 * entity.
	 */
	@Override
	public boolean getCanSpawnHere() {
		return posY > 45.0D && posY < 63.0D && super.getCanSpawnHere();
	}
}
