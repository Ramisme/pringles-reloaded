package net.minecraft.src;

import java.util.Arrays;
import java.util.List;

import net.minecraft.server.MinecraftServer;

public class CommandServerMessage extends CommandBase {
	@Override
	public List getCommandAliases() {
		return Arrays.asList(new String[] { "w", "msg" });
	}

	@Override
	public String getCommandName() {
		return "tell";
	}

	/**
	 * Return the required permission level for this command.
	 */
	@Override
	public int getRequiredPermissionLevel() {
		return 0;
	}

	@Override
	public void processCommand(final ICommandSender par1ICommandSender,
			final String[] par2ArrayOfStr) {
		if (par2ArrayOfStr.length < 2) {
			throw new WrongUsageException("commands.message.usage",
					new Object[0]);
		} else {
			final EntityPlayerMP var3 = CommandBase.func_82359_c(
					par1ICommandSender, par2ArrayOfStr[0]);

			if (var3 == null) {
				throw new PlayerNotFoundException();
			} else if (var3 == par1ICommandSender) {
				throw new PlayerNotFoundException(
						"commands.message.sameTarget", new Object[0]);
			} else {
				final String var4 = CommandBase.func_82361_a(
						par1ICommandSender, par2ArrayOfStr, 1,
						!(par1ICommandSender instanceof EntityPlayer));
				var3.sendChatToPlayer(EnumChatFormatting.GRAY
						+ ""
						+ EnumChatFormatting.ITALIC
						+ var3.translateString(
								"commands.message.display.incoming",
								new Object[] {
										par1ICommandSender
												.getCommandSenderName(), var4 }));
				par1ICommandSender.sendChatToPlayer(EnumChatFormatting.GRAY
						+ ""
						+ EnumChatFormatting.ITALIC
						+ par1ICommandSender.translateString(
								"commands.message.display.outgoing",
								new Object[] { var3.getCommandSenderName(),
										var4 }));
			}
		}
	}

	/**
	 * Adds the strings available in this command to the given list of tab
	 * completion options.
	 */
	@Override
	public List addTabCompletionOptions(
			final ICommandSender par1ICommandSender,
			final String[] par2ArrayOfStr) {
		return CommandBase.getListOfStringsMatchingLastWord(par2ArrayOfStr,
				MinecraftServer.getServer().getAllUsernames());
	}

	/**
	 * Return whether the specified command parameter index is a username
	 * parameter.
	 */
	@Override
	public boolean isUsernameIndex(final String[] par1ArrayOfStr, final int par2) {
		return par2 == 0;
	}
}
