package net.minecraft.src;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class VillageCollection extends WorldSavedData {
	private World worldObj;

	/**
	 * This is a black hole. You can add data to this list through a public
	 * interface, but you can't query that information in any way and it's not
	 * used internally either.
	 */
	private final List villagerPositionsList = new ArrayList();
	private final List newDoors = new ArrayList();
	private final List villageList = new ArrayList();
	private int tickCounter = 0;

	public VillageCollection(final String par1Str) {
		super(par1Str);
	}

	public VillageCollection(final World par1World) {
		super("villages");
		worldObj = par1World;
		markDirty();
	}

	public void func_82566_a(final World par1World) {
		worldObj = par1World;
		final Iterator var2 = villageList.iterator();

		while (var2.hasNext()) {
			final Village var3 = (Village) var2.next();
			var3.func_82691_a(par1World);
		}
	}

	/**
	 * This is a black hole. You can add data to this list through a public
	 * interface, but you can't query that information in any way and it's not
	 * used internally either.
	 */
	public void addVillagerPosition(final int par1, final int par2,
			final int par3) {
		if (villagerPositionsList.size() <= 64) {
			if (!isVillagerPositionPresent(par1, par2, par3)) {
				villagerPositionsList
						.add(new ChunkCoordinates(par1, par2, par3));
			}
		}
	}

	/**
	 * Runs a single tick for the village collection
	 */
	public void tick() {
		++tickCounter;
		final Iterator var1 = villageList.iterator();

		while (var1.hasNext()) {
			final Village var2 = (Village) var1.next();
			var2.tick(tickCounter);
		}

		removeAnnihilatedVillages();
		dropOldestVillagerPosition();
		addNewDoorsToVillageOrCreateVillage();

		if (tickCounter % 400 == 0) {
			markDirty();
		}
	}

	private void removeAnnihilatedVillages() {
		final Iterator var1 = villageList.iterator();

		while (var1.hasNext()) {
			final Village var2 = (Village) var1.next();

			if (var2.isAnnihilated()) {
				var1.remove();
				markDirty();
			}
		}
	}

	/**
	 * Get a list of villages.
	 */
	public List getVillageList() {
		return villageList;
	}

	/**
	 * Finds the nearest village, but only the given coordinates are withing
	 * it's bounding box plus the given the distance.
	 */
	public Village findNearestVillage(final int par1, final int par2,
			final int par3, final int par4) {
		Village var5 = null;
		float var6 = Float.MAX_VALUE;
		final Iterator var7 = villageList.iterator();

		while (var7.hasNext()) {
			final Village var8 = (Village) var7.next();
			final float var9 = var8.getCenter().getDistanceSquared(par1, par2,
					par3);

			if (var9 < var6) {
				final int var10 = par4 + var8.getVillageRadius();

				if (var9 <= var10 * var10) {
					var5 = var8;
					var6 = var9;
				}
			}
		}

		return var5;
	}

	private void dropOldestVillagerPosition() {
		if (!villagerPositionsList.isEmpty()) {
			addUnassignedWoodenDoorsAroundToNewDoorsList((ChunkCoordinates) villagerPositionsList
					.remove(0));
		}
	}

	private void addNewDoorsToVillageOrCreateVillage() {
		int var1 = 0;

		while (var1 < newDoors.size()) {
			final VillageDoorInfo var2 = (VillageDoorInfo) newDoors.get(var1);
			boolean var3 = false;
			final Iterator var4 = villageList.iterator();

			while (true) {
				if (var4.hasNext()) {
					final Village var5 = (Village) var4.next();
					final int var6 = (int) var5.getCenter().getDistanceSquared(
							var2.posX, var2.posY, var2.posZ);
					final int var7 = 32 + var5.getVillageRadius();

					if (var6 > var7 * var7) {
						continue;
					}

					var5.addVillageDoorInfo(var2);
					var3 = true;
				}

				if (!var3) {
					final Village var8 = new Village(worldObj);
					var8.addVillageDoorInfo(var2);
					villageList.add(var8);
					markDirty();
				}

				++var1;
				break;
			}
		}

		newDoors.clear();
	}

	private void addUnassignedWoodenDoorsAroundToNewDoorsList(
			final ChunkCoordinates par1ChunkCoordinates) {
		final byte var2 = 16;
		final byte var3 = 4;
		final byte var4 = 16;

		for (int var5 = par1ChunkCoordinates.posX - var2; var5 < par1ChunkCoordinates.posX
				+ var2; ++var5) {
			for (int var6 = par1ChunkCoordinates.posY - var3; var6 < par1ChunkCoordinates.posY
					+ var3; ++var6) {
				for (int var7 = par1ChunkCoordinates.posZ - var4; var7 < par1ChunkCoordinates.posZ
						+ var4; ++var7) {
					if (isWoodenDoorAt(var5, var6, var7)) {
						final VillageDoorInfo var8 = getVillageDoorAt(var5,
								var6, var7);

						if (var8 == null) {
							addDoorToNewListIfAppropriate(var5, var6, var7);
						} else {
							var8.lastActivityTimestamp = tickCounter;
						}
					}
				}
			}
		}
	}

	private VillageDoorInfo getVillageDoorAt(final int par1, final int par2,
			final int par3) {
		Iterator var4 = newDoors.iterator();
		VillageDoorInfo var5;

		do {
			if (!var4.hasNext()) {
				var4 = villageList.iterator();
				VillageDoorInfo var6;

				do {
					if (!var4.hasNext()) {
						return null;
					}

					final Village var7 = (Village) var4.next();
					var6 = var7.getVillageDoorAt(par1, par2, par3);
				} while (var6 == null);

				return var6;
			}

			var5 = (VillageDoorInfo) var4.next();
		} while (var5.posX != par1 || var5.posZ != par3
				|| Math.abs(var5.posY - par2) > 1);

		return var5;
	}

	private void addDoorToNewListIfAppropriate(final int par1, final int par2,
			final int par3) {
		final int var4 = ((BlockDoor) Block.doorWood).getDoorOrientation(
				worldObj, par1, par2, par3);
		int var5;
		int var6;

		if (var4 != 0 && var4 != 2) {
			var5 = 0;

			for (var6 = -5; var6 < 0; ++var6) {
				if (worldObj.canBlockSeeTheSky(par1, par2, par3 + var6)) {
					--var5;
				}
			}

			for (var6 = 1; var6 <= 5; ++var6) {
				if (worldObj.canBlockSeeTheSky(par1, par2, par3 + var6)) {
					++var5;
				}
			}

			if (var5 != 0) {
				newDoors.add(new VillageDoorInfo(par1, par2, par3, 0,
						var5 > 0 ? -2 : 2, tickCounter));
			}
		} else {
			var5 = 0;

			for (var6 = -5; var6 < 0; ++var6) {
				if (worldObj.canBlockSeeTheSky(par1 + var6, par2, par3)) {
					--var5;
				}
			}

			for (var6 = 1; var6 <= 5; ++var6) {
				if (worldObj.canBlockSeeTheSky(par1 + var6, par2, par3)) {
					++var5;
				}
			}

			if (var5 != 0) {
				newDoors.add(new VillageDoorInfo(par1, par2, par3,
						var5 > 0 ? -2 : 2, 0, tickCounter));
			}
		}
	}

	private boolean isVillagerPositionPresent(final int par1, final int par2,
			final int par3) {
		final Iterator var4 = villagerPositionsList.iterator();
		ChunkCoordinates var5;

		do {
			if (!var4.hasNext()) {
				return false;
			}

			var5 = (ChunkCoordinates) var4.next();
		} while (var5.posX != par1 || var5.posY != par2 || var5.posZ != par3);

		return true;
	}

	private boolean isWoodenDoorAt(final int par1, final int par2,
			final int par3) {
		final int var4 = worldObj.getBlockId(par1, par2, par3);
		return var4 == Block.doorWood.blockID;
	}

	/**
	 * reads in data from the NBTTagCompound into this MapDataBase
	 */
	@Override
	public void readFromNBT(final NBTTagCompound par1NBTTagCompound) {
		tickCounter = par1NBTTagCompound.getInteger("Tick");
		final NBTTagList var2 = par1NBTTagCompound.getTagList("Villages");

		for (int var3 = 0; var3 < var2.tagCount(); ++var3) {
			final NBTTagCompound var4 = (NBTTagCompound) var2.tagAt(var3);
			final Village var5 = new Village();
			var5.readVillageDataFromNBT(var4);
			villageList.add(var5);
		}
	}

	/**
	 * write data to NBTTagCompound from this MapDataBase, similar to Entities
	 * and TileEntities
	 */
	@Override
	public void writeToNBT(final NBTTagCompound par1NBTTagCompound) {
		par1NBTTagCompound.setInteger("Tick", tickCounter);
		final NBTTagList var2 = new NBTTagList("Villages");
		final Iterator var3 = villageList.iterator();

		while (var3.hasNext()) {
			final Village var4 = (Village) var3.next();
			final NBTTagCompound var5 = new NBTTagCompound("Village");
			var4.writeVillageDataToNBT(var5);
			var2.appendTag(var5);
		}

		par1NBTTagCompound.setTag("Villages", var2);
	}
}
