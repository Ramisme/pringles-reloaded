package net.minecraft.src;

public class EntitySnowman extends EntityGolem implements IRangedAttackMob {
	public EntitySnowman(final World par1World) {
		super(par1World);
		texture = "/mob/snowman.png";
		setSize(0.4F, 1.8F);
		getNavigator().setAvoidsWater(true);
		tasks.addTask(1, new EntityAIArrowAttack(this, 0.25F, 20, 10.0F));
		tasks.addTask(2, new EntityAIWander(this, 0.2F));
		tasks.addTask(3, new EntityAIWatchClosest(this, EntityPlayer.class,
				6.0F));
		tasks.addTask(4, new EntityAILookIdle(this));
		targetTasks.addTask(1, new EntityAINearestAttackableTarget(this,
				EntityLiving.class, 16.0F, 0, true, false, IMob.mobSelector));
	}

	/**
	 * Returns true if the newer Entity AI code should be run
	 */
	@Override
	public boolean isAIEnabled() {
		return true;
	}

	@Override
	public int getMaxHealth() {
		return 4;
	}

	/**
	 * Called frequently so the entity can update its state every tick as
	 * required. For example, zombies and skeletons use this to react to
	 * sunlight and start to burn.
	 */
	@Override
	public void onLivingUpdate() {
		super.onLivingUpdate();

		if (isWet()) {
			attackEntityFrom(DamageSource.drown, 1);
		}

		int var1 = MathHelper.floor_double(posX);
		int var2 = MathHelper.floor_double(posZ);

		if (worldObj.getBiomeGenForCoords(var1, var2).getFloatTemperature() > 1.0F) {
			attackEntityFrom(DamageSource.onFire, 1);
		}

		for (var1 = 0; var1 < 4; ++var1) {
			var2 = MathHelper.floor_double(posX + (var1 % 2 * 2 - 1) * 0.25F);
			final int var3 = MathHelper.floor_double(posY);
			final int var4 = MathHelper.floor_double(posZ
					+ (var1 / 2 % 2 * 2 - 1) * 0.25F);

			if (worldObj.getBlockId(var2, var3, var4) == 0
					&& worldObj.getBiomeGenForCoords(var2, var4)
							.getFloatTemperature() < 0.8F
					&& Block.snow.canPlaceBlockAt(worldObj, var2, var3, var4)) {
				worldObj.setBlock(var2, var3, var4, Block.snow.blockID);
			}
		}
	}

	/**
	 * Returns the item ID for the item the mob drops on death.
	 */
	@Override
	protected int getDropItemId() {
		return Item.snowball.itemID;
	}

	/**
	 * Drop 0-2 items of this living's type. @param par1 - Whether this entity
	 * has recently been hit by a player. @param par2 - Level of Looting used to
	 * kill this mob.
	 */
	@Override
	protected void dropFewItems(final boolean par1, final int par2) {
		final int var3 = rand.nextInt(16);

		for (int var4 = 0; var4 < var3; ++var4) {
			dropItem(Item.snowball.itemID, 1);
		}
	}

	/**
	 * Attack the specified entity using a ranged attack.
	 */
	@Override
	public void attackEntityWithRangedAttack(
			final EntityLiving par1EntityLiving, final float par2) {
		final EntitySnowball var3 = new EntitySnowball(worldObj, this);
		final double var4 = par1EntityLiving.posX - posX;
		final double var6 = par1EntityLiving.posY
				+ par1EntityLiving.getEyeHeight() - 1.100000023841858D
				- var3.posY;
		final double var8 = par1EntityLiving.posZ - posZ;
		final float var10 = MathHelper.sqrt_double(var4 * var4 + var8 * var8) * 0.2F;
		var3.setThrowableHeading(var4, var6 + var10, var8, 1.6F, 12.0F);
		playSound("random.bow", 1.0F,
				1.0F / (getRNG().nextFloat() * 0.4F + 0.8F));
		worldObj.spawnEntityInWorld(var3);
	}
}
