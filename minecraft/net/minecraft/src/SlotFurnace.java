package net.minecraft.src;

public class SlotFurnace extends Slot {
	/** The player that is using the GUI where this slot resides. */
	private final EntityPlayer thePlayer;
	private int field_75228_b;

	public SlotFurnace(final EntityPlayer par1EntityPlayer,
			final IInventory par2IInventory, final int par3, final int par4,
			final int par5) {
		super(par2IInventory, par3, par4, par5);
		thePlayer = par1EntityPlayer;
	}

	/**
	 * Check if the stack is a valid item for this slot. Always true beside for
	 * the armor slots.
	 */
	@Override
	public boolean isItemValid(final ItemStack par1ItemStack) {
		return false;
	}

	/**
	 * Decrease the size of the stack in slot (first int arg) by the amount of
	 * the second int arg. Returns the new stack.
	 */
	@Override
	public ItemStack decrStackSize(final int par1) {
		if (getHasStack()) {
			field_75228_b += Math.min(par1, getStack().stackSize);
		}

		return super.decrStackSize(par1);
	}

	@Override
	public void onPickupFromSlot(final EntityPlayer par1EntityPlayer,
			final ItemStack par2ItemStack) {
		this.onCrafting(par2ItemStack);
		super.onPickupFromSlot(par1EntityPlayer, par2ItemStack);
	}

	/**
	 * the itemStack passed in is the output - ie, iron ingots, and pickaxes,
	 * not ore and wood. Typically increases an internal count then calls
	 * onCrafting(item).
	 */
	@Override
	protected void onCrafting(final ItemStack par1ItemStack, final int par2) {
		field_75228_b += par2;
		this.onCrafting(par1ItemStack);
	}

	/**
	 * the itemStack passed in is the output - ie, iron ingots, and pickaxes,
	 * not ore and wood.
	 */
	@Override
	protected void onCrafting(final ItemStack par1ItemStack) {
		par1ItemStack.onCrafting(thePlayer.worldObj, thePlayer, field_75228_b);

		if (!thePlayer.worldObj.isRemote) {
			int var2 = field_75228_b;
			final float var3 = FurnaceRecipes.smelting().getExperience(
					par1ItemStack.itemID);
			int var4;

			if (var3 == 0.0F) {
				var2 = 0;
			} else if (var3 < 1.0F) {
				var4 = MathHelper.floor_float(var2 * var3);

				if (var4 < MathHelper.ceiling_float_int(var2 * var3)
						&& (float) Math.random() < var2 * var3 - var4) {
					++var4;
				}

				var2 = var4;
			}

			while (var2 > 0) {
				var4 = EntityXPOrb.getXPSplit(var2);
				var2 -= var4;
				thePlayer.worldObj.spawnEntityInWorld(new EntityXPOrb(
						thePlayer.worldObj, thePlayer.posX,
						thePlayer.posY + 0.5D, thePlayer.posZ + 0.5D, var4));
			}
		}

		field_75228_b = 0;

		if (par1ItemStack.itemID == Item.ingotIron.itemID) {
			thePlayer.addStat(AchievementList.acquireIron, 1);
		}

		if (par1ItemStack.itemID == Item.fishCooked.itemID) {
			thePlayer.addStat(AchievementList.cookFish, 1);
		}
	}
}
