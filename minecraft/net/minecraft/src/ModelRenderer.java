package net.minecraft.src;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

public class ModelRenderer {
	/** The size of the texture file's width in pixels. */
	public float textureWidth;

	/** The size of the texture file's height in pixels. */
	public float textureHeight;

	/** The X offset into the texture used for displaying this model */
	private int textureOffsetX;

	/** The Y offset into the texture used for displaying this model */
	private int textureOffsetY;
	public float rotationPointX;
	public float rotationPointY;
	public float rotationPointZ;
	public float rotateAngleX;
	public float rotateAngleY;
	public float rotateAngleZ;
	private boolean compiled;

	/** The GL display list rendered by the Tessellator for this model */
	private int displayList;
	public boolean mirror;
	public boolean showModel;

	/** Hides the model. */
	public boolean isHidden;
	public List cubeList;
	public List childModels;
	public final String boxName;
	private final ModelBase baseModel;
	public float field_82906_o;
	public float field_82908_p;
	public float field_82907_q;

	public ModelRenderer(final ModelBase par1ModelBase, final String par2Str) {
		textureWidth = 64.0F;
		textureHeight = 32.0F;
		compiled = false;
		displayList = 0;
		mirror = false;
		showModel = true;
		isHidden = false;
		cubeList = new ArrayList();
		baseModel = par1ModelBase;
		par1ModelBase.boxList.add(this);
		boxName = par2Str;
		setTextureSize(par1ModelBase.textureWidth, par1ModelBase.textureHeight);
	}

	public ModelRenderer(final ModelBase par1ModelBase) {
		this(par1ModelBase, (String) null);
	}

	public ModelRenderer(final ModelBase par1ModelBase, final int par2,
			final int par3) {
		this(par1ModelBase);
		setTextureOffset(par2, par3);
	}

	/**
	 * Sets the current box's rotation points and rotation angles to another
	 * box.
	 */
	public void addChild(final ModelRenderer par1ModelRenderer) {
		if (childModels == null) {
			childModels = new ArrayList();
		}

		childModels.add(par1ModelRenderer);
	}

	public ModelRenderer setTextureOffset(final int par1, final int par2) {
		textureOffsetX = par1;
		textureOffsetY = par2;
		return this;
	}

	public ModelRenderer addBox(String par1Str, final float par2,
			final float par3, final float par4, final int par5, final int par6,
			final int par7) {
		par1Str = boxName + "." + par1Str;
		final TextureOffset var8 = baseModel.getTextureOffset(par1Str);
		setTextureOffset(var8.textureOffsetX, var8.textureOffsetY);
		cubeList.add(new ModelBox(this, textureOffsetX, textureOffsetY, par2,
				par3, par4, par5, par6, par7, 0.0F).func_78244_a(par1Str));
		return this;
	}

	public ModelRenderer addBox(final float par1, final float par2,
			final float par3, final int par4, final int par5, final int par6) {
		cubeList.add(new ModelBox(this, textureOffsetX, textureOffsetY, par1,
				par2, par3, par4, par5, par6, 0.0F));
		return this;
	}

	/**
	 * Creates a textured box. Args: originX, originY, originZ, width, height,
	 * depth, scaleFactor.
	 */
	public void addBox(final float par1, final float par2, final float par3,
			final int par4, final int par5, final int par6, final float par7) {
		cubeList.add(new ModelBox(this, textureOffsetX, textureOffsetY, par1,
				par2, par3, par4, par5, par6, par7));
	}

	public void setRotationPoint(final float par1, final float par2,
			final float par3) {
		rotationPointX = par1;
		rotationPointY = par2;
		rotationPointZ = par3;
	}

	public void render(final float par1) {
		if (!isHidden) {
			if (showModel) {
				if (!compiled) {
					compileDisplayList(par1);
				}

				GL11.glTranslatef(field_82906_o, field_82908_p, field_82907_q);
				int var2;

				if (rotateAngleX == 0.0F && rotateAngleY == 0.0F
						&& rotateAngleZ == 0.0F) {
					if (rotationPointX == 0.0F && rotationPointY == 0.0F
							&& rotationPointZ == 0.0F) {
						GL11.glCallList(displayList);

						if (childModels != null) {
							for (var2 = 0; var2 < childModels.size(); ++var2) {
								((ModelRenderer) childModels.get(var2))
										.render(par1);
							}
						}
					} else {
						GL11.glTranslatef(rotationPointX * par1, rotationPointY
								* par1, rotationPointZ * par1);
						GL11.glCallList(displayList);

						if (childModels != null) {
							for (var2 = 0; var2 < childModels.size(); ++var2) {
								((ModelRenderer) childModels.get(var2))
										.render(par1);
							}
						}

						GL11.glTranslatef(-rotationPointX * par1,
								-rotationPointY * par1, -rotationPointZ * par1);
					}
				} else {
					GL11.glPushMatrix();
					GL11.glTranslatef(rotationPointX * par1, rotationPointY
							* par1, rotationPointZ * par1);

					if (rotateAngleZ != 0.0F) {
						GL11.glRotatef(rotateAngleZ * (180F / (float) Math.PI),
								0.0F, 0.0F, 1.0F);
					}

					if (rotateAngleY != 0.0F) {
						GL11.glRotatef(rotateAngleY * (180F / (float) Math.PI),
								0.0F, 1.0F, 0.0F);
					}

					if (rotateAngleX != 0.0F) {
						GL11.glRotatef(rotateAngleX * (180F / (float) Math.PI),
								1.0F, 0.0F, 0.0F);
					}

					GL11.glCallList(displayList);

					if (childModels != null) {
						for (var2 = 0; var2 < childModels.size(); ++var2) {
							((ModelRenderer) childModels.get(var2))
									.render(par1);
						}
					}

					GL11.glPopMatrix();
				}

				GL11.glTranslatef(-field_82906_o, -field_82908_p,
						-field_82907_q);
			}
		}
	}

	public void renderWithRotation(final float par1) {
		if (!isHidden) {
			if (showModel) {
				if (!compiled) {
					compileDisplayList(par1);
				}

				GL11.glPushMatrix();
				GL11.glTranslatef(rotationPointX * par1, rotationPointY * par1,
						rotationPointZ * par1);

				if (rotateAngleY != 0.0F) {
					GL11.glRotatef(rotateAngleY * (180F / (float) Math.PI),
							0.0F, 1.0F, 0.0F);
				}

				if (rotateAngleX != 0.0F) {
					GL11.glRotatef(rotateAngleX * (180F / (float) Math.PI),
							1.0F, 0.0F, 0.0F);
				}

				if (rotateAngleZ != 0.0F) {
					GL11.glRotatef(rotateAngleZ * (180F / (float) Math.PI),
							0.0F, 0.0F, 1.0F);
				}

				GL11.glCallList(displayList);
				GL11.glPopMatrix();
			}
		}
	}

	/**
	 * Allows the changing of Angles after a box has been rendered
	 */
	public void postRender(final float par1) {
		if (!isHidden) {
			if (showModel) {
				if (!compiled) {
					compileDisplayList(par1);
				}

				if (rotateAngleX == 0.0F && rotateAngleY == 0.0F
						&& rotateAngleZ == 0.0F) {
					if (rotationPointX != 0.0F || rotationPointY != 0.0F
							|| rotationPointZ != 0.0F) {
						GL11.glTranslatef(rotationPointX * par1, rotationPointY
								* par1, rotationPointZ * par1);
					}
				} else {
					GL11.glTranslatef(rotationPointX * par1, rotationPointY
							* par1, rotationPointZ * par1);

					if (rotateAngleZ != 0.0F) {
						GL11.glRotatef(rotateAngleZ * (180F / (float) Math.PI),
								0.0F, 0.0F, 1.0F);
					}

					if (rotateAngleY != 0.0F) {
						GL11.glRotatef(rotateAngleY * (180F / (float) Math.PI),
								0.0F, 1.0F, 0.0F);
					}

					if (rotateAngleX != 0.0F) {
						GL11.glRotatef(rotateAngleX * (180F / (float) Math.PI),
								1.0F, 0.0F, 0.0F);
					}
				}
			}
		}
	}

	/**
	 * Compiles a GL display list for this model
	 */
	private void compileDisplayList(final float par1) {
		displayList = GLAllocation.generateDisplayLists(1);
		GL11.glNewList(displayList, GL11.GL_COMPILE);
		final Tessellator var2 = Tessellator.instance;

		for (int var3 = 0; var3 < cubeList.size(); ++var3) {
			((ModelBox) cubeList.get(var3)).render(var2, par1);
		}

		GL11.glEndList();
		compiled = true;
	}

	/**
	 * Returns the model renderer with the new texture parameters.
	 */
	public ModelRenderer setTextureSize(final int par1, final int par2) {
		textureWidth = par1;
		textureHeight = par2;
		return this;
	}
}
