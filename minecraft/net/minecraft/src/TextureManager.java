package net.minecraft.src;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.imageio.ImageIO;

import net.minecraft.client.Minecraft;

public class TextureManager {
	private static TextureManager instance;
	private int nextTextureID = 0;
	private final HashMap texturesMap = new HashMap();
	private final HashMap mapNameToId = new HashMap();

	public static void init() {
		TextureManager.instance = new TextureManager();
	}

	public static TextureManager instance() {
		return TextureManager.instance;
	}

	public int getNextTextureId() {
		return nextTextureID++;
	}

	public void registerTexture(final String par1Str, final Texture par2Texture) {
		mapNameToId.put(par1Str, Integer.valueOf(par2Texture.getTextureId()));

		if (!texturesMap
				.containsKey(Integer.valueOf(par2Texture.getTextureId()))) {
			texturesMap.put(Integer.valueOf(par2Texture.getTextureId()),
					par2Texture);
		}
	}

	public void registerTexture(final Texture par1Texture) {
		if (texturesMap.containsValue(par1Texture)) {
			Minecraft
					.getMinecraft()
					.getLogAgent()
					.logWarning(
							"TextureManager.registerTexture called, but this texture has already been registered. ignoring.");
		} else {
			texturesMap.put(Integer.valueOf(par1Texture.getTextureId()),
					par1Texture);
		}
	}

	public Stitcher createStitcher(final String par1Str) {
		final int var2 = Minecraft.getGLMaximumTextureSize();
		return new Stitcher(par1Str, var2, var2, true);
	}

	public List createTexture(final String par1Str) {
		return createNewTexture(getBasename(par1Str), par1Str,
				(TextureStitched) null);
	}

	public List createNewTexture(final String var1, final String var2,
			final TextureStitched var3) {
		final ArrayList var4 = new ArrayList();
		final ITexturePack var5 = Minecraft.getMinecraft().texturePackList
				.getSelectedTexturePack();

		try {
			BufferedImage var6 = null;
			FileNotFoundException var7 = null;

			try {
				var6 = ImageIO.read(var5.getResourceAsStream("/" + var2));
			} catch (final FileNotFoundException var19) {
				var7 = var19;
			}

			if (var3 != null
					&& var3.loadTexture(this, var5, var1, var2, var6, var4)) {
				return var4;
			}

			if (var7 != null) {
				throw var7;
			}

			final int var8 = var6.getHeight();
			final int var9 = var6.getWidth();
			final String var10 = var1;
			final int var11 = var6.getWidth();
			final int var12 = var6.getHeight();
			final boolean var13 = var12 > var11
					&& var12 / var11 * var11 == var12;

			if (!var13 && !hasAnimationTxt(var2, var5)) {
				if (var9 == var8) {
					var4.add(makeTexture(var1, 2, var9, var8, 10496, 6408,
							9728, 9728, false, var6));
				} else {
					Minecraft
							.getMinecraft()
							.getLogAgent()
							.logWarning(
									"TextureManager.createTexture: Skipping "
											+ var2
											+ " because of broken aspect ratio and not animation");
				}
			} else {
				final int var14 = var9;
				final int var15 = var9;
				final int var16 = var8 / var9;

				for (int var17 = 0; var17 < var16; ++var17) {
					final Texture var18 = makeTexture(var10, 2, var14, var15,
							10496, 6408, 9728, 9728, false,
							var6.getSubimage(0, var15 * var17, var14, var15));
					var4.add(var18);
				}
			}

			return var4;
		} catch (final FileNotFoundException var20) {
			Minecraft
					.getMinecraft()
					.getLogAgent()
					.logWarning(
							"TextureManager.createTexture called for file "
									+ var2
									+ ", but that file does not exist. Ignoring.");
		} catch (final IOException var21) {
			Minecraft
					.getMinecraft()
					.getLogAgent()
					.logWarning(
							"TextureManager.createTexture encountered an IOException when trying to read file "
									+ var2 + ". Ignoring.");
		}

		return var4;
	}

	/**
	 * Strips directory and file extension from the specified path, returning
	 * only the filename
	 */
	private String getBasename(final String par1Str) {
		if (!par1Str.startsWith("ctm/") && !par1Str.startsWith("mods/")) {
			final File var2 = new File(par1Str);
			return var2.getName().substring(0, var2.getName().lastIndexOf(46));
		} else {
			return par1Str.substring(0, par1Str.lastIndexOf(46));
		}
	}

	/**
	 * Returns true if specified texture pack contains animation data for the
	 * specified texture file
	 */
	private boolean hasAnimationTxt(final String par1Str,
			final ITexturePack par2ITexturePack) {
		final String var3 = "/" + par1Str.substring(0, par1Str.lastIndexOf(46))
				+ ".txt";
		final boolean var4 = par2ITexturePack
				.func_98138_b("/" + par1Str, false);
		return Minecraft.getMinecraft().texturePackList
				.getSelectedTexturePack().func_98138_b(var3, !var4);
	}

	public Texture makeTexture(final String par1Str, final int par2,
			final int par3, final int par4, final int par5, final int par6,
			final int par7, final int par8, final boolean par9,
			final BufferedImage par10BufferedImage) {
		final Texture var11 = new Texture(par1Str, par2, par3, par4, par5,
				par6, par7, par8, par10BufferedImage);
		this.registerTexture(var11);
		return var11;
	}

	public Texture createEmptyTexture(final String par1Str, final int par2,
			final int par3, final int par4, final int par5) {
		return makeTexture(par1Str, par2, par3, par4, 10496, par5, 9728, 9728,
				false, (BufferedImage) null);
	}
}
