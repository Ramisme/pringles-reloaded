package net.minecraft.src;

import java.util.Random;

public class BlockLadder extends Block {
	protected BlockLadder(final int par1) {
		super(par1, Material.circuits);
		setCreativeTab(CreativeTabs.tabDecorations);
	}

	/**
	 * Returns a bounding box from the pool of bounding boxes (this means this
	 * box can change after the pool has been cleared to be reused)
	 */
	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(final World par1World,
			final int par2, final int par3, final int par4) {
		setBlockBoundsBasedOnState(par1World, par2, par3, par4);
		return super.getCollisionBoundingBoxFromPool(par1World, par2, par3,
				par4);
	}

	/**
	 * Returns the bounding box of the wired rectangular prism to render.
	 */
	@Override
	public AxisAlignedBB getSelectedBoundingBoxFromPool(final World par1World,
			final int par2, final int par3, final int par4) {
		setBlockBoundsBasedOnState(par1World, par2, par3, par4);
		return super
				.getSelectedBoundingBoxFromPool(par1World, par2, par3, par4);
	}

	/**
	 * Updates the blocks bounds based on its current state. Args: world, x, y,
	 * z
	 */
	@Override
	public void setBlockBoundsBasedOnState(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		updateLadderBounds(par1IBlockAccess.getBlockMetadata(par2, par3, par4));
	}

	/**
	 * Update the ladder block bounds based on the given metadata value.
	 */
	public void updateLadderBounds(final int par1) {
		final float var3 = 0.125F;

		if (par1 == 2) {
			setBlockBounds(0.0F, 0.0F, 1.0F - var3, 1.0F, 1.0F, 1.0F);
		}

		if (par1 == 3) {
			setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, var3);
		}

		if (par1 == 4) {
			setBlockBounds(1.0F - var3, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
		}

		if (par1 == 5) {
			setBlockBounds(0.0F, 0.0F, 0.0F, var3, 1.0F, 1.0F);
		}
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return 8;
	}

	/**
	 * Checks to see if its valid to put this block at the specified
	 * coordinates. Args: world, x, y, z
	 */
	@Override
	public boolean canPlaceBlockAt(final World par1World, final int par2,
			final int par3, final int par4) {
		return par1World.isBlockNormalCube(par2 - 1, par3, par4) ? true
				: par1World.isBlockNormalCube(par2 + 1, par3, par4) ? true
						: par1World.isBlockNormalCube(par2, par3, par4 - 1) ? true
								: par1World.isBlockNormalCube(par2, par3,
										par4 + 1);
	}

	/**
	 * Called when a block is placed using its ItemBlock. Args: World, X, Y, Z,
	 * side, hitX, hitY, hitZ, block metadata
	 */
	@Override
	public int onBlockPlaced(final World par1World, final int par2,
			final int par3, final int par4, final int par5, final float par6,
			final float par7, final float par8, final int par9) {
		int var10 = par9;

		if ((par9 == 0 || par5 == 2)
				&& par1World.isBlockNormalCube(par2, par3, par4 + 1)) {
			var10 = 2;
		}

		if ((var10 == 0 || par5 == 3)
				&& par1World.isBlockNormalCube(par2, par3, par4 - 1)) {
			var10 = 3;
		}

		if ((var10 == 0 || par5 == 4)
				&& par1World.isBlockNormalCube(par2 + 1, par3, par4)) {
			var10 = 4;
		}

		if ((var10 == 0 || par5 == 5)
				&& par1World.isBlockNormalCube(par2 - 1, par3, par4)) {
			var10 = 5;
		}

		return var10;
	}

	/**
	 * Lets the block know when one of its neighbor changes. Doesn't know which
	 * neighbor changed (coordinates passed are their own) Args: x, y, z,
	 * neighbor blockID
	 */
	@Override
	public void onNeighborBlockChange(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		final int var6 = par1World.getBlockMetadata(par2, par3, par4);
		boolean var7 = false;

		if (var6 == 2 && par1World.isBlockNormalCube(par2, par3, par4 + 1)) {
			var7 = true;
		}

		if (var6 == 3 && par1World.isBlockNormalCube(par2, par3, par4 - 1)) {
			var7 = true;
		}

		if (var6 == 4 && par1World.isBlockNormalCube(par2 + 1, par3, par4)) {
			var7 = true;
		}

		if (var6 == 5 && par1World.isBlockNormalCube(par2 - 1, par3, par4)) {
			var7 = true;
		}

		if (!var7) {
			dropBlockAsItem(par1World, par2, par3, par4, var6, 0);
			par1World.setBlockToAir(par2, par3, par4);
		}

		super.onNeighborBlockChange(par1World, par2, par3, par4, par5);
	}

	/**
	 * Returns the quantity of items to drop on block destruction.
	 */
	@Override
	public int quantityDropped(final Random par1Random) {
		return 1;
	}
}
