package net.minecraft.src;

class RedstoneUpdateInfo {
	int x;
	int y;
	int z;
	long updateTime;

	public RedstoneUpdateInfo(final int par1, final int par2, final int par3,
			final long par4) {
		x = par1;
		y = par2;
		z = par3;
		updateTime = par4;
	}
}
