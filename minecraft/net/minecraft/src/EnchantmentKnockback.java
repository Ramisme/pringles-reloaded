package net.minecraft.src;

public class EnchantmentKnockback extends Enchantment {
	protected EnchantmentKnockback(final int par1, final int par2) {
		super(par1, par2, EnumEnchantmentType.weapon);
		setName("knockback");
	}

	/**
	 * Returns the minimal value of enchantability needed on the enchantment
	 * level passed.
	 */
	@Override
	public int getMinEnchantability(final int par1) {
		return 5 + 20 * (par1 - 1);
	}

	/**
	 * Returns the maximum value of enchantability nedded on the enchantment
	 * level passed.
	 */
	@Override
	public int getMaxEnchantability(final int par1) {
		return super.getMinEnchantability(par1) + 50;
	}

	/**
	 * Returns the maximum level that the enchantment can have.
	 */
	@Override
	public int getMaxLevel() {
		return 2;
	}
}
