package net.minecraft.src;

import java.util.List;
import java.util.Random;

public class ComponentNetherBridgeEnd extends ComponentNetherBridgePiece {
	private final int fillSeed;

	public ComponentNetherBridgeEnd(final int par1, final Random par2Random,
			final StructureBoundingBox par3StructureBoundingBox, final int par4) {
		super(par1);
		coordBaseMode = par4;
		boundingBox = par3StructureBoundingBox;
		fillSeed = par2Random.nextInt();
	}

	public static ComponentNetherBridgeEnd func_74971_a(final List par0List,
			final Random par1Random, final int par2, final int par3,
			final int par4, final int par5, final int par6) {
		final StructureBoundingBox var7 = StructureBoundingBox
				.getComponentToAddBoundingBox(par2, par3, par4, -1, -3, 0, 5,
						10, 8, par5);
		return ComponentNetherBridgePiece.isAboveGround(var7)
				&& StructureComponent.findIntersecting(par0List, var7) == null ? new ComponentNetherBridgeEnd(
				par6, par1Random, var7, par5) : null;
	}

	/**
	 * second Part of Structure generating, this for example places Spiderwebs,
	 * Mob Spawners, it closes Mineshafts at the end, it adds Fences...
	 */
	@Override
	public boolean addComponentParts(final World par1World,
			final Random par2Random,
			final StructureBoundingBox par3StructureBoundingBox) {
		final Random var4 = new Random(fillSeed);
		int var5;
		int var6;
		int var7;

		for (var5 = 0; var5 <= 4; ++var5) {
			for (var6 = 3; var6 <= 4; ++var6) {
				var7 = var4.nextInt(8);
				fillWithBlocks(par1World, par3StructureBoundingBox, var5, var6,
						0, var5, var6, var7, Block.netherBrick.blockID,
						Block.netherBrick.blockID, false);
			}
		}

		var5 = var4.nextInt(8);
		fillWithBlocks(par1World, par3StructureBoundingBox, 0, 5, 0, 0, 5,
				var5, Block.netherBrick.blockID, Block.netherBrick.blockID,
				false);
		var5 = var4.nextInt(8);
		fillWithBlocks(par1World, par3StructureBoundingBox, 4, 5, 0, 4, 5,
				var5, Block.netherBrick.blockID, Block.netherBrick.blockID,
				false);

		for (var5 = 0; var5 <= 4; ++var5) {
			var6 = var4.nextInt(5);
			fillWithBlocks(par1World, par3StructureBoundingBox, var5, 2, 0,
					var5, 2, var6, Block.netherBrick.blockID,
					Block.netherBrick.blockID, false);
		}

		for (var5 = 0; var5 <= 4; ++var5) {
			for (var6 = 0; var6 <= 1; ++var6) {
				var7 = var4.nextInt(3);
				fillWithBlocks(par1World, par3StructureBoundingBox, var5, var6,
						0, var5, var6, var7, Block.netherBrick.blockID,
						Block.netherBrick.blockID, false);
			}
		}

		return true;
	}
}
