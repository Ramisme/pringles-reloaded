package net.minecraft.src;

import java.util.ArrayList;
import java.util.List;

class ContainerCreative extends Container {
	/** the list of items in this container */
	public List itemList = new ArrayList();

	public ContainerCreative(final EntityPlayer par1EntityPlayer) {
		final InventoryPlayer var2 = par1EntityPlayer.inventory;
		int var3;

		for (var3 = 0; var3 < 5; ++var3) {
			for (int var4 = 0; var4 < 9; ++var4) {
				addSlotToContainer(new Slot(
						GuiContainerCreative.getInventory(), var3 * 9 + var4,
						9 + var4 * 18, 18 + var3 * 18));
			}
		}

		for (var3 = 0; var3 < 9; ++var3) {
			addSlotToContainer(new Slot(var2, var3, 9 + var3 * 18, 112));
		}

		scrollTo(0.0F);
	}

	@Override
	public boolean canInteractWith(final EntityPlayer par1EntityPlayer) {
		return true;
	}

	/**
	 * Updates the gui slots ItemStack's based on scroll position.
	 */
	public void scrollTo(final float par1) {
		final int var2 = itemList.size() / 9 - 5 + 1;
		int var3 = (int) (par1 * var2 + 0.5D);

		if (var3 < 0) {
			var3 = 0;
		}

		for (int var4 = 0; var4 < 5; ++var4) {
			for (int var5 = 0; var5 < 9; ++var5) {
				final int var6 = var5 + (var4 + var3) * 9;

				if (var6 >= 0 && var6 < itemList.size()) {
					GuiContainerCreative.getInventory()
							.setInventorySlotContents(var5 + var4 * 9,
									(ItemStack) itemList.get(var6));
				} else {
					GuiContainerCreative.getInventory()
							.setInventorySlotContents(var5 + var4 * 9,
									(ItemStack) null);
				}
			}
		}
	}

	/**
	 * theCreativeContainer seems to be hard coded to 9x5 items
	 */
	public boolean hasMoreThan1PageOfItemsInList() {
		return itemList.size() > 45;
	}

	@Override
	protected void retrySlotClick(final int par1, final int par2,
			final boolean par3, final EntityPlayer par4EntityPlayer) {
	}

	/**
	 * Called when a player shift-clicks on a slot. You must override this or
	 * you will crash when someone does that.
	 */
	@Override
	public ItemStack transferStackInSlot(final EntityPlayer par1EntityPlayer,
			final int par2) {
		if (par2 >= inventorySlots.size() - 9 && par2 < inventorySlots.size()) {
			final Slot var3 = (Slot) inventorySlots.get(par2);

			if (var3 != null && var3.getHasStack()) {
				var3.putStack((ItemStack) null);
			}
		}

		return null;
	}

	@Override
	public boolean func_94530_a(final ItemStack par1ItemStack,
			final Slot par2Slot) {
		return par2Slot.yDisplayPosition > 90;
	}

	@Override
	public boolean func_94531_b(final Slot par1Slot) {
		return par1Slot.inventory instanceof InventoryPlayer
				|| par1Slot.yDisplayPosition > 90
				&& par1Slot.xDisplayPosition <= 162;
	}
}
