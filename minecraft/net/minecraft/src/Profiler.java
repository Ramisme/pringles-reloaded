package net.minecraft.src;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Profiler {
	/** List of parent sections */
	private final List sectionList = new ArrayList();

	/** List of timestamps (System.nanoTime) */
	private final List timestampList = new ArrayList();

	/** Flag profiling enabled */
	public boolean profilingEnabled = false;

	/** Current profiling section */
	private String profilingSection = "";

	/** Profiling map */
	private final Map profilingMap = new HashMap();
	public boolean profilerGlobalEnabled = true;
	private boolean profilerLocalEnabled;
	private long startTickNano;
	public long timeTickNano;
	private long startUpdateChunksNano;
	public long timeUpdateChunksNano;

	public Profiler() {
		profilerLocalEnabled = profilerGlobalEnabled;
		startTickNano = 0L;
		timeTickNano = 0L;
		startUpdateChunksNano = 0L;
		timeUpdateChunksNano = 0L;
	}

	/**
	 * Clear profiling.
	 */
	public void clearProfiling() {
		profilingMap.clear();
		profilingSection = "";
		sectionList.clear();
		profilerLocalEnabled = profilerGlobalEnabled;
	}

	/**
	 * Start section
	 */
	public void startSection(final String par1Str) {
		if (Config.getGameSettings().showDebugInfo) {
			if (startTickNano == 0L && par1Str.equals("tick")) {
				startTickNano = System.nanoTime();
			}

			if (startTickNano != 0L && par1Str.equals("preRenderErrors")) {
				timeTickNano = System.nanoTime() - startTickNano;
				startTickNano = 0L;
			}

			if (startUpdateChunksNano == 0L && par1Str.equals("updatechunks")) {
				startUpdateChunksNano = System.nanoTime();
			}

			if (startUpdateChunksNano != 0L && par1Str.equals("terrain")) {
				timeUpdateChunksNano = System.nanoTime()
						- startUpdateChunksNano;
				startUpdateChunksNano = 0L;
			}
		}

		if (profilerLocalEnabled) {
			if (profilingEnabled) {
				if (profilingSection.length() > 0) {
					profilingSection = profilingSection + ".";
				}

				profilingSection = profilingSection + par1Str;
				sectionList.add(profilingSection);
				timestampList.add(Long.valueOf(System.nanoTime()));
			}
		}
	}

	/**
	 * End section
	 */
	public void endSection() {
		if (profilerLocalEnabled) {
			if (profilingEnabled) {
				final long var1 = System.nanoTime();
				final long var3 = ((Long) timestampList.remove(timestampList
						.size() - 1)).longValue();
				sectionList.remove(sectionList.size() - 1);
				final long var5 = var1 - var3;

				if (profilingMap.containsKey(profilingSection)) {
					profilingMap
							.put(profilingSection, Long
									.valueOf(((Long) profilingMap
											.get(profilingSection)).longValue()
											+ var5));
				} else {
					profilingMap.put(profilingSection, Long.valueOf(var5));
				}

				if (var5 > 100000000L) {
					System.out.println("Something\'s taking too long! \'"
							+ profilingSection + "\' took aprox " + var5
							/ 1000000.0D + " ms");
				}

				profilingSection = !sectionList.isEmpty() ? (String) sectionList
						.get(sectionList.size() - 1) : "";
			}
		}
	}

	/**
	 * Get profiling data
	 */
	public List getProfilingData(String par1Str) {
		profilerLocalEnabled = profilerGlobalEnabled;

		if (!profilerLocalEnabled) {
			return new ArrayList(
					Arrays.asList(new ProfilerResult[] { new ProfilerResult(
							"root", 0.0D, 0.0D) }));
		} else if (!profilingEnabled) {
			return null;
		} else {
			long var2 = profilingMap.containsKey("root") ? ((Long) profilingMap
					.get("root")).longValue() : 0L;
			final long var4 = profilingMap.containsKey(par1Str) ? ((Long) profilingMap
					.get(par1Str)).longValue() : -1L;
			final ArrayList var6 = new ArrayList();

			if (par1Str.length() > 0) {
				par1Str = par1Str + ".";
			}

			long var7 = 0L;
			final Iterator var9 = profilingMap.keySet().iterator();

			while (var9.hasNext()) {
				final String var10 = (String) var9.next();

				if (var10.length() > par1Str.length()
						&& var10.startsWith(par1Str)
						&& var10.indexOf(".", par1Str.length() + 1) < 0) {
					var7 += ((Long) profilingMap.get(var10)).longValue();
				}
			}

			final float var20 = var7;

			if (var7 < var4) {
				var7 = var4;
			}

			if (var2 < var7) {
				var2 = var7;
			}

			Iterator var11 = profilingMap.keySet().iterator();
			String var12;

			while (var11.hasNext()) {
				var12 = (String) var11.next();

				if (var12.length() > par1Str.length()
						&& var12.startsWith(par1Str)
						&& var12.indexOf(".", par1Str.length() + 1) < 0) {
					final long var13 = ((Long) profilingMap.get(var12))
							.longValue();
					final double var15 = var13 * 100.0D / var7;
					final double var17 = var13 * 100.0D / var2;
					final String var19 = var12.substring(par1Str.length());
					var6.add(new ProfilerResult(var19, var15, var17));
				}
			}

			var11 = profilingMap.keySet().iterator();

			while (var11.hasNext()) {
				var12 = (String) var11.next();
				profilingMap.put(var12, Long.valueOf(((Long) profilingMap
						.get(var12)).longValue() * 999L / 1000L));
			}

			if (var7 > var20) {
				var6.add(new ProfilerResult("unspecified", (var7 - var20)
						* 100.0D / var7, (var7 - var20) * 100.0D / var2));
			}

			Collections.sort(var6);
			var6.add(0, new ProfilerResult(par1Str, 100.0D, var7 * 100.0D
					/ var2));
			return var6;
		}
	}

	/**
	 * End current section and start a new section
	 */
	public void endStartSection(final String par1Str) {
		if (profilerLocalEnabled) {
			endSection();
			startSection(par1Str);
		}
	}

	public String getNameOfLastSection() {
		return sectionList.size() == 0 ? "[UNKNOWN]" : (String) sectionList
				.get(sectionList.size() - 1);
	}
}
