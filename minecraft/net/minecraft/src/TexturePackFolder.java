package net.minecraft.src;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class TexturePackFolder extends TexturePackImplementation {
	public TexturePackFolder(final String par1, final File par2,
			final ITexturePack par3ITexturePack) {
		super(par1, par2, par2.getName(), par3ITexturePack);
	}

	@Override
	protected InputStream func_98139_b(final String par1Str) throws IOException {
		final File var2 = new File(texturePackFile, par1Str.substring(1));

		if (!var2.exists()) {
			throw new FileNotFoundException(par1Str);
		} else {
			return new BufferedInputStream(new FileInputStream(var2));
		}
	}

	@Override
	public boolean func_98140_c(final String par1Str) {
		final File var2 = new File(texturePackFile, par1Str);
		return var2.exists() && var2.isFile();
	}

	@Override
	public boolean isCompatible() {
		final File var1 = new File(texturePackFile, "textures/");
		final boolean var2 = var1.exists() && var1.isDirectory();
		final boolean var3 = func_98140_c("terrain.png")
				|| func_98140_c("gui/items.png");
		return var2 || !var3;
	}
}
