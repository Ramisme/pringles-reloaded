package net.minecraft.src;

import org.lwjgl.opengl.GL11;

public class ModelEnderCrystal extends ModelBase {
	/** The cube model for the Ender Crystal. */
	private final ModelRenderer cube;

	/** The glass model for the Ender Crystal. */
	private final ModelRenderer glass = new ModelRenderer(this, "glass");

	/** The base model for the Ender Crystal. */
	private ModelRenderer base;

	public ModelEnderCrystal(final float par1, final boolean par2) {
		glass.setTextureOffset(0, 0).addBox(-4.0F, -4.0F, -4.0F, 8, 8, 8);
		cube = new ModelRenderer(this, "cube");
		cube.setTextureOffset(32, 0).addBox(-4.0F, -4.0F, -4.0F, 8, 8, 8);

		if (par2) {
			base = new ModelRenderer(this, "base");
			base.setTextureOffset(0, 16).addBox(-6.0F, 0.0F, -6.0F, 12, 4, 12);
		}
	}

	/**
	 * Sets the models various rotation angles then renders the model.
	 */
	@Override
	public void render(final Entity par1Entity, final float par2,
			final float par3, final float par4, final float par5,
			final float par6, final float par7) {
		GL11.glPushMatrix();
		GL11.glScalef(2.0F, 2.0F, 2.0F);
		GL11.glTranslatef(0.0F, -0.5F, 0.0F);

		if (base != null) {
			base.render(par7);
		}

		GL11.glRotatef(par3, 0.0F, 1.0F, 0.0F);
		GL11.glTranslatef(0.0F, 0.8F + par4, 0.0F);
		GL11.glRotatef(60.0F, 0.7071F, 0.0F, 0.7071F);
		glass.render(par7);
		final float var8 = 0.875F;
		GL11.glScalef(var8, var8, var8);
		GL11.glRotatef(60.0F, 0.7071F, 0.0F, 0.7071F);
		GL11.glRotatef(par3, 0.0F, 1.0F, 0.0F);
		glass.render(par7);
		GL11.glScalef(var8, var8, var8);
		GL11.glRotatef(60.0F, 0.7071F, 0.0F, 0.7071F);
		GL11.glRotatef(par3, 0.0F, 1.0F, 0.0F);
		cube.render(par7);
		GL11.glPopMatrix();
	}
}
