package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet29DestroyEntity extends Packet {
	/** ID of the entity to be destroyed on the client. */
	public int[] entityId;

	public Packet29DestroyEntity() {
	}

	public Packet29DestroyEntity(final int... par1ArrayOfInteger) {
		entityId = par1ArrayOfInteger;
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		entityId = new int[par1DataInputStream.readByte()];

		for (int var2 = 0; var2 < entityId.length; ++var2) {
			entityId[var2] = par1DataInputStream.readInt();
		}
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		par1DataOutputStream.writeByte(entityId.length);

		for (final int element : entityId) {
			par1DataOutputStream.writeInt(element);
		}
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleDestroyEntity(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 1 + entityId.length * 4;
	}
}
