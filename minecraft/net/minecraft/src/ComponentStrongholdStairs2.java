package net.minecraft.src;

import java.util.ArrayList;
import java.util.Random;

public class ComponentStrongholdStairs2 extends ComponentStrongholdStairs {
	public StructureStrongholdPieceWeight strongholdPieceWeight;
	public ComponentStrongholdPortalRoom strongholdPortalRoom;
	public ArrayList field_75026_c = new ArrayList();

	public ComponentStrongholdStairs2(final int par1, final Random par2Random,
			final int par3, final int par4) {
		super(0, par2Random, par3, par4);
	}

	@Override
	public ChunkPosition getCenter() {
		return strongholdPortalRoom != null ? strongholdPortalRoom.getCenter()
				: super.getCenter();
	}
}
