package net.minecraft.src;

public class NpcMerchant implements IMerchant {
	/** Instance of Merchants Inventory. */
	private final InventoryMerchant theMerchantInventory;

	/** This merchant's current player customer. */
	private final EntityPlayer customer;

	/** The MerchantRecipeList instance. */
	private MerchantRecipeList recipeList;

	public NpcMerchant(final EntityPlayer par1EntityPlayer) {
		customer = par1EntityPlayer;
		theMerchantInventory = new InventoryMerchant(par1EntityPlayer, this);
	}

	@Override
	public EntityPlayer getCustomer() {
		return customer;
	}

	@Override
	public void setCustomer(final EntityPlayer par1EntityPlayer) {
	}

	@Override
	public MerchantRecipeList getRecipes(final EntityPlayer par1EntityPlayer) {
		return recipeList;
	}

	@Override
	public void setRecipes(final MerchantRecipeList par1MerchantRecipeList) {
		recipeList = par1MerchantRecipeList;
	}

	@Override
	public void useRecipe(final MerchantRecipe par1MerchantRecipe) {
	}
}
