package net.minecraft.src;

public class TileEntityNote extends TileEntity {
	/** Note to play */
	public byte note = 0;

	/** stores the latest redstone state */
	public boolean previousRedstoneState = false;

	/**
	 * Writes a tile entity to NBT.
	 */
	@Override
	public void writeToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeToNBT(par1NBTTagCompound);
		par1NBTTagCompound.setByte("note", note);
	}

	/**
	 * Reads a tile entity from NBT.
	 */
	@Override
	public void readFromNBT(final NBTTagCompound par1NBTTagCompound) {
		super.readFromNBT(par1NBTTagCompound);
		note = par1NBTTagCompound.getByte("note");

		if (note < 0) {
			note = 0;
		}

		if (note > 24) {
			note = 24;
		}
	}

	/**
	 * change pitch by -> (currentPitch + 1) % 25
	 */
	public void changePitch() {
		note = (byte) ((note + 1) % 25);
		onInventoryChanged();
	}

	/**
	 * plays the stored note
	 */
	public void triggerNote(final World par1World, final int par2,
			final int par3, final int par4) {
		if (par1World.getBlockMaterial(par2, par3 + 1, par4) == Material.air) {
			final Material var5 = par1World.getBlockMaterial(par2, par3 - 1,
					par4);
			byte var6 = 0;

			if (var5 == Material.rock) {
				var6 = 1;
			}

			if (var5 == Material.sand) {
				var6 = 2;
			}

			if (var5 == Material.glass) {
				var6 = 3;
			}

			if (var5 == Material.wood) {
				var6 = 4;
			}

			par1World.addBlockEvent(par2, par3, par4, Block.music.blockID,
					var6, note);
		}
	}
}
