package net.minecraft.src;

final class EnchantmentModifierLiving implements IEnchantmentModifier {
	/**
	 * Used to calculate the (magic) extra damage based on enchantments of
	 * current equipped player item.
	 */
	public int livingModifier;

	/**
	 * Used as parameter to calculate the (magic) extra damage based on
	 * enchantments of current equipped player item.
	 */
	public EntityLiving entityLiving;

	private EnchantmentModifierLiving() {
	}

	/**
	 * Generic method use to calculate modifiers of offensive or defensive
	 * enchantment values.
	 */
	@Override
	public void calculateModifier(final Enchantment par1Enchantment,
			final int par2) {
		livingModifier += par1Enchantment
				.calcModifierLiving(par2, entityLiving);
	}

	EnchantmentModifierLiving(final Empty3 par1Empty3) {
		this();
	}
}
