package net.minecraft.src;

public class ChunkCache implements IBlockAccess {
	private final int chunkX;
	private final int chunkZ;
	private final Chunk[][] chunkArray;

	/** set by !chunk.getAreLevelsEmpty */
	private boolean hasExtendedLevels;

	/** Reference to the World object. */
	private final World worldObj;

	public ChunkCache(final World par1World, final int par2, final int par3,
			final int par4, final int par5, final int par6, final int par7,
			final int par8) {
		worldObj = par1World;
		chunkX = par2 - par8 >> 4;
		chunkZ = par4 - par8 >> 4;
		final int var9 = par5 + par8 >> 4;
		final int var10 = par7 + par8 >> 4;
		chunkArray = new Chunk[var9 - chunkX + 1][var10 - chunkZ + 1];
		hasExtendedLevels = true;
		int var11;
		int var12;
		Chunk var13;

		for (var11 = chunkX; var11 <= var9; ++var11) {
			for (var12 = chunkZ; var12 <= var10; ++var12) {
				var13 = par1World.getChunkFromChunkCoords(var11, var12);

				if (var13 != null) {
					chunkArray[var11 - chunkX][var12 - chunkZ] = var13;
				}
			}
		}

		for (var11 = par2 >> 4; var11 <= par5 >> 4; ++var11) {
			for (var12 = par4 >> 4; var12 <= par7 >> 4; ++var12) {
				var13 = chunkArray[var11 - chunkX][var12 - chunkZ];

				if (var13 != null && !var13.getAreLevelsEmpty(par3, par6)) {
					hasExtendedLevels = false;
				}
			}
		}
	}

	/**
	 * set by !chunk.getAreLevelsEmpty
	 */
	@Override
	public boolean extendedLevelsInChunkCache() {
		return hasExtendedLevels;
	}

	/**
	 * Returns the block ID at coords x,y,z
	 */
	@Override
	public int getBlockId(final int par1, final int par2, final int par3) {
		if (par2 < 0) {
			return 0;
		} else if (par2 >= 256) {
			return 0;
		} else {
			final int var4 = (par1 >> 4) - chunkX;
			final int var5 = (par3 >> 4) - chunkZ;

			if (var4 >= 0 && var4 < chunkArray.length && var5 >= 0
					&& var5 < chunkArray[var4].length) {
				final Chunk var6 = chunkArray[var4][var5];
				return var6 == null ? 0 : var6.getBlockID(par1 & 15, par2,
						par3 & 15);
			} else {
				return 0;
			}
		}
	}

	/**
	 * Returns the TileEntity associated with a given block in X,Y,Z
	 * coordinates, or null if no TileEntity exists
	 */
	@Override
	public TileEntity getBlockTileEntity(final int par1, final int par2,
			final int par3) {
		final int var4 = (par1 >> 4) - chunkX;
		final int var5 = (par3 >> 4) - chunkZ;
		return chunkArray[var4][var5].getChunkBlockTileEntity(par1 & 15, par2,
				par3 & 15);
	}

	@Override
	public float getBrightness(final int par1, final int par2, final int par3,
			final int par4) {
		int var5 = getLightValue(par1, par2, par3);

		if (var5 < par4) {
			var5 = par4;
		}

		return worldObj.provider.lightBrightnessTable[var5];
	}

	/**
	 * Any Light rendered on a 1.8 Block goes through here
	 */
	@Override
	public int getLightBrightnessForSkyBlocks(final int par1, final int par2,
			final int par3, final int par4) {
		final int var5 = getSkyBlockTypeBrightness(EnumSkyBlock.Sky, par1,
				par2, par3);
		int var6 = getSkyBlockTypeBrightness(EnumSkyBlock.Block, par1, par2,
				par3);

		if (var6 < par4) {
			var6 = par4;
		}

		return var5 << 20 | var6 << 4;
	}

	/**
	 * Returns how bright the block is shown as which is the block's light value
	 * looked up in a lookup table (light values aren't linear for brightness).
	 * Args: x, y, z
	 */
	@Override
	public float getLightBrightness(final int par1, final int par2,
			final int par3) {
		return worldObj.provider.lightBrightnessTable[getLightValue(par1, par2,
				par3)];
	}

	/**
	 * Gets the light value of the specified block coords. Args: x, y, z
	 */
	public int getLightValue(final int par1, final int par2, final int par3) {
		return getLightValueExt(par1, par2, par3, true);
	}

	/**
	 * Get light value with flag
	 */
	public int getLightValueExt(final int par1, final int par2, final int par3,
			final boolean par4) {
		if (par1 >= -30000000 && par3 >= -30000000 && par1 < 30000000
				&& par3 <= 30000000) {
			int var5;
			int var6;

			if (par4) {
				var5 = getBlockId(par1, par2, par3);

				if (var5 == Block.stoneSingleSlab.blockID
						|| var5 == Block.woodSingleSlab.blockID
						|| var5 == Block.tilledField.blockID
						|| var5 == Block.stairsWoodOak.blockID
						|| var5 == Block.stairsCobblestone.blockID) {
					var6 = getLightValueExt(par1, par2 + 1, par3, false);
					final int var7 = getLightValueExt(par1 + 1, par2, par3,
							false);
					final int var8 = getLightValueExt(par1 - 1, par2, par3,
							false);
					final int var9 = getLightValueExt(par1, par2, par3 + 1,
							false);
					final int var10 = getLightValueExt(par1, par2, par3 - 1,
							false);

					if (var7 > var6) {
						var6 = var7;
					}

					if (var8 > var6) {
						var6 = var8;
					}

					if (var9 > var6) {
						var6 = var9;
					}

					if (var10 > var6) {
						var6 = var10;
					}

					return var6;
				}
			}

			if (par2 < 0) {
				return 0;
			} else if (par2 >= 256) {
				var5 = 15 - worldObj.skylightSubtracted;

				if (var5 < 0) {
					var5 = 0;
				}

				return var5;
			} else {
				var5 = (par1 >> 4) - chunkX;
				var6 = (par3 >> 4) - chunkZ;
				return chunkArray[var5][var6].getBlockLightValue(par1 & 15,
						par2, par3 & 15, worldObj.skylightSubtracted);
			}
		} else {
			return 15;
		}
	}

	/**
	 * Returns the block metadata at coords x,y,z
	 */
	@Override
	public int getBlockMetadata(final int par1, final int par2, final int par3) {
		if (par2 < 0) {
			return 0;
		} else if (par2 >= 256) {
			return 0;
		} else {
			final int var4 = (par1 >> 4) - chunkX;
			final int var5 = (par3 >> 4) - chunkZ;
			return chunkArray[var4][var5].getBlockMetadata(par1 & 15, par2,
					par3 & 15);
		}
	}

	/**
	 * Returns the block's material.
	 */
	@Override
	public Material getBlockMaterial(final int par1, final int par2,
			final int par3) {
		final int var4 = getBlockId(par1, par2, par3);
		return var4 == 0 ? Material.air : Block.blocksList[var4].blockMaterial;
	}

	/**
	 * Gets the biome for a given set of x/z coordinates
	 */
	@Override
	public BiomeGenBase getBiomeGenForCoords(final int par1, final int par2) {
		return worldObj.getBiomeGenForCoords(par1, par2);
	}

	/**
	 * Returns true if the block at the specified coordinates is an opaque cube.
	 * Args: x, y, z
	 */
	@Override
	public boolean isBlockOpaqueCube(final int par1, final int par2,
			final int par3) {
		final Block var4 = Block.blocksList[getBlockId(par1, par2, par3)];
		return var4 == null ? false : var4.isOpaqueCube();
	}

	/**
	 * Indicate if a material is a normal solid opaque cube.
	 */
	@Override
	public boolean isBlockNormalCube(final int par1, final int par2,
			final int par3) {
		final Block var4 = Block.blocksList[getBlockId(par1, par2, par3)];
		return var4 == null ? false : var4.blockMaterial.blocksMovement()
				&& var4.renderAsNormalBlock();
	}

	/**
	 * Returns true if the block at the given coordinate has a solid (buildable)
	 * top surface.
	 */
	@Override
	public boolean doesBlockHaveSolidTopSurface(final int par1, final int par2,
			final int par3) {
		final Block var4 = Block.blocksList[getBlockId(par1, par2, par3)];
		return worldObj.isBlockTopFacingSurfaceSolid(var4,
				getBlockMetadata(par1, par2, par3));
	}

	/**
	 * Return the Vec3Pool object for this world.
	 */
	@Override
	public Vec3Pool getWorldVec3Pool() {
		return worldObj.getWorldVec3Pool();
	}

	/**
	 * Returns true if the block at the specified coordinates is empty
	 */
	@Override
	public boolean isAirBlock(final int par1, final int par2, final int par3) {
		final Block var4 = Block.blocksList[getBlockId(par1, par2, par3)];
		return var4 == null;
	}

	/**
	 * Brightness for SkyBlock.Sky is clear white and (through color computing
	 * it is assumed) DEPENDENT ON DAYTIME. Brightness for SkyBlock.Block is
	 * yellowish and independent.
	 */
	public int getSkyBlockTypeBrightness(final EnumSkyBlock par1EnumSkyBlock,
			final int par2, int par3, final int par4) {
		if (par3 < 0) {
			par3 = 0;
		}

		if (par3 >= 256) {
			par3 = 255;
		}

		if (par3 >= 0 && par3 < 256 && par2 >= -30000000 && par4 >= -30000000
				&& par2 < 30000000 && par4 <= 30000000) {
			if (par1EnumSkyBlock == EnumSkyBlock.Sky
					&& worldObj.provider.hasNoSky) {
				return 0;
			} else {
				int var5;
				int var6;

				if (Block.useNeighborBrightness[getBlockId(par2, par3, par4)]) {
					var5 = getSpecialBlockBrightness(par1EnumSkyBlock, par2,
							par3 + 1, par4);
					var6 = getSpecialBlockBrightness(par1EnumSkyBlock,
							par2 + 1, par3, par4);
					final int var7 = getSpecialBlockBrightness(
							par1EnumSkyBlock, par2 - 1, par3, par4);
					final int var8 = getSpecialBlockBrightness(
							par1EnumSkyBlock, par2, par3, par4 + 1);
					final int var9 = getSpecialBlockBrightness(
							par1EnumSkyBlock, par2, par3, par4 - 1);

					if (var6 > var5) {
						var5 = var6;
					}

					if (var7 > var5) {
						var5 = var7;
					}

					if (var8 > var5) {
						var5 = var8;
					}

					if (var9 > var5) {
						var5 = var9;
					}

					return var5;
				} else {
					var5 = (par2 >> 4) - chunkX;
					var6 = (par4 >> 4) - chunkZ;
					return chunkArray[var5][var6].getSavedLightValue(
							par1EnumSkyBlock, par2 & 15, par3, par4 & 15);
				}
			}
		} else {
			return par1EnumSkyBlock.defaultLightValue;
		}
	}

	/**
	 * is only used on stairs and tilled fields
	 */
	public int getSpecialBlockBrightness(final EnumSkyBlock par1EnumSkyBlock,
			final int par2, int par3, final int par4) {
		if (par3 < 0) {
			par3 = 0;
		}

		if (par3 >= 256) {
			par3 = 255;
		}

		if (par3 >= 0 && par3 < 256 && par2 >= -30000000 && par4 >= -30000000
				&& par2 < 30000000 && par4 <= 30000000) {
			final int var5 = (par2 >> 4) - chunkX;
			final int var6 = (par4 >> 4) - chunkZ;
			return chunkArray[var5][var6].getSavedLightValue(par1EnumSkyBlock,
					par2 & 15, par3, par4 & 15);
		} else {
			return par1EnumSkyBlock.defaultLightValue;
		}
	}

	/**
	 * Returns current world height.
	 */
	@Override
	public int getHeight() {
		return 256;
	}

	/**
	 * Is this block powering in the specified direction Args: x, y, z,
	 * direction
	 */
	@Override
	public int isBlockProvidingPowerTo(final int par1, final int par2,
			final int par3, final int par4) {
		final int var5 = getBlockId(par1, par2, par3);
		return var5 == 0 ? 0 : Block.blocksList[var5].isProvidingStrongPower(
				this, par1, par2, par3, par4);
	}
}
