package net.minecraft.src;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

public abstract class MapGenStructure extends MapGenBase {
	/**
	 * Used to store a list of all structures that have been recursively
	 * generated. Used so that during recursive generation, the structure
	 * generator can avoid generating structures that intersect ones that have
	 * already been placed.
	 */
	protected Map structureMap = new HashMap();

	/**
	 * Recursively called by generate() (generate) and optionally by itself.
	 */
	@Override
	protected void recursiveGenerate(final World par1World, final int par2,
			final int par3, final int par4, final int par5,
			final byte[] par6ArrayOfByte) {
		if (!structureMap.containsKey(Long.valueOf(ChunkCoordIntPair
				.chunkXZ2Int(par2, par3)))) {
			rand.nextInt();

			try {
				if (canSpawnStructureAtCoords(par2, par3)) {
					final StructureStart var7 = getStructureStart(par2, par3);
					structureMap.put(Long.valueOf(ChunkCoordIntPair
							.chunkXZ2Int(par2, par3)), var7);
				}
			} catch (final Throwable var10) {
				final CrashReport var8 = CrashReport.makeCrashReport(var10,
						"Exception preparing structure feature");
				final CrashReportCategory var9 = var8
						.makeCategory("Feature being prepared");
				var9.addCrashSectionCallable("Is feature chunk",
						new CallableIsFeatureChunk(this, par2, par3));
				var9.addCrashSection(
						"Chunk location",
						String.format(
								"%d,%d",
								new Object[] { Integer.valueOf(par2),
										Integer.valueOf(par3) }));
				var9.addCrashSectionCallable("Chunk pos hash",
						new CallableChunkPosHash(this, par2, par3));
				var9.addCrashSectionCallable("Structure type",
						new CallableStructureType(this));
				throw new ReportedException(var8);
			}
		}
	}

	/**
	 * Generates structures in specified chunk next to existing structures. Does
	 * *not* generate StructureStarts.
	 */
	public boolean generateStructuresInChunk(final World par1World,
			final Random par2Random, final int par3, final int par4) {
		final int var5 = (par3 << 4) + 8;
		final int var6 = (par4 << 4) + 8;
		boolean var7 = false;
		final Iterator var8 = structureMap.values().iterator();

		while (var8.hasNext()) {
			final StructureStart var9 = (StructureStart) var8.next();

			if (var9.isSizeableStructure()
					&& var9.getBoundingBox().intersectsWith(var5, var6,
							var5 + 15, var6 + 15)) {
				var9.generateStructure(par1World, par2Random,
						new StructureBoundingBox(var5, var6, var5 + 15,
								var6 + 15));
				var7 = true;
			}
		}

		return var7;
	}

	/**
	 * Returns true if the structure generator has generated a structure located
	 * at the given position tuple.
	 */
	public boolean hasStructureAt(final int par1, final int par2, final int par3) {
		final Iterator var4 = structureMap.values().iterator();

		while (var4.hasNext()) {
			final StructureStart var5 = (StructureStart) var4.next();

			if (var5.isSizeableStructure()
					&& var5.getBoundingBox().intersectsWith(par1, par3, par1,
							par3)) {
				final Iterator var6 = var5.getComponents().iterator();

				while (var6.hasNext()) {
					final StructureComponent var7 = (StructureComponent) var6
							.next();

					if (var7.getBoundingBox().isVecInside(par1, par2, par3)) {
						return true;
					}
				}
			}
		}

		return false;
	}

	public ChunkPosition getNearestInstance(final World par1World,
			final int par2, final int par3, final int par4) {
		worldObj = par1World;
		rand.setSeed(par1World.getSeed());
		final long var5 = rand.nextLong();
		final long var7 = rand.nextLong();
		final long var9 = (par2 >> 4) * var5;
		final long var11 = (par4 >> 4) * var7;
		rand.setSeed(var9 ^ var11 ^ par1World.getSeed());
		recursiveGenerate(par1World, par2 >> 4, par4 >> 4, 0, 0, (byte[]) null);
		double var13 = Double.MAX_VALUE;
		ChunkPosition var15 = null;
		final Iterator var16 = structureMap.values().iterator();
		ChunkPosition var19;
		int var21;
		int var20;
		double var23;
		int var22;

		while (var16.hasNext()) {
			final StructureStart var17 = (StructureStart) var16.next();

			if (var17.isSizeableStructure()) {
				final StructureComponent var18 = (StructureComponent) var17
						.getComponents().get(0);
				var19 = var18.getCenter();
				var20 = var19.x - par2;
				var21 = var19.y - par3;
				var22 = var19.z - par4;
				var23 = var20 + var20 * var21 * var21 + var22 * var22;

				if (var23 < var13) {
					var13 = var23;
					var15 = var19;
				}
			}
		}

		if (var15 != null) {
			return var15;
		} else {
			final List var25 = getCoordList();

			if (var25 != null) {
				ChunkPosition var26 = null;
				final Iterator var27 = var25.iterator();

				while (var27.hasNext()) {
					var19 = (ChunkPosition) var27.next();
					var20 = var19.x - par2;
					var21 = var19.y - par3;
					var22 = var19.z - par4;
					var23 = var20 + var20 * var21 * var21 + var22 * var22;

					if (var23 < var13) {
						var13 = var23;
						var26 = var19;
					}
				}

				return var26;
			} else {
				return null;
			}
		}
	}

	/**
	 * Returns a list of other locations at which the structure generation has
	 * been run, or null if not relevant to this structure generator.
	 */
	protected List getCoordList() {
		return null;
	}

	protected abstract boolean canSpawnStructureAtCoords(int var1, int var2);

	protected abstract StructureStart getStructureStart(int var1, int var2);
}
