package net.minecraft.src;

import java.util.concurrent.Callable;

class CallableOSInfo implements Callable {
	/** Reference to the CrashReport object. */
	final CrashReport theCrashReport;

	CallableOSInfo(final CrashReport par1CrashReport) {
		theCrashReport = par1CrashReport;
	}

	public String getOsAsString() {
		return System.getProperty("os.name") + " ("
				+ System.getProperty("os.arch") + ") version "
				+ System.getProperty("os.version");
	}

	@Override
	public Object call() {
		return getOsAsString();
	}
}
