package net.minecraft.src;

import java.util.List;

import net.minecraft.server.MinecraftServer;

public class CommandGameMode extends CommandBase {
	@Override
	public String getCommandName() {
		return "gamemode";
	}

	/**
	 * Return the required permission level for this command.
	 */
	@Override
	public int getRequiredPermissionLevel() {
		return 2;
	}

	@Override
	public String getCommandUsage(final ICommandSender par1ICommandSender) {
		return par1ICommandSender.translateString("commands.gamemode.usage",
				new Object[0]);
	}

	@Override
	public void processCommand(final ICommandSender par1ICommandSender,
			final String[] par2ArrayOfStr) {
		if (par2ArrayOfStr.length > 0) {
			final EnumGameType var3 = getGameModeFromCommand(
					par1ICommandSender, par2ArrayOfStr[0]);
			final EntityPlayerMP var4 = par2ArrayOfStr.length >= 2 ? CommandBase
					.func_82359_c(par1ICommandSender, par2ArrayOfStr[1])
					: CommandBase.getCommandSenderAsPlayer(par1ICommandSender);
			var4.setGameType(var3);
			var4.fallDistance = 0.0F;
			final String var5 = StatCollector.translateToLocal("gameMode."
					+ var3.getName());

			if (var4 != par1ICommandSender) {
				CommandBase.notifyAdmins(par1ICommandSender, 1,
						"commands.gamemode.success.other",
						new Object[] { var4.getEntityName(), var5 });
			} else {
				CommandBase
						.notifyAdmins(par1ICommandSender, 1,
								"commands.gamemode.success.self",
								new Object[] { var5 });
			}
		} else {
			throw new WrongUsageException("commands.gamemode.usage",
					new Object[0]);
		}
	}

	/**
	 * Gets the Game Mode specified in the command.
	 */
	protected EnumGameType getGameModeFromCommand(
			final ICommandSender par1ICommandSender, final String par2Str) {
		return !par2Str.equalsIgnoreCase(EnumGameType.SURVIVAL.getName())
				&& !par2Str.equalsIgnoreCase("s") ? !par2Str
				.equalsIgnoreCase(EnumGameType.CREATIVE.getName())
				&& !par2Str.equalsIgnoreCase("c") ? !par2Str
				.equalsIgnoreCase(EnumGameType.ADVENTURE.getName())
				&& !par2Str.equalsIgnoreCase("a") ? WorldSettings
				.getGameTypeById(CommandBase.parseIntBounded(
						par1ICommandSender, par2Str, 0,
						EnumGameType.values().length - 2))
				: EnumGameType.ADVENTURE : EnumGameType.CREATIVE
				: EnumGameType.SURVIVAL;
	}

	/**
	 * Adds the strings available in this command to the given list of tab
	 * completion options.
	 */
	@Override
	public List addTabCompletionOptions(
			final ICommandSender par1ICommandSender,
			final String[] par2ArrayOfStr) {
		return par2ArrayOfStr.length == 1 ? CommandBase
				.getListOfStringsMatchingLastWord(par2ArrayOfStr, new String[] {
						"survival", "creative", "adventure" })
				: par2ArrayOfStr.length == 2 ? CommandBase
						.getListOfStringsMatchingLastWord(par2ArrayOfStr,
								getListOfPlayerUsernames()) : null;
	}

	/**
	 * Returns String array containing all player usernames in the server.
	 */
	protected String[] getListOfPlayerUsernames() {
		return MinecraftServer.getServer().getAllUsernames();
	}

	/**
	 * Return whether the specified command parameter index is a username
	 * parameter.
	 */
	@Override
	public boolean isUsernameIndex(final String[] par1ArrayOfStr, final int par2) {
		return par2 == 1;
	}
}
