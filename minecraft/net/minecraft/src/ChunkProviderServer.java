package net.minecraft.src;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class ChunkProviderServer implements IChunkProvider {
	/**
	 * used by unload100OldestChunks to iterate the loadedChunkHashMap for
	 * unload (underlying assumption, first in, first out)
	 */
	private final Set chunksToUnload = new HashSet();
	private final Chunk defaultEmptyChunk;
	private final IChunkProvider currentChunkProvider;
	private final IChunkLoader currentChunkLoader;

	/**
	 * if this is false, the defaultEmptyChunk will be returned by the provider
	 */
	public boolean loadChunkOnProvideRequest = true;
	private final LongHashMap loadedChunkHashMap = new LongHashMap();
	private final List loadedChunks = new ArrayList();
	private final WorldServer worldObj;

	public ChunkProviderServer(final WorldServer par1WorldServer,
			final IChunkLoader par2IChunkLoader,
			final IChunkProvider par3IChunkProvider) {
		defaultEmptyChunk = new EmptyChunk(par1WorldServer, 0, 0);
		worldObj = par1WorldServer;
		currentChunkLoader = par2IChunkLoader;
		currentChunkProvider = par3IChunkProvider;
	}

	/**
	 * Checks to see if a chunk exists at x, y
	 */
	@Override
	public boolean chunkExists(final int par1, final int par2) {
		return loadedChunkHashMap.containsItem(ChunkCoordIntPair.chunkXZ2Int(
				par1, par2));
	}

	/**
	 * marks chunk for unload by "unload100OldestChunks" if there is no spawn
	 * point, or if the center of the chunk is outside 200 blocks (x or z) of
	 * the spawn
	 */
	public void unloadChunksIfNotNearSpawn(final int par1, final int par2) {
		if (worldObj.provider.canRespawnHere()) {
			final ChunkCoordinates var3 = worldObj.getSpawnPoint();
			final int var4 = par1 * 16 + 8 - var3.posX;
			final int var5 = par2 * 16 + 8 - var3.posZ;
			final short var6 = 128;

			if (var4 < -var6 || var4 > var6 || var5 < -var6 || var5 > var6) {
				chunksToUnload.add(Long.valueOf(ChunkCoordIntPair.chunkXZ2Int(
						par1, par2)));
			}
		} else {
			chunksToUnload.add(Long.valueOf(ChunkCoordIntPair.chunkXZ2Int(par1,
					par2)));
		}
	}

	/**
	 * marks all chunks for unload, ignoring those near the spawn
	 */
	public void unloadAllChunks() {
		final Iterator var1 = loadedChunks.iterator();

		while (var1.hasNext()) {
			final Chunk var2 = (Chunk) var1.next();
			unloadChunksIfNotNearSpawn(var2.xPosition, var2.zPosition);
		}
	}

	/**
	 * loads or generates the chunk at the chunk location specified
	 */
	@Override
	public Chunk loadChunk(final int par1, final int par2) {
		final long var3 = ChunkCoordIntPair.chunkXZ2Int(par1, par2);
		chunksToUnload.remove(Long.valueOf(var3));
		Chunk var5 = (Chunk) loadedChunkHashMap.getValueByKey(var3);

		if (var5 == null) {
			var5 = safeLoadChunk(par1, par2);

			if (var5 == null) {
				if (currentChunkProvider == null) {
					var5 = defaultEmptyChunk;
				} else {
					try {
						var5 = currentChunkProvider.provideChunk(par1, par2);
					} catch (final Throwable var9) {
						final CrashReport var7 = CrashReport.makeCrashReport(
								var9, "Exception generating new chunk");
						final CrashReportCategory var8 = var7
								.makeCategory("Chunk to be generated");
						var8.addCrashSection(
								"Location",
								String.format("%d,%d",
										new Object[] { Integer.valueOf(par1),
												Integer.valueOf(par2) }));
						var8.addCrashSection("Position hash",
								Long.valueOf(var3));
						var8.addCrashSection("Generator",
								currentChunkProvider.makeString());
						throw new ReportedException(var7);
					}
				}
			}

			loadedChunkHashMap.add(var3, var5);
			loadedChunks.add(var5);

			if (var5 != null) {
				var5.onChunkLoad();
			}

			var5.populateChunk(this, this, par1, par2);
		}

		return var5;
	}

	/**
	 * Will return back a chunk, if it doesn't exist and its not a MP client it
	 * will generates all the blocks for the specified chunk from the map seed
	 * and chunk seed
	 */
	@Override
	public Chunk provideChunk(final int par1, final int par2) {
		final Chunk var3 = (Chunk) loadedChunkHashMap
				.getValueByKey(ChunkCoordIntPair.chunkXZ2Int(par1, par2));
		return var3 == null ? !worldObj.findingSpawnPoint
				&& !loadChunkOnProvideRequest ? defaultEmptyChunk : loadChunk(
				par1, par2) : var3;
	}

	/**
	 * used by loadChunk, but catches any exceptions if the load fails.
	 */
	private Chunk safeLoadChunk(final int par1, final int par2) {
		if (currentChunkLoader == null) {
			return null;
		} else {
			try {
				final Chunk var3 = currentChunkLoader.loadChunk(worldObj, par1,
						par2);

				if (var3 != null) {
					var3.lastSaveTime = worldObj.getTotalWorldTime();

					if (currentChunkProvider != null) {
						currentChunkProvider.recreateStructures(par1, par2);
					}
				}

				return var3;
			} catch (final Exception var4) {
				var4.printStackTrace();
				return null;
			}
		}
	}

	/**
	 * used by saveChunks, but catches any exceptions if the save fails.
	 */
	private void safeSaveExtraChunkData(final Chunk par1Chunk) {
		if (currentChunkLoader != null) {
			try {
				currentChunkLoader.saveExtraChunkData(worldObj, par1Chunk);
			} catch (final Exception var3) {
				var3.printStackTrace();
			}
		}
	}

	/**
	 * used by saveChunks, but catches any exceptions if the save fails.
	 */
	private void safeSaveChunk(final Chunk par1Chunk) {
		if (currentChunkLoader != null) {
			try {
				par1Chunk.lastSaveTime = worldObj.getTotalWorldTime();
				currentChunkLoader.saveChunk(worldObj, par1Chunk);
			} catch (final IOException var3) {
				var3.printStackTrace();
			} catch (final MinecraftException var4) {
				var4.printStackTrace();
			}
		}
	}

	/**
	 * Populates chunk with ores etc etc
	 */
	@Override
	public void populate(final IChunkProvider par1IChunkProvider,
			final int par2, final int par3) {
		final Chunk var4 = provideChunk(par2, par3);

		if (!var4.isTerrainPopulated) {
			var4.isTerrainPopulated = true;

			if (currentChunkProvider != null) {
				currentChunkProvider.populate(par1IChunkProvider, par2, par3);
				var4.setChunkModified();
			}
		}
	}

	/**
	 * Two modes of operation: if passed true, save all Chunks in one go. If
	 * passed false, save up to two chunks. Return true if all chunks have been
	 * saved.
	 */
	@Override
	public boolean saveChunks(final boolean par1,
			final IProgressUpdate par2IProgressUpdate) {
		int var3 = 0;

		for (int var4 = 0; var4 < loadedChunks.size(); ++var4) {
			final Chunk var5 = (Chunk) loadedChunks.get(var4);

			if (par1) {
				safeSaveExtraChunkData(var5);
			}

			if (var5.needsSaving(par1)) {
				safeSaveChunk(var5);
				var5.isModified = false;
				++var3;

				if (var3 == 24 && !par1) {
					return false;
				}
			}
		}

		return true;
	}

	@Override
	public void func_104112_b() {
		if (currentChunkLoader != null) {
			currentChunkLoader.saveExtraData();
		}
	}

	/**
	 * Unloads chunks that are marked to be unloaded. This is not guaranteed to
	 * unload every such chunk.
	 */
	@Override
	public boolean unloadQueuedChunks() {
		if (!worldObj.canNotSave) {
			for (int var1 = 0; var1 < 100; ++var1) {
				if (!chunksToUnload.isEmpty()) {
					final Long var2 = (Long) chunksToUnload.iterator().next();
					final Chunk var3 = (Chunk) loadedChunkHashMap
							.getValueByKey(var2.longValue());
					var3.onChunkUnload();
					safeSaveChunk(var3);
					safeSaveExtraChunkData(var3);
					chunksToUnload.remove(var2);
					loadedChunkHashMap.remove(var2.longValue());
					loadedChunks.remove(var3);
				}
			}

			if (currentChunkLoader != null) {
				currentChunkLoader.chunkTick();
			}
		}

		return currentChunkProvider.unloadQueuedChunks();
	}

	/**
	 * Returns if the IChunkProvider supports saving.
	 */
	@Override
	public boolean canSave() {
		return !worldObj.canNotSave;
	}

	/**
	 * Converts the instance data to a readable string.
	 */
	@Override
	public String makeString() {
		return "ServerChunkCache: " + loadedChunkHashMap.getNumHashElements()
				+ " Drop: " + chunksToUnload.size();
	}

	/**
	 * Returns a list of creatures of the specified type that can spawn at the
	 * given location.
	 */
	@Override
	public List getPossibleCreatures(
			final EnumCreatureType par1EnumCreatureType, final int par2,
			final int par3, final int par4) {
		return currentChunkProvider.getPossibleCreatures(par1EnumCreatureType,
				par2, par3, par4);
	}

	/**
	 * Returns the location of the closest structure of the specified type. If
	 * not found returns null.
	 */
	@Override
	public ChunkPosition findClosestStructure(final World par1World,
			final String par2Str, final int par3, final int par4, final int par5) {
		return currentChunkProvider.findClosestStructure(par1World, par2Str,
				par3, par4, par5);
	}

	@Override
	public int getLoadedChunkCount() {
		return loadedChunkHashMap.getNumHashElements();
	}

	@Override
	public void recreateStructures(final int par1, final int par2) {
	}
}
