package net.minecraft.src;

class GuiFlatPresetsItem {
	/** ID for the item used as icon for this preset. */
	public int iconId;

	/** Name for this preset. */
	public String presetName;

	/** Data for this preset. */
	public String presetData;

	public GuiFlatPresetsItem(final int par1, final String par2Str,
			final String par3Str) {
		iconId = par1;
		presetName = par2Str;
		presetData = par3Str;
	}
}
