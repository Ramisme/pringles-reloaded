package net.minecraft.src;

import java.util.Random;

public class BlockReed extends Block {
	protected BlockReed(final int par1) {
		super(par1, Material.plants);
		final float var2 = 0.375F;
		setBlockBounds(0.5F - var2, 0.0F, 0.5F - var2, 0.5F + var2, 1.0F,
				0.5F + var2);
		setTickRandomly(true);
	}

	/**
	 * Ticks the block if it's been scheduled
	 */
	@Override
	public void updateTick(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		if (par1World.isAirBlock(par2, par3 + 1, par4)) {
			int var6;

			for (var6 = 1; par1World.getBlockId(par2, par3 - var6, par4) == blockID; ++var6) {
				;
			}

			if (var6 < 3) {
				final int var7 = par1World.getBlockMetadata(par2, par3, par4);

				if (var7 == 15) {
					par1World.setBlock(par2, par3 + 1, par4, blockID);
					par1World
							.setBlockMetadataWithNotify(par2, par3, par4, 0, 4);
				} else {
					par1World.setBlockMetadataWithNotify(par2, par3, par4,
							var7 + 1, 4);
				}
			}
		}
	}

	/**
	 * Checks to see if its valid to put this block at the specified
	 * coordinates. Args: world, x, y, z
	 */
	@Override
	public boolean canPlaceBlockAt(final World par1World, final int par2,
			final int par3, final int par4) {
		final int var5 = par1World.getBlockId(par2, par3 - 1, par4);
		return var5 == blockID ? true
				: var5 != Block.grass.blockID && var5 != Block.dirt.blockID
						&& var5 != Block.sand.blockID ? false
						: par1World.getBlockMaterial(par2 - 1, par3 - 1, par4) == Material.water ? true
								: par1World.getBlockMaterial(par2 + 1,
										par3 - 1, par4) == Material.water ? true
										: par1World.getBlockMaterial(par2,
												par3 - 1, par4 - 1) == Material.water ? true
												: par1World.getBlockMaterial(
														par2, par3 - 1,
														par4 + 1) == Material.water;
	}

	/**
	 * Lets the block know when one of its neighbor changes. Doesn't know which
	 * neighbor changed (coordinates passed are their own) Args: x, y, z,
	 * neighbor blockID
	 */
	@Override
	public void onNeighborBlockChange(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		checkBlockCoordValid(par1World, par2, par3, par4);
	}

	/**
	 * Checks if current block pos is valid, if not, breaks the block as
	 * dropable item. Used for reed and cactus.
	 */
	protected final void checkBlockCoordValid(final World par1World,
			final int par2, final int par3, final int par4) {
		if (!canBlockStay(par1World, par2, par3, par4)) {
			dropBlockAsItem(par1World, par2, par3, par4,
					par1World.getBlockMetadata(par2, par3, par4), 0);
			par1World.setBlockToAir(par2, par3, par4);
		}
	}

	/**
	 * Can this block stay at this position. Similar to canPlaceBlockAt except
	 * gets checked often with plants.
	 */
	@Override
	public boolean canBlockStay(final World par1World, final int par2,
			final int par3, final int par4) {
		return canPlaceBlockAt(par1World, par2, par3, par4);
	}

	/**
	 * Returns a bounding box from the pool of bounding boxes (this means this
	 * box can change after the pool has been cleared to be reused)
	 */
	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(final World par1World,
			final int par2, final int par3, final int par4) {
		return null;
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return Item.reed.itemID;
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return 1;
	}

	/**
	 * only called by clickMiddleMouseButton , and passed to
	 * inventory.setCurrentItem (along with isCreative)
	 */
	@Override
	public int idPicked(final World par1World, final int par2, final int par3,
			final int par4) {
		return Item.reed.itemID;
	}
}
