package net.minecraft.src;

public class MinecraftException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6933903242362850591L;

	public MinecraftException(final String par1Str) {
		super(par1Str);
	}
}
