package net.minecraft.src;

import java.util.List;

import net.minecraft.server.MinecraftServer;

public class CommandServerBanlist extends CommandBase {
	@Override
	public String getCommandName() {
		return "banlist";
	}

	/**
	 * Return the required permission level for this command.
	 */
	@Override
	public int getRequiredPermissionLevel() {
		return 3;
	}

	/**
	 * Returns true if the given command sender is allowed to use this command.
	 */
	@Override
	public boolean canCommandSenderUseCommand(
			final ICommandSender par1ICommandSender) {
		return (MinecraftServer.getServer().getConfigurationManager()
				.getBannedIPs().isListActive() || MinecraftServer.getServer()
				.getConfigurationManager().getBannedPlayers().isListActive())
				&& super.canCommandSenderUseCommand(par1ICommandSender);
	}

	@Override
	public String getCommandUsage(final ICommandSender par1ICommandSender) {
		return par1ICommandSender.translateString("commands.banlist.usage",
				new Object[0]);
	}

	@Override
	public void processCommand(final ICommandSender par1ICommandSender,
			final String[] par2ArrayOfStr) {
		if (par2ArrayOfStr.length >= 1
				&& par2ArrayOfStr[0].equalsIgnoreCase("ips")) {
			par1ICommandSender.sendChatToPlayer(par1ICommandSender
					.translateString(
							"commands.banlist.ips",
							new Object[] { Integer.valueOf(MinecraftServer
									.getServer().getConfigurationManager()
									.getBannedIPs().getBannedList().size()) }));
			par1ICommandSender.sendChatToPlayer(CommandBase
					.joinNiceString(MinecraftServer.getServer()
							.getConfigurationManager().getBannedIPs()
							.getBannedList().keySet().toArray()));
		} else {
			par1ICommandSender
					.sendChatToPlayer(par1ICommandSender.translateString(
							"commands.banlist.players",
							new Object[] { Integer.valueOf(MinecraftServer
									.getServer().getConfigurationManager()
									.getBannedPlayers().getBannedList().size()) }));
			par1ICommandSender.sendChatToPlayer(CommandBase
					.joinNiceString(MinecraftServer.getServer()
							.getConfigurationManager().getBannedPlayers()
							.getBannedList().keySet().toArray()));
		}
	}

	/**
	 * Adds the strings available in this command to the given list of tab
	 * completion options.
	 */
	@Override
	public List addTabCompletionOptions(
			final ICommandSender par1ICommandSender,
			final String[] par2ArrayOfStr) {
		return par2ArrayOfStr.length == 1 ? CommandBase
				.getListOfStringsMatchingLastWord(par2ArrayOfStr, new String[] {
						"players", "ips" }) : null;
	}
}
