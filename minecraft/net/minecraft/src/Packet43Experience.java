package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet43Experience extends Packet {
	/** The current experience bar points. */
	public float experience;

	/** The total experience points. */
	public int experienceTotal;

	/** The experience level. */
	public int experienceLevel;

	public Packet43Experience() {
	}

	public Packet43Experience(final float par1, final int par2, final int par3) {
		experience = par1;
		experienceTotal = par2;
		experienceLevel = par3;
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		experience = par1DataInputStream.readFloat();
		experienceLevel = par1DataInputStream.readShort();
		experienceTotal = par1DataInputStream.readShort();
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		par1DataOutputStream.writeFloat(experience);
		par1DataOutputStream.writeShort(experienceLevel);
		par1DataOutputStream.writeShort(experienceTotal);
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleExperience(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 4;
	}

	/**
	 * only false for the abstract Packet class, all real packets return true
	 */
	@Override
	public boolean isRealPacket() {
		return true;
	}

	/**
	 * eg return packet30entity.entityId == entityId; WARNING : will throw if
	 * you compare a packet to a different packet class
	 */
	@Override
	public boolean containsSameEntityIDAs(final Packet par1Packet) {
		return true;
	}
}
