package net.minecraft.src;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class BlockRedstoneTorch extends BlockTorch {
	/** Whether the redstone torch is currently active or not. */
	private boolean torchActive = false;

	/** Map of ArrayLists of RedstoneUpdateInfo. Key of map is World. */
	private static Map redstoneUpdateInfoCache = new HashMap();

	private boolean checkForBurnout(final World par1World, final int par2,
			final int par3, final int par4, final boolean par5) {
		if (!BlockRedstoneTorch.redstoneUpdateInfoCache.containsKey(par1World)) {
			BlockRedstoneTorch.redstoneUpdateInfoCache.put(par1World,
					new ArrayList());
		}

		final List var6 = (List) BlockRedstoneTorch.redstoneUpdateInfoCache
				.get(par1World);

		if (par5) {
			var6.add(new RedstoneUpdateInfo(par2, par3, par4, par1World
					.getTotalWorldTime()));
		}

		int var7 = 0;

		for (int var8 = 0; var8 < var6.size(); ++var8) {
			final RedstoneUpdateInfo var9 = (RedstoneUpdateInfo) var6.get(var8);

			if (var9.x == par2 && var9.y == par3 && var9.z == par4) {
				++var7;

				if (var7 >= 8) {
					return true;
				}
			}
		}

		return false;
	}

	protected BlockRedstoneTorch(final int par1, final boolean par2) {
		super(par1);
		torchActive = par2;
		setTickRandomly(true);
		setCreativeTab((CreativeTabs) null);
	}

	/**
	 * How many world ticks before ticking
	 */
	@Override
	public int tickRate(final World par1World) {
		return 2;
	}

	/**
	 * Called whenever the block is added into the world. Args: world, x, y, z
	 */
	@Override
	public void onBlockAdded(final World par1World, final int par2,
			final int par3, final int par4) {
		if (par1World.getBlockMetadata(par2, par3, par4) == 0) {
			super.onBlockAdded(par1World, par2, par3, par4);
		}

		if (torchActive) {
			par1World.notifyBlocksOfNeighborChange(par2, par3 - 1, par4,
					blockID);
			par1World.notifyBlocksOfNeighborChange(par2, par3 + 1, par4,
					blockID);
			par1World.notifyBlocksOfNeighborChange(par2 - 1, par3, par4,
					blockID);
			par1World.notifyBlocksOfNeighborChange(par2 + 1, par3, par4,
					blockID);
			par1World.notifyBlocksOfNeighborChange(par2, par3, par4 - 1,
					blockID);
			par1World.notifyBlocksOfNeighborChange(par2, par3, par4 + 1,
					blockID);
		}
	}

	/**
	 * ejects contained items into the world, and notifies neighbours of an
	 * update, as appropriate
	 */
	@Override
	public void breakBlock(final World par1World, final int par2,
			final int par3, final int par4, final int par5, final int par6) {
		if (torchActive) {
			par1World.notifyBlocksOfNeighborChange(par2, par3 - 1, par4,
					blockID);
			par1World.notifyBlocksOfNeighborChange(par2, par3 + 1, par4,
					blockID);
			par1World.notifyBlocksOfNeighborChange(par2 - 1, par3, par4,
					blockID);
			par1World.notifyBlocksOfNeighborChange(par2 + 1, par3, par4,
					blockID);
			par1World.notifyBlocksOfNeighborChange(par2, par3, par4 - 1,
					blockID);
			par1World.notifyBlocksOfNeighborChange(par2, par3, par4 + 1,
					blockID);
		}
	}

	/**
	 * Returns true if the block is emitting indirect/weak redstone power on the
	 * specified side. If isBlockNormalCube returns true, standard redstone
	 * propagation rules will apply instead and this will not be called. Args:
	 * World, X, Y, Z, side. Note that the side is reversed - eg it is 1 (up)
	 * when checking the bottom of the block.
	 */
	@Override
	public int isProvidingWeakPower(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		if (!torchActive) {
			return 0;
		} else {
			final int var6 = par1IBlockAccess
					.getBlockMetadata(par2, par3, par4);
			return var6 == 5 && par5 == 1 ? 0 : var6 == 3 && par5 == 3 ? 0
					: var6 == 4 && par5 == 2 ? 0 : var6 == 1 && par5 == 5 ? 0
							: var6 == 2 && par5 == 4 ? 0 : 15;
		}
	}

	/**
	 * Returns true or false based on whether the block the torch is attached to
	 * is providing indirect power.
	 */
	private boolean isIndirectlyPowered(final World par1World, final int par2,
			final int par3, final int par4) {
		final int var5 = par1World.getBlockMetadata(par2, par3, par4);
		return var5 == 5
				&& par1World.getIndirectPowerOutput(par2, par3 - 1, par4, 0) ? true
				: var5 == 3
						&& par1World.getIndirectPowerOutput(par2, par3,
								par4 - 1, 2) ? true : var5 == 4
						&& par1World.getIndirectPowerOutput(par2, par3,
								par4 + 1, 3) ? true : var5 == 1
						&& par1World.getIndirectPowerOutput(par2 - 1, par3,
								par4, 4) ? true : var5 == 2
						&& par1World.getIndirectPowerOutput(par2 + 1, par3,
								par4, 5);
	}

	/**
	 * Ticks the block if it's been scheduled
	 */
	@Override
	public void updateTick(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		final boolean var6 = isIndirectlyPowered(par1World, par2, par3, par4);
		final List var7 = (List) BlockRedstoneTorch.redstoneUpdateInfoCache
				.get(par1World);

		while (var7 != null
				&& !var7.isEmpty()
				&& par1World.getTotalWorldTime()
						- ((RedstoneUpdateInfo) var7.get(0)).updateTime > 60L) {
			var7.remove(0);
		}

		if (torchActive) {
			if (var6) {
				par1World.setBlock(par2, par3, par4,
						Block.torchRedstoneIdle.blockID,
						par1World.getBlockMetadata(par2, par3, par4), 3);

				if (checkForBurnout(par1World, par2, par3, par4, true)) {
					par1World.playSoundEffect(par2 + 0.5F, par3 + 0.5F,
							par4 + 0.5F, "random.fizz", 0.5F,
							2.6F + (par1World.rand.nextFloat() - par1World.rand
									.nextFloat()) * 0.8F);

					for (int var8 = 0; var8 < 5; ++var8) {
						final double var9 = par2 + par5Random.nextDouble()
								* 0.6D + 0.2D;
						final double var11 = par3 + par5Random.nextDouble()
								* 0.6D + 0.2D;
						final double var13 = par4 + par5Random.nextDouble()
								* 0.6D + 0.2D;
						par1World.spawnParticle("smoke", var9, var11, var13,
								0.0D, 0.0D, 0.0D);
					}
				}
			}
		} else if (!var6
				&& !checkForBurnout(par1World, par2, par3, par4, false)) {
			par1World.setBlock(par2, par3, par4,
					Block.torchRedstoneActive.blockID,
					par1World.getBlockMetadata(par2, par3, par4), 3);
		}
	}

	/**
	 * Lets the block know when one of its neighbor changes. Doesn't know which
	 * neighbor changed (coordinates passed are their own) Args: x, y, z,
	 * neighbor blockID
	 */
	@Override
	public void onNeighborBlockChange(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		if (!func_94397_d(par1World, par2, par3, par4, par5)) {
			final boolean var6 = isIndirectlyPowered(par1World, par2, par3,
					par4);

			if (torchActive && var6 || !torchActive && !var6) {
				par1World.scheduleBlockUpdate(par2, par3, par4, blockID,
						tickRate(par1World));
			}
		}
	}

	/**
	 * Returns true if the block is emitting direct/strong redstone power on the
	 * specified side. Args: World, X, Y, Z, side. Note that the side is
	 * reversed - eg it is 1 (up) when checking the bottom of the block.
	 */
	@Override
	public int isProvidingStrongPower(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		return par5 == 0 ? isProvidingWeakPower(par1IBlockAccess, par2, par3,
				par4, par5) : 0;
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return Block.torchRedstoneActive.blockID;
	}

	/**
	 * Can this block provide power. Only wire currently seems to have this
	 * change based on its state.
	 */
	@Override
	public boolean canProvidePower() {
		return true;
	}

	/**
	 * A randomly called display update to be able to add particles or other
	 * items for display
	 */
	@Override
	public void randomDisplayTick(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		if (torchActive) {
			final int var6 = par1World.getBlockMetadata(par2, par3, par4);
			final double var7 = par2 + 0.5F + (par5Random.nextFloat() - 0.5F)
					* 0.2D;
			final double var9 = par3 + 0.7F + (par5Random.nextFloat() - 0.5F)
					* 0.2D;
			final double var11 = par4 + 0.5F + (par5Random.nextFloat() - 0.5F)
					* 0.2D;
			final double var13 = 0.2199999988079071D;
			final double var15 = 0.27000001072883606D;

			if (var6 == 1) {
				par1World.spawnParticle("reddust", var7 - var15, var9 + var13,
						var11, 0.0D, 0.0D, 0.0D);
			} else if (var6 == 2) {
				par1World.spawnParticle("reddust", var7 + var15, var9 + var13,
						var11, 0.0D, 0.0D, 0.0D);
			} else if (var6 == 3) {
				par1World.spawnParticle("reddust", var7, var9 + var13, var11
						- var15, 0.0D, 0.0D, 0.0D);
			} else if (var6 == 4) {
				par1World.spawnParticle("reddust", var7, var9 + var13, var11
						+ var15, 0.0D, 0.0D, 0.0D);
			} else {
				par1World.spawnParticle("reddust", var7, var9, var11, 0.0D,
						0.0D, 0.0D);
			}
		}
	}

	/**
	 * only called by clickMiddleMouseButton , and passed to
	 * inventory.setCurrentItem (along with isCreative)
	 */
	@Override
	public int idPicked(final World par1World, final int par2, final int par3,
			final int par4) {
		return Block.torchRedstoneActive.blockID;
	}

	/**
	 * Returns true if the given block ID is equivalent to this one. Example:
	 * redstoneTorchOn matches itself and redstoneTorchOff, and vice versa. Most
	 * blocks only match themselves.
	 */
	@Override
	public boolean isAssociatedBlockID(final int par1) {
		return par1 == Block.torchRedstoneIdle.blockID
				|| par1 == Block.torchRedstoneActive.blockID;
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		if (torchActive) {
			blockIcon = par1IconRegister.registerIcon("redtorch_lit");
		} else {
			blockIcon = par1IconRegister.registerIcon("redtorch");
		}
	}
}
