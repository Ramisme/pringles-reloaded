package net.minecraft.src;

public class Tuple {
	/** First Object in the Tuple */
	private final Object first;

	/** Second Object in the Tuple */
	private final Object second;

	public Tuple(final Object par1Obj, final Object par2Obj) {
		first = par1Obj;
		second = par2Obj;
	}

	/**
	 * Get the first Object in the Tuple
	 */
	public Object getFirst() {
		return first;
	}

	/**
	 * Get the second Object in the Tuple
	 */
	public Object getSecond() {
		return second;
	}
}
