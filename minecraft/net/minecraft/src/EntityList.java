package net.minecraft.src;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class EntityList {
	/** Provides a mapping between entity classes and a string */
	private static Map stringToClassMapping = new HashMap();

	/** Provides a mapping between a string and an entity classes */
	private static Map classToStringMapping = new HashMap();

	/** provides a mapping between an entityID and an Entity Class */
	private static Map IDtoClassMapping = new HashMap();

	/** provides a mapping between an Entity Class and an entity ID */
	private static Map classToIDMapping = new HashMap();

	/** Maps entity names to their numeric identifiers */
	private static Map stringToIDMapping = new HashMap();

	/** This is a HashMap of the Creative Entity Eggs/Spawners. */
	public static HashMap entityEggs = new LinkedHashMap();

	/**
	 * adds a mapping between Entity classes and both a string representation
	 * and an ID
	 */
	private static void addMapping(final Class par0Class, final String par1Str,
			final int par2) {
		EntityList.stringToClassMapping.put(par1Str, par0Class);
		EntityList.classToStringMapping.put(par0Class, par1Str);
		EntityList.IDtoClassMapping.put(Integer.valueOf(par2), par0Class);
		EntityList.classToIDMapping.put(par0Class, Integer.valueOf(par2));
		EntityList.stringToIDMapping.put(par1Str, Integer.valueOf(par2));
	}

	/**
	 * Adds a entity mapping with egg info.
	 */
	private static void addMapping(final Class par0Class, final String par1Str,
			final int par2, final int par3, final int par4) {
		EntityList.addMapping(par0Class, par1Str, par2);
		EntityList.entityEggs.put(Integer.valueOf(par2), new EntityEggInfo(
				par2, par3, par4));
	}

	/**
	 * Create a new instance of an entity in the world by using the entity name.
	 */
	public static Entity createEntityByName(final String par0Str,
			final World par1World) {
		Entity var2 = null;

		try {
			final Class var3 = (Class) EntityList.stringToClassMapping
					.get(par0Str);

			if (var3 != null) {
				var2 = (Entity) var3
						.getConstructor(new Class[] { World.class })
						.newInstance(new Object[] { par1World });
			}
		} catch (final Exception var4) {
			var4.printStackTrace();
		}

		return var2;
	}

	/**
	 * create a new instance of an entity from NBT store
	 */
	public static Entity createEntityFromNBT(
			final NBTTagCompound par0NBTTagCompound, final World par1World) {
		Entity var2 = null;

		if ("Minecart".equals(par0NBTTagCompound.getString("id"))) {
			switch (par0NBTTagCompound.getInteger("Type")) {
			case 0:
				par0NBTTagCompound.setString("id", "MinecartRideable");
				break;

			case 1:
				par0NBTTagCompound.setString("id", "MinecartChest");
				break;

			case 2:
				par0NBTTagCompound.setString("id", "MinecartFurnace");
			}

			par0NBTTagCompound.removeTag("Type");
		}

		try {
			final Class var3 = (Class) EntityList.stringToClassMapping
					.get(par0NBTTagCompound.getString("id"));

			if (var3 != null) {
				var2 = (Entity) var3
						.getConstructor(new Class[] { World.class })
						.newInstance(new Object[] { par1World });
			}
		} catch (final Exception var4) {
			var4.printStackTrace();
		}

		if (var2 != null) {
			var2.readFromNBT(par0NBTTagCompound);
		} else {
			par1World.getWorldLogAgent().logWarning(
					"Skipping Entity with id "
							+ par0NBTTagCompound.getString("id"));
		}

		return var2;
	}

	/**
	 * Create a new instance of an entity in the world by using an entity ID.
	 */
	public static Entity createEntityByID(final int par0, final World par1World) {
		Entity var2 = null;

		try {
			final Class var3 = EntityList.getClassFromID(par0);

			if (var3 != null) {
				var2 = (Entity) var3
						.getConstructor(new Class[] { World.class })
						.newInstance(new Object[] { par1World });
			}
		} catch (final Exception var4) {
			var4.printStackTrace();
		}

		if (var2 == null) {
			par1World.getWorldLogAgent().logWarning(
					"Skipping Entity with id " + par0);
		}

		return var2;
	}

	/**
	 * gets the entityID of a specific entity
	 */
	public static int getEntityID(final Entity par0Entity) {
		final Class var1 = par0Entity.getClass();
		return EntityList.classToIDMapping.containsKey(var1) ? ((Integer) EntityList.classToIDMapping
				.get(var1)).intValue() : 0;
	}

	/**
	 * Return the class assigned to this entity ID.
	 */
	public static Class getClassFromID(final int par0) {
		return (Class) EntityList.IDtoClassMapping.get(Integer.valueOf(par0));
	}

	/**
	 * Gets the string representation of a specific entity.
	 */
	public static String getEntityString(final Entity par0Entity) {
		return (String) EntityList.classToStringMapping.get(par0Entity
				.getClass());
	}

	/**
	 * Finds the class using IDtoClassMapping and classToStringMapping
	 */
	public static String getStringFromID(final int par0) {
		final Class var1 = EntityList.getClassFromID(par0);
		return var1 != null ? (String) EntityList.classToStringMapping
				.get(var1) : null;
	}

	static {
		EntityList.addMapping(EntityItem.class, "Item", 1);
		EntityList.addMapping(EntityXPOrb.class, "XPOrb", 2);
		EntityList.addMapping(EntityPainting.class, "Painting", 9);
		EntityList.addMapping(EntityArrow.class, "Arrow", 10);
		EntityList.addMapping(EntitySnowball.class, "Snowball", 11);
		EntityList.addMapping(EntityLargeFireball.class, "Fireball", 12);
		EntityList.addMapping(EntitySmallFireball.class, "SmallFireball", 13);
		EntityList.addMapping(EntityEnderPearl.class, "ThrownEnderpearl", 14);
		EntityList.addMapping(EntityEnderEye.class, "EyeOfEnderSignal", 15);
		EntityList.addMapping(EntityPotion.class, "ThrownPotion", 16);
		EntityList.addMapping(EntityExpBottle.class, "ThrownExpBottle", 17);
		EntityList.addMapping(EntityItemFrame.class, "ItemFrame", 18);
		EntityList.addMapping(EntityWitherSkull.class, "WitherSkull", 19);
		EntityList.addMapping(EntityTNTPrimed.class, "PrimedTnt", 20);
		EntityList.addMapping(EntityFallingSand.class, "FallingSand", 21);
		EntityList.addMapping(EntityFireworkRocket.class,
				"FireworksRocketEntity", 22);
		EntityList.addMapping(EntityBoat.class, "Boat", 41);
		EntityList
				.addMapping(EntityMinecartEmpty.class, "MinecartRideable", 42);
		EntityList.addMapping(EntityMinecartChest.class, "MinecartChest", 43);
		EntityList.addMapping(EntityMinecartFurnace.class, "MinecartFurnace",
				44);
		EntityList.addMapping(EntityMinecartTNT.class, "MinecartTNT", 45);
		EntityList.addMapping(EntityMinecartHopper.class, "MinecartHopper", 46);
		EntityList.addMapping(EntityMinecartMobSpawner.class,
				"MinecartSpawner", 47);
		EntityList.addMapping(EntityLiving.class, "Mob", 48);
		EntityList.addMapping(EntityMob.class, "Monster", 49);
		EntityList.addMapping(EntityCreeper.class, "Creeper", 50, 894731, 0);
		EntityList.addMapping(EntitySkeleton.class, "Skeleton", 51, 12698049,
				4802889);
		EntityList.addMapping(EntitySpider.class, "Spider", 52, 3419431,
				11013646);
		EntityList.addMapping(EntityGiantZombie.class, "Giant", 53);
		EntityList.addMapping(EntityZombie.class, "Zombie", 54, 44975, 7969893);
		EntityList.addMapping(EntitySlime.class, "Slime", 55, 5349438, 8306542);
		EntityList.addMapping(EntityGhast.class, "Ghast", 56, 16382457,
				12369084);
		EntityList.addMapping(EntityPigZombie.class, "PigZombie", 57, 15373203,
				5009705);
		EntityList.addMapping(EntityEnderman.class, "Enderman", 58, 1447446, 0);
		EntityList.addMapping(EntityCaveSpider.class, "CaveSpider", 59, 803406,
				11013646);
		EntityList.addMapping(EntitySilverfish.class, "Silverfish", 60,
				7237230, 3158064);
		EntityList.addMapping(EntityBlaze.class, "Blaze", 61, 16167425,
				16775294);
		EntityList.addMapping(EntityMagmaCube.class, "LavaSlime", 62, 3407872,
				16579584);
		EntityList.addMapping(EntityDragon.class, "EnderDragon", 63);
		EntityList.addMapping(EntityWither.class, "WitherBoss", 64);
		EntityList.addMapping(EntityBat.class, "Bat", 65, 4996656, 986895);
		EntityList.addMapping(EntityWitch.class, "Witch", 66, 3407872, 5349438);
		EntityList.addMapping(EntityPig.class, "Pig", 90, 15771042, 14377823);
		EntityList.addMapping(EntitySheep.class, "Sheep", 91, 15198183,
				16758197);
		EntityList.addMapping(EntityCow.class, "Cow", 92, 4470310, 10592673);
		EntityList.addMapping(EntityChicken.class, "Chicken", 93, 10592673,
				16711680);
		EntityList.addMapping(EntitySquid.class, "Squid", 94, 2243405, 7375001);
		EntityList.addMapping(EntityWolf.class, "Wolf", 95, 14144467, 13545366);
		EntityList.addMapping(EntityMooshroom.class, "MushroomCow", 96,
				10489616, 12040119);
		EntityList.addMapping(EntitySnowman.class, "SnowMan", 97);
		EntityList.addMapping(EntityOcelot.class, "Ozelot", 98, 15720061,
				5653556);
		EntityList.addMapping(EntityIronGolem.class, "VillagerGolem", 99);
		EntityList.addMapping(EntityVillager.class, "Villager", 120, 5651507,
				12422002);
		EntityList.addMapping(EntityEnderCrystal.class, "EnderCrystal", 200);
	}
}
