package net.minecraft.src;

public class NumberInvalidException extends CommandException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6977912661045546923L;

	public NumberInvalidException() {
		this("commands.generic.num.invalid", new Object[0]);
	}

	public NumberInvalidException(final String par1Str,
			final Object... par2ArrayOfObj) {
		super(par1Str, par2ArrayOfObj);
	}
}
