package net.minecraft.src;

import java.util.Random;

public abstract class BlockBasePressurePlate extends Block {
	private final String pressurePlateIconName;

	protected BlockBasePressurePlate(final int par1, final String par2Str,
			final Material par3Material) {
		super(par1, par3Material);
		pressurePlateIconName = par2Str;
		setCreativeTab(CreativeTabs.tabRedstone);
		setTickRandomly(true);
		func_94353_c_(getMetaFromWeight(15));
	}

	/**
	 * Updates the blocks bounds based on its current state. Args: world, x, y,
	 * z
	 */
	@Override
	public void setBlockBoundsBasedOnState(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		func_94353_c_(par1IBlockAccess.getBlockMetadata(par2, par3, par4));
	}

	protected void func_94353_c_(final int par1) {
		final boolean var2 = getPowerSupply(par1) > 0;
		final float var3 = 0.0625F;

		if (var2) {
			setBlockBounds(var3, 0.0F, var3, 1.0F - var3, 0.03125F, 1.0F - var3);
		} else {
			setBlockBounds(var3, 0.0F, var3, 1.0F - var3, 0.0625F, 1.0F - var3);
		}
	}

	/**
	 * How many world ticks before ticking
	 */
	@Override
	public int tickRate(final World par1World) {
		return 20;
	}

	/**
	 * Returns a bounding box from the pool of bounding boxes (this means this
	 * box can change after the pool has been cleared to be reused)
	 */
	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(final World par1World,
			final int par2, final int par3, final int par4) {
		return null;
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	@Override
	public boolean getBlocksMovement(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		return true;
	}

	/**
	 * Checks to see if its valid to put this block at the specified
	 * coordinates. Args: world, x, y, z
	 */
	@Override
	public boolean canPlaceBlockAt(final World par1World, final int par2,
			final int par3, final int par4) {
		return par1World.doesBlockHaveSolidTopSurface(par2, par3 - 1, par4)
				|| BlockFence.isIdAFence(par1World.getBlockId(par2, par3 - 1,
						par4));
	}

	/**
	 * Lets the block know when one of its neighbor changes. Doesn't know which
	 * neighbor changed (coordinates passed are their own) Args: x, y, z,
	 * neighbor blockID
	 */
	@Override
	public void onNeighborBlockChange(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		boolean var6 = false;

		if (!par1World.doesBlockHaveSolidTopSurface(par2, par3 - 1, par4)
				&& !BlockFence.isIdAFence(par1World.getBlockId(par2, par3 - 1,
						par4))) {
			var6 = true;
		}

		if (var6) {
			dropBlockAsItem(par1World, par2, par3, par4,
					par1World.getBlockMetadata(par2, par3, par4), 0);
			par1World.setBlockToAir(par2, par3, par4);
		}
	}

	/**
	 * Ticks the block if it's been scheduled
	 */
	@Override
	public void updateTick(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		if (!par1World.isRemote) {
			final int var6 = getPowerSupply(par1World.getBlockMetadata(par2,
					par3, par4));

			if (var6 > 0) {
				setStateIfMobInteractsWithPlate(par1World, par2, par3, par4,
						var6);
			}
		}
	}

	/**
	 * Triggered whenever an entity collides with this block (enters into the
	 * block). Args: world, x, y, z, entity
	 */
	@Override
	public void onEntityCollidedWithBlock(final World par1World,
			final int par2, final int par3, final int par4,
			final Entity par5Entity) {
		if (!par1World.isRemote) {
			final int var6 = getPowerSupply(par1World.getBlockMetadata(par2,
					par3, par4));

			if (var6 == 0) {
				setStateIfMobInteractsWithPlate(par1World, par2, par3, par4,
						var6);
			}
		}
	}

	/**
	 * Checks if there are mobs on the plate. If a mob is on the plate and it is
	 * off, it turns it on, and vice versa.
	 */
	protected void setStateIfMobInteractsWithPlate(final World par1World,
			final int par2, final int par3, final int par4, final int par5) {
		final int var6 = getPlateState(par1World, par2, par3, par4);
		final boolean var7 = par5 > 0;
		final boolean var8 = var6 > 0;

		if (par5 != var6) {
			par1World.setBlockMetadataWithNotify(par2, par3, par4,
					getMetaFromWeight(var6), 2);
			func_94354_b_(par1World, par2, par3, par4);
			par1World.markBlockRangeForRenderUpdate(par2, par3, par4, par2,
					par3, par4);
		}

		if (!var8 && var7) {
			par1World.playSoundEffect(par2 + 0.5D, par3 + 0.1D, par4 + 0.5D,
					"random.click", 0.3F, 0.5F);
		} else if (var8 && !var7) {
			par1World.playSoundEffect(par2 + 0.5D, par3 + 0.1D, par4 + 0.5D,
					"random.click", 0.3F, 0.6F);
		}

		if (var8) {
			par1World.scheduleBlockUpdate(par2, par3, par4, blockID,
					tickRate(par1World));
		}
	}

	protected AxisAlignedBB getSensitiveAABB(final int par1, final int par2,
			final int par3) {
		final float var4 = 0.125F;
		return AxisAlignedBB.getAABBPool().getAABB(par1 + var4, par2,
				par3 + var4, par1 + 1 - var4, par2 + 0.25D, par3 + 1 - var4);
	}

	/**
	 * ejects contained items into the world, and notifies neighbours of an
	 * update, as appropriate
	 */
	@Override
	public void breakBlock(final World par1World, final int par2,
			final int par3, final int par4, final int par5, final int par6) {
		if (getPowerSupply(par6) > 0) {
			func_94354_b_(par1World, par2, par3, par4);
		}

		super.breakBlock(par1World, par2, par3, par4, par5, par6);
	}

	protected void func_94354_b_(final World par1World, final int par2,
			final int par3, final int par4) {
		par1World.notifyBlocksOfNeighborChange(par2, par3, par4, blockID);
		par1World.notifyBlocksOfNeighborChange(par2, par3 - 1, par4, blockID);
	}

	/**
	 * Returns true if the block is emitting indirect/weak redstone power on the
	 * specified side. If isBlockNormalCube returns true, standard redstone
	 * propagation rules will apply instead and this will not be called. Args:
	 * World, X, Y, Z, side. Note that the side is reversed - eg it is 1 (up)
	 * when checking the bottom of the block.
	 */
	@Override
	public int isProvidingWeakPower(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		return getPowerSupply(par1IBlockAccess.getBlockMetadata(par2, par3,
				par4));
	}

	/**
	 * Returns true if the block is emitting direct/strong redstone power on the
	 * specified side. Args: World, X, Y, Z, side. Note that the side is
	 * reversed - eg it is 1 (up) when checking the bottom of the block.
	 */
	@Override
	public int isProvidingStrongPower(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		return par5 == 1 ? getPowerSupply(par1IBlockAccess.getBlockMetadata(
				par2, par3, par4)) : 0;
	}

	/**
	 * Can this block provide power. Only wire currently seems to have this
	 * change based on its state.
	 */
	@Override
	public boolean canProvidePower() {
		return true;
	}

	/**
	 * Sets the block's bounds for rendering it as an item
	 */
	@Override
	public void setBlockBoundsForItemRender() {
		final float var1 = 0.5F;
		final float var2 = 0.125F;
		final float var3 = 0.5F;
		setBlockBounds(0.5F - var1, 0.5F - var2, 0.5F - var3, 0.5F + var1,
				0.5F + var2, 0.5F + var3);
	}

	/**
	 * Returns the mobility information of the block, 0 = free, 1 = can't push
	 * but can move over, 2 = total immobility and stop pistons
	 */
	@Override
	public int getMobilityFlag() {
		return 1;
	}

	/**
	 * Returns the current state of the pressure plate. Returns a value between
	 * 0 and 15 based on the number of items on it.
	 */
	protected abstract int getPlateState(World var1, int var2, int var3,
			int var4);

	/**
	 * Argument is metadata. Returns power level (0-15)
	 */
	protected abstract int getPowerSupply(int var1);

	/**
	 * Argument is weight (0-15). Return the metadata to be set because of it.
	 */
	protected abstract int getMetaFromWeight(int var1);

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		blockIcon = par1IconRegister.registerIcon(pressurePlateIconName);
	}
}
