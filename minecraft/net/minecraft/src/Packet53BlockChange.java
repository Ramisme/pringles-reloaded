package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet53BlockChange extends Packet {
	/** Block X position. */
	public int xPosition;

	/** Block Y position. */
	public int yPosition;

	/** Block Z position. */
	public int zPosition;

	/** The new block type for the block. */
	public int type;

	/** Metadata of the block. */
	public int metadata;

	public Packet53BlockChange() {
		isChunkDataPacket = true;
	}

	public Packet53BlockChange(final int par1, final int par2, final int par3,
			final World par4World) {
		isChunkDataPacket = true;
		xPosition = par1;
		yPosition = par2;
		zPosition = par3;
		type = par4World.getBlockId(par1, par2, par3);
		metadata = par4World.getBlockMetadata(par1, par2, par3);
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		xPosition = par1DataInputStream.readInt();
		yPosition = par1DataInputStream.read();
		zPosition = par1DataInputStream.readInt();
		type = par1DataInputStream.readShort();
		metadata = par1DataInputStream.read();
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		par1DataOutputStream.writeInt(xPosition);
		par1DataOutputStream.write(yPosition);
		par1DataOutputStream.writeInt(zPosition);
		par1DataOutputStream.writeShort(type);
		par1DataOutputStream.write(metadata);
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleBlockChange(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 11;
	}
}
