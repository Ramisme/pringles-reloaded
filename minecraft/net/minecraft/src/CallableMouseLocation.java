package net.minecraft.src;

import java.util.concurrent.Callable;

import org.lwjgl.input.Mouse;

class CallableMouseLocation implements Callable {
	final int field_90026_a;

	final int field_90024_b;

	final EntityRenderer theEntityRenderer;

	CallableMouseLocation(final EntityRenderer par1EntityRenderer,
			final int par2, final int par3) {
		theEntityRenderer = par1EntityRenderer;
		field_90026_a = par2;
		field_90024_b = par3;
	}

	public String callMouseLocation() {
		return String.format(
				"Scaled: (%d, %d). Absolute: (%d, %d)",
				new Object[] { Integer.valueOf(field_90026_a),
						Integer.valueOf(field_90024_b),
						Integer.valueOf(Mouse.getX()),
						Integer.valueOf(Mouse.getY()) });
	}

	@Override
	public Object call() {
		return callMouseLocation();
	}
}
