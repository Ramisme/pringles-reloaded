package net.minecraft.src;

import java.lang.reflect.Constructor;

public class ReflectorConstructor {
	private ReflectorClass reflectorClass = null;
	private Class[] parameterTypes = null;
	private boolean checked = false;
	private Constructor targetConstructor = null;

	public ReflectorConstructor(final ReflectorClass var1, final Class[] var2) {
		reflectorClass = var1;
		parameterTypes = var2;
		getTargetConstructor();
	}

	public Constructor getTargetConstructor() {
		if (checked) {
			return targetConstructor;
		} else {
			checked = true;
			final Class var1 = reflectorClass.getTargetClass();

			if (var1 == null) {
				return null;
			} else {
				targetConstructor = ReflectorConstructor.findConstructor(var1,
						parameterTypes);

				if (targetConstructor == null) {
					Config.dbg("(Reflector) Constructor not present: "
							+ var1.getName() + ", params: "
							+ Config.arrayToString(parameterTypes));
				}

				return targetConstructor;
			}
		}
	}

	private static Constructor findConstructor(final Class var0,
			final Class[] var1) {
		final Constructor[] var2 = var0.getConstructors();

		for (final Constructor var4 : var2) {
			final Class[] var5 = var4.getParameterTypes();

			if (Reflector.matchesTypes(var1, var5)) {
				return var4;
			}
		}

		return null;
	}
}
