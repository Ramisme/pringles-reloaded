package net.minecraft.src;

import java.util.concurrent.Callable;

import net.minecraft.client.Minecraft;

public class CallableTickingScreenName implements Callable {
	/** Reference to the Minecraft object. */
	final Minecraft mc;

	public CallableTickingScreenName(final Minecraft par1Minecraft) {
		mc = par1Minecraft;
	}

	public String getLWJGLVersion() {
		return mc.currentScreen.getClass().getCanonicalName();
	}

	@Override
	public Object call() {
		return getLWJGLVersion();
	}
}
