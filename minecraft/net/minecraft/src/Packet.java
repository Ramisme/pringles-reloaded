package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public abstract class Packet {
	/** Maps packet id to packet class */
	public static IntHashMap packetIdToClassMap = new IntHashMap();

	/** Maps packet class to packet id */
	private static Map packetClassToIdMap = new HashMap();

	/** List of the client's packet IDs. */
	private static Set clientPacketIdList = new HashSet();

	/** List of the server's packet IDs. */
	private static Set serverPacketIdList = new HashSet();
	protected ILogAgent field_98193_m;

	/** the system time in milliseconds when this packet was created. */
	public final long creationTimeMillis = System.currentTimeMillis();
	public static long receivedID;
	public static long receivedSize;

	/** Assumed to be sequential by the profiler. */
	public static long sentID;
	public static long sentSize;

	/**
	 * Only true for Packet51MapChunk, Packet52MultiBlockChange,
	 * Packet53BlockChange and Packet59ComplexEntity. Used to separate them into
	 * a different send queue.
	 */
	public boolean isChunkDataPacket = false;

	/**
	 * Adds a two way mapping between the packet ID and packet class.
	 */
	static void addIdClassMapping(final int par0, final boolean par1,
			final boolean par2, final Class par3Class) {
		if (Packet.packetIdToClassMap.containsItem(par0)) {
			throw new IllegalArgumentException("Duplicate packet id:" + par0);
		} else if (Packet.packetClassToIdMap.containsKey(par3Class)) {
			throw new IllegalArgumentException("Duplicate packet class:"
					+ par3Class);
		} else {
			Packet.packetIdToClassMap.addKey(par0, par3Class);
			Packet.packetClassToIdMap.put(par3Class, Integer.valueOf(par0));

			if (par1) {
				Packet.clientPacketIdList.add(Integer.valueOf(par0));
			}

			if (par2) {
				Packet.serverPacketIdList.add(Integer.valueOf(par0));
			}
		}
	}

	/**
	 * Returns a new instance of the specified Packet class.
	 */
	public static Packet getNewPacket(final ILogAgent par0ILogAgent,
			final int par1) {
		try {
			final Class var2 = (Class) Packet.packetIdToClassMap.lookup(par1);
			return var2 == null ? null : (Packet) var2.newInstance();
		} catch (final Exception var3) {
			var3.printStackTrace();
			par0ILogAgent.logSevere("Skipping packet with id " + par1);
			return null;
		}
	}

	/**
	 * Writes a byte array to the DataOutputStream
	 */
	public static void writeByteArray(
			final DataOutputStream par0DataOutputStream,
			final byte[] par1ArrayOfByte) throws IOException {
		par0DataOutputStream.writeShort(par1ArrayOfByte.length);
		par0DataOutputStream.write(par1ArrayOfByte);
	}

	/**
	 * the first short in the stream indicates the number of bytes to read
	 */
	public static byte[] readBytesFromStream(
			final DataInputStream par0DataInputStream) throws IOException {
		final short var1 = par0DataInputStream.readShort();

		if (var1 < 0) {
			throw new IOException("Key was smaller than nothing!  Weird key!");
		} else {
			final byte[] var2 = new byte[var1];
			par0DataInputStream.readFully(var2);
			return var2;
		}
	}

	/**
	 * Returns the ID of this packet.
	 */
	public final int getPacketId() {
		return ((Integer) Packet.packetClassToIdMap.get(this.getClass()))
				.intValue();
	}

	/**
	 * Read a packet, prefixed by its ID, from the data stream.
	 */
	public static Packet readPacket(final ILogAgent par0ILogAgent,
			final DataInputStream par1DataInputStream, final boolean par2,
			final Socket par3Socket) throws IOException {
		Packet var5 = null;
		final int var6 = par3Socket.getSoTimeout();
		int var9;

		try {
			var9 = par1DataInputStream.read();

			if (var9 == -1) {
				return null;
			}

			if (par2
					&& !Packet.serverPacketIdList.contains(Integer
							.valueOf(var9))
					|| !par2
					&& !Packet.clientPacketIdList.contains(Integer
							.valueOf(var9))) {
				throw new IOException("Bad packet id " + var9);
			}

			var5 = Packet.getNewPacket(par0ILogAgent, var9);

			if (var5 == null) {
				throw new IOException("Bad packet id " + var9);
			}

			var5.field_98193_m = par0ILogAgent;

			if (var5 instanceof Packet254ServerPing) {
				par3Socket.setSoTimeout(1500);
			}

			var5.readPacketData(par1DataInputStream);
			++Packet.receivedID;
			Packet.receivedSize += var5.getPacketSize();
		} catch (final EOFException var8) {
			par0ILogAgent.logSevere("Reached end of stream");
			return null;
		}

		PacketCount.countPacket(var9, var5.getPacketSize());
		++Packet.receivedID;
		Packet.receivedSize += var5.getPacketSize();
		par3Socket.setSoTimeout(var6);
		return var5;
	}

	/**
	 * Writes a packet, prefixed by its ID, to the data stream.
	 */
	public static void writePacket(final Packet par0Packet,
			final DataOutputStream par1DataOutputStream) throws IOException {
		par1DataOutputStream.write(par0Packet.getPacketId());
		par0Packet.writePacketData(par1DataOutputStream);
		++Packet.sentID;
		Packet.sentSize += par0Packet.getPacketSize();
	}

	/**
	 * Writes a String to the DataOutputStream
	 */
	public static void writeString(final String par0Str,
			final DataOutputStream par1DataOutputStream) throws IOException {
		if (par0Str.length() > 32767) {
			throw new IOException("String too big");
		} else {
			par1DataOutputStream.writeShort(par0Str.length());
			par1DataOutputStream.writeChars(par0Str);
		}
	}

	/**
	 * Reads a string from a packet
	 */
	public static String readString(final DataInputStream par0DataInputStream,
			final int par1) throws IOException {
		final short var2 = par0DataInputStream.readShort();

		if (var2 > par1) {
			throw new IOException(
					"Received string length longer than maximum allowed ("
							+ var2 + " > " + par1 + ")");
		} else if (var2 < 0) {
			throw new IOException(
					"Received string length is less than zero! Weird string!");
		} else {
			final StringBuilder var3 = new StringBuilder();

			for (int var4 = 0; var4 < var2; ++var4) {
				var3.append(par0DataInputStream.readChar());
			}

			return var3.toString();
		}
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	public abstract void readPacketData(DataInputStream var1)
			throws IOException;

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	public abstract void writePacketData(DataOutputStream var1)
			throws IOException;

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	public abstract void processPacket(NetHandler var1);

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	public abstract int getPacketSize();

	/**
	 * only false for the abstract Packet class, all real packets return true
	 */
	public boolean isRealPacket() {
		return false;
	}

	/**
	 * eg return packet30entity.entityId == entityId; WARNING : will throw if
	 * you compare a packet to a different packet class
	 */
	public boolean containsSameEntityIDAs(final Packet par1Packet) {
		return false;
	}

	/**
	 * If this returns true, the packet may be processed on any thread;
	 * otherwise it is queued for the main thread to handle.
	 */
	public boolean canProcessAsync() {
		return false;
	}

	@Override
	public String toString() {
		final String var1 = this.getClass().getSimpleName();
		return var1;
	}

	/**
	 * Reads a ItemStack from the InputStream
	 */
	public static ItemStack readItemStack(
			final DataInputStream par0DataInputStream) throws IOException {
		ItemStack var1 = null;
		final short var2 = par0DataInputStream.readShort();

		if (var2 >= 0) {
			final byte var3 = par0DataInputStream.readByte();
			final short var4 = par0DataInputStream.readShort();
			var1 = new ItemStack(var2, var3, var4);
			var1.stackTagCompound = Packet
					.readNBTTagCompound(par0DataInputStream);
		}

		return var1;
	}

	/**
	 * Writes the ItemStack's ID (short), then size (byte), then damage. (short)
	 */
	public static void writeItemStack(final ItemStack par0ItemStack,
			final DataOutputStream par1DataOutputStream) throws IOException {
		if (par0ItemStack == null) {
			par1DataOutputStream.writeShort(-1);
		} else {
			par1DataOutputStream.writeShort(par0ItemStack.itemID);
			par1DataOutputStream.writeByte(par0ItemStack.stackSize);
			par1DataOutputStream.writeShort(par0ItemStack.getItemDamage());
			NBTTagCompound var2 = null;

			if (par0ItemStack.getItem().isDamageable()
					|| par0ItemStack.getItem().getShareTag()) {
				var2 = par0ItemStack.stackTagCompound;
			}

			Packet.writeNBTTagCompound(var2, par1DataOutputStream);
		}
	}

	/**
	 * Reads a compressed NBTTagCompound from the InputStream
	 */
	public static NBTTagCompound readNBTTagCompound(
			final DataInputStream par0DataInputStream) throws IOException {
		final short var1 = par0DataInputStream.readShort();

		if (var1 < 0) {
			return null;
		} else {
			final byte[] var2 = new byte[var1];
			par0DataInputStream.readFully(var2);
			return CompressedStreamTools.decompress(var2);
		}
	}

	/**
	 * Writes a compressed NBTTagCompound to the OutputStream
	 */
	protected static void writeNBTTagCompound(
			final NBTTagCompound par0NBTTagCompound,
			final DataOutputStream par1DataOutputStream) throws IOException {
		if (par0NBTTagCompound == null) {
			par1DataOutputStream.writeShort(-1);
		} else {
			final byte[] var2 = CompressedStreamTools
					.compress(par0NBTTagCompound);
			par1DataOutputStream.writeShort((short) var2.length);
			par1DataOutputStream.write(var2);
		}
	}

	static {
		Packet.addIdClassMapping(0, true, true, Packet0KeepAlive.class);
		Packet.addIdClassMapping(1, true, true, Packet1Login.class);
		Packet.addIdClassMapping(2, false, true, Packet2ClientProtocol.class);
		Packet.addIdClassMapping(3, true, true, Packet3Chat.class);
		Packet.addIdClassMapping(4, true, false, Packet4UpdateTime.class);
		Packet.addIdClassMapping(5, true, false, Packet5PlayerInventory.class);
		Packet.addIdClassMapping(6, true, false, Packet6SpawnPosition.class);
		Packet.addIdClassMapping(7, false, true, Packet7UseEntity.class);
		Packet.addIdClassMapping(8, true, false, Packet8UpdateHealth.class);
		Packet.addIdClassMapping(9, true, true, Packet9Respawn.class);
		Packet.addIdClassMapping(10, true, true, Packet10Flying.class);
		Packet.addIdClassMapping(11, true, true, Packet11PlayerPosition.class);
		Packet.addIdClassMapping(12, true, true, Packet12PlayerLook.class);
		Packet.addIdClassMapping(13, true, true, Packet13PlayerLookMove.class);
		Packet.addIdClassMapping(14, false, true, Packet14BlockDig.class);
		Packet.addIdClassMapping(15, false, true, Packet15Place.class);
		Packet.addIdClassMapping(16, true, true, Packet16BlockItemSwitch.class);
		Packet.addIdClassMapping(17, true, false, Packet17Sleep.class);
		Packet.addIdClassMapping(18, true, true, Packet18Animation.class);
		Packet.addIdClassMapping(19, false, true, Packet19EntityAction.class);
		Packet.addIdClassMapping(20, true, false,
				Packet20NamedEntitySpawn.class);
		Packet.addIdClassMapping(22, true, false, Packet22Collect.class);
		Packet.addIdClassMapping(23, true, false, Packet23VehicleSpawn.class);
		Packet.addIdClassMapping(24, true, false, Packet24MobSpawn.class);
		Packet.addIdClassMapping(25, true, false, Packet25EntityPainting.class);
		Packet.addIdClassMapping(26, true, false, Packet26EntityExpOrb.class);
		Packet.addIdClassMapping(28, true, false, Packet28EntityVelocity.class);
		Packet.addIdClassMapping(29, true, false, Packet29DestroyEntity.class);
		Packet.addIdClassMapping(30, true, false, Packet30Entity.class);
		Packet.addIdClassMapping(31, true, false, Packet31RelEntityMove.class);
		Packet.addIdClassMapping(32, true, false, Packet32EntityLook.class);
		Packet.addIdClassMapping(33, true, false,
				Packet33RelEntityMoveLook.class);
		Packet.addIdClassMapping(34, true, false, Packet34EntityTeleport.class);
		Packet.addIdClassMapping(35, true, false,
				Packet35EntityHeadRotation.class);
		Packet.addIdClassMapping(38, true, false, Packet38EntityStatus.class);
		Packet.addIdClassMapping(39, true, false, Packet39AttachEntity.class);
		Packet.addIdClassMapping(40, true, false, Packet40EntityMetadata.class);
		Packet.addIdClassMapping(41, true, false, Packet41EntityEffect.class);
		Packet.addIdClassMapping(42, true, false,
				Packet42RemoveEntityEffect.class);
		Packet.addIdClassMapping(43, true, false, Packet43Experience.class);
		Packet.addIdClassMapping(51, true, false, Packet51MapChunk.class);
		Packet.addIdClassMapping(52, true, false,
				Packet52MultiBlockChange.class);
		Packet.addIdClassMapping(53, true, false, Packet53BlockChange.class);
		Packet.addIdClassMapping(54, true, false, Packet54PlayNoteBlock.class);
		Packet.addIdClassMapping(55, true, false, Packet55BlockDestroy.class);
		Packet.addIdClassMapping(56, true, false, Packet56MapChunks.class);
		Packet.addIdClassMapping(60, true, false, Packet60Explosion.class);
		Packet.addIdClassMapping(61, true, false, Packet61DoorChange.class);
		Packet.addIdClassMapping(62, true, false, Packet62LevelSound.class);
		Packet.addIdClassMapping(63, true, false, Packet63WorldParticles.class);
		Packet.addIdClassMapping(70, true, false, Packet70GameEvent.class);
		Packet.addIdClassMapping(71, true, false, Packet71Weather.class);
		Packet.addIdClassMapping(100, true, false, Packet100OpenWindow.class);
		Packet.addIdClassMapping(101, true, true, Packet101CloseWindow.class);
		Packet.addIdClassMapping(102, false, true, Packet102WindowClick.class);
		Packet.addIdClassMapping(103, true, false, Packet103SetSlot.class);
		Packet.addIdClassMapping(104, true, false, Packet104WindowItems.class);
		Packet.addIdClassMapping(105, true, false,
				Packet105UpdateProgressbar.class);
		Packet.addIdClassMapping(106, true, true, Packet106Transaction.class);
		Packet.addIdClassMapping(107, true, true,
				Packet107CreativeSetSlot.class);
		Packet.addIdClassMapping(108, false, true, Packet108EnchantItem.class);
		Packet.addIdClassMapping(130, true, true, Packet130UpdateSign.class);
		Packet.addIdClassMapping(131, true, false, Packet131MapData.class);
		Packet.addIdClassMapping(132, true, false,
				Packet132TileEntityData.class);
		Packet.addIdClassMapping(200, true, false, Packet200Statistic.class);
		Packet.addIdClassMapping(201, true, false, Packet201PlayerInfo.class);
		Packet.addIdClassMapping(202, true, true,
				Packet202PlayerAbilities.class);
		Packet.addIdClassMapping(203, true, true, Packet203AutoComplete.class);
		Packet.addIdClassMapping(204, false, true, Packet204ClientInfo.class);
		Packet.addIdClassMapping(205, false, true, Packet205ClientCommand.class);
		Packet.addIdClassMapping(206, true, false, Packet206SetObjective.class);
		Packet.addIdClassMapping(207, true, false, Packet207SetScore.class);
		Packet.addIdClassMapping(208, true, false,
				Packet208SetDisplayObjective.class);
		Packet.addIdClassMapping(209, true, false, Packet209SetPlayerTeam.class);
		Packet.addIdClassMapping(250, true, true, Packet250CustomPayload.class);
		Packet.addIdClassMapping(252, true, true, Packet252SharedKey.class);
		Packet.addIdClassMapping(253, true, false,
				Packet253ServerAuthData.class);
		Packet.addIdClassMapping(254, false, true, Packet254ServerPing.class);
		Packet.addIdClassMapping(255, true, true, Packet255KickDisconnect.class);
	}
}
