package net.minecraft.src;

public class InventoryCrafting implements IInventory {
	/** List of the stacks in the crafting matrix. */
	private final ItemStack[] stackList;

	/** the width of the crafting inventory */
	private final int inventoryWidth;

	/**
	 * Class containing the callbacks for the events on_GUIClosed and
	 * on_CraftMaxtrixChanged.
	 */
	private final Container eventHandler;

	public InventoryCrafting(final Container par1Container, final int par2,
			final int par3) {
		final int var4 = par2 * par3;
		stackList = new ItemStack[var4];
		eventHandler = par1Container;
		inventoryWidth = par2;
	}

	/**
	 * Returns the number of slots in the inventory.
	 */
	@Override
	public int getSizeInventory() {
		return stackList.length;
	}

	/**
	 * Returns the stack in slot i
	 */
	@Override
	public ItemStack getStackInSlot(final int par1) {
		return par1 >= getSizeInventory() ? null : stackList[par1];
	}

	/**
	 * Returns the itemstack in the slot specified (Top left is 0, 0). Args:
	 * row, column
	 */
	public ItemStack getStackInRowAndColumn(final int par1, final int par2) {
		if (par1 >= 0 && par1 < inventoryWidth) {
			final int var3 = par1 + par2 * inventoryWidth;
			return getStackInSlot(var3);
		} else {
			return null;
		}
	}

	/**
	 * Returns the name of the inventory.
	 */
	@Override
	public String getInvName() {
		return "container.crafting";
	}

	/**
	 * If this returns false, the inventory name will be used as an unlocalized
	 * name, and translated into the player's language. Otherwise it will be
	 * used directly.
	 */
	@Override
	public boolean isInvNameLocalized() {
		return false;
	}

	/**
	 * When some containers are closed they call this on each slot, then drop
	 * whatever it returns as an EntityItem - like when you close a workbench
	 * GUI.
	 */
	@Override
	public ItemStack getStackInSlotOnClosing(final int par1) {
		if (stackList[par1] != null) {
			final ItemStack var2 = stackList[par1];
			stackList[par1] = null;
			return var2;
		} else {
			return null;
		}
	}

	/**
	 * Removes from an inventory slot (first arg) up to a specified number
	 * (second arg) of items and returns them in a new stack.
	 */
	@Override
	public ItemStack decrStackSize(final int par1, final int par2) {
		if (stackList[par1] != null) {
			ItemStack var3;

			if (stackList[par1].stackSize <= par2) {
				var3 = stackList[par1];
				stackList[par1] = null;
				eventHandler.onCraftMatrixChanged(this);
				return var3;
			} else {
				var3 = stackList[par1].splitStack(par2);

				if (stackList[par1].stackSize == 0) {
					stackList[par1] = null;
				}

				eventHandler.onCraftMatrixChanged(this);
				return var3;
			}
		} else {
			return null;
		}
	}

	/**
	 * Sets the given item stack to the specified slot in the inventory (can be
	 * crafting or armor sections).
	 */
	@Override
	public void setInventorySlotContents(final int par1,
			final ItemStack par2ItemStack) {
		stackList[par1] = par2ItemStack;
		eventHandler.onCraftMatrixChanged(this);
	}

	/**
	 * Returns the maximum stack size for a inventory slot. Seems to always be
	 * 64, possibly will be extended. *Isn't this more of a set than a get?*
	 */
	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	/**
	 * Called when an the contents of an Inventory change, usually
	 */
	@Override
	public void onInventoryChanged() {
	}

	/**
	 * Do not make give this method the name canInteractWith because it clashes
	 * with Container
	 */
	@Override
	public boolean isUseableByPlayer(final EntityPlayer par1EntityPlayer) {
		return true;
	}

	@Override
	public void openChest() {
	}

	@Override
	public void closeChest() {
	}

	/**
	 * Returns true if automation is allowed to insert the given stack (ignoring
	 * stack size) into the given slot.
	 */
	@Override
	public boolean isStackValidForSlot(final int par1,
			final ItemStack par2ItemStack) {
		return true;
	}
}
