package net.minecraft.src;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TileEntityPiston extends TileEntity {
	private int storedBlockID;
	private int storedMetadata;

	/** the side the front of the piston is on */
	private int storedOrientation;

	/** if this piston is extending or not */
	private boolean extending;
	private boolean shouldHeadBeRendered;
	private float progress;

	/** the progress in (de)extending */
	private float lastProgress;
	private final List pushedObjects = new ArrayList();

	public TileEntityPiston() {
	}

	public TileEntityPiston(final int par1, final int par2, final int par3,
			final boolean par4, final boolean par5) {
		storedBlockID = par1;
		storedMetadata = par2;
		storedOrientation = par3;
		extending = par4;
		shouldHeadBeRendered = par5;
	}

	public int getStoredBlockID() {
		return storedBlockID;
	}

	/**
	 * Returns block data at the location of this entity (client-only).
	 */
	@Override
	public int getBlockMetadata() {
		return storedMetadata;
	}

	/**
	 * Returns true if a piston is extending
	 */
	public boolean isExtending() {
		return extending;
	}

	/**
	 * Returns the orientation of the piston as an int
	 */
	public int getPistonOrientation() {
		return storedOrientation;
	}

	public boolean shouldRenderHead() {
		return shouldHeadBeRendered;
	}

	/**
	 * Get interpolated progress value (between lastProgress and progress) given
	 * the fractional time between ticks as an argument.
	 */
	public float getProgress(float par1) {
		if (par1 > 1.0F) {
			par1 = 1.0F;
		}

		return lastProgress + (progress - lastProgress) * par1;
	}

	public float getOffsetX(final float par1) {
		return extending ? (getProgress(par1) - 1.0F)
				* Facing.offsetsXForSide[storedOrientation]
				: (1.0F - getProgress(par1))
						* Facing.offsetsXForSide[storedOrientation];
	}

	public float getOffsetY(final float par1) {
		return extending ? (getProgress(par1) - 1.0F)
				* Facing.offsetsYForSide[storedOrientation]
				: (1.0F - getProgress(par1))
						* Facing.offsetsYForSide[storedOrientation];
	}

	public float getOffsetZ(final float par1) {
		return extending ? (getProgress(par1) - 1.0F)
				* Facing.offsetsZForSide[storedOrientation]
				: (1.0F - getProgress(par1))
						* Facing.offsetsZForSide[storedOrientation];
	}

	private void updatePushedObjects(float par1, final float par2) {
		if (extending) {
			par1 = 1.0F - par1;
		} else {
			--par1;
		}

		final AxisAlignedBB var3 = Block.pistonMoving.getAxisAlignedBB(
				worldObj, xCoord, yCoord, zCoord, storedBlockID, par1,
				storedOrientation);

		if (var3 != null) {
			final List var4 = worldObj.getEntitiesWithinAABBExcludingEntity(
					(Entity) null, var3);

			if (!var4.isEmpty()) {
				pushedObjects.addAll(var4);
				final Iterator var5 = pushedObjects.iterator();

				while (var5.hasNext()) {
					final Entity var6 = (Entity) var5.next();
					var6.moveEntity(par2
							* Facing.offsetsXForSide[storedOrientation], par2
							* Facing.offsetsYForSide[storedOrientation], par2
							* Facing.offsetsZForSide[storedOrientation]);
				}

				pushedObjects.clear();
			}
		}
	}

	/**
	 * removes a pistons tile entity (and if the piston is moving, stops it)
	 */
	public void clearPistonTileEntity() {
		if (lastProgress < 1.0F && worldObj != null) {
			lastProgress = progress = 1.0F;
			worldObj.removeBlockTileEntity(xCoord, yCoord, zCoord);
			invalidate();

			if (worldObj.getBlockId(xCoord, yCoord, zCoord) == Block.pistonMoving.blockID) {
				worldObj.setBlock(xCoord, yCoord, zCoord, storedBlockID,
						storedMetadata, 3);
				worldObj.notifyBlockOfNeighborChange(xCoord, yCoord, zCoord,
						storedBlockID);
			}
		}
	}

	/**
	 * Allows the entity to update its state. Overridden in most subclasses,
	 * e.g. the mob spawner uses this to count ticks and creates a new spawn
	 * inside its implementation.
	 */
	@Override
	public void updateEntity() {
		lastProgress = progress;

		if (lastProgress >= 1.0F) {
			updatePushedObjects(1.0F, 0.25F);
			worldObj.removeBlockTileEntity(xCoord, yCoord, zCoord);
			invalidate();

			if (worldObj.getBlockId(xCoord, yCoord, zCoord) == Block.pistonMoving.blockID) {
				worldObj.setBlock(xCoord, yCoord, zCoord, storedBlockID,
						storedMetadata, 3);
				worldObj.notifyBlockOfNeighborChange(xCoord, yCoord, zCoord,
						storedBlockID);
			}
		} else {
			progress += 0.5F;

			if (progress >= 1.0F) {
				progress = 1.0F;
			}

			if (extending) {
				updatePushedObjects(progress, progress - lastProgress + 0.0625F);
			}
		}
	}

	/**
	 * Reads a tile entity from NBT.
	 */
	@Override
	public void readFromNBT(final NBTTagCompound par1NBTTagCompound) {
		super.readFromNBT(par1NBTTagCompound);
		storedBlockID = par1NBTTagCompound.getInteger("blockId");
		storedMetadata = par1NBTTagCompound.getInteger("blockData");
		storedOrientation = par1NBTTagCompound.getInteger("facing");
		lastProgress = progress = par1NBTTagCompound.getFloat("progress");
		extending = par1NBTTagCompound.getBoolean("extending");
	}

	/**
	 * Writes a tile entity to NBT.
	 */
	@Override
	public void writeToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeToNBT(par1NBTTagCompound);
		par1NBTTagCompound.setInteger("blockId", storedBlockID);
		par1NBTTagCompound.setInteger("blockData", storedMetadata);
		par1NBTTagCompound.setInteger("facing", storedOrientation);
		par1NBTTagCompound.setFloat("progress", lastProgress);
		par1NBTTagCompound.setBoolean("extending", extending);
	}
}
