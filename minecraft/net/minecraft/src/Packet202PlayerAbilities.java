package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet202PlayerAbilities extends Packet {
	/** Disables player damage. */
	private boolean disableDamage = false;

	/** Indicates whether the player is flying or not. */
	private boolean isFlying = false;

	/** Whether or not to allow the player to fly when they double jump. */
	private boolean allowFlying = false;

	/**
	 * Used to determine if creative mode is enabled, and therefore if items
	 * should be depleted on usage
	 */
	private boolean isCreativeMode = false;
	private float flySpeed;
	private float walkSpeed;

	public Packet202PlayerAbilities() {
	}

	public Packet202PlayerAbilities(
			final PlayerCapabilities par1PlayerCapabilities) {
		setDisableDamage(par1PlayerCapabilities.disableDamage);
		setFlying(par1PlayerCapabilities.isFlying);
		setAllowFlying(par1PlayerCapabilities.allowFlying);
		setCreativeMode(par1PlayerCapabilities.isCreativeMode);
		setFlySpeed(par1PlayerCapabilities.getFlySpeed());
		setWalkSpeed(par1PlayerCapabilities.getWalkSpeed());
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		final byte var2 = par1DataInputStream.readByte();
		setDisableDamage((var2 & 1) > 0);
		setFlying((var2 & 2) > 0);
		setAllowFlying((var2 & 4) > 0);
		setCreativeMode((var2 & 8) > 0);
		setFlySpeed(par1DataInputStream.readByte() / 255.0F);
		setWalkSpeed(par1DataInputStream.readByte() / 255.0F);
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		byte var2 = 0;

		if (getDisableDamage()) {
			var2 = (byte) (var2 | 1);
		}

		if (getFlying()) {
			var2 = (byte) (var2 | 2);
		}

		if (getAllowFlying()) {
			var2 = (byte) (var2 | 4);
		}

		if (isCreativeMode()) {
			var2 = (byte) (var2 | 8);
		}

		par1DataOutputStream.writeByte(var2);
		par1DataOutputStream.writeByte((int) (flySpeed * 255.0F));
		par1DataOutputStream.writeByte((int) (walkSpeed * 255.0F));
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handlePlayerAbilities(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 2;
	}

	public boolean getDisableDamage() {
		return disableDamage;
	}

	/**
	 * Sets whether damage is disabled or not.
	 */
	public void setDisableDamage(final boolean par1) {
		disableDamage = par1;
	}

	public boolean getFlying() {
		return isFlying;
	}

	/**
	 * Sets whether we're currently flying or not.
	 */
	public void setFlying(final boolean par1) {
		isFlying = par1;
	}

	public boolean getAllowFlying() {
		return allowFlying;
	}

	public void setAllowFlying(final boolean par1) {
		allowFlying = par1;
	}

	public boolean isCreativeMode() {
		return isCreativeMode;
	}

	public void setCreativeMode(final boolean par1) {
		isCreativeMode = par1;
	}

	public float getFlySpeed() {
		return flySpeed;
	}

	/**
	 * Sets the flying speed.
	 */
	public void setFlySpeed(final float par1) {
		flySpeed = par1;
	}

	public float getWalkSpeed() {
		return walkSpeed;
	}

	/**
	 * Sets the walking speed.
	 */
	public void setWalkSpeed(final float par1) {
		walkSpeed = par1;
	}

	/**
	 * only false for the abstract Packet class, all real packets return true
	 */
	@Override
	public boolean isRealPacket() {
		return true;
	}

	/**
	 * eg return packet30entity.entityId == entityId; WARNING : will throw if
	 * you compare a packet to a different packet class
	 */
	@Override
	public boolean containsSameEntityIDAs(final Packet par1Packet) {
		return true;
	}
}
