package net.minecraft.src;

import java.util.Random;

public class BlockComparator extends BlockRedstoneLogic implements
		ITileEntityProvider {
	public BlockComparator(final int par1, final boolean par2) {
		super(par1, par2);
		isBlockContainer = true;
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return Item.comparator.itemID;
	}

	/**
	 * only called by clickMiddleMouseButton , and passed to
	 * inventory.setCurrentItem (along with isCreative)
	 */
	@Override
	public int idPicked(final World par1World, final int par2, final int par3,
			final int par4) {
		return Item.comparator.itemID;
	}

	@Override
	protected int func_94481_j_(final int par1) {
		return 2;
	}

	@Override
	protected BlockRedstoneLogic func_94485_e() {
		return Block.redstoneComparatorActive;
	}

	@Override
	protected BlockRedstoneLogic func_94484_i() {
		return Block.redstoneComparatorIdle;
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return 37;
	}

	/**
	 * From the specified side and block metadata retrieves the blocks texture.
	 * Args: side, metadata
	 */
	@Override
	public Icon getIcon(final int par1, final int par2) {
		final boolean var3 = isRepeaterPowered || (par2 & 8) != 0;
		return par1 == 0 ? var3 ? Block.torchRedstoneActive
				.getBlockTextureFromSide(par1) : Block.torchRedstoneIdle
				.getBlockTextureFromSide(par1)
				: par1 == 1 ? var3 ? Block.redstoneComparatorActive.blockIcon
						: blockIcon : Block.stoneDoubleSlab
						.getBlockTextureFromSide(1);
	}

	@Override
	protected boolean func_96470_c(final int par1) {
		return isRepeaterPowered || (par1 & 8) != 0;
	}

	@Override
	protected int func_94480_d(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		return getTileEntityComparator(par1IBlockAccess, par2, par3, par4)
				.func_96100_a();
	}

	private int func_94491_m(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		return !func_94490_c(par5) ? getInputStrength(par1World, par2, par3,
				par4, par5) : Math.max(
				getInputStrength(par1World, par2, par3, par4, par5)
						- func_94482_f(par1World, par2, par3, par4, par5), 0);
	}

	public boolean func_94490_c(final int par1) {
		return (par1 & 4) == 4;
	}

	@Override
	protected boolean func_94478_d(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		final int var6 = getInputStrength(par1World, par2, par3, par4, par5);

		if (var6 >= 15) {
			return true;
		} else if (var6 == 0) {
			return false;
		} else {
			final int var7 = func_94482_f(par1World, par2, par3, par4, par5);
			return var7 == 0 ? true : var6 >= var7;
		}
	}

	/**
	 * Returns the signal strength at one input of the block. Args: world, X, Y,
	 * Z, side
	 */
	@Override
	protected int getInputStrength(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		int var6 = super.getInputStrength(par1World, par2, par3, par4, par5);
		final int var7 = BlockDirectional.getDirection(par5);
		int var8 = par2 + Direction.offsetX[var7];
		int var9 = par4 + Direction.offsetZ[var7];
		int var10 = par1World.getBlockId(var8, par3, var9);

		if (var10 > 0) {
			if (Block.blocksList[var10].hasComparatorInputOverride()) {
				var6 = Block.blocksList[var10].getComparatorInputOverride(
						par1World, var8, par3, var9,
						Direction.rotateOpposite[var7]);
			} else if (var6 < 15 && Block.isNormalCube(var10)) {
				var8 += Direction.offsetX[var7];
				var9 += Direction.offsetZ[var7];
				var10 = par1World.getBlockId(var8, par3, var9);

				if (var10 > 0
						&& Block.blocksList[var10].hasComparatorInputOverride()) {
					var6 = Block.blocksList[var10].getComparatorInputOverride(
							par1World, var8, par3, var9,
							Direction.rotateOpposite[var7]);
				}
			}
		}

		return var6;
	}

	/**
	 * Returns the blockTileEntity at given coordinates.
	 */
	public TileEntityComparator getTileEntityComparator(
			final IBlockAccess par1IBlockAccess, final int par2,
			final int par3, final int par4) {
		return (TileEntityComparator) par1IBlockAccess.getBlockTileEntity(par2,
				par3, par4);
	}

	/**
	 * Called upon block activation (right click on the block.)
	 */
	@Override
	public boolean onBlockActivated(final World par1World, final int par2,
			final int par3, final int par4,
			final EntityPlayer par5EntityPlayer, final int par6,
			final float par7, final float par8, final float par9) {
		final int var10 = par1World.getBlockMetadata(par2, par3, par4);
		final boolean var11 = isRepeaterPowered | (var10 & 8) != 0;
		final boolean var12 = !func_94490_c(var10);
		int var13 = var12 ? 4 : 0;
		var13 |= var11 ? 8 : 0;
		par1World.playSoundEffect(par2 + 0.5D, par3 + 0.5D, par4 + 0.5D,
				"random.click", 0.3F, var12 ? 0.55F : 0.5F);
		par1World.setBlockMetadataWithNotify(par2, par3, par4, var13 | var10
				& 3, 2);
		func_96476_c(par1World, par2, par3, par4, par1World.rand);
		return true;
	}

	@Override
	protected void func_94479_f(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		if (!par1World.isBlockTickScheduled(par2, par3, par4, blockID)) {
			final int var6 = par1World.getBlockMetadata(par2, par3, par4);
			final int var7 = func_94491_m(par1World, par2, par3, par4, var6);
			final int var8 = getTileEntityComparator(par1World, par2, par3,
					par4).func_96100_a();

			if (var7 != var8
					|| func_96470_c(var6) != func_94478_d(par1World, par2,
							par3, par4, var6)) {
				if (func_83011_d(par1World, par2, par3, par4, var6)) {
					par1World.func_82740_a(par2, par3, par4, blockID,
							func_94481_j_(0), -1);
				} else {
					par1World.func_82740_a(par2, par3, par4, blockID,
							func_94481_j_(0), 0);
				}
			}
		}
	}

	private void func_96476_c(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		final int var6 = par1World.getBlockMetadata(par2, par3, par4);
		final int var7 = func_94491_m(par1World, par2, par3, par4, var6);
		final int var8 = getTileEntityComparator(par1World, par2, par3, par4)
				.func_96100_a();
		getTileEntityComparator(par1World, par2, par3, par4).func_96099_a(var7);

		if (var8 != var7 || !func_94490_c(var6)) {
			final boolean var9 = func_94478_d(par1World, par2, par3, par4, var6);
			final boolean var10 = isRepeaterPowered || (var6 & 8) != 0;

			if (var10 && !var9) {
				par1World.setBlockMetadataWithNotify(par2, par3, par4, var6
						& -9, 2);
			} else if (!var10 && var9) {
				par1World.setBlockMetadataWithNotify(par2, par3, par4,
						var6 | 8, 2);
			}

			func_94483_i_(par1World, par2, par3, par4);
		}
	}

	/**
	 * Ticks the block if it's been scheduled
	 */
	@Override
	public void updateTick(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		if (isRepeaterPowered) {
			final int var6 = par1World.getBlockMetadata(par2, par3, par4);
			par1World.setBlock(par2, par3, par4, func_94484_i().blockID,
					var6 | 8, 4);
		}

		func_96476_c(par1World, par2, par3, par4, par5Random);
	}

	/**
	 * Called whenever the block is added into the world. Args: world, x, y, z
	 */
	@Override
	public void onBlockAdded(final World par1World, final int par2,
			final int par3, final int par4) {
		super.onBlockAdded(par1World, par2, par3, par4);
		par1World.setBlockTileEntity(par2, par3, par4,
				createNewTileEntity(par1World));
	}

	/**
	 * ejects contained items into the world, and notifies neighbours of an
	 * update, as appropriate
	 */
	@Override
	public void breakBlock(final World par1World, final int par2,
			final int par3, final int par4, final int par5, final int par6) {
		super.breakBlock(par1World, par2, par3, par4, par5, par6);
		par1World.removeBlockTileEntity(par2, par3, par4);
		func_94483_i_(par1World, par2, par3, par4);
	}

	/**
	 * Called when the block receives a BlockEvent - see World.addBlockEvent. By
	 * default, passes it on to the tile entity at this location. Args: world,
	 * x, y, z, blockID, EventID, event parameter
	 */
	@Override
	public boolean onBlockEventReceived(final World par1World, final int par2,
			final int par3, final int par4, final int par5, final int par6) {
		super.onBlockEventReceived(par1World, par2, par3, par4, par5, par6);
		final TileEntity var7 = par1World.getBlockTileEntity(par2, par3, par4);
		return var7 != null ? var7.receiveClientEvent(par5, par6) : false;
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		blockIcon = par1IconRegister
				.registerIcon(isRepeaterPowered ? "comparator_lit"
						: "comparator");
	}

	/**
	 * Returns a new instance of a block's tile entity class. Called on placing
	 * the block.
	 */
	@Override
	public TileEntity createNewTileEntity(final World par1World) {
		return new TileEntityComparator();
	}
}
