package net.minecraft.src;

public class StitchHolder implements Comparable {
	private final Texture theTexture;
	private final int width;
	private final int height;
	private boolean rotated;
	private float scaleFactor = 1.0F;

	public StitchHolder(final Texture par1Texture) {
		theTexture = par1Texture;
		width = par1Texture.getWidth();
		height = par1Texture.getHeight();
		rotated = ceil16(height) > ceil16(width);
	}

	public Texture func_98150_a() {
		return theTexture;
	}

	public int getWidth() {
		return rotated ? ceil16((int) (height * scaleFactor))
				: ceil16((int) (width * scaleFactor));
	}

	public int getHeight() {
		return rotated ? ceil16((int) (width * scaleFactor))
				: ceil16((int) (height * scaleFactor));
	}

	public void rotate() {
		rotated = !rotated;
	}

	public boolean isRotated() {
		return rotated;
	}

	private int ceil16(final int par1) {
		final int var2 = TextureUtils.ceilPowerOfTwo(par1);
		return var2 < 16 ? 16 : var2;
	}

	public void setNewDimension(final int par1) {
		if (width > par1 && height > par1) {
			scaleFactor = (float) par1 / (float) Math.min(width, height);
		}
	}

	@Override
	public String toString() {
		return "TextureHolder{width=" + width + ", height=" + height + '}';
	}

	/**
	 * See Comparable.compareTo.
	 */
	public int compareToStitchHolder(final StitchHolder par1StitchHolder) {
		int var2;

		if (getHeight() == par1StitchHolder.getHeight()) {
			if (getWidth() == par1StitchHolder.getWidth()) {
				if (theTexture.getTextureName() == null) {
					return par1StitchHolder.theTexture.getTextureName() == null ? 0
							: -1;
				}

				return theTexture.getTextureName().compareTo(
						par1StitchHolder.theTexture.getTextureName());
			}

			var2 = getWidth() < par1StitchHolder.getWidth() ? 1 : -1;
		} else {
			var2 = getHeight() < par1StitchHolder.getHeight() ? 1 : -1;
		}

		return var2;
	}

	@Override
	public int compareTo(final Object par1Obj) {
		return compareToStitchHolder((StitchHolder) par1Obj);
	}
}
