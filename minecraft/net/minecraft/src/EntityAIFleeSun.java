package net.minecraft.src;

import java.util.Random;

public class EntityAIFleeSun extends EntityAIBase {
	private final EntityCreature theCreature;
	private double shelterX;
	private double shelterY;
	private double shelterZ;
	private final float movementSpeed;
	private final World theWorld;

	public EntityAIFleeSun(final EntityCreature par1EntityCreature,
			final float par2) {
		theCreature = par1EntityCreature;
		movementSpeed = par2;
		theWorld = par1EntityCreature.worldObj;
		setMutexBits(1);
	}

	/**
	 * Returns whether the EntityAIBase should begin execution.
	 */
	@Override
	public boolean shouldExecute() {
		if (!theWorld.isDaytime()) {
			return false;
		} else if (!theCreature.isBurning()) {
			return false;
		} else if (!theWorld.canBlockSeeTheSky(
				MathHelper.floor_double(theCreature.posX),
				(int) theCreature.boundingBox.minY,
				MathHelper.floor_double(theCreature.posZ))) {
			return false;
		} else {
			final Vec3 var1 = findPossibleShelter();

			if (var1 == null) {
				return false;
			} else {
				shelterX = var1.xCoord;
				shelterY = var1.yCoord;
				shelterZ = var1.zCoord;
				return true;
			}
		}
	}

	/**
	 * Returns whether an in-progress EntityAIBase should continue executing
	 */
	@Override
	public boolean continueExecuting() {
		return !theCreature.getNavigator().noPath();
	}

	/**
	 * Execute a one shot task or start executing a continuous task
	 */
	@Override
	public void startExecuting() {
		theCreature.getNavigator().tryMoveToXYZ(shelterX, shelterY, shelterZ,
				movementSpeed);
	}

	private Vec3 findPossibleShelter() {
		final Random var1 = theCreature.getRNG();

		for (int var2 = 0; var2 < 10; ++var2) {
			final int var3 = MathHelper.floor_double(theCreature.posX
					+ var1.nextInt(20) - 10.0D);
			final int var4 = MathHelper
					.floor_double(theCreature.boundingBox.minY
							+ var1.nextInt(6) - 3.0D);
			final int var5 = MathHelper.floor_double(theCreature.posZ
					+ var1.nextInt(20) - 10.0D);

			if (!theWorld.canBlockSeeTheSky(var3, var4, var5)
					&& theCreature.getBlockPathWeight(var3, var4, var5) < 0.0F) {
				return theWorld.getWorldVec3Pool().getVecFromPool(var3, var4,
						var5);
			}
		}

		return null;
	}
}
