package net.minecraft.src;

public class EntitySilverfish extends EntityMob {
	/**
	 * A cooldown before this entity will search for another Silverfish to join
	 * them in battle.
	 */
	private int allySummonCooldown;

	public EntitySilverfish(final World par1World) {
		super(par1World);
		texture = "/mob/silverfish.png";
		setSize(0.3F, 0.7F);
		moveSpeed = 0.6F;
	}

	@Override
	public int getMaxHealth() {
		return 8;
	}

	/**
	 * returns if this entity triggers Block.onEntityWalking on the blocks they
	 * walk on. used for spiders and wolves to prevent them from trampling crops
	 */
	@Override
	protected boolean canTriggerWalking() {
		return false;
	}

	/**
	 * Finds the closest player within 16 blocks to attack, or null if this
	 * Entity isn't interested in attacking (Animals, Spiders at day, peaceful
	 * PigZombies).
	 */
	@Override
	protected Entity findPlayerToAttack() {
		final double var1 = 8.0D;
		return worldObj.getClosestVulnerablePlayerToEntity(this, var1);
	}

	/**
	 * Returns the sound this mob makes while it's alive.
	 */
	@Override
	protected String getLivingSound() {
		return "mob.silverfish.say";
	}

	/**
	 * Returns the sound this mob makes when it is hurt.
	 */
	@Override
	protected String getHurtSound() {
		return "mob.silverfish.hit";
	}

	/**
	 * Returns the sound this mob makes on death.
	 */
	@Override
	protected String getDeathSound() {
		return "mob.silverfish.kill";
	}

	/**
	 * Called when the entity is attacked.
	 */
	@Override
	public boolean attackEntityFrom(final DamageSource par1DamageSource,
			final int par2) {
		if (isEntityInvulnerable()) {
			return false;
		} else {
			if (allySummonCooldown <= 0
					&& (par1DamageSource instanceof EntityDamageSource || par1DamageSource == DamageSource.magic)) {
				allySummonCooldown = 20;
			}

			return super.attackEntityFrom(par1DamageSource, par2);
		}
	}

	/**
	 * Basic mob attack. Default to touch of death in EntityCreature. Overridden
	 * by each mob to define their attack.
	 */
	@Override
	protected void attackEntity(final Entity par1Entity, final float par2) {
		if (attackTime <= 0 && par2 < 1.2F
				&& par1Entity.boundingBox.maxY > boundingBox.minY
				&& par1Entity.boundingBox.minY < boundingBox.maxY) {
			attackTime = 20;
			attackEntityAsMob(par1Entity);
		}
	}

	/**
	 * Plays step sound at given x, y, z for the entity
	 */
	@Override
	protected void playStepSound(final int par1, final int par2,
			final int par3, final int par4) {
		playSound("mob.silverfish.step", 0.15F, 1.0F);
	}

	/**
	 * Returns the item ID for the item the mob drops on death.
	 */
	@Override
	protected int getDropItemId() {
		return 0;
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate() {
		renderYawOffset = rotationYaw;
		super.onUpdate();
	}

	@Override
	protected void updateEntityActionState() {
		super.updateEntityActionState();

		if (!worldObj.isRemote) {
			int var1;
			int var2;
			int var3;
			int var5;

			if (allySummonCooldown > 0) {
				--allySummonCooldown;

				if (allySummonCooldown == 0) {
					var1 = MathHelper.floor_double(posX);
					var2 = MathHelper.floor_double(posY);
					var3 = MathHelper.floor_double(posZ);
					boolean var4 = false;

					for (var5 = 0; !var4 && var5 <= 5 && var5 >= -5; var5 = var5 <= 0 ? 1 - var5
							: 0 - var5) {
						for (int var6 = 0; !var4 && var6 <= 10 && var6 >= -10; var6 = var6 <= 0 ? 1 - var6
								: 0 - var6) {
							for (int var7 = 0; !var4 && var7 <= 10
									&& var7 >= -10; var7 = var7 <= 0 ? 1 - var7
									: 0 - var7) {
								final int var8 = worldObj.getBlockId(var1
										+ var6, var2 + var5, var3 + var7);

								if (var8 == Block.silverfish.blockID) {
									worldObj.destroyBlock(var1 + var6, var2
											+ var5, var3 + var7, false);
									Block.silverfish.onBlockDestroyedByPlayer(
											worldObj, var1 + var6, var2 + var5,
											var3 + var7, 0);

									if (rand.nextBoolean()) {
										var4 = true;
										break;
									}
								}
							}
						}
					}
				}
			}

			if (entityToAttack == null && !hasPath()) {
				var1 = MathHelper.floor_double(posX);
				var2 = MathHelper.floor_double(posY + 0.5D);
				var3 = MathHelper.floor_double(posZ);
				final int var9 = rand.nextInt(6);
				var5 = worldObj.getBlockId(var1 + Facing.offsetsXForSide[var9],
						var2 + Facing.offsetsYForSide[var9], var3
								+ Facing.offsetsZForSide[var9]);

				if (BlockSilverfish.getPosingIdByMetadata(var5)) {
					worldObj.setBlock(var1 + Facing.offsetsXForSide[var9], var2
							+ Facing.offsetsYForSide[var9], var3
							+ Facing.offsetsZForSide[var9],
							Block.silverfish.blockID,
							BlockSilverfish.getMetadataForBlockType(var5), 3);
					spawnExplosionParticle();
					setDead();
				} else {
					updateWanderPath();
				}
			} else if (entityToAttack != null && !hasPath()) {
				entityToAttack = null;
			}
		}
	}

	/**
	 * Takes a coordinate in and returns a weight to determine how likely this
	 * creature will try to path to the block. Args: x, y, z
	 */
	@Override
	public float getBlockPathWeight(final int par1, final int par2,
			final int par3) {
		return worldObj.getBlockId(par1, par2 - 1, par3) == Block.stone.blockID ? 10.0F
				: super.getBlockPathWeight(par1, par2, par3);
	}

	/**
	 * Checks to make sure the light is not too bright where the mob is spawning
	 */
	@Override
	protected boolean isValidLightLevel() {
		return true;
	}

	/**
	 * Checks if the entity's current position is a valid location to spawn this
	 * entity.
	 */
	@Override
	public boolean getCanSpawnHere() {
		if (super.getCanSpawnHere()) {
			final EntityPlayer var1 = worldObj.getClosestPlayerToEntity(this,
					5.0D);
			return var1 == null;
		} else {
			return false;
		}
	}

	/**
	 * Returns the amount of damage a mob should deal.
	 */
	@Override
	public int getAttackStrength(final Entity par1Entity) {
		return 1;
	}

	/**
	 * Get this Entity's EnumCreatureAttribute
	 */
	@Override
	public EnumCreatureAttribute getCreatureAttribute() {
		return EnumCreatureAttribute.ARTHROPOD;
	}
}
