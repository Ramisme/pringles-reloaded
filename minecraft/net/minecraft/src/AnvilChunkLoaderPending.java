package net.minecraft.src;

class AnvilChunkLoaderPending {
	public final ChunkCoordIntPair chunkCoordinate;
	public final NBTTagCompound nbtTags;

	public AnvilChunkLoaderPending(
			final ChunkCoordIntPair par1ChunkCoordIntPair,
			final NBTTagCompound par2NBTTagCompound) {
		chunkCoordinate = par1ChunkCoordIntPair;
		nbtTags = par2NBTTagCompound;
	}
}
