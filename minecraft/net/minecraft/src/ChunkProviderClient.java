package net.minecraft.src;

import java.util.ArrayList;
import java.util.List;

public class ChunkProviderClient implements IChunkProvider {
	/**
	 * The completely empty chunk used by ChunkProviderClient when chunkMapping
	 * doesn't contain the requested coordinates.
	 */
	private final Chunk blankChunk;

	/**
	 * The mapping between ChunkCoordinates and Chunks that ChunkProviderClient
	 * maintains.
	 */
	private final LongHashMap chunkMapping = new LongHashMap();

	/**
	 * This may have been intended to be an iterable version of all currently
	 * loaded chunks (MultiplayerChunkCache), with identical contents to
	 * chunkMapping's values. However it is never actually added to.
	 */
	private final List chunkListing = new ArrayList();

	/** Reference to the World object. */
	private final World worldObj;

	public ChunkProviderClient(final World par1World) {
		blankChunk = new EmptyChunk(par1World, 0, 0);
		worldObj = par1World;
	}

	/**
	 * Checks to see if a chunk exists at x, y
	 */
	@Override
	public boolean chunkExists(final int par1, final int par2) {
		return true;
	}

	/**
	 * Unload chunk from ChunkProviderClient's hashmap. Called in response to a
	 * Packet50PreChunk with its mode field set to false
	 */
	public void unloadChunk(final int par1, final int par2) {
		final Chunk var3 = provideChunk(par1, par2);

		if (!var3.isEmpty()) {
			var3.onChunkUnload();
		}

		chunkMapping.remove(ChunkCoordIntPair.chunkXZ2Int(par1, par2));
		chunkListing.remove(var3);
	}

	/**
	 * loads or generates the chunk at the chunk location specified
	 */
	@Override
	public Chunk loadChunk(final int par1, final int par2) {
		final Chunk var3 = new Chunk(worldObj, par1, par2);
		chunkMapping.add(ChunkCoordIntPair.chunkXZ2Int(par1, par2), var3);
		var3.isChunkLoaded = true;
		return var3;
	}

	/**
	 * Will return back a chunk, if it doesn't exist and its not a MP client it
	 * will generates all the blocks for the specified chunk from the map seed
	 * and chunk seed
	 */
	@Override
	public Chunk provideChunk(final int par1, final int par2) {
		final Chunk var3 = (Chunk) chunkMapping.getValueByKey(ChunkCoordIntPair
				.chunkXZ2Int(par1, par2));
		return var3 == null ? blankChunk : var3;
	}

	/**
	 * Two modes of operation: if passed true, save all Chunks in one go. If
	 * passed false, save up to two chunks. Return true if all chunks have been
	 * saved.
	 */
	@Override
	public boolean saveChunks(final boolean par1,
			final IProgressUpdate par2IProgressUpdate) {
		return true;
	}

	@Override
	public void func_104112_b() {
	}

	/**
	 * Unloads chunks that are marked to be unloaded. This is not guaranteed to
	 * unload every such chunk.
	 */
	@Override
	public boolean unloadQueuedChunks() {
		return false;
	}

	/**
	 * Returns if the IChunkProvider supports saving.
	 */
	@Override
	public boolean canSave() {
		return false;
	}

	/**
	 * Populates chunk with ores etc etc
	 */
	@Override
	public void populate(final IChunkProvider par1IChunkProvider,
			final int par2, final int par3) {
	}

	/**
	 * Converts the instance data to a readable string.
	 */
	@Override
	public String makeString() {
		return "MultiplayerChunkCache: " + chunkMapping.getNumHashElements();
	}

	/**
	 * Returns a list of creatures of the specified type that can spawn at the
	 * given location.
	 */
	@Override
	public List getPossibleCreatures(
			final EnumCreatureType par1EnumCreatureType, final int par2,
			final int par3, final int par4) {
		return null;
	}

	/**
	 * Returns the location of the closest structure of the specified type. If
	 * not found returns null.
	 */
	@Override
	public ChunkPosition findClosestStructure(final World par1World,
			final String par2Str, final int par3, final int par4, final int par5) {
		return null;
	}

	@Override
	public int getLoadedChunkCount() {
		return chunkListing.size();
	}

	@Override
	public void recreateStructures(final int par1, final int par2) {
	}
}
