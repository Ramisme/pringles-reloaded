package net.minecraft.src;

import java.util.List;

import net.minecraft.server.MinecraftServer;

public class CommandXP extends CommandBase {
	@Override
	public String getCommandName() {
		return "xp";
	}

	/**
	 * Return the required permission level for this command.
	 */
	@Override
	public int getRequiredPermissionLevel() {
		return 2;
	}

	@Override
	public String getCommandUsage(final ICommandSender par1ICommandSender) {
		return par1ICommandSender.translateString("commands.xp.usage",
				new Object[0]);
	}

	@Override
	public void processCommand(final ICommandSender par1ICommandSender,
			final String[] par2ArrayOfStr) {
		if (par2ArrayOfStr.length <= 0) {
			throw new WrongUsageException("commands.xp.usage", new Object[0]);
		} else {
			String var4 = par2ArrayOfStr[0];
			final boolean var5 = var4.endsWith("l") || var4.endsWith("L");

			if (var5 && var4.length() > 1) {
				var4 = var4.substring(0, var4.length() - 1);
			}

			int var6 = CommandBase.parseInt(par1ICommandSender, var4);
			final boolean var7 = var6 < 0;

			if (var7) {
				var6 *= -1;
			}

			EntityPlayerMP var3;

			if (par2ArrayOfStr.length > 1) {
				var3 = CommandBase.func_82359_c(par1ICommandSender,
						par2ArrayOfStr[1]);
			} else {
				var3 = CommandBase.getCommandSenderAsPlayer(par1ICommandSender);
			}

			if (var5) {
				if (var7) {
					var3.addExperienceLevel(-var6);
					CommandBase.notifyAdmins(
							par1ICommandSender,
							"commands.xp.success.negative.levels",
							new Object[] { Integer.valueOf(var6),
									var3.getEntityName() });
				} else {
					var3.addExperienceLevel(var6);
					CommandBase.notifyAdmins(
							par1ICommandSender,
							"commands.xp.success.levels",
							new Object[] { Integer.valueOf(var6),
									var3.getEntityName() });
				}
			} else {
				if (var7) {
					throw new WrongUsageException(
							"commands.xp.failure.widthdrawXp", new Object[0]);
				}

				var3.addExperience(var6);
				CommandBase.notifyAdmins(
						par1ICommandSender,
						"commands.xp.success",
						new Object[] { Integer.valueOf(var6),
								var3.getEntityName() });
			}
		}
	}

	/**
	 * Adds the strings available in this command to the given list of tab
	 * completion options.
	 */
	@Override
	public List addTabCompletionOptions(
			final ICommandSender par1ICommandSender,
			final String[] par2ArrayOfStr) {
		return par2ArrayOfStr.length == 2 ? CommandBase
				.getListOfStringsMatchingLastWord(par2ArrayOfStr,
						getAllUsernames()) : null;
	}

	protected String[] getAllUsernames() {
		return MinecraftServer.getServer().getAllUsernames();
	}

	/**
	 * Return whether the specified command parameter index is a username
	 * parameter.
	 */
	@Override
	public boolean isUsernameIndex(final String[] par1ArrayOfStr, final int par2) {
		return par2 == 1;
	}
}
