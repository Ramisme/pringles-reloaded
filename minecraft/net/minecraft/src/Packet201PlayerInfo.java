package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet201PlayerInfo extends Packet {
	/** The player's name. */
	public String playerName;

	/** Byte that tells whether the player is connected. */
	public boolean isConnected;
	public int ping;

	public Packet201PlayerInfo() {
	}

	public Packet201PlayerInfo(final String par1Str, final boolean par2,
			final int par3) {
		playerName = par1Str;
		isConnected = par2;
		ping = par3;
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		playerName = Packet.readString(par1DataInputStream, 16);
		isConnected = par1DataInputStream.readByte() != 0;
		ping = par1DataInputStream.readShort();
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		Packet.writeString(playerName, par1DataOutputStream);
		par1DataOutputStream.writeByte(isConnected ? 1 : 0);
		par1DataOutputStream.writeShort(ping);
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handlePlayerInfo(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return playerName.length() + 2 + 1 + 2;
	}
}
