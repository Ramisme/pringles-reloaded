package net.minecraft.src;

import net.minecraft.client.Minecraft;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

public class GuiStats extends GuiScreen {
	private static RenderItem renderItem = new RenderItem();
	protected GuiScreen parentGui;

	/** The title of the stats screen. */
	protected String statsTitle = "Select world";

	/** The slot for general stats. */
	private GuiSlotStatsGeneral slotGeneral;

	/** The slot for item stats. */
	private GuiSlotStatsItem slotItem;

	/** The slot for block stats. */
	private GuiSlotStatsBlock slotBlock;
	private final StatFileWriter statFileWriter;

	/** The currently-selected slot. */
	private GuiSlot selectedSlot = null;

	public GuiStats(final GuiScreen par1GuiScreen,
			final StatFileWriter par2StatFileWriter) {
		parentGui = par1GuiScreen;
		statFileWriter = par2StatFileWriter;
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui() {
		statsTitle = StatCollector.translateToLocal("gui.stats");
		slotGeneral = new GuiSlotStatsGeneral(this);
		slotGeneral.registerScrollButtons(buttonList, 1, 1);
		slotItem = new GuiSlotStatsItem(this);
		slotItem.registerScrollButtons(buttonList, 1, 1);
		slotBlock = new GuiSlotStatsBlock(this);
		slotBlock.registerScrollButtons(buttonList, 1, 1);
		selectedSlot = slotGeneral;
		addHeaderButtons();
	}

	/**
	 * Creates the buttons that appear at the top of the Stats GUI.
	 */
	public void addHeaderButtons() {
		final StringTranslate var1 = StringTranslate.getInstance();
		buttonList.add(new GuiButton(0, width / 2 + 4, height - 28, 150, 20,
				var1.translateKey("gui.done")));
		buttonList.add(new GuiButton(1, width / 2 - 154, height - 52, 100, 20,
				var1.translateKey("stat.generalButton")));
		GuiButton var2;
		buttonList.add(var2 = new GuiButton(2, width / 2 - 46, height - 52,
				100, 20, var1.translateKey("stat.blocksButton")));
		GuiButton var3;
		buttonList.add(var3 = new GuiButton(3, width / 2 + 62, height - 52,
				100, 20, var1.translateKey("stat.itemsButton")));

		if (slotBlock.getSize() == 0) {
			var2.enabled = false;
		}

		if (slotItem.getSize() == 0) {
			var3.enabled = false;
		}
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	@Override
	protected void actionPerformed(final GuiButton par1GuiButton) {
		if (par1GuiButton.enabled) {
			if (par1GuiButton.id == 0) {
				mc.displayGuiScreen(parentGui);
			} else if (par1GuiButton.id == 1) {
				selectedSlot = slotGeneral;
			} else if (par1GuiButton.id == 3) {
				selectedSlot = slotItem;
			} else if (par1GuiButton.id == 2) {
				selectedSlot = slotBlock;
			} else {
				selectedSlot.actionPerformed(par1GuiButton);
			}
		}
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(final int par1, final int par2, final float par3) {
		selectedSlot.drawScreen(par1, par2, par3);
		drawCenteredString(fontRenderer, statsTitle, width / 2, 20, 16777215);
		super.drawScreen(par1, par2, par3);
	}

	/**
	 * Draws the item sprite on top of the background sprite.
	 */
	private void drawItemSprite(final int par1, final int par2, final int par3) {
		drawButtonBackground(par1 + 1, par2 + 1);
		GL11.glEnable(GL12.GL_RESCALE_NORMAL);
		RenderHelper.enableGUIStandardItemLighting();
		GuiStats.renderItem.renderItemIntoGUI(fontRenderer, mc.renderEngine,
				new ItemStack(par3, 1, 0), par1 + 2, par2 + 2);
		RenderHelper.disableStandardItemLighting();
		GL11.glDisable(GL12.GL_RESCALE_NORMAL);
	}

	/**
	 * Draws a gray box that serves as a button background.
	 */
	private void drawButtonBackground(final int par1, final int par2) {
		this.drawSprite(par1, par2, 0, 0);
	}

	/**
	 * Draws a sprite from /gui/slot.png.
	 */
	private void drawSprite(final int par1, final int par2, final int par3,
			final int par4) {
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		mc.renderEngine.bindTexture("/gui/slot.png");
		final Tessellator var9 = Tessellator.instance;
		var9.startDrawingQuads();
		var9.addVertexWithUV(par1 + 0, par2 + 18, zLevel,
				(par3 + 0) * 0.0078125F, (par4 + 18) * 0.0078125F);
		var9.addVertexWithUV(par1 + 18, par2 + 18, zLevel,
				(par3 + 18) * 0.0078125F, (par4 + 18) * 0.0078125F);
		var9.addVertexWithUV(par1 + 18, par2 + 0, zLevel,
				(par3 + 18) * 0.0078125F, (par4 + 0) * 0.0078125F);
		var9.addVertexWithUV(par1 + 0, par2 + 0, zLevel,
				(par3 + 0) * 0.0078125F, (par4 + 0) * 0.0078125F);
		var9.draw();
	}

	static Minecraft getMinecraft(final GuiStats par0GuiStats) {
		return par0GuiStats.mc;
	}

	/**
	 * there are 11 identical methods like this
	 */
	static FontRenderer getFontRenderer1(final GuiStats par0GuiStats) {
		return par0GuiStats.fontRenderer;
	}

	static StatFileWriter getStatsFileWriter(final GuiStats par0GuiStats) {
		return par0GuiStats.statFileWriter;
	}

	/**
	 * there are 11 identical methods like this
	 */
	static FontRenderer getFontRenderer2(final GuiStats par0GuiStats) {
		return par0GuiStats.fontRenderer;
	}

	/**
	 * there are 11 identical methods like this
	 */
	static FontRenderer getFontRenderer3(final GuiStats par0GuiStats) {
		return par0GuiStats.fontRenderer;
	}

	/**
	 * exactly the same as 27141
	 */
	static Minecraft getMinecraft1(final GuiStats par0GuiStats) {
		return par0GuiStats.mc;
	}

	/**
	 * Draws a sprite from /gui/slot.png.
	 */
	static void drawSprite(final GuiStats par0GuiStats, final int par1,
			final int par2, final int par3, final int par4) {
		par0GuiStats.drawSprite(par1, par2, par3, par4);
	}

	/**
	 * exactly the same as 27141 and 27143
	 */
	static Minecraft getMinecraft2(final GuiStats par0GuiStats) {
		return par0GuiStats.mc;
	}

	/**
	 * there are 11 identical methods like this
	 */
	static FontRenderer getFontRenderer4(final GuiStats par0GuiStats) {
		return par0GuiStats.fontRenderer;
	}

	/**
	 * there are 11 identical methods like this
	 */
	static FontRenderer getFontRenderer5(final GuiStats par0GuiStats) {
		return par0GuiStats.fontRenderer;
	}

	/**
	 * there are 11 identical methods like this
	 */
	static FontRenderer getFontRenderer6(final GuiStats par0GuiStats) {
		return par0GuiStats.fontRenderer;
	}

	/**
	 * there are 11 identical methods like this
	 */
	static FontRenderer getFontRenderer7(final GuiStats par0GuiStats) {
		return par0GuiStats.fontRenderer;
	}

	/**
	 * there are 11 identical methods like this
	 */
	static FontRenderer getFontRenderer8(final GuiStats par0GuiStats) {
		return par0GuiStats.fontRenderer;
	}

	static void drawGradientRect(final GuiStats par0GuiStats, final int par1,
			final int par2, final int par3, final int par4, final int par5,
			final int par6) {
		par0GuiStats.drawGradientRect(par1, par2, par3, par4, par5, par6);
	}

	/**
	 * there are 11 identical methods like this
	 */
	static FontRenderer getFontRenderer9(final GuiStats par0GuiStats) {
		return par0GuiStats.fontRenderer;
	}

	/**
	 * there are 11 identical methods like this
	 */
	static FontRenderer getFontRenderer10(final GuiStats par0GuiStats) {
		return par0GuiStats.fontRenderer;
	}

	/**
	 * exactly the same as 27129
	 */
	static void drawGradientRect1(final GuiStats par0GuiStats, final int par1,
			final int par2, final int par3, final int par4, final int par5,
			final int par6) {
		par0GuiStats.drawGradientRect(par1, par2, par3, par4, par5, par6);
	}

	/**
	 * there are 11 identical methods like this
	 */
	static FontRenderer getFontRenderer11(final GuiStats par0GuiStats) {
		return par0GuiStats.fontRenderer;
	}

	/**
	 * Draws the item sprite on top of the background sprite.
	 */
	static void drawItemSprite(final GuiStats par0GuiStats, final int par1,
			final int par2, final int par3) {
		par0GuiStats.drawItemSprite(par1, par2, par3);
	}
}
