package net.minecraft.src;

import net.minecraft.client.Minecraft;

import org.ramisme.pringles.Pringles;

public class EntityPlayerSP extends EntityPlayer {
	public MovementInput movementInput;
	protected Minecraft mc;

	/**
	 * Used to tell if the player pressed forward twice. If this is at 0 and
	 * it's pressed (And they are allowed to sprint, aka enough food on the
	 * ground etc) it sets this to 7. If it's pressed and it's greater than 0
	 * enable sprinting.
	 */
	protected int sprintToggleTimer = 0;

	/** Ticks left before sprinting is disabled. */
	public int sprintingTicksLeft = 0;
	public float renderArmYaw;
	public float renderArmPitch;
	public float prevRenderArmYaw;
	public float prevRenderArmPitch;
	private final MouseFilter field_71162_ch = new MouseFilter();
	private final MouseFilter field_71160_ci = new MouseFilter();
	private final MouseFilter field_71161_cj = new MouseFilter();

	/** The amount of time an entity has been in a Portal */
	public float timeInPortal;

	/** The amount of time an entity has been in a Portal the previous tick */
	public float prevTimeInPortal;

	public EntityPlayerSP(final Minecraft par1Minecraft, final World par2World,
			final Session par3Session, final int par4) {
		super(par2World);
		mc = par1Minecraft;
		dimension = par4;

		if (par3Session != null && par3Session.username != null
				&& par3Session.username.length() > 0) {
			skinUrl = "http://skins.minecraft.net/MinecraftSkins/"
					+ StringUtils.stripControlCodes(par3Session.username)
					+ ".png";
		}

		username = par3Session.username;
	}

	/**
	 * Tries to moves the entity by the passed in displacement. Args: x, y, z
	 */
	@Override
	public void moveEntity(final double par1, final double par3,
			final double par5) {
		super.moveEntity(par1, par3, par5);
	}

	@Override
	public void updateEntityActionState() {
		super.updateEntityActionState();
		moveStrafing = movementInput.moveStrafe;
		moveForward = movementInput.moveForward;
		isJumping = movementInput.jump;
		prevRenderArmYaw = renderArmYaw;
		prevRenderArmPitch = renderArmPitch;
		renderArmPitch = (float) (renderArmPitch + (rotationPitch - renderArmPitch) * 0.5D);
		renderArmYaw = (float) (renderArmYaw + (rotationYaw - renderArmYaw) * 0.5D);
	}

	/**
	 * Returns whether the entity is in a local (client) world
	 */
	@Override
	protected boolean isClientWorld() {
		return true;
	}

	/**
	 * Called frequently so the entity can update its state every tick as
	 * required. For example, zombies and skeletons use this to react to
	 * sunlight and start to burn.
	 */
	@Override
	public void onLivingUpdate() {
		if (sprintingTicksLeft > 0) {
			--sprintingTicksLeft;

			if (sprintingTicksLeft == 0) {
				setSprinting(false);
			}
		}

		if (sprintToggleTimer > 0) {
			--sprintToggleTimer;
		}

		if (mc.playerController.enableEverythingIsScrewedUpMode()) {
			posX = posZ = 0.5D;
			posX = 0.0D;
			posZ = 0.0D;
			rotationYaw = ticksExisted / 12.0F;
			rotationPitch = 10.0F;
			posY = 68.5D;
		} else {
			if (!mc.statFileWriter
					.hasAchievementUnlocked(AchievementList.openInventory)) {
				mc.guiAchievement
						.queueAchievementInformation(AchievementList.openInventory);
			}

			prevTimeInPortal = timeInPortal;

			if (inPortal) {
				if (mc.currentScreen != null) {
					mc.displayGuiScreen((GuiScreen) null);
				}

				if (timeInPortal == 0.0F) {
					mc.sndManager.playSoundFX("portal.trigger", 1.0F,
							rand.nextFloat() * 0.4F + 0.8F);
				}

				timeInPortal += 0.0125F;

				if (timeInPortal >= 1.0F) {
					timeInPortal = 1.0F;
				}

				inPortal = false;
			} else if (this.isPotionActive(Potion.confusion)
					&& getActivePotionEffect(Potion.confusion).getDuration() > 60) {
				timeInPortal += 0.006666667F;

				if (timeInPortal > 1.0F) {
					timeInPortal = 1.0F;
				}
			} else {
				if (timeInPortal > 0.0F) {
					timeInPortal -= 0.05F;
				}

				if (timeInPortal < 0.0F) {
					timeInPortal = 0.0F;
				}
			}

			if (timeUntilPortal > 0) {
				--timeUntilPortal;
			}

			final boolean var1 = movementInput.jump;
			final float var2 = 0.8F;
			final boolean var3 = movementInput.moveForward >= var2;
			movementInput.updatePlayerMoveState();

			if (isUsingItem()) {
				movementInput.moveStrafe *= 0.2F;
				movementInput.moveForward *= 0.2F;
				sprintToggleTimer = 0;
			}

			if (movementInput.sneak && ySize < 0.2F) {
				ySize = 0.2F;
			}

			pushOutOfBlocks(posX - width * 0.35D, boundingBox.minY + 0.5D, posZ
					+ width * 0.35D);
			pushOutOfBlocks(posX - width * 0.35D, boundingBox.minY + 0.5D, posZ
					- width * 0.35D);
			pushOutOfBlocks(posX + width * 0.35D, boundingBox.minY + 0.5D, posZ
					- width * 0.35D);
			pushOutOfBlocks(posX + width * 0.35D, boundingBox.minY + 0.5D, posZ
					+ width * 0.35D);
			final boolean var4 = getFoodStats().getFoodLevel() > 6.0F
					|| capabilities.allowFlying;

			if (onGround && !var3 && movementInput.moveForward >= var2
					&& !isSprinting() && var4 && !isUsingItem()
					&& !this.isPotionActive(Potion.blindness)) {
				if (sprintToggleTimer == 0) {
					sprintToggleTimer = 7;
				} else {
					setSprinting(true);
					sprintToggleTimer = 0;
				}
			}

			if (isSneaking()) {
				sprintToggleTimer = 0;
			}

			if (isSprinting()
					&& (movementInput.moveForward < var2
							|| isCollidedHorizontally || !var4)) {
				setSprinting(false);
			}

			if (capabilities.allowFlying && !var1 && movementInput.jump) {
				if (flyToggleTimer == 0) {
					flyToggleTimer = 7;
				} else {
					capabilities.isFlying = !capabilities.isFlying;
					sendPlayerAbilities();
					flyToggleTimer = 0;
				}
			}

			if (capabilities.isFlying) {
				if (movementInput.sneak) {
					motionY -= 0.15D;
				}

				if (movementInput.jump) {
					motionY += 0.15D;
				}
			}

			super.onLivingUpdate();

			if (onGround && capabilities.isFlying) {
				capabilities.isFlying = false;
				sendPlayerAbilities();
			}
		}
	}

	/**
	 * Gets the player's field of view multiplier. (ex. when flying)
	 */
	public float getFOVMultiplier() {
		float var1 = 1.0F;

		if (capabilities.isFlying) {
			var1 *= 1.1F;
		}

		var1 *= (landMovementFactor * getSpeedModifier() / speedOnGround + 1.0F) / 2.0F;

		if (isUsingItem() && getItemInUse().itemID == Item.bow.itemID) {
			final int var2 = getItemInUseDuration();
			float var3 = var2 / 20.0F;

			if (var3 > 1.0F) {
				var3 = 1.0F;
			} else {
				var3 *= var3;
			}

			var1 *= 1.0F - var3 * 0.15F;
		}

		return var1;
	}

	@Override
	public void updateCloak() {
		cloakUrl = "http://skins.minecraft.net/MinecraftCloaks/"
				+ StringUtils.stripControlCodes(username) + ".png";
	}

	/**
	 * sets current screen to null (used on escape buttons of GUIs)
	 */
	@Override
	public void closeScreen() {
		super.closeScreen();
		mc.displayGuiScreen((GuiScreen) null);
	}

	/**
	 * Displays the GUI for editing a sign. Args: tileEntitySign
	 */
	@Override
	public void displayGUIEditSign(final TileEntity par1TileEntity) {
		if (par1TileEntity instanceof TileEntitySign) {
			mc.displayGuiScreen(new GuiEditSign((TileEntitySign) par1TileEntity));
		} else if (par1TileEntity instanceof TileEntityCommandBlock) {
			mc.displayGuiScreen(new GuiCommandBlock(
					(TileEntityCommandBlock) par1TileEntity));
		}
	}

	/**
	 * Displays the GUI for interacting with a book.
	 */
	@Override
	public void displayGUIBook(final ItemStack par1ItemStack) {
		final Item var2 = par1ItemStack.getItem();

		if (var2 == Item.writtenBook) {
			mc.displayGuiScreen(new GuiScreenBook(this, par1ItemStack, false));
		} else if (var2 == Item.writableBook) {
			mc.displayGuiScreen(new GuiScreenBook(this, par1ItemStack, true));
		}
	}

	/**
	 * Displays the GUI for interacting with a chest inventory. Args:
	 * chestInventory
	 */
	@Override
	public void displayGUIChest(final IInventory par1IInventory) {
		mc.displayGuiScreen(new GuiChest(inventory, par1IInventory));
	}

	@Override
	public void displayGUIHopper(final TileEntityHopper par1TileEntityHopper) {
		mc.displayGuiScreen(new GuiHopper(inventory, par1TileEntityHopper));
	}

	@Override
	public void displayGUIHopperMinecart(
			final EntityMinecartHopper par1EntityMinecartHopper) {
		mc.displayGuiScreen(new GuiHopper(inventory, par1EntityMinecartHopper));
	}

	/**
	 * Displays the crafting GUI for a workbench.
	 */
	@Override
	public void displayGUIWorkbench(final int par1, final int par2,
			final int par3) {
		mc.displayGuiScreen(new GuiCrafting(inventory, worldObj, par1, par2,
				par3));
	}

	@Override
	public void displayGUIEnchantment(final int par1, final int par2,
			final int par3, final String par4Str) {
		mc.displayGuiScreen(new GuiEnchantment(inventory, worldObj, par1, par2,
				par3, par4Str));
	}

	/**
	 * Displays the GUI for interacting with an anvil.
	 */
	@Override
	public void displayGUIAnvil(final int par1, final int par2, final int par3) {
		mc.displayGuiScreen(new GuiRepair(inventory, worldObj, par1, par2, par3));
	}

	/**
	 * Displays the furnace GUI for the passed in furnace entity. Args:
	 * tileEntityFurnace
	 */
	@Override
	public void displayGUIFurnace(final TileEntityFurnace par1TileEntityFurnace) {
		mc.displayGuiScreen(new GuiFurnace(inventory, par1TileEntityFurnace));
	}

	/**
	 * Displays the GUI for interacting with a brewing stand.
	 */
	@Override
	public void displayGUIBrewingStand(
			final TileEntityBrewingStand par1TileEntityBrewingStand) {
		mc.displayGuiScreen(new GuiBrewingStand(inventory,
				par1TileEntityBrewingStand));
	}

	/**
	 * Displays the GUI for interacting with a beacon.
	 */
	@Override
	public void displayGUIBeacon(final TileEntityBeacon par1TileEntityBeacon) {
		mc.displayGuiScreen(new GuiBeacon(inventory, par1TileEntityBeacon));
	}

	/**
	 * Displays the dipsenser GUI for the passed in dispenser entity. Args:
	 * TileEntityDispenser
	 */
	@Override
	public void displayGUIDispenser(
			final TileEntityDispenser par1TileEntityDispenser) {
		mc.displayGuiScreen(new GuiDispenser(inventory, par1TileEntityDispenser));
	}

	@Override
	public void displayGUIMerchant(final IMerchant par1IMerchant,
			final String par2Str) {
		mc.displayGuiScreen(new GuiMerchant(inventory, par1IMerchant, worldObj,
				par2Str));
	}

	/**
	 * Called when the player performs a critical hit on the Entity. Args:
	 * entity that was hit critically
	 */
	@Override
	public void onCriticalHit(final Entity par1Entity) {
		mc.effectRenderer.addEffect(new EntityCrit2FX(mc.theWorld, par1Entity));
	}

	@Override
	public void onEnchantmentCritical(final Entity par1Entity) {
		final EntityCrit2FX var2 = new EntityCrit2FX(mc.theWorld, par1Entity,
				"magicCrit");
		mc.effectRenderer.addEffect(var2);
	}

	/**
	 * Called whenever an item is picked up from walking over it. Args:
	 * pickedUpEntity, stackSize
	 */
	@Override
	public void onItemPickup(final Entity par1Entity, final int par2) {
		mc.effectRenderer.addEffect(new EntityPickupFX(mc.theWorld, par1Entity,
				this, -0.5F));
	}

	/**
	 * Returns if this entity is sneaking.
	 */
	@Override
	public boolean isSneaking() {
		return movementInput.sneak && !sleeping;
	}

	/**
	 * Updates health locally.
	 */
	public void setHealth(final int par1) {
		final int var2 = getHealth() - par1;

		if (var2 <= 0) {
			setEntityHealth(par1);

			if (var2 < 0) {
				hurtResistantTime = maxHurtResistantTime / 2;
			}
		} else {
			lastDamage = var2;
			setEntityHealth(getHealth());
			hurtResistantTime = maxHurtResistantTime;
			damageEntity(DamageSource.generic, var2);
			hurtTime = maxHurtTime = 10;
		}
	}

	/**
	 * Add a chat message to the player
	 */
	@Override
	public void addChatMessage(final String par1Str) {
		mc.ingameGUI.getChatGUI().addTranslatedMessage(par1Str, new Object[0]);
	}

	/**
	 * Adds a value to a statistic field.
	 */
	@Override
	public void addStat(final StatBase par1StatBase, final int par2) {
		if (par1StatBase != null) {
			if (par1StatBase.isAchievement()) {
				final Achievement var3 = (Achievement) par1StatBase;

				if (var3.parentAchievement == null
						|| mc.statFileWriter
								.hasAchievementUnlocked(var3.parentAchievement)) {
					if (!mc.statFileWriter.hasAchievementUnlocked(var3)) {
						mc.guiAchievement.queueTakenAchievement(var3);
					}

					mc.statFileWriter.readStat(par1StatBase, par2);
				}
			} else {
				mc.statFileWriter.readStat(par1StatBase, par2);
			}
		}
	}

	private boolean isBlockTranslucent(final int par1, final int par2,
			final int par3) {
		return worldObj.isBlockNormalCube(par1, par2, par3);
	}

	/**
	 * Adds velocity to push the entity out of blocks at the specified x, y, z
	 * position Args: x, y, z
	 */
	@Override
	protected boolean pushOutOfBlocks(final double par1, final double par3,
			final double par5) {
		// TODO: EntityPlayerSP#pushOutOfBlocks
		if (Pringles.getInstance().getFactory().getModuleManager()
				.getModule("freecam").isEnabled()) {
			return false;
		}

		final int var7 = MathHelper.floor_double(par1);
		final int var8 = MathHelper.floor_double(par3);
		final int var9 = MathHelper.floor_double(par5);
		final double var10 = par1 - var7;
		final double var12 = par5 - var9;

		if (isBlockTranslucent(var7, var8, var9)
				|| isBlockTranslucent(var7, var8 + 1, var9)) {
			final boolean var14 = !isBlockTranslucent(var7 - 1, var8, var9)
					&& !isBlockTranslucent(var7 - 1, var8 + 1, var9);
			final boolean var15 = !isBlockTranslucent(var7 + 1, var8, var9)
					&& !isBlockTranslucent(var7 + 1, var8 + 1, var9);
			final boolean var16 = !isBlockTranslucent(var7, var8, var9 - 1)
					&& !isBlockTranslucent(var7, var8 + 1, var9 - 1);
			final boolean var17 = !isBlockTranslucent(var7, var8, var9 + 1)
					&& !isBlockTranslucent(var7, var8 + 1, var9 + 1);
			byte var18 = -1;
			double var19 = 9999.0D;

			if (var14 && var10 < var19) {
				var19 = var10;
				var18 = 0;
			}

			if (var15 && 1.0D - var10 < var19) {
				var19 = 1.0D - var10;
				var18 = 1;
			}

			if (var16 && var12 < var19) {
				var19 = var12;
				var18 = 4;
			}

			if (var17 && 1.0D - var12 < var19) {
				var19 = 1.0D - var12;
				var18 = 5;
			}

			final float var21 = 0.1F;

			if (var18 == 0) {
				motionX = -var21;
			}

			if (var18 == 1) {
				motionX = var21;
			}

			if (var18 == 4) {
				motionZ = -var21;
			}

			if (var18 == 5) {
				motionZ = var21;
			}
		}

		return false;
	}

	/**
	 * Set sprinting switch for Entity.
	 */
	@Override
	public void setSprinting(final boolean par1) {
		super.setSprinting(par1);
		sprintingTicksLeft = par1 ? 600 : 0;
	}

	/**
	 * Sets the current XP, total XP, and level number.
	 */
	public void setXPStats(final float par1, final int par2, final int par3) {
		experience = par1;
		experienceTotal = par2;
		experienceLevel = par3;
	}

	@Override
	public void sendChatToPlayer(final String par1Str) {
		mc.ingameGUI.getChatGUI().printChatMessage(par1Str);
	}

	/**
	 * Returns true if the command sender is allowed to use the given command.
	 */
	@Override
	public boolean canCommandSenderUseCommand(final int par1,
			final String par2Str) {
		return par1 <= 0;
	}

	/**
	 * Return the position for this command sender.
	 */
	@Override
	public ChunkCoordinates getPlayerCoordinates() {
		return new ChunkCoordinates(MathHelper.floor_double(posX + 0.5D),
				MathHelper.floor_double(posY + 0.5D),
				MathHelper.floor_double(posZ + 0.5D));
	}

	/**
	 * Returns the item that this EntityLiving is holding, if any.
	 */
	@Override
	public ItemStack getHeldItem() {
		return inventory.getCurrentItem();
	}

	@Override
	public void playSound(final String par1Str, final float par2,
			final float par3) {
		worldObj.playSound(posX, posY - yOffset, posZ, par1Str, par2, par3,
				false);
	}
}
