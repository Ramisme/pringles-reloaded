package net.minecraft.src;

import argo.jdom.JdomParser;
import argo.jdom.JsonRootNode;
import argo.saj.InvalidSyntaxException;

public class McoServerAddress extends ValueObject {
	public String field_96417_a;

	public static McoServerAddress func_98162_a(final String par0Str) {
		final McoServerAddress var1 = new McoServerAddress();

		try {
			final JsonRootNode var2 = new JdomParser().parse(par0Str);
			var1.field_96417_a = var2
					.getStringValue(new Object[] { "address" });
		} catch (final InvalidSyntaxException var3) {
			;
		} catch (final IllegalArgumentException var4) {
			;
		}

		return var1;
	}
}
