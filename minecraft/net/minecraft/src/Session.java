package net.minecraft.src;

public class Session {
	public String username;
	public String sessionId;

	public Session(final String par1Str, final String par2Str) {
		username = par1Str;
		sessionId = par2Str;
	}
}
