package net.minecraft.src;

import java.util.concurrent.Callable;

import net.minecraft.client.Minecraft;

public class CallableParticleScreenName implements Callable {
	final Minecraft theMinecraft;

	public CallableParticleScreenName(final Minecraft par1Minecraft) {
		theMinecraft = par1Minecraft;
	}

	public String callParticleScreenName() {
		return theMinecraft.currentScreen.getClass().getCanonicalName();
	}

	@Override
	public Object call() {
		return callParticleScreenName();
	}
}
