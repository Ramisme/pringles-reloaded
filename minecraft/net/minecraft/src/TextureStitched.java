package net.minecraft.src;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;

public class TextureStitched implements Icon {
	private final String textureName;

	/** texture sheet containing this texture */
	protected Texture textureSheet;
	protected List textureList;
	private List listAnimationTuples;
	protected boolean rotated;

	/** x position of this icon on the texture sheet in pixels */
	protected int originX;

	/** y position of this icon on the texture sheet in pixels */
	protected int originY;

	/** width of this icon in pixels */
	private int width;

	/** height of this icon in pixels */
	private int height;
	private float minU;
	private float maxU;
	private float minV;
	private float maxV;
	protected int frameCounter = 0;
	protected int tickCounter = 0;
	private int indexInMap = -1;
	public float baseU;
	public float baseV;
	public Texture tileTexture = null;
	private int currentAnimationIndex = -1;

	public static TextureStitched makeTextureStitched(final String par0Str) {
		return "clock".equals(par0Str) ? new TextureClock() : "compass"
				.equals(par0Str) ? new TextureCompass() : new TextureStitched(
				par0Str);
	}

	protected TextureStitched(final String par1) {
		textureName = par1;
	}

	public void init(final Texture par1Texture, final List par2List,
			final int par3, final int par4, final int par5, final int par6,
			final boolean par7) {
		textureSheet = par1Texture;
		textureList = par2List;
		originX = par3;
		originY = par4;
		width = par5;
		height = par6;
		rotated = par7;
		final float var8 = 0.01F / par1Texture.getWidth();
		final float var9 = 0.01F / par1Texture.getHeight();
		minU = (float) par3 / (float) par1Texture.getWidth() + var8;
		maxU = (float) (par3 + par5) / (float) par1Texture.getWidth() - var8;
		minV = (float) par4 / (float) par1Texture.getHeight() + var9;
		maxV = (float) (par4 + par6) / (float) par1Texture.getHeight() - var9;
		baseU = Math.min(minU, maxU);
		baseV = Math.min(minV, maxV);
	}

	public void copyFrom(final TextureStitched par1TextureStitched) {
		init(par1TextureStitched.textureSheet, par1TextureStitched.textureList,
				par1TextureStitched.originX, par1TextureStitched.originY,
				par1TextureStitched.width, par1TextureStitched.height,
				par1TextureStitched.rotated);
	}

	/**
	 * Returns the X position of this icon on its texture sheet, in pixels.
	 */
	@Override
	public int getOriginX() {
		return originX;
	}

	/**
	 * Returns the Y position of this icon on its texture sheet, in pixels.
	 */
	@Override
	public int getOriginY() {
		return originY;
	}

	/**
	 * Returns the minimum U coordinate to use when rendering with this icon.
	 */
	@Override
	public float getMinU() {
		return minU;
	}

	/**
	 * Returns the maximum U coordinate to use when rendering with this icon.
	 */
	@Override
	public float getMaxU() {
		return maxU;
	}

	/**
	 * Gets a U coordinate on the icon. 0 returns uMin and 16 returns uMax.
	 * Other arguments return in-between values.
	 */
	@Override
	public float getInterpolatedU(final double par1) {
		final float var3 = maxU - minU;
		return minU + var3 * ((float) par1 / 16.0F);
	}

	/**
	 * Returns the minimum V coordinate to use when rendering with this icon.
	 */
	@Override
	public float getMinV() {
		return minV;
	}

	/**
	 * Returns the maximum V coordinate to use when rendering with this icon.
	 */
	@Override
	public float getMaxV() {
		return maxV;
	}

	/**
	 * Gets a V coordinate on the icon. 0 returns vMin and 16 returns vMax.
	 * Other arguments return in-between values.
	 */
	@Override
	public float getInterpolatedV(final double par1) {
		final float var3 = maxV - minV;
		return minV + var3 * ((float) par1 / 16.0F);
	}

	@Override
	public String getIconName() {
		return textureName;
	}

	/**
	 * Returns the width of the texture sheet this icon is on, in pixels.
	 */
	@Override
	public int getSheetWidth() {
		return textureSheet.getWidth();
	}

	/**
	 * Returns the height of the texture sheet this icon is on, in pixels.
	 */
	@Override
	public int getSheetHeight() {
		return textureSheet.getHeight();
	}

	public void updateAnimation() {
		if (listAnimationTuples != null) {
			Tuple var1 = (Tuple) listAnimationTuples.get(frameCounter);
			++tickCounter;

			if (tickCounter >= ((Integer) var1.getSecond()).intValue()) {
				final int var2 = ((Integer) var1.getFirst()).intValue();
				frameCounter = (frameCounter + 1) % listAnimationTuples.size();
				tickCounter = 0;
				var1 = (Tuple) listAnimationTuples.get(frameCounter);
				final int var3 = ((Integer) var1.getFirst()).intValue();

				if (var2 != var3 && var3 >= 0 && var3 < textureList.size()) {
					textureSheet.func_104062_b(originX, originY,
							(Texture) textureList.get(var3));
					currentAnimationIndex = var3;
				}
			}
		} else {
			final int var4 = frameCounter;
			frameCounter = (frameCounter + 1) % textureList.size();

			if (var4 != frameCounter) {
				textureSheet.func_104062_b(originX, originY,
						(Texture) textureList.get(frameCounter));
				currentAnimationIndex = frameCounter;
			}
		}
	}

	public void readAnimationInfo(final BufferedReader par1BufferedReader) {
		final ArrayList var2 = new ArrayList();

		try {
			for (String var3 = par1BufferedReader.readLine(); var3 != null; var3 = par1BufferedReader
					.readLine()) {
				var3 = var3.trim();

				if (var3.length() > 0) {
					final String[] var4 = var3.split(",");
					final String[] var5 = var4;
					final int var6 = var4.length;

					for (int var7 = 0; var7 < var6; ++var7) {
						final String var8 = var5[var7];
						final int var9 = var8.indexOf(42);

						if (var9 > 0) {
							final Integer var10 = new Integer(var8.substring(0,
									var9));
							final Integer var11 = new Integer(
									var8.substring(var9 + 1));
							var2.add(new Tuple(var10, var11));
						} else {
							var2.add(new Tuple(new Integer(var8), Integer
									.valueOf(1)));
						}
					}
				}
			}
		} catch (final Exception var12) {
			System.err.println("Failed to read animation info for "
					+ textureName + ": " + var12.getMessage());
		}

		if (!var2.isEmpty() && var2.size() < 600) {
			listAnimationTuples = var2;
		}
	}

	public Texture getTexture() {
		return textureSheet;
	}

	public int getIndexInMap() {
		return indexInMap;
	}

	public void setIndexInMap(final int var1) {
		indexInMap = var1;
	}

	public void deleteTextures() {
		if (tileTexture != null) {
			tileTexture.deleteTexture();
		}

		if (textureList != null) {
			for (int var1 = 0; var1 < textureList.size(); ++var1) {
				final Texture var2 = (Texture) textureList.get(var1);
				var2.deleteTexture();
			}
		}
	}

	public void createTileTexture() {
		if (tileTexture == null) {
			final Texture var1 = (Texture) textureList.get(0);
			tileTexture = var1.duplicate(3);
			tileTexture.uploadTexture();
		}
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	@Override
	public String toString() {
		return "Icon: " + textureName + ", " + originX + "," + originY + ", "
				+ width + "x" + height + ", " + rotated;
	}

	public void updateTileAnimation() {
		if (tileTexture != null) {
			if (currentAnimationIndex >= 0) {
				final Texture var1 = (Texture) textureList
						.get(currentAnimationIndex);
				tileTexture.bindTexture(0);
				tileTexture.setTextureBound(true);
				tileTexture.copyFrom(0, 0, var1, false);
				tileTexture.setTextureBound(false);
				currentAnimationIndex = -1;
			}
		}
	}

	public boolean loadTexture(final TextureManager var1,
			final ITexturePack var2, final String var3, final String var4,
			final BufferedImage var5, final ArrayList var6) {
		return false;
	}

	public void createAndUploadTextures() {
		Config.dbg("Forge method not implemented: TextureStitched.createAndUploadTextures()");
	}
}
