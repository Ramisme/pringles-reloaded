package net.minecraft.src;

public class DemoWorldManager extends ItemInWorldManager {
	private boolean field_73105_c = false;
	private boolean demoTimeExpired = false;
	private int field_73104_e = 0;
	private int field_73102_f = 0;

	public DemoWorldManager(final World par1World) {
		super(par1World);
	}

	@Override
	public void updateBlockRemoving() {
		super.updateBlockRemoving();
		++field_73102_f;
		final long var1 = theWorld.getTotalWorldTime();
		final long var3 = var1 / 24000L + 1L;

		if (!field_73105_c && field_73102_f > 20) {
			field_73105_c = true;
			thisPlayerMP.playerNetServerHandler
					.sendPacketToPlayer(new Packet70GameEvent(5, 0));
		}

		demoTimeExpired = var1 > 120500L;

		if (demoTimeExpired) {
			++field_73104_e;
		}

		if (var1 % 24000L == 500L) {
			if (var3 <= 6L) {
				thisPlayerMP.sendChatToPlayer(thisPlayerMP.translateString(
						"demo.day." + var3, new Object[0]));
			}
		} else if (var3 == 1L) {
			if (var1 == 100L) {
				thisPlayerMP.playerNetServerHandler
						.sendPacketToPlayer(new Packet70GameEvent(5, 101));
			} else if (var1 == 175L) {
				thisPlayerMP.playerNetServerHandler
						.sendPacketToPlayer(new Packet70GameEvent(5, 102));
			} else if (var1 == 250L) {
				thisPlayerMP.playerNetServerHandler
						.sendPacketToPlayer(new Packet70GameEvent(5, 103));
			}
		} else if (var3 == 5L && var1 % 24000L == 22000L) {
			thisPlayerMP.sendChatToPlayer(thisPlayerMP.translateString(
					"demo.day.warning", new Object[0]));
		}
	}

	/**
	 * Sends a message to the player reminding them that this is the demo
	 * version
	 */
	private void sendDemoReminder() {
		if (field_73104_e > 100) {
			thisPlayerMP.sendChatToPlayer(thisPlayerMP.translateString(
					"demo.reminder", new Object[0]));
			field_73104_e = 0;
		}
	}

	/**
	 * if not creative, it calls destroyBlockInWorldPartially untill the block
	 * is broken first. par4 is the specific side. tryHarvestBlock can also be
	 * the result of this call
	 */
	@Override
	public void onBlockClicked(final int par1, final int par2, final int par3,
			final int par4) {
		if (demoTimeExpired) {
			sendDemoReminder();
		} else {
			super.onBlockClicked(par1, par2, par3, par4);
		}
	}

	@Override
	public void uncheckedTryHarvestBlock(final int par1, final int par2,
			final int par3) {
		if (!demoTimeExpired) {
			super.uncheckedTryHarvestBlock(par1, par2, par3);
		}
	}

	/**
	 * Attempts to harvest a block at the given coordinate
	 */
	@Override
	public boolean tryHarvestBlock(final int par1, final int par2,
			final int par3) {
		return demoTimeExpired ? false : super
				.tryHarvestBlock(par1, par2, par3);
	}

	/**
	 * Attempts to right-click use an item by the given EntityPlayer in the
	 * given World
	 */
	@Override
	public boolean tryUseItem(final EntityPlayer par1EntityPlayer,
			final World par2World, final ItemStack par3ItemStack) {
		if (demoTimeExpired) {
			sendDemoReminder();
			return false;
		} else {
			return super.tryUseItem(par1EntityPlayer, par2World, par3ItemStack);
		}
	}

	/**
	 * Activate the clicked on block, otherwise use the held item. Args: player,
	 * world, itemStack, x, y, z, side, xOffset, yOffset, zOffset
	 */
	@Override
	public boolean activateBlockOrUseItem(final EntityPlayer par1EntityPlayer,
			final World par2World, final ItemStack par3ItemStack,
			final int par4, final int par5, final int par6, final int par7,
			final float par8, final float par9, final float par10) {
		if (demoTimeExpired) {
			sendDemoReminder();
			return false;
		} else {
			return super.activateBlockOrUseItem(par1EntityPlayer, par2World,
					par3ItemStack, par4, par5, par6, par7, par8, par9, par10);
		}
	}
}
