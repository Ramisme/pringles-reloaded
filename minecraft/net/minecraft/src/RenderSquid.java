package net.minecraft.src;

import org.lwjgl.opengl.GL11;

public class RenderSquid extends RenderLiving {
	public RenderSquid(final ModelBase par1ModelBase, final float par2) {
		super(par1ModelBase, par2);
	}

	/**
	 * Renders the Living Squid.
	 */
	public void renderLivingSquid(final EntitySquid par1EntitySquid,
			final double par2, final double par4, final double par6,
			final float par8, final float par9) {
		super.doRenderLiving(par1EntitySquid, par2, par4, par6, par8, par9);
	}

	/**
	 * Rotates the Squid's corpse.
	 */
	protected void rotateSquidsCorpse(final EntitySquid par1EntitySquid,
			final float par2, final float par3, final float par4) {
		final float var5 = par1EntitySquid.prevSquidPitch
				+ (par1EntitySquid.squidPitch - par1EntitySquid.prevSquidPitch)
				* par4;
		final float var6 = par1EntitySquid.prevSquidYaw
				+ (par1EntitySquid.squidYaw - par1EntitySquid.prevSquidYaw)
				* par4;
		GL11.glTranslatef(0.0F, 0.5F, 0.0F);
		GL11.glRotatef(180.0F - par3, 0.0F, 1.0F, 0.0F);
		GL11.glRotatef(var5, 1.0F, 0.0F, 0.0F);
		GL11.glRotatef(var6, 0.0F, 1.0F, 0.0F);
		GL11.glTranslatef(0.0F, -1.2F, 0.0F);
	}

	protected float handleRotationFloat(final EntitySquid par1EntitySquid,
			final float par2) {
		return par1EntitySquid.prevTentacleAngle
				+ (par1EntitySquid.tentacleAngle - par1EntitySquid.prevTentacleAngle)
				* par2;
	}

	/**
	 * Defines what float the third param in setRotationAngles of ModelBase is
	 */
	@Override
	protected float handleRotationFloat(final EntityLiving par1EntityLiving,
			final float par2) {
		return this.handleRotationFloat((EntitySquid) par1EntityLiving, par2);
	}

	@Override
	protected void rotateCorpse(final EntityLiving par1EntityLiving,
			final float par2, final float par3, final float par4) {
		rotateSquidsCorpse((EntitySquid) par1EntityLiving, par2, par3, par4);
	}

	@Override
	public void doRenderLiving(final EntityLiving par1EntityLiving,
			final double par2, final double par4, final double par6,
			final float par8, final float par9) {
		renderLivingSquid((EntitySquid) par1EntityLiving, par2, par4, par6,
				par8, par9);
	}

	/**
	 * Actually renders the given argument. This is a synthetic bridge method,
	 * always casting down its argument and then handing it off to a worker
	 * function which does the actual work. In all probabilty, the class Render
	 * is generic (Render<T extends Entity) and this method has signature public
	 * void doRender(T entity, double d, double d1, double d2, float f, float
	 * f1). But JAD is pre 1.5 so doesn't do that.
	 */
	@Override
	public void doRender(final Entity par1Entity, final double par2,
			final double par4, final double par6, final float par8,
			final float par9) {
		renderLivingSquid((EntitySquid) par1Entity, par2, par4, par6, par8,
				par9);
	}
}
