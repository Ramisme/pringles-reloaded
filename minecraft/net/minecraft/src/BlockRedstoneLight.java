package net.minecraft.src;

import java.util.Random;

public class BlockRedstoneLight extends Block {
	/** Whether this lamp block is the powered version. */
	private final boolean powered;

	public BlockRedstoneLight(final int par1, final boolean par2) {
		super(par1, Material.redstoneLight);
		powered = par2;

		if (par2) {
			setLightValue(1.0F);
		}
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		if (powered) {
			blockIcon = par1IconRegister.registerIcon("redstoneLight_lit");
		} else {
			blockIcon = par1IconRegister.registerIcon("redstoneLight");
		}
	}

	/**
	 * Called whenever the block is added into the world. Args: world, x, y, z
	 */
	@Override
	public void onBlockAdded(final World par1World, final int par2,
			final int par3, final int par4) {
		if (!par1World.isRemote) {
			if (powered
					&& !par1World.isBlockIndirectlyGettingPowered(par2, par3,
							par4)) {
				par1World.scheduleBlockUpdate(par2, par3, par4, blockID, 4);
			} else if (!powered
					&& par1World.isBlockIndirectlyGettingPowered(par2, par3,
							par4)) {
				par1World.setBlock(par2, par3, par4,
						Block.redstoneLampActive.blockID, 0, 2);
			}
		}
	}

	/**
	 * Lets the block know when one of its neighbor changes. Doesn't know which
	 * neighbor changed (coordinates passed are their own) Args: x, y, z,
	 * neighbor blockID
	 */
	@Override
	public void onNeighborBlockChange(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		if (!par1World.isRemote) {
			if (powered
					&& !par1World.isBlockIndirectlyGettingPowered(par2, par3,
							par4)) {
				par1World.scheduleBlockUpdate(par2, par3, par4, blockID, 4);
			} else if (!powered
					&& par1World.isBlockIndirectlyGettingPowered(par2, par3,
							par4)) {
				par1World.setBlock(par2, par3, par4,
						Block.redstoneLampActive.blockID, 0, 2);
			}
		}
	}

	/**
	 * Ticks the block if it's been scheduled
	 */
	@Override
	public void updateTick(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		if (!par1World.isRemote && powered
				&& !par1World.isBlockIndirectlyGettingPowered(par2, par3, par4)) {
			par1World.setBlock(par2, par3, par4,
					Block.redstoneLampIdle.blockID, 0, 2);
		}
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return Block.redstoneLampIdle.blockID;
	}

	/**
	 * only called by clickMiddleMouseButton , and passed to
	 * inventory.setCurrentItem (along with isCreative)
	 */
	@Override
	public int idPicked(final World par1World, final int par2, final int par3,
			final int par4) {
		return Block.redstoneLampIdle.blockID;
	}
}
