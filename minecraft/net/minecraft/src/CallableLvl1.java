package net.minecraft.src;

import java.util.concurrent.Callable;

class CallableLvl1 implements Callable {
	final int field_85179_a;

	/** Reference to the World object. */
	final World theWorld;

	CallableLvl1(final World par1World, final int par2) {
		theWorld = par1World;
		field_85179_a = par2;
	}

	public String getWorldEntitiesAsString() {
		try {
			return String.format(
					"ID #%d (%s // %s)",
					new Object[] {
							Integer.valueOf(field_85179_a),
							Block.blocksList[field_85179_a]
									.getUnlocalizedName(),
							Block.blocksList[field_85179_a].getClass()
									.getCanonicalName() });
		} catch (final Throwable var2) {
			return "ID #" + field_85179_a;
		}
	}

	@Override
	public Object call() {
		return getWorldEntitiesAsString();
	}
}
