package net.minecraft.src;

import java.util.Random;

public class NoiseGeneratorOctaves extends NoiseGenerator {
	/**
	 * Collection of noise generation functions. Output is combined to produce
	 * different octaves of noise.
	 */
	private final NoiseGeneratorPerlin[] generatorCollection;
	private final int octaves;

	public NoiseGeneratorOctaves(final Random par1Random, final int par2) {
		octaves = par2;
		generatorCollection = new NoiseGeneratorPerlin[par2];

		for (int var3 = 0; var3 < par2; ++var3) {
			generatorCollection[var3] = new NoiseGeneratorPerlin(par1Random);
		}
	}

	/**
	 * pars:(par2,3,4=noiseOffset ; so that adjacent noise segments connect)
	 * (pars5,6,7=x,y,zArraySize),(pars8,10,12 = x,y,z noiseScale)
	 */
	public double[] generateNoiseOctaves(double[] par1ArrayOfDouble,
			final int par2, final int par3, final int par4, final int par5,
			final int par6, final int par7, final double par8,
			final double par10, final double par12) {
		if (par1ArrayOfDouble == null) {
			par1ArrayOfDouble = new double[par5 * par6 * par7];
		} else {
			for (int var14 = 0; var14 < par1ArrayOfDouble.length; ++var14) {
				par1ArrayOfDouble[var14] = 0.0D;
			}
		}

		double var27 = 1.0D;

		for (int var16 = 0; var16 < octaves; ++var16) {
			double var17 = par2 * var27 * par8;
			final double var19 = par3 * var27 * par10;
			double var21 = par4 * var27 * par12;
			long var23 = MathHelper.floor_double_long(var17);
			long var25 = MathHelper.floor_double_long(var21);
			var17 -= var23;
			var21 -= var25;
			var23 %= 16777216L;
			var25 %= 16777216L;
			var17 += var23;
			var21 += var25;
			generatorCollection[var16].populateNoiseArray(par1ArrayOfDouble,
					var17, var19, var21, par5, par6, par7, par8 * var27, par10
							* var27, par12 * var27, var27);
			var27 /= 2.0D;
		}

		return par1ArrayOfDouble;
	}

	/**
	 * Bouncer function to the main one with some default arguments.
	 */
	public double[] generateNoiseOctaves(final double[] par1ArrayOfDouble,
			final int par2, final int par3, final int par4, final int par5,
			final double par6, final double par8, final double par10) {
		return this.generateNoiseOctaves(par1ArrayOfDouble, par2, 10, par3,
				par4, 1, par5, par6, 1.0D, par8);
	}
}
