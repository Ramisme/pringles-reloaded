package net.minecraft.src;

import java.util.Random;

public class BlockCrops extends BlockFlower {
	private Icon[] iconArray;

	protected BlockCrops(final int par1) {
		super(par1);
		setTickRandomly(true);
		final float var2 = 0.5F;
		setBlockBounds(0.5F - var2, 0.0F, 0.5F - var2, 0.5F + var2, 0.25F,
				0.5F + var2);
		setCreativeTab((CreativeTabs) null);
		setHardness(0.0F);
		setStepSound(Block.soundGrassFootstep);
		disableStats();
	}

	/**
	 * Gets passed in the blockID of the block below and supposed to return true
	 * if its allowed to grow on the type of blockID passed in. Args: blockID
	 */
	@Override
	protected boolean canThisPlantGrowOnThisBlockID(final int par1) {
		return par1 == Block.tilledField.blockID;
	}

	/**
	 * Ticks the block if it's been scheduled
	 */
	@Override
	public void updateTick(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		super.updateTick(par1World, par2, par3, par4, par5Random);

		if (par1World.getBlockLightValue(par2, par3 + 1, par4) >= 9) {
			int var6 = par1World.getBlockMetadata(par2, par3, par4);

			if (var6 < 7) {
				final float var7 = getGrowthRate(par1World, par2, par3, par4);

				if (par5Random.nextInt((int) (25.0F / var7) + 1) == 0) {
					++var6;
					par1World.setBlockMetadataWithNotify(par2, par3, par4,
							var6, 2);
				}
			}
		}
	}

	/**
	 * Apply bonemeal to the crops.
	 */
	public void fertilize(final World par1World, final int par2,
			final int par3, final int par4) {
		int var5 = par1World.getBlockMetadata(par2, par3, par4)
				+ MathHelper.getRandomIntegerInRange(par1World.rand, 2, 5);

		if (var5 > 7) {
			var5 = 7;
		}

		par1World.setBlockMetadataWithNotify(par2, par3, par4, var5, 2);
	}

	/**
	 * Gets the growth rate for the crop. Setup to encourage rows by halving
	 * growth rate if there is diagonals, crops on different sides that aren't
	 * opposing, and by adding growth for every crop next to this one (and for
	 * crop below this one). Args: x, y, z
	 */
	private float getGrowthRate(final World par1World, final int par2,
			final int par3, final int par4) {
		float var5 = 1.0F;
		final int var6 = par1World.getBlockId(par2, par3, par4 - 1);
		final int var7 = par1World.getBlockId(par2, par3, par4 + 1);
		final int var8 = par1World.getBlockId(par2 - 1, par3, par4);
		final int var9 = par1World.getBlockId(par2 + 1, par3, par4);
		final int var10 = par1World.getBlockId(par2 - 1, par3, par4 - 1);
		final int var11 = par1World.getBlockId(par2 + 1, par3, par4 - 1);
		final int var12 = par1World.getBlockId(par2 + 1, par3, par4 + 1);
		final int var13 = par1World.getBlockId(par2 - 1, par3, par4 + 1);
		final boolean var14 = var8 == blockID || var9 == blockID;
		final boolean var15 = var6 == blockID || var7 == blockID;
		final boolean var16 = var10 == blockID || var11 == blockID
				|| var12 == blockID || var13 == blockID;

		for (int var17 = par2 - 1; var17 <= par2 + 1; ++var17) {
			for (int var18 = par4 - 1; var18 <= par4 + 1; ++var18) {
				final int var19 = par1World.getBlockId(var17, par3 - 1, var18);
				float var20 = 0.0F;

				if (var19 == Block.tilledField.blockID) {
					var20 = 1.0F;

					if (par1World.getBlockMetadata(var17, par3 - 1, var18) > 0) {
						var20 = 3.0F;
					}
				}

				if (var17 != par2 || var18 != par4) {
					var20 /= 4.0F;
				}

				var5 += var20;
			}
		}

		if (var16 || var14 && var15) {
			var5 /= 2.0F;
		}

		return var5;
	}

	/**
	 * From the specified side and block metadata retrieves the blocks texture.
	 * Args: side, metadata
	 */
	@Override
	public Icon getIcon(final int par1, int par2) {
		if (par2 < 0 || par2 > 7) {
			par2 = 7;
		}

		return iconArray[par2];
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return 6;
	}

	/**
	 * Generate a seed ItemStack for this crop.
	 */
	protected int getSeedItem() {
		return Item.seeds.itemID;
	}

	/**
	 * Generate a crop produce ItemStack for this crop.
	 */
	protected int getCropItem() {
		return Item.wheat.itemID;
	}

	/**
	 * Drops the block items with a specified chance of dropping the specified
	 * items
	 */
	@Override
	public void dropBlockAsItemWithChance(final World par1World,
			final int par2, final int par3, final int par4, final int par5,
			final float par6, final int par7) {
		super.dropBlockAsItemWithChance(par1World, par2, par3, par4, par5,
				par6, 0);

		if (!par1World.isRemote) {
			if (par5 >= 7) {
				final int var8 = 3 + par7;

				for (int var9 = 0; var9 < var8; ++var9) {
					if (par1World.rand.nextInt(15) <= par5) {
						dropBlockAsItem_do(par1World, par2, par3, par4,
								new ItemStack(getSeedItem(), 1, 0));
					}
				}
			}
		}
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return par1 == 7 ? getCropItem() : getSeedItem();
	}

	/**
	 * Returns the quantity of items to drop on block destruction.
	 */
	@Override
	public int quantityDropped(final Random par1Random) {
		return 1;
	}

	/**
	 * only called by clickMiddleMouseButton , and passed to
	 * inventory.setCurrentItem (along with isCreative)
	 */
	@Override
	public int idPicked(final World par1World, final int par2, final int par3,
			final int par4) {
		return getSeedItem();
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		iconArray = new Icon[8];

		for (int var2 = 0; var2 < iconArray.length; ++var2) {
			iconArray[var2] = par1IconRegister.registerIcon("crops_" + var2);
		}
	}
}
