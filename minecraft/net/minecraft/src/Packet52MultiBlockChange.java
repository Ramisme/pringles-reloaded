package net.minecraft.src;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet52MultiBlockChange extends Packet {
	/** Chunk X position. */
	public int xPosition;

	/** Chunk Z position. */
	public int zPosition;

	/** The metadata for each block changed. */
	public byte[] metadataArray;

	/** The size of the arrays. */
	public int size;
	private static byte[] field_73449_e = new byte[0];

	public Packet52MultiBlockChange() {
		isChunkDataPacket = true;
	}

	public Packet52MultiBlockChange(final int par1, final int par2,
			final short[] par3ArrayOfShort, final int par4,
			final World par5World) {
		isChunkDataPacket = true;
		xPosition = par1;
		zPosition = par2;
		size = par4;
		final int var6 = 4 * par4;
		final Chunk var7 = par5World.getChunkFromChunkCoords(par1, par2);

		try {
			if (par4 >= 64) {
				field_98193_m
						.logInfo("ChunkTilesUpdatePacket compress " + par4);

				if (Packet52MultiBlockChange.field_73449_e.length < var6) {
					Packet52MultiBlockChange.field_73449_e = new byte[var6];
				}
			} else {
				final ByteArrayOutputStream var8 = new ByteArrayOutputStream(
						var6);
				final DataOutputStream var9 = new DataOutputStream(var8);

				for (int var10 = 0; var10 < par4; ++var10) {
					final int var11 = par3ArrayOfShort[var10] >> 12 & 15;
					final int var12 = par3ArrayOfShort[var10] >> 8 & 15;
					final int var13 = par3ArrayOfShort[var10] & 255;
					var9.writeShort(par3ArrayOfShort[var10]);
					var9.writeShort((short) ((var7.getBlockID(var11, var13,
							var12) & 4095) << 4 | var7.getBlockMetadata(var11,
							var13, var12) & 15));
				}

				metadataArray = var8.toByteArray();

				if (metadataArray.length != var6) {
					throw new RuntimeException("Expected length " + var6
							+ " doesn\'t match received length "
							+ metadataArray.length);
				}
			}
		} catch (final IOException var14) {
			field_98193_m.logSevereException("Couldn\'t create chunk packet",
					var14);
			metadataArray = null;
		}
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		xPosition = par1DataInputStream.readInt();
		zPosition = par1DataInputStream.readInt();
		size = par1DataInputStream.readShort() & 65535;
		final int var2 = par1DataInputStream.readInt();

		if (var2 > 0) {
			metadataArray = new byte[var2];
			par1DataInputStream.readFully(metadataArray);
		}
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		par1DataOutputStream.writeInt(xPosition);
		par1DataOutputStream.writeInt(zPosition);
		par1DataOutputStream.writeShort((short) size);

		if (metadataArray != null) {
			par1DataOutputStream.writeInt(metadataArray.length);
			par1DataOutputStream.write(metadataArray);
		} else {
			par1DataOutputStream.writeInt(0);
		}
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleMultiBlockChange(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 10 + size * 4;
	}
}
