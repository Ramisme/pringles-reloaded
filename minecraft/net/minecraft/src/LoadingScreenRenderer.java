package net.minecraft.src;

import net.minecraft.client.Minecraft;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

public class LoadingScreenRenderer implements IProgressUpdate {
	private String field_73727_a = "";

	/** A reference to the Minecraft object. */
	private final Minecraft mc;

	/**
	 * The text currently displayed (i.e. the argument to the last call to
	 * printText or func_73722_d)
	 */
	private String currentlyDisplayedText = "";
	private long field_73723_d = Minecraft.getSystemTime();
	private boolean field_73724_e = false;

	public LoadingScreenRenderer(final Minecraft par1Minecraft) {
		mc = par1Minecraft;
	}

	/**
	 * this string, followed by "working..." and then the "% complete" are the 3
	 * lines shown. This resets progress to 0, and the WorkingString to
	 * "working...".
	 */
	@Override
	public void resetProgressAndMessage(final String par1Str) {
		field_73724_e = false;
		func_73722_d(par1Str);
	}

	/**
	 * "Saving level", or the loading,or downloading equivelent
	 */
	@Override
	public void displayProgressMessage(final String par1Str) {
		field_73724_e = true;
		func_73722_d(par1Str);
	}

	public void func_73722_d(final String par1Str) {
		currentlyDisplayedText = par1Str;

		if (!mc.running) {
			if (!field_73724_e) {
				throw new MinecraftError();
			}
		} else {
			final ScaledResolution var2 = new ScaledResolution(mc.gameSettings,
					mc.displayWidth, mc.displayHeight);
			GL11.glClear(GL11.GL_DEPTH_BUFFER_BIT);
			GL11.glMatrixMode(GL11.GL_PROJECTION);
			GL11.glLoadIdentity();
			GL11.glOrtho(0.0D, var2.getScaledWidth_double(),
					var2.getScaledHeight_double(), 0.0D, 100.0D, 300.0D);
			GL11.glMatrixMode(GL11.GL_MODELVIEW);
			GL11.glLoadIdentity();
			GL11.glTranslatef(0.0F, 0.0F, -200.0F);
		}
	}

	/**
	 * This is called with "Working..." by resetProgressAndMessage
	 */
	@Override
	public void resetProgresAndWorkingMessage(final String par1Str) {
		if (!mc.running) {
			if (!field_73724_e) {
				throw new MinecraftError();
			}
		} else {
			field_73723_d = 0L;
			field_73727_a = par1Str;
			setLoadingProgress(-1);
			field_73723_d = 0L;
		}
	}

	/**
	 * Updates the progress bar on the loading screen to the specified amount.
	 * Args: loadProgress
	 */
	@Override
	public void setLoadingProgress(final int par1) {
		if (!mc.running) {
			if (!field_73724_e) {
				throw new MinecraftError();
			}
		} else {
			final long var2 = Minecraft.getSystemTime();

			if (var2 - field_73723_d >= 100L) {
				field_73723_d = var2;
				final ScaledResolution var4 = new ScaledResolution(
						mc.gameSettings, mc.displayWidth, mc.displayHeight);
				final int var5 = var4.getScaledWidth();
				final int var6 = var4.getScaledHeight();
				GL11.glClear(GL11.GL_DEPTH_BUFFER_BIT);
				GL11.glMatrixMode(GL11.GL_PROJECTION);
				GL11.glLoadIdentity();
				GL11.glOrtho(0.0D, var4.getScaledWidth_double(),
						var4.getScaledHeight_double(), 0.0D, 100.0D, 300.0D);
				GL11.glMatrixMode(GL11.GL_MODELVIEW);
				GL11.glLoadIdentity();
				GL11.glTranslatef(0.0F, 0.0F, -200.0F);
				GL11.glClear(GL11.GL_COLOR_BUFFER_BIT
						| GL11.GL_DEPTH_BUFFER_BIT);
				final Tessellator var7 = Tessellator.instance;
				mc.renderEngine.bindTexture("/gui/background.png");
				final float var8 = 32.0F;
				var7.startDrawingQuads();
				var7.setColorOpaque_I(4210752);
				var7.addVertexWithUV(0.0D, var6, 0.0D, 0.0D, var6 / var8);
				var7.addVertexWithUV(var5, var6, 0.0D, var5 / var8, var6 / var8);
				var7.addVertexWithUV(var5, 0.0D, 0.0D, var5 / var8, 0.0D);
				var7.addVertexWithUV(0.0D, 0.0D, 0.0D, 0.0D, 0.0D);
				var7.draw();

				if (par1 >= 0) {
					final byte var9 = 100;
					final byte var10 = 2;
					final int var11 = var5 / 2 - var9 / 2;
					final int var12 = var6 / 2 + 16;
					GL11.glDisable(GL11.GL_TEXTURE_2D);
					var7.startDrawingQuads();
					var7.setColorOpaque_I(8421504);
					var7.addVertex(var11, var12, 0.0D);
					var7.addVertex(var11, var12 + var10, 0.0D);
					var7.addVertex(var11 + var9, var12 + var10, 0.0D);
					var7.addVertex(var11 + var9, var12, 0.0D);
					var7.setColorOpaque_I(8454016);
					var7.addVertex(var11, var12, 0.0D);
					var7.addVertex(var11, var12 + var10, 0.0D);
					var7.addVertex(var11 + par1, var12 + var10, 0.0D);
					var7.addVertex(var11 + par1, var12, 0.0D);
					var7.draw();
					GL11.glEnable(GL11.GL_TEXTURE_2D);
				}

				mc.fontRenderer.drawStringWithShadow(currentlyDisplayedText,
						(var5 - mc.fontRenderer
								.getStringWidth(currentlyDisplayedText)) / 2,
						var6 / 2 - 4 - 16, 16777215);
				mc.fontRenderer
						.drawStringWithShadow(field_73727_a,
								(var5 - mc.fontRenderer
										.getStringWidth(field_73727_a)) / 2,
								var6 / 2 - 4 + 8, 16777215);
				Display.update();

				try {
					Thread.yield();
				} catch (final Exception var13) {
					;
				}
			}
		}
	}

	/**
	 * called when there is no more progress to be had, both on completion and
	 * failure
	 */
	@Override
	public void onNoMoreProgress() {
	}
}
