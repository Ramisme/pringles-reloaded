package net.minecraft.src;

import java.util.Random;

class StructureStrongholdStones extends StructurePieceBlockSelector {
	private StructureStrongholdStones() {
	}

	/**
	 * picks Block Ids and Metadata (Silverfish)
	 */
	@Override
	public void selectBlocks(final Random par1Random, final int par2,
			final int par3, final int par4, final boolean par5) {
		if (par5) {
			selectedBlockId = Block.stoneBrick.blockID;
			final float var6 = par1Random.nextFloat();

			if (var6 < 0.2F) {
				selectedBlockMetaData = 2;
			} else if (var6 < 0.5F) {
				selectedBlockMetaData = 1;
			} else if (var6 < 0.55F) {
				selectedBlockId = Block.silverfish.blockID;
				selectedBlockMetaData = 2;
			} else {
				selectedBlockMetaData = 0;
			}
		} else {
			selectedBlockId = 0;
			selectedBlockMetaData = 0;
		}
	}

	StructureStrongholdStones(
			final StructureStrongholdPieceWeight2 par1StructureStrongholdPieceWeight2) {
		this();
	}
}
