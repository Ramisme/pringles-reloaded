package net.minecraft.src;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import net.minecraft.client.Minecraft;

public class ThreadLanServerPing extends Thread {
	private final String motd;

	/** The socket we're using to send packets on. */
	private final DatagramSocket socket;
	private boolean isStopping = true;
	private final String address;

	public ThreadLanServerPing(final String par1Str, final String par2Str)
			throws IOException {
		super("LanServerPinger");
		motd = par1Str;
		address = par2Str;
		setDaemon(true);
		socket = new DatagramSocket();
	}

	@Override
	public void run() {
		final String var1 = ThreadLanServerPing.getPingResponse(motd, address);
		final byte[] var2 = var1.getBytes();

		while (!isInterrupted() && isStopping) {
			try {
				final InetAddress var3 = InetAddress.getByName("224.0.2.60");
				final DatagramPacket var4 = new DatagramPacket(var2,
						var2.length, var3, 4445);
				socket.send(var4);
			} catch (final IOException var6) {
				Minecraft.getMinecraft().getLogAgent()
						.logWarning("LanServerPinger: " + var6.getMessage());
				break;
			}

			try {
				Thread.sleep(1500L);
			} catch (final InterruptedException var5) {
				;
			}
		}
	}

	@Override
	public void interrupt() {
		super.interrupt();
		isStopping = false;
	}

	public static String getPingResponse(final String par0Str,
			final String par1Str) {
		return "[MOTD]" + par0Str + "[/MOTD][AD]" + par1Str + "[/AD]";
	}

	public static String getMotdFromPingResponse(final String par0Str) {
		final int var1 = par0Str.indexOf("[MOTD]");

		if (var1 < 0) {
			return "missing no";
		} else {
			final int var2 = par0Str.indexOf("[/MOTD]",
					var1 + "[MOTD]".length());
			return var2 < var1 ? "missing no" : par0Str.substring(var1
					+ "[MOTD]".length(), var2);
		}
	}

	public static String getAdFromPingResponse(final String par0Str) {
		final int var1 = par0Str.indexOf("[/MOTD]");

		if (var1 < 0) {
			return null;
		} else {
			final int var2 = par0Str.indexOf("[/MOTD]",
					var1 + "[/MOTD]".length());

			if (var2 >= 0) {
				return null;
			} else {
				final int var3 = par0Str.indexOf("[AD]",
						var1 + "[/MOTD]".length());

				if (var3 < 0) {
					return null;
				} else {
					final int var4 = par0Str.indexOf("[/AD]",
							var3 + "[AD]".length());
					return var4 < var3 ? null : par0Str.substring(
							var3 + "[AD]".length(), var4);
				}
			}
		}
	}
}
