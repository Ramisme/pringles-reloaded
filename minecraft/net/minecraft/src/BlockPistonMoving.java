package net.minecraft.src;

import java.util.Random;

public class BlockPistonMoving extends BlockContainer {
	public BlockPistonMoving(final int par1) {
		super(par1, Material.piston);
		setHardness(-1.0F);
	}

	/**
	 * Returns a new instance of a block's tile entity class. Called on placing
	 * the block.
	 */
	@Override
	public TileEntity createNewTileEntity(final World par1World) {
		return null;
	}

	/**
	 * Called whenever the block is added into the world. Args: world, x, y, z
	 */
	@Override
	public void onBlockAdded(final World par1World, final int par2,
			final int par3, final int par4) {
	}

	/**
	 * ejects contained items into the world, and notifies neighbours of an
	 * update, as appropriate
	 */
	@Override
	public void breakBlock(final World par1World, final int par2,
			final int par3, final int par4, final int par5, final int par6) {
		final TileEntity var7 = par1World.getBlockTileEntity(par2, par3, par4);

		if (var7 instanceof TileEntityPiston) {
			((TileEntityPiston) var7).clearPistonTileEntity();
		} else {
			super.breakBlock(par1World, par2, par3, par4, par5, par6);
		}
	}

	/**
	 * Checks to see if its valid to put this block at the specified
	 * coordinates. Args: world, x, y, z
	 */
	@Override
	public boolean canPlaceBlockAt(final World par1World, final int par2,
			final int par3, final int par4) {
		return false;
	}

	/**
	 * checks to see if you can place this block can be placed on that side of a
	 * block: BlockLever overrides
	 */
	@Override
	public boolean canPlaceBlockOnSide(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		return false;
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return -1;
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	/**
	 * Called upon block activation (right click on the block.)
	 */
	@Override
	public boolean onBlockActivated(final World par1World, final int par2,
			final int par3, final int par4,
			final EntityPlayer par5EntityPlayer, final int par6,
			final float par7, final float par8, final float par9) {
		if (!par1World.isRemote
				&& par1World.getBlockTileEntity(par2, par3, par4) == null) {
			par1World.setBlockToAir(par2, par3, par4);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return 0;
	}

	/**
	 * Drops the block items with a specified chance of dropping the specified
	 * items
	 */
	@Override
	public void dropBlockAsItemWithChance(final World par1World,
			final int par2, final int par3, final int par4, final int par5,
			final float par6, final int par7) {
		if (!par1World.isRemote) {
			final TileEntityPiston var8 = getTileEntityAtLocation(par1World,
					par2, par3, par4);

			if (var8 != null) {
				Block.blocksList[var8.getStoredBlockID()]
						.dropBlockAsItem(par1World, par2, par3, par4,
								var8.getBlockMetadata(), 0);
			}
		}
	}

	/**
	 * Lets the block know when one of its neighbor changes. Doesn't know which
	 * neighbor changed (coordinates passed are their own) Args: x, y, z,
	 * neighbor blockID
	 */
	@Override
	public void onNeighborBlockChange(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		if (!par1World.isRemote
				&& par1World.getBlockTileEntity(par2, par3, par4) == null) {
			;
		}
	}

	/**
	 * gets a new TileEntityPiston created with the arguments provided.
	 */
	public static TileEntity getTileEntity(final int par0, final int par1,
			final int par2, final boolean par3, final boolean par4) {
		return new TileEntityPiston(par0, par1, par2, par3, par4);
	}

	/**
	 * Returns a bounding box from the pool of bounding boxes (this means this
	 * box can change after the pool has been cleared to be reused)
	 */
	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(final World par1World,
			final int par2, final int par3, final int par4) {
		final TileEntityPiston var5 = getTileEntityAtLocation(par1World, par2,
				par3, par4);

		if (var5 == null) {
			return null;
		} else {
			float var6 = var5.getProgress(0.0F);

			if (var5.isExtending()) {
				var6 = 1.0F - var6;
			}

			return getAxisAlignedBB(par1World, par2, par3, par4,
					var5.getStoredBlockID(), var6, var5.getPistonOrientation());
		}
	}

	/**
	 * Updates the blocks bounds based on its current state. Args: world, x, y,
	 * z
	 */
	@Override
	public void setBlockBoundsBasedOnState(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		final TileEntityPiston var5 = getTileEntityAtLocation(par1IBlockAccess,
				par2, par3, par4);

		if (var5 != null) {
			final Block var6 = Block.blocksList[var5.getStoredBlockID()];

			if (var6 == null || var6 == this) {
				return;
			}

			var6.setBlockBoundsBasedOnState(par1IBlockAccess, par2, par3, par4);
			float var7 = var5.getProgress(0.0F);

			if (var5.isExtending()) {
				var7 = 1.0F - var7;
			}

			final int var8 = var5.getPistonOrientation();
			minX = var6.getBlockBoundsMinX() - Facing.offsetsXForSide[var8]
					* var7;
			minY = var6.getBlockBoundsMinY() - Facing.offsetsYForSide[var8]
					* var7;
			minZ = var6.getBlockBoundsMinZ() - Facing.offsetsZForSide[var8]
					* var7;
			maxX = var6.getBlockBoundsMaxX() - Facing.offsetsXForSide[var8]
					* var7;
			maxY = var6.getBlockBoundsMaxY() - Facing.offsetsYForSide[var8]
					* var7;
			maxZ = var6.getBlockBoundsMaxZ() - Facing.offsetsZForSide[var8]
					* var7;
		}
	}

	public AxisAlignedBB getAxisAlignedBB(final World par1World,
			final int par2, final int par3, final int par4, final int par5,
			final float par6, final int par7) {
		if (par5 != 0 && par5 != blockID) {
			final AxisAlignedBB var8 = Block.blocksList[par5]
					.getCollisionBoundingBoxFromPool(par1World, par2, par3,
							par4);

			if (var8 == null) {
				return null;
			} else {
				if (Facing.offsetsXForSide[par7] < 0) {
					var8.minX -= Facing.offsetsXForSide[par7] * par6;
				} else {
					var8.maxX -= Facing.offsetsXForSide[par7] * par6;
				}

				if (Facing.offsetsYForSide[par7] < 0) {
					var8.minY -= Facing.offsetsYForSide[par7] * par6;
				} else {
					var8.maxY -= Facing.offsetsYForSide[par7] * par6;
				}

				if (Facing.offsetsZForSide[par7] < 0) {
					var8.minZ -= Facing.offsetsZForSide[par7] * par6;
				} else {
					var8.maxZ -= Facing.offsetsZForSide[par7] * par6;
				}

				return var8;
			}
		} else {
			return null;
		}
	}

	/**
	 * gets the piston tile entity at the specified location
	 */
	private TileEntityPiston getTileEntityAtLocation(
			final IBlockAccess par1IBlockAccess, final int par2,
			final int par3, final int par4) {
		final TileEntity var5 = par1IBlockAccess.getBlockTileEntity(par2, par3,
				par4);
		return var5 instanceof TileEntityPiston ? (TileEntityPiston) var5
				: null;
	}

	/**
	 * only called by clickMiddleMouseButton , and passed to
	 * inventory.setCurrentItem (along with isCreative)
	 */
	@Override
	public int idPicked(final World par1World, final int par2, final int par3,
			final int par4) {
		return 0;
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		blockIcon = par1IconRegister.registerIcon("piston_top");
	}
}
