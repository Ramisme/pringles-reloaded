package net.minecraft.src;

public class PositionImpl implements IPosition {
	protected final double x;
	protected final double y;
	protected final double z;

	public PositionImpl(final double par1, final double par3, final double par5) {
		x = par1;
		y = par3;
		z = par5;
	}

	@Override
	public double getX() {
		return x;
	}

	@Override
	public double getY() {
		return y;
	}

	@Override
	public double getZ() {
		return z;
	}
}
