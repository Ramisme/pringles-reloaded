package net.minecraft.src;

public class CombatEntry {
	private final DamageSource field_94569_a;
	private final int field_94568_c;
	private final String field_94566_e;
	private final float field_94564_f;

	public CombatEntry(final DamageSource par1DamageSource, final int par2,
			final int par3, final int par4, final String par5Str,
			final float par6) {
		field_94569_a = par1DamageSource;
		field_94568_c = par4;
		field_94566_e = par5Str;
		field_94564_f = par6;
	}

	public DamageSource func_94560_a() {
		return field_94569_a;
	}

	public int func_94563_c() {
		return field_94568_c;
	}

	public boolean func_94559_f() {
		return field_94569_a.getEntity() instanceof EntityLiving;
	}

	public String func_94562_g() {
		return field_94566_e;
	}

	public String func_94558_h() {
		return func_94560_a().getEntity() == null ? null : func_94560_a()
				.getEntity().getTranslatedEntityName();
	}

	public float func_94561_i() {
		return field_94569_a == DamageSource.outOfWorld ? Float.MAX_VALUE
				: field_94564_f;
	}
}
