package net.minecraft.src;

import java.awt.Component;

import org.lwjgl.input.Mouse;

public class MouseHelper {
	private final Component windowComponent;
	/** Mouse delta X this frame */
	public int deltaX;

	/** Mouse delta Y this frame */
	public int deltaY;

	public MouseHelper(final Component par1Component,
			final GameSettings par2GameSettings) {
		windowComponent = par1Component;
	}

	/**
	 * Grabs the mouse cursor it doesn't move and isn't seen.
	 */
	public void grabMouseCursor() {
		Mouse.setGrabbed(true);
		deltaX = 0;
		deltaY = 0;
	}

	/**
	 * Ungrabs the mouse cursor so it can be moved and set it to the center of
	 * the screen
	 */
	public void ungrabMouseCursor() {
		int var1 = windowComponent.getWidth();
		int var2 = windowComponent.getHeight();

		if (windowComponent.getParent() != null) {
			var1 = windowComponent.getParent().getWidth();
			var2 = windowComponent.getParent().getHeight();
		}

		Mouse.setCursorPosition(var1 / 2, var2 / 2);
		Mouse.setGrabbed(false);
	}

	public void mouseXYChange() {
		deltaX = Mouse.getDX();
		deltaY = Mouse.getDY();
	}
}
