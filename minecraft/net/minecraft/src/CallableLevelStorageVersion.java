package net.minecraft.src;

import java.util.concurrent.Callable;

class CallableLevelStorageVersion implements Callable {
	final WorldInfo worldInfoInstance;

	CallableLevelStorageVersion(final WorldInfo par1WorldInfo) {
		worldInfoInstance = par1WorldInfo;
	}

	public String callLevelStorageFormat() {
		String var1 = "Unknown?";

		try {
			switch (WorldInfo.getSaveVersion(worldInfoInstance)) {
			case 19132:
				var1 = "McRegion";
				break;

			case 19133:
				var1 = "Anvil";
			}
		} catch (final Throwable var3) {
			;
		}

		return String.format(
				"0x%05X - %s",
				new Object[] {
						Integer.valueOf(WorldInfo
								.getSaveVersion(worldInfoInstance)), var1 });
	}

	@Override
	public Object call() {
		return callLevelStorageFormat();
	}
}
