package net.minecraft.src;

import java.util.List;

import net.minecraft.server.MinecraftServer;

public class CommandGameRule extends CommandBase {
	@Override
	public String getCommandName() {
		return "gamerule";
	}

	/**
	 * Return the required permission level for this command.
	 */
	@Override
	public int getRequiredPermissionLevel() {
		return 2;
	}

	@Override
	public String getCommandUsage(final ICommandSender par1ICommandSender) {
		return par1ICommandSender.translateString("commands.gamerule.usage",
				new Object[0]);
	}

	@Override
	public void processCommand(final ICommandSender par1ICommandSender,
			final String[] par2ArrayOfStr) {
		String var6;

		if (par2ArrayOfStr.length == 2) {
			var6 = par2ArrayOfStr[0];
			final String var7 = par2ArrayOfStr[1];
			final GameRules var8 = getGameRules();

			if (var8.hasRule(var6)) {
				var8.setOrCreateGameRule(var6, var7);
				CommandBase.notifyAdmins(par1ICommandSender,
						"commands.gamerule.success", new Object[0]);
			} else {
				CommandBase.notifyAdmins(par1ICommandSender,
						"commands.gamerule.norule", new Object[] { var6 });
			}
		} else if (par2ArrayOfStr.length == 1) {
			var6 = par2ArrayOfStr[0];
			final GameRules var4 = getGameRules();

			if (var4.hasRule(var6)) {
				final String var5 = var4.getGameRuleStringValue(var6);
				par1ICommandSender.sendChatToPlayer(var6 + " = " + var5);
			} else {
				CommandBase.notifyAdmins(par1ICommandSender,
						"commands.gamerule.norule", new Object[] { var6 });
			}
		} else if (par2ArrayOfStr.length == 0) {
			final GameRules var3 = getGameRules();
			par1ICommandSender.sendChatToPlayer(CommandBase.joinNiceString(var3
					.getRules()));
		} else {
			throw new WrongUsageException("commands.gamerule.usage",
					new Object[0]);
		}
	}

	/**
	 * Adds the strings available in this command to the given list of tab
	 * completion options.
	 */
	@Override
	public List addTabCompletionOptions(
			final ICommandSender par1ICommandSender,
			final String[] par2ArrayOfStr) {
		return par2ArrayOfStr.length == 1 ? CommandBase
				.getListOfStringsMatchingLastWord(par2ArrayOfStr,
						getGameRules().getRules())
				: par2ArrayOfStr.length == 2 ? CommandBase
						.getListOfStringsMatchingLastWord(par2ArrayOfStr,
								new String[] { "true", "false" }) : null;
	}

	/**
	 * Return the game rule set this command should be able to manipulate.
	 */
	private GameRules getGameRules() {
		return MinecraftServer.getServer().worldServerForDimension(0)
				.getGameRules();
	}
}
