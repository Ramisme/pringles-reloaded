package net.minecraft.src;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

class GuiFlatPresetsListSlot extends GuiSlot {
	public int field_82459_a;

	final GuiFlatPresets flatPresetsGui;

	public GuiFlatPresetsListSlot(final GuiFlatPresets par1) {
		super(par1.mc, par1.width, par1.height, 80, par1.height - 37, 24);
		flatPresetsGui = par1;
		field_82459_a = -1;
	}

	private void func_82457_a(final int par1, final int par2, final int par3) {
		func_82456_d(par1 + 1, par2 + 1);
		GL11.glEnable(GL12.GL_RESCALE_NORMAL);
		RenderHelper.enableGUIStandardItemLighting();
		GuiFlatPresets.getPresetIconRenderer().renderItemIntoGUI(
				flatPresetsGui.fontRenderer, flatPresetsGui.mc.renderEngine,
				new ItemStack(par3, 1, 0), par1 + 2, par2 + 2);
		RenderHelper.disableStandardItemLighting();
		GL11.glDisable(GL12.GL_RESCALE_NORMAL);
	}

	private void func_82456_d(final int par1, final int par2) {
		func_82455_b(par1, par2, 0, 0);
	}

	private void func_82455_b(final int par1, final int par2, final int par3,
			final int par4) {
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		flatPresetsGui.mc.renderEngine.bindTexture("/gui/slot.png");
		final Tessellator var9 = Tessellator.instance;
		var9.startDrawingQuads();
		var9.addVertexWithUV(par1 + 0, par2 + 18, flatPresetsGui.zLevel,
				(par3 + 0) * 0.0078125F, (par4 + 18) * 0.0078125F);
		var9.addVertexWithUV(par1 + 18, par2 + 18, flatPresetsGui.zLevel,
				(par3 + 18) * 0.0078125F, (par4 + 18) * 0.0078125F);
		var9.addVertexWithUV(par1 + 18, par2 + 0, flatPresetsGui.zLevel,
				(par3 + 18) * 0.0078125F, (par4 + 0) * 0.0078125F);
		var9.addVertexWithUV(par1 + 0, par2 + 0, flatPresetsGui.zLevel,
				(par3 + 0) * 0.0078125F, (par4 + 0) * 0.0078125F);
		var9.draw();
	}

	/**
	 * Gets the size of the current slot list.
	 */
	@Override
	protected int getSize() {
		return GuiFlatPresets.getPresets().size();
	}

	/**
	 * the element in the slot that was clicked, boolean for wether it was
	 * double clicked or not
	 */
	@Override
	protected void elementClicked(final int par1, final boolean par2) {
		field_82459_a = par1;
		flatPresetsGui.func_82296_g();
		GuiFlatPresets
				.func_82298_b(flatPresetsGui)
				.setText(
						((GuiFlatPresetsItem) GuiFlatPresets
								.getPresets()
								.get(GuiFlatPresets
										.func_82292_a(flatPresetsGui).field_82459_a)).presetData);
	}

	/**
	 * returns true if the element passed in is currently selected
	 */
	@Override
	protected boolean isSelected(final int par1) {
		return par1 == field_82459_a;
	}

	@Override
	protected void drawBackground() {
	}

	@Override
	protected void drawSlot(final int par1, final int par2, final int par3,
			final int par4, final Tessellator par5Tessellator) {
		final GuiFlatPresetsItem var6 = (GuiFlatPresetsItem) GuiFlatPresets
				.getPresets().get(par1);
		func_82457_a(par2, par3, var6.iconId);
		flatPresetsGui.fontRenderer.drawString(var6.presetName, par2 + 18 + 5,
				par3 + 6, 16777215);
	}
}
