package net.minecraft.src;

public class EnchantmentOxygen extends Enchantment {
	public EnchantmentOxygen(final int par1, final int par2) {
		super(par1, par2, EnumEnchantmentType.armor_head);
		setName("oxygen");
	}

	/**
	 * Returns the minimal value of enchantability needed on the enchantment
	 * level passed.
	 */
	@Override
	public int getMinEnchantability(final int par1) {
		return 10 * par1;
	}

	/**
	 * Returns the maximum value of enchantability nedded on the enchantment
	 * level passed.
	 */
	@Override
	public int getMaxEnchantability(final int par1) {
		return getMinEnchantability(par1) + 30;
	}

	/**
	 * Returns the maximum level that the enchantment can have.
	 */
	@Override
	public int getMaxLevel() {
		return 3;
	}
}
