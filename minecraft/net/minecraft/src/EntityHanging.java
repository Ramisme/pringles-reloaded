package net.minecraft.src;

import java.util.Iterator;
import java.util.List;

public abstract class EntityHanging extends Entity {
	private int tickCounter1;
	public int hangingDirection;
	public int xPosition;
	public int yPosition;
	public int zPosition;

	public EntityHanging(final World par1World) {
		super(par1World);
		tickCounter1 = 0;
		hangingDirection = 0;
		yOffset = 0.0F;
		setSize(0.5F, 0.5F);
	}

	public EntityHanging(final World par1World, final int par2, final int par3,
			final int par4, final int par5) {
		this(par1World);
		xPosition = par2;
		yPosition = par3;
		zPosition = par4;
	}

	@Override
	protected void entityInit() {
	}

	public void setDirection(final int par1) {
		hangingDirection = par1;
		prevRotationYaw = rotationYaw = par1 * 90;
		float var2 = func_82329_d();
		float var3 = func_82330_g();
		float var4 = func_82329_d();

		if (par1 != 2 && par1 != 0) {
			var2 = 0.5F;
		} else {
			var4 = 0.5F;
			rotationYaw = prevRotationYaw = Direction.rotateOpposite[par1] * 90;
		}

		var2 /= 32.0F;
		var3 /= 32.0F;
		var4 /= 32.0F;
		float var5 = xPosition + 0.5F;
		float var6 = yPosition + 0.5F;
		float var7 = zPosition + 0.5F;
		final float var8 = 0.5625F;

		if (par1 == 2) {
			var7 -= var8;
		}

		if (par1 == 1) {
			var5 -= var8;
		}

		if (par1 == 0) {
			var7 += var8;
		}

		if (par1 == 3) {
			var5 += var8;
		}

		if (par1 == 2) {
			var5 -= func_70517_b(func_82329_d());
		}

		if (par1 == 1) {
			var7 += func_70517_b(func_82329_d());
		}

		if (par1 == 0) {
			var5 += func_70517_b(func_82329_d());
		}

		if (par1 == 3) {
			var7 -= func_70517_b(func_82329_d());
		}

		var6 += func_70517_b(func_82330_g());
		setPosition(var5, var6, var7);
		final float var9 = -0.03125F;
		boundingBox.setBounds(var5 - var2 - var9, var6 - var3 - var9, var7
				- var4 - var9, var5 + var2 + var9, var6 + var3 + var9, var7
				+ var4 + var9);
	}

	private float func_70517_b(final int par1) {
		return par1 == 32 ? 0.5F : par1 == 64 ? 0.5F : 0.0F;
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate() {
		if (tickCounter1++ == 100 && !worldObj.isRemote) {
			tickCounter1 = 0;

			if (!isDead && !onValidSurface()) {
				setDead();
				dropItemStack();
			}
		}
	}

	/**
	 * checks to make sure painting can be placed there
	 */
	public boolean onValidSurface() {
		if (!worldObj.getCollidingBoundingBoxes(this, boundingBox).isEmpty()) {
			return false;
		} else {
			final int var1 = Math.max(1, func_82329_d() / 16);
			final int var2 = Math.max(1, func_82330_g() / 16);
			int var3 = xPosition;
			int var4 = yPosition;
			int var5 = zPosition;

			if (hangingDirection == 2) {
				var3 = MathHelper.floor_double(posX - func_82329_d() / 32.0F);
			}

			if (hangingDirection == 1) {
				var5 = MathHelper.floor_double(posZ - func_82329_d() / 32.0F);
			}

			if (hangingDirection == 0) {
				var3 = MathHelper.floor_double(posX - func_82329_d() / 32.0F);
			}

			if (hangingDirection == 3) {
				var5 = MathHelper.floor_double(posZ - func_82329_d() / 32.0F);
			}

			var4 = MathHelper.floor_double(posY - func_82330_g() / 32.0F);

			for (int var6 = 0; var6 < var1; ++var6) {
				for (int var7 = 0; var7 < var2; ++var7) {
					Material var8;

					if (hangingDirection != 2 && hangingDirection != 0) {
						var8 = worldObj.getBlockMaterial(xPosition,
								var4 + var7, var5 + var6);
					} else {
						var8 = worldObj.getBlockMaterial(var3 + var6, var4
								+ var7, zPosition);
					}

					if (!var8.isSolid()) {
						return false;
					}
				}
			}

			final List var9 = worldObj.getEntitiesWithinAABBExcludingEntity(
					this, boundingBox);
			final Iterator var10 = var9.iterator();
			Entity var11;

			do {
				if (!var10.hasNext()) {
					return true;
				}

				var11 = (Entity) var10.next();
			} while (!(var11 instanceof EntityHanging));

			return false;
		}
	}

	/**
	 * Returns true if other Entities should be prevented from moving through
	 * this Entity.
	 */
	@Override
	public boolean canBeCollidedWith() {
		return true;
	}

	@Override
	public boolean func_85031_j(final Entity par1Entity) {
		return par1Entity instanceof EntityPlayer ? attackEntityFrom(
				DamageSource.causePlayerDamage((EntityPlayer) par1Entity), 0)
				: false;
	}

	/**
	 * Called when the entity is attacked.
	 */
	@Override
	public boolean attackEntityFrom(final DamageSource par1DamageSource,
			final int par2) {
		if (isEntityInvulnerable()) {
			return false;
		} else {
			if (!isDead && !worldObj.isRemote) {
				setDead();
				setBeenAttacked();
				EntityPlayer var3 = null;

				if (par1DamageSource.getEntity() instanceof EntityPlayer) {
					var3 = (EntityPlayer) par1DamageSource.getEntity();
				}

				if (var3 != null && var3.capabilities.isCreativeMode) {
					return true;
				}

				dropItemStack();
			}

			return true;
		}
	}

	/**
	 * Tries to moves the entity by the passed in displacement. Args: x, y, z
	 */
	@Override
	public void moveEntity(final double par1, final double par3,
			final double par5) {
		if (!worldObj.isRemote && !isDead
				&& par1 * par1 + par3 * par3 + par5 * par5 > 0.0D) {
			setDead();
			dropItemStack();
		}
	}

	/**
	 * Adds to the current velocity of the entity. Args: x, y, z
	 */
	@Override
	public void addVelocity(final double par1, final double par3,
			final double par5) {
		if (!worldObj.isRemote && !isDead
				&& par1 * par1 + par3 * par3 + par5 * par5 > 0.0D) {
			setDead();
			dropItemStack();
		}
	}

	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	@Override
	public void writeEntityToNBT(final NBTTagCompound par1NBTTagCompound) {
		par1NBTTagCompound.setByte("Direction", (byte) hangingDirection);
		par1NBTTagCompound.setInteger("TileX", xPosition);
		par1NBTTagCompound.setInteger("TileY", yPosition);
		par1NBTTagCompound.setInteger("TileZ", zPosition);

		switch (hangingDirection) {
		case 0:
			par1NBTTagCompound.setByte("Dir", (byte) 2);
			break;

		case 1:
			par1NBTTagCompound.setByte("Dir", (byte) 1);
			break;

		case 2:
			par1NBTTagCompound.setByte("Dir", (byte) 0);
			break;

		case 3:
			par1NBTTagCompound.setByte("Dir", (byte) 3);
		}
	}

	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	@Override
	public void readEntityFromNBT(final NBTTagCompound par1NBTTagCompound) {
		if (par1NBTTagCompound.hasKey("Direction")) {
			hangingDirection = par1NBTTagCompound.getByte("Direction");
		} else {
			switch (par1NBTTagCompound.getByte("Dir")) {
			case 0:
				hangingDirection = 2;
				break;

			case 1:
				hangingDirection = 1;
				break;

			case 2:
				hangingDirection = 0;
				break;

			case 3:
				hangingDirection = 3;
			}
		}

		xPosition = par1NBTTagCompound.getInteger("TileX");
		yPosition = par1NBTTagCompound.getInteger("TileY");
		zPosition = par1NBTTagCompound.getInteger("TileZ");
		setDirection(hangingDirection);
	}

	public abstract int func_82329_d();

	public abstract int func_82330_g();

	/**
	 * Drop the item currently on this item frame.
	 */
	public abstract void dropItemStack();
}
