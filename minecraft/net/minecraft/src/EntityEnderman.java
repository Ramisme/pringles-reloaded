package net.minecraft.src;

public class EntityEnderman extends EntityMob {
	private static boolean[] carriableBlocks = new boolean[256];

	/**
	 * Counter to delay the teleportation of an enderman towards the currently
	 * attacked target
	 */
	private int teleportDelay = 0;
	private int field_70826_g = 0;
	private boolean field_104003_g;

	public EntityEnderman(final World par1World) {
		super(par1World);
		texture = "/mob/enderman.png";
		moveSpeed = 0.2F;
		setSize(0.6F, 2.9F);
		stepHeight = 1.0F;
	}

	@Override
	public int getMaxHealth() {
		return 40;
	}

	@Override
	protected void entityInit() {
		super.entityInit();
		dataWatcher.addObject(16, new Byte((byte) 0));
		dataWatcher.addObject(17, new Byte((byte) 0));
		dataWatcher.addObject(18, new Byte((byte) 0));
	}

	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	@Override
	public void writeEntityToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeEntityToNBT(par1NBTTagCompound);
		par1NBTTagCompound.setShort("carried", (short) getCarried());
		par1NBTTagCompound.setShort("carriedData", (short) getCarryingData());
	}

	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	@Override
	public void readEntityFromNBT(final NBTTagCompound par1NBTTagCompound) {
		super.readEntityFromNBT(par1NBTTagCompound);
		setCarried(par1NBTTagCompound.getShort("carried"));
		setCarryingData(par1NBTTagCompound.getShort("carriedData"));
	}

	/**
	 * Finds the closest player within 16 blocks to attack, or null if this
	 * Entity isn't interested in attacking (Animals, Spiders at day, peaceful
	 * PigZombies).
	 */
	@Override
	protected Entity findPlayerToAttack() {
		final EntityPlayer var1 = worldObj.getClosestVulnerablePlayerToEntity(
				this, 64.0D);

		if (var1 != null) {
			if (shouldAttackPlayer(var1)) {
				field_104003_g = true;

				if (field_70826_g == 0) {
					worldObj.playSoundAtEntity(var1, "mob.endermen.stare",
							1.0F, 1.0F);
				}

				if (field_70826_g++ == 5) {
					field_70826_g = 0;
					setScreaming(true);
					return var1;
				}
			} else {
				field_70826_g = 0;
			}
		}

		return null;
	}

	/**
	 * Checks to see if this enderman should be attacking this player
	 */
	private boolean shouldAttackPlayer(final EntityPlayer par1EntityPlayer) {
		final ItemStack var2 = par1EntityPlayer.inventory.armorInventory[3];

		if (var2 != null && var2.itemID == Block.pumpkin.blockID) {
			return false;
		} else {
			final Vec3 var3 = par1EntityPlayer.getLook(1.0F).normalize();
			Vec3 var4 = worldObj.getWorldVec3Pool().getVecFromPool(
					posX - par1EntityPlayer.posX,
					boundingBox.minY
							+ height
							/ 2.0F
							- (par1EntityPlayer.posY + par1EntityPlayer
									.getEyeHeight()),
					posZ - par1EntityPlayer.posZ);
			final double var5 = var4.lengthVector();
			var4 = var4.normalize();
			final double var7 = var3.dotProduct(var4);
			return var7 > 1.0D - 0.025D / var5 ? par1EntityPlayer
					.canEntityBeSeen(this) : false;
		}
	}

	/**
	 * Called frequently so the entity can update its state every tick as
	 * required. For example, zombies and skeletons use this to react to
	 * sunlight and start to burn.
	 */
	@Override
	public void onLivingUpdate() {
		if (isWet()) {
			attackEntityFrom(DamageSource.drown, 1);
		}

		moveSpeed = entityToAttack != null ? 6.5F : 0.3F;
		int var1;

		if (!worldObj.isRemote
				&& worldObj.getGameRules().getGameRuleBooleanValue(
						"mobGriefing")) {
			int var2;
			int var3;
			int var4;

			if (getCarried() == 0) {
				if (rand.nextInt(20) == 0) {
					var1 = MathHelper.floor_double(posX - 2.0D
							+ rand.nextDouble() * 4.0D);
					var2 = MathHelper.floor_double(posY + rand.nextDouble()
							* 3.0D);
					var3 = MathHelper.floor_double(posZ - 2.0D
							+ rand.nextDouble() * 4.0D);
					var4 = worldObj.getBlockId(var1, var2, var3);

					if (EntityEnderman.carriableBlocks[var4]) {
						setCarried(worldObj.getBlockId(var1, var2, var3));
						setCarryingData(worldObj.getBlockMetadata(var1, var2,
								var3));
						worldObj.setBlock(var1, var2, var3, 0);
					}
				}
			} else if (rand.nextInt(2000) == 0) {
				var1 = MathHelper.floor_double(posX - 1.0D + rand.nextDouble()
						* 2.0D);
				var2 = MathHelper.floor_double(posY + rand.nextDouble() * 2.0D);
				var3 = MathHelper.floor_double(posZ - 1.0D + rand.nextDouble()
						* 2.0D);
				var4 = worldObj.getBlockId(var1, var2, var3);
				final int var5 = worldObj.getBlockId(var1, var2 - 1, var3);

				if (var4 == 0 && var5 > 0
						&& Block.blocksList[var5].renderAsNormalBlock()) {
					worldObj.setBlock(var1, var2, var3, getCarried(),
							getCarryingData(), 3);
					setCarried(0);
				}
			}
		}

		for (var1 = 0; var1 < 2; ++var1) {
			worldObj.spawnParticle("portal", posX + (rand.nextDouble() - 0.5D)
					* width, posY + rand.nextDouble() * height - 0.25D, posZ
					+ (rand.nextDouble() - 0.5D) * width,
					(rand.nextDouble() - 0.5D) * 2.0D, -rand.nextDouble(),
					(rand.nextDouble() - 0.5D) * 2.0D);
		}

		if (worldObj.isDaytime() && !worldObj.isRemote) {
			final float var6 = getBrightness(1.0F);

			if (var6 > 0.5F
					&& worldObj.canBlockSeeTheSky(
							MathHelper.floor_double(posX),
							MathHelper.floor_double(posY),
							MathHelper.floor_double(posZ))
					&& rand.nextFloat() * 30.0F < (var6 - 0.4F) * 2.0F) {
				entityToAttack = null;
				setScreaming(false);
				field_104003_g = false;
				teleportRandomly();
			}
		}

		if (isWet() || isBurning()) {
			entityToAttack = null;
			setScreaming(false);
			field_104003_g = false;
			teleportRandomly();
		}

		if (isScreaming() && !field_104003_g && rand.nextInt(100) == 0) {
			setScreaming(false);
		}

		isJumping = false;

		if (entityToAttack != null) {
			faceEntity(entityToAttack, 100.0F, 100.0F);
		}

		if (!worldObj.isRemote && isEntityAlive()) {
			if (entityToAttack != null) {
				if (entityToAttack instanceof EntityPlayer
						&& shouldAttackPlayer((EntityPlayer) entityToAttack)) {
					moveStrafing = moveForward = 0.0F;
					moveSpeed = 0.0F;

					if (entityToAttack.getDistanceSqToEntity(this) < 16.0D) {
						teleportRandomly();
					}

					teleportDelay = 0;
				} else if (entityToAttack.getDistanceSqToEntity(this) > 256.0D
						&& teleportDelay++ >= 30
						&& teleportToEntity(entityToAttack)) {
					teleportDelay = 0;
				}
			} else {
				setScreaming(false);
				teleportDelay = 0;
			}
		}

		super.onLivingUpdate();
	}

	/**
	 * Teleport the enderman to a random nearby position
	 */
	protected boolean teleportRandomly() {
		final double var1 = posX + (rand.nextDouble() - 0.5D) * 64.0D;
		final double var3 = posY + (rand.nextInt(64) - 32);
		final double var5 = posZ + (rand.nextDouble() - 0.5D) * 64.0D;
		return teleportTo(var1, var3, var5);
	}

	/**
	 * Teleport the enderman to another entity
	 */
	protected boolean teleportToEntity(final Entity par1Entity) {
		Vec3 var2 = worldObj.getWorldVec3Pool().getVecFromPool(
				posX - par1Entity.posX,
				boundingBox.minY + height / 2.0F - par1Entity.posY
						+ par1Entity.getEyeHeight(), posZ - par1Entity.posZ);
		var2 = var2.normalize();
		final double var3 = 16.0D;
		final double var5 = posX + (rand.nextDouble() - 0.5D) * 8.0D
				- var2.xCoord * var3;
		final double var7 = posY + (rand.nextInt(16) - 8) - var2.yCoord * var3;
		final double var9 = posZ + (rand.nextDouble() - 0.5D) * 8.0D
				- var2.zCoord * var3;
		return teleportTo(var5, var7, var9);
	}

	/**
	 * Teleport the enderman
	 */
	protected boolean teleportTo(final double par1, final double par3,
			final double par5) {
		final double var7 = posX;
		final double var9 = posY;
		final double var11 = posZ;
		posX = par1;
		posY = par3;
		posZ = par5;
		boolean var13 = false;
		final int var14 = MathHelper.floor_double(posX);
		int var15 = MathHelper.floor_double(posY);
		final int var16 = MathHelper.floor_double(posZ);
		int var18;

		if (worldObj.blockExists(var14, var15, var16)) {
			boolean var17 = false;

			while (!var17 && var15 > 0) {
				var18 = worldObj.getBlockId(var14, var15 - 1, var16);

				if (var18 != 0
						&& Block.blocksList[var18].blockMaterial
								.blocksMovement()) {
					var17 = true;
				} else {
					--posY;
					--var15;
				}
			}

			if (var17) {
				setPosition(posX, posY, posZ);

				if (worldObj.getCollidingBoundingBoxes(this, boundingBox)
						.isEmpty() && !worldObj.isAnyLiquid(boundingBox)) {
					var13 = true;
				}
			}
		}

		if (!var13) {
			setPosition(var7, var9, var11);
			return false;
		} else {
			final short var30 = 128;

			for (var18 = 0; var18 < var30; ++var18) {
				final double var19 = var18 / (var30 - 1.0D);
				final float var21 = (rand.nextFloat() - 0.5F) * 0.2F;
				final float var22 = (rand.nextFloat() - 0.5F) * 0.2F;
				final float var23 = (rand.nextFloat() - 0.5F) * 0.2F;
				final double var24 = var7 + (posX - var7) * var19
						+ (rand.nextDouble() - 0.5D) * width * 2.0D;
				final double var26 = var9 + (posY - var9) * var19
						+ rand.nextDouble() * height;
				final double var28 = var11 + (posZ - var11) * var19
						+ (rand.nextDouble() - 0.5D) * width * 2.0D;
				worldObj.spawnParticle("portal", var24, var26, var28, var21,
						var22, var23);
			}

			worldObj.playSoundEffect(var7, var9, var11, "mob.endermen.portal",
					1.0F, 1.0F);
			playSound("mob.endermen.portal", 1.0F, 1.0F);
			return true;
		}
	}

	/**
	 * Returns the sound this mob makes while it's alive.
	 */
	@Override
	protected String getLivingSound() {
		return isScreaming() ? "mob.endermen.scream" : "mob.endermen.idle";
	}

	/**
	 * Returns the sound this mob makes when it is hurt.
	 */
	@Override
	protected String getHurtSound() {
		return "mob.endermen.hit";
	}

	/**
	 * Returns the sound this mob makes on death.
	 */
	@Override
	protected String getDeathSound() {
		return "mob.endermen.death";
	}

	/**
	 * Returns the item ID for the item the mob drops on death.
	 */
	@Override
	protected int getDropItemId() {
		return Item.enderPearl.itemID;
	}

	/**
	 * Drop 0-2 items of this living's type. @param par1 - Whether this entity
	 * has recently been hit by a player. @param par2 - Level of Looting used to
	 * kill this mob.
	 */
	@Override
	protected void dropFewItems(final boolean par1, final int par2) {
		final int var3 = getDropItemId();

		if (var3 > 0) {
			final int var4 = rand.nextInt(2 + par2);

			for (int var5 = 0; var5 < var4; ++var5) {
				dropItem(var3, 1);
			}
		}
	}

	/**
	 * Set the id of the block an enderman carries
	 */
	public void setCarried(final int par1) {
		dataWatcher.updateObject(16, Byte.valueOf((byte) (par1 & 255)));
	}

	/**
	 * Get the id of the block an enderman carries
	 */
	public int getCarried() {
		return dataWatcher.getWatchableObjectByte(16);
	}

	/**
	 * Set the metadata of the block an enderman carries
	 */
	public void setCarryingData(final int par1) {
		dataWatcher.updateObject(17, Byte.valueOf((byte) (par1 & 255)));
	}

	/**
	 * Get the metadata of the block an enderman carries
	 */
	public int getCarryingData() {
		return dataWatcher.getWatchableObjectByte(17);
	}

	/**
	 * Called when the entity is attacked.
	 */
	@Override
	public boolean attackEntityFrom(final DamageSource par1DamageSource,
			final int par2) {
		if (isEntityInvulnerable()) {
			return false;
		} else {
			setScreaming(true);

			if (par1DamageSource instanceof EntityDamageSource
					&& par1DamageSource.getEntity() instanceof EntityPlayer) {
				field_104003_g = true;
			}

			if (par1DamageSource instanceof EntityDamageSourceIndirect) {
				field_104003_g = false;

				for (int var3 = 0; var3 < 64; ++var3) {
					if (teleportRandomly()) {
						return true;
					}
				}

				return false;
			} else {
				return super.attackEntityFrom(par1DamageSource, par2);
			}
		}
	}

	public boolean isScreaming() {
		return dataWatcher.getWatchableObjectByte(18) > 0;
	}

	public void setScreaming(final boolean par1) {
		dataWatcher.updateObject(18, Byte.valueOf((byte) (par1 ? 1 : 0)));
	}

	/**
	 * Returns the amount of damage a mob should deal.
	 */
	@Override
	public int getAttackStrength(final Entity par1Entity) {
		return 7;
	}

	static {
		EntityEnderman.carriableBlocks[Block.grass.blockID] = true;
		EntityEnderman.carriableBlocks[Block.dirt.blockID] = true;
		EntityEnderman.carriableBlocks[Block.sand.blockID] = true;
		EntityEnderman.carriableBlocks[Block.gravel.blockID] = true;
		EntityEnderman.carriableBlocks[Block.plantYellow.blockID] = true;
		EntityEnderman.carriableBlocks[Block.plantRed.blockID] = true;
		EntityEnderman.carriableBlocks[Block.mushroomBrown.blockID] = true;
		EntityEnderman.carriableBlocks[Block.mushroomRed.blockID] = true;
		EntityEnderman.carriableBlocks[Block.tnt.blockID] = true;
		EntityEnderman.carriableBlocks[Block.cactus.blockID] = true;
		EntityEnderman.carriableBlocks[Block.blockClay.blockID] = true;
		EntityEnderman.carriableBlocks[Block.pumpkin.blockID] = true;
		EntityEnderman.carriableBlocks[Block.melon.blockID] = true;
		EntityEnderman.carriableBlocks[Block.mycelium.blockID] = true;
	}
}
