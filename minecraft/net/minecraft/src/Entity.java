package net.minecraft.src;

import java.util.List;
import java.util.Random;
import java.util.UUID;

import net.minecraft.server.MinecraftServer;

import org.ramisme.pringles.Pringles;

public abstract class Entity {
	private static int nextEntityID = 0;
	public int entityId;
	public double renderDistanceWeight;

	/**
	 * Blocks entities from spawning when they do their AABB check to make sure
	 * the spot is clear of entities that can prevent spawning.
	 */
	public boolean preventEntitySpawning;

	/** The entity that is riding this entity */
	public Entity riddenByEntity;

	/** The entity we are currently riding */
	public Entity ridingEntity;
	public boolean field_98038_p;

	/** Reference to the World object. */
	public World worldObj;
	public double prevPosX;
	public double prevPosY;
	public double prevPosZ;

	/** Entity position X */
	public double posX;

	/** Entity position Y */
	public double posY;

	/** Entity position Z */
	public double posZ;

	/** Entity motion X */
	public double motionX;

	/** Entity motion Y */
	public double motionY;

	/** Entity motion Z */
	public double motionZ;

	/** Entity rotation Yaw */
	public float rotationYaw;

	/** Entity rotation Pitch */
	public float rotationPitch;
	public float prevRotationYaw;
	public float prevRotationPitch;

	/** Axis aligned bounding box. */
	public final AxisAlignedBB boundingBox;
	public boolean onGround;

	/**
	 * True if after a move this entity has collided with something on X- or
	 * Z-axis
	 */
	public boolean isCollidedHorizontally;

	/**
	 * True if after a move this entity has collided with something on Y-axis
	 */
	public boolean isCollidedVertically;

	/**
	 * True if after a move this entity has collided with something either
	 * vertically or horizontally
	 */
	public boolean isCollided;
	public boolean velocityChanged;
	protected boolean isInWeb;
	public boolean field_70135_K;

	/**
	 * Gets set by setDead, so this must be the flag whether an Entity is dead
	 * (inactive may be better term)
	 */
	public boolean isDead;
	public float yOffset;

	/** How wide this entity is considered to be */
	public float width;

	/** How high this entity is considered to be */
	public float height;

	/** The previous ticks distance walked multiplied by 0.6 */
	public float prevDistanceWalkedModified;

	/** The distance walked multiplied by 0.6 */
	public float distanceWalkedModified;
	public float distanceWalkedOnStepModified;
	public float fallDistance;

	/**
	 * The distance that has to be exceeded in order to triger a new step sound
	 * and an onEntityWalking event on a block
	 */
	private int nextStepDistance;

	/**
	 * The entity's X coordinate at the previous tick, used to calculate
	 * position during rendering routines
	 */
	public double lastTickPosX;

	/**
	 * The entity's Y coordinate at the previous tick, used to calculate
	 * position during rendering routines
	 */
	public double lastTickPosY;

	/**
	 * The entity's Z coordinate at the previous tick, used to calculate
	 * position during rendering routines
	 */
	public double lastTickPosZ;
	public float ySize;

	/**
	 * How high this entity can step up when running into a block to try to get
	 * over it (currently make note the entity will always step up this amount
	 * and not just the amount needed)
	 */
	public float stepHeight;

	/**
	 * Whether this entity won't clip with collision or not (make note it won't
	 * disable gravity)
	 */
	public boolean noClip;

	/**
	 * Reduces the velocity applied by entity collisions by the specified
	 * percent.
	 */
	public float entityCollisionReduction;
	protected Random rand;

	/** How many ticks has this entity had ran since being alive */
	public int ticksExisted;

	/**
	 * The amount of ticks you have to stand inside of fire before be set on
	 * fire
	 */
	public int fireResistance;
	private int fire;

	/**
	 * Whether this entity is currently inside of water (if it handles water
	 * movement that is)
	 */
	protected boolean inWater;

	/**
	 * Remaining time an entity will be "immune" to further damage after being
	 * hurt.
	 */
	public int hurtResistantTime;
	private boolean firstUpdate;

	/** downloadable location of player's skin */
	public String skinUrl;

	/** downloadable location of player's cloak */
	public String cloakUrl;
	protected boolean isImmuneToFire;
	protected DataWatcher dataWatcher;
	private double entityRiderPitchDelta;
	private double entityRiderYawDelta;

	/** Has this entity been added to the chunk its within */
	public boolean addedToChunk;
	public int chunkCoordX;
	public int chunkCoordY;
	public int chunkCoordZ;
	public int serverPosX;
	public int serverPosY;
	public int serverPosZ;

	/**
	 * Render entity even if it is outside the camera frustum. Only true in
	 * EntityFish for now. Used in RenderGlobal: render if ignoreFrustumCheck or
	 * in frustum.
	 */
	public boolean ignoreFrustumCheck;
	public boolean isAirBorne;
	public int timeUntilPortal;

	/** Whether the entity is inside a Portal */
	protected boolean inPortal;
	protected int field_82153_h;

	/** Which dimension the player is in (-1 = the Nether, 0 = normal world) */
	public int dimension;
	protected int teleportDirection;
	private boolean invulnerable;
	private UUID entityUniqueID;
	public EnumEntitySize myEntitySize;

	public Entity(final World par1World) {
		entityId = Entity.nextEntityID++;
		renderDistanceWeight = 1.0D;
		preventEntitySpawning = false;
		boundingBox = AxisAlignedBB.getBoundingBox(0.0D, 0.0D, 0.0D, 0.0D,
				0.0D, 0.0D);
		onGround = false;
		isCollided = false;
		velocityChanged = false;
		field_70135_K = true;
		isDead = false;
		yOffset = 0.0F;
		width = 0.6F;
		height = 1.8F;
		prevDistanceWalkedModified = 0.0F;
		distanceWalkedModified = 0.0F;
		distanceWalkedOnStepModified = 0.0F;
		fallDistance = 0.0F;
		nextStepDistance = 1;
		ySize = 0.0F;
		stepHeight = 0.0F;
		noClip = false;
		entityCollisionReduction = 0.0F;
		rand = new Random();
		ticksExisted = 0;
		fireResistance = 1;
		fire = 0;
		inWater = false;
		hurtResistantTime = 0;
		firstUpdate = true;
		isImmuneToFire = false;
		dataWatcher = new DataWatcher();
		addedToChunk = false;
		teleportDirection = 0;
		invulnerable = false;
		entityUniqueID = UUID.randomUUID();
		myEntitySize = EnumEntitySize.SIZE_2;
		worldObj = par1World;
		setPosition(0.0D, 0.0D, 0.0D);

		if (par1World != null) {
			dimension = par1World.provider.dimensionId;
		}

		dataWatcher.addObject(0, Byte.valueOf((byte) 0));
		dataWatcher.addObject(1, Short.valueOf((short) 300));
		entityInit();
	}

	protected abstract void entityInit();

	public DataWatcher getDataWatcher() {
		return dataWatcher;
	}

	@Override
	public boolean equals(final Object par1Obj) {
		return par1Obj instanceof Entity ? ((Entity) par1Obj).entityId == entityId
				: false;
	}

	@Override
	public int hashCode() {
		return entityId;
	}

	/**
	 * Keeps moving the entity up so it isn't colliding with blocks and other
	 * requirements for this entity to be spawned (only actually used on players
	 * though its also on Entity)
	 */
	protected void preparePlayerToSpawn() {
		if (worldObj != null) {
			while (posY > 0.0D) {
				setPosition(posX, posY, posZ);

				if (worldObj.getCollidingBoundingBoxes(this, boundingBox)
						.isEmpty()) {
					break;
				}

				++posY;
			}

			motionX = motionY = motionZ = 0.0D;
			rotationPitch = 0.0F;
		}
	}

	/**
	 * Will get destroyed next tick.
	 */
	public void setDead() {
		isDead = true;
	}

	/**
	 * Sets the width and height of the entity. Args: width, height
	 */
	protected void setSize(final float par1, final float par2) {
		if (par1 != width || par2 != height) {
			width = par1;
			height = par2;
			boundingBox.maxX = boundingBox.minX + width;
			boundingBox.maxZ = boundingBox.minZ + width;
			boundingBox.maxY = boundingBox.minY + height;
		}

		final float var3 = par1 % 2.0F;

		if (var3 < 0.375D) {
			myEntitySize = EnumEntitySize.SIZE_1;
		} else if (var3 < 0.75D) {
			myEntitySize = EnumEntitySize.SIZE_2;
		} else if (var3 < 1.0D) {
			myEntitySize = EnumEntitySize.SIZE_3;
		} else if (var3 < 1.375D) {
			myEntitySize = EnumEntitySize.SIZE_4;
		} else if (var3 < 1.75D) {
			myEntitySize = EnumEntitySize.SIZE_5;
		} else {
			myEntitySize = EnumEntitySize.SIZE_6;
		}
	}

	/**
	 * Sets the rotation of the entity
	 */
	protected void setRotation(final float par1, final float par2) {
		rotationYaw = par1 % 360.0F;
		rotationPitch = par2 % 360.0F;
	}

	/**
	 * Sets the x,y,z of the entity from the given parameters. Also seems to set
	 * up a bounding box.
	 */
	public void setPosition(final double par1, final double par3,
			final double par5) {
		posX = par1;
		posY = par3;
		posZ = par5;
		final float var7 = width / 2.0F;
		final float var8 = height;
		boundingBox.setBounds(par1 - var7, par3 - yOffset + ySize, par5 - var7,
				par1 + var7, par3 - yOffset + ySize + var8, par5 + var7);
	}

	/**
	 * Adds par1*0.15 to the entity's yaw, and *subtracts* par2*0.15 from the
	 * pitch. Clamps pitch from -90 to 90. Both arguments in degrees.
	 */
	public void setAngles(final float par1, final float par2) {
		final float var3 = rotationPitch;
		final float var4 = rotationYaw;
		rotationYaw = (float) (rotationYaw + par1 * 0.15D);
		rotationPitch = (float) (rotationPitch - par2 * 0.15D);

		if (rotationPitch < -90.0F) {
			rotationPitch = -90.0F;
		}

		if (rotationPitch > 90.0F) {
			rotationPitch = 90.0F;
		}

		prevRotationPitch += rotationPitch - var3;
		prevRotationYaw += rotationYaw - var4;
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	public void onUpdate() {
		onEntityUpdate();
	}

	/**
	 * Gets called every tick from main Entity class
	 */
	public void onEntityUpdate() {
		worldObj.theProfiler.startSection("entityBaseTick");

		if (ridingEntity != null && ridingEntity.isDead) {
			ridingEntity = null;
		}

		prevDistanceWalkedModified = distanceWalkedModified;
		prevPosX = posX;
		prevPosY = posY;
		prevPosZ = posZ;
		prevRotationPitch = rotationPitch;
		prevRotationYaw = rotationYaw;
		int var2;

		if (!worldObj.isRemote && worldObj instanceof WorldServer) {
			worldObj.theProfiler.startSection("portal");
			final MinecraftServer var1 = ((WorldServer) worldObj)
					.getMinecraftServer();
			var2 = getMaxInPortalTime();

			if (inPortal) {
				if (var1.getAllowNether()) {
					if (ridingEntity == null && field_82153_h++ >= var2) {
						field_82153_h = var2;
						timeUntilPortal = getPortalCooldown();
						byte var3;

						if (worldObj.provider.dimensionId == -1) {
							var3 = 0;
						} else {
							var3 = -1;
						}

						travelToDimension(var3);
					}

					inPortal = false;
				}
			} else {
				if (field_82153_h > 0) {
					field_82153_h -= 4;
				}

				if (field_82153_h < 0) {
					field_82153_h = 0;
				}
			}

			if (timeUntilPortal > 0) {
				--timeUntilPortal;
			}

			worldObj.theProfiler.endSection();
		}

		if (isSprinting() && !isInWater()) {
			final int var5 = MathHelper.floor_double(posX);
			var2 = MathHelper.floor_double(posY - 0.20000000298023224D
					- yOffset);
			final int var6 = MathHelper.floor_double(posZ);
			final int var4 = worldObj.getBlockId(var5, var2, var6);

			if (var4 > 0) {
				worldObj.spawnParticle(
						"tilecrack_" + var4 + "_"
								+ worldObj.getBlockMetadata(var5, var2, var6),
						posX + (rand.nextFloat() - 0.5D) * width,
						boundingBox.minY + 0.1D, posZ
								+ (rand.nextFloat() - 0.5D) * width,
						-motionX * 4.0D, 1.5D, -motionZ * 4.0D);
			}
		}

		handleWaterMovement();

		if (worldObj.isRemote) {
			fire = 0;
		} else if (fire > 0) {
			if (isImmuneToFire) {
				fire -= 4;

				if (fire < 0) {
					fire = 0;
				}
			} else {
				if (fire % 20 == 0) {
					attackEntityFrom(DamageSource.onFire, 1);
				}

				--fire;
			}
		}

		if (handleLavaMovement()) {
			setOnFireFromLava();
			fallDistance *= 0.5F;
		}

		if (posY < -64.0D) {
			kill();
		}

		if (!worldObj.isRemote) {
			setFlag(0, fire > 0);
			setFlag(2, ridingEntity != null);
		}

		firstUpdate = false;
		worldObj.theProfiler.endSection();
	}

	/**
	 * Return the amount of time this entity should stay in a portal before
	 * being transported.
	 */
	public int getMaxInPortalTime() {
		return 0;
	}

	/**
	 * Called whenever the entity is walking inside of lava.
	 */
	protected void setOnFireFromLava() {
		if (!isImmuneToFire) {
			attackEntityFrom(DamageSource.lava, 4);
			setFire(15);
		}
	}

	/**
	 * Sets entity to burn for x amount of seconds, cannot lower amount of
	 * existing fire.
	 */
	public void setFire(final int par1) {
		int var2 = par1 * 20;
		var2 = EnchantmentProtection.func_92093_a(this, var2);

		if (fire < var2) {
			fire = var2;
		}
	}

	/**
	 * Removes fire from entity.
	 */
	public void extinguish() {
		fire = 0;
	}

	/**
	 * sets the dead flag. Used when you fall off the bottom of the world.
	 */
	protected void kill() {
		setDead();
	}

	/**
	 * Checks if the offset position from the entity's current position is
	 * inside of liquid. Args: x, y, z
	 */
	public boolean isOffsetPositionInLiquid(final double par1,
			final double par3, final double par5) {
		final AxisAlignedBB var7 = boundingBox.getOffsetBoundingBox(par1, par3,
				par5);
		final List var8 = worldObj.getCollidingBoundingBoxes(this, var7);
		return !var8.isEmpty() ? false : !worldObj.isAnyLiquid(var7);
	}

	/**
	 * Tries to moves the entity by the passed in displacement. Args: x, y, z
	 */
	public void moveEntity(double par1, double par3, double par5) {
		if (noClip) {
			boundingBox.offset(par1, par3, par5);
			posX = (boundingBox.minX + boundingBox.maxX) / 2.0D;
			posY = boundingBox.minY + yOffset - ySize;
			posZ = (boundingBox.minZ + boundingBox.maxZ) / 2.0D;
		} else {
			worldObj.theProfiler.startSection("move");
			ySize *= 0.4F;
			final double var7 = posX;
			final double var9 = posY;
			final double var11 = posZ;

			if (isInWeb) {
				isInWeb = false;
				par1 *= 0.25D;
				par3 *= 0.05000000074505806D;
				par5 *= 0.25D;
				motionX = 0.0D;
				motionY = 0.0D;
				motionZ = 0.0D;
			}

			double var13 = par1;
			final double var15 = par3;
			double var17 = par5;
			final AxisAlignedBB var19 = boundingBox.copy();
			final boolean var20 = onGround && isSneaking()
					&& this instanceof EntityPlayer;

			if (var20
					|| Pringles.getInstance().getFactory().getModuleManager()
							.getModule("parkour").isEnabled() && onGround) {
				double var21;

				for (var21 = 0.05D; par1 != 0.0D
						&& worldObj.getCollidingBoundingBoxes(
								this,
								boundingBox.getOffsetBoundingBox(par1, -1.0D,
										0.0D)).isEmpty(); var13 = par1) {
					if (par1 < var21 && par1 >= -var21) {
						par1 = 0.0D;
					} else if (par1 > 0.0D) {
						par1 -= var21;
					} else {
						par1 += var21;
					}
				}

				for (; par5 != 0.0D
						&& worldObj.getCollidingBoundingBoxes(
								this,
								boundingBox.getOffsetBoundingBox(0.0D, -1.0D,
										par5)).isEmpty(); var17 = par5) {
					if (par5 < var21 && par5 >= -var21) {
						par5 = 0.0D;
					} else if (par5 > 0.0D) {
						par5 -= var21;
					} else {
						par5 += var21;
					}
				}

				while (par1 != 0.0D
						&& par5 != 0.0D
						&& worldObj.getCollidingBoundingBoxes(
								this,
								boundingBox.getOffsetBoundingBox(par1, -1.0D,
										par5)).isEmpty()) {
					if (par1 < var21 && par1 >= -var21) {
						par1 = 0.0D;
					} else if (par1 > 0.0D) {
						par1 -= var21;
					} else {
						par1 += var21;
					}

					if (par5 < var21 && par5 >= -var21) {
						par5 = 0.0D;
					} else if (par5 > 0.0D) {
						par5 -= var21;
					} else {
						par5 += var21;
					}

					var13 = par1;
					var17 = par5;
				}
			}

			List var35 = worldObj.getCollidingBoundingBoxes(this,
					boundingBox.addCoord(par1, par3, par5));

			for (int var22 = 0; var22 < var35.size(); ++var22) {
				par3 = ((AxisAlignedBB) var35.get(var22)).calculateYOffset(
						boundingBox, par3);
			}

			boundingBox.offset(0.0D, par3, 0.0D);

			if (!field_70135_K && var15 != par3) {
				par5 = 0.0D;
				par3 = 0.0D;
				par1 = 0.0D;
			}

			final boolean var34 = onGround || var15 != par3 && var15 < 0.0D;
			int var23;

			for (var23 = 0; var23 < var35.size(); ++var23) {
				par1 = ((AxisAlignedBB) var35.get(var23)).calculateXOffset(
						boundingBox, par1);
			}

			boundingBox.offset(par1, 0.0D, 0.0D);

			if (!field_70135_K && var13 != par1) {
				par5 = 0.0D;
				par3 = 0.0D;
				par1 = 0.0D;
			}

			for (var23 = 0; var23 < var35.size(); ++var23) {
				par5 = ((AxisAlignedBB) var35.get(var23)).calculateZOffset(
						boundingBox, par5);
			}

			boundingBox.offset(0.0D, 0.0D, par5);

			if (!field_70135_K && var17 != par5) {
				par5 = 0.0D;
				par3 = 0.0D;
				par1 = 0.0D;
			}

			double var25;
			double var27;
			int var30;
			double var36;

			if (stepHeight > 0.0F && var34 && (var20 || ySize < 0.05F)
					&& (var13 != par1 || var17 != par5)) {
				var36 = par1;
				var25 = par3;
				var27 = par5;
				par1 = var13;
				par3 = stepHeight;
				par5 = var17;
				final AxisAlignedBB var29 = boundingBox.copy();
				boundingBox.setBB(var19);
				var35 = worldObj.getCollidingBoundingBoxes(this,
						boundingBox.addCoord(var13, par3, var17));

				for (var30 = 0; var30 < var35.size(); ++var30) {
					par3 = ((AxisAlignedBB) var35.get(var30)).calculateYOffset(
							boundingBox, par3);
				}

				boundingBox.offset(0.0D, par3, 0.0D);

				if (!field_70135_K && var15 != par3) {
					par5 = 0.0D;
					par3 = 0.0D;
					par1 = 0.0D;
				}

				for (var30 = 0; var30 < var35.size(); ++var30) {
					par1 = ((AxisAlignedBB) var35.get(var30)).calculateXOffset(
							boundingBox, par1);
				}

				boundingBox.offset(par1, 0.0D, 0.0D);

				if (!field_70135_K && var13 != par1) {
					par5 = 0.0D;
					par3 = 0.0D;
					par1 = 0.0D;
				}

				for (var30 = 0; var30 < var35.size(); ++var30) {
					par5 = ((AxisAlignedBB) var35.get(var30)).calculateZOffset(
							boundingBox, par5);
				}

				boundingBox.offset(0.0D, 0.0D, par5);

				if (!field_70135_K && var17 != par5) {
					par5 = 0.0D;
					par3 = 0.0D;
					par1 = 0.0D;
				}

				if (!field_70135_K && var15 != par3) {
					par5 = 0.0D;
					par3 = 0.0D;
					par1 = 0.0D;
				} else {
					par3 = -stepHeight;

					for (var30 = 0; var30 < var35.size(); ++var30) {
						par3 = ((AxisAlignedBB) var35.get(var30))
								.calculateYOffset(boundingBox, par3);
					}

					boundingBox.offset(0.0D, par3, 0.0D);
				}

				if (var36 * var36 + var27 * var27 >= par1 * par1 + par5 * par5) {
					par1 = var36;
					par3 = var25;
					par5 = var27;
					boundingBox.setBB(var29);
				}
			}

			worldObj.theProfiler.endSection();
			worldObj.theProfiler.startSection("rest");
			posX = (boundingBox.minX + boundingBox.maxX) / 2.0D;
			posY = boundingBox.minY + yOffset - ySize;
			posZ = (boundingBox.minZ + boundingBox.maxZ) / 2.0D;
			isCollidedHorizontally = var13 != par1 || var17 != par5;
			isCollidedVertically = var15 != par3;
			onGround = var15 != par3 && var15 < 0.0D;
			isCollided = isCollidedHorizontally || isCollidedVertically;
			updateFallState(par3, onGround);

			if (var13 != par1) {
				motionX = 0.0D;
			}

			if (var15 != par3) {
				motionY = 0.0D;
			}

			if (var17 != par5) {
				motionZ = 0.0D;
			}

			var36 = posX - var7;
			var25 = posY - var9;
			var27 = posZ - var11;

			if (canTriggerWalking() && !var20 && ridingEntity == null) {
				final int var37 = MathHelper.floor_double(posX);
				var30 = MathHelper.floor_double(posY - 0.20000000298023224D
						- yOffset);
				final int var31 = MathHelper.floor_double(posZ);
				int var32 = worldObj.getBlockId(var37, var30, var31);

				if (var32 == 0) {
					final int var33 = worldObj.blockGetRenderType(var37,
							var30 - 1, var31);

					if (var33 == 11 || var33 == 32 || var33 == 21) {
						var32 = worldObj.getBlockId(var37, var30 - 1, var31);
					}
				}

				if (var32 != Block.ladder.blockID) {
					var25 = 0.0D;
				}

				distanceWalkedModified = (float) (distanceWalkedModified + MathHelper
						.sqrt_double(var36 * var36 + var27 * var27) * 0.6D);
				distanceWalkedOnStepModified = (float) (distanceWalkedOnStepModified + MathHelper
						.sqrt_double(var36 * var36 + var25 * var25 + var27
								* var27) * 0.6D);

				if (distanceWalkedOnStepModified > nextStepDistance
						&& var32 > 0) {
					nextStepDistance = (int) distanceWalkedOnStepModified + 1;

					if (isInWater()) {
						float var39 = MathHelper.sqrt_double(motionX * motionX
								* 0.20000000298023224D + motionY * motionY
								+ motionZ * motionZ * 0.20000000298023224D) * 0.35F;

						if (var39 > 1.0F) {
							var39 = 1.0F;
						}

						playSound(
								"liquid.swim",
								var39,
								1.0F + (rand.nextFloat() - rand.nextFloat()) * 0.4F);
					}

					playStepSound(var37, var30, var31, var32);
					Block.blocksList[var32].onEntityWalking(worldObj, var37,
							var30, var31, this);
				}
			}

			doBlockCollisions();
			final boolean var38 = isWet();

			if (worldObj.isBoundingBoxBurning(boundingBox.contract(0.001D,
					0.001D, 0.001D))) {
				dealFireDamage(1);

				if (!var38) {
					++fire;

					if (fire == 0) {
						setFire(8);
					}
				}
			} else if (fire <= 0) {
				fire = -fireResistance;
			}

			if (var38 && fire > 0) {
				playSound("random.fizz", 0.7F,
						1.6F + (rand.nextFloat() - rand.nextFloat()) * 0.4F);
				fire = -fireResistance;
			}

			worldObj.theProfiler.endSection();
		}
	}

	/**
	 * Checks for block collisions, and calls the associated onBlockCollided
	 * method for the collided block.
	 */
	protected void doBlockCollisions() {
		final int var1 = MathHelper.floor_double(boundingBox.minX + 0.001D);
		final int var2 = MathHelper.floor_double(boundingBox.minY + 0.001D);
		final int var3 = MathHelper.floor_double(boundingBox.minZ + 0.001D);
		final int var4 = MathHelper.floor_double(boundingBox.maxX - 0.001D);
		final int var5 = MathHelper.floor_double(boundingBox.maxY - 0.001D);
		final int var6 = MathHelper.floor_double(boundingBox.maxZ - 0.001D);

		if (worldObj.checkChunksExist(var1, var2, var3, var4, var5, var6)) {
			for (int var7 = var1; var7 <= var4; ++var7) {
				for (int var8 = var2; var8 <= var5; ++var8) {
					for (int var9 = var3; var9 <= var6; ++var9) {
						final int var10 = worldObj.getBlockId(var7, var8, var9);

						if (var10 > 0) {
							Block.blocksList[var10].onEntityCollidedWithBlock(
									worldObj, var7, var8, var9, this);
						}
					}
				}
			}
		}
	}

	/**
	 * Plays step sound at given x, y, z for the entity
	 */
	protected void playStepSound(final int par1, final int par2,
			final int par3, final int par4) {
		StepSound var5 = Block.blocksList[par4].stepSound;

		if (worldObj.getBlockId(par1, par2 + 1, par3) == Block.snow.blockID) {
			var5 = Block.snow.stepSound;
			playSound(var5.getStepSound(), var5.getVolume() * 0.15F,
					var5.getPitch());
		} else if (!Block.blocksList[par4].blockMaterial.isLiquid()) {
			playSound(var5.getStepSound(), var5.getVolume() * 0.15F,
					var5.getPitch());
		}
	}

	public void playSound(final String par1Str, final float par2,
			final float par3) {
		worldObj.playSoundAtEntity(this, par1Str, par2, par3);
	}

	/**
	 * returns if this entity triggers Block.onEntityWalking on the blocks they
	 * walk on. used for spiders and wolves to prevent them from trampling crops
	 */
	protected boolean canTriggerWalking() {
		return true;
	}

	/**
	 * Takes in the distance the entity has fallen this tick and whether its on
	 * the ground to update the fall distance and deal fall damage if landing on
	 * the ground. Args: distanceFallenThisTick, onGround
	 */
	protected void updateFallState(final double par1, final boolean par3) {
		if (par3) {
			if (fallDistance > 0.0F) {
				fall(fallDistance);
				fallDistance = 0.0F;
			}
		} else if (par1 < 0.0D) {
			fallDistance = (float) (fallDistance - par1);
		}
	}

	/**
	 * returns the bounding box for this entity
	 */
	public AxisAlignedBB getBoundingBox() {
		return null;
	}

	/**
	 * Will deal the specified amount of damage to the entity if the entity
	 * isn't immune to fire damage. Args: amountDamage
	 */
	protected void dealFireDamage(final int par1) {
		if (!isImmuneToFire) {
			attackEntityFrom(DamageSource.inFire, par1);
		}
	}

	public final boolean isImmuneToFire() {
		return isImmuneToFire;
	}

	/**
	 * Called when the mob is falling. Calculates and applies fall damage.
	 */
	protected void fall(final float par1) {
		if (riddenByEntity != null) {
			riddenByEntity.fall(par1);
		}
	}

	/**
	 * Checks if this entity is either in water or on an open air block in rain
	 * (used in wolves).
	 */
	public boolean isWet() {
		return inWater
				|| worldObj.canLightningStrikeAt(MathHelper.floor_double(posX),
						MathHelper.floor_double(posY),
						MathHelper.floor_double(posZ))
				|| worldObj.canLightningStrikeAt(MathHelper.floor_double(posX),
						MathHelper.floor_double(posY + height),
						MathHelper.floor_double(posZ));
	}

	/**
	 * Checks if this entity is inside water (if inWater field is true as a
	 * result of handleWaterMovement() returning true)
	 */
	public boolean isInWater() {
		return inWater;
	}

	/**
	 * Returns if this entity is in water and will end up adding the waters
	 * velocity to the entity
	 */
	public boolean handleWaterMovement() {
		if (worldObj.handleMaterialAcceleration(
				boundingBox.expand(0.0D, -0.4000000059604645D, 0.0D).contract(
						0.001D, 0.001D, 0.001D), Material.water, this)) {
			if (!inWater && !firstUpdate) {
				float var1 = MathHelper.sqrt_double(motionX * motionX
						* 0.20000000298023224D + motionY * motionY + motionZ
						* motionZ * 0.20000000298023224D) * 0.2F;

				if (var1 > 1.0F) {
					var1 = 1.0F;
				}

				playSound("liquid.splash", var1,
						1.0F + (rand.nextFloat() - rand.nextFloat()) * 0.4F);
				final float var2 = MathHelper.floor_double(boundingBox.minY);
				int var3;
				float var4;
				float var5;

				for (var3 = 0; var3 < 1.0F + width * 20.0F; ++var3) {
					var4 = (rand.nextFloat() * 2.0F - 1.0F) * width;
					var5 = (rand.nextFloat() * 2.0F - 1.0F) * width;
					worldObj.spawnParticle("bubble", posX + var4, var2 + 1.0F,
							posZ + var5, motionX, motionY - rand.nextFloat()
									* 0.2F, motionZ);
				}

				for (var3 = 0; var3 < 1.0F + width * 20.0F; ++var3) {
					var4 = (rand.nextFloat() * 2.0F - 1.0F) * width;
					var5 = (rand.nextFloat() * 2.0F - 1.0F) * width;
					worldObj.spawnParticle("splash", posX + var4, var2 + 1.0F,
							posZ + var5, motionX, motionY, motionZ);
				}
			}

			fallDistance = 0.0F;
			inWater = true;
			fire = 0;
		} else {
			inWater = false;
		}

		return inWater;
	}

	/**
	 * Checks if the current block the entity is within of the specified
	 * material type
	 */
	public boolean isInsideOfMaterial(final Material par1Material) {
		final double var2 = posY + getEyeHeight();
		final int var4 = MathHelper.floor_double(posX);
		final int var5 = MathHelper.floor_float(MathHelper.floor_double(var2));
		final int var6 = MathHelper.floor_double(posZ);
		final int var7 = worldObj.getBlockId(var4, var5, var6);

		if (var7 != 0 && Block.blocksList[var7].blockMaterial == par1Material) {
			final float var8 = BlockFluid.getFluidHeightPercent(worldObj
					.getBlockMetadata(var4, var5, var6)) - 0.11111111F;
			final float var9 = var5 + 1 - var8;
			return var2 < var9;
		} else {
			return false;
		}
	}

	public float getEyeHeight() {
		return 0.0F;
	}

	/**
	 * Whether or not the current entity is in lava
	 */
	public boolean handleLavaMovement() {
		return worldObj.isMaterialInBB(boundingBox.expand(
				-0.10000000149011612D, -0.4000000059604645D,
				-0.10000000149011612D), Material.lava);
	}

	/**
	 * Used in both water and by flying objects
	 */
	public void moveFlying(float par1, float par2, final float par3) {
		float var4 = par1 * par1 + par2 * par2;

		if (var4 >= 1.0E-4F) {
			var4 = MathHelper.sqrt_float(var4);

			if (var4 < 1.0F) {
				var4 = 1.0F;
			}

			var4 = par3 / var4;
			par1 *= var4;
			par2 *= var4;
			final float var5 = MathHelper.sin(rotationYaw * (float) Math.PI
					/ 180.0F);
			final float var6 = MathHelper.cos(rotationYaw * (float) Math.PI
					/ 180.0F);
			motionX += par1 * var6 - par2 * var5;
			motionZ += par2 * var6 + par1 * var5;
		}
	}

	public int getBrightnessForRender(final float par1) {
		final int var2 = MathHelper.floor_double(posX);
		final int var3 = MathHelper.floor_double(posZ);

		if (worldObj.blockExists(var2, 0, var3)) {
			final double var4 = (boundingBox.maxY - boundingBox.minY) * 0.66D;
			final int var6 = MathHelper.floor_double(posY - yOffset + var4);
			return worldObj.getLightBrightnessForSkyBlocks(var2, var6, var3, 0);
		} else {
			return 0;
		}
	}

	/**
	 * Gets how bright this entity is.
	 */
	public float getBrightness(final float par1) {
		final int var2 = MathHelper.floor_double(posX);
		final int var3 = MathHelper.floor_double(posZ);

		if (worldObj.blockExists(var2, 0, var3)) {
			final double var4 = (boundingBox.maxY - boundingBox.minY) * 0.66D;
			final int var6 = MathHelper.floor_double(posY - yOffset + var4);
			return worldObj.getLightBrightness(var2, var6, var3);
		} else {
			return 0.0F;
		}
	}

	/**
	 * Sets the reference to the World object.
	 */
	public void setWorld(final World par1World) {
		worldObj = par1World;
	}

	/**
	 * Sets the entity's position and rotation. Args: posX, posY, posZ, yaw,
	 * pitch
	 */
	public void setPositionAndRotation(final double par1, final double par3,
			final double par5, final float par7, final float par8) {
		prevPosX = posX = par1;
		prevPosY = posY = par3;
		prevPosZ = posZ = par5;
		prevRotationYaw = rotationYaw = par7;
		prevRotationPitch = rotationPitch = par8;
		ySize = 0.0F;
		final double var9 = prevRotationYaw - par7;

		if (var9 < -180.0D) {
			prevRotationYaw += 360.0F;
		}

		if (var9 >= 180.0D) {
			prevRotationYaw -= 360.0F;
		}

		setPosition(posX, posY, posZ);
		setRotation(par7, par8);
	}

	/**
	 * Sets the location and Yaw/Pitch of an entity in the world
	 */
	public void setLocationAndAngles(final double par1, final double par3,
			final double par5, final float par7, final float par8) {
		lastTickPosX = prevPosX = posX = par1;
		lastTickPosY = prevPosY = posY = par3 + yOffset;
		lastTickPosZ = prevPosZ = posZ = par5;
		rotationYaw = par7;
		rotationPitch = par8;
		setPosition(posX, posY, posZ);
	}

	/**
	 * Returns the distance to the entity. Args: entity
	 */
	public float getDistanceToEntity(final Entity par1Entity) {
		final float var2 = (float) (posX - par1Entity.posX);
		final float var3 = (float) (posY - par1Entity.posY);
		final float var4 = (float) (posZ - par1Entity.posZ);
		return MathHelper.sqrt_float(var2 * var2 + var3 * var3 + var4 * var4);
	}

	/**
	 * Gets the squared distance to the position. Args: x, y, z
	 */
	public double getDistanceSq(final double par1, final double par3,
			final double par5) {
		final double var7 = posX - par1;
		final double var9 = posY - par3;
		final double var11 = posZ - par5;
		return var7 * var7 + var9 * var9 + var11 * var11;
	}

	/**
	 * Gets the distance to the position. Args: x, y, z
	 */
	public double getDistance(final double par1, final double par3,
			final double par5) {
		final double var7 = posX - par1;
		final double var9 = posY - par3;
		final double var11 = posZ - par5;
		return MathHelper
				.sqrt_double(var7 * var7 + var9 * var9 + var11 * var11);
	}

	/**
	 * Returns the squared distance to the entity. Args: entity
	 */
	public double getDistanceSqToEntity(final Entity par1Entity) {
		final double var2 = posX - par1Entity.posX;
		final double var4 = posY - par1Entity.posY;
		final double var6 = posZ - par1Entity.posZ;
		return var2 * var2 + var4 * var4 + var6 * var6;
	}

	/**
	 * Called by a player entity when they collide with an entity
	 */
	public void onCollideWithPlayer(final EntityPlayer par1EntityPlayer) {
	}

	/**
	 * Applies a velocity to each of the entities pushing them away from each
	 * other. Args: entity
	 */
	public void applyEntityCollision(final Entity par1Entity) {
		if (par1Entity.riddenByEntity != this
				&& par1Entity.ridingEntity != this) {
			double var2 = par1Entity.posX - posX;
			double var4 = par1Entity.posZ - posZ;
			double var6 = MathHelper.abs_max(var2, var4);

			if (var6 >= 0.009999999776482582D) {
				var6 = MathHelper.sqrt_double(var6);
				var2 /= var6;
				var4 /= var6;
				double var8 = 1.0D / var6;

				if (var8 > 1.0D) {
					var8 = 1.0D;
				}

				var2 *= var8;
				var4 *= var8;
				var2 *= 0.05000000074505806D;
				var4 *= 0.05000000074505806D;
				var2 *= 1.0F - entityCollisionReduction;
				var4 *= 1.0F - entityCollisionReduction;
				addVelocity(-var2, 0.0D, -var4);
				par1Entity.addVelocity(var2, 0.0D, var4);
			}
		}
	}

	/**
	 * Adds to the current velocity of the entity. Args: x, y, z
	 */
	public void addVelocity(final double par1, final double par3,
			final double par5) {
		motionX += par1;
		motionY += par3;
		motionZ += par5;
		isAirBorne = true;
	}

	/**
	 * Sets that this entity has been attacked.
	 */
	protected void setBeenAttacked() {
		velocityChanged = true;
	}

	/**
	 * Called when the entity is attacked.
	 */
	public boolean attackEntityFrom(final DamageSource par1DamageSource,
			final int par2) {
		if (isEntityInvulnerable()) {
			return false;
		} else {
			setBeenAttacked();
			return false;
		}
	}

	/**
	 * Returns true if other Entities should be prevented from moving through
	 * this Entity.
	 */
	public boolean canBeCollidedWith() {
		return false;
	}

	/**
	 * Returns true if this entity should push and be pushed by other entities
	 * when colliding.
	 */
	public boolean canBePushed() {
		return false;
	}

	/**
	 * Adds a value to the player score. Currently not actually used and the
	 * entity passed in does nothing. Args: entity, scoreToAdd
	 */
	public void addToPlayerScore(final Entity par1Entity, final int par2) {
	}

	/**
	 * Checks using a Vec3d to determine if this entity is within range of that
	 * vector to be rendered. Args: vec3D
	 */
	public boolean isInRangeToRenderVec3D(final Vec3 par1Vec3) {
		final double var2 = posX - par1Vec3.xCoord;
		final double var4 = posY - par1Vec3.yCoord;
		final double var6 = posZ - par1Vec3.zCoord;
		final double var8 = var2 * var2 + var4 * var4 + var6 * var6;
		return isInRangeToRenderDist(var8);
	}

	/**
	 * Checks if the entity is in range to render by using the past in distance
	 * and comparing it to its average edge length * 64 * renderDistanceWeight
	 * Args: distance
	 */
	public boolean isInRangeToRenderDist(final double par1) {
		double var3 = boundingBox.getAverageEdgeLength();
		var3 *= 64.0D * renderDistanceWeight;
		return par1 < var3 * var3;
	}

	/**
	 * Returns the texture's file path as a String.
	 */
	public String getTexture() {
		return null;
	}

	public boolean addNotRiddenEntityID(final NBTTagCompound par1NBTTagCompound) {
		final String var2 = getEntityString();

		if (!isDead && var2 != null) {
			par1NBTTagCompound.setString("id", var2);
			writeToNBT(par1NBTTagCompound);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * adds the ID of this entity to the NBT given
	 */
	public boolean addEntityID(final NBTTagCompound par1NBTTagCompound) {
		final String var2 = getEntityString();

		if (!isDead && var2 != null && riddenByEntity == null) {
			par1NBTTagCompound.setString("id", var2);
			writeToNBT(par1NBTTagCompound);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Save the entity to NBT (calls an abstract helper method to write extra
	 * data)
	 */
	public void writeToNBT(final NBTTagCompound par1NBTTagCompound) {
		try {
			par1NBTTagCompound.setTag("Pos", newDoubleNBTList(new double[] {
					posX, posY + ySize, posZ }));
			par1NBTTagCompound.setTag("Motion", newDoubleNBTList(new double[] {
					motionX, motionY, motionZ }));
			par1NBTTagCompound.setTag("Rotation", newFloatNBTList(new float[] {
					rotationYaw, rotationPitch }));
			par1NBTTagCompound.setFloat("FallDistance", fallDistance);
			par1NBTTagCompound.setShort("Fire", (short) fire);
			par1NBTTagCompound.setShort("Air", (short) getAir());
			par1NBTTagCompound.setBoolean("OnGround", onGround);
			par1NBTTagCompound.setInteger("Dimension", dimension);
			par1NBTTagCompound.setBoolean("Invulnerable", invulnerable);
			par1NBTTagCompound.setInteger("PortalCooldown", timeUntilPortal);
			par1NBTTagCompound.setLong("UUIDMost",
					entityUniqueID.getMostSignificantBits());
			par1NBTTagCompound.setLong("UUIDLeast",
					entityUniqueID.getLeastSignificantBits());
			writeEntityToNBT(par1NBTTagCompound);

			if (ridingEntity != null) {
				final NBTTagCompound var2 = new NBTTagCompound("Riding");

				if (ridingEntity.addNotRiddenEntityID(var2)) {
					par1NBTTagCompound.setTag("Riding", var2);
				}
			}
		} catch (final Throwable var5) {
			final CrashReport var3 = CrashReport.makeCrashReport(var5,
					"Saving entity NBT");
			final CrashReportCategory var4 = var3
					.makeCategory("Entity being saved");
			func_85029_a(var4);
			throw new ReportedException(var3);
		}
	}

	/**
	 * Reads the entity from NBT (calls an abstract helper method to read
	 * specialized data)
	 */
	public void readFromNBT(final NBTTagCompound par1NBTTagCompound) {
		try {
			final NBTTagList var2 = par1NBTTagCompound.getTagList("Pos");
			final NBTTagList var6 = par1NBTTagCompound.getTagList("Motion");
			final NBTTagList var7 = par1NBTTagCompound.getTagList("Rotation");
			motionX = ((NBTTagDouble) var6.tagAt(0)).data;
			motionY = ((NBTTagDouble) var6.tagAt(1)).data;
			motionZ = ((NBTTagDouble) var6.tagAt(2)).data;

			if (Math.abs(motionX) > 10.0D) {
				motionX = 0.0D;
			}

			if (Math.abs(motionY) > 10.0D) {
				motionY = 0.0D;
			}

			if (Math.abs(motionZ) > 10.0D) {
				motionZ = 0.0D;
			}

			prevPosX = lastTickPosX = posX = ((NBTTagDouble) var2.tagAt(0)).data;
			prevPosY = lastTickPosY = posY = ((NBTTagDouble) var2.tagAt(1)).data;
			prevPosZ = lastTickPosZ = posZ = ((NBTTagDouble) var2.tagAt(2)).data;
			prevRotationYaw = rotationYaw = ((NBTTagFloat) var7.tagAt(0)).data;
			prevRotationPitch = rotationPitch = ((NBTTagFloat) var7.tagAt(1)).data;
			fallDistance = par1NBTTagCompound.getFloat("FallDistance");
			fire = par1NBTTagCompound.getShort("Fire");
			setAir(par1NBTTagCompound.getShort("Air"));
			onGround = par1NBTTagCompound.getBoolean("OnGround");
			dimension = par1NBTTagCompound.getInteger("Dimension");
			invulnerable = par1NBTTagCompound.getBoolean("Invulnerable");
			timeUntilPortal = par1NBTTagCompound.getInteger("PortalCooldown");

			if (par1NBTTagCompound.hasKey("UUIDMost")
					&& par1NBTTagCompound.hasKey("UUIDLeast")) {
				entityUniqueID = new UUID(
						par1NBTTagCompound.getLong("UUIDMost"),
						par1NBTTagCompound.getLong("UUIDLeast"));
			}

			setPosition(posX, posY, posZ);
			setRotation(rotationYaw, rotationPitch);
			readEntityFromNBT(par1NBTTagCompound);
		} catch (final Throwable var5) {
			final CrashReport var3 = CrashReport.makeCrashReport(var5,
					"Loading entity NBT");
			final CrashReportCategory var4 = var3
					.makeCategory("Entity being loaded");
			func_85029_a(var4);
			throw new ReportedException(var3);
		}
	}

	/**
	 * Returns the string that identifies this Entity's class
	 */
	protected final String getEntityString() {
		return EntityList.getEntityString(this);
	}

	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	protected abstract void readEntityFromNBT(NBTTagCompound var1);

	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	protected abstract void writeEntityToNBT(NBTTagCompound var1);

	/**
	 * creates a NBT list from the array of doubles passed to this function
	 */
	protected NBTTagList newDoubleNBTList(final double... par1ArrayOfDouble) {
		final NBTTagList var2 = new NBTTagList();
		final double[] var3 = par1ArrayOfDouble;
		final int var4 = par1ArrayOfDouble.length;

		for (int var5 = 0; var5 < var4; ++var5) {
			final double var6 = var3[var5];
			var2.appendTag(new NBTTagDouble((String) null, var6));
		}

		return var2;
	}

	/**
	 * Returns a new NBTTagList filled with the specified floats
	 */
	protected NBTTagList newFloatNBTList(final float... par1ArrayOfFloat) {
		final NBTTagList var2 = new NBTTagList();
		final float[] var3 = par1ArrayOfFloat;
		final int var4 = par1ArrayOfFloat.length;

		for (int var5 = 0; var5 < var4; ++var5) {
			final float var6 = var3[var5];
			var2.appendTag(new NBTTagFloat((String) null, var6));
		}

		return var2;
	}

	public float getShadowSize() {
		return height / 2.0F;
	}

	/**
	 * Drops an item stack at the entity's position. Args: itemID, count
	 */
	public EntityItem dropItem(final int par1, final int par2) {
		return dropItemWithOffset(par1, par2, 0.0F);
	}

	/**
	 * Drops an item stack with a specified y offset. Args: itemID, count,
	 * yOffset
	 */
	public EntityItem dropItemWithOffset(final int par1, final int par2,
			final float par3) {
		return entityDropItem(new ItemStack(par1, par2, 0), par3);
	}

	/**
	 * Drops an item at the position of the entity.
	 */
	public EntityItem entityDropItem(final ItemStack par1ItemStack,
			final float par2) {
		final EntityItem var3 = new EntityItem(worldObj, posX, posY + par2,
				posZ, par1ItemStack);
		var3.delayBeforeCanPickup = 10;
		worldObj.spawnEntityInWorld(var3);
		return var3;
	}

	/**
	 * Checks whether target entity is alive.
	 */
	public boolean isEntityAlive() {
		return !isDead;
	}

	/**
	 * Checks if this entity is inside of an opaque block
	 */
	public boolean isEntityInsideOpaqueBlock() {
		// TODO: Entity#isEntityInsideOpaqueBlock
		if (Pringles.getInstance().getFactory().getModuleManager()
				.getModule("freecam").isEnabled()) {
			return false;
		}

		for (int var1 = 0; var1 < 8; ++var1) {
			final float var2 = ((var1 >> 0) % 2 - 0.5F) * width * 0.8F;
			final float var3 = ((var1 >> 1) % 2 - 0.5F) * 0.1F;
			final float var4 = ((var1 >> 2) % 2 - 0.5F) * width * 0.8F;
			final int var5 = MathHelper.floor_double(posX + var2);
			final int var6 = MathHelper.floor_double(posY + getEyeHeight()
					+ var3);
			final int var7 = MathHelper.floor_double(posZ + var4);

			if (worldObj.isBlockNormalCube(var5, var6, var7)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Called when a player interacts with a mob. e.g. gets milk from a cow,
	 * gets into the saddle on a pig.
	 */
	public boolean interact(final EntityPlayer par1EntityPlayer) {
		return false;
	}

	/**
	 * Returns a boundingBox used to collide the entity with other entities and
	 * blocks. This enables the entity to be pushable on contact, like boats or
	 * minecarts.
	 */
	public AxisAlignedBB getCollisionBox(final Entity par1Entity) {
		return null;
	}

	/**
	 * Handles updating while being ridden by an entity
	 */
	public void updateRidden() {
		if (ridingEntity.isDead) {
			ridingEntity = null;
		} else {
			motionX = 0.0D;
			motionY = 0.0D;
			motionZ = 0.0D;
			onUpdate();

			if (ridingEntity != null) {
				ridingEntity.updateRiderPosition();
				entityRiderYawDelta += ridingEntity.rotationYaw
						- ridingEntity.prevRotationYaw;

				for (entityRiderPitchDelta += ridingEntity.rotationPitch
						- ridingEntity.prevRotationPitch; entityRiderYawDelta >= 180.0D; entityRiderYawDelta -= 360.0D) {
					;
				}

				while (entityRiderYawDelta < -180.0D) {
					entityRiderYawDelta += 360.0D;
				}

				while (entityRiderPitchDelta >= 180.0D) {
					entityRiderPitchDelta -= 360.0D;
				}

				while (entityRiderPitchDelta < -180.0D) {
					entityRiderPitchDelta += 360.0D;
				}

				double var1 = entityRiderYawDelta * 0.5D;
				double var3 = entityRiderPitchDelta * 0.5D;
				final float var5 = 10.0F;

				if (var1 > var5) {
					var1 = var5;
				}

				if (var1 < -var5) {
					var1 = -var5;
				}

				if (var3 > var5) {
					var3 = var5;
				}

				if (var3 < -var5) {
					var3 = -var5;
				}

				entityRiderYawDelta -= var1;
				entityRiderPitchDelta -= var3;
				rotationYaw = (float) (rotationYaw + var1);
				rotationPitch = (float) (rotationPitch + var3);
			}
		}
	}

	public void updateRiderPosition() {
		if (riddenByEntity != null) {
			if (!(riddenByEntity instanceof EntityPlayer)
					|| !((EntityPlayer) riddenByEntity).func_71066_bF()) {
				riddenByEntity.lastTickPosX = lastTickPosX;
				riddenByEntity.lastTickPosY = lastTickPosY
						+ getMountedYOffset() + riddenByEntity.getYOffset();
				riddenByEntity.lastTickPosZ = lastTickPosZ;
			}

			riddenByEntity.setPosition(posX, posY + getMountedYOffset()
					+ riddenByEntity.getYOffset(), posZ);
		}
	}

	/**
	 * Returns the Y Offset of this entity.
	 */
	public double getYOffset() {
		return yOffset;
	}

	/**
	 * Returns the Y offset from the entity's position for any entity riding
	 * this one.
	 */
	public double getMountedYOffset() {
		return height * 0.75D;
	}

	/**
	 * Called when a player mounts an entity. e.g. mounts a pig, mounts a boat.
	 */
	public void mountEntity(final Entity par1Entity) {
		entityRiderPitchDelta = 0.0D;
		entityRiderYawDelta = 0.0D;

		if (par1Entity == null) {
			if (ridingEntity != null) {
				setLocationAndAngles(ridingEntity.posX,
						ridingEntity.boundingBox.minY + ridingEntity.height,
						ridingEntity.posZ, rotationYaw, rotationPitch);
				ridingEntity.riddenByEntity = null;
			}

			ridingEntity = null;
		} else {
			if (ridingEntity != null) {
				ridingEntity.riddenByEntity = null;
			}

			ridingEntity = par1Entity;
			par1Entity.riddenByEntity = this;
		}
	}

	/**
	 * Called when a player unounts an entity.
	 */
	public void unmountEntity(final Entity par1Entity) {
		double var3 = posX;
		double var5 = posY;
		double var7 = posZ;

		if (par1Entity != null) {
			var3 = par1Entity.posX;
			var5 = par1Entity.boundingBox.minY + par1Entity.height;
			var7 = par1Entity.posZ;
		}

		for (double var9 = -1.5D; var9 < 2.0D; ++var9) {
			for (double var11 = -1.5D; var11 < 2.0D; ++var11) {
				if (var9 != 0.0D || var11 != 0.0D) {
					final int var13 = (int) (posX + var9);
					final int var14 = (int) (posZ + var11);
					final AxisAlignedBB var2 = boundingBox
							.getOffsetBoundingBox(var9, 1.0D, var11);

					if (worldObj.getCollidingBlockBounds(var2).isEmpty()) {
						if (worldObj.doesBlockHaveSolidTopSurface(var13,
								(int) posY, var14)) {
							setLocationAndAngles(posX + var9, posY + 1.0D, posZ
									+ var11, rotationYaw, rotationPitch);
							return;
						}

						if (worldObj.doesBlockHaveSolidTopSurface(var13,
								(int) posY - 1, var14)
								|| worldObj.getBlockMaterial(var13,
										(int) posY - 1, var14) == Material.water) {
							var3 = posX + var9;
							var5 = posY + 1.0D;
							var7 = posZ + var11;
						}
					}
				}
			}
		}

		setLocationAndAngles(var3, var5, var7, rotationYaw, rotationPitch);
	}

	/**
	 * Sets the position and rotation. Only difference from the other one is no
	 * bounding on the rotation. Args: posX, posY, posZ, yaw, pitch
	 */
	public void setPositionAndRotation2(final double par1, double par3,
			final double par5, final float par7, final float par8,
			final int par9) {
		setPosition(par1, par3, par5);
		setRotation(par7, par8);
		final List var10 = worldObj.getCollidingBoundingBoxes(this,
				boundingBox.contract(0.03125D, 0.0D, 0.03125D));

		if (!var10.isEmpty()) {
			double var11 = 0.0D;

			for (int var13 = 0; var13 < var10.size(); ++var13) {
				final AxisAlignedBB var14 = (AxisAlignedBB) var10.get(var13);

				if (var14.maxY > var11) {
					var11 = var14.maxY;
				}
			}

			par3 += var11 - boundingBox.minY;
			setPosition(par1, par3, par5);
		}
	}

	public float getCollisionBorderSize() {
		return 0.1F;
	}

	/**
	 * returns a (normalized) vector of where this entity is looking
	 */
	public Vec3 getLookVec() {
		return null;
	}

	/**
	 * Called by portal blocks when an entity is within it.
	 */
	public void setInPortal() {
		if (timeUntilPortal > 0) {
			timeUntilPortal = getPortalCooldown();
		} else {
			final double var1 = prevPosX - posX;
			final double var3 = prevPosZ - posZ;

			if (!worldObj.isRemote && !inPortal) {
				teleportDirection = Direction.getMovementDirection(var1, var3);
			}

			inPortal = true;
		}
	}

	/**
	 * Return the amount of cooldown before this entity can use a portal again.
	 */
	public int getPortalCooldown() {
		return 900;
	}

	/**
	 * Sets the velocity to the args. Args: x, y, z
	 */
	public void setVelocity(final double par1, final double par3,
			final double par5) {
		motionX = par1;
		motionY = par3;
		motionZ = par5;
	}

	public void handleHealthUpdate(final byte par1) {
	}

	/**
	 * Setups the entity to do the hurt animation. Only used by packets in
	 * multiplayer.
	 */
	public void performHurtAnimation() {
	}

	public void updateCloak() {
	}

	public ItemStack[] getLastActiveItems() {
		return null;
	}

	/**
	 * Sets the held item, or an armor slot. Slot 0 is held item. Slot 1-4 is
	 * armor. Params: Item, slot
	 */
	public void setCurrentItemOrArmor(final int par1,
			final ItemStack par2ItemStack) {
	}

	/**
	 * Returns true if the entity is on fire. Used by render to add the fire
	 * effect on rendering.
	 */
	public boolean isBurning() {
		return fire > 0 || getFlag(0);
	}

	/**
	 * Returns true if the entity is riding another entity, used by render to
	 * rotate the legs to be in 'sit' position for players.
	 */
	public boolean isRiding() {
		return ridingEntity != null || getFlag(2);
	}

	/**
	 * Returns if this entity is sneaking.
	 */
	public boolean isSneaking() {
		return getFlag(1);
	}

	/**
	 * Sets the sneaking flag.
	 */
	public void setSneaking(final boolean par1) {
		setFlag(1, par1);
	}

	/**
	 * Get if the Entity is sprinting.
	 */
	public boolean isSprinting() {
		return getFlag(3);
	}

	/**
	 * Set sprinting switch for Entity.
	 */
	public void setSprinting(final boolean par1) {
		setFlag(3, par1);
	}

	public boolean isInvisible() {
		return getFlag(5);
	}

	public boolean func_98034_c(final EntityPlayer par1EntityPlayer) {
		return isInvisible();
	}

	public void setInvisible(final boolean par1) {
		setFlag(5, par1);
	}

	public boolean isEating() {
		return getFlag(4);
	}

	public void setEating(final boolean par1) {
		setFlag(4, par1);
	}

	/**
	 * Returns true if the flag is active for the entity. Known flags: 0) is
	 * burning; 1) is sneaking; 2) is riding something; 3) is sprinting; 4) is
	 * eating
	 */
	protected boolean getFlag(final int par1) {
		return (dataWatcher.getWatchableObjectByte(0) & 1 << par1) != 0;
	}

	/**
	 * Enable or disable a entity flag, see getEntityFlag to read the know
	 * flags.
	 */
	protected void setFlag(final int par1, final boolean par2) {
		final byte var3 = dataWatcher.getWatchableObjectByte(0);

		if (par2) {
			dataWatcher
					.updateObject(0, Byte.valueOf((byte) (var3 | 1 << par1)));
		} else {
			dataWatcher.updateObject(0,
					Byte.valueOf((byte) (var3 & ~(1 << par1))));
		}
	}

	public int getAir() {
		return dataWatcher.getWatchableObjectShort(1);
	}

	public void setAir(final int par1) {
		dataWatcher.updateObject(1, Short.valueOf((short) par1));
	}

	/**
	 * Called when a lightning bolt hits the entity.
	 */
	public void onStruckByLightning(
			final EntityLightningBolt par1EntityLightningBolt) {
		dealFireDamage(5);
		++fire;

		if (fire == 0) {
			setFire(8);
		}
	}

	/**
	 * This method gets called when the entity kills another one.
	 */
	public void onKillEntity(final EntityLiving par1EntityLiving) {
	}

	/**
	 * Adds velocity to push the entity out of blocks at the specified x, y, z
	 * position Args: x, y, z
	 */
	protected boolean pushOutOfBlocks(final double par1, final double par3,
			final double par5) {
		final int var7 = MathHelper.floor_double(par1);
		final int var8 = MathHelper.floor_double(par3);
		final int var9 = MathHelper.floor_double(par5);
		final double var10 = par1 - var7;
		final double var12 = par3 - var8;
		final double var14 = par5 - var9;
		final List var16 = worldObj.getCollidingBlockBounds(boundingBox);

		if (var16.isEmpty() && !worldObj.func_85174_u(var7, var8, var9)) {
			return false;
		} else {
			final boolean var17 = !worldObj.func_85174_u(var7 - 1, var8, var9);
			final boolean var18 = !worldObj.func_85174_u(var7 + 1, var8, var9);
			final boolean var20 = !worldObj.func_85174_u(var7, var8 + 1, var9);
			final boolean var21 = !worldObj.func_85174_u(var7, var8, var9 - 1);
			final boolean var22 = !worldObj.func_85174_u(var7, var8, var9 + 1);
			byte var23 = 3;
			double var24 = 9999.0D;

			if (var17 && var10 < var24) {
				var24 = var10;
				var23 = 0;
			}

			if (var18 && 1.0D - var10 < var24) {
				var24 = 1.0D - var10;
				var23 = 1;
			}

			if (var20 && 1.0D - var12 < var24) {
				var24 = 1.0D - var12;
				var23 = 3;
			}

			if (var21 && var14 < var24) {
				var24 = var14;
				var23 = 4;
			}

			if (var22 && 1.0D - var14 < var24) {
				var24 = 1.0D - var14;
				var23 = 5;
			}

			final float var26 = rand.nextFloat() * 0.2F + 0.1F;

			if (var23 == 0) {
				motionX = -var26;
			}

			if (var23 == 1) {
				motionX = var26;
			}

			if (var23 == 2) {
				motionY = -var26;
			}

			if (var23 == 3) {
				motionY = var26;
			}

			if (var23 == 4) {
				motionZ = -var26;
			}

			if (var23 == 5) {
				motionZ = var26;
			}

			return true;
		}
	}

	/**
	 * Sets the Entity inside a web block.
	 */
	public void setInWeb() {
		isInWeb = true;
		fallDistance = 0.0F;
	}

	/**
	 * Gets the username of the entity.
	 */
	public String getEntityName() {
		String var1 = EntityList.getEntityString(this);

		if (var1 == null) {
			var1 = "generic";
		}

		return StatCollector.translateToLocal("entity." + var1 + ".name");
	}

	/**
	 * Return the Entity parts making up this Entity (currently only for
	 * dragons)
	 */
	public Entity[] getParts() {
		return null;
	}

	/**
	 * Returns true if Entity argument is equal to this Entity
	 */
	public boolean isEntityEqual(final Entity par1Entity) {
		return this == par1Entity;
	}

	public float getRotationYawHead() {
		return 0.0F;
	}

	/**
	 * Sets the head's yaw rotation of the entity.
	 */
	public void setRotationYawHead(final float par1) {
	}

	/**
	 * If returns false, the item will not inflict any damage against entities.
	 */
	public boolean canAttackWithItem() {
		return true;
	}

	public boolean func_85031_j(final Entity par1Entity) {
		return false;
	}

	@Override
	public String toString() {
		return String.format(
				"%s[\'%s\'/%d, l=\'%s\', x=%.2f, y=%.2f, z=%.2f]",
				new Object[] {
						this.getClass().getSimpleName(),
						getEntityName(),
						Integer.valueOf(entityId),
						worldObj == null ? "~NULL~" : worldObj.getWorldInfo()
								.getWorldName(), Double.valueOf(posX),
						Double.valueOf(posY), Double.valueOf(posZ) });
	}

	/**
	 * Return whether this entity is invulnerable to damage.
	 */
	public boolean isEntityInvulnerable() {
		return invulnerable;
	}

	public void func_82149_j(final Entity par1Entity) {
		setLocationAndAngles(par1Entity.posX, par1Entity.posY, par1Entity.posZ,
				par1Entity.rotationYaw, par1Entity.rotationPitch);
	}

	/**
	 * Copies important data from another entity to this entity. Used when
	 * teleporting entities between worlds, as this actually deletes the
	 * teleporting entity and re-creates it on the other side. Params: Entity to
	 * copy from, unused (always true)
	 */
	public void copyDataFrom(final Entity par1Entity, final boolean par2) {
		final NBTTagCompound var3 = new NBTTagCompound();
		par1Entity.writeToNBT(var3);
		readFromNBT(var3);
		timeUntilPortal = par1Entity.timeUntilPortal;
		teleportDirection = par1Entity.teleportDirection;
	}

	/**
	 * Teleports the entity to another dimension. Params: Dimension number to
	 * teleport to
	 */
	public void travelToDimension(final int par1) {
		if (!worldObj.isRemote && !isDead) {
			worldObj.theProfiler.startSection("changeDimension");
			final MinecraftServer var2 = MinecraftServer.getServer();
			final int var3 = dimension;
			final WorldServer var4 = var2.worldServerForDimension(var3);
			final WorldServer var5 = var2.worldServerForDimension(par1);
			dimension = par1;
			worldObj.removeEntity(this);
			isDead = false;
			worldObj.theProfiler.startSection("reposition");
			var2.getConfigurationManager().transferEntityToWorld(this, var3,
					var4, var5);
			worldObj.theProfiler.endStartSection("reloading");
			final Entity var6 = EntityList.createEntityByName(
					EntityList.getEntityString(this), var5);

			if (var6 != null) {
				var6.copyDataFrom(this, true);
				var5.spawnEntityInWorld(var6);
			}

			isDead = true;
			worldObj.theProfiler.endSection();
			var4.resetUpdateEntityTick();
			var5.resetUpdateEntityTick();
			worldObj.theProfiler.endSection();
		}
	}

	public float func_82146_a(final Explosion par1Explosion,
			final World par2World, final int par3, final int par4,
			final int par5, final Block par6Block) {
		return par6Block.getExplosionResistance(this);
	}

	public boolean func_96091_a(final Explosion par1Explosion,
			final World par2World, final int par3, final int par4,
			final int par5, final int par6, final float par7) {
		return true;
	}

	public int func_82143_as() {
		return 3;
	}

	public int getTeleportDirection() {
		return teleportDirection;
	}

	/**
	 * Return whether this entity should NOT trigger a pressure plate or a
	 * tripwire.
	 */
	public boolean doesEntityNotTriggerPressurePlate() {
		return false;
	}

	public void func_85029_a(final CrashReportCategory par1CrashReportCategory) {
		par1CrashReportCategory.addCrashSectionCallable("Entity Type",
				new CallableEntityType(this));
		par1CrashReportCategory.addCrashSection("Entity ID",
				Integer.valueOf(entityId));
		par1CrashReportCategory.addCrashSectionCallable("Entity Name",
				new CallableEntityName(this));
		par1CrashReportCategory.addCrashSection(
				"Entity\'s Exact location",
				String.format(
						"%.2f, %.2f, %.2f",
						new Object[] { Double.valueOf(posX),
								Double.valueOf(posY), Double.valueOf(posZ) }));
		par1CrashReportCategory.addCrashSection(
				"Entity\'s Block location",
				CrashReportCategory.getLocationInfo(
						MathHelper.floor_double(posX),
						MathHelper.floor_double(posY),
						MathHelper.floor_double(posZ)));
		par1CrashReportCategory.addCrashSection(
				"Entity\'s Momentum",
				String.format(
						"%.2f, %.2f, %.2f",
						new Object[] { Double.valueOf(motionX),
								Double.valueOf(motionY),
								Double.valueOf(motionZ) }));
	}

	/**
	 * Return whether this entity should be rendered as on fire.
	 */
	public boolean canRenderOnFire() {
		return isBurning();
	}

	public boolean func_96092_aw() {
		return true;
	}

	/**
	 * Returns the translated name of the entity.
	 */
	public String getTranslatedEntityName() {
		return getEntityName();
	}
}
