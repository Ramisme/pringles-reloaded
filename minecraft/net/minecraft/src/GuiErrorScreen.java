package net.minecraft.src;

public class GuiErrorScreen extends GuiScreen {
	/**
	 * Unused class. Would contain a message drawn to the center of the screen.
	 */
	private final String message1;

	/**
	 * Unused class. Would contain a message drawn to the center of the screen.
	 */
	private final String message2;

	public GuiErrorScreen(final String par1Str, final String par2Str) {
		message1 = par1Str;
		message2 = par2Str;
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui() {
		super.initGui();
		buttonList.add(new GuiButton(0, width / 2 - 100, 140, StatCollector
				.translateToLocal("gui.cancel")));
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(final int par1, final int par2, final float par3) {
		drawGradientRect(0, 0, width, height, -12574688, -11530224);
		drawCenteredString(fontRenderer, message1, width / 2, 90, 16777215);
		drawCenteredString(fontRenderer, message2, width / 2, 110, 16777215);
		super.drawScreen(par1, par2, par3);
	}

	/**
	 * Fired when a key is typed. This is the equivalent of
	 * KeyListener.keyTyped(KeyEvent e).
	 */
	@Override
	protected void keyTyped(final char par1, final int par2) {
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	@Override
	protected void actionPerformed(final GuiButton par1GuiButton) {
		mc.displayGuiScreen((GuiScreen) null);
	}
}
