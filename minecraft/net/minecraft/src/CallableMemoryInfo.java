package net.minecraft.src;

import java.util.concurrent.Callable;

class CallableMemoryInfo implements Callable {
	/** Reference to the CrashReport object. */
	final CrashReport theCrashReport;

	CallableMemoryInfo(final CrashReport par1CrashReport) {
		theCrashReport = par1CrashReport;
	}

	/**
	 * Returns the memory information as a String. Includes the Free Memory in
	 * bytes and MB, Total Memory in bytes and MB, and Max Memory in Bytes and
	 * MB.
	 */
	public String getMemoryInfoAsString() {
		final Runtime var1 = Runtime.getRuntime();
		final long var2 = var1.maxMemory();
		final long var4 = var1.totalMemory();
		final long var6 = var1.freeMemory();
		final long var8 = var2 / 1024L / 1024L;
		final long var10 = var4 / 1024L / 1024L;
		final long var12 = var6 / 1024L / 1024L;
		return var6 + " bytes (" + var12 + " MB) / " + var4 + " bytes ("
				+ var10 + " MB) up to " + var2 + " bytes (" + var8 + " MB)";
	}

	@Override
	public Object call() {
		return getMemoryInfoAsString();
	}
}
