package net.minecraft.src;

import java.util.Iterator;

import net.minecraft.server.MinecraftServer;

public class WorldManager implements IWorldAccess {
	/** Reference to the MinecraftServer object. */
	private final MinecraftServer mcServer;

	/** The WorldServer object. */
	private final WorldServer theWorldServer;

	public WorldManager(final MinecraftServer par1MinecraftServer,
			final WorldServer par2WorldServer) {
		mcServer = par1MinecraftServer;
		theWorldServer = par2WorldServer;
	}

	/**
	 * Spawns a particle. Arg: particleType, x, y, z, velX, velY, velZ
	 */
	@Override
	public void spawnParticle(final String par1Str, final double par2,
			final double par4, final double par6, final double par8,
			final double par10, final double par12) {
	}

	/**
	 * Called on all IWorldAccesses when an entity is created or loaded. On
	 * client worlds, starts downloading any necessary textures. On server
	 * worlds, adds the entity to the entity tracker.
	 */
	@Override
	public void onEntityCreate(final Entity par1Entity) {
		theWorldServer.getEntityTracker().addEntityToTracker(par1Entity);
	}

	/**
	 * Called on all IWorldAccesses when an entity is unloaded or destroyed. On
	 * client worlds, releases any downloaded textures. On server worlds,
	 * removes the entity from the entity tracker.
	 */
	@Override
	public void onEntityDestroy(final Entity par1Entity) {
		theWorldServer.getEntityTracker().removeEntityFromAllTrackingPlayers(
				par1Entity);
	}

	/**
	 * Plays the specified sound. Arg: soundName, x, y, z, volume, pitch
	 */
	@Override
	public void playSound(final String par1Str, final double par2,
			final double par4, final double par6, final float par8,
			final float par9) {
		mcServer.getConfigurationManager().sendToAllNear(par2, par4, par6,
				par8 > 1.0F ? (double) (16.0F * par8) : 16.0D,
				theWorldServer.provider.dimensionId,
				new Packet62LevelSound(par1Str, par2, par4, par6, par8, par9));
	}

	/**
	 * Plays sound to all near players except the player reference given
	 */
	@Override
	public void playSoundToNearExcept(final EntityPlayer par1EntityPlayer,
			final String par2Str, final double par3, final double par5,
			final double par7, final float par9, final float par10) {
		mcServer.getConfigurationManager().sendToAllNearExcept(
				par1EntityPlayer, par3, par5, par7,
				par9 > 1.0F ? (double) (16.0F * par9) : 16.0D,
				theWorldServer.provider.dimensionId,
				new Packet62LevelSound(par2Str, par3, par5, par7, par9, par10));
	}

	/**
	 * On the client, re-renders all blocks in this range, inclusive. On the
	 * server, does nothing. Args: min x, min y, min z, max x, max y, max z
	 */
	@Override
	public void markBlockRangeForRenderUpdate(final int par1, final int par2,
			final int par3, final int par4, final int par5, final int par6) {
	}

	/**
	 * On the client, re-renders the block. On the server, sends the block to
	 * the client (which will re-render it), including the tile entity
	 * description packet if applicable. Args: x, y, z
	 */
	@Override
	public void markBlockForUpdate(final int par1, final int par2,
			final int par3) {
		theWorldServer.getPlayerManager().flagChunkForUpdate(par1, par2, par3);
	}

	/**
	 * On the client, re-renders this block. On the server, does nothing. Used
	 * for lighting updates.
	 */
	@Override
	public void markBlockForRenderUpdate(final int par1, final int par2,
			final int par3) {
	}

	/**
	 * Plays the specified record. Arg: recordName, x, y, z
	 */
	@Override
	public void playRecord(final String par1Str, final int par2,
			final int par3, final int par4) {
	}

	/**
	 * Plays a pre-canned sound effect along with potentially auxiliary
	 * data-driven one-shot behaviour (particles, etc).
	 */
	@Override
	public void playAuxSFX(final EntityPlayer par1EntityPlayer, final int par2,
			final int par3, final int par4, final int par5, final int par6) {
		mcServer.getConfigurationManager().sendToAllNearExcept(
				par1EntityPlayer, par3, par4, par5, 64.0D,
				theWorldServer.provider.dimensionId,
				new Packet61DoorChange(par2, par3, par4, par5, par6, false));
	}

	@Override
	public void broadcastSound(final int par1, final int par2, final int par3,
			final int par4, final int par5) {
		mcServer.getConfigurationManager().sendPacketToAllPlayers(
				new Packet61DoorChange(par1, par2, par3, par4, par5, true));
	}

	/**
	 * Starts (or continues) destroying a block with given ID at the given
	 * coordinates for the given partially destroyed value
	 */
	@Override
	public void destroyBlockPartially(final int par1, final int par2,
			final int par3, final int par4, final int par5) {
		final Iterator var6 = mcServer.getConfigurationManager().playerEntityList
				.iterator();

		while (var6.hasNext()) {
			final EntityPlayerMP var7 = (EntityPlayerMP) var6.next();

			if (var7 != null && var7.worldObj == theWorldServer
					&& var7.entityId != par1) {
				final double var8 = par2 - var7.posX;
				final double var10 = par3 - var7.posY;
				final double var12 = par4 - var7.posZ;

				if (var8 * var8 + var10 * var10 + var12 * var12 < 1024.0D) {
					var7.playerNetServerHandler
							.sendPacketToPlayer(new Packet55BlockDestroy(par1,
									par2, par3, par4, par5));
				}
			}
		}
	}
}
