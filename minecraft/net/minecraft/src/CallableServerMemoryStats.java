package net.minecraft.src;

import java.util.concurrent.Callable;

import net.minecraft.server.MinecraftServer;

public class CallableServerMemoryStats implements Callable {
	/** Reference to the MinecraftServer object. */
	final MinecraftServer mcServer;

	public CallableServerMemoryStats(final MinecraftServer par1) {
		mcServer = par1;
	}

	public String callServerMemoryStats() {
		return MinecraftServer.getServerConfigurationManager(mcServer)
				.getCurrentPlayerCount()
				+ " / "
				+ MinecraftServer.getServerConfigurationManager(mcServer)
						.getMaxPlayers()
				+ "; "
				+ MinecraftServer.getServerConfigurationManager(mcServer).playerEntityList;
	}

	@Override
	public Object call() {
		return callServerMemoryStats();
	}
}
