package net.minecraft.src;

import java.util.concurrent.Callable;

class CallableEffectDuration implements Callable {
	final PotionEffect field_102037_a;

	final EntityLiving field_102036_b;

	CallableEffectDuration(final EntityLiving par1EntityLiving,
			final PotionEffect par2PotionEffect) {
		field_102036_b = par1EntityLiving;
		field_102037_a = par2PotionEffect;
	}

	public String func_102035_a() {
		return field_102037_a.getDuration() + "";
	}

	@Override
	public Object call() {
		return func_102035_a();
	}
}
