package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Packet60Explosion extends Packet {
	public double explosionX;
	public double explosionY;
	public double explosionZ;
	public float explosionSize;
	public List chunkPositionRecords;

	/** X velocity of the player being pushed by the explosion */
	private float playerVelocityX;

	/** Y velocity of the player being pushed by the explosion */
	private float playerVelocityY;

	/** Z velocity of the player being pushed by the explosion */
	private float playerVelocityZ;

	public Packet60Explosion() {
	}

	public Packet60Explosion(final double par1, final double par3,
			final double par5, final float par7, final List par8List,
			final Vec3 par9Vec3) {
		explosionX = par1;
		explosionY = par3;
		explosionZ = par5;
		explosionSize = par7;
		chunkPositionRecords = new ArrayList(par8List);

		if (par9Vec3 != null) {
			playerVelocityX = (float) par9Vec3.xCoord;
			playerVelocityY = (float) par9Vec3.yCoord;
			playerVelocityZ = (float) par9Vec3.zCoord;
		}
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		explosionX = par1DataInputStream.readDouble();
		explosionY = par1DataInputStream.readDouble();
		explosionZ = par1DataInputStream.readDouble();
		explosionSize = par1DataInputStream.readFloat();
		final int var2 = par1DataInputStream.readInt();
		chunkPositionRecords = new ArrayList(var2);
		final int var3 = (int) explosionX;
		final int var4 = (int) explosionY;
		final int var5 = (int) explosionZ;

		for (int var6 = 0; var6 < var2; ++var6) {
			final int var7 = par1DataInputStream.readByte() + var3;
			final int var8 = par1DataInputStream.readByte() + var4;
			final int var9 = par1DataInputStream.readByte() + var5;
			chunkPositionRecords.add(new ChunkPosition(var7, var8, var9));
		}

		playerVelocityX = par1DataInputStream.readFloat();
		playerVelocityY = par1DataInputStream.readFloat();
		playerVelocityZ = par1DataInputStream.readFloat();
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		par1DataOutputStream.writeDouble(explosionX);
		par1DataOutputStream.writeDouble(explosionY);
		par1DataOutputStream.writeDouble(explosionZ);
		par1DataOutputStream.writeFloat(explosionSize);
		par1DataOutputStream.writeInt(chunkPositionRecords.size());
		final int var2 = (int) explosionX;
		final int var3 = (int) explosionY;
		final int var4 = (int) explosionZ;
		final Iterator var5 = chunkPositionRecords.iterator();

		while (var5.hasNext()) {
			final ChunkPosition var6 = (ChunkPosition) var5.next();
			final int var7 = var6.x - var2;
			final int var8 = var6.y - var3;
			final int var9 = var6.z - var4;
			par1DataOutputStream.writeByte(var7);
			par1DataOutputStream.writeByte(var8);
			par1DataOutputStream.writeByte(var9);
		}

		par1DataOutputStream.writeFloat(playerVelocityX);
		par1DataOutputStream.writeFloat(playerVelocityY);
		par1DataOutputStream.writeFloat(playerVelocityZ);
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleExplosion(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 32 + chunkPositionRecords.size() * 3 + 3;
	}

	/**
	 * Gets the X velocity of the player being pushed by the explosion.
	 */
	public float getPlayerVelocityX() {
		return playerVelocityX;
	}

	/**
	 * Gets the Y velocity of the player being pushed by the explosion.
	 */
	public float getPlayerVelocityY() {
		return playerVelocityY;
	}

	/**
	 * Gets the Z velocity of the player being pushed by the explosion.
	 */
	public float getPlayerVelocityZ() {
		return playerVelocityZ;
	}
}
