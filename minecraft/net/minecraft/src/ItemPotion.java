package net.minecraft.src;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ItemPotion extends Item {
	/** maps potion damage values to lists of effect names */
	private final HashMap effectCache = new HashMap();
	private static final Map field_77835_b = new LinkedHashMap();
	private Icon field_94591_c;
	private Icon field_94590_d;
	private Icon field_94592_ct;

	public ItemPotion(final int par1) {
		super(par1);
		setMaxStackSize(1);
		setHasSubtypes(true);
		setMaxDamage(0);
		setCreativeTab(CreativeTabs.tabBrewing);
	}

	/**
	 * Returns a list of potion effects for the specified itemstack.
	 */
	public List getEffects(final ItemStack par1ItemStack) {
		if (par1ItemStack.hasTagCompound()
				&& par1ItemStack.getTagCompound().hasKey("CustomPotionEffects")) {
			final ArrayList var6 = new ArrayList();
			final NBTTagList var3 = par1ItemStack.getTagCompound().getTagList(
					"CustomPotionEffects");

			for (int var4 = 0; var4 < var3.tagCount(); ++var4) {
				final NBTTagCompound var5 = (NBTTagCompound) var3.tagAt(var4);
				var6.add(PotionEffect.readCustomPotionEffectFromNBT(var5));
			}

			return var6;
		} else {
			List var2 = (List) effectCache.get(Integer.valueOf(par1ItemStack
					.getItemDamage()));

			if (var2 == null) {
				var2 = PotionHelper.getPotionEffects(
						par1ItemStack.getItemDamage(), false);
				effectCache.put(Integer.valueOf(par1ItemStack.getItemDamage()),
						var2);
			}

			return var2;
		}
	}

	/**
	 * Returns a list of effects for the specified potion damage value.
	 */
	public List getEffects(final int par1) {
		List var2 = (List) effectCache.get(Integer.valueOf(par1));

		if (var2 == null) {
			var2 = PotionHelper.getPotionEffects(par1, false);
			effectCache.put(Integer.valueOf(par1), var2);
		}

		return var2;
	}

	@Override
	public ItemStack onEaten(final ItemStack par1ItemStack,
			final World par2World, final EntityPlayer par3EntityPlayer) {
		if (!par3EntityPlayer.capabilities.isCreativeMode) {
			--par1ItemStack.stackSize;
		}

		if (!par2World.isRemote) {
			final List var4 = this.getEffects(par1ItemStack);

			if (var4 != null) {
				final Iterator var5 = var4.iterator();

				while (var5.hasNext()) {
					final PotionEffect var6 = (PotionEffect) var5.next();
					par3EntityPlayer.addPotionEffect(new PotionEffect(var6));
				}
			}
		}

		if (!par3EntityPlayer.capabilities.isCreativeMode) {
			if (par1ItemStack.stackSize <= 0) {
				return new ItemStack(Item.glassBottle);
			}

			par3EntityPlayer.inventory.addItemStackToInventory(new ItemStack(
					Item.glassBottle));
		}

		return par1ItemStack;
	}

	/**
	 * How long it takes to use or consume an item
	 */
	@Override
	public int getMaxItemUseDuration(final ItemStack par1ItemStack) {
		return 32;
	}

	/**
	 * returns the action that specifies what animation to play when the items
	 * is being used
	 */
	@Override
	public EnumAction getItemUseAction(final ItemStack par1ItemStack) {
		return EnumAction.drink;
	}

	/**
	 * Called whenever this item is equipped and the right mouse button is
	 * pressed. Args: itemStack, world, entityPlayer
	 */
	@Override
	public ItemStack onItemRightClick(final ItemStack par1ItemStack,
			final World par2World, final EntityPlayer par3EntityPlayer) {
		if (ItemPotion.isSplash(par1ItemStack.getItemDamage())) {
			if (!par3EntityPlayer.capabilities.isCreativeMode) {
				--par1ItemStack.stackSize;
			}

			par2World.playSoundAtEntity(par3EntityPlayer, "random.bow", 0.5F,
					0.4F / (Item.itemRand.nextFloat() * 0.4F + 0.8F));

			if (!par2World.isRemote) {
				par2World.spawnEntityInWorld(new EntityPotion(par2World,
						par3EntityPlayer, par1ItemStack));
			}

			return par1ItemStack;
		} else {
			par3EntityPlayer.setItemInUse(par1ItemStack,
					getMaxItemUseDuration(par1ItemStack));
			return par1ItemStack;
		}
	}

	/**
	 * Callback for item usage. If the item does something special on right
	 * clicking, he will have one of those. Return True if something happen and
	 * false if it don't. This is for ITEMS, not BLOCKS
	 */
	@Override
	public boolean onItemUse(final ItemStack par1ItemStack,
			final EntityPlayer par2EntityPlayer, final World par3World,
			final int par4, final int par5, final int par6, final int par7,
			final float par8, final float par9, final float par10) {
		return false;
	}

	/**
	 * Gets an icon index based on an item's damage value
	 */
	@Override
	public Icon getIconFromDamage(final int par1) {
		return ItemPotion.isSplash(par1) ? field_94591_c : field_94590_d;
	}

	/**
	 * Gets an icon index based on an item's damage value and the given render
	 * pass
	 */
	@Override
	public Icon getIconFromDamageForRenderPass(final int par1, final int par2) {
		return par2 == 0 ? field_94592_ct : super
				.getIconFromDamageForRenderPass(par1, par2);
	}

	/**
	 * returns wether or not a potion is a throwable splash potion based on
	 * damage value
	 */
	public static boolean isSplash(final int par0) {
		return (par0 & 16384) != 0;
	}

	public int getColorFromDamage(final int par1) {
		return PotionHelper.func_77915_a(par1, false);
	}

	@Override
	public int getColorFromItemStack(final ItemStack par1ItemStack,
			final int par2) {
		return par2 > 0 ? 16777215 : getColorFromDamage(par1ItemStack
				.getItemDamage());
	}

	@Override
	public boolean requiresMultipleRenderPasses() {
		return true;
	}

	public boolean isEffectInstant(final int par1) {
		final List var2 = this.getEffects(par1);

		if (var2 != null && !var2.isEmpty()) {
			final Iterator var3 = var2.iterator();
			PotionEffect var4;

			do {
				if (!var3.hasNext()) {
					return false;
				}

				var4 = (PotionEffect) var3.next();
			} while (!Potion.potionTypes[var4.getPotionID()].isInstant());

			return true;
		} else {
			return false;
		}
	}

	@Override
	public String getItemDisplayName(final ItemStack par1ItemStack) {
		if (par1ItemStack.getItemDamage() == 0) {
			return StatCollector.translateToLocal("item.emptyPotion.name")
					.trim();
		} else {
			String var2 = "";

			if (ItemPotion.isSplash(par1ItemStack.getItemDamage())) {
				var2 = StatCollector.translateToLocal("potion.prefix.grenade")
						.trim() + " ";
			}

			final List var3 = Item.potion.getEffects(par1ItemStack);
			String var4;

			if (var3 != null && !var3.isEmpty()) {
				var4 = ((PotionEffect) var3.get(0)).getEffectName();
				var4 = var4 + ".postfix";
				return var2 + StatCollector.translateToLocal(var4).trim();
			} else {
				var4 = PotionHelper.func_77905_c(par1ItemStack.getItemDamage());
				return StatCollector.translateToLocal(var4).trim() + " "
						+ super.getItemDisplayName(par1ItemStack);
			}
		}
	}

	/**
	 * allows items to add custom lines of information to the mouseover
	 * description
	 */
	@Override
	public void addInformation(final ItemStack par1ItemStack,
			final EntityPlayer par2EntityPlayer, final List par3List,
			final boolean par4) {
		if (par1ItemStack.getItemDamage() != 0) {
			final List var5 = Item.potion.getEffects(par1ItemStack);

			if (var5 != null && !var5.isEmpty()) {
				final Iterator var9 = var5.iterator();

				while (var9.hasNext()) {
					final PotionEffect var7 = (PotionEffect) var9.next();
					String var8 = StatCollector.translateToLocal(
							var7.getEffectName()).trim();

					if (var7.getAmplifier() > 0) {
						var8 = var8
								+ " "
								+ StatCollector
										.translateToLocal(
												"potion.potency."
														+ var7.getAmplifier())
										.trim();
					}

					if (var7.getDuration() > 20) {
						var8 = var8 + " (" + Potion.getDurationString(var7)
								+ ")";
					}

					if (Potion.potionTypes[var7.getPotionID()].isBadEffect()) {
						par3List.add(EnumChatFormatting.RED + var8);
					} else {
						par3List.add(EnumChatFormatting.GRAY + var8);
					}
				}
			} else {
				final String var6 = StatCollector.translateToLocal(
						"potion.empty").trim();
				par3List.add(EnumChatFormatting.GRAY + var6);
			}
		}
	}

	@Override
	public boolean hasEffect(final ItemStack par1ItemStack) {
		final List var2 = this.getEffects(par1ItemStack);
		return var2 != null && !var2.isEmpty();
	}

	/**
	 * returns a list of items with the same ID, but different meta (eg: dye
	 * returns 16 items)
	 */
	@Override
	public void getSubItems(final int par1,
			final CreativeTabs par2CreativeTabs, final List par3List) {
		super.getSubItems(par1, par2CreativeTabs, par3List);
		int var5;

		if (ItemPotion.field_77835_b.isEmpty()) {
			for (int var4 = 0; var4 <= 15; ++var4) {
				for (var5 = 0; var5 <= 1; ++var5) {
					int var6;

					if (var5 == 0) {
						var6 = var4 | 8192;
					} else {
						var6 = var4 | 16384;
					}

					for (int var7 = 0; var7 <= 2; ++var7) {
						int var8 = var6;

						if (var7 != 0) {
							if (var7 == 1) {
								var8 = var6 | 32;
							} else if (var7 == 2) {
								var8 = var6 | 64;
							}
						}

						final List var9 = PotionHelper.getPotionEffects(var8,
								false);

						if (var9 != null && !var9.isEmpty()) {
							ItemPotion.field_77835_b.put(var9,
									Integer.valueOf(var8));
						}
					}
				}
			}
		}

		final Iterator var10 = ItemPotion.field_77835_b.values().iterator();

		while (var10.hasNext()) {
			var5 = ((Integer) var10.next()).intValue();
			par3List.add(new ItemStack(par1, 1, var5));
		}
	}

	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		field_94590_d = par1IconRegister.registerIcon("potion");
		field_94591_c = par1IconRegister.registerIcon("potion_splash");
		field_94592_ct = par1IconRegister.registerIcon("potion_contents");
	}

	public static Icon func_94589_d(final String par0Str) {
		return par0Str == "potion" ? Item.potion.field_94590_d
				: par0Str == "potion_splash" ? Item.potion.field_94591_c
						: par0Str == "potion_contents" ? Item.potion.field_94592_ct
								: null;
	}
}
