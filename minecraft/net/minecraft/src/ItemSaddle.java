package net.minecraft.src;

public class ItemSaddle extends Item {
	public ItemSaddle(final int par1) {
		super(par1);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabTransport);
	}

	/**
	 * Called when a player right clicks an entity with an item.
	 */
	@Override
	public boolean itemInteractionForEntity(final ItemStack par1ItemStack,
			final EntityLiving par2EntityLiving) {
		if (par2EntityLiving instanceof EntityPig) {
			final EntityPig var3 = (EntityPig) par2EntityLiving;

			if (!var3.getSaddled() && !var3.isChild()) {
				var3.setSaddled(true);
				--par1ItemStack.stackSize;
			}

			return true;
		} else {
			return false;
		}
	}

	/**
	 * Current implementations of this method in child classes do not use the
	 * entry argument beside ev. They just raise the damage on the stack.
	 */
	@Override
	public boolean hitEntity(final ItemStack par1ItemStack,
			final EntityLiving par2EntityLiving,
			final EntityLiving par3EntityLiving) {
		itemInteractionForEntity(par1ItemStack, par2EntityLiving);
		return true;
	}
}
