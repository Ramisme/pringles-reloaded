package net.minecraft.src;

import org.lwjgl.opengl.GL11;

public class RenderGiantZombie extends RenderLiving {
	/** Scale of the model to use */
	private final float scale;

	public RenderGiantZombie(final ModelBase par1ModelBase, final float par2,
			final float par3) {
		super(par1ModelBase, par2 * par3);
		scale = par3;
	}

	/**
	 * Applies the scale to the transform matrix
	 */
	protected void preRenderScale(
			final EntityGiantZombie par1EntityGiantZombie, final float par2) {
		GL11.glScalef(scale, scale, scale);
	}

	/**
	 * Allows the render to do any OpenGL state modifications necessary before
	 * the model is rendered. Args: entityLiving, partialTickTime
	 */
	@Override
	protected void preRenderCallback(final EntityLiving par1EntityLiving,
			final float par2) {
		preRenderScale((EntityGiantZombie) par1EntityLiving, par2);
	}
}
