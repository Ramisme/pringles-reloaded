package net.minecraft.src;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

public class RenderFireball extends Render {
	private final float field_77002_a;

	public RenderFireball(final float par1) {
		field_77002_a = par1;
	}

	public void doRenderFireball(final EntityFireball par1EntityFireball,
			final double par2, final double par4, final double par6,
			final float par8, final float par9) {
		GL11.glPushMatrix();
		GL11.glTranslatef((float) par2, (float) par4, (float) par6);
		GL11.glEnable(GL12.GL_RESCALE_NORMAL);
		final float var10 = field_77002_a;
		GL11.glScalef(var10 / 1.0F, var10 / 1.0F, var10 / 1.0F);
		final Icon var11 = Item.fireballCharge.getIconFromDamage(0);
		loadTexture("/gui/items.png");
		final Tessellator var12 = Tessellator.instance;
		final float var13 = var11.getMinU();
		final float var14 = var11.getMaxU();
		final float var15 = var11.getMinV();
		final float var16 = var11.getMaxV();
		final float var17 = 1.0F;
		final float var18 = 0.5F;
		final float var19 = 0.25F;
		GL11.glRotatef(180.0F - renderManager.playerViewY, 0.0F, 1.0F, 0.0F);
		GL11.glRotatef(-renderManager.playerViewX, 1.0F, 0.0F, 0.0F);
		var12.startDrawingQuads();
		var12.setNormal(0.0F, 1.0F, 0.0F);
		var12.addVertexWithUV(0.0F - var18, 0.0F - var19, 0.0D, var13, var16);
		var12.addVertexWithUV(var17 - var18, 0.0F - var19, 0.0D, var14, var16);
		var12.addVertexWithUV(var17 - var18, 1.0F - var19, 0.0D, var14, var15);
		var12.addVertexWithUV(0.0F - var18, 1.0F - var19, 0.0D, var13, var15);
		var12.draw();
		GL11.glDisable(GL12.GL_RESCALE_NORMAL);
		GL11.glPopMatrix();
	}

	/**
	 * Actually renders the given argument. This is a synthetic bridge method,
	 * always casting down its argument and then handing it off to a worker
	 * function which does the actual work. In all probabilty, the class Render
	 * is generic (Render<T extends Entity) and this method has signature public
	 * void doRender(T entity, double d, double d1, double d2, float f, float
	 * f1). But JAD is pre 1.5 so doesn't do that.
	 */
	@Override
	public void doRender(final Entity par1Entity, final double par2,
			final double par4, final double par6, final float par8,
			final float par9) {
		doRenderFireball((EntityFireball) par1Entity, par2, par4, par6, par8,
				par9);
	}
}
