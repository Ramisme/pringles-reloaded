package net.minecraft.src;

public class NextTickListEntry implements Comparable {
	/** The id number for the next tick entry */
	private static long nextTickEntryID = 0L;

	/** X position this tick is occuring at */
	public int xCoord;

	/** Y position this tick is occuring at */
	public int yCoord;

	/** Z position this tick is occuring at */
	public int zCoord;

	/**
	 * blockID of the scheduled tick (ensures when the tick occurs its still for
	 * this block)
	 */
	public int blockID;

	/** Time this tick is scheduled to occur at */
	public long scheduledTime;
	public int field_82754_f;

	/** The id of the tick entry */
	private final long tickEntryID;

	public NextTickListEntry(final int par1, final int par2, final int par3,
			final int par4) {
		tickEntryID = NextTickListEntry.nextTickEntryID++;
		xCoord = par1;
		yCoord = par2;
		zCoord = par3;
		blockID = par4;
	}

	@Override
	public boolean equals(final Object par1Obj) {
		if (!(par1Obj instanceof NextTickListEntry)) {
			return false;
		} else {
			final NextTickListEntry var2 = (NextTickListEntry) par1Obj;
			return xCoord == var2.xCoord && yCoord == var2.yCoord
					&& zCoord == var2.zCoord
					&& Block.isAssociatedBlockID(blockID, var2.blockID);
		}
	}

	@Override
	public int hashCode() {
		return (xCoord * 1024 * 1024 + zCoord * 1024 + yCoord) * 256;
	}

	/**
	 * Sets the scheduled time for this tick entry
	 */
	public NextTickListEntry setScheduledTime(final long par1) {
		scheduledTime = par1;
		return this;
	}

	public void func_82753_a(final int par1) {
		field_82754_f = par1;
	}

	/**
	 * Compares this tick entry to another tick entry for sorting purposes.
	 * Compared first based on the scheduled time and second based on
	 * tickEntryID.
	 */
	public int comparer(final NextTickListEntry par1NextTickListEntry) {
		return scheduledTime < par1NextTickListEntry.scheduledTime ? -1
				: scheduledTime > par1NextTickListEntry.scheduledTime ? 1
						: field_82754_f != par1NextTickListEntry.field_82754_f ? field_82754_f
								- par1NextTickListEntry.field_82754_f
								: tickEntryID < par1NextTickListEntry.tickEntryID ? -1
										: tickEntryID > par1NextTickListEntry.tickEntryID ? 1
												: 0;
	}

	@Override
	public String toString() {
		return blockID + ": (" + xCoord + ", " + yCoord + ", " + zCoord + "), "
				+ scheduledTime + ", " + field_82754_f + ", " + tickEntryID;
	}

	@Override
	public int compareTo(final Object par1Obj) {
		return comparer((NextTickListEntry) par1Obj);
	}
}
