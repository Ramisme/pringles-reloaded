package net.minecraft.src;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import net.minecraft.server.MinecraftServer;

public class WorldServerOF extends WorldServer {
	private NextTickHashSet nextTickHashSet = null;
	private TreeSet pendingTickList = null;

	public WorldServerOF(final MinecraftServer var1, final ISaveHandler var2,
			final String var3, final int var4, final WorldSettings var5,
			final Profiler var6, final ILogAgent var7) {
		super(var1, var2, var3, var4, var5, var6, var7);
		fixSetNextTicks();
	}

	private void fixSetNextTicks() {
		try {
			final Field[] var1 = WorldServer.class.getDeclaredFields();

			if (var1.length > 5) {
				final Field var2 = var1[3];
				var2.setAccessible(true);

				if (var2.getType() == Set.class) {
					final Set var3 = (Set) var2.get(this);
					final NextTickHashSet var4 = new NextTickHashSet(var3);
					var2.set(this, var4);
					final Field var5 = var1[4];
					var5.setAccessible(true);
					pendingTickList = (TreeSet) var5.get(this);
					nextTickHashSet = var4;
				}
			}
		} catch (final Exception var6) {
			Config.dbg("Error setting WorldServer.nextTickSet: "
					+ var6.getMessage());
		}
	}

	@Override
	public List getPendingBlockUpdates(final Chunk var1, final boolean var2) {
		if (nextTickHashSet != null && pendingTickList != null) {
			ArrayList var3 = null;
			final ChunkCoordIntPair var4 = var1.getChunkCoordIntPair();
			final int var5 = var4.chunkXPos << 4;
			final int var6 = var5 + 16;
			final int var7 = var4.chunkZPos << 4;
			final int var8 = var7 + 16;
			final Iterator var9 = nextTickHashSet.getNextTickEntries(
					var4.chunkXPos, var4.chunkZPos);

			while (var9.hasNext()) {
				final NextTickListEntry var10 = (NextTickListEntry) var9.next();

				if (var10.xCoord >= var5 && var10.xCoord < var6
						&& var10.zCoord >= var7 && var10.zCoord < var8) {
					if (var2) {
						pendingTickList.remove(var10);
						var9.remove();
					}

					if (var3 == null) {
						var3 = new ArrayList();
					}

					var3.add(var10);
				} else {
					Config.dbg("Not matching: " + var5 + "," + var7);
				}
			}

			return var3;
		} else {
			return super.getPendingBlockUpdates(var1, var2);
		}
	}

	/**
	 * Updates all weather states.
	 */
	@Override
	protected void updateWeather() {
		if (Config.isWeatherEnabled()) {
			super.updateWeather();
		} else {
			fixWorldWeather();
		}

		if (!Config.isTimeDefault()) {
			fixWorldTime();
		}
	}

	private void fixWorldWeather() {
		if (worldInfo.isRaining() || worldInfo.isThundering()) {
			worldInfo.setRainTime(0);
			worldInfo.setRaining(false);
			setRainStrength(0.0F);
			worldInfo.setThunderTime(0);
			worldInfo.setThundering(false);
			getMinecraftServer().getConfigurationManager()
					.sendPacketToAllPlayers(new Packet70GameEvent(2, 0));
		}
	}

	private void fixWorldTime() {
		if (worldInfo.getGameType().getID() == 1) {
			final long var1 = getWorldTime();
			final long var3 = var1 % 24000L;

			if (Config.isTimeDayOnly()) {
				if (var3 <= 1000L) {
					setWorldTime(var1 - var3 + 1001L);
				}

				if (var3 >= 11000L) {
					setWorldTime(var1 - var3 + 24001L);
				}
			}

			if (Config.isTimeNightOnly()) {
				if (var3 <= 14000L) {
					setWorldTime(var1 - var3 + 14001L);
				}

				if (var3 >= 22000L) {
					setWorldTime(var1 - var3 + 24000L + 14001L);
				}
			}
		}
	}
}
