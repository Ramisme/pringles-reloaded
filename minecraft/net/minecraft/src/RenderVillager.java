package net.minecraft.src;

import org.lwjgl.opengl.GL11;

public class RenderVillager extends RenderLiving {
	/** Model of the villager. */
	protected ModelVillager villagerModel;

	public RenderVillager() {
		super(new ModelVillager(0.0F), 0.5F);
		villagerModel = (ModelVillager) mainModel;
	}

	/**
	 * Determines wether Villager Render pass or not.
	 */
	protected int shouldVillagerRenderPass(
			final EntityVillager par1EntityVillager, final int par2,
			final float par3) {
		return -1;
	}

	public void renderVillager(final EntityVillager par1EntityVillager,
			final double par2, final double par4, final double par6,
			final float par8, final float par9) {
		super.doRenderLiving(par1EntityVillager, par2, par4, par6, par8, par9);
	}

	protected void renderVillagerEquipedItems(
			final EntityVillager par1EntityVillager, final float par2) {
		super.renderEquippedItems(par1EntityVillager, par2);
	}

	protected void preRenderVillager(final EntityVillager par1EntityVillager,
			final float par2) {
		float var3 = 0.9375F;

		if (par1EntityVillager.getGrowingAge() < 0) {
			var3 = (float) (var3 * 0.5D);
			shadowSize = 0.25F;
		} else {
			shadowSize = 0.5F;
		}

		GL11.glScalef(var3, var3, var3);
	}

	/**
	 * Allows the render to do any OpenGL state modifications necessary before
	 * the model is rendered. Args: entityLiving, partialTickTime
	 */
	@Override
	protected void preRenderCallback(final EntityLiving par1EntityLiving,
			final float par2) {
		preRenderVillager((EntityVillager) par1EntityLiving, par2);
	}

	/**
	 * Queries whether should render the specified pass or not.
	 */
	@Override
	protected int shouldRenderPass(final EntityLiving par1EntityLiving,
			final int par2, final float par3) {
		return shouldVillagerRenderPass((EntityVillager) par1EntityLiving,
				par2, par3);
	}

	@Override
	protected void renderEquippedItems(final EntityLiving par1EntityLiving,
			final float par2) {
		renderVillagerEquipedItems((EntityVillager) par1EntityLiving, par2);
	}

	@Override
	public void doRenderLiving(final EntityLiving par1EntityLiving,
			final double par2, final double par4, final double par6,
			final float par8, final float par9) {
		renderVillager((EntityVillager) par1EntityLiving, par2, par4, par6,
				par8, par9);
	}

	/**
	 * Actually renders the given argument. This is a synthetic bridge method,
	 * always casting down its argument and then handing it off to a worker
	 * function which does the actual work. In all probabilty, the class Render
	 * is generic (Render<T extends Entity) and this method has signature public
	 * void doRender(T entity, double d, double d1, double d2, float f, float
	 * f1). But JAD is pre 1.5 so doesn't do that.
	 */
	@Override
	public void doRender(final Entity par1Entity, final double par2,
			final double par4, final double par6, final float par8,
			final float par9) {
		renderVillager((EntityVillager) par1Entity, par2, par4, par6, par8,
				par9);
	}
}
