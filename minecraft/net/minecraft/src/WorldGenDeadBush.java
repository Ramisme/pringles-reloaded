package net.minecraft.src;

import java.util.Random;

public class WorldGenDeadBush extends WorldGenerator {
	/** stores the ID for WorldGenDeadBush */
	private final int deadBushID;

	public WorldGenDeadBush(final int par1) {
		deadBushID = par1;
	}

	@Override
	public boolean generate(final World par1World, final Random par2Random,
			final int par3, int par4, final int par5) {
		int var11;

		for (; ((var11 = par1World.getBlockId(par3, par4, par5)) == 0 || var11 == Block.leaves.blockID)
				&& par4 > 0; --par4) {
			;
		}

		for (int var7 = 0; var7 < 4; ++var7) {
			final int var8 = par3 + par2Random.nextInt(8)
					- par2Random.nextInt(8);
			final int var9 = par4 + par2Random.nextInt(4)
					- par2Random.nextInt(4);
			final int var10 = par5 + par2Random.nextInt(8)
					- par2Random.nextInt(8);

			if (par1World.isAirBlock(var8, var9, var10)
					&& Block.blocksList[deadBushID].canBlockStay(par1World,
							var8, var9, var10)) {
				par1World.setBlock(var8, var9, var10, deadBushID, 0, 2);
			}
		}

		return true;
	}
}
