package net.minecraft.src;

import java.util.List;
import java.util.Random;

public class ComponentVillageField2 extends ComponentVillage {
	private int averageGroundLevel = -1;

	/** First crop type for this field. */
	private final int cropTypeA;

	/** Second crop type for this field. */
	private final int cropTypeB;

	public ComponentVillageField2(
			final ComponentVillageStartPiece par1ComponentVillageStartPiece,
			final int par2, final Random par3Random,
			final StructureBoundingBox par4StructureBoundingBox, final int par5) {
		super(par1ComponentVillageStartPiece, par2);
		coordBaseMode = par5;
		boundingBox = par4StructureBoundingBox;
		cropTypeA = pickRandomCrop(par3Random);
		cropTypeB = pickRandomCrop(par3Random);
	}

	/**
	 * Returns a crop type to be planted on this field.
	 */
	private int pickRandomCrop(final Random par1Random) {
		switch (par1Random.nextInt(5)) {
		case 0:
			return Block.carrot.blockID;

		case 1:
			return Block.potato.blockID;

		default:
			return Block.crops.blockID;
		}
	}

	public static ComponentVillageField2 func_74902_a(
			final ComponentVillageStartPiece par0ComponentVillageStartPiece,
			final List par1List, final Random par2Random, final int par3,
			final int par4, final int par5, final int par6, final int par7) {
		final StructureBoundingBox var8 = StructureBoundingBox
				.getComponentToAddBoundingBox(par3, par4, par5, 0, 0, 0, 7, 4,
						9, par6);
		return ComponentVillage.canVillageGoDeeper(var8)
				&& StructureComponent.findIntersecting(par1List, var8) == null ? new ComponentVillageField2(
				par0ComponentVillageStartPiece, par7, par2Random, var8, par6)
				: null;
	}

	/**
	 * second Part of Structure generating, this for example places Spiderwebs,
	 * Mob Spawners, it closes Mineshafts at the end, it adds Fences...
	 */
	@Override
	public boolean addComponentParts(final World par1World,
			final Random par2Random,
			final StructureBoundingBox par3StructureBoundingBox) {
		if (averageGroundLevel < 0) {
			averageGroundLevel = getAverageGroundLevel(par1World,
					par3StructureBoundingBox);

			if (averageGroundLevel < 0) {
				return true;
			}

			boundingBox.offset(0,
					averageGroundLevel - boundingBox.maxY + 4 - 1, 0);
		}

		fillWithBlocks(par1World, par3StructureBoundingBox, 0, 1, 0, 6, 4, 8,
				0, 0, false);
		fillWithBlocks(par1World, par3StructureBoundingBox, 1, 0, 1, 2, 0, 7,
				Block.tilledField.blockID, Block.tilledField.blockID, false);
		fillWithBlocks(par1World, par3StructureBoundingBox, 4, 0, 1, 5, 0, 7,
				Block.tilledField.blockID, Block.tilledField.blockID, false);
		fillWithBlocks(par1World, par3StructureBoundingBox, 0, 0, 0, 0, 0, 8,
				Block.wood.blockID, Block.wood.blockID, false);
		fillWithBlocks(par1World, par3StructureBoundingBox, 6, 0, 0, 6, 0, 8,
				Block.wood.blockID, Block.wood.blockID, false);
		fillWithBlocks(par1World, par3StructureBoundingBox, 1, 0, 0, 5, 0, 0,
				Block.wood.blockID, Block.wood.blockID, false);
		fillWithBlocks(par1World, par3StructureBoundingBox, 1, 0, 8, 5, 0, 8,
				Block.wood.blockID, Block.wood.blockID, false);
		fillWithBlocks(par1World, par3StructureBoundingBox, 3, 0, 1, 3, 0, 7,
				Block.waterMoving.blockID, Block.waterMoving.blockID, false);
		int var4;

		for (var4 = 1; var4 <= 7; ++var4) {
			placeBlockAtCurrentPosition(par1World, cropTypeA,
					MathHelper.getRandomIntegerInRange(par2Random, 2, 7), 1, 1,
					var4, par3StructureBoundingBox);
			placeBlockAtCurrentPosition(par1World, cropTypeA,
					MathHelper.getRandomIntegerInRange(par2Random, 2, 7), 2, 1,
					var4, par3StructureBoundingBox);
			placeBlockAtCurrentPosition(par1World, cropTypeB,
					MathHelper.getRandomIntegerInRange(par2Random, 2, 7), 4, 1,
					var4, par3StructureBoundingBox);
			placeBlockAtCurrentPosition(par1World, cropTypeB,
					MathHelper.getRandomIntegerInRange(par2Random, 2, 7), 5, 1,
					var4, par3StructureBoundingBox);
		}

		for (var4 = 0; var4 < 9; ++var4) {
			for (int var5 = 0; var5 < 7; ++var5) {
				clearCurrentPositionBlocksUpwards(par1World, var5, 4, var4,
						par3StructureBoundingBox);
				fillCurrentPositionBlocksDownwards(par1World,
						Block.dirt.blockID, 0, var5, -1, var4,
						par3StructureBoundingBox);
			}
		}

		return true;
	}
}
