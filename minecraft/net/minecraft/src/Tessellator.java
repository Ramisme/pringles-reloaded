package net.minecraft.src;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

import org.lwjgl.opengl.ARBBufferObject;
import org.lwjgl.opengl.ARBVertexBufferObject;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLContext;
import org.ramisme.pringles.modules.world.wallhack.WallhackUtil;

public class Tessellator {
	/**
	 * Boolean used to check whether quads should be drawn as two triangles.
	 * Initialized to false and never changed.
	 */
	private static boolean convertQuadsToTriangles = false;

	/**
	 * Boolean used to check if we should use vertex buffers. Initialized to
	 * false and never changed.
	 */
	private static boolean tryVBO = false;

	/** The byte buffer used for GL allocation. */
	private ByteBuffer byteBuffer;

	/** The same memory as byteBuffer, but referenced as an integer buffer. */
	private IntBuffer intBuffer;

	/** The same memory as byteBuffer, but referenced as an float buffer. */
	private FloatBuffer floatBuffer;

	/** Short buffer */
	private ShortBuffer shortBuffer;

	/** Raw integer array. */
	private int[] rawBuffer;

	/**
	 * The number of vertices to be drawn in the next draw call. Reset to 0
	 * between draw calls.
	 */
	private int vertexCount;

	/** The first coordinate to be used for the texture. */
	private double textureU;

	/** The second coordinate to be used for the texture. */
	private double textureV;
	private int brightness;

	/** The color (RGBA) value to be used for the following draw call. */
	private int color;

	/**
	 * Whether the current draw object for this tessellator has color values.
	 */
	private boolean hasColor;

	/**
	 * Whether the current draw object for this tessellator has texture
	 * coordinates.
	 */
	private boolean hasTexture;
	private boolean hasBrightness;

	/**
	 * Whether the current draw object for this tessellator has normal values.
	 */
	private boolean hasNormals;

	/** The index into the raw buffer to be used for the next data. */
	private int rawBufferIndex;

	/**
	 * The number of vertices manually added to the given draw call. This
	 * differs from vertexCount because it adds extra vertices when converting
	 * quads to triangles.
	 */
	private int addedVertices;

	/** Disables all color information for the following draw call. */
	private boolean isColorDisabled;

	/** The draw mode currently being used by the tessellator. */
	public int drawMode;

	/**
	 * An offset to be applied along the x-axis for all vertices in this draw
	 * call.
	 */
	public double xOffset;

	/**
	 * An offset to be applied along the y-axis for all vertices in this draw
	 * call.
	 */
	public double yOffset;

	/**
	 * An offset to be applied along the z-axis for all vertices in this draw
	 * call.
	 */
	public double zOffset;

	/** The normal to be applied to the face being drawn. */
	private int normal;

	/** The static instance of the Tessellator. */
	public static Tessellator instance = new Tessellator(524288);

	/** Whether this tessellator is currently in draw mode. */
	public boolean isDrawing;

	/** Whether we are currently using VBO or not. */
	private boolean useVBO;

	/** An IntBuffer used to store the indices of vertex buffer objects. */
	private IntBuffer vertexBuffers;

	/**
	 * The index of the last VBO used. This is used in round-robin fashion,
	 * sequentially, through the vboCount vertex buffers.
	 */
	private int vboIndex;

	/** Number of vertex buffer objects allocated for use. */
	private final int vboCount;

	/** The size of the buffers used (in integers). */
	private int bufferSize;
	private boolean renderingChunk;
	private static boolean littleEndianByteOrder = ByteOrder.nativeOrder() == ByteOrder.LITTLE_ENDIAN;
	public static boolean renderingWorldRenderer = false;
	public boolean defaultTexture;
	public int textureID;
	public boolean autoGrow;
	private final RenderEngine renderEngine;
	private VertexData[] vertexDatas;
	private TextureStitched[] vertexQuadIcons;

	public Tessellator() {
		this(65536);
		defaultTexture = false;
	}

	public Tessellator(final int par1) {
		renderingChunk = false;
		defaultTexture = true;
		textureID = 0;
		autoGrow = true;
		renderEngine = Config.getRenderEngine();
		vertexDatas = null;
		vertexQuadIcons = null;
		vertexCount = 0;
		hasColor = false;
		hasTexture = false;
		hasBrightness = false;
		hasNormals = false;
		rawBufferIndex = 0;
		addedVertices = 0;
		isColorDisabled = false;
		isDrawing = false;
		useVBO = false;
		vboIndex = 0;
		vboCount = 10;
		bufferSize = par1;
		byteBuffer = GLAllocation.createDirectByteBuffer(par1 * 4);
		intBuffer = byteBuffer.asIntBuffer();
		floatBuffer = byteBuffer.asFloatBuffer();
		shortBuffer = byteBuffer.asShortBuffer();
		rawBuffer = new int[par1];
		useVBO = Tessellator.tryVBO
				&& GLContext.getCapabilities().GL_ARB_vertex_buffer_object;

		if (useVBO) {
			vertexBuffers = GLAllocation.createDirectIntBuffer(vboCount);
			ARBBufferObject.glGenBuffersARB(vertexBuffers);
		}

		vertexDatas = new VertexData[4];

		for (int var2 = 0; var2 < vertexDatas.length; ++var2) {
			vertexDatas[var2] = new VertexData();
		}
	}

	private void draw(final int var1, final int var2) {
		final int var3 = var2 - var1;

		if (var3 > 0) {
			final int var4 = var1 * 4;
			final int var5 = var3 * 4;

			if (useVBO) {
				throw new IllegalStateException("VBO not implemented");
			} else {
				floatBuffer.position(3);
				GL11.glTexCoordPointer(2, 32, floatBuffer);
				OpenGlHelper
						.setClientActiveTexture(OpenGlHelper.lightmapTexUnit);
				shortBuffer.position(14);
				GL11.glTexCoordPointer(2, 32, shortBuffer);
				GL11.glEnableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
				OpenGlHelper
						.setClientActiveTexture(OpenGlHelper.defaultTexUnit);
				byteBuffer.position(20);
				GL11.glColorPointer(4, true, 32, byteBuffer);
				floatBuffer.position(0);
				GL11.glVertexPointer(3, 32, floatBuffer);

				if (drawMode == 7 && Tessellator.convertQuadsToTriangles) {
					GL11.glDrawArrays(GL11.GL_TRIANGLES, var4, var5);
				} else {
					GL11.glDrawArrays(drawMode, var4, var5);
				}
			}
		}
	}

	/**
	 * Draws the data set up in this tessellator and resets the state to prepare
	 * for new drawing.
	 */
	public int draw() {
		if (!isDrawing) {
			throw new IllegalStateException("Not tesselating!");
		} else {
			isDrawing = false;

			if (vertexCount > 0) {
				intBuffer.clear();
				intBuffer.put(rawBuffer, 0, rawBufferIndex);
				byteBuffer.position(0);
				byteBuffer.limit(rawBufferIndex * 4);

				if (useVBO) {
					vboIndex = (vboIndex + 1) % vboCount;
					ARBBufferObject.glBindBufferARB(
							ARBVertexBufferObject.GL_ARRAY_BUFFER_ARB,
							vertexBuffers.get(vboIndex));
					ARBBufferObject.glBufferDataARB(
							ARBVertexBufferObject.GL_ARRAY_BUFFER_ARB,
							byteBuffer, ARBBufferObject.GL_STREAM_DRAW_ARB);
				}

				if (hasTexture) {
					if (useVBO) {
						GL11.glTexCoordPointer(2, GL11.GL_FLOAT, 32, 12L);
					} else {
						floatBuffer.position(3);
						GL11.glTexCoordPointer(2, 32, floatBuffer);
					}

					GL11.glEnableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
				}

				if (hasBrightness) {
					OpenGlHelper
							.setClientActiveTexture(OpenGlHelper.lightmapTexUnit);

					if (useVBO) {
						GL11.glTexCoordPointer(2, GL11.GL_SHORT, 32, 28L);
					} else {
						shortBuffer.position(14);
						GL11.glTexCoordPointer(2, 32, shortBuffer);
					}

					GL11.glEnableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
					OpenGlHelper
							.setClientActiveTexture(OpenGlHelper.defaultTexUnit);
				}

				if (hasColor) {
					if (useVBO) {
						GL11.glColorPointer(4, GL11.GL_UNSIGNED_BYTE, 32, 20L);
					} else {
						byteBuffer.position(20);
						GL11.glColorPointer(4, true, 32, byteBuffer);
					}

					GL11.glEnableClientState(GL11.GL_COLOR_ARRAY);
				}

				if (hasNormals) {
					if (useVBO) {
						GL11.glNormalPointer(GL11.GL_UNSIGNED_BYTE, 32, 24L);
					} else {
						byteBuffer.position(24);
						GL11.glNormalPointer(32, byteBuffer);
					}

					GL11.glEnableClientState(GL11.GL_NORMAL_ARRAY);
				}

				if (useVBO) {
					GL11.glVertexPointer(3, GL11.GL_FLOAT, 32, 0L);
				} else {
					floatBuffer.position(0);
					GL11.glVertexPointer(3, 32, floatBuffer);
				}

				GL11.glEnableClientState(GL11.GL_VERTEX_ARRAY);

				if (drawMode == 7 && Tessellator.convertQuadsToTriangles) {
					GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, vertexCount);
				} else {
					GL11.glDrawArrays(drawMode, 0, vertexCount);
				}

				GL11.glDisableClientState(GL11.GL_VERTEX_ARRAY);

				if (hasTexture) {
					GL11.glDisableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
				}

				if (hasBrightness) {
					OpenGlHelper
							.setClientActiveTexture(OpenGlHelper.lightmapTexUnit);
					GL11.glDisableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
					OpenGlHelper
							.setClientActiveTexture(OpenGlHelper.defaultTexUnit);
				}

				if (hasColor) {
					GL11.glDisableClientState(GL11.GL_COLOR_ARRAY);
				}

				if (hasNormals) {
					GL11.glDisableClientState(GL11.GL_NORMAL_ARRAY);
				}
			}

			final int var1 = rawBufferIndex * 4;
			reset();
			return var1;
		}
	}

	/**
	 * Clears the tessellator state in preparation for new drawing.
	 */
	private void reset() {
		vertexCount = 0;
		byteBuffer.clear();
		rawBufferIndex = 0;
		addedVertices = 0;
	}

	/**
	 * Sets draw mode in the tessellator to draw quads.
	 */
	public void startDrawingQuads() {
		startDrawing(7);
	}

	/**
	 * Resets tessellator state and prepares for drawing (with the specified
	 * draw mode).
	 */
	public void startDrawing(final int par1) {
		if (isDrawing) {
			throw new IllegalStateException("Already tesselating!");
		} else {
			isDrawing = true;
			reset();
			drawMode = par1;
			hasNormals = false;
			hasColor = false;
			hasTexture = false;
			hasBrightness = false;
			isColorDisabled = false;
		}
	}

	/**
	 * Sets the texture coordinates.
	 */
	public void setTextureUV(final double par1, final double par3) {
		hasTexture = true;
		textureU = par1;
		textureV = par3;
	}

	public void setBrightness(final int par1) {
		hasBrightness = true;
		brightness = par1;
	}

	/**
	 * Sets the RGB values as specified, converting from floats between 0 and 1
	 * to integers from 0-255.
	 */
	public void setColorOpaque_F(final float par1, final float par2,
			final float par3) {
		setColorOpaque((int) (par1 * 255.0F), (int) (par2 * 255.0F),
				(int) (par3 * 255.0F));
	}

	/**
	 * Sets the RGBA values for the color, converting from floats between 0 and
	 * 1 to integers from 0-255.
	 */
	public void setColorRGBA_F(final float par1, final float par2,
			final float par3, final float par4) {
		setColorRGBA((int) (par1 * 255.0F), (int) (par2 * 255.0F),
				(int) (par3 * 255.0F), (int) (par4 * 255.0F));
	}

	/**
	 * Sets the RGB values as specified, and sets alpha to opaque.
	 */
	public void setColorOpaque(final int par1, final int par2, final int par3) {
		// TODO: Tessellator#setColorOpaque
		setColorRGBA(par1, par2, par3,
				WallhackUtil.getInstance().isEnabled() ? WallhackUtil
						.getInstance().getOpacity() : 255);
	}

	/**
	 * Sets the RGBA values for the color. Also clamps them to 0-255.
	 */
	public void setColorRGBA(int par1, int par2, int par3, int par4) {
		if (!isColorDisabled) {
			if (par1 > 255) {
				par1 = 255;
			}

			if (par2 > 255) {
				par2 = 255;
			}

			if (par3 > 255) {
				par3 = 255;
			}

			if (par4 > 255) {
				par4 = 255;
			}

			if (par1 < 0) {
				par1 = 0;
			}

			if (par2 < 0) {
				par2 = 0;
			}

			if (par3 < 0) {
				par3 = 0;
			}

			if (par4 < 0) {
				par4 = 0;
			}

			hasColor = true;

			if (Tessellator.littleEndianByteOrder) {
				color = par4 << 24 | par3 << 16 | par2 << 8 | par1;
			} else {
				color = par1 << 24 | par2 << 16 | par3 << 8 | par4;
			}
		}
	}

	/**
	 * Adds a vertex specifying both x,y,z and the texture u,v for it.
	 */
	public void addVertexWithUV(final double par1, final double par3,
			final double par5, final double par7, final double par9) {
		setTextureUV(par7, par9);
		addVertex(par1, par3, par5);
	}

	/**
	 * Adds a vertex with the specified x,y,z to the current draw call. It will
	 * trigger a draw() if the buffer gets full.
	 */
	public void addVertex(final double par1, final double par3,
			final double par5) {
		if (autoGrow && rawBufferIndex >= bufferSize - 32) {
			Config.dbg("Expand tessellator buffer, old: " + bufferSize
					+ ", new: " + bufferSize * 2);
			bufferSize *= 2;
			final int[] var7 = new int[bufferSize];
			System.arraycopy(rawBuffer, 0, var7, 0, rawBuffer.length);
			rawBuffer = var7;
			byteBuffer = GLAllocation.createDirectByteBuffer(bufferSize * 4);
			intBuffer = byteBuffer.asIntBuffer();
			floatBuffer = byteBuffer.asFloatBuffer();
			shortBuffer = byteBuffer.asShortBuffer();

			if (vertexQuadIcons != null) {
				final TextureStitched[] var8 = new TextureStitched[bufferSize / 4];
				System.arraycopy(vertexQuadIcons, 0, var8, 0,
						vertexQuadIcons.length);
				vertexQuadIcons = var8;
			}
		}

		++addedVertices;

		if (drawMode == 7 && Tessellator.convertQuadsToTriangles
				&& addedVertices % 4 == 0) {
			for (int var9 = 0; var9 < 2; ++var9) {
				final int var10 = 8 * (3 - var9);

				if (hasTexture) {
					rawBuffer[rawBufferIndex + 3] = rawBuffer[rawBufferIndex
							- var10 + 3];
					rawBuffer[rawBufferIndex + 4] = rawBuffer[rawBufferIndex
							- var10 + 4];
				}

				if (hasBrightness) {
					rawBuffer[rawBufferIndex + 7] = rawBuffer[rawBufferIndex
							- var10 + 7];
				}

				if (hasColor) {
					rawBuffer[rawBufferIndex + 5] = rawBuffer[rawBufferIndex
							- var10 + 5];
				}

				rawBuffer[rawBufferIndex + 0] = rawBuffer[rawBufferIndex
						- var10 + 0];
				rawBuffer[rawBufferIndex + 1] = rawBuffer[rawBufferIndex
						- var10 + 1];
				rawBuffer[rawBufferIndex + 2] = rawBuffer[rawBufferIndex
						- var10 + 2];
				++vertexCount;
				rawBufferIndex += 8;
			}
		}

		if (hasTexture) {
			rawBuffer[rawBufferIndex + 3] = Float
					.floatToRawIntBits((float) textureU);
			rawBuffer[rawBufferIndex + 4] = Float
					.floatToRawIntBits((float) textureV);
		}

		if (hasBrightness) {
			rawBuffer[rawBufferIndex + 7] = brightness;
		}

		if (hasColor) {
			rawBuffer[rawBufferIndex + 5] = color;
		}

		if (hasNormals) {
			rawBuffer[rawBufferIndex + 6] = normal;
		}

		rawBuffer[rawBufferIndex + 0] = Float
				.floatToRawIntBits((float) (par1 + xOffset));
		rawBuffer[rawBufferIndex + 1] = Float
				.floatToRawIntBits((float) (par3 + yOffset));
		rawBuffer[rawBufferIndex + 2] = Float
				.floatToRawIntBits((float) (par5 + zOffset));
		rawBufferIndex += 8;
		++vertexCount;

		if (!autoGrow && addedVertices % 4 == 0
				&& rawBufferIndex >= bufferSize - 32) {
			this.draw();
			isDrawing = true;
		}
	}

	/**
	 * Sets the color to the given opaque value (stored as byte values packed in
	 * an integer).
	 */
	public void setColorOpaque_I(final int par1) {
		final int var2 = par1 >> 16 & 255;
		final int var3 = par1 >> 8 & 255;
		final int var4 = par1 & 255;
		setColorOpaque(var2, var3, var4);
	}

	/**
	 * Sets the color to the given color (packed as bytes in integer) and alpha
	 * values.
	 */
	public void setColorRGBA_I(final int par1, final int par2) {
		final int var3 = par1 >> 16 & 255;
		final int var4 = par1 >> 8 & 255;
		final int var5 = par1 & 255;
		setColorRGBA(var3, var4, var5, par2);
	}

	/**
	 * Disables colors for the current draw call.
	 */
	public void disableColor() {
		isColorDisabled = true;
	}

	/**
	 * Sets the normal for the current draw call.
	 */
	public void setNormal(final float par1, final float par2, final float par3) {
		hasNormals = true;
		final byte var4 = (byte) (int) (par1 * 127.0F);
		final byte var5 = (byte) (int) (par2 * 127.0F);
		final byte var6 = (byte) (int) (par3 * 127.0F);
		normal = var4 & 255 | (var5 & 255) << 8 | (var6 & 255) << 16;
	}

	/**
	 * Sets the translation for all vertices in the current draw call.
	 */
	public void setTranslation(final double par1, final double par3,
			final double par5) {
		xOffset = par1;
		yOffset = par3;
		zOffset = par5;
	}

	/**
	 * Offsets the translation for all vertices in the current draw call.
	 */
	public void addTranslation(final float par1, final float par2,
			final float par3) {
		xOffset += par1;
		yOffset += par2;
		zOffset += par3;
	}

	public boolean isRenderingChunk() {
		return renderingChunk;
	}

	public void setRenderingChunk(final boolean var1) {
		renderingChunk = var1;
	}
}
