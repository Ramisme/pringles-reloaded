package net.minecraft.src;

class GuiSnooperList extends GuiSlot {
	final GuiSnooper snooperGui;

	public GuiSnooperList(final GuiSnooper par1GuiSnooper) {
		super(par1GuiSnooper.mc, par1GuiSnooper.width, par1GuiSnooper.height,
				80, par1GuiSnooper.height - 40,
				par1GuiSnooper.fontRenderer.FONT_HEIGHT + 1);
		snooperGui = par1GuiSnooper;
	}

	/**
	 * Gets the size of the current slot list.
	 */
	@Override
	protected int getSize() {
		return GuiSnooper.func_74095_a(snooperGui).size();
	}

	/**
	 * the element in the slot that was clicked, boolean for wether it was
	 * double clicked or not
	 */
	@Override
	protected void elementClicked(final int par1, final boolean par2) {
	}

	/**
	 * returns true if the element passed in is currently selected
	 */
	@Override
	protected boolean isSelected(final int par1) {
		return false;
	}

	@Override
	protected void drawBackground() {
	}

	@Override
	protected void drawSlot(final int par1, final int par2, final int par3,
			final int par4, final Tessellator par5Tessellator) {
		snooperGui.fontRenderer.drawString(
				(String) GuiSnooper.func_74095_a(snooperGui).get(par1), 10,
				par3, 16777215);
		snooperGui.fontRenderer.drawString(
				(String) GuiSnooper.func_74094_b(snooperGui).get(par1), 230,
				par3, 16777215);
	}

	@Override
	protected int getScrollBarX() {
		return snooperGui.width - 10;
	}
}
