package net.minecraft.src;

public class EntityTNTPrimed extends Entity {
	/** How long the fuse is */
	public int fuse;
	private EntityLiving tntPlacedBy;

	public EntityTNTPrimed(final World par1World) {
		super(par1World);
		fuse = 0;
		preventEntitySpawning = true;
		setSize(0.98F, 0.98F);
		yOffset = height / 2.0F;
	}

	public EntityTNTPrimed(final World par1World, final double par2,
			final double par4, final double par6,
			final EntityLiving par8EntityLiving) {
		this(par1World);
		setPosition(par2, par4, par6);
		final float var9 = (float) (Math.random() * Math.PI * 2.0D);
		motionX = -((float) Math.sin(var9)) * 0.02F;
		motionY = 0.20000000298023224D;
		motionZ = -((float) Math.cos(var9)) * 0.02F;
		fuse = 80;
		prevPosX = par2;
		prevPosY = par4;
		prevPosZ = par6;
		tntPlacedBy = par8EntityLiving;
	}

	@Override
	protected void entityInit() {
	}

	/**
	 * returns if this entity triggers Block.onEntityWalking on the blocks they
	 * walk on. used for spiders and wolves to prevent them from trampling crops
	 */
	@Override
	protected boolean canTriggerWalking() {
		return false;
	}

	/**
	 * Returns true if other Entities should be prevented from moving through
	 * this Entity.
	 */
	@Override
	public boolean canBeCollidedWith() {
		return !isDead;
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate() {
		prevPosX = posX;
		prevPosY = posY;
		prevPosZ = posZ;
		motionY -= 0.03999999910593033D;
		moveEntity(motionX, motionY, motionZ);
		motionX *= 0.9800000190734863D;
		motionY *= 0.9800000190734863D;
		motionZ *= 0.9800000190734863D;

		if (onGround) {
			motionX *= 0.699999988079071D;
			motionZ *= 0.699999988079071D;
			motionY *= -0.5D;
		}

		if (fuse-- <= 0) {
			setDead();

			if (!worldObj.isRemote) {
				explode();
			}
		} else {
			worldObj.spawnParticle("smoke", posX, posY + 0.5D, posZ, 0.0D,
					0.0D, 0.0D);
		}
	}

	private void explode() {
		final float var1 = 4.0F;
		worldObj.createExplosion(this, posX, posY, posZ, var1, true);
	}

	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	@Override
	protected void writeEntityToNBT(final NBTTagCompound par1NBTTagCompound) {
		par1NBTTagCompound.setByte("Fuse", (byte) fuse);
	}

	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	@Override
	protected void readEntityFromNBT(final NBTTagCompound par1NBTTagCompound) {
		fuse = par1NBTTagCompound.getByte("Fuse");
	}

	@Override
	public float getShadowSize() {
		return 0.0F;
	}

	/**
	 * returns null or the entityliving it was placed or ignited by
	 */
	public EntityLiving getTntPlacedBy() {
		return tntPlacedBy;
	}
}
