package net.minecraft.src;

public class BlockRail extends BlockRailBase {
	private Icon theIcon;

	protected BlockRail(final int par1) {
		super(par1, false);
	}

	/**
	 * From the specified side and block metadata retrieves the blocks texture.
	 * Args: side, metadata
	 */
	@Override
	public Icon getIcon(final int par1, final int par2) {
		return par2 >= 6 ? theIcon : blockIcon;
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		super.registerIcons(par1IconRegister);
		theIcon = par1IconRegister.registerIcon("rail_turn");
	}

	@Override
	protected void func_94358_a(final World par1World, final int par2,
			final int par3, final int par4, final int par5, final int par6,
			final int par7) {
		if (par7 > 0
				&& Block.blocksList[par7].canProvidePower()
				&& new BlockBaseRailLogic(this, par1World, par2, par3, par4)
						.getNumberOfAdjacentTracks() == 3) {
			refreshTrackShape(par1World, par2, par3, par4, false);
		}
	}
}
