package net.minecraft.src;

import java.util.concurrent.Callable;

class CallableItemName implements Callable {
	final ItemStack theItemStack;

	final InventoryPlayer playerInventory;

	CallableItemName(final InventoryPlayer par1InventoryPlayer,
			final ItemStack par2ItemStack) {
		playerInventory = par1InventoryPlayer;
		theItemStack = par2ItemStack;
	}

	public String callItemDisplayName() {
		return theItemStack.getDisplayName();
	}

	@Override
	public Object call() {
		return callItemDisplayName();
	}
}
