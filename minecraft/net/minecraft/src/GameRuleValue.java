package net.minecraft.src;

class GameRuleValue {
	private String valueString;
	private boolean valueBoolean;
	private int valueInteger;
	private double valueDouble;

	public GameRuleValue(final String par1Str) {
		setValue(par1Str);
	}

	/**
	 * Set this game rule value.
	 */
	public void setValue(final String par1Str) {
		valueString = par1Str;
		valueBoolean = Boolean.parseBoolean(par1Str);

		try {
			valueInteger = Integer.parseInt(par1Str);
		} catch (final NumberFormatException var4) {
			;
		}

		try {
			valueDouble = Double.parseDouble(par1Str);
		} catch (final NumberFormatException var3) {
			;
		}
	}

	/**
	 * Gets the GameRule's value as String.
	 */
	public String getGameRuleStringValue() {
		return valueString;
	}

	/**
	 * Gets the GameRule's value as boolean.
	 */
	public boolean getGameRuleBooleanValue() {
		return valueBoolean;
	}
}
