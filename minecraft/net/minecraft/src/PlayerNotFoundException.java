package net.minecraft.src;

public class PlayerNotFoundException extends CommandException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8123138732952007697L;

	public PlayerNotFoundException() {
		this("commands.generic.player.notFound", new Object[0]);
	}

	public PlayerNotFoundException(final String par1Str,
			final Object... par2ArrayOfObj) {
		super(par1Str, par2ArrayOfObj);
	}
}
