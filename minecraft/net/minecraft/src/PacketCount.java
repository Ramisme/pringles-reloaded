package net.minecraft.src;

import java.util.HashMap;
import java.util.Map;

public class PacketCount {
	/** If false, countPacket does nothing */
	public static boolean allowCounting = true;

	/** A count of the total number of each packet sent grouped by IDs. */
	private static final Map packetCountForID = new HashMap();

	/** A count of the total size of each packet sent grouped by IDs. */
	private static final Map sizeCountForID = new HashMap();

	/** Used to make threads queue to add packets */
	private static final Object lock = new Object();

	public static void countPacket(final int par0, final long par1) {
		if (PacketCount.allowCounting) {
			synchronized (PacketCount.lock) {
				if (PacketCount.packetCountForID.containsKey(Integer
						.valueOf(par0))) {
					PacketCount.packetCountForID
							.put(Integer.valueOf(par0),
									Long.valueOf(((Long) PacketCount.packetCountForID
											.get(Integer.valueOf(par0)))
											.longValue() + 1L));
					PacketCount.sizeCountForID.put(Integer.valueOf(par0), Long
							.valueOf(((Long) PacketCount.sizeCountForID
									.get(Integer.valueOf(par0))).longValue()
									+ par1));
				} else {
					PacketCount.packetCountForID.put(Integer.valueOf(par0),
							Long.valueOf(1L));
					PacketCount.sizeCountForID.put(Integer.valueOf(par0),
							Long.valueOf(par1));
				}
			}
		}
	}
}
