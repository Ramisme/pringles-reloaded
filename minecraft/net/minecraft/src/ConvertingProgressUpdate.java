package net.minecraft.src;

import net.minecraft.server.MinecraftServer;

public class ConvertingProgressUpdate implements IProgressUpdate {
	private long field_96245_b;

	/** Reference to the MinecraftServer object. */
	final MinecraftServer mcServer;

	public ConvertingProgressUpdate(final MinecraftServer par1) {
		mcServer = par1;
		field_96245_b = System.currentTimeMillis();
	}

	/**
	 * "Saving level", or the loading,or downloading equivelent
	 */
	@Override
	public void displayProgressMessage(final String par1Str) {
	}

	/**
	 * this string, followed by "working..." and then the "% complete" are the 3
	 * lines shown. This resets progress to 0, and the WorkingString to
	 * "working...".
	 */
	@Override
	public void resetProgressAndMessage(final String par1Str) {
	}

	/**
	 * Updates the progress bar on the loading screen to the specified amount.
	 * Args: loadProgress
	 */
	@Override
	public void setLoadingProgress(final int par1) {
		if (System.currentTimeMillis() - field_96245_b >= 1000L) {
			field_96245_b = System.currentTimeMillis();
			mcServer.getLogAgent().logInfo("Converting... " + par1 + "%");
		}
	}

	/**
	 * called when there is no more progress to be had, both on completion and
	 * failure
	 */
	@Override
	public void onNoMoreProgress() {
	}

	/**
	 * This is called with "Working..." by resetProgressAndMessage
	 */
	@Override
	public void resetProgresAndWorkingMessage(final String par1Str) {
	}
}
