package net.minecraft.src;

import java.util.List;

public class EntityAIAvoidEntity extends EntityAIBase {
	public final IEntitySelector field_98218_a = new EntityAIAvoidEntitySelector(
			this);

	/** The entity we are attached to */
	private final EntityCreature theEntity;
	private final float farSpeed;
	private final float nearSpeed;
	private Entity closestLivingEntity;
	private final float distanceFromEntity;

	/** The PathEntity of our entity */
	private PathEntity entityPathEntity;

	/** The PathNavigate of our entity */
	private final PathNavigate entityPathNavigate;

	/** The class of the entity we should avoid */
	private final Class targetEntityClass;

	public EntityAIAvoidEntity(final EntityCreature par1EntityCreature,
			final Class par2Class, final float par3, final float par4,
			final float par5) {
		theEntity = par1EntityCreature;
		targetEntityClass = par2Class;
		distanceFromEntity = par3;
		farSpeed = par4;
		nearSpeed = par5;
		entityPathNavigate = par1EntityCreature.getNavigator();
		setMutexBits(1);
	}

	/**
	 * Returns whether the EntityAIBase should begin execution.
	 */
	@Override
	public boolean shouldExecute() {
		if (targetEntityClass == EntityPlayer.class) {
			if (theEntity instanceof EntityTameable
					&& ((EntityTameable) theEntity).isTamed()) {
				return false;
			}

			closestLivingEntity = theEntity.worldObj.getClosestPlayerToEntity(
					theEntity, distanceFromEntity);

			if (closestLivingEntity == null) {
				return false;
			}
		} else {
			final List var1 = theEntity.worldObj.selectEntitiesWithinAABB(
					targetEntityClass, theEntity.boundingBox.expand(
							distanceFromEntity, 3.0D, distanceFromEntity),
					field_98218_a);

			if (var1.isEmpty()) {
				return false;
			}

			closestLivingEntity = (Entity) var1.get(0);
		}

		final Vec3 var2 = RandomPositionGenerator
				.findRandomTargetBlockAwayFrom(
						theEntity,
						16,
						7,
						theEntity.worldObj.getWorldVec3Pool().getVecFromPool(
								closestLivingEntity.posX,
								closestLivingEntity.posY,
								closestLivingEntity.posZ));

		if (var2 == null) {
			return false;
		} else if (closestLivingEntity.getDistanceSq(var2.xCoord, var2.yCoord,
				var2.zCoord) < closestLivingEntity
				.getDistanceSqToEntity(theEntity)) {
			return false;
		} else {
			entityPathEntity = entityPathNavigate.getPathToXYZ(var2.xCoord,
					var2.yCoord, var2.zCoord);
			return entityPathEntity == null ? false : entityPathEntity
					.isDestinationSame(var2);
		}
	}

	/**
	 * Returns whether an in-progress EntityAIBase should continue executing
	 */
	@Override
	public boolean continueExecuting() {
		return !entityPathNavigate.noPath();
	}

	/**
	 * Execute a one shot task or start executing a continuous task
	 */
	@Override
	public void startExecuting() {
		entityPathNavigate.setPath(entityPathEntity, farSpeed);
	}

	/**
	 * Resets the task
	 */
	@Override
	public void resetTask() {
		closestLivingEntity = null;
	}

	/**
	 * Updates the task
	 */
	@Override
	public void updateTask() {
		if (theEntity.getDistanceSqToEntity(closestLivingEntity) < 49.0D) {
			theEntity.getNavigator().setSpeed(nearSpeed);
		} else {
			theEntity.getNavigator().setSpeed(farSpeed);
		}
	}

	static EntityCreature func_98217_a(
			final EntityAIAvoidEntity par0EntityAIAvoidEntity) {
		return par0EntityAIAvoidEntity.theEntity;
	}
}
