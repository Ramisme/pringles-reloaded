package net.minecraft.src;

public class EntityAIOwnerHurtByTarget extends EntityAITarget {
	EntityTameable theDefendingTameable;
	EntityLiving theOwnerAttacker;

	public EntityAIOwnerHurtByTarget(final EntityTameable par1EntityTameable) {
		super(par1EntityTameable, 32.0F, false);
		theDefendingTameable = par1EntityTameable;
		setMutexBits(1);
	}

	/**
	 * Returns whether the EntityAIBase should begin execution.
	 */
	@Override
	public boolean shouldExecute() {
		if (!theDefendingTameable.isTamed()) {
			return false;
		} else {
			final EntityLiving var1 = theDefendingTameable.getOwner();

			if (var1 == null) {
				return false;
			} else {
				theOwnerAttacker = var1.getAITarget();
				return isSuitableTarget(theOwnerAttacker, false);
			}
		}
	}

	/**
	 * Execute a one shot task or start executing a continuous task
	 */
	@Override
	public void startExecuting() {
		taskOwner.setAttackTarget(theOwnerAttacker);
		super.startExecuting();
	}
}
