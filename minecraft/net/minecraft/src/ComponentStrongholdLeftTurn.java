package net.minecraft.src;

import java.util.List;
import java.util.Random;

public class ComponentStrongholdLeftTurn extends ComponentStronghold {
	protected final EnumDoor doorType;

	public ComponentStrongholdLeftTurn(final int par1, final Random par2Random,
			final StructureBoundingBox par3StructureBoundingBox, final int par4) {
		super(par1);
		coordBaseMode = par4;
		doorType = getRandomDoor(par2Random);
		boundingBox = par3StructureBoundingBox;
	}

	/**
	 * Initiates construction of the Structure Component picked, at the current
	 * Location of StructGen
	 */
	@Override
	public void buildComponent(final StructureComponent par1StructureComponent,
			final List par2List, final Random par3Random) {
		if (coordBaseMode != 2 && coordBaseMode != 3) {
			getNextComponentZ(
					(ComponentStrongholdStairs2) par1StructureComponent,
					par2List, par3Random, 1, 1);
		} else {
			getNextComponentX(
					(ComponentStrongholdStairs2) par1StructureComponent,
					par2List, par3Random, 1, 1);
		}
	}

	public static ComponentStrongholdLeftTurn findValidPlacement(
			final List par0List, final Random par1Random, final int par2,
			final int par3, final int par4, final int par5, final int par6) {
		final StructureBoundingBox var7 = StructureBoundingBox
				.getComponentToAddBoundingBox(par2, par3, par4, -1, -1, 0, 5,
						5, 5, par5);
		return ComponentStronghold.canStrongholdGoDeeper(var7)
				&& StructureComponent.findIntersecting(par0List, var7) == null ? new ComponentStrongholdLeftTurn(
				par6, par1Random, var7, par5) : null;
	}

	/**
	 * second Part of Structure generating, this for example places Spiderwebs,
	 * Mob Spawners, it closes Mineshafts at the end, it adds Fences...
	 */
	@Override
	public boolean addComponentParts(final World par1World,
			final Random par2Random,
			final StructureBoundingBox par3StructureBoundingBox) {
		if (isLiquidInStructureBoundingBox(par1World, par3StructureBoundingBox)) {
			return false;
		} else {
			fillWithRandomizedBlocks(par1World, par3StructureBoundingBox, 0, 0,
					0, 4, 4, 4, true, par2Random,
					StructureStrongholdPieces.getStrongholdStones());
			placeDoor(par1World, par2Random, par3StructureBoundingBox,
					doorType, 1, 1, 0);

			if (coordBaseMode != 2 && coordBaseMode != 3) {
				fillWithBlocks(par1World, par3StructureBoundingBox, 4, 1, 1, 4,
						3, 3, 0, 0, false);
			} else {
				fillWithBlocks(par1World, par3StructureBoundingBox, 0, 1, 1, 0,
						3, 3, 0, 0, false);
			}

			return true;
		}
	}
}
