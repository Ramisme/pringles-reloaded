package net.minecraft.src;

import java.applet.Applet;
import java.applet.AppletStub;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

public class MinecraftFakeLauncher extends Applet implements AppletStub {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9209221260893174338L;
	/** Arguments that were passed to Minecraft.jar (username, sessionid etc) */
	final Map arguments;

	public MinecraftFakeLauncher(final Map par1Map) {
		arguments = par1Map;
	}

	@Override
	public void appletResize(final int par1, final int par2) {
	}

	@Override
	public boolean isActive() {
		return true;
	}

	@Override
	public URL getDocumentBase() {
		try {
			return new URL("http://www.minecraft.net/game/");
		} catch (final MalformedURLException var2) {
			var2.printStackTrace();
			return null;
		}
	}

	@Override
	public String getParameter(final String par1Str) {
		if (arguments.containsKey(par1Str)) {
			return (String) arguments.get(par1Str);
		} else {
			System.err.println("Client asked for parameter: " + par1Str);
			return null;
		}
	}
}
