package net.minecraft.src;

import java.util.concurrent.Callable;

class CallableLvl2 implements Callable {
	/** Reference to the World object. */
	final World theWorld;

	CallableLvl2(final World par1World) {
		theWorld = par1World;
	}

	/**
	 * Returns the size and contents of the player entity list.
	 */
	public String getPlayerEntities() {
		return theWorld.playerEntities.size() + " total; "
				+ theWorld.playerEntities.toString();
	}

	@Override
	public Object call() {
		return getPlayerEntities();
	}
}
