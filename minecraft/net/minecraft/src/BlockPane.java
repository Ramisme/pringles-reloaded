package net.minecraft.src;

import java.util.List;
import java.util.Random;

public class BlockPane extends Block {
	/**
	 * Holds the texture index of the side of the pane (the thin lateral side)
	 */
	private final String sideTextureIndex;

	/**
	 * If this field is true, the pane block drops itself when destroyed (like
	 * the iron fences), otherwise, it's just destroyed (like glass panes)
	 */
	private final boolean canDropItself;
	private final String field_94402_c;
	private Icon theIcon;

	protected BlockPane(final int par1, final String par2Str,
			final String par3Str, final Material par4Material,
			final boolean par5) {
		super(par1, par4Material);
		sideTextureIndex = par3Str;
		canDropItself = par5;
		field_94402_c = par2Str;
		setCreativeTab(CreativeTabs.tabDecorations);
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return !canDropItself ? 0 : super.idDropped(par1, par2Random, par3);
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return 18;
	}

	/**
	 * Returns true if the given side of this block type should be rendered, if
	 * the adjacent block is at the given coordinates. Args: blockAccess, x, y,
	 * z, side
	 */
	@Override
	public boolean shouldSideBeRendered(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		final int var6 = par1IBlockAccess.getBlockId(par2, par3, par4);
		return var6 == blockID ? false : super.shouldSideBeRendered(
				par1IBlockAccess, par2, par3, par4, par5);
	}

	/**
	 * Adds all intersecting collision boxes to a list. (Be sure to only add
	 * boxes to the list if they intersect the mask.) Parameters: World, X, Y,
	 * Z, mask, list, colliding entity
	 */
	@Override
	public void addCollisionBoxesToList(final World par1World, final int par2,
			final int par3, final int par4,
			final AxisAlignedBB par5AxisAlignedBB, final List par6List,
			final Entity par7Entity) {
		final boolean var8 = canThisPaneConnectToThisBlockID(par1World
				.getBlockId(par2, par3, par4 - 1));
		final boolean var9 = canThisPaneConnectToThisBlockID(par1World
				.getBlockId(par2, par3, par4 + 1));
		final boolean var10 = canThisPaneConnectToThisBlockID(par1World
				.getBlockId(par2 - 1, par3, par4));
		final boolean var11 = canThisPaneConnectToThisBlockID(par1World
				.getBlockId(par2 + 1, par3, par4));

		if ((!var10 || !var11) && (var10 || var11 || var8 || var9)) {
			if (var10 && !var11) {
				setBlockBounds(0.0F, 0.0F, 0.4375F, 0.5F, 1.0F, 0.5625F);
				super.addCollisionBoxesToList(par1World, par2, par3, par4,
						par5AxisAlignedBB, par6List, par7Entity);
			} else if (!var10 && var11) {
				setBlockBounds(0.5F, 0.0F, 0.4375F, 1.0F, 1.0F, 0.5625F);
				super.addCollisionBoxesToList(par1World, par2, par3, par4,
						par5AxisAlignedBB, par6List, par7Entity);
			}
		} else {
			setBlockBounds(0.0F, 0.0F, 0.4375F, 1.0F, 1.0F, 0.5625F);
			super.addCollisionBoxesToList(par1World, par2, par3, par4,
					par5AxisAlignedBB, par6List, par7Entity);
		}

		if ((!var8 || !var9) && (var10 || var11 || var8 || var9)) {
			if (var8 && !var9) {
				setBlockBounds(0.4375F, 0.0F, 0.0F, 0.5625F, 1.0F, 0.5F);
				super.addCollisionBoxesToList(par1World, par2, par3, par4,
						par5AxisAlignedBB, par6List, par7Entity);
			} else if (!var8 && var9) {
				setBlockBounds(0.4375F, 0.0F, 0.5F, 0.5625F, 1.0F, 1.0F);
				super.addCollisionBoxesToList(par1World, par2, par3, par4,
						par5AxisAlignedBB, par6List, par7Entity);
			}
		} else {
			setBlockBounds(0.4375F, 0.0F, 0.0F, 0.5625F, 1.0F, 1.0F);
			super.addCollisionBoxesToList(par1World, par2, par3, par4,
					par5AxisAlignedBB, par6List, par7Entity);
		}
	}

	/**
	 * Sets the block's bounds for rendering it as an item
	 */
	@Override
	public void setBlockBoundsForItemRender() {
		setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
	}

	/**
	 * Updates the blocks bounds based on its current state. Args: world, x, y,
	 * z
	 */
	@Override
	public void setBlockBoundsBasedOnState(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		float var5 = 0.4375F;
		float var6 = 0.5625F;
		float var7 = 0.4375F;
		float var8 = 0.5625F;
		final boolean var9 = canThisPaneConnectToThisBlockID(par1IBlockAccess
				.getBlockId(par2, par3, par4 - 1));
		final boolean var10 = canThisPaneConnectToThisBlockID(par1IBlockAccess
				.getBlockId(par2, par3, par4 + 1));
		final boolean var11 = canThisPaneConnectToThisBlockID(par1IBlockAccess
				.getBlockId(par2 - 1, par3, par4));
		final boolean var12 = canThisPaneConnectToThisBlockID(par1IBlockAccess
				.getBlockId(par2 + 1, par3, par4));

		if ((!var11 || !var12) && (var11 || var12 || var9 || var10)) {
			if (var11 && !var12) {
				var5 = 0.0F;
			} else if (!var11 && var12) {
				var6 = 1.0F;
			}
		} else {
			var5 = 0.0F;
			var6 = 1.0F;
		}

		if ((!var9 || !var10) && (var11 || var12 || var9 || var10)) {
			if (var9 && !var10) {
				var7 = 0.0F;
			} else if (!var9 && var10) {
				var8 = 1.0F;
			}
		} else {
			var7 = 0.0F;
			var8 = 1.0F;
		}

		setBlockBounds(var5, 0.0F, var7, var6, 1.0F, var8);
	}

	/**
	 * Returns the texture index of the thin side of the pane.
	 */
	public Icon getSideTextureIndex() {
		return theIcon;
	}

	/**
	 * Gets passed in the blockID of the block adjacent and supposed to return
	 * true if its allowed to connect to the type of blockID passed in. Args:
	 * blockID
	 */
	public final boolean canThisPaneConnectToThisBlockID(final int par1) {
		return Block.opaqueCubeLookup[par1] || par1 == blockID
				|| par1 == Block.glass.blockID;
	}

	/**
	 * Return true if a player with Silk Touch can harvest this block directly,
	 * and not its normal drops.
	 */
	@Override
	protected boolean canSilkHarvest() {
		return true;
	}

	/**
	 * Returns an item stack containing a single instance of the current block
	 * type. 'i' is the block's subtype/damage and is ignored for blocks which
	 * do not support subtypes. Blocks which cannot be harvested should return
	 * null.
	 */
	@Override
	protected ItemStack createStackedBlock(final int par1) {
		return new ItemStack(blockID, 1, par1);
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		blockIcon = par1IconRegister.registerIcon(field_94402_c);
		theIcon = par1IconRegister.registerIcon(sideTextureIndex);
	}
}
