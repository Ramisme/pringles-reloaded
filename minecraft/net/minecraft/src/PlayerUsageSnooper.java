package net.minecraft.src;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

public class PlayerUsageSnooper {
	/** String map for report data */
	private final Map dataMap = new HashMap();
	private final String uniqueID = UUID.randomUUID().toString();

	/** URL of the server to send the report to */
	private final URL serverUrl;
	private final IPlayerUsage playerStatsCollector;

	/** set to fire the snooperThread every 15 mins */
	private final java.util.Timer threadTrigger = new java.util.Timer(
			"Snooper Timer", true);
	private final Object syncLock = new Object();
	private final long field_98224_g = System.currentTimeMillis();
	private boolean isRunning = false;

	/** incremented on every getSelfCounterFor */
	private int selfCounter = 0;

	public PlayerUsageSnooper(final String par1Str,
			final IPlayerUsage par2IPlayerUsage) {
		try {
			serverUrl = new URL("http://snoop.minecraft.net/" + par1Str
					+ "?version=" + 1);
		} catch (final MalformedURLException var4) {
			throw new IllegalArgumentException();
		}

		playerStatsCollector = par2IPlayerUsage;
	}

	/**
	 * Note issuing start multiple times is not an error.
	 */
	public void startSnooper() {
		if (!isRunning) {
			isRunning = true;
			addBaseDataToSnooper();
			threadTrigger.schedule(new PlayerUsageSnooperThread(this), 0L,
					900000L);
		}
	}

	private void addBaseDataToSnooper() {
		addJvmArgsToSnooper();
		addData("snooper_token", uniqueID);
		addData("os_name", System.getProperty("os.name"));
		addData("os_version", System.getProperty("os.version"));
		addData("os_architecture", System.getProperty("os.arch"));
		addData("java_version", System.getProperty("java.version"));
		addData("version", "1.5.2");
		playerStatsCollector.addServerTypeToSnooper(this);
	}

	private void addJvmArgsToSnooper() {
		final RuntimeMXBean var1 = ManagementFactory.getRuntimeMXBean();
		final List var2 = var1.getInputArguments();
		int var3 = 0;
		final Iterator var4 = var2.iterator();

		while (var4.hasNext()) {
			final String var5 = (String) var4.next();

			if (var5.startsWith("-X")) {
				addData("jvm_arg[" + var3++ + "]", var5);
			}
		}

		addData("jvm_args", Integer.valueOf(var3));
	}

	public void addMemoryStatsToSnooper() {
		addData("memory_total",
				Long.valueOf(Runtime.getRuntime().totalMemory()));
		addData("memory_max", Long.valueOf(Runtime.getRuntime().maxMemory()));
		addData("memory_free", Long.valueOf(Runtime.getRuntime().freeMemory()));
		addData("cpu_cores",
				Integer.valueOf(Runtime.getRuntime().availableProcessors()));
		addData("run_time",
				Long.valueOf((System.currentTimeMillis() - field_98224_g) / 60L * 1000L));
		playerStatsCollector.addServerStatsToSnooper(this);
	}

	/**
	 * Adds information to the report
	 */
	public void addData(final String par1Str, final Object par2Obj) {
		synchronized (syncLock) {
			dataMap.put(par1Str, par2Obj);
		}
	}

	public Map getCurrentStats() {
		final LinkedHashMap var1 = new LinkedHashMap();
		synchronized (syncLock) {
			addMemoryStatsToSnooper();
			final Iterator var3 = dataMap.entrySet().iterator();

			while (var3.hasNext()) {
				final Entry var4 = (Entry) var3.next();
				var1.put(var4.getKey(), var4.getValue().toString());
			}

			return var1;
		}
	}

	public boolean isSnooperRunning() {
		return isRunning;
	}

	public void stopSnooper() {
		threadTrigger.cancel();
	}

	public String getUniqueID() {
		return uniqueID;
	}

	static IPlayerUsage getStatsCollectorFor(
			final PlayerUsageSnooper par0PlayerUsageSnooper) {
		return par0PlayerUsageSnooper.playerStatsCollector;
	}

	static Object getSyncLockFor(final PlayerUsageSnooper par0PlayerUsageSnooper) {
		return par0PlayerUsageSnooper.syncLock;
	}

	static Map getDataMapFor(final PlayerUsageSnooper par0PlayerUsageSnooper) {
		return par0PlayerUsageSnooper.dataMap;
	}

	/**
	 * returns a value indicating how many times this function has been run on
	 * the snooper
	 */
	static int getSelfCounterFor(final PlayerUsageSnooper par0PlayerUsageSnooper) {
		return par0PlayerUsageSnooper.selfCounter++;
	}

	static URL getServerUrlFor(final PlayerUsageSnooper par0PlayerUsageSnooper) {
		return par0PlayerUsageSnooper.serverUrl;
	}
}
