package net.minecraft.src;

import java.util.concurrent.Callable;

class CallableLevelGeneratorOptions implements Callable {
	final WorldInfo worldInfoInstance;

	CallableLevelGeneratorOptions(final WorldInfo par1WorldInfo) {
		worldInfoInstance = par1WorldInfo;
	}

	public String callLevelGeneratorOptions() {
		return WorldInfo.getWorldGeneratorOptions(worldInfoInstance);
	}

	@Override
	public Object call() {
		return callLevelGeneratorOptions();
	}
}
