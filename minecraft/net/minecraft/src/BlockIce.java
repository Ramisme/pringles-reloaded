package net.minecraft.src;

import java.util.Random;

public class BlockIce extends BlockBreakable {
	public BlockIce(final int par1) {
		super(par1, "ice", Material.ice, false);
		slipperiness = 0.98F;
		setTickRandomly(true);
		setCreativeTab(CreativeTabs.tabBlock);
	}

	/**
	 * Returns which pass should this block be rendered on. 0 for solids and 1
	 * for alpha
	 */
	@Override
	public int getRenderBlockPass() {
		return 1;
	}

	/**
	 * Returns true if the given side of this block type should be rendered, if
	 * the adjacent block is at the given coordinates. Args: blockAccess, x, y,
	 * z, side
	 */
	@Override
	public boolean shouldSideBeRendered(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		return super.shouldSideBeRendered(par1IBlockAccess, par2, par3, par4,
				1 - par5);
	}

	/**
	 * Called when the player destroys a block with an item that can harvest it.
	 * (i, j, k) are the coordinates of the block and l is the block's
	 * subtype/damage.
	 */
	@Override
	public void harvestBlock(final World par1World,
			final EntityPlayer par2EntityPlayer, final int par3,
			final int par4, final int par5, final int par6) {
		par2EntityPlayer.addStat(StatList.mineBlockStatArray[blockID], 1);
		par2EntityPlayer.addExhaustion(0.025F);

		if (canSilkHarvest()
				&& EnchantmentHelper.getSilkTouchModifier(par2EntityPlayer)) {
			final ItemStack var9 = createStackedBlock(par6);

			if (var9 != null) {
				dropBlockAsItem_do(par1World, par3, par4, par5, var9);
			}
		} else {
			if (par1World.provider.isHellWorld) {
				par1World.setBlockToAir(par3, par4, par5);
				return;
			}

			final int var7 = EnchantmentHelper
					.getFortuneModifier(par2EntityPlayer);
			dropBlockAsItem(par1World, par3, par4, par5, par6, var7);
			final Material var8 = par1World.getBlockMaterial(par3, par4 - 1,
					par5);

			if (var8.blocksMovement() || var8.isLiquid()) {
				par1World.setBlock(par3, par4, par5, Block.waterMoving.blockID);
			}
		}
	}

	/**
	 * Returns the quantity of items to drop on block destruction.
	 */
	@Override
	public int quantityDropped(final Random par1Random) {
		return 0;
	}

	/**
	 * Ticks the block if it's been scheduled
	 */
	@Override
	public void updateTick(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		if (par1World.getSavedLightValue(EnumSkyBlock.Block, par2, par3, par4) > 11 - Block.lightOpacity[blockID]) {
			if (par1World.provider.isHellWorld) {
				par1World.setBlockToAir(par2, par3, par4);
				return;
			}

			dropBlockAsItem(par1World, par2, par3, par4,
					par1World.getBlockMetadata(par2, par3, par4), 0);
			par1World.setBlock(par2, par3, par4, Block.waterStill.blockID);
		}
	}

	/**
	 * Returns the mobility information of the block, 0 = free, 1 = can't push
	 * but can move over, 2 = total immobility and stop pistons
	 */
	@Override
	public int getMobilityFlag() {
		return 0;
	}
}
