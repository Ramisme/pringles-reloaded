package net.minecraft.src;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class EntityTracker {
	private final WorldServer theWorld;

	/**
	 * List of tracked entities, used for iteration operations on tracked
	 * entities.
	 */
	private final Set trackedEntities = new HashSet();
	private final IntHashMap trackedEntityIDs = new IntHashMap();
	private final int entityViewDistance;

	public EntityTracker(final WorldServer par1WorldServer) {
		theWorld = par1WorldServer;
		entityViewDistance = par1WorldServer.getMinecraftServer()
				.getConfigurationManager().getEntityViewDistance();
	}

	/**
	 * if entity is a player sends all tracked events to the player, otherwise,
	 * adds with a visibility and update arate based on the class type
	 */
	public void addEntityToTracker(final Entity par1Entity) {
		if (par1Entity instanceof EntityPlayerMP) {
			this.addEntityToTracker(par1Entity, 512, 2);
			final EntityPlayerMP var2 = (EntityPlayerMP) par1Entity;
			final Iterator var3 = trackedEntities.iterator();

			while (var3.hasNext()) {
				final EntityTrackerEntry var4 = (EntityTrackerEntry) var3
						.next();

				if (var4.myEntity != var2) {
					var4.tryStartWachingThis(var2);
				}
			}
		} else if (par1Entity instanceof EntityFishHook) {
			this.addEntityToTracker(par1Entity, 64, 5, true);
		} else if (par1Entity instanceof EntityArrow) {
			this.addEntityToTracker(par1Entity, 64, 20, false);
		} else if (par1Entity instanceof EntitySmallFireball) {
			this.addEntityToTracker(par1Entity, 64, 10, false);
		} else if (par1Entity instanceof EntityFireball) {
			this.addEntityToTracker(par1Entity, 64, 10, false);
		} else if (par1Entity instanceof EntitySnowball) {
			this.addEntityToTracker(par1Entity, 64, 10, true);
		} else if (par1Entity instanceof EntityEnderPearl) {
			this.addEntityToTracker(par1Entity, 64, 10, true);
		} else if (par1Entity instanceof EntityEnderEye) {
			this.addEntityToTracker(par1Entity, 64, 4, true);
		} else if (par1Entity instanceof EntityEgg) {
			this.addEntityToTracker(par1Entity, 64, 10, true);
		} else if (par1Entity instanceof EntityPotion) {
			this.addEntityToTracker(par1Entity, 64, 10, true);
		} else if (par1Entity instanceof EntityExpBottle) {
			this.addEntityToTracker(par1Entity, 64, 10, true);
		} else if (par1Entity instanceof EntityFireworkRocket) {
			this.addEntityToTracker(par1Entity, 64, 10, true);
		} else if (par1Entity instanceof EntityItem) {
			this.addEntityToTracker(par1Entity, 64, 20, true);
		} else if (par1Entity instanceof EntityMinecart) {
			this.addEntityToTracker(par1Entity, 80, 3, true);
		} else if (par1Entity instanceof EntityBoat) {
			this.addEntityToTracker(par1Entity, 80, 3, true);
		} else if (par1Entity instanceof EntitySquid) {
			this.addEntityToTracker(par1Entity, 64, 3, true);
		} else if (par1Entity instanceof EntityWither) {
			this.addEntityToTracker(par1Entity, 80, 3, false);
		} else if (par1Entity instanceof EntityBat) {
			this.addEntityToTracker(par1Entity, 80, 3, false);
		} else if (par1Entity instanceof IAnimals) {
			this.addEntityToTracker(par1Entity, 80, 3, true);
		} else if (par1Entity instanceof EntityDragon) {
			this.addEntityToTracker(par1Entity, 160, 3, true);
		} else if (par1Entity instanceof EntityTNTPrimed) {
			this.addEntityToTracker(par1Entity, 160, 10, true);
		} else if (par1Entity instanceof EntityFallingSand) {
			this.addEntityToTracker(par1Entity, 160, 20, true);
		} else if (par1Entity instanceof EntityPainting) {
			this.addEntityToTracker(par1Entity, 160, Integer.MAX_VALUE, false);
		} else if (par1Entity instanceof EntityXPOrb) {
			this.addEntityToTracker(par1Entity, 160, 20, true);
		} else if (par1Entity instanceof EntityEnderCrystal) {
			this.addEntityToTracker(par1Entity, 256, Integer.MAX_VALUE, false);
		} else if (par1Entity instanceof EntityItemFrame) {
			this.addEntityToTracker(par1Entity, 160, Integer.MAX_VALUE, false);
		}
	}

	public void addEntityToTracker(final Entity par1Entity, final int par2,
			final int par3) {
		this.addEntityToTracker(par1Entity, par2, par3, false);
	}

	public void addEntityToTracker(final Entity par1Entity, int par2,
			final int par3, final boolean par4) {
		if (par2 > entityViewDistance) {
			par2 = entityViewDistance;
		}

		try {
			if (trackedEntityIDs.containsItem(par1Entity.entityId)) {
				throw new IllegalStateException("Entity is already tracked!");
			}

			final EntityTrackerEntry var5 = new EntityTrackerEntry(par1Entity,
					par2, par3, par4);
			trackedEntities.add(var5);
			trackedEntityIDs.addKey(par1Entity.entityId, var5);
			var5.sendEventsToPlayers(theWorld.playerEntities);
		} catch (final Throwable var11) {
			final CrashReport var6 = CrashReport.makeCrashReport(var11,
					"Adding entity to track");
			final CrashReportCategory var7 = var6
					.makeCategory("Entity To Track");
			var7.addCrashSection("Tracking range", par2 + " blocks");
			var7.addCrashSectionCallable("Update interval",
					new CallableEntityTracker(this, par3));
			par1Entity.func_85029_a(var7);
			final CrashReportCategory var8 = var6
					.makeCategory("Entity That Is Already Tracked");
			((EntityTrackerEntry) trackedEntityIDs.lookup(par1Entity.entityId)).myEntity
					.func_85029_a(var8);

			try {
				throw new ReportedException(var6);
			} catch (final ReportedException var10) {
				System.err
						.println("\"Silently\" catching entity tracking error.");
				var10.printStackTrace();
			}
		}
	}

	public void removeEntityFromAllTrackingPlayers(final Entity par1Entity) {
		if (par1Entity instanceof EntityPlayerMP) {
			final EntityPlayerMP var2 = (EntityPlayerMP) par1Entity;
			final Iterator var3 = trackedEntities.iterator();

			while (var3.hasNext()) {
				final EntityTrackerEntry var4 = (EntityTrackerEntry) var3
						.next();
				var4.removeFromWatchingList(var2);
			}
		}

		final EntityTrackerEntry var5 = (EntityTrackerEntry) trackedEntityIDs
				.removeObject(par1Entity.entityId);

		if (var5 != null) {
			trackedEntities.remove(var5);
			var5.informAllAssociatedPlayersOfItemDestruction();
		}
	}

	public void updateTrackedEntities() {
		final ArrayList var1 = new ArrayList();
		final Iterator var2 = trackedEntities.iterator();

		while (var2.hasNext()) {
			final EntityTrackerEntry var3 = (EntityTrackerEntry) var2.next();
			var3.sendLocationToAllClients(theWorld.playerEntities);

			if (var3.playerEntitiesUpdated
					&& var3.myEntity instanceof EntityPlayerMP) {
				var1.add(var3.myEntity);
			}
		}

		for (int var6 = 0; var6 < var1.size(); ++var6) {
			final EntityPlayerMP var7 = (EntityPlayerMP) var1.get(var6);
			final Iterator var4 = trackedEntities.iterator();

			while (var4.hasNext()) {
				final EntityTrackerEntry var5 = (EntityTrackerEntry) var4
						.next();

				if (var5.myEntity != var7) {
					var5.tryStartWachingThis(var7);
				}
			}
		}
	}

	/**
	 * does not send the packet to the entity if the entity is a player
	 */
	public void sendPacketToAllPlayersTrackingEntity(final Entity par1Entity,
			final Packet par2Packet) {
		final EntityTrackerEntry var3 = (EntityTrackerEntry) trackedEntityIDs
				.lookup(par1Entity.entityId);

		if (var3 != null) {
			var3.sendPacketToAllTrackingPlayers(par2Packet);
		}
	}

	/**
	 * sends to the entity if the entity is a player
	 */
	public void sendPacketToAllAssociatedPlayers(final Entity par1Entity,
			final Packet par2Packet) {
		final EntityTrackerEntry var3 = (EntityTrackerEntry) trackedEntityIDs
				.lookup(par1Entity.entityId);

		if (var3 != null) {
			var3.sendPacketToAllAssociatedPlayers(par2Packet);
		}
	}

	public void removePlayerFromTrackers(final EntityPlayerMP par1EntityPlayerMP) {
		final Iterator var2 = trackedEntities.iterator();

		while (var2.hasNext()) {
			final EntityTrackerEntry var3 = (EntityTrackerEntry) var2.next();
			var3.removePlayerFromTracker(par1EntityPlayerMP);
		}
	}

	public void func_85172_a(final EntityPlayerMP par1EntityPlayerMP,
			final Chunk par2Chunk) {
		final Iterator var3 = trackedEntities.iterator();

		while (var3.hasNext()) {
			final EntityTrackerEntry var4 = (EntityTrackerEntry) var3.next();

			if (var4.myEntity != par1EntityPlayerMP
					&& var4.myEntity.chunkCoordX == par2Chunk.xPosition
					&& var4.myEntity.chunkCoordZ == par2Chunk.zPosition) {
				var4.tryStartWachingThis(par1EntityPlayerMP);
			}
		}
	}
}
