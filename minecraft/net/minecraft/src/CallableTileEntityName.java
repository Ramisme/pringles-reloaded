package net.minecraft.src;

import java.util.concurrent.Callable;

class CallableTileEntityName implements Callable {
	final TileEntity theTileEntity;

	CallableTileEntityName(final TileEntity par1TileEntity) {
		theTileEntity = par1TileEntity;
	}

	public String callTileEntityName() {
		return (String) TileEntity.getClassToNameMap().get(
				theTileEntity.getClass())
				+ " // " + theTileEntity.getClass().getCanonicalName();
	}

	@Override
	public Object call() {
		return callTileEntityName();
	}
}
