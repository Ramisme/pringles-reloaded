package net.minecraft.src;

public class EntityIronGolem extends EntityGolem {
	/** deincrements, and a distance-to-home check is done at 0 */
	private int homeCheckTimer = 0;
	Village villageObj = null;
	private int attackTimer;
	private int holdRoseTick;

	public EntityIronGolem(final World par1World) {
		super(par1World);
		texture = "/mob/villager_golem.png";
		setSize(1.4F, 2.9F);
		getNavigator().setAvoidsWater(true);
		tasks.addTask(1, new EntityAIAttackOnCollide(this, 0.25F, true));
		tasks.addTask(2, new EntityAIMoveTowardsTarget(this, 0.22F, 32.0F));
		tasks.addTask(3, new EntityAIMoveThroughVillage(this, 0.16F, true));
		tasks.addTask(4, new EntityAIMoveTwardsRestriction(this, 0.16F));
		tasks.addTask(5, new EntityAILookAtVillager(this));
		tasks.addTask(6, new EntityAIWander(this, 0.16F));
		tasks.addTask(7, new EntityAIWatchClosest(this, EntityPlayer.class,
				6.0F));
		tasks.addTask(8, new EntityAILookIdle(this));
		targetTasks.addTask(1, new EntityAIDefendVillage(this));
		targetTasks.addTask(2, new EntityAIHurtByTarget(this, false));
		targetTasks.addTask(3, new EntityAINearestAttackableTarget(this,
				EntityLiving.class, 16.0F, 0, false, true, IMob.mobSelector));
	}

	@Override
	protected void entityInit() {
		super.entityInit();
		dataWatcher.addObject(16, Byte.valueOf((byte) 0));
	}

	/**
	 * Returns true if the newer Entity AI code should be run
	 */
	@Override
	public boolean isAIEnabled() {
		return true;
	}

	/**
	 * main AI tick function, replaces updateEntityActionState
	 */
	@Override
	protected void updateAITick() {
		if (--homeCheckTimer <= 0) {
			homeCheckTimer = 70 + rand.nextInt(50);
			villageObj = worldObj.villageCollectionObj.findNearestVillage(
					MathHelper.floor_double(posX),
					MathHelper.floor_double(posY),
					MathHelper.floor_double(posZ), 32);

			if (villageObj == null) {
				detachHome();
			} else {
				final ChunkCoordinates var1 = villageObj.getCenter();
				setHomeArea(var1.posX, var1.posY, var1.posZ,
						(int) (villageObj.getVillageRadius() * 0.6F));
			}
		}

		super.updateAITick();
	}

	@Override
	public int getMaxHealth() {
		return 100;
	}

	/**
	 * Decrements the entity's air supply when underwater
	 */
	@Override
	protected int decreaseAirSupply(final int par1) {
		return par1;
	}

	@Override
	protected void collideWithEntity(final Entity par1Entity) {
		if (par1Entity instanceof IMob && getRNG().nextInt(20) == 0) {
			setAttackTarget((EntityLiving) par1Entity);
		}

		super.collideWithEntity(par1Entity);
	}

	/**
	 * Called frequently so the entity can update its state every tick as
	 * required. For example, zombies and skeletons use this to react to
	 * sunlight and start to burn.
	 */
	@Override
	public void onLivingUpdate() {
		super.onLivingUpdate();

		if (attackTimer > 0) {
			--attackTimer;
		}

		if (holdRoseTick > 0) {
			--holdRoseTick;
		}

		if (motionX * motionX + motionZ * motionZ > 2.500000277905201E-7D
				&& rand.nextInt(5) == 0) {
			final int var1 = MathHelper.floor_double(posX);
			final int var2 = MathHelper.floor_double(posY
					- 0.20000000298023224D - yOffset);
			final int var3 = MathHelper.floor_double(posZ);
			final int var4 = worldObj.getBlockId(var1, var2, var3);

			if (var4 > 0) {
				worldObj.spawnParticle(
						"tilecrack_" + var4 + "_"
								+ worldObj.getBlockMetadata(var1, var2, var3),
						posX + (rand.nextFloat() - 0.5D) * width,
						boundingBox.minY + 0.1D, posZ
								+ (rand.nextFloat() - 0.5D) * width,
						4.0D * (rand.nextFloat() - 0.5D), 0.5D,
						(rand.nextFloat() - 0.5D) * 4.0D);
			}
		}
	}

	/**
	 * Returns true if this entity can attack entities of the specified class.
	 */
	@Override
	public boolean canAttackClass(final Class par1Class) {
		return isPlayerCreated()
				&& EntityPlayer.class.isAssignableFrom(par1Class) ? false
				: super.canAttackClass(par1Class);
	}

	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	@Override
	public void writeEntityToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeEntityToNBT(par1NBTTagCompound);
		par1NBTTagCompound.setBoolean("PlayerCreated", isPlayerCreated());
	}

	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	@Override
	public void readEntityFromNBT(final NBTTagCompound par1NBTTagCompound) {
		super.readEntityFromNBT(par1NBTTagCompound);
		setPlayerCreated(par1NBTTagCompound.getBoolean("PlayerCreated"));
	}

	@Override
	public boolean attackEntityAsMob(final Entity par1Entity) {
		attackTimer = 10;
		worldObj.setEntityState(this, (byte) 4);
		final boolean var2 = par1Entity.attackEntityFrom(
				DamageSource.causeMobDamage(this), 7 + rand.nextInt(15));

		if (var2) {
			par1Entity.motionY += 0.4000000059604645D;
		}

		playSound("mob.irongolem.throw", 1.0F, 1.0F);
		return var2;
	}

	@Override
	public void handleHealthUpdate(final byte par1) {
		if (par1 == 4) {
			attackTimer = 10;
			playSound("mob.irongolem.throw", 1.0F, 1.0F);
		} else if (par1 == 11) {
			holdRoseTick = 400;
		} else {
			super.handleHealthUpdate(par1);
		}
	}

	public Village getVillage() {
		return villageObj;
	}

	public int getAttackTimer() {
		return attackTimer;
	}

	public void setHoldingRose(final boolean par1) {
		holdRoseTick = par1 ? 400 : 0;
		worldObj.setEntityState(this, (byte) 11);
	}

	/**
	 * Returns the sound this mob makes while it's alive.
	 */
	@Override
	protected String getLivingSound() {
		return "none";
	}

	/**
	 * Returns the sound this mob makes when it is hurt.
	 */
	@Override
	protected String getHurtSound() {
		return "mob.irongolem.hit";
	}

	/**
	 * Returns the sound this mob makes on death.
	 */
	@Override
	protected String getDeathSound() {
		return "mob.irongolem.death";
	}

	/**
	 * Plays step sound at given x, y, z for the entity
	 */
	@Override
	protected void playStepSound(final int par1, final int par2,
			final int par3, final int par4) {
		playSound("mob.irongolem.walk", 1.0F, 1.0F);
	}

	/**
	 * Drop 0-2 items of this living's type. @param par1 - Whether this entity
	 * has recently been hit by a player. @param par2 - Level of Looting used to
	 * kill this mob.
	 */
	@Override
	protected void dropFewItems(final boolean par1, final int par2) {
		final int var3 = rand.nextInt(3);
		int var4;

		for (var4 = 0; var4 < var3; ++var4) {
			dropItem(Block.plantRed.blockID, 1);
		}

		var4 = 3 + rand.nextInt(3);

		for (int var5 = 0; var5 < var4; ++var5) {
			dropItem(Item.ingotIron.itemID, 1);
		}
	}

	public int getHoldRoseTick() {
		return holdRoseTick;
	}

	public boolean isPlayerCreated() {
		return (dataWatcher.getWatchableObjectByte(16) & 1) != 0;
	}

	public void setPlayerCreated(final boolean par1) {
		final byte var2 = dataWatcher.getWatchableObjectByte(16);

		if (par1) {
			dataWatcher.updateObject(16, Byte.valueOf((byte) (var2 | 1)));
		} else {
			dataWatcher.updateObject(16, Byte.valueOf((byte) (var2 & -2)));
		}
	}

	/**
	 * Called when the mob's health reaches 0.
	 */
	@Override
	public void onDeath(final DamageSource par1DamageSource) {
		if (!isPlayerCreated() && attackingPlayer != null && villageObj != null) {
			villageObj.setReputationForPlayer(
					attackingPlayer.getCommandSenderName(), -5);
		}

		super.onDeath(par1DamageSource);
	}
}
