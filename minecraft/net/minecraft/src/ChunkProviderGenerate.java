package net.minecraft.src;

import java.util.List;
import java.util.Random;

public class ChunkProviderGenerate implements IChunkProvider {
	/** RNG. */
	private final Random rand;

	/** A NoiseGeneratorOctaves used in generating terrain */
	private final NoiseGeneratorOctaves noiseGen1;

	/** A NoiseGeneratorOctaves used in generating terrain */
	private final NoiseGeneratorOctaves noiseGen2;

	/** A NoiseGeneratorOctaves used in generating terrain */
	private final NoiseGeneratorOctaves noiseGen3;

	/** A NoiseGeneratorOctaves used in generating terrain */
	private final NoiseGeneratorOctaves noiseGen4;

	/** A NoiseGeneratorOctaves used in generating terrain */
	public NoiseGeneratorOctaves noiseGen5;

	/** A NoiseGeneratorOctaves used in generating terrain */
	public NoiseGeneratorOctaves noiseGen6;
	public NoiseGeneratorOctaves mobSpawnerNoise;

	/** Reference to the World object. */
	private final World worldObj;

	/** are map structures going to be generated (e.g. strongholds) */
	private final boolean mapFeaturesEnabled;

	/** Holds the overall noise array used in chunk generation */
	private double[] noiseArray;
	private double[] stoneNoise = new double[256];
	private final MapGenBase caveGenerator = new MapGenCaves();

	/** Holds Stronghold Generator */
	private final MapGenStronghold strongholdGenerator = new MapGenStronghold();

	/** Holds Village Generator */
	private final MapGenVillage villageGenerator = new MapGenVillage();

	/** Holds Mineshaft Generator */
	private final MapGenMineshaft mineshaftGenerator = new MapGenMineshaft();
	private final MapGenScatteredFeature scatteredFeatureGenerator = new MapGenScatteredFeature();

	/** Holds ravine generator */
	private final MapGenBase ravineGenerator = new MapGenRavine();

	/** The biomes that are used to generate the chunk */
	private BiomeGenBase[] biomesForGeneration;

	/** A double array that hold terrain noise from noiseGen3 */
	double[] noise3;

	/** A double array that hold terrain noise */
	double[] noise1;

	/** A double array that hold terrain noise from noiseGen2 */
	double[] noise2;

	/** A double array that hold terrain noise from noiseGen5 */
	double[] noise5;

	/** A double array that holds terrain noise from noiseGen6 */
	double[] noise6;

	/**
	 * Used to store the 5x5 parabolic field that is used during terrain
	 * generation.
	 */
	float[] parabolicField;
	int[][] field_73219_j = new int[32][32];

	public ChunkProviderGenerate(final World par1World, final long par2,
			final boolean par4) {
		worldObj = par1World;
		mapFeaturesEnabled = par4;
		rand = new Random(par2);
		noiseGen1 = new NoiseGeneratorOctaves(rand, 16);
		noiseGen2 = new NoiseGeneratorOctaves(rand, 16);
		noiseGen3 = new NoiseGeneratorOctaves(rand, 8);
		noiseGen4 = new NoiseGeneratorOctaves(rand, 4);
		noiseGen5 = new NoiseGeneratorOctaves(rand, 10);
		noiseGen6 = new NoiseGeneratorOctaves(rand, 16);
		mobSpawnerNoise = new NoiseGeneratorOctaves(rand, 8);
	}

	/**
	 * Generates the shape of the terrain for the chunk though its all stone
	 * though the water is frozen if the temperature is low enough
	 */
	public void generateTerrain(final int par1, final int par2,
			final byte[] par3ArrayOfByte) {
		final byte var4 = 4;
		final byte var5 = 16;
		final byte var6 = 63;
		final int var7 = var4 + 1;
		final byte var8 = 17;
		final int var9 = var4 + 1;
		biomesForGeneration = worldObj.getWorldChunkManager()
				.getBiomesForGeneration(biomesForGeneration, par1 * 4 - 2,
						par2 * 4 - 2, var7 + 5, var9 + 5);
		noiseArray = initializeNoiseField(noiseArray, par1 * var4, 0, par2
				* var4, var7, var8, var9);

		for (int var10 = 0; var10 < var4; ++var10) {
			for (int var11 = 0; var11 < var4; ++var11) {
				for (int var12 = 0; var12 < var5; ++var12) {
					final double var13 = 0.125D;
					double var15 = noiseArray[((var10 + 0) * var9 + var11 + 0)
							* var8 + var12 + 0];
					double var17 = noiseArray[((var10 + 0) * var9 + var11 + 1)
							* var8 + var12 + 0];
					double var19 = noiseArray[((var10 + 1) * var9 + var11 + 0)
							* var8 + var12 + 0];
					double var21 = noiseArray[((var10 + 1) * var9 + var11 + 1)
							* var8 + var12 + 0];
					final double var23 = (noiseArray[((var10 + 0) * var9
							+ var11 + 0)
							* var8 + var12 + 1] - var15)
							* var13;
					final double var25 = (noiseArray[((var10 + 0) * var9
							+ var11 + 1)
							* var8 + var12 + 1] - var17)
							* var13;
					final double var27 = (noiseArray[((var10 + 1) * var9
							+ var11 + 0)
							* var8 + var12 + 1] - var19)
							* var13;
					final double var29 = (noiseArray[((var10 + 1) * var9
							+ var11 + 1)
							* var8 + var12 + 1] - var21)
							* var13;

					for (int var31 = 0; var31 < 8; ++var31) {
						final double var32 = 0.25D;
						double var34 = var15;
						double var36 = var17;
						final double var38 = (var19 - var15) * var32;
						final double var40 = (var21 - var17) * var32;

						for (int var42 = 0; var42 < 4; ++var42) {
							int var43 = var42 + var10 * 4 << 11
									| 0 + var11 * 4 << 7 | var12 * 8 + var31;
							final short var44 = 128;
							var43 -= var44;
							final double var45 = 0.25D;
							final double var49 = (var36 - var34) * var45;
							double var47 = var34 - var49;

							for (int var51 = 0; var51 < 4; ++var51) {
								if ((var47 += var49) > 0.0D) {
									par3ArrayOfByte[var43 += var44] = (byte) Block.stone.blockID;
								} else if (var12 * 8 + var31 < var6) {
									par3ArrayOfByte[var43 += var44] = (byte) Block.waterStill.blockID;
								} else {
									par3ArrayOfByte[var43 += var44] = 0;
								}
							}

							var34 += var38;
							var36 += var40;
						}

						var15 += var23;
						var17 += var25;
						var19 += var27;
						var21 += var29;
					}
				}
			}
		}
	}

	/**
	 * Replaces the stone that was placed in with blocks that match the biome
	 */
	public void replaceBlocksForBiome(final int par1, final int par2,
			final byte[] par3ArrayOfByte,
			final BiomeGenBase[] par4ArrayOfBiomeGenBase) {
		final byte var5 = 63;
		final double var6 = 0.03125D;
		stoneNoise = noiseGen4.generateNoiseOctaves(stoneNoise, par1 * 16,
				par2 * 16, 0, 16, 16, 1, var6 * 2.0D, var6 * 2.0D, var6 * 2.0D);

		for (int var8 = 0; var8 < 16; ++var8) {
			for (int var9 = 0; var9 < 16; ++var9) {
				final BiomeGenBase var10 = par4ArrayOfBiomeGenBase[var9 + var8
						* 16];
				final float var11 = var10.getFloatTemperature();
				final int var12 = (int) (stoneNoise[var8 + var9 * 16] / 3.0D + 3.0D + rand
						.nextDouble() * 0.25D);
				int var13 = -1;
				byte var14 = var10.topBlock;
				byte var15 = var10.fillerBlock;

				for (int var16 = 127; var16 >= 0; --var16) {
					final int var17 = (var9 * 16 + var8) * 128 + var16;

					if (var16 <= 0 + rand.nextInt(5)) {
						par3ArrayOfByte[var17] = (byte) Block.bedrock.blockID;
					} else {
						final byte var18 = par3ArrayOfByte[var17];

						if (var18 == 0) {
							var13 = -1;
						} else if (var18 == Block.stone.blockID) {
							if (var13 == -1) {
								if (var12 <= 0) {
									var14 = 0;
									var15 = (byte) Block.stone.blockID;
								} else if (var16 >= var5 - 4
										&& var16 <= var5 + 1) {
									var14 = var10.topBlock;
									var15 = var10.fillerBlock;
								}

								if (var16 < var5 && var14 == 0) {
									if (var11 < 0.15F) {
										var14 = (byte) Block.ice.blockID;
									} else {
										var14 = (byte) Block.waterStill.blockID;
									}
								}

								var13 = var12;

								if (var16 >= var5 - 1) {
									par3ArrayOfByte[var17] = var14;
								} else {
									par3ArrayOfByte[var17] = var15;
								}
							} else if (var13 > 0) {
								--var13;
								par3ArrayOfByte[var17] = var15;

								if (var13 == 0 && var15 == Block.sand.blockID) {
									var13 = rand.nextInt(4);
									var15 = (byte) Block.sandStone.blockID;
								}
							}
						}
					}
				}
			}
		}
	}

	/**
	 * loads or generates the chunk at the chunk location specified
	 */
	@Override
	public Chunk loadChunk(final int par1, final int par2) {
		return provideChunk(par1, par2);
	}

	/**
	 * Will return back a chunk, if it doesn't exist and its not a MP client it
	 * will generates all the blocks for the specified chunk from the map seed
	 * and chunk seed
	 */
	@Override
	public Chunk provideChunk(final int par1, final int par2) {
		rand.setSeed(par1 * 341873128712L + par2 * 132897987541L);
		final byte[] var3 = new byte[32768];
		generateTerrain(par1, par2, var3);
		biomesForGeneration = worldObj.getWorldChunkManager()
				.loadBlockGeneratorData(biomesForGeneration, par1 * 16,
						par2 * 16, 16, 16);
		replaceBlocksForBiome(par1, par2, var3, biomesForGeneration);
		caveGenerator.generate(this, worldObj, par1, par2, var3);
		ravineGenerator.generate(this, worldObj, par1, par2, var3);

		if (mapFeaturesEnabled) {
			mineshaftGenerator.generate(this, worldObj, par1, par2, var3);
			villageGenerator.generate(this, worldObj, par1, par2, var3);
			strongholdGenerator.generate(this, worldObj, par1, par2, var3);
			scatteredFeatureGenerator
					.generate(this, worldObj, par1, par2, var3);
		}

		final Chunk var4 = new Chunk(worldObj, var3, par1, par2);
		final byte[] var5 = var4.getBiomeArray();

		for (int var6 = 0; var6 < var5.length; ++var6) {
			var5[var6] = (byte) biomesForGeneration[var6].biomeID;
		}

		var4.generateSkylightMap();
		return var4;
	}

	/**
	 * generates a subset of the level's terrain data. Takes 7 arguments: the
	 * [empty] noise array, the position, and the size.
	 */
	private double[] initializeNoiseField(double[] par1ArrayOfDouble,
			final int par2, final int par3, final int par4, final int par5,
			final int par6, final int par7) {
		if (par1ArrayOfDouble == null) {
			par1ArrayOfDouble = new double[par5 * par6 * par7];
		}

		if (parabolicField == null) {
			parabolicField = new float[25];

			for (int var8 = -2; var8 <= 2; ++var8) {
				for (int var9 = -2; var9 <= 2; ++var9) {
					final float var10 = 10.0F / MathHelper.sqrt_float(var8
							* var8 + var9 * var9 + 0.2F);
					parabolicField[var8 + 2 + (var9 + 2) * 5] = var10;
				}
			}
		}

		final double var44 = 684.412D;
		final double var45 = 684.412D;
		noise5 = noiseGen5.generateNoiseOctaves(noise5, par2, par4, par5, par7,
				1.121D, 1.121D, 0.5D);
		noise6 = noiseGen6.generateNoiseOctaves(noise6, par2, par4, par5, par7,
				200.0D, 200.0D, 0.5D);
		noise3 = noiseGen3.generateNoiseOctaves(noise3, par2, par3, par4, par5,
				par6, par7, var44 / 80.0D, var45 / 160.0D, var44 / 80.0D);
		noise1 = noiseGen1.generateNoiseOctaves(noise1, par2, par3, par4, par5,
				par6, par7, var44, var45, var44);
		noise2 = noiseGen2.generateNoiseOctaves(noise2, par2, par3, par4, par5,
				par6, par7, var44, var45, var44);
		int var12 = 0;
		int var13 = 0;

		for (int var14 = 0; var14 < par5; ++var14) {
			for (int var15 = 0; var15 < par7; ++var15) {
				float var16 = 0.0F;
				float var17 = 0.0F;
				float var18 = 0.0F;
				final byte var19 = 2;
				final BiomeGenBase var20 = biomesForGeneration[var14 + 2
						+ (var15 + 2) * (par5 + 5)];

				for (int var21 = -var19; var21 <= var19; ++var21) {
					for (int var22 = -var19; var22 <= var19; ++var22) {
						final BiomeGenBase var23 = biomesForGeneration[var14
								+ var21 + 2 + (var15 + var22 + 2) * (par5 + 5)];
						float var24 = parabolicField[var21 + 2 + (var22 + 2)
								* 5]
								/ (var23.minHeight + 2.0F);

						if (var23.minHeight > var20.minHeight) {
							var24 /= 2.0F;
						}

						var16 += var23.maxHeight * var24;
						var17 += var23.minHeight * var24;
						var18 += var24;
					}
				}

				var16 /= var18;
				var17 /= var18;
				var16 = var16 * 0.9F + 0.1F;
				var17 = (var17 * 4.0F - 1.0F) / 8.0F;
				double var47 = noise6[var13] / 8000.0D;

				if (var47 < 0.0D) {
					var47 = -var47 * 0.3D;
				}

				var47 = var47 * 3.0D - 2.0D;

				if (var47 < 0.0D) {
					var47 /= 2.0D;

					if (var47 < -1.0D) {
						var47 = -1.0D;
					}

					var47 /= 1.4D;
					var47 /= 2.0D;
				} else {
					if (var47 > 1.0D) {
						var47 = 1.0D;
					}

					var47 /= 8.0D;
				}

				++var13;

				for (int var46 = 0; var46 < par6; ++var46) {
					double var48 = var17;
					final double var26 = var16;
					var48 += var47 * 0.2D;
					var48 = var48 * par6 / 16.0D;
					final double var28 = par6 / 2.0D + var48 * 4.0D;
					double var30 = 0.0D;
					double var32 = (var46 - var28) * 12.0D * 128.0D / 128.0D
							/ var26;

					if (var32 < 0.0D) {
						var32 *= 4.0D;
					}

					final double var34 = noise1[var12] / 512.0D;
					final double var36 = noise2[var12] / 512.0D;
					final double var38 = (noise3[var12] / 10.0D + 1.0D) / 2.0D;

					if (var38 < 0.0D) {
						var30 = var34;
					} else if (var38 > 1.0D) {
						var30 = var36;
					} else {
						var30 = var34 + (var36 - var34) * var38;
					}

					var30 -= var32;

					if (var46 > par6 - 4) {
						final double var40 = (var46 - (par6 - 4)) / 3.0F;
						var30 = var30 * (1.0D - var40) + -10.0D * var40;
					}

					par1ArrayOfDouble[var12] = var30;
					++var12;
				}
			}
		}

		return par1ArrayOfDouble;
	}

	/**
	 * Checks to see if a chunk exists at x, y
	 */
	@Override
	public boolean chunkExists(final int par1, final int par2) {
		return true;
	}

	/**
	 * Populates chunk with ores etc etc
	 */
	@Override
	public void populate(final IChunkProvider par1IChunkProvider,
			final int par2, final int par3) {
		BlockSand.fallInstantly = true;
		int var4 = par2 * 16;
		int var5 = par3 * 16;
		final BiomeGenBase var6 = worldObj.getBiomeGenForCoords(var4 + 16,
				var5 + 16);
		rand.setSeed(worldObj.getSeed());
		final long var7 = rand.nextLong() / 2L * 2L + 1L;
		final long var9 = rand.nextLong() / 2L * 2L + 1L;
		rand.setSeed(par2 * var7 + par3 * var9 ^ worldObj.getSeed());
		boolean var11 = false;

		if (mapFeaturesEnabled) {
			mineshaftGenerator.generateStructuresInChunk(worldObj, rand, par2,
					par3);
			var11 = villageGenerator.generateStructuresInChunk(worldObj, rand,
					par2, par3);
			strongholdGenerator.generateStructuresInChunk(worldObj, rand, par2,
					par3);
			scatteredFeatureGenerator.generateStructuresInChunk(worldObj, rand,
					par2, par3);
		}

		int var12;
		int var13;
		int var14;

		if (!var11 && rand.nextInt(4) == 0) {
			var12 = var4 + rand.nextInt(16) + 8;
			var13 = rand.nextInt(128);
			var14 = var5 + rand.nextInt(16) + 8;
			new WorldGenLakes(Block.waterStill.blockID).generate(worldObj,
					rand, var12, var13, var14);
		}

		if (!var11 && rand.nextInt(8) == 0) {
			var12 = var4 + rand.nextInt(16) + 8;
			var13 = rand.nextInt(rand.nextInt(120) + 8);
			var14 = var5 + rand.nextInt(16) + 8;

			if (var13 < 63 || rand.nextInt(10) == 0) {
				new WorldGenLakes(Block.lavaStill.blockID).generate(worldObj,
						rand, var12, var13, var14);
			}
		}

		for (var12 = 0; var12 < 8; ++var12) {
			var13 = var4 + rand.nextInt(16) + 8;
			var14 = rand.nextInt(128);
			final int var15 = var5 + rand.nextInt(16) + 8;

			if (new WorldGenDungeons().generate(worldObj, rand, var13, var14,
					var15)) {
				;
			}
		}

		var6.decorate(worldObj, rand, var4, var5);
		SpawnerAnimals.performWorldGenSpawning(worldObj, var6, var4 + 8,
				var5 + 8, 16, 16, rand);
		var4 += 8;
		var5 += 8;

		for (var12 = 0; var12 < 16; ++var12) {
			for (var13 = 0; var13 < 16; ++var13) {
				var14 = worldObj.getPrecipitationHeight(var4 + var12, var5
						+ var13);

				if (worldObj.isBlockFreezable(var12 + var4, var14 - 1, var13
						+ var5)) {
					worldObj.setBlock(var12 + var4, var14 - 1, var13 + var5,
							Block.ice.blockID, 0, 2);
				}

				if (worldObj.canSnowAt(var12 + var4, var14, var13 + var5)) {
					worldObj.setBlock(var12 + var4, var14, var13 + var5,
							Block.snow.blockID, 0, 2);
				}
			}
		}

		BlockSand.fallInstantly = false;
	}

	/**
	 * Two modes of operation: if passed true, save all Chunks in one go. If
	 * passed false, save up to two chunks. Return true if all chunks have been
	 * saved.
	 */
	@Override
	public boolean saveChunks(final boolean par1,
			final IProgressUpdate par2IProgressUpdate) {
		return true;
	}

	@Override
	public void func_104112_b() {
	}

	/**
	 * Unloads chunks that are marked to be unloaded. This is not guaranteed to
	 * unload every such chunk.
	 */
	@Override
	public boolean unloadQueuedChunks() {
		return false;
	}

	/**
	 * Returns if the IChunkProvider supports saving.
	 */
	@Override
	public boolean canSave() {
		return true;
	}

	/**
	 * Converts the instance data to a readable string.
	 */
	@Override
	public String makeString() {
		return "RandomLevelSource";
	}

	/**
	 * Returns a list of creatures of the specified type that can spawn at the
	 * given location.
	 */
	@Override
	public List getPossibleCreatures(
			final EnumCreatureType par1EnumCreatureType, final int par2,
			final int par3, final int par4) {
		final BiomeGenBase var5 = worldObj.getBiomeGenForCoords(par2, par4);
		return var5 == null ? null
				: var5 == BiomeGenBase.swampland
						&& par1EnumCreatureType == EnumCreatureType.monster
						&& scatteredFeatureGenerator.hasStructureAt(par2, par3,
								par4) ? scatteredFeatureGenerator
						.getScatteredFeatureSpawnList() : var5
						.getSpawnableList(par1EnumCreatureType);
	}

	/**
	 * Returns the location of the closest structure of the specified type. If
	 * not found returns null.
	 */
	@Override
	public ChunkPosition findClosestStructure(final World par1World,
			final String par2Str, final int par3, final int par4, final int par5) {
		return "Stronghold".equals(par2Str) && strongholdGenerator != null ? strongholdGenerator
				.getNearestInstance(par1World, par3, par4, par5) : null;
	}

	@Override
	public int getLoadedChunkCount() {
		return 0;
	}

	@Override
	public void recreateStructures(final int par1, final int par2) {
		if (mapFeaturesEnabled) {
			mineshaftGenerator.generate(this, worldObj, par1, par2,
					(byte[]) null);
			villageGenerator
					.generate(this, worldObj, par1, par2, (byte[]) null);
			strongholdGenerator.generate(this, worldObj, par1, par2,
					(byte[]) null);
			scatteredFeatureGenerator.generate(this, worldObj, par1, par2,
					(byte[]) null);
		}
	}
}
