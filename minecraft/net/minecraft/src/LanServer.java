package net.minecraft.src;

import net.minecraft.client.Minecraft;

public class LanServer {
	private final String lanServerMotd;
	private final String lanServerIpPort;

	/** Last time this LanServer was seen. */
	private long timeLastSeen;

	public LanServer(final String par1Str, final String par2Str) {
		lanServerMotd = par1Str;
		lanServerIpPort = par2Str;
		timeLastSeen = Minecraft.getSystemTime();
	}

	public String getServerMotd() {
		return lanServerMotd;
	}

	public String getServerIpPort() {
		return lanServerIpPort;
	}

	/**
	 * Updates the time this LanServer was last seen.
	 */
	public void updateLastSeen() {
		timeLastSeen = Minecraft.getSystemTime();
	}
}
