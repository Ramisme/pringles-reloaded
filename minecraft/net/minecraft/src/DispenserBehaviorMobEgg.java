package net.minecraft.src;

final class DispenserBehaviorMobEgg extends BehaviorDefaultDispenseItem {
	/**
	 * Dispense the specified stack, play the dispense sound and spawn
	 * particles.
	 */
	@Override
	public ItemStack dispenseStack(final IBlockSource par1IBlockSource,
			final ItemStack par2ItemStack) {
		final EnumFacing var3 = BlockDispenser.getFacing(par1IBlockSource
				.getBlockMetadata());
		final double var4 = par1IBlockSource.getX() + var3.getFrontOffsetX();
		final double var6 = par1IBlockSource.getYInt() + 0.2F;
		final double var8 = par1IBlockSource.getZ() + var3.getFrontOffsetZ();
		final Entity var10 = ItemMonsterPlacer.spawnCreature(
				par1IBlockSource.getWorld(), par2ItemStack.getItemDamage(),
				var4, var6, var8);

		if (var10 instanceof EntityLiving && par2ItemStack.hasDisplayName()) {
			((EntityLiving) var10).func_94058_c(par2ItemStack.getDisplayName());
		}

		par2ItemStack.splitStack(1);
		return par2ItemStack;
	}
}
