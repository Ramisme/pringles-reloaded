package net.minecraft.src;

public class EntityAITargetNonTamed extends EntityAINearestAttackableTarget {
	private final EntityTameable theTameable;

	public EntityAITargetNonTamed(final EntityTameable par1EntityTameable,
			final Class par2Class, final float par3, final int par4,
			final boolean par5) {
		super(par1EntityTameable, par2Class, par3, par4, par5);
		theTameable = par1EntityTameable;
	}

	/**
	 * Returns whether the EntityAIBase should begin execution.
	 */
	@Override
	public boolean shouldExecute() {
		return theTameable.isTamed() ? false : super.shouldExecute();
	}
}
