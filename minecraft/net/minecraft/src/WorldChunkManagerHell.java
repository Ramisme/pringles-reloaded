package net.minecraft.src;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class WorldChunkManagerHell extends WorldChunkManager {
	/** The biome generator object. */
	private final BiomeGenBase biomeGenerator;
	private final float hellTemperature;

	/** The rainfall in the world */
	private final float rainfall;

	public WorldChunkManagerHell(final BiomeGenBase par1BiomeGenBase,
			final float par2, final float par3) {
		biomeGenerator = par1BiomeGenBase;
		hellTemperature = par2;
		rainfall = par3;
	}

	/**
	 * Returns the BiomeGenBase related to the x, z position on the world.
	 */
	@Override
	public BiomeGenBase getBiomeGenAt(final int par1, final int par2) {
		return biomeGenerator;
	}

	/**
	 * Returns an array of biomes for the location input.
	 */
	@Override
	public BiomeGenBase[] getBiomesForGeneration(
			BiomeGenBase[] par1ArrayOfBiomeGenBase, final int par2,
			final int par3, final int par4, final int par5) {
		if (par1ArrayOfBiomeGenBase == null
				|| par1ArrayOfBiomeGenBase.length < par4 * par5) {
			par1ArrayOfBiomeGenBase = new BiomeGenBase[par4 * par5];
		}

		Arrays.fill(par1ArrayOfBiomeGenBase, 0, par4 * par5, biomeGenerator);
		return par1ArrayOfBiomeGenBase;
	}

	/**
	 * Returns a list of temperatures to use for the specified blocks. Args:
	 * listToReuse, x, y, width, length
	 */
	@Override
	public float[] getTemperatures(float[] par1ArrayOfFloat, final int par2,
			final int par3, final int par4, final int par5) {
		if (par1ArrayOfFloat == null || par1ArrayOfFloat.length < par4 * par5) {
			par1ArrayOfFloat = new float[par4 * par5];
		}

		Arrays.fill(par1ArrayOfFloat, 0, par4 * par5, hellTemperature);
		return par1ArrayOfFloat;
	}

	/**
	 * Returns a list of rainfall values for the specified blocks. Args:
	 * listToReuse, x, z, width, length.
	 */
	@Override
	public float[] getRainfall(float[] par1ArrayOfFloat, final int par2,
			final int par3, final int par4, final int par5) {
		if (par1ArrayOfFloat == null || par1ArrayOfFloat.length < par4 * par5) {
			par1ArrayOfFloat = new float[par4 * par5];
		}

		Arrays.fill(par1ArrayOfFloat, 0, par4 * par5, rainfall);
		return par1ArrayOfFloat;
	}

	/**
	 * Returns biomes to use for the blocks and loads the other data like
	 * temperature and humidity onto the WorldChunkManager Args: oldBiomeList,
	 * x, z, width, depth
	 */
	@Override
	public BiomeGenBase[] loadBlockGeneratorData(
			BiomeGenBase[] par1ArrayOfBiomeGenBase, final int par2,
			final int par3, final int par4, final int par5) {
		if (par1ArrayOfBiomeGenBase == null
				|| par1ArrayOfBiomeGenBase.length < par4 * par5) {
			par1ArrayOfBiomeGenBase = new BiomeGenBase[par4 * par5];
		}

		Arrays.fill(par1ArrayOfBiomeGenBase, 0, par4 * par5, biomeGenerator);
		return par1ArrayOfBiomeGenBase;
	}

	/**
	 * Return a list of biomes for the specified blocks. Args: listToReuse, x,
	 * y, width, length, cacheFlag (if false, don't check biomeCache to avoid
	 * infinite loop in BiomeCacheBlock)
	 */
	@Override
	public BiomeGenBase[] getBiomeGenAt(
			final BiomeGenBase[] par1ArrayOfBiomeGenBase, final int par2,
			final int par3, final int par4, final int par5, final boolean par6) {
		return loadBlockGeneratorData(par1ArrayOfBiomeGenBase, par2, par3,
				par4, par5);
	}

	/**
	 * Finds a valid position within a range, that is in one of the listed
	 * biomes. Searches {par1,par2} +-par3 blocks. Strongly favors positive y
	 * positions.
	 */
	@Override
	public ChunkPosition findBiomePosition(final int par1, final int par2,
			final int par3, final List par4List, final Random par5Random) {
		return par4List.contains(biomeGenerator) ? new ChunkPosition(par1
				- par3 + par5Random.nextInt(par3 * 2 + 1), 0, par2 - par3
				+ par5Random.nextInt(par3 * 2 + 1)) : null;
	}

	/**
	 * checks given Chunk's Biomes against List of allowed ones
	 */
	@Override
	public boolean areBiomesViable(final int par1, final int par2,
			final int par3, final List par4List) {
		return par4List.contains(biomeGenerator);
	}
}
