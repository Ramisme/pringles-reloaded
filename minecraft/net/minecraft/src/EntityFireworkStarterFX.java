package net.minecraft.src;

import net.minecraft.client.Minecraft;

public class EntityFireworkStarterFX extends EntityFX {
	private int field_92042_ax = 0;
	private final EffectRenderer field_92040_ay;
	private NBTTagList fireworkExplosions;
	boolean field_92041_a;

	public EntityFireworkStarterFX(final World par1World, final double par2,
			final double par4, final double par6, final double par8,
			final double par10, final double par12,
			final EffectRenderer par14EffectRenderer,
			final NBTTagCompound par15NBTTagCompound) {
		super(par1World, par2, par4, par6, 0.0D, 0.0D, 0.0D);
		motionX = par8;
		motionY = par10;
		motionZ = par12;
		field_92040_ay = par14EffectRenderer;
		particleMaxAge = 8;

		if (par15NBTTagCompound != null) {
			fireworkExplosions = par15NBTTagCompound.getTagList("Explosions");

			if (fireworkExplosions.tagCount() == 0) {
				fireworkExplosions = null;
			} else {
				particleMaxAge = fireworkExplosions.tagCount() * 2 - 1;

				for (int var16 = 0; var16 < fireworkExplosions.tagCount(); ++var16) {
					final NBTTagCompound var17 = (NBTTagCompound) fireworkExplosions
							.tagAt(var16);

					if (var17.getBoolean("Flicker")) {
						field_92041_a = true;
						particleMaxAge += 15;
						break;
					}
				}
			}
		}
	}

	@Override
	public void renderParticle(final Tessellator par1Tessellator,
			final float par2, final float par3, final float par4,
			final float par5, final float par6, final float par7) {
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate() {
		boolean var1;

		if (field_92042_ax == 0 && fireworkExplosions != null) {
			var1 = func_92037_i();
			boolean var2 = false;

			if (fireworkExplosions.tagCount() >= 3) {
				var2 = true;
			} else {
				for (int var3 = 0; var3 < fireworkExplosions.tagCount(); ++var3) {
					final NBTTagCompound var4 = (NBTTagCompound) fireworkExplosions
							.tagAt(var3);

					if (var4.getByte("Type") == 1) {
						var2 = true;
						break;
					}
				}
			}

			final String var15 = "fireworks." + (var2 ? "largeBlast" : "blast")
					+ (var1 ? "_far" : "");
			worldObj.playSound(posX, posY, posZ, var15, 20.0F,
					0.95F + rand.nextFloat() * 0.1F, true);
		}

		if (field_92042_ax % 2 == 0 && fireworkExplosions != null
				&& field_92042_ax / 2 < fireworkExplosions.tagCount()) {
			final int var13 = field_92042_ax / 2;
			final NBTTagCompound var14 = (NBTTagCompound) fireworkExplosions
					.tagAt(var13);
			final byte var17 = var14.getByte("Type");
			final boolean var18 = var14.getBoolean("Trail");
			final boolean var5 = var14.getBoolean("Flicker");
			final int[] var6 = var14.getIntArray("Colors");
			final int[] var7 = var14.getIntArray("FadeColors");

			if (var17 == 1) {
				func_92035_a(0.5D, 4, var6, var7, var18, var5);
			} else if (var17 == 2) {
				func_92038_a(0.5D, new double[][] { { 0.0D, 1.0D },
						{ 0.3455D, 0.309D }, { 0.9511D, 0.309D },
						{ 0.3795918367346939D, -0.12653061224489795D },
						{ 0.6122448979591837D, -0.8040816326530612D },
						{ 0.0D, -0.35918367346938773D } }, var6, var7, var18,
						var5, false);
			} else if (var17 == 3) {
				func_92038_a(0.5D, new double[][] { { 0.0D, 0.2D },
						{ 0.2D, 0.2D }, { 0.2D, 0.6D }, { 0.6D, 0.6D },
						{ 0.6D, 0.2D }, { 0.2D, 0.2D }, { 0.2D, 0.0D },
						{ 0.4D, 0.0D }, { 0.4D, -0.6D }, { 0.2D, -0.6D },
						{ 0.2D, -0.4D }, { 0.0D, -0.4D } }, var6, var7, var18,
						var5, true);
			} else if (var17 == 4) {
				func_92036_a(var6, var7, var18, var5);
			} else {
				func_92035_a(0.25D, 2, var6, var7, var18, var5);
			}

			final int var8 = var6[0];
			final float var9 = ((var8 & 16711680) >> 16) / 255.0F;
			final float var10 = ((var8 & 65280) >> 8) / 255.0F;
			final float var11 = ((var8 & 255) >> 0) / 255.0F;
			final EntityFireworkOverlayFX var12 = new EntityFireworkOverlayFX(
					worldObj, posX, posY, posZ);
			var12.setRBGColorF(var9, var10, var11);
			field_92040_ay.addEffect(var12);
		}

		++field_92042_ax;

		if (field_92042_ax > particleMaxAge) {
			if (field_92041_a) {
				var1 = func_92037_i();
				final String var16 = "fireworks."
						+ (var1 ? "twinkle_far" : "twinkle");
				worldObj.playSound(posX, posY, posZ, var16, 20.0F,
						0.9F + rand.nextFloat() * 0.15F, true);
			}

			setDead();
		}
	}

	private boolean func_92037_i() {
		final Minecraft var1 = Minecraft.getMinecraft();
		return var1 == null
				|| var1.renderViewEntity == null
				|| var1.renderViewEntity.getDistanceSq(posX, posY, posZ) >= 256.0D;
	}

	private void func_92034_a(final double par1, final double par3,
			final double par5, final double par7, final double par9,
			final double par11, final int[] par13ArrayOfInteger,
			final int[] par14ArrayOfInteger, final boolean par15,
			final boolean par16) {
		final EntityFireworkSparkFX var17 = new EntityFireworkSparkFX(worldObj,
				par1, par3, par5, par7, par9, par11, field_92040_ay);
		var17.func_92045_e(par15);
		var17.func_92043_f(par16);
		final int var18 = rand.nextInt(par13ArrayOfInteger.length);
		var17.func_92044_a(par13ArrayOfInteger[var18]);

		if (par14ArrayOfInteger != null && par14ArrayOfInteger.length > 0) {
			var17.func_92046_g(par14ArrayOfInteger[rand
					.nextInt(par14ArrayOfInteger.length)]);
		}

		field_92040_ay.addEffect(var17);
	}

	private void func_92035_a(final double par1, final int par3,
			final int[] par4ArrayOfInteger, final int[] par5ArrayOfInteger,
			final boolean par6, final boolean par7) {
		final double var8 = posX;
		final double var10 = posY;
		final double var12 = posZ;

		for (int var14 = -par3; var14 <= par3; ++var14) {
			for (int var15 = -par3; var15 <= par3; ++var15) {
				for (int var16 = -par3; var16 <= par3; ++var16) {
					final double var17 = var15
							+ (rand.nextDouble() - rand.nextDouble()) * 0.5D;
					final double var19 = var14
							+ (rand.nextDouble() - rand.nextDouble()) * 0.5D;
					final double var21 = var16
							+ (rand.nextDouble() - rand.nextDouble()) * 0.5D;
					final double var23 = MathHelper.sqrt_double(var17 * var17
							+ var19 * var19 + var21 * var21)
							/ par1 + rand.nextGaussian() * 0.05D;
					func_92034_a(var8, var10, var12, var17 / var23, var19
							/ var23, var21 / var23, par4ArrayOfInteger,
							par5ArrayOfInteger, par6, par7);

					if (var14 != -par3 && var14 != par3 && var15 != -par3
							&& var15 != par3) {
						var16 += par3 * 2 - 1;
					}
				}
			}
		}
	}

	private void func_92038_a(final double par1,
			final double[][] par3ArrayOfDouble, final int[] par4ArrayOfInteger,
			final int[] par5ArrayOfInteger, final boolean par6,
			final boolean par7, final boolean par8) {
		final double var9 = par3ArrayOfDouble[0][0];
		final double var11 = par3ArrayOfDouble[0][1];
		func_92034_a(posX, posY, posZ, var9 * par1, var11 * par1, 0.0D,
				par4ArrayOfInteger, par5ArrayOfInteger, par6, par7);
		final float var13 = rand.nextFloat() * (float) Math.PI;
		final double var14 = par8 ? 0.034D : 0.34D;

		for (int var16 = 0; var16 < 3; ++var16) {
			final double var17 = var13 + var16 * (float) Math.PI * var14;
			double var19 = var9;
			double var21 = var11;

			for (int var23 = 1; var23 < par3ArrayOfDouble.length; ++var23) {
				final double var24 = par3ArrayOfDouble[var23][0];
				final double var26 = par3ArrayOfDouble[var23][1];

				for (double var28 = 0.25D; var28 <= 1.0D; var28 += 0.25D) {
					double var30 = (var19 + (var24 - var19) * var28) * par1;
					final double var32 = (var21 + (var26 - var21) * var28)
							* par1;
					final double var34 = var30 * Math.sin(var17);
					var30 *= Math.cos(var17);

					for (double var36 = -1.0D; var36 <= 1.0D; var36 += 2.0D) {
						func_92034_a(posX, posY, posZ, var30 * var36, var32,
								var34 * var36, par4ArrayOfInteger,
								par5ArrayOfInteger, par6, par7);
					}
				}

				var19 = var24;
				var21 = var26;
			}
		}
	}

	private void func_92036_a(final int[] par1ArrayOfInteger,
			final int[] par2ArrayOfInteger, final boolean par3,
			final boolean par4) {
		final double var5 = rand.nextGaussian() * 0.05D;
		final double var7 = rand.nextGaussian() * 0.05D;

		for (int var9 = 0; var9 < 70; ++var9) {
			final double var10 = motionX * 0.5D + rand.nextGaussian() * 0.15D
					+ var5;
			final double var12 = motionZ * 0.5D + rand.nextGaussian() * 0.15D
					+ var7;
			final double var14 = motionY * 0.5D + rand.nextDouble() * 0.5D;
			func_92034_a(posX, posY, posZ, var10, var14, var12,
					par1ArrayOfInteger, par2ArrayOfInteger, par3, par4);
		}
	}

	@Override
	public int getFXLayer() {
		return 0;
	}
}
