package net.minecraft.src;

import java.util.Iterator;
import java.util.Random;

public class BlockBed extends BlockDirectional {
	/** Maps the foot-of-bed block to the head-of-bed block. */
	public static final int[][] footBlockToHeadBlockMap = new int[][] {
			{ 0, 1 }, { -1, 0 }, { 0, -1 }, { 1, 0 } };
	private Icon[] field_94472_b;
	private Icon[] bedSideIcons;
	private Icon[] bedTopIcons;

	public BlockBed(final int par1) {
		super(par1, Material.cloth);
		setBounds();
	}

	/**
	 * Called upon block activation (right click on the block.)
	 */
	@Override
	public boolean onBlockActivated(final World par1World, int par2,
			final int par3, int par4, final EntityPlayer par5EntityPlayer,
			final int par6, final float par7, final float par8, final float par9) {
		if (par1World.isRemote) {
			return true;
		} else {
			int var10 = par1World.getBlockMetadata(par2, par3, par4);

			if (!BlockBed.isBlockHeadOfBed(var10)) {
				final int var11 = BlockDirectional.getDirection(var10);
				par2 += BlockBed.footBlockToHeadBlockMap[var11][0];
				par4 += BlockBed.footBlockToHeadBlockMap[var11][1];

				if (par1World.getBlockId(par2, par3, par4) != blockID) {
					return true;
				}

				var10 = par1World.getBlockMetadata(par2, par3, par4);
			}

			if (par1World.provider.canRespawnHere()
					&& par1World.getBiomeGenForCoords(par2, par4) != BiomeGenBase.hell) {
				if (BlockBed.isBedOccupied(var10)) {
					EntityPlayer var20 = null;
					final Iterator var12 = par1World.playerEntities.iterator();

					while (var12.hasNext()) {
						final EntityPlayer var21 = (EntityPlayer) var12.next();

						if (var21.isPlayerSleeping()) {
							final ChunkCoordinates var14 = var21.playerLocation;

							if (var14.posX == par2 && var14.posY == par3
									&& var14.posZ == par4) {
								var20 = var21;
							}
						}
					}

					if (var20 != null) {
						par5EntityPlayer.addChatMessage("tile.bed.occupied");
						return true;
					}

					BlockBed.setBedOccupied(par1World, par2, par3, par4, false);
				}

				final EnumStatus var19 = par5EntityPlayer.sleepInBedAt(par2,
						par3, par4);

				if (var19 == EnumStatus.OK) {
					BlockBed.setBedOccupied(par1World, par2, par3, par4, true);
					return true;
				} else {
					if (var19 == EnumStatus.NOT_POSSIBLE_NOW) {
						par5EntityPlayer.addChatMessage("tile.bed.noSleep");
					} else if (var19 == EnumStatus.NOT_SAFE) {
						par5EntityPlayer.addChatMessage("tile.bed.notSafe");
					}

					return true;
				}
			} else {
				double var18 = par2 + 0.5D;
				double var13 = par3 + 0.5D;
				double var15 = par4 + 0.5D;
				par1World.setBlockToAir(par2, par3, par4);
				final int var17 = BlockDirectional.getDirection(var10);
				par2 += BlockBed.footBlockToHeadBlockMap[var17][0];
				par4 += BlockBed.footBlockToHeadBlockMap[var17][1];

				if (par1World.getBlockId(par2, par3, par4) == blockID) {
					par1World.setBlockToAir(par2, par3, par4);
					var18 = (var18 + par2 + 0.5D) / 2.0D;
					var13 = (var13 + par3 + 0.5D) / 2.0D;
					var15 = (var15 + par4 + 0.5D) / 2.0D;
				}

				par1World.newExplosion((Entity) null, par2 + 0.5F, par3 + 0.5F,
						par4 + 0.5F, 5.0F, true, true);
				return true;
			}
		}
	}

	/**
	 * From the specified side and block metadata retrieves the blocks texture.
	 * Args: side, metadata
	 */
	@Override
	public Icon getIcon(final int par1, final int par2) {
		if (par1 == 0) {
			return Block.planks.getBlockTextureFromSide(par1);
		} else {
			final int var3 = BlockDirectional.getDirection(par2);
			final int var4 = Direction.bedDirection[var3][par1];
			final int var5 = BlockBed.isBlockHeadOfBed(par2) ? 1 : 0;
			return (var5 != 1 || var4 != 2) && (var5 != 0 || var4 != 3) ? var4 != 5
					&& var4 != 4 ? bedTopIcons[var5] : bedSideIcons[var5]
					: field_94472_b[var5];
		}
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		bedTopIcons = new Icon[] {
				par1IconRegister.registerIcon("bed_feet_top"),
				par1IconRegister.registerIcon("bed_head_top") };
		field_94472_b = new Icon[] {
				par1IconRegister.registerIcon("bed_feet_end"),
				par1IconRegister.registerIcon("bed_head_end") };
		bedSideIcons = new Icon[] {
				par1IconRegister.registerIcon("bed_feet_side"),
				par1IconRegister.registerIcon("bed_head_side") };
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return 14;
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * Updates the blocks bounds based on its current state. Args: world, x, y,
	 * z
	 */
	@Override
	public void setBlockBoundsBasedOnState(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		setBounds();
	}

	/**
	 * Lets the block know when one of its neighbor changes. Doesn't know which
	 * neighbor changed (coordinates passed are their own) Args: x, y, z,
	 * neighbor blockID
	 */
	@Override
	public void onNeighborBlockChange(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		final int var6 = par1World.getBlockMetadata(par2, par3, par4);
		final int var7 = BlockDirectional.getDirection(var6);

		if (BlockBed.isBlockHeadOfBed(var6)) {
			if (par1World.getBlockId(par2
					- BlockBed.footBlockToHeadBlockMap[var7][0], par3, par4
					- BlockBed.footBlockToHeadBlockMap[var7][1]) != blockID) {
				par1World.setBlockToAir(par2, par3, par4);
			}
		} else if (par1World.getBlockId(par2
				+ BlockBed.footBlockToHeadBlockMap[var7][0], par3, par4
				+ BlockBed.footBlockToHeadBlockMap[var7][1]) != blockID) {
			par1World.setBlockToAir(par2, par3, par4);

			if (!par1World.isRemote) {
				dropBlockAsItem(par1World, par2, par3, par4, var6, 0);
			}
		}
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return BlockBed.isBlockHeadOfBed(par1) ? 0 : Item.bed.itemID;
	}

	/**
	 * Set the bounds of the bed block.
	 */
	private void setBounds() {
		setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.5625F, 1.0F);
	}

	/**
	 * Returns whether or not this bed block is the head of the bed.
	 */
	public static boolean isBlockHeadOfBed(final int par0) {
		return (par0 & 8) != 0;
	}

	/**
	 * Return whether or not the bed is occupied.
	 */
	public static boolean isBedOccupied(final int par0) {
		return (par0 & 4) != 0;
	}

	/**
	 * Sets whether or not the bed is occupied.
	 */
	public static void setBedOccupied(final World par0World, final int par1,
			final int par2, final int par3, final boolean par4) {
		int var5 = par0World.getBlockMetadata(par1, par2, par3);

		if (par4) {
			var5 |= 4;
		} else {
			var5 &= -5;
		}

		par0World.setBlockMetadataWithNotify(par1, par2, par3, var5, 4);
	}

	/**
	 * Gets the nearest empty chunk coordinates for the player to wake up from a
	 * bed into.
	 */
	public static ChunkCoordinates getNearestEmptyChunkCoordinates(
			final World par0World, final int par1, final int par2,
			final int par3, int par4) {
		final int var5 = par0World.getBlockMetadata(par1, par2, par3);
		final int var6 = BlockDirectional.getDirection(var5);

		for (int var7 = 0; var7 <= 1; ++var7) {
			final int var8 = par1 - BlockBed.footBlockToHeadBlockMap[var6][0]
					* var7 - 1;
			final int var9 = par3 - BlockBed.footBlockToHeadBlockMap[var6][1]
					* var7 - 1;
			final int var10 = var8 + 2;
			final int var11 = var9 + 2;

			for (int var12 = var8; var12 <= var10; ++var12) {
				for (int var13 = var9; var13 <= var11; ++var13) {
					if (par0World.doesBlockHaveSolidTopSurface(var12, par2 - 1,
							var13)
							&& par0World.isAirBlock(var12, par2, var13)
							&& par0World.isAirBlock(var12, par2 + 1, var13)) {
						if (par4 <= 0) {
							return new ChunkCoordinates(var12, par2, var13);
						}

						--par4;
					}
				}
			}
		}

		return null;
	}

	/**
	 * Drops the block items with a specified chance of dropping the specified
	 * items
	 */
	@Override
	public void dropBlockAsItemWithChance(final World par1World,
			final int par2, final int par3, final int par4, final int par5,
			final float par6, final int par7) {
		if (!BlockBed.isBlockHeadOfBed(par5)) {
			super.dropBlockAsItemWithChance(par1World, par2, par3, par4, par5,
					par6, 0);
		}
	}

	/**
	 * Returns the mobility information of the block, 0 = free, 1 = can't push
	 * but can move over, 2 = total immobility and stop pistons
	 */
	@Override
	public int getMobilityFlag() {
		return 1;
	}

	/**
	 * only called by clickMiddleMouseButton , and passed to
	 * inventory.setCurrentItem (along with isCreative)
	 */
	@Override
	public int idPicked(final World par1World, final int par2, final int par3,
			final int par4) {
		return Item.bed.itemID;
	}

	/**
	 * Called when the block is attempted to be harvested
	 */
	@Override
	public void onBlockHarvested(final World par1World, int par2,
			final int par3, int par4, final int par5,
			final EntityPlayer par6EntityPlayer) {
		if (par6EntityPlayer.capabilities.isCreativeMode
				&& BlockBed.isBlockHeadOfBed(par5)) {
			final int var7 = BlockDirectional.getDirection(par5);
			par2 -= BlockBed.footBlockToHeadBlockMap[var7][0];
			par4 -= BlockBed.footBlockToHeadBlockMap[var7][1];

			if (par1World.getBlockId(par2, par3, par4) == blockID) {
				par1World.setBlockToAir(par2, par3, par4);
			}
		}
	}
}
