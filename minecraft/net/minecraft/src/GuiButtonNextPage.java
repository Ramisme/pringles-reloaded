package net.minecraft.src;

import net.minecraft.client.Minecraft;

import org.lwjgl.opengl.GL11;

class GuiButtonNextPage extends GuiButton {
	/**
	 * True for pointing right (next page), false for pointing left (previous
	 * page).
	 */
	private final boolean nextPage;

	public GuiButtonNextPage(final int par1, final int par2, final int par3,
			final boolean par4) {
		super(par1, par2, par3, 23, 13, "");
		nextPage = par4;
	}

	/**
	 * Draws this button to the screen.
	 */
	@Override
	public void drawButton(final Minecraft par1Minecraft, final int par2,
			final int par3) {
		if (drawButton) {
			final boolean var4 = par2 >= xPosition && par3 >= yPosition
					&& par2 < xPosition + width && par3 < yPosition + height;
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			par1Minecraft.renderEngine.bindTexture("/gui/book.png");
			int var5 = 0;
			int var6 = 192;

			if (var4) {
				var5 += 23;
			}

			if (!nextPage) {
				var6 += 13;
			}

			drawTexturedModalRect(xPosition, yPosition, var5, var6, 23, 13);
		}
	}
}
