package net.minecraft.src;

import java.util.concurrent.Callable;

class CallableLevelWeather implements Callable {
	final WorldInfo worldInfoInstance;

	CallableLevelWeather(final WorldInfo par1WorldInfo) {
		worldInfoInstance = par1WorldInfo;
	}

	public String callLevelWeatherInfo() {
		return String
				.format("Rain time: %d (now: %b), thunder time: %d (now: %b)",
						new Object[] {
								Integer.valueOf(WorldInfo
										.getRainTime(worldInfoInstance)),
								Boolean.valueOf(WorldInfo
										.getRaining(worldInfoInstance)),
								Integer.valueOf(WorldInfo
										.getThunderTime(worldInfoInstance)),
								Boolean.valueOf(WorldInfo
										.getThundering(worldInfoInstance)) });
	}

	@Override
	public Object call() {
		return callLevelWeatherInfo();
	}
}
