package net.minecraft.src;

import java.io.IOException;
import java.text.DateFormat;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.lwjgl.input.Keyboard;

public class GuiScreenSubscription extends GuiScreen {
	private final GuiScreen field_98067_a;
	private final McoServer field_98065_b;
	private int field_98068_n;
	private String field_98069_o;

	public GuiScreenSubscription(final GuiScreen par1GuiScreen,
			final McoServer par2McoServer) {
		field_98067_a = par1GuiScreen;
		field_98065_b = par2McoServer;
	}

	/**
	 * Called from the main game loop to update the screen.
	 */
	@Override
	public void updateScreen() {
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui() {
		func_98063_a(field_98065_b.field_96408_a);
		final StringTranslate var1 = StringTranslate.getInstance();
		Keyboard.enableRepeatEvents(true);
		buttonList.add(new GuiButton(0, width / 2 - 100, height / 4 + 120 + 12,
				var1.translateKey("gui.cancel")));
	}

	private void func_98063_a(final long par1) {
		final McoClient var3 = new McoClient(mc.session);

		try {
			final ValueObjectSubscription var4 = var3.func_98177_f(par1);
			field_98068_n = var4.field_98170_b;
			field_98069_o = func_98062_b(var4.field_98171_a);
		} catch (final ExceptionMcoService var5) {
			;
		} catch (final IOException var6) {
			;
		}
	}

	private String func_98062_b(final long par1) {
		final GregorianCalendar var3 = new GregorianCalendar(
				TimeZone.getDefault());
		var3.setTimeInMillis(par1);
		return DateFormat.getDateTimeInstance().format(var3.getTime());
	}

	/**
	 * Called when the screen is unloaded. Used to disable keyboard repeat
	 * events
	 */
	@Override
	public void onGuiClosed() {
		Keyboard.enableRepeatEvents(false);
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	@Override
	protected void actionPerformed(final GuiButton par1GuiButton) {
		if (par1GuiButton.enabled) {
			if (par1GuiButton.id == 0) {
				mc.displayGuiScreen(field_98067_a);
			} else if (par1GuiButton.id == 1) {
				;
			}
		}
	}

	/**
	 * Fired when a key is typed. This is the equivalent of
	 * KeyListener.keyTyped(KeyEvent e).
	 */
	@Override
	protected void keyTyped(final char par1, final int par2) {
	}

	/**
	 * Called when the mouse is clicked.
	 */
	@Override
	protected void mouseClicked(final int par1, final int par2, final int par3) {
		super.mouseClicked(par1, par2, par3);
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(final int par1, final int par2, final float par3) {
		final StringTranslate var4 = StringTranslate.getInstance();
		drawDefaultBackground();
		drawCenteredString(fontRenderer,
				var4.translateKey("mco.configure.world.subscription.title"),
				width / 2, 17, 16777215);
		drawString(fontRenderer,
				var4.translateKey("mco.configure.world.subscription.start"),
				width / 2 - 100, 53, 10526880);
		drawString(fontRenderer, field_98069_o, width / 2 - 100, 66, 16777215);
		drawString(fontRenderer,
				var4.translateKey("mco.configure.world.subscription.daysleft"),
				width / 2 - 100, 85, 10526880);
		drawString(fontRenderer, String.valueOf(field_98068_n),
				width / 2 - 100, 98, 16777215);
		super.drawScreen(par1, par2, par3);
	}
}
