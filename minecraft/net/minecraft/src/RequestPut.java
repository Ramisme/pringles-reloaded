package net.minecraft.src;

import java.io.OutputStream;

public class RequestPut extends Request {
	private final byte[] field_96369_c;

	public RequestPut(final String par1Str, final byte[] par2ArrayOfByte,
			final int par3, final int par4) {
		super(par1Str, par3, par4);
		field_96369_c = par2ArrayOfByte;
	}

	public RequestPut func_96368_f() {
		try {
			field_96367_a.setDoOutput(true);
			field_96367_a.setDoInput(true);
			field_96367_a.setRequestMethod("PUT");
			final OutputStream var1 = field_96367_a.getOutputStream();
			var1.write(field_96369_c);
			var1.flush();
			return this;
		} catch (final Exception var2) {
			throw new ExceptionMcoHttp("Failed URL: " + field_96365_b, var2);
		}
	}

	@Override
	public Request func_96359_e() {
		return func_96368_f();
	}
}
