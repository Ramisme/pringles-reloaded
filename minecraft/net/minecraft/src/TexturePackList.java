package net.minecraft.src;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.minecraft.client.Minecraft;

public class TexturePackList {
	/**
	 * An instance of TexturePackDefault for the always available builtin
	 * texture pack.
	 */
	private static final ITexturePack defaultTexturePack = new TexturePackDefault();

	/** The Minecraft instance. */
	private final Minecraft mc;

	/** The directory the texture packs will be loaded from. */
	private final File texturePackDir;

	/** Folder for the multi-player texturepacks. Returns File. */
	private final File mpTexturePackFolder;

	/** The list of the available texture packs. */
	private List availableTexturePacks = new ArrayList();

	/**
	 * A mapping of texture IDs to TexturePackBase objects used by
	 * updateAvaliableTexturePacks() to avoid reloading texture packs that
	 * haven't changed on disk.
	 */
	private final Map texturePackCache = new HashMap();

	/** The TexturePack that will be used. */
	private ITexturePack selectedTexturePack;

	/** True if a texture pack is downloading in the background. */
	private boolean isDownloading;

	public TexturePackList(final File par1File, final Minecraft par2Minecraft) {
		mc = par2Minecraft;
		texturePackDir = new File(par1File, "texturepacks");
		mpTexturePackFolder = new File(par1File, "texturepacks-mp-cache");
		createTexturePackDirs();
		updateAvaliableTexturePacks();
	}

	/**
	 * Create the "texturepacks" and "texturepacks-mp-cache" directories if they
	 * don't already exist.
	 */
	private void createTexturePackDirs() {
		if (!texturePackDir.isDirectory()) {
			texturePackDir.delete();
			texturePackDir.mkdirs();
		}

		if (!mpTexturePackFolder.isDirectory()) {
			mpTexturePackFolder.delete();
			mpTexturePackFolder.mkdirs();
		}
	}

	/**
	 * Sets the new TexturePack to be used, returning true if it has actually
	 * changed, false if nothing changed.
	 */
	public boolean setTexturePack(final ITexturePack par1ITexturePack) {
		if (par1ITexturePack == selectedTexturePack) {
			return false;
		} else {
			isDownloading = false;
			selectedTexturePack = par1ITexturePack;
			mc.gameSettings.skin = par1ITexturePack.getTexturePackFileName();
			mc.gameSettings.saveOptions();
			return true;
		}
	}

	/**
	 * filename must end in .zip
	 */
	public void requestDownloadOfTexture(final String par1Str) {
		String var2 = par1Str.substring(par1Str.lastIndexOf("/") + 1);

		if (var2.contains("?")) {
			var2 = var2.substring(0, var2.indexOf("?"));
		}

		if (var2.endsWith(".zip")) {
			final File var3 = new File(mpTexturePackFolder, var2);
			downloadTexture(par1Str, var3);
		}
	}

	private void downloadTexture(final String par1Str, final File par2File) {
		final HashMap var3 = new HashMap();
		final GuiProgress var4 = new GuiProgress();
		var3.put("X-Minecraft-Username", mc.session.username);
		var3.put("X-Minecraft-Version", "1.5.2");
		var3.put("X-Minecraft-Supported-Resolutions", "16");
		isDownloading = true;
		mc.displayGuiScreen(var4);
		HttpUtil.downloadTexturePack(par2File, par1Str,
				new TexturePackDownloadSuccess(this), var3, 10000000, var4);
	}

	/**
	 * Return true if a texture pack is downloading in the background.
	 */
	public boolean getIsDownloading() {
		return isDownloading;
	}

	/**
	 * Called from Minecraft.loadWorld() if getIsDownloading() returned true to
	 * prepare the downloaded texture for usage.
	 */
	public void onDownloadFinished() {
		isDownloading = false;
		updateAvaliableTexturePacks();
		mc.scheduleTexturePackRefresh();
	}

	/**
	 * check the texture packs the client has installed
	 */
	public void updateAvaliableTexturePacks() {
		final ArrayList var1 = new ArrayList();
		selectedTexturePack = TexturePackList.defaultTexturePack;
		var1.add(TexturePackList.defaultTexturePack);
		Iterator var2 = getTexturePackDirContents().iterator();

		while (var2.hasNext()) {
			final File var3 = (File) var2.next();
			final String var4 = this.generateTexturePackID(var3);

			if (var4 != null) {
				Object var5 = texturePackCache.get(var4);

				if (var5 == null) {
					var5 = var3.isDirectory() ? new TexturePackFolder(var4,
							var3, TexturePackList.defaultTexturePack)
							: new TexturePackCustom(var4, var3,
									TexturePackList.defaultTexturePack);
					texturePackCache.put(var4, var5);
				}

				if (((ITexturePack) var5).getTexturePackFileName().equals(
						mc.gameSettings.skin)) {
					selectedTexturePack = (ITexturePack) var5;
				}

				var1.add(var5);
			}
		}

		availableTexturePacks.removeAll(var1);
		var2 = availableTexturePacks.iterator();

		while (var2.hasNext()) {
			final ITexturePack var6 = (ITexturePack) var2.next();
			var6.deleteTexturePack(mc.renderEngine);
			texturePackCache.remove(var6.getTexturePackID());
		}

		availableTexturePacks = var1;
	}

	/**
	 * Generate an internal texture pack ID from the file/directory name, last
	 * modification time, and file size. Returns null if the file/directory is
	 * not a texture pack.
	 */
	private String generateTexturePackID(final File par1File) {
		return par1File.isFile()
				&& par1File.getName().toLowerCase().endsWith(".zip") ? par1File
				.getName()
				+ ":"
				+ par1File.length()
				+ ":"
				+ par1File.lastModified() : par1File.isDirectory()
				&& new File(par1File, "pack.txt").exists() ? par1File.getName()
				+ ":folder:" + par1File.lastModified() : null;
	}

	/**
	 * Return a List<File> of file/directories in the texture pack directory.
	 */
	private List getTexturePackDirContents() {
		return texturePackDir.exists() && texturePackDir.isDirectory() ? Arrays
				.asList(texturePackDir.listFiles()) : Collections.emptyList();
	}

	/**
	 * Returns a list of the available texture packs.
	 */
	public List availableTexturePacks() {
		return Collections.unmodifiableList(availableTexturePacks);
	}

	public ITexturePack getSelectedTexturePack() {
		return selectedTexturePack;
	}

	public boolean func_77300_f() {
		if (!mc.gameSettings.serverTextures) {
			return false;
		} else {
			final ServerData var1 = mc.getServerData();
			return var1 == null ? true : var1.func_78840_c();
		}
	}

	public boolean getAcceptsTextures() {
		if (!mc.gameSettings.serverTextures) {
			return false;
		} else {
			final ServerData var1 = mc.getServerData();
			return var1 == null ? false : var1.getAcceptsTextures();
		}
	}

	static boolean isDownloading(final TexturePackList par0TexturePackList) {
		return par0TexturePackList.isDownloading;
	}

	/**
	 * Set the selectedTexturePack field (Inner class static accessor method).
	 */
	static ITexturePack setSelectedTexturePack(
			final TexturePackList par0TexturePackList,
			final ITexturePack par1ITexturePack) {
		return par0TexturePackList.selectedTexturePack = par1ITexturePack;
	}

	/**
	 * Generate an internal texture pack ID from the file/directory name, last
	 * modification time, and file size. Returns null if the file/directory is
	 * not a texture pack. (Inner class static accessor method).
	 */
	static String generateTexturePackID(
			final TexturePackList par0TexturePackList, final File par1File) {
		return par0TexturePackList.generateTexturePackID(par1File);
	}

	static ITexturePack func_98143_h() {
		return TexturePackList.defaultTexturePack;
	}

	static Minecraft getMinecraft(final TexturePackList par0TexturePackList) {
		return par0TexturePackList.mc;
	}
}
