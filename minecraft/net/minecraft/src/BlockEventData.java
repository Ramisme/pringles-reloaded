package net.minecraft.src;

public class BlockEventData {
	private final int coordX;
	private final int coordY;
	private final int coordZ;
	private final int blockID;

	/** Different for each blockID */
	private final int eventID;

	/** Different for each blockID, eventID */
	private final int eventParameter;

	public BlockEventData(final int par1, final int par2, final int par3,
			final int par4, final int par5, final int par6) {
		coordX = par1;
		coordY = par2;
		coordZ = par3;
		eventID = par5;
		eventParameter = par6;
		blockID = par4;
	}

	/**
	 * Get the X coordinate.
	 */
	public int getX() {
		return coordX;
	}

	/**
	 * Get the Y coordinate.
	 */
	public int getY() {
		return coordY;
	}

	/**
	 * Get the Z coordinate.
	 */
	public int getZ() {
		return coordZ;
	}

	/**
	 * Get the Event ID (different for each BlockID)
	 */
	public int getEventID() {
		return eventID;
	}

	/**
	 * Get the Event Parameter (different for each BlockID,EventID)
	 */
	public int getEventParameter() {
		return eventParameter;
	}

	/**
	 * Gets the BlockID for this BlockEventData
	 */
	public int getBlockID() {
		return blockID;
	}

	@Override
	public boolean equals(final Object par1Obj) {
		if (!(par1Obj instanceof BlockEventData)) {
			return false;
		} else {
			final BlockEventData var2 = (BlockEventData) par1Obj;
			return coordX == var2.coordX && coordY == var2.coordY
					&& coordZ == var2.coordZ && eventID == var2.eventID
					&& eventParameter == var2.eventParameter
					&& blockID == var2.blockID;
		}
	}

	@Override
	public String toString() {
		return "TE(" + coordX + "," + coordY + "," + coordZ + ")," + eventID
				+ "," + eventParameter + "," + blockID;
	}
}
