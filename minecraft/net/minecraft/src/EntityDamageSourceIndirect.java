package net.minecraft.src;

public class EntityDamageSourceIndirect extends EntityDamageSource {
	private final Entity indirectEntity;

	public EntityDamageSourceIndirect(final String par1Str,
			final Entity par2Entity, final Entity par3Entity) {
		super(par1Str, par2Entity);
		indirectEntity = par3Entity;
	}

	@Override
	public Entity getSourceOfDamage() {
		return damageSourceEntity;
	}

	@Override
	public Entity getEntity() {
		return indirectEntity;
	}

	/**
	 * Returns the message to be displayed on player death.
	 */
	@Override
	public String getDeathMessage(final EntityLiving par1EntityLiving) {
		final String var2 = indirectEntity == null ? damageSourceEntity
				.getTranslatedEntityName() : indirectEntity
				.getTranslatedEntityName();
		final ItemStack var3 = indirectEntity instanceof EntityLiving ? ((EntityLiving) indirectEntity)
				.getHeldItem() : null;
		final String var4 = "death.attack." + damageType;
		final String var5 = var4 + ".item";
		return var3 != null && var3.hasDisplayName()
				&& StatCollector.func_94522_b(var5) ? StatCollector
				.translateToLocalFormatted(
						var5,
						new Object[] {
								par1EntityLiving.getTranslatedEntityName(),
								var2, var3.getDisplayName() }) : StatCollector
				.translateToLocalFormatted(var4, new Object[] {
						par1EntityLiving.getTranslatedEntityName(), var2 });
	}
}
