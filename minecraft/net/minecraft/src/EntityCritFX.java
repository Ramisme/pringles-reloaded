package net.minecraft.src;

public class EntityCritFX extends EntityFX {
	float field_70561_a;

	public EntityCritFX(final World par1World, final double par2,
			final double par4, final double par6, final double par8,
			final double par10, final double par12) {
		this(par1World, par2, par4, par6, par8, par10, par12, 1.0F);
	}

	public EntityCritFX(final World par1World, final double par2,
			final double par4, final double par6, final double par8,
			final double par10, final double par12, final float par14) {
		super(par1World, par2, par4, par6, 0.0D, 0.0D, 0.0D);
		motionX *= 0.10000000149011612D;
		motionY *= 0.10000000149011612D;
		motionZ *= 0.10000000149011612D;
		motionX += par8 * 0.4D;
		motionY += par10 * 0.4D;
		motionZ += par12 * 0.4D;
		particleRed = particleGreen = particleBlue = (float) (Math.random() * 0.30000001192092896D + 0.6000000238418579D);
		particleScale *= 0.75F;
		particleScale *= par14;
		field_70561_a = particleScale;
		particleMaxAge = (int) (6.0D / (Math.random() * 0.8D + 0.6D));
		particleMaxAge = (int) (particleMaxAge * par14);
		noClip = false;
		setParticleTextureIndex(65);
		onUpdate();
	}

	@Override
	public void renderParticle(final Tessellator par1Tessellator,
			final float par2, final float par3, final float par4,
			final float par5, final float par6, final float par7) {
		float var8 = (particleAge + par2) / particleMaxAge * 32.0F;

		if (var8 < 0.0F) {
			var8 = 0.0F;
		}

		if (var8 > 1.0F) {
			var8 = 1.0F;
		}

		particleScale = field_70561_a * var8;
		super.renderParticle(par1Tessellator, par2, par3, par4, par5, par6,
				par7);
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate() {
		prevPosX = posX;
		prevPosY = posY;
		prevPosZ = posZ;

		if (particleAge++ >= particleMaxAge) {
			setDead();
		}

		moveEntity(motionX, motionY, motionZ);
		particleGreen = (float) (particleGreen * 0.96D);
		particleBlue = (float) (particleBlue * 0.9D);
		motionX *= 0.699999988079071D;
		motionY *= 0.699999988079071D;
		motionZ *= 0.699999988079071D;
		motionY -= 0.019999999552965164D;

		if (onGround) {
			motionX *= 0.699999988079071D;
			motionZ *= 0.699999988079071D;
		}
	}
}
