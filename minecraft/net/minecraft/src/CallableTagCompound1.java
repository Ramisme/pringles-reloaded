package net.minecraft.src;

import java.util.concurrent.Callable;

class CallableTagCompound1 implements Callable {
	final String field_82585_a;

	final NBTTagCompound theNBTTagCompound;

	CallableTagCompound1(final NBTTagCompound par1NBTTagCompound,
			final String par2Str) {
		theNBTTagCompound = par1NBTTagCompound;
		field_82585_a = par2Str;
	}

	public String func_82583_a() {
		return NBTBase.NBTTypes[((NBTBase) NBTTagCompound.getTagMap(
				theNBTTagCompound).get(field_82585_a)).getId()];
	}

	@Override
	public Object call() {
		return func_82583_a();
	}
}
