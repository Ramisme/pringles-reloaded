package net.minecraft.src;

import java.awt.BorderLayout;
import java.awt.Canvas;

import net.minecraft.client.Minecraft;
import net.minecraft.client.MinecraftApplet;

import org.lwjgl.LWJGLException;

public class MinecraftAppletImpl extends Minecraft {
	/** Reference to the main frame, in this case, the applet window itself. */
	final MinecraftApplet mainFrame;

	public MinecraftAppletImpl(final MinecraftApplet par1MinecraftApplet,
			final Canvas par2Canvas, final MinecraftApplet par3MinecraftApplet,
			final int par4, final int par5, final boolean par6) {
		super(par2Canvas, par3MinecraftApplet, par4, par5, par6);
		mainFrame = par1MinecraftApplet;
	}

	@Override
	public void displayCrashReportInternal(final CrashReport par1CrashReport) {
		mainFrame.removeAll();
		mainFrame.setLayout(new BorderLayout());
		mainFrame.add(new PanelCrashReport(par1CrashReport), "Center");
		mainFrame.validate();
	}

	/**
	 * Starts the game: initializes the canvas, the title, the settings,
	 * etcetera.
	 */
	@Override
	public void startGame() throws LWJGLException {
		mcDataDir = Minecraft.getMinecraftDir();
		gameSettings = new GameSettings(this, mcDataDir);

		if (gameSettings.overrideHeight > 0 && gameSettings.overrideWidth > 0
				&& mainFrame.getParent() != null
				&& mainFrame.getParent().getParent() != null) {
			mainFrame
					.getParent()
					.getParent()
					.setSize(gameSettings.overrideWidth,
							gameSettings.overrideHeight);
		}

		super.startGame();
	}
}
