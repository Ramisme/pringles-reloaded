package net.minecraft.src;

public class SoundUpdaterMinecart implements IUpdatePlayerListBox {
	private final SoundManager theSoundManager;

	/** Minecart which sound is being updated. */
	private final EntityMinecart theMinecart;

	/** The player that is getting the minecart sound updates. */
	private final EntityPlayerSP thePlayer;
	private boolean playerSPRidingMinecart = false;
	private boolean minecartIsDead = false;
	private boolean minecartIsMoving = false;
	private boolean silent = false;
	private float minecartSoundPitch = 0.0F;
	private float minecartMoveSoundVolume = 0.0F;
	private float minecartRideSoundVolume = 0.0F;
	private double minecartSpeed = 0.0D;

	public SoundUpdaterMinecart(final SoundManager par1SoundManager,
			final EntityMinecart par2EntityMinecart,
			final EntityPlayerSP par3EntityPlayerSP) {
		theSoundManager = par1SoundManager;
		theMinecart = par2EntityMinecart;
		thePlayer = par3EntityPlayerSP;
	}

	/**
	 * Updates the JList with a new model.
	 */
	@Override
	public void update() {
		boolean var1 = false;
		final boolean var2 = playerSPRidingMinecart;
		final boolean var3 = minecartIsDead;
		final boolean var4 = minecartIsMoving;
		final float var5 = minecartMoveSoundVolume;
		final float var6 = minecartSoundPitch;
		final float var7 = minecartRideSoundVolume;
		playerSPRidingMinecart = thePlayer != null
				&& theMinecart.riddenByEntity == thePlayer;
		minecartIsDead = theMinecart.isDead;
		minecartSpeed = MathHelper.sqrt_double(theMinecart.motionX
				* theMinecart.motionX + theMinecart.motionZ
				* theMinecart.motionZ);
		minecartIsMoving = minecartSpeed >= 0.01D;

		if (var2 && !playerSPRidingMinecart) {
			theSoundManager.stopEntitySound(thePlayer);
		}

		if (minecartIsDead || !silent && minecartMoveSoundVolume == 0.0F
				&& minecartRideSoundVolume == 0.0F) {
			if (!var3) {
				theSoundManager.stopEntitySound(theMinecart);

				if (var2 || playerSPRidingMinecart) {
					theSoundManager.stopEntitySound(thePlayer);
				}
			}

			silent = true;

			if (minecartIsDead) {
				return;
			}
		}

		if (!theSoundManager.isEntitySoundPlaying(theMinecart)
				&& minecartMoveSoundVolume > 0.0F) {
			theSoundManager.playEntitySound("minecart.base", theMinecart,
					minecartMoveSoundVolume, minecartSoundPitch, false);
			silent = false;
			var1 = true;
		}

		if (playerSPRidingMinecart
				&& !theSoundManager.isEntitySoundPlaying(thePlayer)
				&& minecartRideSoundVolume > 0.0F) {
			theSoundManager.playEntitySound("minecart.inside", thePlayer,
					minecartRideSoundVolume, 1.0F, true);
			silent = false;
			var1 = true;
		}

		if (minecartIsMoving) {
			if (minecartSoundPitch < 1.0F) {
				minecartSoundPitch += 0.0025F;
			}

			if (minecartSoundPitch > 1.0F) {
				minecartSoundPitch = 1.0F;
			}

			float var10 = MathHelper.clamp_float((float) minecartSpeed, 0.0F,
					4.0F) / 4.0F;
			minecartRideSoundVolume = 0.0F + var10 * 0.75F;
			var10 = MathHelper.clamp_float(var10 * 2.0F, 0.0F, 1.0F);
			minecartMoveSoundVolume = 0.0F + var10 * 0.7F;
		} else if (var4) {
			minecartMoveSoundVolume = 0.0F;
			minecartSoundPitch = 0.0F;
			minecartRideSoundVolume = 0.0F;
		}

		if (!silent) {
			if (minecartSoundPitch != var6) {
				theSoundManager.setEntitySoundPitch(theMinecart,
						minecartSoundPitch);
			}

			if (minecartMoveSoundVolume != var5) {
				theSoundManager.setEntitySoundVolume(theMinecart,
						minecartMoveSoundVolume);
			}

			if (minecartRideSoundVolume != var7) {
				theSoundManager.setEntitySoundVolume(thePlayer,
						minecartRideSoundVolume);
			}
		}

		if (!var1
				&& (minecartMoveSoundVolume > 0.0F || minecartRideSoundVolume > 0.0F)) {
			theSoundManager.updateSoundLocation(theMinecart);

			if (playerSPRidingMinecart) {
				theSoundManager.updateSoundLocation(thePlayer, theMinecart);
			}
		} else {
			if (theSoundManager.isEntitySoundPlaying(theMinecart)) {
				theSoundManager.stopEntitySound(theMinecart);
			}

			if (playerSPRidingMinecart
					&& theSoundManager.isEntitySoundPlaying(thePlayer)) {
				theSoundManager.stopEntitySound(thePlayer);
			}
		}
	}
}
