package net.minecraft.src;

public class ItemAxe extends ItemTool {
	/** an array of the blocks this axe is effective against */
	private static Block[] blocksEffectiveAgainst = new Block[] { Block.planks,
			Block.bookShelf, Block.wood, Block.chest, Block.stoneDoubleSlab,
			Block.stoneSingleSlab, Block.pumpkin, Block.pumpkinLantern };

	protected ItemAxe(final int par1,
			final EnumToolMaterial par2EnumToolMaterial) {
		super(par1, 3, par2EnumToolMaterial, ItemAxe.blocksEffectiveAgainst);
	}

	/**
	 * Returns the strength of the stack against a given block. 1.0F base,
	 * (Quality+1)*2 if correct blocktype, 1.5F if sword
	 */
	@Override
	public float getStrVsBlock(final ItemStack par1ItemStack,
			final Block par2Block) {
		return par2Block != null
				&& (par2Block.blockMaterial == Material.wood
						|| par2Block.blockMaterial == Material.plants || par2Block.blockMaterial == Material.vine) ? efficiencyOnProperMaterial
				: super.getStrVsBlock(par1ItemStack, par2Block);
	}
}
