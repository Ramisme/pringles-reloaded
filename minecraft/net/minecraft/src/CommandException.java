package net.minecraft.src;

public class CommandException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8364738816485839547L;
	private final Object[] errorObjects;

	public CommandException(final String par1Str,
			final Object... par2ArrayOfObj) {
		super(par1Str);
		errorObjects = par2ArrayOfObj;
	}

	public Object[] getErrorOjbects() {
		return errorObjects;
	}
}
