package net.minecraft.src;

import java.util.List;

public class EntityPigZombie extends EntityZombie {
	/** Above zero if this PigZombie is Angry. */
	private int angerLevel = 0;

	/** A random delay until this PigZombie next makes a sound. */
	private int randomSoundDelay = 0;

	public EntityPigZombie(final World par1World) {
		super(par1World);
		texture = "/mob/pigzombie.png";
		moveSpeed = 0.5F;
		isImmuneToFire = true;
	}

	/**
	 * Returns true if the newer Entity AI code should be run
	 */
	@Override
	protected boolean isAIEnabled() {
		return false;
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate() {
		moveSpeed = entityToAttack != null ? 0.95F : 0.5F;

		if (randomSoundDelay > 0 && --randomSoundDelay == 0) {
			playSound(
					"mob.zombiepig.zpigangry",
					getSoundVolume() * 2.0F,
					((rand.nextFloat() - rand.nextFloat()) * 0.2F + 1.0F) * 1.8F);
		}

		super.onUpdate();
	}

	/**
	 * Returns the texture's file path as a String.
	 */
	@Override
	public String getTexture() {
		return "/mob/pigzombie.png";
	}

	/**
	 * Checks if the entity's current position is a valid location to spawn this
	 * entity.
	 */
	@Override
	public boolean getCanSpawnHere() {
		return worldObj.difficultySetting > 0
				&& worldObj.checkNoEntityCollision(boundingBox)
				&& worldObj.getCollidingBoundingBoxes(this, boundingBox)
						.isEmpty() && !worldObj.isAnyLiquid(boundingBox);
	}

	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	@Override
	public void writeEntityToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeEntityToNBT(par1NBTTagCompound);
		par1NBTTagCompound.setShort("Anger", (short) angerLevel);
	}

	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	@Override
	public void readEntityFromNBT(final NBTTagCompound par1NBTTagCompound) {
		super.readEntityFromNBT(par1NBTTagCompound);
		angerLevel = par1NBTTagCompound.getShort("Anger");
	}

	/**
	 * Finds the closest player within 16 blocks to attack, or null if this
	 * Entity isn't interested in attacking (Animals, Spiders at day, peaceful
	 * PigZombies).
	 */
	@Override
	protected Entity findPlayerToAttack() {
		return angerLevel == 0 ? null : super.findPlayerToAttack();
	}

	/**
	 * Called when the entity is attacked.
	 */
	@Override
	public boolean attackEntityFrom(final DamageSource par1DamageSource,
			final int par2) {
		if (isEntityInvulnerable()) {
			return false;
		} else {
			final Entity var3 = par1DamageSource.getEntity();

			if (var3 instanceof EntityPlayer) {
				final List var4 = worldObj
						.getEntitiesWithinAABBExcludingEntity(this,
								boundingBox.expand(32.0D, 32.0D, 32.0D));

				for (int var5 = 0; var5 < var4.size(); ++var5) {
					final Entity var6 = (Entity) var4.get(var5);

					if (var6 instanceof EntityPigZombie) {
						final EntityPigZombie var7 = (EntityPigZombie) var6;
						var7.becomeAngryAt(var3);
					}
				}

				becomeAngryAt(var3);
			}

			return super.attackEntityFrom(par1DamageSource, par2);
		}
	}

	/**
	 * Causes this PigZombie to become angry at the supplied Entity (which will
	 * be a player).
	 */
	private void becomeAngryAt(final Entity par1Entity) {
		entityToAttack = par1Entity;
		angerLevel = 400 + rand.nextInt(400);
		randomSoundDelay = rand.nextInt(40);
	}

	/**
	 * Returns the sound this mob makes while it's alive.
	 */
	@Override
	protected String getLivingSound() {
		return "mob.zombiepig.zpig";
	}

	/**
	 * Returns the sound this mob makes when it is hurt.
	 */
	@Override
	protected String getHurtSound() {
		return "mob.zombiepig.zpighurt";
	}

	/**
	 * Returns the sound this mob makes on death.
	 */
	@Override
	protected String getDeathSound() {
		return "mob.zombiepig.zpigdeath";
	}

	/**
	 * Drop 0-2 items of this living's type. @param par1 - Whether this entity
	 * has recently been hit by a player. @param par2 - Level of Looting used to
	 * kill this mob.
	 */
	@Override
	protected void dropFewItems(final boolean par1, final int par2) {
		int var3 = rand.nextInt(2 + par2);
		int var4;

		for (var4 = 0; var4 < var3; ++var4) {
			dropItem(Item.rottenFlesh.itemID, 1);
		}

		var3 = rand.nextInt(2 + par2);

		for (var4 = 0; var4 < var3; ++var4) {
			dropItem(Item.goldNugget.itemID, 1);
		}
	}

	/**
	 * Called when a player interacts with a mob. e.g. gets milk from a cow,
	 * gets into the saddle on a pig.
	 */
	@Override
	public boolean interact(final EntityPlayer par1EntityPlayer) {
		return false;
	}

	@Override
	protected void dropRareDrop(final int par1) {
		dropItem(Item.ingotGold.itemID, 1);
	}

	/**
	 * Returns the item ID for the item the mob drops on death.
	 */
	@Override
	protected int getDropItemId() {
		return Item.rottenFlesh.itemID;
	}

	/**
	 * Makes entity wear random armor based on difficulty
	 */
	@Override
	protected void addRandomArmor() {
		setCurrentItemOrArmor(0, new ItemStack(Item.swordGold));
	}

	/**
	 * Initialize this creature.
	 */
	@Override
	public void initCreature() {
		super.initCreature();
		setVillager(false);
	}

	/**
	 * Returns the amount of damage a mob should deal.
	 */
	@Override
	public int getAttackStrength(final Entity par1Entity) {
		final ItemStack var2 = getHeldItem();
		int var3 = 5;

		if (var2 != null) {
			var3 += var2.getDamageVsEntity(this);
		}

		return var3;
	}
}
