package net.minecraft.src;

public class EntityMinecartEmpty extends EntityMinecart {
	public EntityMinecartEmpty(final World par1World) {
		super(par1World);
	}

	public EntityMinecartEmpty(final World par1, final double par2,
			final double par4, final double par6) {
		super(par1, par2, par4, par6);
	}

	/**
	 * Called when a player interacts with a mob. e.g. gets milk from a cow,
	 * gets into the saddle on a pig.
	 */
	@Override
	public boolean interact(final EntityPlayer par1EntityPlayer) {
		if (riddenByEntity != null && riddenByEntity instanceof EntityPlayer
				&& riddenByEntity != par1EntityPlayer) {
			return true;
		} else if (riddenByEntity != null && riddenByEntity != par1EntityPlayer) {
			return false;
		} else {
			if (!worldObj.isRemote) {
				par1EntityPlayer.mountEntity(this);
			}

			return true;
		}
	}

	@Override
	public int getMinecartType() {
		return 0;
	}
}
