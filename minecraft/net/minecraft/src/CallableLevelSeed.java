package net.minecraft.src;

import java.util.concurrent.Callable;

class CallableLevelSeed implements Callable {
	final WorldInfo worldInfoInstance;

	CallableLevelSeed(final WorldInfo par1WorldInfo) {
		worldInfoInstance = par1WorldInfo;
	}

	public String callLevelSeed() {
		return String.valueOf(worldInfoInstance.getSeed());
	}

	@Override
	public Object call() {
		return callLevelSeed();
	}
}
