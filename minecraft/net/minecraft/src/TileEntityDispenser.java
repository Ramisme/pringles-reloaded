package net.minecraft.src;

import java.util.Random;

public class TileEntityDispenser extends TileEntity implements IInventory {
	private ItemStack[] dispenserContents = new ItemStack[9];

	/**
	 * random number generator for instance. Used in random item stack
	 * selection.
	 */
	private final Random dispenserRandom = new Random();
	protected String customName;

	/**
	 * Returns the number of slots in the inventory.
	 */
	@Override
	public int getSizeInventory() {
		return 9;
	}

	/**
	 * Returns the stack in slot i
	 */
	@Override
	public ItemStack getStackInSlot(final int par1) {
		return dispenserContents[par1];
	}

	/**
	 * Removes from an inventory slot (first arg) up to a specified number
	 * (second arg) of items and returns them in a new stack.
	 */
	@Override
	public ItemStack decrStackSize(final int par1, final int par2) {
		if (dispenserContents[par1] != null) {
			ItemStack var3;

			if (dispenserContents[par1].stackSize <= par2) {
				var3 = dispenserContents[par1];
				dispenserContents[par1] = null;
				onInventoryChanged();
				return var3;
			} else {
				var3 = dispenserContents[par1].splitStack(par2);

				if (dispenserContents[par1].stackSize == 0) {
					dispenserContents[par1] = null;
				}

				onInventoryChanged();
				return var3;
			}
		} else {
			return null;
		}
	}

	/**
	 * When some containers are closed they call this on each slot, then drop
	 * whatever it returns as an EntityItem - like when you close a workbench
	 * GUI.
	 */
	@Override
	public ItemStack getStackInSlotOnClosing(final int par1) {
		if (dispenserContents[par1] != null) {
			final ItemStack var2 = dispenserContents[par1];
			dispenserContents[par1] = null;
			return var2;
		} else {
			return null;
		}
	}

	public int getRandomStackFromInventory() {
		int var1 = -1;
		int var2 = 1;

		for (int var3 = 0; var3 < dispenserContents.length; ++var3) {
			if (dispenserContents[var3] != null
					&& dispenserRandom.nextInt(var2++) == 0) {
				var1 = var3;
			}
		}

		return var1;
	}

	/**
	 * Sets the given item stack to the specified slot in the inventory (can be
	 * crafting or armor sections).
	 */
	@Override
	public void setInventorySlotContents(final int par1,
			final ItemStack par2ItemStack) {
		dispenserContents[par1] = par2ItemStack;

		if (par2ItemStack != null
				&& par2ItemStack.stackSize > getInventoryStackLimit()) {
			par2ItemStack.stackSize = getInventoryStackLimit();
		}

		onInventoryChanged();
	}

	/**
	 * Add item stack in first available inventory slot
	 */
	public int addItem(final ItemStack par1ItemStack) {
		for (int var2 = 0; var2 < dispenserContents.length; ++var2) {
			if (dispenserContents[var2] == null
					|| dispenserContents[var2].itemID == 0) {
				setInventorySlotContents(var2, par1ItemStack);
				return var2;
			}
		}

		return -1;
	}

	/**
	 * Returns the name of the inventory.
	 */
	@Override
	public String getInvName() {
		return isInvNameLocalized() ? customName : "container.dispenser";
	}

	public void setCustomName(final String par1Str) {
		customName = par1Str;
	}

	/**
	 * If this returns false, the inventory name will be used as an unlocalized
	 * name, and translated into the player's language. Otherwise it will be
	 * used directly.
	 */
	@Override
	public boolean isInvNameLocalized() {
		return customName != null;
	}

	/**
	 * Reads a tile entity from NBT.
	 */
	@Override
	public void readFromNBT(final NBTTagCompound par1NBTTagCompound) {
		super.readFromNBT(par1NBTTagCompound);
		final NBTTagList var2 = par1NBTTagCompound.getTagList("Items");
		dispenserContents = new ItemStack[getSizeInventory()];

		for (int var3 = 0; var3 < var2.tagCount(); ++var3) {
			final NBTTagCompound var4 = (NBTTagCompound) var2.tagAt(var3);
			final int var5 = var4.getByte("Slot") & 255;

			if (var5 >= 0 && var5 < dispenserContents.length) {
				dispenserContents[var5] = ItemStack.loadItemStackFromNBT(var4);
			}
		}

		if (par1NBTTagCompound.hasKey("CustomName")) {
			customName = par1NBTTagCompound.getString("CustomName");
		}
	}

	/**
	 * Writes a tile entity to NBT.
	 */
	@Override
	public void writeToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeToNBT(par1NBTTagCompound);
		final NBTTagList var2 = new NBTTagList();

		for (int var3 = 0; var3 < dispenserContents.length; ++var3) {
			if (dispenserContents[var3] != null) {
				final NBTTagCompound var4 = new NBTTagCompound();
				var4.setByte("Slot", (byte) var3);
				dispenserContents[var3].writeToNBT(var4);
				var2.appendTag(var4);
			}
		}

		par1NBTTagCompound.setTag("Items", var2);

		if (isInvNameLocalized()) {
			par1NBTTagCompound.setString("CustomName", customName);
		}
	}

	/**
	 * Returns the maximum stack size for a inventory slot. Seems to always be
	 * 64, possibly will be extended. *Isn't this more of a set than a get?*
	 */
	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	/**
	 * Do not make give this method the name canInteractWith because it clashes
	 * with Container
	 */
	@Override
	public boolean isUseableByPlayer(final EntityPlayer par1EntityPlayer) {
		return worldObj.getBlockTileEntity(xCoord, yCoord, zCoord) != this ? false
				: par1EntityPlayer.getDistanceSq(xCoord + 0.5D, yCoord + 0.5D,
						zCoord + 0.5D) <= 64.0D;
	}

	@Override
	public void openChest() {
	}

	@Override
	public void closeChest() {
	}

	/**
	 * Returns true if automation is allowed to insert the given stack (ignoring
	 * stack size) into the given slot.
	 */
	@Override
	public boolean isStackValidForSlot(final int par1,
			final ItemStack par2ItemStack) {
		return true;
	}
}
