package net.minecraft.src;

import java.util.List;

public class BlockSandStone extends Block {
	public static final String[] SAND_STONE_TYPES = new String[] { "default",
			"chiseled", "smooth" };
	private static final String[] field_94405_b = new String[] {
			"sandstone_side", "sandstone_carved", "sandstone_smooth" };
	private Icon[] field_94406_c;
	private Icon field_94403_cO;
	private Icon field_94404_cP;

	public BlockSandStone(final int par1) {
		super(par1, Material.rock);
		setCreativeTab(CreativeTabs.tabBlock);
	}

	/**
	 * From the specified side and block metadata retrieves the blocks texture.
	 * Args: side, metadata
	 */
	@Override
	public Icon getIcon(final int par1, int par2) {
		if (par1 != 1 && (par1 != 0 || par2 != 1 && par2 != 2)) {
			if (par1 == 0) {
				return field_94404_cP;
			} else {
				if (par2 < 0 || par2 >= field_94406_c.length) {
					par2 = 0;
				}

				return field_94406_c[par2];
			}
		} else {
			return field_94403_cO;
		}
	}

	/**
	 * Determines the damage on the item the block drops. Used in cloth and
	 * wood.
	 */
	@Override
	public int damageDropped(final int par1) {
		return par1;
	}

	/**
	 * returns a list of blocks with the same ID, but different meta (eg: wood
	 * returns 4 blocks)
	 */
	@Override
	public void getSubBlocks(final int par1,
			final CreativeTabs par2CreativeTabs, final List par3List) {
		par3List.add(new ItemStack(par1, 1, 0));
		par3List.add(new ItemStack(par1, 1, 1));
		par3List.add(new ItemStack(par1, 1, 2));
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		field_94406_c = new Icon[BlockSandStone.field_94405_b.length];

		for (int var2 = 0; var2 < field_94406_c.length; ++var2) {
			field_94406_c[var2] = par1IconRegister
					.registerIcon(BlockSandStone.field_94405_b[var2]);
		}

		field_94403_cO = par1IconRegister.registerIcon("sandstone_top");
		field_94404_cP = par1IconRegister.registerIcon("sandstone_bottom");
	}
}
