package net.minecraft.src;

import java.util.concurrent.Callable;

class CallableStructureType implements Callable {
	final MapGenStructure theMapStructureGenerator;

	CallableStructureType(final MapGenStructure par1MapGenStructure) {
		theMapStructureGenerator = par1MapGenStructure;
	}

	public String callStructureType() {
		return theMapStructureGenerator.getClass().getCanonicalName();
	}

	@Override
	public Object call() {
		return callStructureType();
	}
}
