package net.minecraft.src;

public class ItemFood extends Item {
	/** Number of ticks to run while 'EnumAction'ing until result. */
	public final int itemUseDuration;

	/** The amount this food item heals the player. */
	private final int healAmount;
	private final float saturationModifier;

	/** Whether wolves like this food (true for raw and cooked porkchop). */
	private final boolean isWolfsFavoriteMeat;

	/**
	 * If this field is true, the food can be consumed even if the player don't
	 * need to eat.
	 */
	private boolean alwaysEdible;

	/**
	 * represents the potion effect that will occurr upon eating this food. Set
	 * by setPotionEffect
	 */
	private int potionId;

	/** set by setPotionEffect */
	private int potionDuration;

	/** set by setPotionEffect */
	private int potionAmplifier;

	/** probably of the set potion effect occurring */
	private float potionEffectProbability;

	public ItemFood(final int par1, final int par2, final float par3,
			final boolean par4) {
		super(par1);
		itemUseDuration = 32;
		healAmount = par2;
		isWolfsFavoriteMeat = par4;
		saturationModifier = par3;
		setCreativeTab(CreativeTabs.tabFood);
	}

	public ItemFood(final int par1, final int par2, final boolean par3) {
		this(par1, par2, 0.6F, par3);
	}

	@Override
	public ItemStack onEaten(final ItemStack par1ItemStack,
			final World par2World, final EntityPlayer par3EntityPlayer) {
		--par1ItemStack.stackSize;
		par3EntityPlayer.getFoodStats().addStats(this);
		par2World.playSoundAtEntity(par3EntityPlayer, "random.burp", 0.5F,
				par2World.rand.nextFloat() * 0.1F + 0.9F);
		onFoodEaten(par1ItemStack, par2World, par3EntityPlayer);
		return par1ItemStack;
	}

	protected void onFoodEaten(final ItemStack par1ItemStack,
			final World par2World, final EntityPlayer par3EntityPlayer) {
		if (!par2World.isRemote && potionId > 0
				&& par2World.rand.nextFloat() < potionEffectProbability) {
			par3EntityPlayer.addPotionEffect(new PotionEffect(potionId,
					potionDuration * 20, potionAmplifier));
		}
	}

	/**
	 * How long it takes to use or consume an item
	 */
	@Override
	public int getMaxItemUseDuration(final ItemStack par1ItemStack) {
		return 32;
	}

	/**
	 * returns the action that specifies what animation to play when the items
	 * is being used
	 */
	@Override
	public EnumAction getItemUseAction(final ItemStack par1ItemStack) {
		return EnumAction.eat;
	}

	/**
	 * Called whenever this item is equipped and the right mouse button is
	 * pressed. Args: itemStack, world, entityPlayer
	 */
	@Override
	public ItemStack onItemRightClick(final ItemStack par1ItemStack,
			final World par2World, final EntityPlayer par3EntityPlayer) {
		if (par3EntityPlayer.canEat(alwaysEdible)) {
			par3EntityPlayer.setItemInUse(par1ItemStack,
					getMaxItemUseDuration(par1ItemStack));
		}

		return par1ItemStack;
	}

	public int getHealAmount() {
		return healAmount;
	}

	/**
	 * gets the saturationModifier of the ItemFood
	 */
	public float getSaturationModifier() {
		return saturationModifier;
	}

	/**
	 * Whether wolves like this food (true for raw and cooked porkchop).
	 */
	public boolean isWolfsFavoriteMeat() {
		return isWolfsFavoriteMeat;
	}

	/**
	 * sets a potion effect on the item. Args: int potionId, int duration (will
	 * be multiplied by 20), int amplifier, float probability of effect
	 * happening
	 */
	public ItemFood setPotionEffect(final int par1, final int par2,
			final int par3, final float par4) {
		potionId = par1;
		potionDuration = par2;
		potionAmplifier = par3;
		potionEffectProbability = par4;
		return this;
	}

	/**
	 * Set the field 'alwaysEdible' to true, and make the food edible even if
	 * the player don't need to eat.
	 */
	public ItemFood setAlwaysEdible() {
		alwaysEdible = true;
		return this;
	}
}
