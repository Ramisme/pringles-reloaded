package net.minecraft.src;

public class EntityAIMoveTowardsTarget extends EntityAIBase {
	private final EntityCreature theEntity;
	private EntityLiving targetEntity;
	private double movePosX;
	private double movePosY;
	private double movePosZ;
	private final float field_75425_f;
	private final float field_75426_g;

	public EntityAIMoveTowardsTarget(final EntityCreature par1EntityCreature,
			final float par2, final float par3) {
		theEntity = par1EntityCreature;
		field_75425_f = par2;
		field_75426_g = par3;
		setMutexBits(1);
	}

	/**
	 * Returns whether the EntityAIBase should begin execution.
	 */
	@Override
	public boolean shouldExecute() {
		targetEntity = theEntity.getAttackTarget();

		if (targetEntity == null) {
			return false;
		} else if (targetEntity.getDistanceSqToEntity(theEntity) > field_75426_g
				* field_75426_g) {
			return false;
		} else {
			final Vec3 var1 = RandomPositionGenerator
					.findRandomTargetBlockTowards(
							theEntity,
							16,
							7,
							theEntity.worldObj.getWorldVec3Pool()
									.getVecFromPool(targetEntity.posX,
											targetEntity.posY,
											targetEntity.posZ));

			if (var1 == null) {
				return false;
			} else {
				movePosX = var1.xCoord;
				movePosY = var1.yCoord;
				movePosZ = var1.zCoord;
				return true;
			}
		}
	}

	/**
	 * Returns whether an in-progress EntityAIBase should continue executing
	 */
	@Override
	public boolean continueExecuting() {
		return !theEntity.getNavigator().noPath()
				&& targetEntity.isEntityAlive()
				&& targetEntity.getDistanceSqToEntity(theEntity) < field_75426_g
						* field_75426_g;
	}

	/**
	 * Resets the task
	 */
	@Override
	public void resetTask() {
		targetEntity = null;
	}

	/**
	 * Execute a one shot task or start executing a continuous task
	 */
	@Override
	public void startExecuting() {
		theEntity.getNavigator().tryMoveToXYZ(movePosX, movePosY, movePosZ,
				field_75425_f);
	}
}
