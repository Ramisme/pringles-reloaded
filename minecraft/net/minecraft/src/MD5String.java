package net.minecraft.src;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5String {
	/** The salt prepended to the string to be hashed */
	private final String salt;

	public MD5String(final String par1Str) {
		salt = par1Str;
	}

	/**
	 * Gets the MD5 string
	 */
	public String getMD5String(final String par1Str) {
		try {
			final String var2 = salt + par1Str;
			final MessageDigest var3 = MessageDigest.getInstance("MD5");
			var3.update(var2.getBytes(), 0, var2.length());
			return new BigInteger(1, var3.digest()).toString(16);
		} catch (final NoSuchAlgorithmException var4) {
			throw new RuntimeException(var4);
		}
	}
}
