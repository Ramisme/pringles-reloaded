package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet102WindowClick extends Packet {
	/** The id of the window which was clicked. 0 for player inventory. */
	public int window_Id;

	/** The clicked slot (-999 is outside of inventory) */
	public int inventorySlot;

	/** 1 when right-clicking and otherwise 0 */
	public int mouseClick;

	/** A unique number for the action, used for transaction handling */
	public short action;

	/** Item stack for inventory */
	public ItemStack itemStack;
	public int holdingShift;

	public Packet102WindowClick() {
	}

	public Packet102WindowClick(final int par1, final int par2, final int par3,
			final int par4, final ItemStack par5ItemStack, final short par6) {
		window_Id = par1;
		inventorySlot = par2;
		mouseClick = par3;
		itemStack = par5ItemStack != null ? par5ItemStack.copy() : null;
		action = par6;
		holdingShift = par4;
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleWindowClick(this);
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		window_Id = par1DataInputStream.readByte();
		inventorySlot = par1DataInputStream.readShort();
		mouseClick = par1DataInputStream.readByte();
		action = par1DataInputStream.readShort();
		holdingShift = par1DataInputStream.readByte();
		itemStack = Packet.readItemStack(par1DataInputStream);
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		par1DataOutputStream.writeByte(window_Id);
		par1DataOutputStream.writeShort(inventorySlot);
		par1DataOutputStream.writeByte(mouseClick);
		par1DataOutputStream.writeShort(action);
		par1DataOutputStream.writeByte(holdingShift);
		Packet.writeItemStack(itemStack, par1DataOutputStream);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 11;
	}
}
