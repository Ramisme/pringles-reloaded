package net.minecraft.src;

public class EntityAIOpenDoor extends EntityAIDoorInteract {
	boolean field_75361_i;
	int field_75360_j;

	public EntityAIOpenDoor(final EntityLiving par1EntityLiving,
			final boolean par2) {
		super(par1EntityLiving);
		theEntity = par1EntityLiving;
		field_75361_i = par2;
	}

	/**
	 * Returns whether an in-progress EntityAIBase should continue executing
	 */
	@Override
	public boolean continueExecuting() {
		return field_75361_i && field_75360_j > 0 && super.continueExecuting();
	}

	/**
	 * Execute a one shot task or start executing a continuous task
	 */
	@Override
	public void startExecuting() {
		field_75360_j = 20;
		targetDoor.onPoweredBlockChange(theEntity.worldObj, entityPosX,
				entityPosY, entityPosZ, true);
	}

	/**
	 * Resets the task
	 */
	@Override
	public void resetTask() {
		if (field_75361_i) {
			targetDoor.onPoweredBlockChange(theEntity.worldObj, entityPosX,
					entityPosY, entityPosZ, false);
		}
	}

	/**
	 * Updates the task
	 */
	@Override
	public void updateTask() {
		--field_75360_j;
		super.updateTask();
	}
}
