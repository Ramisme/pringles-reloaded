package net.minecraft.src;

import java.util.concurrent.Callable;

class CallableEntityType implements Callable {
	final Entity theEntity;

	CallableEntityType(final Entity par1Entity) {
		theEntity = par1Entity;
	}

	public String callEntityType() {
		return EntityList.getEntityString(theEntity) + " ("
				+ theEntity.getClass().getCanonicalName() + ")";
	}

	@Override
	public Object call() {
		return callEntityType();
	}
}
