package net.minecraft.src;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public abstract class EntityPlayer extends EntityLiving implements
		ICommandSender {
	/** Inventory of the player */
	public InventoryPlayer inventory = new InventoryPlayer(this);
	private InventoryEnderChest theInventoryEnderChest = new InventoryEnderChest();

	/**
	 * The Container for the player's inventory (which opens when they press E)
	 */
	public Container inventoryContainer;

	/** The Container the player has open. */
	public Container openContainer;

	/** The player's food stats. (See class FoodStats) */
	protected FoodStats foodStats = new FoodStats();

	/**
	 * Used to tell if the player pressed jump twice. If this is at 0 and it's
	 * pressed (And they are allowed to fly, as defined in the player's
	 * movementInput) it sets this to 7. If it's pressed and it's greater than 0
	 * enable fly.
	 */
	protected int flyToggleTimer = 0;
	public byte field_71098_bD = 0;
	public float prevCameraYaw;
	public float cameraYaw;
	public String username;

	/**
	 * Used by EntityPlayer to prevent too many xp orbs from getting absorbed at
	 * once.
	 */
	public int xpCooldown = 0;
	public double field_71091_bM;
	public double field_71096_bN;
	public double field_71097_bO;
	public double field_71094_bP;
	public double field_71095_bQ;
	public double field_71085_bR;

	/** Boolean value indicating weather a player is sleeping or not */
	protected boolean sleeping;

	/**
	 * The chunk coordinates of the bed the player is in (null if player isn't
	 * in a bed).
	 */
	public ChunkCoordinates playerLocation;
	private int sleepTimer;
	public float field_71079_bU;
	public float field_71082_cx;
	public float field_71089_bV;

	/**
	 * Holds the last coordinate to spawn based on last bed that the player
	 * sleep.
	 */
	private ChunkCoordinates spawnChunk;

	/**
	 * Whether this player's spawn point is forced, preventing execution of bed
	 * checks.
	 */
	private boolean spawnForced;

	/** Holds the coordinate of the player when enter a minecraft to ride. */
	private ChunkCoordinates startMinecartRidingCoordinate;

	/** The player's capabilities. (See class PlayerCapabilities) */
	public PlayerCapabilities capabilities = new PlayerCapabilities();

	/** The current experience level the player is on. */
	public int experienceLevel;

	/**
	 * The total amount of experience the player has. This also includes the
	 * amount of experience within their Experience Bar.
	 */
	public int experienceTotal;

	/**
	 * The current amount of experience the player has within their Experience
	 * Bar.
	 */
	public float experience;

	/**
	 * This is the item that is in use when the player is holding down the
	 * useItemButton (e.g., bow, food, sword)
	 */
	private ItemStack itemInUse;

	/**
	 * This field starts off equal to getMaxItemUseDuration and is decremented
	 * on each tick
	 */
	private int itemInUseCount;
	protected float speedOnGround = 0.1F;
	protected float speedInAir = 0.02F;
	private int field_82249_h = 0;

	/**
	 * An instance of a fishing rod's hook. If this isn't null, the icon image
	 * of the fishing rod is slightly different
	 */
	public EntityFishHook fishEntity = null;

	public EntityPlayer(final World par1World) {
		super(par1World);
		inventoryContainer = new ContainerPlayer(inventory,
				!par1World.isRemote, this);
		openContainer = inventoryContainer;
		yOffset = 1.62F;
		final ChunkCoordinates var2 = par1World.getSpawnPoint();
		setLocationAndAngles(var2.posX + 0.5D, var2.posY + 1, var2.posZ + 0.5D,
				0.0F, 0.0F);
		entityType = "humanoid";
		field_70741_aB = 180.0F;
		fireResistance = 20;
		texture = "/mob/char.png";
	}

	@Override
	public int getMaxHealth() {
		return 20;
	}

	@Override
	protected void entityInit() {
		super.entityInit();
		dataWatcher.addObject(16, Byte.valueOf((byte) 0));
		dataWatcher.addObject(17, Byte.valueOf((byte) 0));
		dataWatcher.addObject(18, Integer.valueOf(0));
	}

	/**
	 * returns the ItemStack containing the itemInUse
	 */
	public ItemStack getItemInUse() {
		return itemInUse;
	}

	/**
	 * Returns the item in use count
	 */
	public int getItemInUseCount() {
		return itemInUseCount;
	}

	/**
	 * Checks if the entity is currently using an item (e.g., bow, food, sword)
	 * by holding down the useItemButton
	 */
	public boolean isUsingItem() {
		return itemInUse != null;
	}

	/**
	 * gets the duration for how long the current itemInUse has been in use
	 */
	public int getItemInUseDuration() {
		return isUsingItem() ? itemInUse.getMaxItemUseDuration()
				- itemInUseCount : 0;
	}

	public void stopUsingItem() {
		if (itemInUse != null) {
			itemInUse.onPlayerStoppedUsing(worldObj, this, itemInUseCount);
		}

		clearItemInUse();
	}

	public void clearItemInUse() {
		itemInUse = null;
		itemInUseCount = 0;

		if (!worldObj.isRemote) {
			setEating(false);
		}
	}

	@Override
	public boolean isBlocking() {
		return isUsingItem()
				&& Item.itemsList[itemInUse.itemID].getItemUseAction(itemInUse) == EnumAction.block;
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate() {
		if (itemInUse != null) {
			final ItemStack var1 = inventory.getCurrentItem();

			if (var1 == itemInUse) {
				if (itemInUseCount <= 25 && itemInUseCount % 4 == 0) {
					updateItemUse(var1, 5);
				}

				if (--itemInUseCount == 0 && !worldObj.isRemote) {
					onItemUseFinish();
				}
			} else {
				clearItemInUse();
			}
		}

		if (xpCooldown > 0) {
			--xpCooldown;
		}

		if (isPlayerSleeping()) {
			++sleepTimer;

			if (sleepTimer > 100) {
				sleepTimer = 100;
			}

			if (!worldObj.isRemote) {
				if (!isInBed()) {
					wakeUpPlayer(true, true, false);
				} else if (worldObj.isDaytime()) {
					wakeUpPlayer(false, true, true);
				}
			}
		} else if (sleepTimer > 0) {
			++sleepTimer;

			if (sleepTimer >= 110) {
				sleepTimer = 0;
			}
		}

		super.onUpdate();

		if (!worldObj.isRemote && openContainer != null
				&& !openContainer.canInteractWith(this)) {
			closeScreen();
			openContainer = inventoryContainer;
		}

		if (isBurning() && capabilities.disableDamage) {
			extinguish();
		}

		field_71091_bM = field_71094_bP;
		field_71096_bN = field_71095_bQ;
		field_71097_bO = field_71085_bR;
		final double var9 = posX - field_71094_bP;
		final double var3 = posY - field_71095_bQ;
		final double var5 = posZ - field_71085_bR;
		final double var7 = 10.0D;

		if (var9 > var7) {
			field_71091_bM = field_71094_bP = posX;
		}

		if (var5 > var7) {
			field_71097_bO = field_71085_bR = posZ;
		}

		if (var3 > var7) {
			field_71096_bN = field_71095_bQ = posY;
		}

		if (var9 < -var7) {
			field_71091_bM = field_71094_bP = posX;
		}

		if (var5 < -var7) {
			field_71097_bO = field_71085_bR = posZ;
		}

		if (var3 < -var7) {
			field_71096_bN = field_71095_bQ = posY;
		}

		field_71094_bP += var9 * 0.25D;
		field_71085_bR += var5 * 0.25D;
		field_71095_bQ += var3 * 0.25D;
		addStat(StatList.minutesPlayedStat, 1);

		if (ridingEntity == null) {
			startMinecartRidingCoordinate = null;
		}

		if (!worldObj.isRemote) {
			foodStats.onUpdate(this);
		}
	}

	/**
	 * Return the amount of time this entity should stay in a portal before
	 * being transported.
	 */
	@Override
	public int getMaxInPortalTime() {
		return capabilities.disableDamage ? 0 : 80;
	}

	/**
	 * Return the amount of cooldown before this entity can use a portal again.
	 */
	@Override
	public int getPortalCooldown() {
		return 10;
	}

	@Override
	public void playSound(final String par1Str, final float par2,
			final float par3) {
		worldObj.playSoundToNearExcept(this, par1Str, par2, par3);
	}

	/**
	 * Plays sounds and makes particles for item in use state
	 */
	protected void updateItemUse(final ItemStack par1ItemStack, final int par2) {
		if (par1ItemStack.getItemUseAction() == EnumAction.drink) {
			playSound("random.drink", 0.5F,
					worldObj.rand.nextFloat() * 0.1F + 0.9F);
		}

		if (par1ItemStack.getItemUseAction() == EnumAction.eat) {
			for (int var3 = 0; var3 < par2; ++var3) {
				final Vec3 var4 = worldObj.getWorldVec3Pool().getVecFromPool(
						(rand.nextFloat() - 0.5D) * 0.1D,
						Math.random() * 0.1D + 0.1D, 0.0D);
				var4.rotateAroundX(-rotationPitch * (float) Math.PI / 180.0F);
				var4.rotateAroundY(-rotationYaw * (float) Math.PI / 180.0F);
				Vec3 var5 = worldObj.getWorldVec3Pool().getVecFromPool(
						(rand.nextFloat() - 0.5D) * 0.3D,
						-rand.nextFloat() * 0.6D - 0.3D, 0.6D);
				var5.rotateAroundX(-rotationPitch * (float) Math.PI / 180.0F);
				var5.rotateAroundY(-rotationYaw * (float) Math.PI / 180.0F);
				var5 = var5.addVector(posX, posY + getEyeHeight(), posZ);
				worldObj.spawnParticle("iconcrack_"
						+ par1ItemStack.getItem().itemID, var5.xCoord,
						var5.yCoord, var5.zCoord, var4.xCoord,
						var4.yCoord + 0.05D, var4.zCoord);
			}

			playSound("random.eat", 0.5F + 0.5F * rand.nextInt(2),
					(rand.nextFloat() - rand.nextFloat()) * 0.2F + 1.0F);
		}
	}

	/**
	 * Used for when item use count runs out, ie: eating completed
	 */
	protected void onItemUseFinish() {
		if (itemInUse != null) {
			updateItemUse(itemInUse, 16);
			final int var1 = itemInUse.stackSize;
			final ItemStack var2 = itemInUse.onFoodEaten(worldObj, this);

			if (var2 != itemInUse || var2 != null && var2.stackSize != var1) {
				inventory.mainInventory[inventory.currentItem] = var2;

				if (var2.stackSize == 0) {
					inventory.mainInventory[inventory.currentItem] = null;
				}
			}

			clearItemInUse();
		}
	}

	@Override
	public void handleHealthUpdate(final byte par1) {
		if (par1 == 9) {
			onItemUseFinish();
		} else {
			super.handleHealthUpdate(par1);
		}
	}

	/**
	 * Dead and sleeping entities cannot move
	 */
	@Override
	protected boolean isMovementBlocked() {
		return getHealth() <= 0 || isPlayerSleeping();
	}

	/**
	 * sets current screen to null (used on escape buttons of GUIs)
	 */
	protected void closeScreen() {
		openContainer = inventoryContainer;
	}

	/**
	 * Called when a player mounts an entity. e.g. mounts a pig, mounts a boat.
	 */
	@Override
	public void mountEntity(final Entity par1Entity) {
		if (ridingEntity == par1Entity) {
			unmountEntity(par1Entity);

			if (ridingEntity != null) {
				ridingEntity.riddenByEntity = null;
			}

			ridingEntity = null;
		} else {
			super.mountEntity(par1Entity);
		}
	}

	/**
	 * Handles updating while being ridden by an entity
	 */
	@Override
	public void updateRidden() {
		final double var1 = posX;
		final double var3 = posY;
		final double var5 = posZ;
		final float var7 = rotationYaw;
		final float var8 = rotationPitch;
		super.updateRidden();
		prevCameraYaw = cameraYaw;
		cameraYaw = 0.0F;
		addMountedMovementStat(posX - var1, posY - var3, posZ - var5);

		if (ridingEntity instanceof EntityPig) {
			rotationPitch = var8;
			rotationYaw = var7;
			renderYawOffset = ((EntityPig) ridingEntity).renderYawOffset;
		}
	}

	/**
	 * Keeps moving the entity up so it isn't colliding with blocks and other
	 * requirements for this entity to be spawned (only actually used on players
	 * though its also on Entity)
	 */
	@Override
	public void preparePlayerToSpawn() {
		yOffset = 1.62F;
		setSize(0.6F, 1.8F);
		super.preparePlayerToSpawn();
		setEntityHealth(getMaxHealth());
		deathTime = 0;
	}

	@Override
	protected void updateEntityActionState() {
		updateArmSwingProgress();
	}

	/**
	 * Called frequently so the entity can update its state every tick as
	 * required. For example, zombies and skeletons use this to react to
	 * sunlight and start to burn.
	 */
	@Override
	public void onLivingUpdate() {
		if (flyToggleTimer > 0) {
			--flyToggleTimer;
		}

		if (worldObj.difficultySetting == 0 && getHealth() < getMaxHealth()
				&& ticksExisted % 20 * 12 == 0) {
			heal(1);
		}

		inventory.decrementAnimations();
		prevCameraYaw = cameraYaw;
		super.onLivingUpdate();
		landMovementFactor = capabilities.getWalkSpeed();
		jumpMovementFactor = speedInAir;

		if (isSprinting()) {
			landMovementFactor = (float) (landMovementFactor + capabilities
					.getWalkSpeed() * 0.3D);
			jumpMovementFactor = (float) (jumpMovementFactor + speedInAir * 0.3D);
		}

		float var1 = MathHelper.sqrt_double(motionX * motionX + motionZ
				* motionZ);
		float var2 = (float) Math.atan(-motionY * 0.20000000298023224D) * 15.0F;

		if (var1 > 0.1F) {
			var1 = 0.1F;
		}

		if (!onGround || getHealth() <= 0) {
			var1 = 0.0F;
		}

		if (onGround || getHealth() <= 0) {
			var2 = 0.0F;
		}

		cameraYaw += (var1 - cameraYaw) * 0.4F;
		cameraPitch += (var2 - cameraPitch) * 0.8F;

		if (getHealth() > 0) {
			final List var3 = worldObj.getEntitiesWithinAABBExcludingEntity(
					this, boundingBox.expand(1.0D, 0.5D, 1.0D));

			if (var3 != null) {
				for (int var4 = 0; var4 < var3.size(); ++var4) {
					final Entity var5 = (Entity) var3.get(var4);

					if (!var5.isDead) {
						collideWithPlayer(var5);
					}
				}
			}
		}
	}

	private void collideWithPlayer(final Entity par1Entity) {
		par1Entity.onCollideWithPlayer(this);
	}

	public int getScore() {
		return dataWatcher.getWatchableObjectInt(18);
	}

	/**
	 * Set player's score
	 */
	public void setScore(final int par1) {
		dataWatcher.updateObject(18, Integer.valueOf(par1));
	}

	/**
	 * Add to player's score
	 */
	public void addScore(final int par1) {
		final int var2 = getScore();
		dataWatcher.updateObject(18, Integer.valueOf(var2 + par1));
	}

	/**
	 * Called when the mob's health reaches 0.
	 */
	@Override
	public void onDeath(final DamageSource par1DamageSource) {
		super.onDeath(par1DamageSource);
		setSize(0.2F, 0.2F);
		setPosition(posX, posY, posZ);
		motionY = 0.10000000149011612D;

		if (username.equals("Notch")) {
			dropPlayerItemWithRandomChoice(new ItemStack(Item.appleRed, 1),
					true);
		}

		if (!worldObj.getGameRules().getGameRuleBooleanValue("keepInventory")) {
			inventory.dropAllItems();
		}

		if (par1DamageSource != null) {
			motionX = -MathHelper.cos((attackedAtYaw + rotationYaw)
					* (float) Math.PI / 180.0F) * 0.1F;
			motionZ = -MathHelper.sin((attackedAtYaw + rotationYaw)
					* (float) Math.PI / 180.0F) * 0.1F;
		} else {
			motionX = motionZ = 0.0D;
		}

		yOffset = 0.1F;
		addStat(StatList.deathsStat, 1);
	}

	/**
	 * Adds a value to the player score. Currently not actually used and the
	 * entity passed in does nothing. Args: entity, scoreToAdd
	 */
	@Override
	public void addToPlayerScore(final Entity par1Entity, final int par2) {
		addScore(par2);
		final Collection var3 = getWorldScoreboard().func_96520_a(
				ScoreObjectiveCriteria.field_96640_e);

		if (par1Entity instanceof EntityPlayer) {
			addStat(StatList.playerKillsStat, 1);
			var3.addAll(getWorldScoreboard().func_96520_a(
					ScoreObjectiveCriteria.field_96639_d));
		} else {
			addStat(StatList.mobKillsStat, 1);
		}

		final Iterator var4 = var3.iterator();

		while (var4.hasNext()) {
			final ScoreObjective var5 = (ScoreObjective) var4.next();
			final Score var6 = getWorldScoreboard().func_96529_a(
					getEntityName(), var5);
			var6.func_96648_a();
		}
	}

	/**
	 * Called when player presses the drop item key
	 */
	public EntityItem dropOneItem(final boolean par1) {
		return dropPlayerItemWithRandomChoice(inventory.decrStackSize(
				inventory.currentItem,
				par1 && inventory.getCurrentItem() != null ? inventory
						.getCurrentItem().stackSize : 1), false);
	}

	/**
	 * Args: itemstack - called when player drops an item stack that's not in
	 * his inventory (like items still placed in a workbench while the
	 * workbench'es GUI gets closed)
	 */
	public EntityItem dropPlayerItem(final ItemStack par1ItemStack) {
		return dropPlayerItemWithRandomChoice(par1ItemStack, false);
	}

	/**
	 * Args: itemstack, flag
	 */
	public EntityItem dropPlayerItemWithRandomChoice(
			final ItemStack par1ItemStack, final boolean par2) {
		if (par1ItemStack == null) {
			return null;
		} else {
			final EntityItem var3 = new EntityItem(worldObj, posX, posY
					- 0.30000001192092896D + getEyeHeight(), posZ,
					par1ItemStack);
			var3.delayBeforeCanPickup = 40;
			float var4 = 0.1F;
			float var5;

			if (par2) {
				var5 = rand.nextFloat() * 0.5F;
				final float var6 = rand.nextFloat() * (float) Math.PI * 2.0F;
				var3.motionX = -MathHelper.sin(var6) * var5;
				var3.motionZ = MathHelper.cos(var6) * var5;
				var3.motionY = 0.20000000298023224D;
			} else {
				var4 = 0.3F;
				var3.motionX = -MathHelper.sin(rotationYaw / 180.0F
						* (float) Math.PI)
						* MathHelper.cos(rotationPitch / 180.0F
								* (float) Math.PI) * var4;
				var3.motionZ = MathHelper.cos(rotationYaw / 180.0F
						* (float) Math.PI)
						* MathHelper.cos(rotationPitch / 180.0F
								* (float) Math.PI) * var4;
				var3.motionY = -MathHelper.sin(rotationPitch / 180.0F
						* (float) Math.PI)
						* var4 + 0.1F;
				var4 = 0.02F;
				var5 = rand.nextFloat() * (float) Math.PI * 2.0F;
				var4 *= rand.nextFloat();
				var3.motionX += Math.cos(var5) * var4;
				var3.motionY += (rand.nextFloat() - rand.nextFloat()) * 0.1F;
				var3.motionZ += Math.sin(var5) * var4;
			}

			joinEntityItemWithWorld(var3);
			addStat(StatList.dropStat, 1);
			return var3;
		}
	}

	/**
	 * Joins the passed in entity item with the world. Args: entityItem
	 */
	protected void joinEntityItemWithWorld(final EntityItem par1EntityItem) {
		worldObj.spawnEntityInWorld(par1EntityItem);
	}

	/**
	 * Returns how strong the player is against the specified block at this
	 * moment
	 */
	public float getCurrentPlayerStrVsBlock(final Block par1Block,
			final boolean par2) {
		float var3 = inventory.getStrVsBlock(par1Block);

		if (var3 > 1.0F) {
			final int var4 = EnchantmentHelper.getEfficiencyModifier(this);
			final ItemStack var5 = inventory.getCurrentItem();

			if (var4 > 0 && var5 != null) {
				final float var6 = var4 * var4 + 1;

				if (!var5.canHarvestBlock(par1Block) && var3 <= 1.0F) {
					var3 += var6 * 0.08F;
				} else {
					var3 += var6;
				}
			}
		}

		if (this.isPotionActive(Potion.digSpeed)) {
			var3 *= 1.0F + (getActivePotionEffect(Potion.digSpeed)
					.getAmplifier() + 1) * 0.2F;
		}

		if (this.isPotionActive(Potion.digSlowdown)) {
			var3 *= 1.0F - (getActivePotionEffect(Potion.digSlowdown)
					.getAmplifier() + 1) * 0.2F;
		}

		if (isInsideOfMaterial(Material.water)
				&& !EnchantmentHelper.getAquaAffinityModifier(this)) {
			var3 /= 5.0F;
		}

		if (!onGround) {
			var3 /= 5.0F;
		}

		return var3;
	}

	/**
	 * Checks if the player has the ability to harvest a block (checks current
	 * inventory item for a tool if necessary)
	 */
	public boolean canHarvestBlock(final Block par1Block) {
		return inventory.canHarvestBlock(par1Block);
	}

	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	@Override
	public void readEntityFromNBT(final NBTTagCompound par1NBTTagCompound) {
		super.readEntityFromNBT(par1NBTTagCompound);
		final NBTTagList var2 = par1NBTTagCompound.getTagList("Inventory");
		inventory.readFromNBT(var2);
		inventory.currentItem = par1NBTTagCompound
				.getInteger("SelectedItemSlot");
		sleeping = par1NBTTagCompound.getBoolean("Sleeping");
		sleepTimer = par1NBTTagCompound.getShort("SleepTimer");
		experience = par1NBTTagCompound.getFloat("XpP");
		experienceLevel = par1NBTTagCompound.getInteger("XpLevel");
		experienceTotal = par1NBTTagCompound.getInteger("XpTotal");
		setScore(par1NBTTagCompound.getInteger("Score"));

		if (sleeping) {
			playerLocation = new ChunkCoordinates(
					MathHelper.floor_double(posX),
					MathHelper.floor_double(posY),
					MathHelper.floor_double(posZ));
			wakeUpPlayer(true, true, false);
		}

		if (par1NBTTagCompound.hasKey("SpawnX")
				&& par1NBTTagCompound.hasKey("SpawnY")
				&& par1NBTTagCompound.hasKey("SpawnZ")) {
			spawnChunk = new ChunkCoordinates(
					par1NBTTagCompound.getInteger("SpawnX"),
					par1NBTTagCompound.getInteger("SpawnY"),
					par1NBTTagCompound.getInteger("SpawnZ"));
			spawnForced = par1NBTTagCompound.getBoolean("SpawnForced");
		}

		foodStats.readNBT(par1NBTTagCompound);
		capabilities.readCapabilitiesFromNBT(par1NBTTagCompound);

		if (par1NBTTagCompound.hasKey("EnderItems")) {
			final NBTTagList var3 = par1NBTTagCompound.getTagList("EnderItems");
			theInventoryEnderChest.loadInventoryFromNBT(var3);
		}
	}

	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	@Override
	public void writeEntityToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeEntityToNBT(par1NBTTagCompound);
		par1NBTTagCompound.setTag("Inventory",
				inventory.writeToNBT(new NBTTagList()));
		par1NBTTagCompound
				.setInteger("SelectedItemSlot", inventory.currentItem);
		par1NBTTagCompound.setBoolean("Sleeping", sleeping);
		par1NBTTagCompound.setShort("SleepTimer", (short) sleepTimer);
		par1NBTTagCompound.setFloat("XpP", experience);
		par1NBTTagCompound.setInteger("XpLevel", experienceLevel);
		par1NBTTagCompound.setInteger("XpTotal", experienceTotal);
		par1NBTTagCompound.setInteger("Score", getScore());

		if (spawnChunk != null) {
			par1NBTTagCompound.setInteger("SpawnX", spawnChunk.posX);
			par1NBTTagCompound.setInteger("SpawnY", spawnChunk.posY);
			par1NBTTagCompound.setInteger("SpawnZ", spawnChunk.posZ);
			par1NBTTagCompound.setBoolean("SpawnForced", spawnForced);
		}

		foodStats.writeNBT(par1NBTTagCompound);
		capabilities.writeCapabilitiesToNBT(par1NBTTagCompound);
		par1NBTTagCompound.setTag("EnderItems",
				theInventoryEnderChest.saveInventoryToNBT());
	}

	/**
	 * Displays the GUI for interacting with a chest inventory. Args:
	 * chestInventory
	 */
	public void displayGUIChest(final IInventory par1IInventory) {
	}

	public void displayGUIHopper(final TileEntityHopper par1TileEntityHopper) {
	}

	public void displayGUIHopperMinecart(
			final EntityMinecartHopper par1EntityMinecartHopper) {
	}

	public void displayGUIEnchantment(final int par1, final int par2,
			final int par3, final String par4Str) {
	}

	/**
	 * Displays the GUI for interacting with an anvil.
	 */
	public void displayGUIAnvil(final int par1, final int par2, final int par3) {
	}

	/**
	 * Displays the crafting GUI for a workbench.
	 */
	public void displayGUIWorkbench(final int par1, final int par2,
			final int par3) {
	}

	@Override
	public float getEyeHeight() {
		return 0.12F;
	}

	/**
	 * sets the players height back to normal after doing things like sleeping
	 * and dieing
	 */
	protected void resetHeight() {
		yOffset = 1.62F;
	}

	/**
	 * Called when the entity is attacked.
	 */
	@Override
	public boolean attackEntityFrom(final DamageSource par1DamageSource,
			int par2) {
		if (isEntityInvulnerable()) {
			return false;
		} else if (capabilities.disableDamage
				&& !par1DamageSource.canHarmInCreative()) {
			return false;
		} else {
			entityAge = 0;

			if (getHealth() <= 0) {
				return false;
			} else {
				if (isPlayerSleeping() && !worldObj.isRemote) {
					wakeUpPlayer(true, true, false);
				}

				if (par1DamageSource.isDifficultyScaled()) {
					if (worldObj.difficultySetting == 0) {
						par2 = 0;
					}

					if (worldObj.difficultySetting == 1) {
						par2 = par2 / 2 + 1;
					}

					if (worldObj.difficultySetting == 3) {
						par2 = par2 * 3 / 2;
					}
				}

				if (par2 == 0) {
					return false;
				} else {
					Entity var3 = par1DamageSource.getEntity();

					if (var3 instanceof EntityArrow
							&& ((EntityArrow) var3).shootingEntity != null) {
						var3 = ((EntityArrow) var3).shootingEntity;
					}

					if (var3 instanceof EntityLiving) {
						alertWolves((EntityLiving) var3, false);
					}

					addStat(StatList.damageTakenStat, par2);
					return super.attackEntityFrom(par1DamageSource, par2);
				}
			}
		}
	}

	public boolean func_96122_a(final EntityPlayer par1EntityPlayer) {
		final ScorePlayerTeam var2 = getTeam();
		final ScorePlayerTeam var3 = par1EntityPlayer.getTeam();
		return var2 != var3 ? true : var2 != null ? var2.func_96665_g() : true;
	}

	/**
	 * Called when the player attack or gets attacked, it's alert all wolves in
	 * the area that are owned by the player to join the attack or defend the
	 * player.
	 */
	protected void alertWolves(final EntityLiving par1EntityLiving,
			final boolean par2) {
		if (!(par1EntityLiving instanceof EntityCreeper)
				&& !(par1EntityLiving instanceof EntityGhast)) {
			if (par1EntityLiving instanceof EntityWolf) {
				final EntityWolf var3 = (EntityWolf) par1EntityLiving;

				if (var3.isTamed() && username.equals(var3.getOwnerName())) {
					return;
				}
			}

			if (!(par1EntityLiving instanceof EntityPlayer)
					|| func_96122_a((EntityPlayer) par1EntityLiving)) {
				final List var6 = worldObj.getEntitiesWithinAABB(
						EntityWolf.class,
						AxisAlignedBB
								.getAABBPool()
								.getAABB(posX, posY, posZ, posX + 1.0D,
										posY + 1.0D, posZ + 1.0D)
								.expand(16.0D, 4.0D, 16.0D));
				final Iterator var4 = var6.iterator();

				while (var4.hasNext()) {
					final EntityWolf var5 = (EntityWolf) var4.next();

					if (var5.isTamed() && var5.getEntityToAttack() == null
							&& username.equals(var5.getOwnerName())
							&& (!par2 || !var5.isSitting())) {
						var5.setSitting(false);
						var5.setTarget(par1EntityLiving);
					}
				}
			}
		}
	}

	@Override
	protected void damageArmor(final int par1) {
		inventory.damageArmor(par1);
	}

	/**
	 * Returns the current armor value as determined by a call to
	 * InventoryPlayer.getTotalArmorValue
	 */
	@Override
	public int getTotalArmorValue() {
		return inventory.getTotalArmorValue();
	}

	public float func_82243_bO() {
		int var1 = 0;
		final ItemStack[] var2 = inventory.armorInventory;
		final int var3 = var2.length;

		for (int var4 = 0; var4 < var3; ++var4) {
			final ItemStack var5 = var2[var4];

			if (var5 != null) {
				++var1;
			}
		}

		return (float) var1 / (float) inventory.armorInventory.length;
	}

	/**
	 * Deals damage to the entity. If its a EntityPlayer then will take damage
	 * from the armor first and then health second with the reduced value. Args:
	 * damageAmount
	 */
	@Override
	protected void damageEntity(final DamageSource par1DamageSource, int par2) {
		if (!isEntityInvulnerable()) {
			if (!par1DamageSource.isUnblockable() && isBlocking()) {
				par2 = 1 + par2 >> 1;
			}

			par2 = applyArmorCalculations(par1DamageSource, par2);
			par2 = applyPotionDamageCalculations(par1DamageSource, par2);
			addExhaustion(par1DamageSource.getHungerDamage());
			final int var3 = getHealth();
			setEntityHealth(getHealth() - par2);
			field_94063_bt.func_94547_a(par1DamageSource, var3, par2);
		}
	}

	/**
	 * Displays the furnace GUI for the passed in furnace entity. Args:
	 * tileEntityFurnace
	 */
	public void displayGUIFurnace(final TileEntityFurnace par1TileEntityFurnace) {
	}

	/**
	 * Displays the dipsenser GUI for the passed in dispenser entity. Args:
	 * TileEntityDispenser
	 */
	public void displayGUIDispenser(
			final TileEntityDispenser par1TileEntityDispenser) {
	}

	/**
	 * Displays the GUI for editing a sign. Args: tileEntitySign
	 */
	public void displayGUIEditSign(final TileEntity par1TileEntity) {
	}

	/**
	 * Displays the GUI for interacting with a brewing stand.
	 */
	public void displayGUIBrewingStand(
			final TileEntityBrewingStand par1TileEntityBrewingStand) {
	}

	/**
	 * Displays the GUI for interacting with a beacon.
	 */
	public void displayGUIBeacon(final TileEntityBeacon par1TileEntityBeacon) {
	}

	public void displayGUIMerchant(final IMerchant par1IMerchant,
			final String par2Str) {
	}

	/**
	 * Displays the GUI for interacting with a book.
	 */
	public void displayGUIBook(final ItemStack par1ItemStack) {
	}

	public boolean interactWith(final Entity par1Entity) {
		if (par1Entity.interact(this)) {
			return true;
		} else {
			ItemStack var2 = getCurrentEquippedItem();

			if (var2 != null && par1Entity instanceof EntityLiving) {
				if (capabilities.isCreativeMode) {
					var2 = var2.copy();
				}

				if (var2.interactWith((EntityLiving) par1Entity)) {
					if (var2.stackSize <= 0 && !capabilities.isCreativeMode) {
						destroyCurrentEquippedItem();
					}

					return true;
				}
			}

			return false;
		}
	}

	/**
	 * Returns the currently being used item by the player.
	 */
	public ItemStack getCurrentEquippedItem() {
		return inventory.getCurrentItem();
	}

	/**
	 * Destroys the currently equipped item from the player's inventory.
	 */
	public void destroyCurrentEquippedItem() {
		inventory.setInventorySlotContents(inventory.currentItem,
				(ItemStack) null);
	}

	/**
	 * Returns the Y Offset of this entity.
	 */
	@Override
	public double getYOffset() {
		return yOffset - 0.5F;
	}

	/**
	 * Attacks for the player the targeted entity with the currently equipped
	 * item. The equipped item has hitEntity called on it. Args: targetEntity
	 */
	public void attackTargetEntityWithCurrentItem(final Entity par1Entity) {
		if (par1Entity.canAttackWithItem()) {
			if (!par1Entity.func_85031_j(this)) {
				int var2 = inventory.getDamageVsEntity(par1Entity);

				if (this.isPotionActive(Potion.damageBoost)) {
					var2 += 3 << getActivePotionEffect(Potion.damageBoost)
							.getAmplifier();
				}

				if (this.isPotionActive(Potion.weakness)) {
					var2 -= 2 << getActivePotionEffect(Potion.weakness)
							.getAmplifier();
				}

				int var3 = 0;
				int var4 = 0;

				if (par1Entity instanceof EntityLiving) {
					var4 = EnchantmentHelper.getEnchantmentModifierLiving(this,
							(EntityLiving) par1Entity);
					var3 += EnchantmentHelper.getKnockbackModifier(this,
							(EntityLiving) par1Entity);
				}

				if (isSprinting()) {
					++var3;
				}

				if (var2 > 0 || var4 > 0) {
					final boolean var5 = fallDistance > 0.0F && !onGround
							&& !isOnLadder() && !isInWater()
							&& !this.isPotionActive(Potion.blindness)
							&& ridingEntity == null
							&& par1Entity instanceof EntityLiving;

					if (var5 && var2 > 0) {
						var2 += rand.nextInt(var2 / 2 + 2);
					}

					var2 += var4;
					boolean var6 = false;
					final int var7 = EnchantmentHelper
							.getFireAspectModifier(this);

					if (par1Entity instanceof EntityLiving && var7 > 0
							&& !par1Entity.isBurning()) {
						var6 = true;
						par1Entity.setFire(1);
					}

					final boolean var8 = par1Entity.attackEntityFrom(
							DamageSource.causePlayerDamage(this), var2);

					if (var8) {
						if (var3 > 0) {
							par1Entity.addVelocity(
									-MathHelper.sin(rotationYaw
											* (float) Math.PI / 180.0F)
											* var3 * 0.5F,
									0.1D,
									MathHelper.cos(rotationYaw
											* (float) Math.PI / 180.0F)
											* var3 * 0.5F);
							motionX *= 0.6D;
							motionZ *= 0.6D;
							setSprinting(false);
						}

						if (var5) {
							onCriticalHit(par1Entity);
						}

						if (var4 > 0) {
							onEnchantmentCritical(par1Entity);
						}

						if (var2 >= 18) {
							triggerAchievement(AchievementList.overkill);
						}

						setLastAttackingEntity(par1Entity);

						if (par1Entity instanceof EntityLiving) {
							EnchantmentThorns.func_92096_a(this,
									(EntityLiving) par1Entity, rand);
						}
					}

					final ItemStack var9 = getCurrentEquippedItem();
					Object var10 = par1Entity;

					if (par1Entity instanceof EntityDragonPart) {
						final IEntityMultiPart var11 = ((EntityDragonPart) par1Entity).entityDragonObj;

						if (var11 != null && var11 instanceof EntityLiving) {
							var10 = var11;
						}
					}

					if (var9 != null && var10 instanceof EntityLiving) {
						var9.hitEntity((EntityLiving) var10, this);

						if (var9.stackSize <= 0) {
							destroyCurrentEquippedItem();
						}
					}

					if (par1Entity instanceof EntityLiving) {
						if (par1Entity.isEntityAlive()) {
							alertWolves((EntityLiving) par1Entity, true);
						}

						addStat(StatList.damageDealtStat, var2);

						if (var7 > 0 && var8) {
							par1Entity.setFire(var7 * 4);
						} else if (var6) {
							par1Entity.extinguish();
						}
					}

					addExhaustion(0.3F);
				}
			}
		}
	}

	/**
	 * Called when the player performs a critical hit on the Entity. Args:
	 * entity that was hit critically
	 */
	public void onCriticalHit(final Entity par1Entity) {
	}

	public void onEnchantmentCritical(final Entity par1Entity) {
	}

	public void respawnPlayer() {
	}

	/**
	 * Will get destroyed next tick.
	 */
	@Override
	public void setDead() {
		super.setDead();
		inventoryContainer.onCraftGuiClosed(this);

		if (openContainer != null) {
			openContainer.onCraftGuiClosed(this);
		}
	}

	/**
	 * Checks if this entity is inside of an opaque block
	 */
	@Override
	public boolean isEntityInsideOpaqueBlock() {
		return !sleeping && super.isEntityInsideOpaqueBlock();
	}

	public boolean func_71066_bF() {
		return false;
	}

	/**
	 * Attempts to have the player sleep in a bed at the specified location.
	 */
	public EnumStatus sleepInBedAt(final int par1, final int par2,
			final int par3) {
		if (!worldObj.isRemote) {
			if (isPlayerSleeping() || !isEntityAlive()) {
				return EnumStatus.OTHER_PROBLEM;
			}

			if (!worldObj.provider.isSurfaceWorld()) {
				return EnumStatus.NOT_POSSIBLE_HERE;
			}

			if (worldObj.isDaytime()) {
				return EnumStatus.NOT_POSSIBLE_NOW;
			}

			if (Math.abs(posX - par1) > 3.0D || Math.abs(posY - par2) > 2.0D
					|| Math.abs(posZ - par3) > 3.0D) {
				return EnumStatus.TOO_FAR_AWAY;
			}

			final double var4 = 8.0D;
			final double var6 = 5.0D;
			final List var8 = worldObj.getEntitiesWithinAABB(
					EntityMob.class,
					AxisAlignedBB.getAABBPool().getAABB(par1 - var4,
							par2 - var6, par3 - var4, par1 + var4, par2 + var6,
							par3 + var4));

			if (!var8.isEmpty()) {
				return EnumStatus.NOT_SAFE;
			}
		}

		setSize(0.2F, 0.2F);
		yOffset = 0.2F;

		if (worldObj.blockExists(par1, par2, par3)) {
			final int var9 = worldObj.getBlockMetadata(par1, par2, par3);
			final int var5 = BlockDirectional.getDirection(var9);
			float var10 = 0.5F;
			float var7 = 0.5F;

			switch (var5) {
			case 0:
				var7 = 0.9F;
				break;

			case 1:
				var10 = 0.1F;
				break;

			case 2:
				var7 = 0.1F;
				break;

			case 3:
				var10 = 0.9F;
			}

			func_71013_b(var5);
			setPosition(par1 + var10, par2 + 0.9375F, par3 + var7);
		} else {
			setPosition(par1 + 0.5F, par2 + 0.9375F, par3 + 0.5F);
		}

		sleeping = true;
		sleepTimer = 0;
		playerLocation = new ChunkCoordinates(par1, par2, par3);
		motionX = motionZ = motionY = 0.0D;

		if (!worldObj.isRemote) {
			worldObj.updateAllPlayersSleepingFlag();
		}

		return EnumStatus.OK;
	}

	private void func_71013_b(final int par1) {
		field_71079_bU = 0.0F;
		field_71089_bV = 0.0F;

		switch (par1) {
		case 0:
			field_71089_bV = -1.8F;
			break;

		case 1:
			field_71079_bU = 1.8F;
			break;

		case 2:
			field_71089_bV = 1.8F;
			break;

		case 3:
			field_71079_bU = -1.8F;
		}
	}

	/**
	 * Wake up the player if they're sleeping.
	 */
	public void wakeUpPlayer(final boolean par1, final boolean par2,
			final boolean par3) {
		setSize(0.6F, 1.8F);
		resetHeight();
		final ChunkCoordinates var4 = playerLocation;
		ChunkCoordinates var5 = playerLocation;

		if (var4 != null
				&& worldObj.getBlockId(var4.posX, var4.posY, var4.posZ) == Block.bed.blockID) {
			BlockBed.setBedOccupied(worldObj, var4.posX, var4.posY, var4.posZ,
					false);
			var5 = BlockBed.getNearestEmptyChunkCoordinates(worldObj,
					var4.posX, var4.posY, var4.posZ, 0);

			if (var5 == null) {
				var5 = new ChunkCoordinates(var4.posX, var4.posY + 1, var4.posZ);
			}

			setPosition(var5.posX + 0.5F, var5.posY + yOffset + 0.1F,
					var5.posZ + 0.5F);
		}

		sleeping = false;

		if (!worldObj.isRemote && par2) {
			worldObj.updateAllPlayersSleepingFlag();
		}

		if (par1) {
			sleepTimer = 0;
		} else {
			sleepTimer = 100;
		}

		if (par3) {
			setSpawnChunk(playerLocation, false);
		}
	}

	/**
	 * Checks if the player is currently in a bed
	 */
	private boolean isInBed() {
		return worldObj.getBlockId(playerLocation.posX, playerLocation.posY,
				playerLocation.posZ) == Block.bed.blockID;
	}

	/**
	 * Ensure that a block enabling respawning exists at the specified
	 * coordinates and find an empty space nearby to spawn.
	 */
	public static ChunkCoordinates verifyRespawnCoordinates(
			final World par0World, final ChunkCoordinates par1ChunkCoordinates,
			final boolean par2) {
		final IChunkProvider var3 = par0World.getChunkProvider();
		var3.loadChunk(par1ChunkCoordinates.posX - 3 >> 4,
				par1ChunkCoordinates.posZ - 3 >> 4);
		var3.loadChunk(par1ChunkCoordinates.posX + 3 >> 4,
				par1ChunkCoordinates.posZ - 3 >> 4);
		var3.loadChunk(par1ChunkCoordinates.posX - 3 >> 4,
				par1ChunkCoordinates.posZ + 3 >> 4);
		var3.loadChunk(par1ChunkCoordinates.posX + 3 >> 4,
				par1ChunkCoordinates.posZ + 3 >> 4);

		if (par0World.getBlockId(par1ChunkCoordinates.posX,
				par1ChunkCoordinates.posY, par1ChunkCoordinates.posZ) == Block.bed.blockID) {
			final ChunkCoordinates var8 = BlockBed
					.getNearestEmptyChunkCoordinates(par0World,
							par1ChunkCoordinates.posX,
							par1ChunkCoordinates.posY,
							par1ChunkCoordinates.posZ, 0);
			return var8;
		} else {
			final Material var4 = par0World.getBlockMaterial(
					par1ChunkCoordinates.posX, par1ChunkCoordinates.posY,
					par1ChunkCoordinates.posZ);
			final Material var5 = par0World.getBlockMaterial(
					par1ChunkCoordinates.posX, par1ChunkCoordinates.posY + 1,
					par1ChunkCoordinates.posZ);
			final boolean var6 = !var4.isSolid() && !var4.isLiquid();
			final boolean var7 = !var5.isSolid() && !var5.isLiquid();
			return par2 && var6 && var7 ? par1ChunkCoordinates : null;
		}
	}

	/**
	 * Returns the orientation of the bed in degrees.
	 */
	public float getBedOrientationInDegrees() {
		if (playerLocation != null) {
			final int var1 = worldObj.getBlockMetadata(playerLocation.posX,
					playerLocation.posY, playerLocation.posZ);
			final int var2 = BlockDirectional.getDirection(var1);

			switch (var2) {
			case 0:
				return 90.0F;

			case 1:
				return 0.0F;

			case 2:
				return 270.0F;

			case 3:
				return 180.0F;
			}
		}

		return 0.0F;
	}

	/**
	 * Returns whether player is sleeping or not
	 */
	@Override
	public boolean isPlayerSleeping() {
		return sleeping;
	}

	/**
	 * Returns whether or not the player is asleep and the screen has fully
	 * faded.
	 */
	public boolean isPlayerFullyAsleep() {
		return sleeping && sleepTimer >= 100;
	}

	public int getSleepTimer() {
		return sleepTimer;
	}

	protected boolean getHideCape(final int par1) {
		return (dataWatcher.getWatchableObjectByte(16) & 1 << par1) != 0;
	}

	protected void setHideCape(final int par1, final boolean par2) {
		final byte var3 = dataWatcher.getWatchableObjectByte(16);

		if (par2) {
			dataWatcher.updateObject(16,
					Byte.valueOf((byte) (var3 | 1 << par1)));
		} else {
			dataWatcher.updateObject(16,
					Byte.valueOf((byte) (var3 & ~(1 << par1))));
		}
	}

	/**
	 * Add a chat message to the player
	 */
	public void addChatMessage(final String par1Str) {
	}

	/**
	 * Returns the location of the bed the player will respawn at, or null if
	 * the player has not slept in a bed.
	 */
	public ChunkCoordinates getBedLocation() {
		return spawnChunk;
	}

	public boolean isSpawnForced() {
		return spawnForced;
	}

	/**
	 * Defines a spawn coordinate to player spawn. Used by bed after the player
	 * sleep on it.
	 */
	public void setSpawnChunk(final ChunkCoordinates par1ChunkCoordinates,
			final boolean par2) {
		if (par1ChunkCoordinates != null) {
			spawnChunk = new ChunkCoordinates(par1ChunkCoordinates);
			spawnForced = par2;
		} else {
			spawnChunk = null;
			spawnForced = false;
		}
	}

	/**
	 * Will trigger the specified trigger.
	 */
	public void triggerAchievement(final StatBase par1StatBase) {
		addStat(par1StatBase, 1);
	}

	/**
	 * Adds a value to a statistic field.
	 */
	public void addStat(final StatBase par1StatBase, final int par2) {
	}

	/**
	 * Causes this entity to do an upwards motion (jumping).
	 */
	@Override
	public void jump() {
		super.jump();
		addStat(StatList.jumpStat, 1);

		if (isSprinting()) {
			addExhaustion(0.8F);
		} else {
			addExhaustion(0.2F);
		}
	}

	/**
	 * Moves the entity based on the specified heading. Args: strafe, forward
	 */
	@Override
	public void moveEntityWithHeading(final float par1, final float par2) {
		final double var3 = posX;
		final double var5 = posY;
		final double var7 = posZ;

		if (capabilities.isFlying && ridingEntity == null) {
			final double var9 = motionY;
			final float var11 = jumpMovementFactor;
			jumpMovementFactor = capabilities.getFlySpeed();
			super.moveEntityWithHeading(par1, par2);
			motionY = var9 * 0.6D;
			jumpMovementFactor = var11;
		} else {
			super.moveEntityWithHeading(par1, par2);
		}

		addMovementStat(posX - var3, posY - var5, posZ - var7);
	}

	/**
	 * Adds a value to a movement statistic field - like run, walk, swin or
	 * climb.
	 */
	public void addMovementStat(final double par1, final double par3,
			final double par5) {
		if (ridingEntity == null) {
			int var7;

			if (isInsideOfMaterial(Material.water)) {
				var7 = Math.round(MathHelper.sqrt_double(par1 * par1 + par3
						* par3 + par5 * par5) * 100.0F);

				if (var7 > 0) {
					addStat(StatList.distanceDoveStat, var7);
					addExhaustion(0.015F * var7 * 0.01F);
				}
			} else if (isInWater()) {
				var7 = Math.round(MathHelper.sqrt_double(par1 * par1 + par5
						* par5) * 100.0F);

				if (var7 > 0) {
					addStat(StatList.distanceSwumStat, var7);
					addExhaustion(0.015F * var7 * 0.01F);
				}
			} else if (isOnLadder()) {
				if (par3 > 0.0D) {
					addStat(StatList.distanceClimbedStat,
							(int) Math.round(par3 * 100.0D));
				}
			} else if (onGround) {
				var7 = Math.round(MathHelper.sqrt_double(par1 * par1 + par5
						* par5) * 100.0F);

				if (var7 > 0) {
					addStat(StatList.distanceWalkedStat, var7);

					if (isSprinting()) {
						addExhaustion(0.099999994F * var7 * 0.01F);
					} else {
						addExhaustion(0.01F * var7 * 0.01F);
					}
				}
			} else {
				var7 = Math.round(MathHelper.sqrt_double(par1 * par1 + par5
						* par5) * 100.0F);

				if (var7 > 25) {
					addStat(StatList.distanceFlownStat, var7);
				}
			}
		}
	}

	/**
	 * Adds a value to a mounted movement statistic field - by minecart, boat,
	 * or pig.
	 */
	private void addMountedMovementStat(final double par1, final double par3,
			final double par5) {
		if (ridingEntity != null) {
			final int var7 = Math.round(MathHelper.sqrt_double(par1 * par1
					+ par3 * par3 + par5 * par5) * 100.0F);

			if (var7 > 0) {
				if (ridingEntity instanceof EntityMinecart) {
					addStat(StatList.distanceByMinecartStat, var7);

					if (startMinecartRidingCoordinate == null) {
						startMinecartRidingCoordinate = new ChunkCoordinates(
								MathHelper.floor_double(posX),
								MathHelper.floor_double(posY),
								MathHelper.floor_double(posZ));
					} else if (startMinecartRidingCoordinate
							.getDistanceSquared(MathHelper.floor_double(posX),
									MathHelper.floor_double(posY),
									MathHelper.floor_double(posZ)) >= 1000000.0D) {
						addStat(AchievementList.onARail, 1);
					}
				} else if (ridingEntity instanceof EntityBoat) {
					addStat(StatList.distanceByBoatStat, var7);
				} else if (ridingEntity instanceof EntityPig) {
					addStat(StatList.distanceByPigStat, var7);
				}
			}
		}
	}

	/**
	 * Called when the mob is falling. Calculates and applies fall damage.
	 */
	@Override
	protected void fall(final float par1) {
		if (!capabilities.allowFlying) {
			if (par1 >= 2.0F) {
				addStat(StatList.distanceFallenStat,
						(int) Math.round(par1 * 100.0D));
			}

			super.fall(par1);
		}
	}

	/**
	 * This method gets called when the entity kills another one.
	 */
	@Override
	public void onKillEntity(final EntityLiving par1EntityLiving) {
		if (par1EntityLiving instanceof IMob) {
			triggerAchievement(AchievementList.killEnemy);
		}
	}

	/**
	 * Sets the Entity inside a web block.
	 */
	@Override
	public void setInWeb() {
		if (!capabilities.isFlying) {
			super.setInWeb();
		}
	}

	/**
	 * Gets the Icon Index of the item currently held
	 */
	@Override
	public Icon getItemIcon(final ItemStack par1ItemStack, final int par2) {
		Icon var3 = super.getItemIcon(par1ItemStack, par2);

		if (par1ItemStack.itemID == Item.fishingRod.itemID
				&& fishEntity != null) {
			var3 = Item.fishingRod.func_94597_g();
		} else {
			if (par1ItemStack.getItem().requiresMultipleRenderPasses()) {
				return par1ItemStack.getItem().getIconFromDamageForRenderPass(
						par1ItemStack.getItemDamage(), par2);
			}

			if (itemInUse != null && par1ItemStack.itemID == Item.bow.itemID) {
				final int var4 = par1ItemStack.getMaxItemUseDuration()
						- itemInUseCount;

				if (var4 >= 18) {
					return Item.bow.getItemIconForUseDuration(2);
				}

				if (var4 > 13) {
					return Item.bow.getItemIconForUseDuration(1);
				}

				if (var4 > 0) {
					return Item.bow.getItemIconForUseDuration(0);
				}
			}
		}

		return var3;
	}

	@Override
	public ItemStack getCurrentArmor(final int par1) {
		return inventory.armorItemInSlot(par1);
	}

	/**
	 * Makes entity wear random armor based on difficulty
	 */
	@Override
	protected void addRandomArmor() {
	}

	@Override
	protected void func_82162_bC() {
	}

	/**
	 * This method increases the player's current amount of experience.
	 */
	public void addExperience(int par1) {
		addScore(par1);
		final int var2 = Integer.MAX_VALUE - experienceTotal;

		if (par1 > var2) {
			par1 = var2;
		}

		experience += (float) par1 / (float) xpBarCap();

		for (experienceTotal += par1; experience >= 1.0F; experience /= xpBarCap()) {
			experience = (experience - 1.0F) * xpBarCap();
			addExperienceLevel(1);
		}
	}

	/**
	 * Add experience levels to this player.
	 */
	public void addExperienceLevel(final int par1) {
		experienceLevel += par1;

		if (experienceLevel < 0) {
			experienceLevel = 0;
			experience = 0.0F;
			experienceTotal = 0;
		}

		if (par1 > 0 && experienceLevel % 5 == 0
				&& field_82249_h < ticksExisted - 100.0F) {
			final float var2 = experienceLevel > 30 ? 1.0F
					: experienceLevel / 30.0F;
			worldObj.playSoundAtEntity(this, "random.levelup", var2 * 0.75F,
					1.0F);
			field_82249_h = ticksExisted;
		}
	}

	/**
	 * This method returns the cap amount of experience that the experience bar
	 * can hold. With each level, the experience cap on the player's experience
	 * bar is raised by 10.
	 */
	public int xpBarCap() {
		return experienceLevel >= 30 ? 62 + (experienceLevel - 30) * 7
				: experienceLevel >= 15 ? 17 + (experienceLevel - 15) * 3 : 17;
	}

	/**
	 * increases exhaustion level by supplied amount
	 */
	public void addExhaustion(final float par1) {
		if (!capabilities.disableDamage) {
			if (!worldObj.isRemote) {
				foodStats.addExhaustion(par1);
			}
		}
	}

	/**
	 * Returns the player's FoodStats object.
	 */
	public FoodStats getFoodStats() {
		return foodStats;
	}

	public boolean canEat(final boolean par1) {
		return (par1 || foodStats.needFood()) && !capabilities.disableDamage;
	}

	/**
	 * Checks if the player's health is not full and not zero.
	 */
	public boolean shouldHeal() {
		return getHealth() > 0 && getHealth() < getMaxHealth();
	}

	/**
	 * sets the itemInUse when the use item button is clicked. Args: itemstack,
	 * int maxItemUseDuration
	 */
	public void setItemInUse(final ItemStack par1ItemStack, final int par2) {
		if (par1ItemStack != itemInUse) {
			itemInUse = par1ItemStack;
			itemInUseCount = par2;

			if (!worldObj.isRemote) {
				setEating(true);
			}
		}
	}

	/**
	 * Returns true if the item the player is holding can harvest the block at
	 * the given coords. Args: x, y, z.
	 */
	public boolean canCurrentToolHarvestBlock(final int par1, final int par2,
			final int par3) {
		if (capabilities.allowEdit) {
			return true;
		} else {
			final int var4 = worldObj.getBlockId(par1, par2, par3);

			if (var4 > 0) {
				final Block var5 = Block.blocksList[var4];

				if (var5.blockMaterial.isAlwaysHarvested()) {
					return true;
				}

				if (getCurrentEquippedItem() != null) {
					final ItemStack var6 = getCurrentEquippedItem();

					if (var6.canHarvestBlock(var5)
							|| var6.getStrVsBlock(var5) > 1.0F) {
						return true;
					}
				}
			}

			return false;
		}
	}

	public boolean canPlayerEdit(final int par1, final int par2,
			final int par3, final int par4, final ItemStack par5ItemStack) {
		return capabilities.allowEdit ? true
				: par5ItemStack != null ? par5ItemStack.func_82835_x() : false;
	}

	/**
	 * Get the experience points the entity currently has.
	 */
	@Override
	protected int getExperiencePoints(final EntityPlayer par1EntityPlayer) {
		if (worldObj.getGameRules().getGameRuleBooleanValue("keepInventory")) {
			return 0;
		} else {
			final int var2 = experienceLevel * 7;
			return var2 > 100 ? 100 : var2;
		}
	}

	/**
	 * Only use is to identify if class is an instance of player for experience
	 * dropping
	 */
	@Override
	protected boolean isPlayer() {
		return true;
	}

	/**
	 * Gets the username of the entity.
	 */
	@Override
	public String getEntityName() {
		return username;
	}

	@Override
	public boolean func_94062_bN() {
		return super.func_94062_bN();
	}

	@Override
	public boolean func_94059_bO() {
		return true;
	}

	@Override
	public boolean canPickUpLoot() {
		return false;
	}

	/**
	 * Copies the values from the given player into this player if boolean par2
	 * is true. Always clones Ender Chest Inventory.
	 */
	public void clonePlayer(final EntityPlayer par1EntityPlayer,
			final boolean par2) {
		if (par2) {
			inventory.copyInventory(par1EntityPlayer.inventory);
			health = par1EntityPlayer.health;
			foodStats = par1EntityPlayer.foodStats;
			experienceLevel = par1EntityPlayer.experienceLevel;
			experienceTotal = par1EntityPlayer.experienceTotal;
			experience = par1EntityPlayer.experience;
			setScore(par1EntityPlayer.getScore());
			teleportDirection = par1EntityPlayer.teleportDirection;
		} else if (worldObj.getGameRules().getGameRuleBooleanValue(
				"keepInventory")) {
			inventory.copyInventory(par1EntityPlayer.inventory);
			experienceLevel = par1EntityPlayer.experienceLevel;
			experienceTotal = par1EntityPlayer.experienceTotal;
			experience = par1EntityPlayer.experience;
			setScore(par1EntityPlayer.getScore());
		}

		theInventoryEnderChest = par1EntityPlayer.theInventoryEnderChest;
	}

	/**
	 * returns if this entity triggers Block.onEntityWalking on the blocks they
	 * walk on. used for spiders and wolves to prevent them from trampling crops
	 */
	@Override
	protected boolean canTriggerWalking() {
		return !capabilities.isFlying;
	}

	/**
	 * Sends the player's abilities to the server (if there is one).
	 */
	public void sendPlayerAbilities() {
	}

	/**
	 * Sets the player's game mode and sends it to them.
	 */
	public void setGameType(final EnumGameType par1EnumGameType) {
	}

	/**
	 * Gets the name of this command sender (usually username, but possibly
	 * "Rcon")
	 */
	@Override
	public String getCommandSenderName() {
		return username;
	}

	public StringTranslate getTranslator() {
		return StringTranslate.getInstance();
	}

	/**
	 * Translates and formats the given string key with the given arguments.
	 */
	@Override
	public String translateString(final String par1Str,
			final Object... par2ArrayOfObj) {
		return getTranslator().translateKeyFormat(par1Str, par2ArrayOfObj);
	}

	/**
	 * Returns the InventoryEnderChest of this player.
	 */
	public InventoryEnderChest getInventoryEnderChest() {
		return theInventoryEnderChest;
	}

	/**
	 * 0 = item, 1-n is armor
	 */
	@Override
	public ItemStack getCurrentItemOrArmor(final int par1) {
		return par1 == 0 ? inventory.getCurrentItem()
				: inventory.armorInventory[par1 - 1];
	}

	/**
	 * Returns the item that this EntityLiving is holding, if any.
	 */
	@Override
	public ItemStack getHeldItem() {
		return inventory.getCurrentItem();
	}

	/**
	 * Sets the held item, or an armor slot. Slot 0 is held item. Slot 1-4 is
	 * armor. Params: Item, slot
	 */
	@Override
	public void setCurrentItemOrArmor(final int par1,
			final ItemStack par2ItemStack) {
		inventory.armorInventory[par1] = par2ItemStack;
	}

	@Override
	public boolean func_98034_c(final EntityPlayer par1EntityPlayer) {
		if (!isInvisible()) {
			return false;
		} else {
			final ScorePlayerTeam var2 = getTeam();
			return var2 == null || par1EntityPlayer == null
					|| par1EntityPlayer.getTeam() != var2
					|| !var2.func_98297_h();
		}
	}

	@Override
	public ItemStack[] getLastActiveItems() {
		return inventory.armorInventory;
	}

	public boolean getHideCape() {
		return this.getHideCape(1);
	}

	@Override
	public boolean func_96092_aw() {
		return !capabilities.isFlying;
	}

	public Scoreboard getWorldScoreboard() {
		return worldObj.getScoreboard();
	}

	public ScorePlayerTeam getTeam() {
		return getWorldScoreboard().getPlayersTeam(username);
	}

	/**
	 * Returns the translated name of the entity.
	 */
	@Override
	public String getTranslatedEntityName() {
		return ScorePlayerTeam.func_96667_a(getTeam(), username);
	}
}
