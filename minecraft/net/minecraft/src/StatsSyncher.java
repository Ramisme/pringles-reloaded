package net.minecraft.src;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

public class StatsSyncher {
	private volatile boolean isBusy = false;
	private volatile Map field_77430_b = null;
	private volatile Map field_77431_c = null;

	/**
	 * The StatFileWriter object, presumably used to write to the statistics
	 * files
	 */
	private final StatFileWriter statFileWriter;

	/** A file named 'stats_' [lower case username] '_unsent.dat' */
	private final File unsentDataFile;

	/** A file named 'stats_' [lower case username] '.dat' */
	private final File dataFile;

	/** A file named 'stats_' [lower case username] '_unsent.tmp' */
	private final File unsentTempFile;

	/** A file named 'stats_' [lower case username] '.tmp' */
	private final File tempFile;

	/** A file named 'stats_' [lower case username] '_unsent.old' */
	private final File unsentOldFile;

	/** A file named 'stats_' [lower case username] '.old' */
	private final File oldFile;

	/** The Session object */
	private final Session theSession;
	private int field_77433_l = 0;
	private int field_77434_m = 0;

	public StatsSyncher(final Session par1Session,
			final StatFileWriter par2StatFileWriter, final File par3File) {
		unsentDataFile = new File(par3File, "stats_"
				+ par1Session.username.toLowerCase() + "_unsent.dat");
		dataFile = new File(par3File, "stats_"
				+ par1Session.username.toLowerCase() + ".dat");
		unsentOldFile = new File(par3File, "stats_"
				+ par1Session.username.toLowerCase() + "_unsent.old");
		oldFile = new File(par3File, "stats_"
				+ par1Session.username.toLowerCase() + ".old");
		unsentTempFile = new File(par3File, "stats_"
				+ par1Session.username.toLowerCase() + "_unsent.tmp");
		tempFile = new File(par3File, "stats_"
				+ par1Session.username.toLowerCase() + ".tmp");

		if (!par1Session.username.toLowerCase().equals(par1Session.username)) {
			func_77412_a(par3File, "stats_" + par1Session.username
					+ "_unsent.dat", unsentDataFile);
			func_77412_a(par3File, "stats_" + par1Session.username + ".dat",
					dataFile);
			func_77412_a(par3File, "stats_" + par1Session.username
					+ "_unsent.old", unsentOldFile);
			func_77412_a(par3File, "stats_" + par1Session.username + ".old",
					oldFile);
			func_77412_a(par3File, "stats_" + par1Session.username
					+ "_unsent.tmp", unsentTempFile);
			func_77412_a(par3File, "stats_" + par1Session.username + ".tmp",
					tempFile);
		}

		statFileWriter = par2StatFileWriter;
		theSession = par1Session;

		if (unsentDataFile.exists()) {
			par2StatFileWriter.writeStats(func_77417_a(unsentDataFile,
					unsentTempFile, unsentOldFile));
		}

		beginReceiveStats();
	}

	private void func_77412_a(final File par1File, final String par2Str,
			final File par3File) {
		final File var4 = new File(par1File, par2Str);

		if (var4.exists() && !var4.isDirectory() && !par3File.exists()) {
			var4.renameTo(par3File);
		}
	}

	private Map func_77417_a(final File par1File, final File par2File,
			final File par3File) {
		return par1File.exists() ? func_77413_a(par1File)
				: par3File.exists() ? func_77413_a(par3File) : par2File
						.exists() ? func_77413_a(par2File) : null;
	}

	private Map func_77413_a(final File par1File) {
		BufferedReader var2 = null;

		try {
			var2 = new BufferedReader(new FileReader(par1File));
			String var3 = "";
			final StringBuilder var4 = new StringBuilder();

			while ((var3 = var2.readLine()) != null) {
				var4.append(var3);
			}

			final Map var5 = StatFileWriter.func_77453_b(var4.toString());
			return var5;
		} catch (final Exception var15) {
			var15.printStackTrace();
		} finally {
			if (var2 != null) {
				try {
					var2.close();
				} catch (final Exception var14) {
					var14.printStackTrace();
				}
			}
		}

		return null;
	}

	private void func_77421_a(final Map par1Map, final File par2File,
			final File par3File, final File par4File) throws IOException {
		final PrintWriter var5 = new PrintWriter(
				new FileWriter(par3File, false));

		try {
			var5.print(StatFileWriter.func_77441_a(theSession.username,
					"local", par1Map));
		} finally {
			var5.close();
		}

		if (par4File.exists()) {
			par4File.delete();
		}

		if (par2File.exists()) {
			par2File.renameTo(par4File);
		}

		par3File.renameTo(par2File);
	}

	/**
	 * Attempts to begin receiving stats from the server. Will throw an
	 * IllegalStateException if the syncher is already busy.
	 */
	public void beginReceiveStats() {
		if (isBusy) {
			throw new IllegalStateException(
					"Can\'t get stats from server while StatsSyncher is busy!");
		} else {
			field_77433_l = 100;
			isBusy = true;
			new ThreadStatSyncherReceive(this).start();
		}
	}

	/**
	 * Attempts to begin sending stats to the server. Will throw an
	 * IllegalStateException if the syncher is already busy.
	 */
	public void beginSendStats(final Map par1Map) {
		if (isBusy) {
			throw new IllegalStateException(
					"Can\'t save stats while StatsSyncher is busy!");
		} else {
			field_77433_l = 100;
			isBusy = true;
			new ThreadStatSyncherSend(this, par1Map).start();
		}
	}

	public void syncStatsFileWithMap(final Map par1Map) {
		int var2 = 30;

		while (isBusy) {
			--var2;

			if (var2 <= 0) {
				break;
			}

			try {
				Thread.sleep(100L);
			} catch (final InterruptedException var10) {
				var10.printStackTrace();
			}
		}

		isBusy = true;

		try {
			func_77421_a(par1Map, unsentDataFile, unsentTempFile, unsentOldFile);
		} catch (final Exception var8) {
			var8.printStackTrace();
		} finally {
			isBusy = false;
		}
	}

	public boolean func_77425_c() {
		return field_77433_l <= 0 && !isBusy && field_77431_c == null;
	}

	public void func_77422_e() {
		if (field_77433_l > 0) {
			--field_77433_l;
		}

		if (field_77434_m > 0) {
			--field_77434_m;
		}

		if (field_77431_c != null) {
			statFileWriter.func_77448_c(field_77431_c);
			field_77431_c = null;
		}

		if (field_77430_b != null) {
			statFileWriter.func_77452_b(field_77430_b);
			field_77430_b = null;
		}
	}

	static Map func_77419_a(final StatsSyncher par0StatsSyncher) {
		return par0StatsSyncher.field_77430_b;
	}

	static File func_77408_b(final StatsSyncher par0StatsSyncher) {
		return par0StatsSyncher.dataFile;
	}

	static File func_77407_c(final StatsSyncher par0StatsSyncher) {
		return par0StatsSyncher.tempFile;
	}

	static File func_77411_d(final StatsSyncher par0StatsSyncher) {
		return par0StatsSyncher.oldFile;
	}

	static void func_77414_a(final StatsSyncher par0StatsSyncher,
			final Map par1Map, final File par2File, final File par3File,
			final File par4File) throws IOException {
		par0StatsSyncher.func_77421_a(par1Map, par2File, par3File, par4File);
	}

	static Map func_77416_a(final StatsSyncher par0StatsSyncher,
			final Map par1Map) {
		return par0StatsSyncher.field_77430_b = par1Map;
	}

	static Map func_77410_a(final StatsSyncher par0StatsSyncher,
			final File par1File, final File par2File, final File par3File) {
		return par0StatsSyncher.func_77417_a(par1File, par2File, par3File);
	}

	static boolean setBusy(final StatsSyncher par0StatsSyncher,
			final boolean par1) {
		return par0StatsSyncher.isBusy = par1;
	}

	static File getUnsentDataFile(final StatsSyncher par0StatsSyncher) {
		return par0StatsSyncher.unsentDataFile;
	}

	static File getUnsentTempFile(final StatsSyncher par0StatsSyncher) {
		return par0StatsSyncher.unsentTempFile;
	}

	static File getUnsentOldFile(final StatsSyncher par0StatsSyncher) {
		return par0StatsSyncher.unsentOldFile;
	}
}
