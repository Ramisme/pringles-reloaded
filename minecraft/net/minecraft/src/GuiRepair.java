package net.minecraft.src;

import java.util.List;

import net.minecraft.client.Minecraft;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

public class GuiRepair extends GuiContainer implements ICrafting {
	private final ContainerRepair repairContainer;
	private GuiTextField itemNameField;
	private final InventoryPlayer field_82325_q;

	public GuiRepair(final InventoryPlayer par1, final World par2World,
			final int par3, final int par4, final int par5) {
		super(new ContainerRepair(par1, par2World, par3, par4, par5,
				Minecraft.getMinecraft().thePlayer));
		field_82325_q = par1;
		repairContainer = (ContainerRepair) inventorySlots;
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui() {
		super.initGui();
		Keyboard.enableRepeatEvents(true);
		final int var1 = (width - xSize) / 2;
		final int var2 = (height - ySize) / 2;
		itemNameField = new GuiTextField(fontRenderer, var1 + 62, var2 + 24,
				103, 12);
		itemNameField.setTextColor(-1);
		itemNameField.setDisabledTextColour(-1);
		itemNameField.setEnableBackgroundDrawing(false);
		itemNameField.setMaxStringLength(30);
		inventorySlots.removeCraftingFromCrafters(this);
		inventorySlots.addCraftingToCrafters(this);
	}

	/**
	 * Called when the screen is unloaded. Used to disable keyboard repeat
	 * events
	 */
	@Override
	public void onGuiClosed() {
		super.onGuiClosed();
		Keyboard.enableRepeatEvents(false);
		inventorySlots.removeCraftingFromCrafters(this);
	}

	/**
	 * Draw the foreground layer for the GuiContainer (everything in front of
	 * the items)
	 */
	@Override
	protected void drawGuiContainerForegroundLayer(final int par1,
			final int par2) {
		GL11.glDisable(GL11.GL_LIGHTING);
		fontRenderer.drawString(
				StatCollector.translateToLocal("container.repair"), 60, 6,
				4210752);

		if (repairContainer.maximumCost > 0) {
			int var3 = 8453920;
			boolean var4 = true;
			String var5 = StatCollector
					.translateToLocalFormatted("container.repair.cost",
							new Object[] { Integer
									.valueOf(repairContainer.maximumCost) });

			if (repairContainer.maximumCost >= 40
					&& !mc.thePlayer.capabilities.isCreativeMode) {
				var5 = StatCollector
						.translateToLocal("container.repair.expensive");
				var3 = 16736352;
			} else if (!repairContainer.getSlot(2).getHasStack()) {
				var4 = false;
			} else if (!repairContainer.getSlot(2).canTakeStack(
					field_82325_q.player)) {
				var3 = 16736352;
			}

			if (var4) {
				final int var6 = -16777216 | (var3 & 16579836) >> 2 | var3
						& -16777216;
				final int var7 = xSize - 8 - fontRenderer.getStringWidth(var5);
				final byte var8 = 67;

				if (fontRenderer.getUnicodeFlag()) {
					Gui.drawRect(var7 - 3, var8 - 2, xSize - 7, var8 + 10,
							-16777216);
					Gui.drawRect(var7 - 2, var8 - 1, xSize - 8, var8 + 9,
							-12895429);
				} else {
					fontRenderer.drawString(var5, var7, var8 + 1, var6);
					fontRenderer.drawString(var5, var7 + 1, var8, var6);
					fontRenderer.drawString(var5, var7 + 1, var8 + 1, var6);
				}

				fontRenderer.drawString(var5, var7, var8, var3);
			}
		}

		GL11.glEnable(GL11.GL_LIGHTING);
	}

	/**
	 * Fired when a key is typed. This is the equivalent of
	 * KeyListener.keyTyped(KeyEvent e).
	 */
	@Override
	protected void keyTyped(final char par1, final int par2) {
		if (itemNameField.textboxKeyTyped(par1, par2)) {
			repairContainer.updateItemName(itemNameField.getText());
			mc.thePlayer.sendQueue.addToSendQueue(new Packet250CustomPayload(
					"MC|ItemName", itemNameField.getText().getBytes()));
		} else {
			super.keyTyped(par1, par2);
		}
	}

	/**
	 * Called when the mouse is clicked.
	 */
	@Override
	protected void mouseClicked(final int par1, final int par2, final int par3) {
		super.mouseClicked(par1, par2, par3);
		itemNameField.mouseClicked(par1, par2, par3);
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(final int par1, final int par2, final float par3) {
		super.drawScreen(par1, par2, par3);
		GL11.glDisable(GL11.GL_LIGHTING);
		itemNameField.drawTextBox();
	}

	/**
	 * Draw the background layer for the GuiContainer (everything behind the
	 * items)
	 */
	@Override
	protected void drawGuiContainerBackgroundLayer(final float par1,
			final int par2, final int par3) {
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		mc.renderEngine.bindTexture("/gui/repair.png");
		final int var4 = (width - xSize) / 2;
		final int var5 = (height - ySize) / 2;
		drawTexturedModalRect(var4, var5, 0, 0, xSize, ySize);
		drawTexturedModalRect(var4 + 59, var5 + 20, 0, ySize
				+ (repairContainer.getSlot(0).getHasStack() ? 0 : 16), 110, 16);

		if ((repairContainer.getSlot(0).getHasStack() || repairContainer
				.getSlot(1).getHasStack())
				&& !repairContainer.getSlot(2).getHasStack()) {
			drawTexturedModalRect(var4 + 99, var5 + 45, xSize, 0, 28, 21);
		}
	}

	@Override
	public void sendContainerAndContentsToPlayer(final Container par1Container,
			final List par2List) {
		sendSlotContents(par1Container, 0, par1Container.getSlot(0).getStack());
	}

	/**
	 * Sends the contents of an inventory slot to the client-side Container.
	 * This doesn't have to match the actual contents of that slot. Args:
	 * Container, slot number, slot contents
	 */
	@Override
	public void sendSlotContents(final Container par1Container, final int par2,
			final ItemStack par3ItemStack) {
		if (par2 == 0) {
			itemNameField.setText(par3ItemStack == null ? "" : par3ItemStack
					.getDisplayName());
			itemNameField.setEnabled(par3ItemStack != null);

			if (par3ItemStack != null) {
				repairContainer.updateItemName(itemNameField.getText());
				mc.thePlayer.sendQueue
						.addToSendQueue(new Packet250CustomPayload(
								"MC|ItemName", itemNameField.getText()
										.getBytes()));
			}
		}
	}

	/**
	 * Sends two ints to the client-side Container. Used for furnace burning
	 * time, smelting progress, brewing progress, and enchanting level. Normally
	 * the first int identifies which variable to update, and the second
	 * contains the new value. Both are truncated to shorts in non-local SMP.
	 */
	@Override
	public void sendProgressBarUpdate(final Container par1Container,
			final int par2, final int par3) {
	}
}
