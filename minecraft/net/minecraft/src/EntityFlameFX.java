package net.minecraft.src;

public class EntityFlameFX extends EntityFX {
	/** the scale of the flame FX */
	private final float flameScale;

	public EntityFlameFX(final World par1World, final double par2,
			final double par4, final double par6, final double par8,
			final double par10, final double par12) {
		super(par1World, par2, par4, par6, par8, par10, par12);
		this.motionX = this.motionX * 0.009999999776482582D + par8;
		this.motionY = this.motionY * 0.009999999776482582D + par10;
		this.motionZ = this.motionZ * 0.009999999776482582D + par12;
		double var10000 = par2
				+ (this.rand.nextFloat() - this.rand.nextFloat()) * 0.05F;
		var10000 = par4 + (this.rand.nextFloat() - this.rand.nextFloat())
				* 0.05F;
		var10000 = par6 + (this.rand.nextFloat() - this.rand.nextFloat())
				* 0.05F;
		flameScale = particleScale;
		particleRed = particleGreen = particleBlue = 1.0F;
		particleMaxAge = (int) (8.0D / (Math.random() * 0.8D + 0.2D)) + 4;
		noClip = true;
		setParticleTextureIndex(48);

	}

	@Override
	public void renderParticle(final Tessellator par1Tessellator,
			final float par2, final float par3, final float par4,
			final float par5, final float par6, final float par7) {
		final float var8 = (particleAge + par2) / particleMaxAge;
		particleScale = flameScale * (1.0F - var8 * var8 * 0.5F);
		super.renderParticle(par1Tessellator, par2, par3, par4, par5, par6,
				par7);
	}

	@Override
	public int getBrightnessForRender(final float par1) {
		float var2 = (particleAge + par1) / particleMaxAge;

		if (var2 < 0.0F) {
			var2 = 0.0F;
		}

		if (var2 > 1.0F) {
			var2 = 1.0F;
		}

		final int var3 = super.getBrightnessForRender(par1);
		int var4 = var3 & 255;
		final int var5 = var3 >> 16 & 255;
		var4 += (int) (var2 * 15.0F * 16.0F);

		if (var4 > 240) {
			var4 = 240;
		}

		return var4 | var5 << 16;
	}

	/**
	 * Gets how bright this entity is.
	 */
	@Override
	public float getBrightness(final float par1) {
		float var2 = (particleAge + par1) / particleMaxAge;

		if (var2 < 0.0F) {
			var2 = 0.0F;
		}

		if (var2 > 1.0F) {
			var2 = 1.0F;
		}

		final float var3 = super.getBrightness(par1);
		return var3 * var2 + (1.0F - var2);
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate() {
		prevPosX = posX;
		prevPosY = posY;
		prevPosZ = posZ;

		if (particleAge++ >= particleMaxAge) {
			setDead();
		}

		moveEntity(motionX, motionY, motionZ);
		motionX *= 0.9599999785423279D;
		motionY *= 0.9599999785423279D;
		motionZ *= 0.9599999785423279D;

		if (onGround) {
			motionX *= 0.699999988079071D;
			motionZ *= 0.699999988079071D;
		}
	}
}
