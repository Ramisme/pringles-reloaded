package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet103SetSlot extends Packet {
	/** The window which is being updated. 0 for player inventory */
	public int windowId;

	/** Slot that should be updated */
	public int itemSlot;

	/** Item stack */
	public ItemStack myItemStack;

	public Packet103SetSlot() {
	}

	public Packet103SetSlot(final int par1, final int par2,
			final ItemStack par3ItemStack) {
		windowId = par1;
		itemSlot = par2;
		myItemStack = par3ItemStack == null ? par3ItemStack : par3ItemStack
				.copy();
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleSetSlot(this);
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		windowId = par1DataInputStream.readByte();
		itemSlot = par1DataInputStream.readShort();
		myItemStack = Packet.readItemStack(par1DataInputStream);
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		par1DataOutputStream.writeByte(windowId);
		par1DataOutputStream.writeShort(itemSlot);
		Packet.writeItemStack(myItemStack, par1DataOutputStream);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 8;
	}
}
