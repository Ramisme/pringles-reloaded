package net.minecraft.src;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import argo.jdom.JdomParser;
import argo.jdom.JsonNode;
import argo.jdom.JsonRootNode;
import argo.saj.InvalidSyntaxException;

public class ValueObjectList extends ValueObject {
	public List field_96430_d;

	public static ValueObjectList func_98161_a(final String par0Str) {
		final ValueObjectList var1 = new ValueObjectList();
		var1.field_96430_d = new ArrayList();

		try {
			final JsonRootNode var2 = new JdomParser().parse(par0Str);

			if (var2.isArrayNode(new Object[] { "servers" })) {
				final Iterator var3 = var2.getArrayNode(
						new Object[] { "servers" }).iterator();

				while (var3.hasNext()) {
					final JsonNode var4 = (JsonNode) var3.next();
					var1.field_96430_d.add(McoServer.func_98163_a(var4));
				}
			}
		} catch (final InvalidSyntaxException var5) {
			;
		} catch (final IllegalArgumentException var6) {
			;
		}

		return var1;
	}
}
