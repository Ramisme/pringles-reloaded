package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import net.minecraft.client.Minecraft;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ThreadDownloadResources extends Thread {
	/** The folder to store the resources in. */
	public File resourcesFolder;

	/** A reference to the Minecraft object. */
	private final Minecraft mc;

	/** Set to true when Minecraft is closing down. */
	private boolean closing = false;

	public ThreadDownloadResources(final File par1File,
			final Minecraft par2Minecraft) {
		mc = par2Minecraft;
		setName("Resource download thread");
		setDaemon(true);
		resourcesFolder = new File(par1File, "resources/");

		if (!resourcesFolder.exists() && !resourcesFolder.mkdirs()) {
			throw new RuntimeException(
					"The working directory could not be created: "
							+ resourcesFolder);
		}
	}

	@Override
	public void run() {
		try {
			final URL var1 = new URL(
					"http://s3.amazonaws.com/MinecraftResources/");
			final DocumentBuilderFactory var2 = DocumentBuilderFactory
					.newInstance();
			final DocumentBuilder var3 = var2.newDocumentBuilder();
			final Document var4 = var3.parse(var1.openStream());
			final NodeList var5 = var4.getElementsByTagName("Contents");

			for (int var6 = 0; var6 < 2; ++var6) {
				for (int var7 = 0; var7 < var5.getLength(); ++var7) {
					final Node var8 = var5.item(var7);

					if (var8.getNodeType() == 1) {
						final Element var9 = (Element) var8;
						final String var10 = var9.getElementsByTagName("Key")
								.item(0).getChildNodes().item(0).getNodeValue();
						final long var11 = Long.parseLong(var9
								.getElementsByTagName("Size").item(0)
								.getChildNodes().item(0).getNodeValue());

						if (var11 > 0L) {
							downloadAndInstallResource(var1, var10, var11, var6);

							if (closing) {
								return;
							}
						}
					}
				}
			}
		} catch (final Exception var13) {
			loadResource(resourcesFolder, "");
			var13.printStackTrace();
		}
	}

	/**
	 * Reloads the resource folder and passes the resources to Minecraft to
	 * install.
	 */
	public void reloadResources() {
		loadResource(resourcesFolder, "");
	}

	/**
	 * Loads a resource and passes it to Minecraft to install.
	 */
	private void loadResource(final File par1File, final String par2Str) {
		final File[] var3 = par1File.listFiles();

		for (final File element : var3) {
			if (element.isDirectory()) {
				loadResource(element, par2Str + element.getName() + "/");
			} else {
				try {
					mc.installResource(par2Str + element.getName(), element);
				} catch (final Exception var6) {
					mc.getLogAgent().logWarning(
							"Failed to add " + par2Str + element.getName()
									+ " in resources");
				}
			}
		}
	}

	/**
	 * Downloads the resource and saves it to disk then installs it.
	 */
	private void downloadAndInstallResource(final URL par1URL,
			final String par2Str, final long par3, final int par5) {
		try {
			final int var6 = par2Str.indexOf("/");
			final String var7 = par2Str.substring(0, var6);

			if (var7.equalsIgnoreCase("sound3")) {
				if (par5 != 0) {
					return;
				}
			} else if (par5 != 1) {
				return;
			}

			final File var8 = new File(resourcesFolder, par2Str);

			if (!var8.exists() || var8.length() != par3) {
				var8.getParentFile().mkdirs();
				final String var9 = par2Str.replaceAll(" ", "%20");
				downloadResource(new URL(par1URL, var9), var8, par3);

				if (closing) {
					return;
				}
			}

			mc.installResource(par2Str, var8);
		} catch (final Exception var10) {
			var10.printStackTrace();
		}
	}

	/**
	 * Downloads the resource and saves it to disk.
	 */
	private void downloadResource(final URL par1URL, final File par2File,
			final long par3) throws IOException {
		final byte[] var5 = new byte[4096];
		final DataInputStream var6 = new DataInputStream(par1URL.openStream());
		final DataOutputStream var7 = new DataOutputStream(
				new FileOutputStream(par2File));
		do {
			int var9;

			if ((var9 = var6.read(var5)) < 0) {
				var6.close();
				var7.close();
				return;
			}

			var7.write(var5, 0, var9);
		} while (!closing);
		var7.close();
	}

	/**
	 * Called when Minecraft is closing down.
	 */
	public void closeMinecraft() {
		closing = true;
	}
}
