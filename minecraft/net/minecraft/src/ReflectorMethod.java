package net.minecraft.src;

import java.lang.reflect.Method;

public class ReflectorMethod {
	private ReflectorClass reflectorClass;
	private String targetMethodName;
	private Class[] targetMethodParameterTypes;
	private boolean checked;
	private Method targetMethod;

	public ReflectorMethod(final ReflectorClass var1, final String var2) {
		this(var1, var2, (Class[]) null);
	}

	public ReflectorMethod(final ReflectorClass var1, final String var2,
			final Class[] var3) {
		reflectorClass = null;
		targetMethodName = null;
		targetMethodParameterTypes = null;
		checked = false;
		targetMethod = null;
		reflectorClass = var1;
		targetMethodName = var2;
		targetMethodParameterTypes = var3;
		getTargetMethod();
	}

	public Method getTargetMethod() {
		if (checked) {
			return targetMethod;
		} else {
			checked = true;
			final Class var1 = reflectorClass.getTargetClass();

			if (var1 == null) {
				return null;
			} else {
				final Method[] var2 = var1.getMethods();
				int var3 = 0;
				Method var4;

				while (true) {
					if (var3 >= var2.length) {
						Config.log("(Reflector) Method not pesent: "
								+ var1.getName() + "." + targetMethodName);
						return null;
					}

					var4 = var2[var3];

					if (var4.getName().equals(targetMethodName)) {
						if (targetMethodParameterTypes == null) {
							break;
						}

						final Class[] var5 = var4.getParameterTypes();

						if (Reflector.matchesTypes(targetMethodParameterTypes,
								var5)) {
							break;
						}
					}

					++var3;
				}

				targetMethod = var4;
				return targetMethod;
			}
		}
	}

	public boolean exists() {
		return getTargetMethod() != null;
	}

	public Class getReturnType() {
		final Method var1 = getTargetMethod();
		return var1 == null ? null : var1.getReturnType();
	}

	public void deactivate() {
		checked = true;
		targetMethod = null;
	}
}
