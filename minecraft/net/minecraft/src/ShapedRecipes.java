package net.minecraft.src;

public class ShapedRecipes implements IRecipe {
	/** How many horizontal slots this recipe is wide. */
	private final int recipeWidth;

	/** How many vertical slots this recipe uses. */
	private final int recipeHeight;

	/** Is a array of ItemStack that composes the recipe. */
	private final ItemStack[] recipeItems;

	/** Is the ItemStack that you get when craft the recipe. */
	private final ItemStack recipeOutput;

	/** Is the itemID of the output item that you get when craft the recipe. */
	public final int recipeOutputItemID;
	private boolean field_92101_f = false;

	public ShapedRecipes(final int par1, final int par2,
			final ItemStack[] par3ArrayOfItemStack,
			final ItemStack par4ItemStack) {
		recipeOutputItemID = par4ItemStack.itemID;
		recipeWidth = par1;
		recipeHeight = par2;
		recipeItems = par3ArrayOfItemStack;
		recipeOutput = par4ItemStack;
	}

	@Override
	public ItemStack getRecipeOutput() {
		return recipeOutput;
	}

	/**
	 * Used to check if a recipe matches current crafting inventory
	 */
	@Override
	public boolean matches(final InventoryCrafting par1InventoryCrafting,
			final World par2World) {
		for (int var3 = 0; var3 <= 3 - recipeWidth; ++var3) {
			for (int var4 = 0; var4 <= 3 - recipeHeight; ++var4) {
				if (checkMatch(par1InventoryCrafting, var3, var4, true)) {
					return true;
				}

				if (checkMatch(par1InventoryCrafting, var3, var4, false)) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * Checks if the region of a crafting inventory is match for the recipe.
	 */
	private boolean checkMatch(final InventoryCrafting par1InventoryCrafting,
			final int par2, final int par3, final boolean par4) {
		for (int var5 = 0; var5 < 3; ++var5) {
			for (int var6 = 0; var6 < 3; ++var6) {
				final int var7 = var5 - par2;
				final int var8 = var6 - par3;
				ItemStack var9 = null;

				if (var7 >= 0 && var8 >= 0 && var7 < recipeWidth
						&& var8 < recipeHeight) {
					if (par4) {
						var9 = recipeItems[recipeWidth - var7 - 1 + var8
								* recipeWidth];
					} else {
						var9 = recipeItems[var7 + var8 * recipeWidth];
					}
				}

				final ItemStack var10 = par1InventoryCrafting
						.getStackInRowAndColumn(var5, var6);

				if (var10 != null || var9 != null) {
					if (var10 == null && var9 != null || var10 != null
							&& var9 == null) {
						return false;
					}

					if (var9.itemID != var10.itemID) {
						return false;
					}

					if (var9.getItemDamage() != 32767
							&& var9.getItemDamage() != var10.getItemDamage()) {
						return false;
					}
				}
			}
		}

		return true;
	}

	/**
	 * Returns an Item that is the result of this recipe
	 */
	@Override
	public ItemStack getCraftingResult(
			final InventoryCrafting par1InventoryCrafting) {
		final ItemStack var2 = getRecipeOutput().copy();

		if (field_92101_f) {
			for (int var3 = 0; var3 < par1InventoryCrafting.getSizeInventory(); ++var3) {
				final ItemStack var4 = par1InventoryCrafting
						.getStackInSlot(var3);

				if (var4 != null && var4.hasTagCompound()) {
					var2.setTagCompound((NBTTagCompound) var4.stackTagCompound
							.copy());
				}
			}
		}

		return var2;
	}

	/**
	 * Returns the size of the recipe area
	 */
	@Override
	public int getRecipeSize() {
		return recipeWidth * recipeHeight;
	}

	public ShapedRecipes func_92100_c() {
		field_92101_f = true;
		return this;
	}
}
