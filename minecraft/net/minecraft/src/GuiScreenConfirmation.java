package net.minecraft.src;

public class GuiScreenConfirmation extends GuiYesNo {
	private final String field_96288_n;

	public GuiScreenConfirmation(final GuiScreen par1GuiScreen,
			final String par2Str, final String par3Str, final String par4Str,
			final int par5) {
		super(par1GuiScreen, par2Str, par3Str, par5);
		field_96288_n = par4Str;
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui() {
		buttonList.add(new GuiSmallButton(0, width / 2 - 155, height / 6 + 112,
				buttonText1));
		buttonList.add(new GuiSmallButton(1, width / 2 - 155 + 160,
				height / 6 + 112, buttonText2));
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(final int par1, final int par2, final float par3) {
		super.drawScreen(par1, par2, par3);
		drawCenteredString(fontRenderer, field_96288_n, width / 2, 110,
				16777215);
	}
}
