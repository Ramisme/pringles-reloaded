package net.minecraft.src;

public class EntitySpellParticleFX extends EntityFX {
	/** Base spell texture index */
	private int baseSpellTextureIndex = 128;

	public EntitySpellParticleFX(final World par1World, final double par2,
			final double par4, final double par6, final double par8,
			final double par10, final double par12) {
		super(par1World, par2, par4, par6, par8, par10, par12);
		motionY *= 0.20000000298023224D;

		if (par8 == 0.0D && par12 == 0.0D) {
			motionX *= 0.10000000149011612D;
			motionZ *= 0.10000000149011612D;
		}

		particleScale *= 0.75F;
		particleMaxAge = (int) (8.0D / (Math.random() * 0.8D + 0.2D));
		noClip = false;
	}

	@Override
	public void renderParticle(final Tessellator par1Tessellator,
			final float par2, final float par3, final float par4,
			final float par5, final float par6, final float par7) {
		float var8 = (particleAge + par2) / particleMaxAge * 32.0F;

		if (var8 < 0.0F) {
			var8 = 0.0F;
		}

		if (var8 > 1.0F) {
			var8 = 1.0F;
		}

		super.renderParticle(par1Tessellator, par2, par3, par4, par5, par6,
				par7);
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate() {
		prevPosX = posX;
		prevPosY = posY;
		prevPosZ = posZ;

		if (particleAge++ >= particleMaxAge) {
			setDead();
		}

		setParticleTextureIndex(baseSpellTextureIndex + 7 - particleAge * 8
				/ particleMaxAge);
		motionY += 0.004D;
		moveEntity(motionX, motionY, motionZ);

		if (posY == prevPosY) {
			motionX *= 1.1D;
			motionZ *= 1.1D;
		}

		motionX *= 0.9599999785423279D;
		motionY *= 0.9599999785423279D;
		motionZ *= 0.9599999785423279D;

		if (onGround) {
			motionX *= 0.699999988079071D;
			motionZ *= 0.699999988079071D;
		}
	}

	/**
	 * Sets the base spell texture index
	 */
	public void setBaseSpellTextureIndex(final int par1) {
		baseSpellTextureIndex = par1;
	}
}
