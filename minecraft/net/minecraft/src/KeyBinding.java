package net.minecraft.src;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class KeyBinding {
	public static List keybindArray = new ArrayList();
	public static IntHashMap hash = new IntHashMap();
	public String keyDescription;
	public int keyCode;

	/** because _303 wanted me to call it that(Caironater) */
	public boolean pressed;
	public int pressTime = 0;

	public static void onTick(final int par0) {
		final KeyBinding var1 = (KeyBinding) KeyBinding.hash.lookup(par0);

		if (var1 != null) {
			++var1.pressTime;
		}
	}

	public static void setKeyBindState(final int par0, final boolean par1) {
		final KeyBinding var2 = (KeyBinding) KeyBinding.hash.lookup(par0);

		if (var2 != null) {
			var2.pressed = par1;
		}
	}

	public static void unPressAllKeys() {
		final Iterator var0 = KeyBinding.keybindArray.iterator();

		while (var0.hasNext()) {
			final KeyBinding var1 = (KeyBinding) var0.next();
			var1.unpressKey();
		}
	}

	public static void resetKeyBindingArrayAndHash() {
		KeyBinding.hash.clearMap();
		final Iterator var0 = KeyBinding.keybindArray.iterator();

		while (var0.hasNext()) {
			final KeyBinding var1 = (KeyBinding) var0.next();
			KeyBinding.hash.addKey(var1.keyCode, var1);
		}
	}

	public KeyBinding(final String par1Str, final int par2) {
		keyDescription = par1Str;
		keyCode = par2;
		KeyBinding.keybindArray.add(this);
		KeyBinding.hash.addKey(par2, this);
	}

	public boolean isPressed() {
		if (pressTime == 0) {
			return false;
		} else {
			--pressTime;
			return true;
		}
	}

	private void unpressKey() {
		pressTime = 0;
		pressed = false;
	}
}
