package net.minecraft.src;

public abstract class EntityWaterMob extends EntityCreature implements IAnimals {
	public EntityWaterMob(final World par1World) {
		super(par1World);
	}

	@Override
	public boolean canBreatheUnderwater() {
		return true;
	}

	/**
	 * Checks if the entity's current position is a valid location to spawn this
	 * entity.
	 */
	@Override
	public boolean getCanSpawnHere() {
		return worldObj.checkNoEntityCollision(boundingBox);
	}

	/**
	 * Get number of ticks, at least during which the living entity will be
	 * silent.
	 */
	@Override
	public int getTalkInterval() {
		return 120;
	}

	/**
	 * Determines if an entity can be despawned, used on idle far away entities
	 */
	@Override
	protected boolean canDespawn() {
		return true;
	}

	/**
	 * Get the experience points the entity currently has.
	 */
	@Override
	protected int getExperiencePoints(final EntityPlayer par1EntityPlayer) {
		return 1 + worldObj.rand.nextInt(3);
	}

	/**
	 * Gets called every tick from main Entity class
	 */
	@Override
	public void onEntityUpdate() {
		int var1 = getAir();
		super.onEntityUpdate();

		if (isEntityAlive() && !isInsideOfMaterial(Material.water)) {
			--var1;
			setAir(var1);

			if (getAir() == -20) {
				setAir(0);
				attackEntityFrom(DamageSource.drown, 2);
			}
		} else {
			setAir(300);
		}
	}
}
