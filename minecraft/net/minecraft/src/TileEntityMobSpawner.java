package net.minecraft.src;

public class TileEntityMobSpawner extends TileEntity {
	private final MobSpawnerBaseLogic field_98050_a = new TileEntityMobSpawnerLogic(
			this);

	/**
	 * Reads a tile entity from NBT.
	 */
	@Override
	public void readFromNBT(final NBTTagCompound par1NBTTagCompound) {
		super.readFromNBT(par1NBTTagCompound);
		field_98050_a.readFromNBT(par1NBTTagCompound);
	}

	/**
	 * Writes a tile entity to NBT.
	 */
	@Override
	public void writeToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeToNBT(par1NBTTagCompound);
		field_98050_a.writeToNBT(par1NBTTagCompound);
	}

	/**
	 * Allows the entity to update its state. Overridden in most subclasses,
	 * e.g. the mob spawner uses this to count ticks and creates a new spawn
	 * inside its implementation.
	 */
	@Override
	public void updateEntity() {
		field_98050_a.updateSpawner();
		super.updateEntity();
	}

	/**
	 * Overriden in a sign to provide the text.
	 */
	@Override
	public Packet getDescriptionPacket() {
		final NBTTagCompound var1 = new NBTTagCompound();
		writeToNBT(var1);
		var1.removeTag("SpawnPotentials");
		return new Packet132TileEntityData(xCoord, yCoord, zCoord, 1, var1);
	}

	/**
	 * Called when a client event is received with the event number and
	 * argument, see World.sendClientEvent
	 */
	@Override
	public boolean receiveClientEvent(final int par1, final int par2) {
		return field_98050_a.setDelayToMin(par1) ? true : super
				.receiveClientEvent(par1, par2);
	}

	public MobSpawnerBaseLogic func_98049_a() {
		return field_98050_a;
	}
}
