package net.minecraft.src;

import org.lwjgl.input.Keyboard;

public class GuiScreenServerList extends GuiScreen {
	/** Needed a change as a local variable was conflicting on construct */
	private final GuiScreen guiScreen;

	/** Instance of ServerData. */
	private final ServerData theServerData;
	private GuiTextField serverTextField;

	public GuiScreenServerList(final GuiScreen par1GuiScreen,
			final ServerData par2ServerData) {
		guiScreen = par1GuiScreen;
		theServerData = par2ServerData;
	}

	/**
	 * Called from the main game loop to update the screen.
	 */
	@Override
	public void updateScreen() {
		serverTextField.updateCursorCounter();
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui() {
		final StringTranslate var1 = StringTranslate.getInstance();
		Keyboard.enableRepeatEvents(true);
		buttonList.clear();
		buttonList.add(new GuiButton(0, width / 2 - 100, height / 4 + 96 + 12,
				var1.translateKey("selectServer.select")));
		buttonList.add(new GuiButton(1, width / 2 - 100, height / 4 + 120 + 12,
				var1.translateKey("gui.cancel")));
		serverTextField = new GuiTextField(fontRenderer, width / 2 - 100, 116,
				200, 20);
		serverTextField.setMaxStringLength(128);
		serverTextField.setFocused(true);
		serverTextField.setText(mc.gameSettings.lastServer);
		((GuiButton) buttonList.get(0)).enabled = serverTextField.getText()
				.length() > 0
				&& serverTextField.getText().split(":").length > 0;
	}

	/**
	 * Called when the screen is unloaded. Used to disable keyboard repeat
	 * events
	 */
	@Override
	public void onGuiClosed() {
		Keyboard.enableRepeatEvents(false);
		mc.gameSettings.lastServer = serverTextField.getText();
		mc.gameSettings.saveOptions();
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	@Override
	protected void actionPerformed(final GuiButton par1GuiButton) {
		if (par1GuiButton.enabled) {
			if (par1GuiButton.id == 1) {
				guiScreen.confirmClicked(false, 0);
			} else if (par1GuiButton.id == 0) {
				theServerData.serverIP = serverTextField.getText();
				guiScreen.confirmClicked(true, 0);
			}
		}
	}

	/**
	 * Fired when a key is typed. This is the equivalent of
	 * KeyListener.keyTyped(KeyEvent e).
	 */
	@Override
	protected void keyTyped(final char par1, final int par2) {
		if (serverTextField.textboxKeyTyped(par1, par2)) {
			((GuiButton) buttonList.get(0)).enabled = serverTextField.getText()
					.length() > 0
					&& serverTextField.getText().split(":").length > 0;
		} else if (par2 == 28) {
			actionPerformed((GuiButton) buttonList.get(0));
		}
	}

	/**
	 * Called when the mouse is clicked.
	 */
	@Override
	protected void mouseClicked(final int par1, final int par2, final int par3) {
		super.mouseClicked(par1, par2, par3);
		serverTextField.mouseClicked(par1, par2, par3);
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(final int par1, final int par2, final float par3) {
		final StringTranslate var4 = StringTranslate.getInstance();
		drawDefaultBackground();
		drawCenteredString(fontRenderer,
				var4.translateKey("selectServer.direct"), width / 2,
				height / 4 - 60 + 20, 16777215);
		drawString(fontRenderer, var4.translateKey("addServer.enterIp"),
				width / 2 - 100, 100, 10526880);
		serverTextField.drawTextBox();
		super.drawScreen(par1, par2, par3);
	}
}
