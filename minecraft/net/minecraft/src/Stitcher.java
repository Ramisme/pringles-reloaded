package net.minecraft.src;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class Stitcher {
	private final Set setStitchHolders;
	private final List stitchSlots;
	private int currentWidth;
	private int currentHeight;
	private final int maxWidth;
	private final int maxHeight;
	private final boolean forcePowerOf2;

	/** Max size (width or height) of a single tile */
	private final int maxTileDimension;
	private Texture atlasTexture;
	private final String textureName;

	public Stitcher(final String par1Str, final int par2, final int par3,
			final boolean par4) {
		this(par1Str, par2, par3, par4, 0);
	}

	public Stitcher(final String par1, final int par2, final int par3,
			final boolean par4, final int par5) {
		setStitchHolders = new HashSet(256);
		stitchSlots = new ArrayList(256);
		currentWidth = 0;
		currentHeight = 0;
		textureName = par1;
		maxWidth = par2;
		maxHeight = par3;
		forcePowerOf2 = par4;
		maxTileDimension = par5;
	}

	public void addStitchHolder(final StitchHolder par1StitchHolder) {
		if (maxTileDimension > 0) {
			par1StitchHolder.setNewDimension(maxTileDimension);
		}

		setStitchHolders.add(par1StitchHolder);
	}

	public Texture getTexture() {
		if (forcePowerOf2) {
			currentWidth = getCeilPowerOf2(currentWidth);
			currentHeight = getCeilPowerOf2(currentHeight);
		}

		atlasTexture = TextureManager.instance().createEmptyTexture(
				textureName, 1, currentWidth, currentHeight, 6408);
		atlasTexture.fillRect(atlasTexture.getTextureRect(), -65536);
		final List var1 = getStichSlots();

		for (int var2 = 0; var2 < var1.size(); ++var2) {
			final StitchSlot var3 = (StitchSlot) var1.get(var2);
			final StitchHolder var4 = var3.getStitchHolder();
			atlasTexture.copyFrom(var3.getOriginX(), var3.getOriginY(),
					var4.func_98150_a(), var4.isRotated());
		}

		TextureManager.instance().registerTexture(textureName, atlasTexture);
		return atlasTexture;
	}

	public void doStitch() {
		final StitchHolder[] var1 = (StitchHolder[]) setStitchHolders
				.toArray(new StitchHolder[setStitchHolders.size()]);
		Arrays.sort(var1);
		atlasTexture = null;

		for (final StitchHolder element : var1) {
			final StitchHolder var3 = element;

			if (!allocateSlot(var3)) {
				throw new StitcherException(var3);
			}
		}
	}

	public List getStichSlots() {
		final ArrayList var1 = new ArrayList();
		final Iterator var2 = stitchSlots.iterator();

		while (var2.hasNext()) {
			final StitchSlot var3 = (StitchSlot) var2.next();
			var3.getAllStitchSlots(var1);
		}

		return var1;
	}

	/**
	 * Returns power of 2 >= the specified value
	 */
	private int getCeilPowerOf2(final int par1) {
		int var2 = par1 - 1;
		var2 |= var2 >> 1;
		var2 |= var2 >> 2;
		var2 |= var2 >> 4;
		var2 |= var2 >> 8;
		var2 |= var2 >> 16;
		return var2 + 1;
	}

	/**
	 * Attempts to find space for specified tile
	 */
	private boolean allocateSlot(final StitchHolder par1StitchHolder) {
		for (int var2 = 0; var2 < stitchSlots.size(); ++var2) {
			if (((StitchSlot) stitchSlots.get(var2))
					.func_94182_a(par1StitchHolder)) {
				return true;
			}

			par1StitchHolder.rotate();

			if (((StitchSlot) stitchSlots.get(var2))
					.func_94182_a(par1StitchHolder)) {
				return true;
			}

			par1StitchHolder.rotate();
		}

		return expandAndAllocateSlot(par1StitchHolder);
	}

	/**
	 * Expand stitched texture in order to make space for specified tile
	 */
	private boolean expandAndAllocateSlot(final StitchHolder par1StitchHolder) {
		final int var2 = Math.min(par1StitchHolder.getHeight(),
				par1StitchHolder.getWidth());
		final boolean var3 = currentWidth == 0 && currentHeight == 0;
		boolean var4;

		if (forcePowerOf2) {
			final int var5 = getCeilPowerOf2(currentWidth);
			final int var6 = getCeilPowerOf2(currentHeight);
			final int var7 = getCeilPowerOf2(currentWidth + var2);
			final int var8 = getCeilPowerOf2(currentHeight + var2);
			final boolean var9 = var7 <= maxWidth;
			final boolean var10 = var8 <= maxHeight;

			if (!var9 && !var10) {
				return false;
			}

			final int var11 = Math.max(par1StitchHolder.getHeight(),
					par1StitchHolder.getWidth());

			if (var3 && !var9
					&& getCeilPowerOf2(currentHeight + var11) > maxHeight) {
				return false;
			}

			final boolean var12 = var5 != var7;
			final boolean var13 = var6 != var8;
			if (var12 ^ var13) {
				if (var12 && var9) {
				} else {
				}
			} else if (var9 && var5 <= var6) {
			} else {
			}

			final int var14 = getCeilPowerOf2(currentWidth + var2);
			final int var15 = getCeilPowerOf2(currentHeight + var2);
			var4 = var14 <= var15;
		} else {
			final boolean var16 = currentWidth + var2 <= maxWidth;
			final boolean var18 = currentHeight + var2 <= maxHeight;

			if (!var16 && !var18) {
				return false;
			}

			var4 = (var3 || currentWidth <= currentHeight) && var16;
		}

		StitchSlot var17;

		if (var4) {
			if (par1StitchHolder.getWidth() > par1StitchHolder.getHeight()) {
				par1StitchHolder.rotate();
			}

			if (currentHeight == 0) {
				currentHeight = par1StitchHolder.getHeight();
			}

			var17 = new StitchSlot(currentWidth, 0,
					par1StitchHolder.getWidth(), currentHeight);
			currentWidth += par1StitchHolder.getWidth();
		} else {
			var17 = new StitchSlot(0, currentHeight, currentWidth,
					par1StitchHolder.getHeight());
			currentHeight += par1StitchHolder.getHeight();
		}

		var17.func_94182_a(par1StitchHolder);
		stitchSlots.add(var17);
		return true;
	}
}
