package net.minecraft.src;

public class EntityDragonPart extends Entity {
	/** The dragon entity this dragon part belongs to */
	public final IEntityMultiPart entityDragonObj;

	/** The name of the Dragon Part */
	public final String name;

	public EntityDragonPart(final IEntityMultiPart par1, final String par2,
			final float par3, final float par4) {
		super(par1.func_82194_d());
		setSize(par3, par4);
		entityDragonObj = par1;
		name = par2;
	}

	@Override
	protected void entityInit() {
	}

	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	@Override
	protected void readEntityFromNBT(final NBTTagCompound par1NBTTagCompound) {
	}

	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	@Override
	protected void writeEntityToNBT(final NBTTagCompound par1NBTTagCompound) {
	}

	/**
	 * Returns true if other Entities should be prevented from moving through
	 * this Entity.
	 */
	@Override
	public boolean canBeCollidedWith() {
		return true;
	}

	/**
	 * Called when the entity is attacked.
	 */
	@Override
	public boolean attackEntityFrom(final DamageSource par1DamageSource,
			final int par2) {
		return isEntityInvulnerable() ? false : entityDragonObj
				.attackEntityFromPart(this, par1DamageSource, par2);
	}

	/**
	 * Returns true if Entity argument is equal to this Entity
	 */
	@Override
	public boolean isEntityEqual(final Entity par1Entity) {
		return this == par1Entity || entityDragonObj == par1Entity;
	}
}
