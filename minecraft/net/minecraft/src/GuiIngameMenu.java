package net.minecraft.src;

public class GuiIngameMenu extends GuiScreen {
	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui() {
		buttonList.clear();
		final byte var1 = -16;
		buttonList.add(new GuiButton(1, width / 2 - 100, height / 4 + 120
				+ var1, StatCollector.translateToLocal("menu.returnToMenu")));

		if (!mc.isIntegratedServerRunning()) {
			((GuiButton) buttonList.get(0)).displayString = StatCollector
					.translateToLocal("menu.disconnect");
		}

		buttonList.add(new GuiButton(4, width / 2 - 100,
				height / 4 + 24 + var1, StatCollector
						.translateToLocal("menu.returnToGame")));
		buttonList.add(new GuiButton(0, width / 2 - 100,
				height / 4 + 96 + var1, 98, 20, StatCollector
						.translateToLocal("menu.options")));
		GuiButton var3;
		buttonList.add(var3 = new GuiButton(7, width / 2 + 2, height / 4 + 96
				+ var1, 98, 20, StatCollector
				.translateToLocal("menu.shareToLan")));
		buttonList.add(new GuiButton(5, width / 2 - 100,
				height / 4 + 48 + var1, 98, 20, StatCollector
						.translateToLocal("gui.achievements")));
		buttonList.add(new GuiButton(6, width / 2 + 2, height / 4 + 48 + var1,
				98, 20, StatCollector.translateToLocal("gui.stats")));
		var3.enabled = mc.isSingleplayer()
				&& !mc.getIntegratedServer().getPublic();
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	@Override
	protected void actionPerformed(final GuiButton par1GuiButton) {
		switch (par1GuiButton.id) {
		case 0:
			mc.displayGuiScreen(new GuiOptions(this, mc.gameSettings));
			break;

		case 1:
			par1GuiButton.enabled = false;
			mc.statFileWriter.readStat(StatList.leaveGameStat, 1);
			mc.theWorld.sendQuittingDisconnectingPacket();
			mc.loadWorld((WorldClient) null);
			mc.displayGuiScreen(new GuiMainMenu());

		case 2:
		case 3:
		default:
			break;

		case 4:
			mc.displayGuiScreen((GuiScreen) null);
			mc.setIngameFocus();
			mc.sndManager.resumeAllSounds();
			break;

		case 5:
			mc.displayGuiScreen(new GuiAchievements(mc.statFileWriter));
			break;

		case 6:
			mc.displayGuiScreen(new GuiStats(this, mc.statFileWriter));
			break;

		case 7:
			mc.displayGuiScreen(new GuiShareToLan(this));
		}
	}

	/**
	 * Called from the main game loop to update the screen.
	 */
	@Override
	public void updateScreen() {
		super.updateScreen();
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(final int par1, final int par2, final float par3) {
		drawDefaultBackground();
		drawCenteredString(fontRenderer, "Game menu", width / 2, 40, 16777215);
		super.drawScreen(par1, par2, par3);
	}
}
