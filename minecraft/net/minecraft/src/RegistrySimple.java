package net.minecraft.src;

import java.util.HashMap;
import java.util.Map;

public class RegistrySimple implements IRegistry {
	/** Objects registered on this registry. */
	protected final Map registryObjects = new HashMap();

	@Override
	public Object func_82594_a(final Object par1Obj) {
		return registryObjects.get(par1Obj);
	}

	/**
	 * Register an object on this registry.
	 */
	@Override
	public void putObject(final Object par1Obj, final Object par2Obj) {
		registryObjects.put(par1Obj, par2Obj);
	}
}
