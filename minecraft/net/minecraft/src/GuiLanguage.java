package net.minecraft.src;

public class GuiLanguage extends GuiScreen {
	/** This GUI's parent GUI. */
	protected GuiScreen parentGui;

	/**
	 * Timer used to update texture packs, decreases every tick and is reset to
	 * 20 and updates texture packs upon reaching 0.
	 */
	private int updateTimer = -1;

	/** This GUI's language list. */
	private GuiSlotLanguage languageList;

	/** For saving the user's language selection to disk. */
	private final GameSettings theGameSettings;

	/** This GUI's 'Done' button. */
	private GuiSmallButton doneButton;

	public GuiLanguage(final GuiScreen par1GuiScreen,
			final GameSettings par2GameSettings) {
		parentGui = par1GuiScreen;
		theGameSettings = par2GameSettings;
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui() {
		final StringTranslate var1 = StringTranslate.getInstance();
		buttonList.add(doneButton = new GuiSmallButton(6, width / 2 - 75,
				height - 38, var1.translateKey("gui.done")));
		languageList = new GuiSlotLanguage(this);
		languageList.registerScrollButtons(buttonList, 7, 8);
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	@Override
	protected void actionPerformed(final GuiButton par1GuiButton) {
		if (par1GuiButton.enabled) {
			switch (par1GuiButton.id) {
			case 5:
				break;

			case 6:
				mc.displayGuiScreen(parentGui);
				break;

			default:
				languageList.actionPerformed(par1GuiButton);
			}
		}
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(final int par1, final int par2, final float par3) {
		languageList.drawScreen(par1, par2, par3);

		if (updateTimer <= 0) {
			mc.texturePackList.updateAvaliableTexturePacks();
			updateTimer += 20;
		}

		final StringTranslate var4 = StringTranslate.getInstance();
		drawCenteredString(fontRenderer, var4.translateKey("options.language"),
				width / 2, 16, 16777215);
		drawCenteredString(fontRenderer,
				"(" + var4.translateKey("options.languageWarning") + ")",
				width / 2, height - 56, 8421504);
		super.drawScreen(par1, par2, par3);
	}

	/**
	 * Called from the main game loop to update the screen.
	 */
	@Override
	public void updateScreen() {
		super.updateScreen();
		--updateTimer;
	}

	/**
	 * Gets the relevant instance of GameSettings. Synthetic method for use in
	 * GuiSlotLanguage
	 */
	static GameSettings getGameSettings(final GuiLanguage par0GuiLanguage) {
		return par0GuiLanguage.theGameSettings;
	}

	/**
	 * Returns the private doneButton field.
	 */
	static GuiSmallButton getDoneButton(final GuiLanguage par0GuiLanguage) {
		return par0GuiLanguage.doneButton;
	}
}
