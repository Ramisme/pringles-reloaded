package net.minecraft.src;

class SlotArmor extends Slot {
	/**
	 * The armor type that can be placed on that slot, it uses the same values
	 * of armorType field on ItemArmor.
	 */
	final int armorType;

	/**
	 * The parent class of this clot, ContainerPlayer, SlotArmor is a Anon inner
	 * class.
	 */
	final ContainerPlayer parent;

	SlotArmor(final ContainerPlayer par1ContainerPlayer,
			final IInventory par2IInventory, final int par3, final int par4,
			final int par5, final int par6) {
		super(par2IInventory, par3, par4, par5);
		parent = par1ContainerPlayer;
		armorType = par6;
	}

	/**
	 * Returns the maximum stack size for a given slot (usually the same as
	 * getInventoryStackLimit(), but 1 in the case of armor slots)
	 */
	@Override
	public int getSlotStackLimit() {
		return 1;
	}

	/**
	 * Check if the stack is a valid item for this slot. Always true beside for
	 * the armor slots.
	 */
	@Override
	public boolean isItemValid(final ItemStack par1ItemStack) {
		return par1ItemStack == null ? false
				: par1ItemStack.getItem() instanceof ItemArmor ? ((ItemArmor) par1ItemStack
						.getItem()).armorType == armorType
						: par1ItemStack.getItem().itemID != Block.pumpkin.blockID
								&& par1ItemStack.getItem().itemID != Item.skull.itemID ? false
								: armorType == 0;
	}

	/**
	 * Returns the icon index on items.png that is used as background image of
	 * the slot.
	 */
	@Override
	public Icon getBackgroundIconIndex() {
		return ItemArmor.func_94602_b(armorType);
	}
}
