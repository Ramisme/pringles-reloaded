package net.minecraft.src;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketTimeoutException;

public class RConThreadClient extends RConThreadBase {
	/**
	 * True if the client has succefssfully logged into the RCon, otherwise
	 * false
	 */
	private boolean loggedIn = false;

	/** The client's Socket connection */
	private Socket clientSocket;

	/** A buffer for incoming Socket data */
	private final byte[] buffer = new byte[1460];

	/** The RCon password */
	private final String rconPassword;

	RConThreadClient(final IServer par1IServer, final Socket par2Socket) {
		super(par1IServer);
		clientSocket = par2Socket;

		try {
			clientSocket.setSoTimeout(0);
		} catch (final Exception var4) {
			running = false;
		}

		rconPassword = par1IServer.getStringProperty("rcon.password", "");
		logInfo("Rcon connection from: " + par2Socket.getInetAddress());
	}

	@Override
	public void run() {
		try {
			while (true) {
				if (!running) {
					break;
				}

				final BufferedInputStream var1 = new BufferedInputStream(
						clientSocket.getInputStream());
				final int var2 = var1.read(buffer, 0, 1460);

				if (10 > var2) {
					return;
				}

				final byte var3 = 0;
				final int var4 = RConUtils.getBytesAsLEInt(buffer, 0, var2);

				if (var4 == var2 - 4) {
					int var21 = var3 + 4;
					final int var5 = RConUtils.getBytesAsLEInt(buffer, var21,
							var2);
					var21 += 4;
					final int var6 = RConUtils.getRemainingBytesAsLEInt(buffer,
							var21);
					var21 += 4;

					switch (var6) {
					case 2:
						if (loggedIn) {
							final String var8 = RConUtils.getBytesAsString(
									buffer, var21, var2);

							try {
								sendMultipacketResponse(var5,
										server.executeCommand(var8));
							} catch (final Exception var16) {
								sendMultipacketResponse(var5,
										"Error executing: " + var8 + " ("
												+ var16.getMessage() + ")");
							}

							continue;
						}

						sendLoginFailedResponse();
						continue;

					case 3:
						final String var7 = RConUtils.getBytesAsString(buffer,
								var21, var2);
						if (0 != var7.length() && var7.equals(rconPassword)) {
							loggedIn = true;
							sendResponse(var5, 2, "");
							continue;
						}

						loggedIn = false;
						sendLoginFailedResponse();
						continue;

					default:
						sendMultipacketResponse(var5, String.format(
								"Unknown request %s",
								new Object[] { Integer.toHexString(var6) }));
						continue;
					}
				}
			}
		} catch (final SocketTimeoutException var17) {
		} catch (final IOException var18) {
		} catch (final Exception var19) {
			System.out.println(var19);
		} finally {
			this.closeSocket();
		}
	}

	/**
	 * Sends the given response message to the client
	 */
	private void sendResponse(final int par1, final int par2,
			final String par3Str) throws IOException {
		final ByteArrayOutputStream var4 = new ByteArrayOutputStream(1248);
		final DataOutputStream var5 = new DataOutputStream(var4);
		var5.writeInt(Integer.reverseBytes(par3Str.length() + 10));
		var5.writeInt(Integer.reverseBytes(par1));
		var5.writeInt(Integer.reverseBytes(par2));
		var5.writeBytes(par3Str);
		var5.write(0);
		var5.write(0);
		clientSocket.getOutputStream().write(var4.toByteArray());
	}

	/**
	 * Sends the standard RCon 'authorization failed' response packet
	 */
	private void sendLoginFailedResponse() throws IOException {
		sendResponse(-1, 2, "");
	}

	/**
	 * Splits the response message into individual packets and sends each one
	 */
	private void sendMultipacketResponse(final int par1, String par2Str)
			throws IOException {
		int var3 = par2Str.length();

		do {
			final int var4 = 4096 <= var3 ? 4096 : var3;
			sendResponse(par1, 0, par2Str.substring(0, var4));
			par2Str = par2Str.substring(var4);
			var3 = par2Str.length();
		} while (0 != var3);
	}

	/**
	 * Closes the client socket
	 */
	private void closeSocket() {
		if (null != clientSocket) {
			try {
				clientSocket.close();
			} catch (final IOException var2) {
				logWarning("IO: " + var2.getMessage());
			}

			clientSocket = null;
		}
	}
}
