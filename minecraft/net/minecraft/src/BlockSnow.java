package net.minecraft.src;

import java.util.Random;

public class BlockSnow extends Block {
	protected BlockSnow(final int par1) {
		super(par1, Material.snow);
		setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.125F, 1.0F);
		setTickRandomly(true);
		setCreativeTab(CreativeTabs.tabDecorations);
		setBlockBoundsForSnowDepth(0);
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		blockIcon = par1IconRegister.registerIcon("snow");
	}

	/**
	 * Returns a bounding box from the pool of bounding boxes (this means this
	 * box can change after the pool has been cleared to be reused)
	 */
	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(final World par1World,
			final int par2, final int par3, final int par4) {
		final int var5 = par1World.getBlockMetadata(par2, par3, par4) & 7;
		final float var6 = 0.125F;
		return AxisAlignedBB.getAABBPool().getAABB(par2 + minX, par3 + minY,
				par4 + minZ, par2 + maxX, par3 + var5 * var6, par4 + maxZ);
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	/**
	 * Sets the block's bounds for rendering it as an item
	 */
	@Override
	public void setBlockBoundsForItemRender() {
		setBlockBoundsForSnowDepth(0);
	}

	/**
	 * Updates the blocks bounds based on its current state. Args: world, x, y,
	 * z
	 */
	@Override
	public void setBlockBoundsBasedOnState(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		setBlockBoundsForSnowDepth(par1IBlockAccess.getBlockMetadata(par2,
				par3, par4));
	}

	/**
	 * calls setBlockBounds based on the depth of the snow. Int is any values
	 * 0x0-0x7, usually this blocks metadata.
	 */
	protected void setBlockBoundsForSnowDepth(final int par1) {
		final int var2 = par1 & 7;
		final float var3 = 2 * (1 + var2) / 16.0F;
		setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, var3, 1.0F);
	}

	/**
	 * Checks to see if its valid to put this block at the specified
	 * coordinates. Args: world, x, y, z
	 */
	@Override
	public boolean canPlaceBlockAt(final World par1World, final int par2,
			final int par3, final int par4) {
		final int var5 = par1World.getBlockId(par2, par3 - 1, par4);
		return var5 == 0 ? false
				: var5 == blockID
						&& (par1World.getBlockMetadata(par2, par3 - 1, par4) & 7) == 7 ? true
						: var5 != Block.leaves.blockID
								&& !Block.blocksList[var5].isOpaqueCube() ? false
								: par1World.getBlockMaterial(par2, par3 - 1,
										par4).blocksMovement();
	}

	/**
	 * Lets the block know when one of its neighbor changes. Doesn't know which
	 * neighbor changed (coordinates passed are their own) Args: x, y, z,
	 * neighbor blockID
	 */
	@Override
	public void onNeighborBlockChange(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		canSnowStay(par1World, par2, par3, par4);
	}

	/**
	 * Checks if this snow block can stay at this location.
	 */
	private boolean canSnowStay(final World par1World, final int par2,
			final int par3, final int par4) {
		if (!canPlaceBlockAt(par1World, par2, par3, par4)) {
			dropBlockAsItem(par1World, par2, par3, par4,
					par1World.getBlockMetadata(par2, par3, par4), 0);
			par1World.setBlockToAir(par2, par3, par4);
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Called when the player destroys a block with an item that can harvest it.
	 * (i, j, k) are the coordinates of the block and l is the block's
	 * subtype/damage.
	 */
	@Override
	public void harvestBlock(final World par1World,
			final EntityPlayer par2EntityPlayer, final int par3,
			final int par4, final int par5, final int par6) {
		final int var7 = Item.snowball.itemID;
		final int var8 = par6 & 7;
		dropBlockAsItem_do(par1World, par3, par4, par5, new ItemStack(var7,
				var8 + 1, 0));
		par1World.setBlockToAir(par3, par4, par5);
		par2EntityPlayer.addStat(StatList.mineBlockStatArray[blockID], 1);
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return Item.snowball.itemID;
	}

	/**
	 * Returns the quantity of items to drop on block destruction.
	 */
	@Override
	public int quantityDropped(final Random par1Random) {
		return 0;
	}

	/**
	 * Ticks the block if it's been scheduled
	 */
	@Override
	public void updateTick(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		if (par1World.getSavedLightValue(EnumSkyBlock.Block, par2, par3, par4) > 11) {
			dropBlockAsItem(par1World, par2, par3, par4,
					par1World.getBlockMetadata(par2, par3, par4), 0);
			par1World.setBlockToAir(par2, par3, par4);
		}
	}

	/**
	 * Returns true if the given side of this block type should be rendered, if
	 * the adjacent block is at the given coordinates. Args: blockAccess, x, y,
	 * z, side
	 */
	@Override
	public boolean shouldSideBeRendered(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		return par5 == 1 ? true : super.shouldSideBeRendered(par1IBlockAccess,
				par2, par3, par4, par5);
	}
}
