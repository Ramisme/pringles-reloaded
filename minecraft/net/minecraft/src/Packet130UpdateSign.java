package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet130UpdateSign extends Packet {
	public int xPosition;
	public int yPosition;
	public int zPosition;
	public String[] signLines;

	public Packet130UpdateSign() {
		isChunkDataPacket = true;
	}

	public Packet130UpdateSign(final int par1, final int par2, final int par3,
			final String[] par4ArrayOfStr) {
		isChunkDataPacket = true;
		xPosition = par1;
		yPosition = par2;
		zPosition = par3;
		signLines = new String[] { par4ArrayOfStr[0], par4ArrayOfStr[1],
				par4ArrayOfStr[2], par4ArrayOfStr[3] };
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		xPosition = par1DataInputStream.readInt();
		yPosition = par1DataInputStream.readShort();
		zPosition = par1DataInputStream.readInt();
		signLines = new String[4];

		for (int var2 = 0; var2 < 4; ++var2) {
			signLines[var2] = Packet.readString(par1DataInputStream, 15);
		}
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		par1DataOutputStream.writeInt(xPosition);
		par1DataOutputStream.writeShort(yPosition);
		par1DataOutputStream.writeInt(zPosition);

		for (int var2 = 0; var2 < 4; ++var2) {
			Packet.writeString(signLines[var2], par1DataOutputStream);
		}
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleUpdateSign(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		int var1 = 0;

		for (int var2 = 0; var2 < 4; ++var2) {
			var1 += signLines[var2].length();
		}

		return var1;
	}
}
