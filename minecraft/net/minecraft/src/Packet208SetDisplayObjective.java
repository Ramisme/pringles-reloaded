package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet208SetDisplayObjective extends Packet {
	/** The position of the scoreboard. 0 = list, 1 = sidebar, 2 = belowName. */
	public int scoreboardPosition;

	/** The unique name for the scoreboard to be displayed. */
	public String scoreName;

	public Packet208SetDisplayObjective() {
	}

	public Packet208SetDisplayObjective(final int par1,
			final ScoreObjective par2ScoreObjective) {
		scoreboardPosition = par1;

		if (par2ScoreObjective == null) {
			scoreName = "";
		} else {
			scoreName = par2ScoreObjective.getName();
		}
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		scoreboardPosition = par1DataInputStream.readByte();
		scoreName = Packet.readString(par1DataInputStream, 16);
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		par1DataOutputStream.writeByte(scoreboardPosition);
		Packet.writeString(scoreName, par1DataOutputStream);
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleSetDisplayObjective(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 3 + scoreName.length();
	}
}
