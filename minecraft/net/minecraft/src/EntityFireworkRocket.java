package net.minecraft.src;

public class EntityFireworkRocket extends Entity {
	/** The age of the firework in ticks. */
	private int fireworkAge;

	/**
	 * The lifetime of the firework in ticks. When the age reaches the lifetime
	 * the firework explodes.
	 */
	private int lifetime;

	public EntityFireworkRocket(final World par1World) {
		super(par1World);
		setSize(0.25F, 0.25F);
	}

	@Override
	protected void entityInit() {
		dataWatcher.addObjectByDataType(8, 5);
	}

	/**
	 * Checks if the entity is in range to render by using the past in distance
	 * and comparing it to its average edge length * 64 * renderDistanceWeight
	 * Args: distance
	 */
	@Override
	public boolean isInRangeToRenderDist(final double par1) {
		return par1 < 4096.0D;
	}

	public EntityFireworkRocket(final World par1World, final double par2,
			final double par4, final double par6, final ItemStack par8ItemStack) {
		super(par1World);
		fireworkAge = 0;
		setSize(0.25F, 0.25F);
		setPosition(par2, par4, par6);
		yOffset = 0.0F;
		int var9 = 1;

		if (par8ItemStack != null && par8ItemStack.hasTagCompound()) {
			dataWatcher.updateObject(8, par8ItemStack);
			final NBTTagCompound var10 = par8ItemStack.getTagCompound();
			final NBTTagCompound var11 = var10.getCompoundTag("Fireworks");

			if (var11 != null) {
				var9 += var11.getByte("Flight");
			}
		}

		motionX = rand.nextGaussian() * 0.001D;
		motionZ = rand.nextGaussian() * 0.001D;
		motionY = 0.05D;
		lifetime = 10 * var9 + rand.nextInt(6) + rand.nextInt(7);
	}

	/**
	 * Sets the velocity to the args. Args: x, y, z
	 */
	@Override
	public void setVelocity(final double par1, final double par3,
			final double par5) {
		motionX = par1;
		motionY = par3;
		motionZ = par5;

		if (prevRotationPitch == 0.0F && prevRotationYaw == 0.0F) {
			final float var7 = MathHelper
					.sqrt_double(par1 * par1 + par5 * par5);
			prevRotationYaw = rotationYaw = (float) (Math.atan2(par1, par5) * 180.0D / Math.PI);
			prevRotationPitch = rotationPitch = (float) (Math.atan2(par3, var7) * 180.0D / Math.PI);
		}
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate() {
		lastTickPosX = posX;
		lastTickPosY = posY;
		lastTickPosZ = posZ;
		super.onUpdate();
		motionX *= 1.15D;
		motionZ *= 1.15D;
		motionY += 0.04D;
		moveEntity(motionX, motionY, motionZ);
		final float var1 = MathHelper.sqrt_double(motionX * motionX + motionZ
				* motionZ);
		rotationYaw = (float) (Math.atan2(motionX, motionZ) * 180.0D / Math.PI);

		for (rotationPitch = (float) (Math.atan2(motionY, var1) * 180.0D / Math.PI); rotationPitch
				- prevRotationPitch < -180.0F; prevRotationPitch -= 360.0F) {
			;
		}

		while (rotationPitch - prevRotationPitch >= 180.0F) {
			prevRotationPitch += 360.0F;
		}

		while (rotationYaw - prevRotationYaw < -180.0F) {
			prevRotationYaw -= 360.0F;
		}

		while (rotationYaw - prevRotationYaw >= 180.0F) {
			prevRotationYaw += 360.0F;
		}

		rotationPitch = prevRotationPitch + (rotationPitch - prevRotationPitch)
				* 0.2F;
		rotationYaw = prevRotationYaw + (rotationYaw - prevRotationYaw) * 0.2F;

		if (fireworkAge == 0) {
			worldObj.playSoundAtEntity(this, "fireworks.launch", 3.0F, 1.0F);
		}

		++fireworkAge;

		if (worldObj.isRemote && fireworkAge % 2 < 2) {
			worldObj.spawnParticle("fireworksSpark", posX, posY - 0.3D, posZ,
					rand.nextGaussian() * 0.05D, -motionY * 0.5D,
					rand.nextGaussian() * 0.05D);
		}

		if (!worldObj.isRemote && fireworkAge > lifetime) {
			worldObj.setEntityState(this, (byte) 17);
			setDead();
		}
	}

	@Override
	public void handleHealthUpdate(final byte par1) {
		if (par1 == 17 && worldObj.isRemote) {
			final ItemStack var2 = dataWatcher.getWatchableObjectItemStack(8);
			NBTTagCompound var3 = null;

			if (var2 != null && var2.hasTagCompound()) {
				var3 = var2.getTagCompound().getCompoundTag("Fireworks");
			}

			worldObj.func_92088_a(posX, posY, posZ, motionX, motionY, motionZ,
					var3);
		}

		super.handleHealthUpdate(par1);
	}

	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	@Override
	public void writeEntityToNBT(final NBTTagCompound par1NBTTagCompound) {
		par1NBTTagCompound.setInteger("Life", fireworkAge);
		par1NBTTagCompound.setInteger("LifeTime", lifetime);
		final ItemStack var2 = dataWatcher.getWatchableObjectItemStack(8);

		if (var2 != null) {
			final NBTTagCompound var3 = new NBTTagCompound();
			var2.writeToNBT(var3);
			par1NBTTagCompound.setCompoundTag("FireworksItem", var3);
		}
	}

	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	@Override
	public void readEntityFromNBT(final NBTTagCompound par1NBTTagCompound) {
		fireworkAge = par1NBTTagCompound.getInteger("Life");
		lifetime = par1NBTTagCompound.getInteger("LifeTime");
		final NBTTagCompound var2 = par1NBTTagCompound
				.getCompoundTag("FireworksItem");

		if (var2 != null) {
			final ItemStack var3 = ItemStack.loadItemStackFromNBT(var2);

			if (var3 != null) {
				dataWatcher.updateObject(8, var3);
			}
		}
	}

	@Override
	public float getShadowSize() {
		return 0.0F;
	}

	/**
	 * Gets how bright this entity is.
	 */
	@Override
	public float getBrightness(final float par1) {
		return super.getBrightness(par1);
	}

	@Override
	public int getBrightnessForRender(final float par1) {
		return super.getBrightnessForRender(par1);
	}

	/**
	 * If returns false, the item will not inflict any damage against entities.
	 */
	@Override
	public boolean canAttackWithItem() {
		return false;
	}
}
