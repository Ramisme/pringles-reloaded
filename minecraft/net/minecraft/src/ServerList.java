package net.minecraft.src;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.minecraft.client.Minecraft;

public class ServerList {
	/** The Minecraft instance. */
	private final Minecraft mc;

	/** List of ServerData instances. */
	private final List servers = new ArrayList();

	public ServerList(final Minecraft par1Minecraft) {
		mc = par1Minecraft;
		loadServerList();
	}

	/**
	 * Loads a list of servers from servers.dat, by running
	 * ServerData.getServerDataFromNBTCompound on each NBT compound found in the
	 * "servers" tag list.
	 */
	public void loadServerList() {
		try {
			final NBTTagCompound var1 = CompressedStreamTools.read(new File(
					mc.mcDataDir, "servers.dat"));
			final NBTTagList var2 = var1.getTagList("servers");
			servers.clear();

			for (int var3 = 0; var3 < var2.tagCount(); ++var3) {
				servers.add(ServerData
						.getServerDataFromNBTCompound((NBTTagCompound) var2
								.tagAt(var3)));
			}
		} catch (final Exception var4) {
			var4.printStackTrace();
		}
	}

	/**
	 * Runs getNBTCompound on each ServerData instance, puts everything into a
	 * "servers" NBT list and writes it to servers.dat.
	 */
	public void saveServerList() {
		try {
			final NBTTagList var1 = new NBTTagList();
			final Iterator var2 = servers.iterator();

			while (var2.hasNext()) {
				final ServerData var3 = (ServerData) var2.next();
				var1.appendTag(var3.getNBTCompound());
			}

			final NBTTagCompound var5 = new NBTTagCompound();
			var5.setTag("servers", var1);
			CompressedStreamTools.safeWrite(var5, new File(mc.mcDataDir,
					"servers.dat"));
		} catch (final Exception var4) {
			var4.printStackTrace();
		}
	}

	/**
	 * Gets the ServerData instance stored for the given index in the list.
	 */
	public ServerData getServerData(final int par1) {
		return (ServerData) servers.get(par1);
	}

	/**
	 * Removes the ServerData instance stored for the given index in the list.
	 */
	public void removeServerData(final int par1) {
		servers.remove(par1);
	}

	/**
	 * Adds the given ServerData instance to the list.
	 */
	public void addServerData(final ServerData par1ServerData) {
		servers.add(par1ServerData);
	}

	/**
	 * Counts the number of ServerData instances in the list.
	 */
	public int countServers() {
		return servers.size();
	}

	/**
	 * Takes two list indexes, and swaps their order around.
	 */
	public void swapServers(final int par1, final int par2) {
		final ServerData var3 = getServerData(par1);
		servers.set(par1, getServerData(par2));
		servers.set(par2, var3);
		saveServerList();
	}

	/**
	 * Sets the given index in the list to the given ServerData instance.
	 */
	public void setServer(final int par1, final ServerData par2ServerData) {
		servers.set(par1, par2ServerData);
	}

	public static void func_78852_b(final ServerData par0ServerData) {
		final ServerList var1 = new ServerList(Minecraft.getMinecraft());
		var1.loadServerList();

		for (int var2 = 0; var2 < var1.countServers(); ++var2) {
			final ServerData var3 = var1.getServerData(var2);

			if (var3.serverName.equals(par0ServerData.serverName)
					&& var3.serverIP.equals(par0ServerData.serverIP)) {
				var1.setServer(var2, par0ServerData);
				break;
			}
		}

		var1.saveServerList();
	}
}
