package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet250CustomPayload extends Packet {
	/** Name of the 'channel' used to send data */
	public String channel;

	/** Length of the data to be read */
	public int length;

	/** Any data */
	public byte[] data;

	public Packet250CustomPayload() {
	}

	public Packet250CustomPayload(final String par1Str,
			final byte[] par2ArrayOfByte) {
		channel = par1Str;
		data = par2ArrayOfByte;

		if (par2ArrayOfByte != null) {
			length = par2ArrayOfByte.length;

			if (length > 32767) {
				throw new IllegalArgumentException(
						"Payload may not be larger than 32k");
			}
		}
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		channel = Packet.readString(par1DataInputStream, 20);
		length = par1DataInputStream.readShort();

		if (length > 0 && length < 32767) {
			data = new byte[length];
			par1DataInputStream.readFully(data);
		}
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		Packet.writeString(channel, par1DataOutputStream);
		par1DataOutputStream.writeShort((short) length);

		if (data != null) {
			par1DataOutputStream.write(data);
		}
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleCustomPayload(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 2 + channel.length() * 2 + 2 + length;
	}
}
