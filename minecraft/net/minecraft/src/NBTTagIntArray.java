package net.minecraft.src;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Arrays;

public class NBTTagIntArray extends NBTBase {
	/** The array of saved integers */
	public int[] intArray;

	public NBTTagIntArray(final String par1Str) {
		super(par1Str);
	}

	public NBTTagIntArray(final String par1Str, final int[] par2ArrayOfInteger) {
		super(par1Str);
		intArray = par2ArrayOfInteger;
	}

	/**
	 * Write the actual data contents of the tag, implemented in NBT extension
	 * classes
	 */
	@Override
	void write(final DataOutput par1DataOutput) throws IOException {
		par1DataOutput.writeInt(intArray.length);

		for (final int element : intArray) {
			par1DataOutput.writeInt(element);
		}
	}

	/**
	 * Read the actual data contents of the tag, implemented in NBT extension
	 * classes
	 */
	@Override
	void load(final DataInput par1DataInput) throws IOException {
		final int var2 = par1DataInput.readInt();
		intArray = new int[var2];

		for (int var3 = 0; var3 < var2; ++var3) {
			intArray[var3] = par1DataInput.readInt();
		}
	}

	/**
	 * Gets the type byte for the tag.
	 */
	@Override
	public byte getId() {
		return (byte) 11;
	}

	@Override
	public String toString() {
		return "[" + intArray.length + " bytes]";
	}

	/**
	 * Creates a clone of the tag.
	 */
	@Override
	public NBTBase copy() {
		final int[] var1 = new int[intArray.length];
		System.arraycopy(intArray, 0, var1, 0, intArray.length);
		return new NBTTagIntArray(getName(), var1);
	}

	@Override
	public boolean equals(final Object par1Obj) {
		if (!super.equals(par1Obj)) {
			return false;
		} else {
			final NBTTagIntArray var2 = (NBTTagIntArray) par1Obj;
			return intArray == null && var2.intArray == null
					|| intArray != null
					&& Arrays.equals(intArray, var2.intArray);
		}
	}

	@Override
	public int hashCode() {
		return super.hashCode() ^ Arrays.hashCode(intArray);
	}
}
