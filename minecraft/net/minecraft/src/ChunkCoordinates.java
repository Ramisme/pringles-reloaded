package net.minecraft.src;

public class ChunkCoordinates implements Comparable {
	public int posX;

	/** the y coordinate */
	public int posY;

	/** the z coordinate */
	public int posZ;

	public ChunkCoordinates() {
	}

	public ChunkCoordinates(final int par1, final int par2, final int par3) {
		posX = par1;
		posY = par2;
		posZ = par3;
	}

	public ChunkCoordinates(final ChunkCoordinates par1ChunkCoordinates) {
		posX = par1ChunkCoordinates.posX;
		posY = par1ChunkCoordinates.posY;
		posZ = par1ChunkCoordinates.posZ;
	}

	@Override
	public boolean equals(final Object par1Obj) {
		if (!(par1Obj instanceof ChunkCoordinates)) {
			return false;
		} else {
			final ChunkCoordinates var2 = (ChunkCoordinates) par1Obj;
			return posX == var2.posX && posY == var2.posY && posZ == var2.posZ;
		}
	}

	@Override
	public int hashCode() {
		return posX + posZ << 8 + posY << 16;
	}

	/**
	 * Compare the coordinate with another coordinate
	 */
	public int compareChunkCoordinate(
			final ChunkCoordinates par1ChunkCoordinates) {
		return posY == par1ChunkCoordinates.posY ? posZ == par1ChunkCoordinates.posZ ? posX
				- par1ChunkCoordinates.posX
				: posZ - par1ChunkCoordinates.posZ
				: posY - par1ChunkCoordinates.posY;
	}

	public void set(final int par1, final int par2, final int par3) {
		posX = par1;
		posY = par2;
		posZ = par3;
	}

	/**
	 * Returns the squared distance between this coordinates and the coordinates
	 * given as argument.
	 */
	public float getDistanceSquared(final int par1, final int par2,
			final int par3) {
		final int var4 = posX - par1;
		final int var5 = posY - par2;
		final int var6 = posZ - par3;
		return var4 * var4 + var5 * var5 + var6 * var6;
	}

	/**
	 * Return the squared distance between this coordinates and the
	 * ChunkCoordinates given as argument.
	 */
	public float getDistanceSquaredToChunkCoordinates(
			final ChunkCoordinates par1ChunkCoordinates) {
		return getDistanceSquared(par1ChunkCoordinates.posX,
				par1ChunkCoordinates.posY, par1ChunkCoordinates.posZ);
	}

	@Override
	public int compareTo(final Object par1Obj) {
		return compareChunkCoordinate((ChunkCoordinates) par1Obj);
	}
}
