package net.minecraft.src;

public class BlockLeavesBase extends Block {
	/**
	 * Used to determine how to display leaves based on the graphics level. May
	 * also be used in rendering for transparency, not sure.
	 */
	protected boolean graphicsLevel;

	protected BlockLeavesBase(final int par1, final Material par2Material,
			final boolean par3) {
		super(par1, par2Material);
		graphicsLevel = par3;
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * Returns true if the given side of this block type should be rendered, if
	 * the adjacent block is at the given coordinates. Args: blockAccess, x, y,
	 * z, side
	 */
	@Override
	public boolean shouldSideBeRendered(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		final int var6 = par1IBlockAccess.getBlockId(par2, par3, par4);
		return !graphicsLevel && var6 == blockID ? false : super
				.shouldSideBeRendered(par1IBlockAccess, par2, par3, par4, par5);
	}
}
