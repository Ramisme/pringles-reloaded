package net.minecraft.src;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

class ThreadConnectToOnlineServer extends Thread {
	final McoServer field_96597_a;

	final GuiSlotOnlineServerList field_96596_b;

	ThreadConnectToOnlineServer(
			final GuiSlotOnlineServerList par1GuiSlotOnlineServerList,
			final McoServer par2McoServer) {
		field_96596_b = par1GuiSlotOnlineServerList;
		field_96597_a = par2McoServer;
	}

	@Override
	public void run() {
		boolean var27 = false;
		label194: {
			label195: {
				label196: {
					label197: {
						label198: {
							try {
								var27 = true;

								if (!field_96597_a.field_96411_l) {
									field_96597_a.field_96411_l = true;
									field_96597_a.field_96412_m = -2L;
									field_96597_a.field_96414_k = "";
									GuiScreenOnlineServers.func_101014_j();
									final long var1 = System.nanoTime();
									GuiScreenOnlineServers.func_101002_a(
											field_96596_b.field_96294_a,
											field_96597_a);
									final long var3 = System.nanoTime();
									field_96597_a.field_96412_m = (var3 - var1) / 1000000L;
									var27 = false;
								} else if (field_96597_a.field_102022_m) {
									field_96597_a.field_102022_m = false;
									GuiScreenOnlineServers.func_101002_a(
											field_96596_b.field_96294_a,
											field_96597_a);
									var27 = false;
								} else {
									var27 = false;
								}

								break label194;
							} catch (final UnknownHostException var35) {
								field_96597_a.field_96412_m = -1L;
								var27 = false;
								break label195;
							} catch (final SocketTimeoutException var36) {
								field_96597_a.field_96412_m = -1L;
								var27 = false;
								break label196;
							} catch (final ConnectException var37) {
								field_96597_a.field_96412_m = -1L;
								var27 = false;
								break label198;
							} catch (final IOException var38) {
								field_96597_a.field_96412_m = -1L;
								var27 = false;
							} catch (final Exception var39) {
								field_96597_a.field_96412_m = -1L;
								var27 = false;
								break label197;
							} finally {
								if (var27) {
									synchronized (GuiScreenOnlineServers
											.func_101007_h()) {
										GuiScreenOnlineServers.func_101013_k();
									}
								}
							}

							synchronized (GuiScreenOnlineServers
									.func_101007_h()) {
								GuiScreenOnlineServers.func_101013_k();
								return;
							}
						}

						synchronized (GuiScreenOnlineServers.func_101007_h()) {
							GuiScreenOnlineServers.func_101013_k();
							return;
						}
					}

					synchronized (GuiScreenOnlineServers.func_101007_h()) {
						GuiScreenOnlineServers.func_101013_k();
						return;
					}
				}

				synchronized (GuiScreenOnlineServers.func_101007_h()) {
					GuiScreenOnlineServers.func_101013_k();
					return;
				}
			}

			synchronized (GuiScreenOnlineServers.func_101007_h()) {
				GuiScreenOnlineServers.func_101013_k();
				return;
			}
		}

		synchronized (GuiScreenOnlineServers.func_101007_h()) {
			GuiScreenOnlineServers.func_101013_k();
		}
	}
}
