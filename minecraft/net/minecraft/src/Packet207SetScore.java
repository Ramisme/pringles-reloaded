package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet207SetScore extends Packet {
	/** An unique name to be displayed in the list. */
	public String itemName = "";

	/**
	 * The unique name for the scoreboard to be updated. Only sent when
	 * updateOrRemove does not equal 1.
	 */
	public String scoreName = "";

	/**
	 * The score to be displayed next to the entry. Only sent when Update/Remove
	 * does not equal 1.
	 */
	public int value = 0;

	/** 0 to create/update an item. 1 to remove an item. */
	public int updateOrRemove = 0;

	public Packet207SetScore() {
	}

	public Packet207SetScore(final Score par1, final int par2) {
		itemName = par1.func_96653_e();
		scoreName = par1.func_96645_d().getName();
		value = par1.func_96652_c();
		updateOrRemove = par2;
	}

	public Packet207SetScore(final String par1) {
		itemName = par1;
		scoreName = "";
		value = 0;
		updateOrRemove = 1;
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		itemName = Packet.readString(par1DataInputStream, 16);
		updateOrRemove = par1DataInputStream.readByte();

		if (updateOrRemove != 1) {
			scoreName = Packet.readString(par1DataInputStream, 16);
			value = par1DataInputStream.readInt();
		}
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		Packet.writeString(itemName, par1DataOutputStream);
		par1DataOutputStream.writeByte(updateOrRemove);

		if (updateOrRemove != 1) {
			Packet.writeString(scoreName, par1DataOutputStream);
			par1DataOutputStream.writeInt(value);
		}
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleSetScore(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 2 + itemName.length() + 2 + scoreName.length() + 4 + 1;
	}
}
