package net.minecraft.src;

public class GuiProgress extends GuiScreen implements IProgressUpdate {
	private String progressMessage = "";
	private String workingMessage = "";
	private int currentProgress = 0;
	private boolean noMoreProgress;

	/**
	 * "Saving level", or the loading,or downloading equivelent
	 */
	@Override
	public void displayProgressMessage(final String par1Str) {
		resetProgressAndMessage(par1Str);
	}

	/**
	 * this string, followed by "working..." and then the "% complete" are the 3
	 * lines shown. This resets progress to 0, and the WorkingString to
	 * "working...".
	 */
	@Override
	public void resetProgressAndMessage(final String par1Str) {
		progressMessage = par1Str;
		resetProgresAndWorkingMessage("Working...");
	}

	/**
	 * This is called with "Working..." by resetProgressAndMessage
	 */
	@Override
	public void resetProgresAndWorkingMessage(final String par1Str) {
		workingMessage = par1Str;
		setLoadingProgress(0);
	}

	/**
	 * Updates the progress bar on the loading screen to the specified amount.
	 * Args: loadProgress
	 */
	@Override
	public void setLoadingProgress(final int par1) {
		currentProgress = par1;
	}

	/**
	 * called when there is no more progress to be had, both on completion and
	 * failure
	 */
	@Override
	public void onNoMoreProgress() {
		noMoreProgress = true;
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(final int par1, final int par2, final float par3) {
		if (noMoreProgress) {
			mc.displayGuiScreen((GuiScreen) null);
		} else {
			drawDefaultBackground();
			drawCenteredString(fontRenderer, progressMessage, width / 2, 70,
					16777215);
			drawCenteredString(fontRenderer, workingMessage + " "
					+ currentProgress + "%", width / 2, 90, 16777215);
			super.drawScreen(par1, par2, par3);
		}
	}
}
