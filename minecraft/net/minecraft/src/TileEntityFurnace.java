package net.minecraft.src;

public class TileEntityFurnace extends TileEntity implements ISidedInventory {
	private static final int[] field_102010_d = new int[] { 0 };
	private static final int[] field_102011_e = new int[] { 2, 1 };
	private static final int[] field_102009_f = new int[] { 1 };

	/**
	 * The ItemStacks that hold the items currently being used in the furnace
	 */
	private ItemStack[] furnaceItemStacks = new ItemStack[3];

	/** The number of ticks that the furnace will keep burning */
	public int furnaceBurnTime = 0;

	/**
	 * The number of ticks that a fresh copy of the currently-burning item would
	 * keep the furnace burning for
	 */
	public int currentItemBurnTime = 0;

	/** The number of ticks that the current item has been cooking for */
	public int furnaceCookTime = 0;
	private String field_94130_e;

	/**
	 * Returns the number of slots in the inventory.
	 */
	@Override
	public int getSizeInventory() {
		return furnaceItemStacks.length;
	}

	/**
	 * Returns the stack in slot i
	 */
	@Override
	public ItemStack getStackInSlot(final int par1) {
		return furnaceItemStacks[par1];
	}

	/**
	 * Removes from an inventory slot (first arg) up to a specified number
	 * (second arg) of items and returns them in a new stack.
	 */
	@Override
	public ItemStack decrStackSize(final int par1, final int par2) {
		if (furnaceItemStacks[par1] != null) {
			ItemStack var3;

			if (furnaceItemStacks[par1].stackSize <= par2) {
				var3 = furnaceItemStacks[par1];
				furnaceItemStacks[par1] = null;
				return var3;
			} else {
				var3 = furnaceItemStacks[par1].splitStack(par2);

				if (furnaceItemStacks[par1].stackSize == 0) {
					furnaceItemStacks[par1] = null;
				}

				return var3;
			}
		} else {
			return null;
		}
	}

	/**
	 * When some containers are closed they call this on each slot, then drop
	 * whatever it returns as an EntityItem - like when you close a workbench
	 * GUI.
	 */
	@Override
	public ItemStack getStackInSlotOnClosing(final int par1) {
		if (furnaceItemStacks[par1] != null) {
			final ItemStack var2 = furnaceItemStacks[par1];
			furnaceItemStacks[par1] = null;
			return var2;
		} else {
			return null;
		}
	}

	/**
	 * Sets the given item stack to the specified slot in the inventory (can be
	 * crafting or armor sections).
	 */
	@Override
	public void setInventorySlotContents(final int par1,
			final ItemStack par2ItemStack) {
		furnaceItemStacks[par1] = par2ItemStack;

		if (par2ItemStack != null
				&& par2ItemStack.stackSize > getInventoryStackLimit()) {
			par2ItemStack.stackSize = getInventoryStackLimit();
		}
	}

	/**
	 * Returns the name of the inventory.
	 */
	@Override
	public String getInvName() {
		return isInvNameLocalized() ? field_94130_e : "container.furnace";
	}

	/**
	 * If this returns false, the inventory name will be used as an unlocalized
	 * name, and translated into the player's language. Otherwise it will be
	 * used directly.
	 */
	@Override
	public boolean isInvNameLocalized() {
		return field_94130_e != null && field_94130_e.length() > 0;
	}

	public void func_94129_a(final String par1Str) {
		field_94130_e = par1Str;
	}

	/**
	 * Reads a tile entity from NBT.
	 */
	@Override
	public void readFromNBT(final NBTTagCompound par1NBTTagCompound) {
		super.readFromNBT(par1NBTTagCompound);
		final NBTTagList var2 = par1NBTTagCompound.getTagList("Items");
		furnaceItemStacks = new ItemStack[getSizeInventory()];

		for (int var3 = 0; var3 < var2.tagCount(); ++var3) {
			final NBTTagCompound var4 = (NBTTagCompound) var2.tagAt(var3);
			final byte var5 = var4.getByte("Slot");

			if (var5 >= 0 && var5 < furnaceItemStacks.length) {
				furnaceItemStacks[var5] = ItemStack.loadItemStackFromNBT(var4);
			}
		}

		furnaceBurnTime = par1NBTTagCompound.getShort("BurnTime");
		furnaceCookTime = par1NBTTagCompound.getShort("CookTime");
		currentItemBurnTime = TileEntityFurnace
				.getItemBurnTime(furnaceItemStacks[1]);

		if (par1NBTTagCompound.hasKey("CustomName")) {
			field_94130_e = par1NBTTagCompound.getString("CustomName");
		}
	}

	/**
	 * Writes a tile entity to NBT.
	 */
	@Override
	public void writeToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeToNBT(par1NBTTagCompound);
		par1NBTTagCompound.setShort("BurnTime", (short) furnaceBurnTime);
		par1NBTTagCompound.setShort("CookTime", (short) furnaceCookTime);
		final NBTTagList var2 = new NBTTagList();

		for (int var3 = 0; var3 < furnaceItemStacks.length; ++var3) {
			if (furnaceItemStacks[var3] != null) {
				final NBTTagCompound var4 = new NBTTagCompound();
				var4.setByte("Slot", (byte) var3);
				furnaceItemStacks[var3].writeToNBT(var4);
				var2.appendTag(var4);
			}
		}

		par1NBTTagCompound.setTag("Items", var2);

		if (isInvNameLocalized()) {
			par1NBTTagCompound.setString("CustomName", field_94130_e);
		}
	}

	/**
	 * Returns the maximum stack size for a inventory slot. Seems to always be
	 * 64, possibly will be extended. *Isn't this more of a set than a get?*
	 */
	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	/**
	 * Returns an integer between 0 and the passed value representing how close
	 * the current item is to being completely cooked
	 */
	public int getCookProgressScaled(final int par1) {
		return furnaceCookTime * par1 / 200;
	}

	/**
	 * Returns an integer between 0 and the passed value representing how much
	 * burn time is left on the current fuel item, where 0 means that the item
	 * is exhausted and the passed value means that the item is fresh
	 */
	public int getBurnTimeRemainingScaled(final int par1) {
		if (currentItemBurnTime == 0) {
			currentItemBurnTime = 200;
		}

		return furnaceBurnTime * par1 / currentItemBurnTime;
	}

	/**
	 * Returns true if the furnace is currently burning
	 */
	public boolean isBurning() {
		return furnaceBurnTime > 0;
	}

	/**
	 * Allows the entity to update its state. Overridden in most subclasses,
	 * e.g. the mob spawner uses this to count ticks and creates a new spawn
	 * inside its implementation.
	 */
	@Override
	public void updateEntity() {
		final boolean var1 = furnaceBurnTime > 0;
		boolean var2 = false;

		if (furnaceBurnTime > 0) {
			--furnaceBurnTime;
		}

		if (!worldObj.isRemote) {
			if (furnaceBurnTime == 0 && canSmelt()) {
				currentItemBurnTime = furnaceBurnTime = TileEntityFurnace
						.getItemBurnTime(furnaceItemStacks[1]);

				if (furnaceBurnTime > 0) {
					var2 = true;

					if (furnaceItemStacks[1] != null) {
						--furnaceItemStacks[1].stackSize;

						if (furnaceItemStacks[1].stackSize == 0) {
							final Item var3 = furnaceItemStacks[1].getItem()
									.getContainerItem();
							furnaceItemStacks[1] = var3 != null ? new ItemStack(
									var3) : null;
						}
					}
				}
			}

			if (isBurning() && canSmelt()) {
				++furnaceCookTime;

				if (furnaceCookTime == 200) {
					furnaceCookTime = 0;
					smeltItem();
					var2 = true;
				}
			} else {
				furnaceCookTime = 0;
			}

			if (var1 != furnaceBurnTime > 0) {
				var2 = true;
				BlockFurnace.updateFurnaceBlockState(furnaceBurnTime > 0,
						worldObj, xCoord, yCoord, zCoord);
			}
		}

		if (var2) {
			onInventoryChanged();
		}
	}

	/**
	 * Returns true if the furnace can smelt an item, i.e. has a source item,
	 * destination stack isn't full, etc.
	 */
	private boolean canSmelt() {
		if (furnaceItemStacks[0] == null) {
			return false;
		} else {
			final ItemStack var1 = FurnaceRecipes.smelting().getSmeltingResult(
					furnaceItemStacks[0].getItem().itemID);
			return var1 == null ? false
					: furnaceItemStacks[2] == null ? true
							: !furnaceItemStacks[2].isItemEqual(var1) ? false
									: furnaceItemStacks[2].stackSize < getInventoryStackLimit()
											&& furnaceItemStacks[2].stackSize < furnaceItemStacks[2]
													.getMaxStackSize() ? true
											: furnaceItemStacks[2].stackSize < var1
													.getMaxStackSize();
		}
	}

	/**
	 * Turn one item from the furnace source stack into the appropriate smelted
	 * item in the furnace result stack
	 */
	public void smeltItem() {
		if (canSmelt()) {
			final ItemStack var1 = FurnaceRecipes.smelting().getSmeltingResult(
					furnaceItemStacks[0].getItem().itemID);

			if (furnaceItemStacks[2] == null) {
				furnaceItemStacks[2] = var1.copy();
			} else if (furnaceItemStacks[2].itemID == var1.itemID) {
				++furnaceItemStacks[2].stackSize;
			}

			--furnaceItemStacks[0].stackSize;

			if (furnaceItemStacks[0].stackSize <= 0) {
				furnaceItemStacks[0] = null;
			}
		}
	}

	/**
	 * Returns the number of ticks that the supplied fuel item will keep the
	 * furnace burning, or 0 if the item isn't fuel
	 */
	public static int getItemBurnTime(final ItemStack par0ItemStack) {
		if (par0ItemStack == null) {
			return 0;
		} else {
			final int var1 = par0ItemStack.getItem().itemID;
			final Item var2 = par0ItemStack.getItem();

			if (var1 < 256 && Block.blocksList[var1] != null) {
				final Block var3 = Block.blocksList[var1];

				if (var3 == Block.woodSingleSlab) {
					return 150;
				}

				if (var3.blockMaterial == Material.wood) {
					return 300;
				}
			}

			return var2 instanceof ItemTool
					&& ((ItemTool) var2).getToolMaterialName().equals("WOOD") ? 200
					: var2 instanceof ItemSword
							&& ((ItemSword) var2).getToolMaterialName().equals(
									"WOOD") ? 200
							: var2 instanceof ItemHoe
									&& ((ItemHoe) var2).getMaterialName()
											.equals("WOOD") ? 200
									: var1 == Item.stick.itemID ? 100
											: var1 == Item.coal.itemID ? 1600
													: var1 == Item.bucketLava.itemID ? 20000
															: var1 == Block.sapling.blockID ? 100
																	: var1 == Item.blazeRod.itemID ? 2400
																			: 0;
		}
	}

	/**
	 * Return true if item is a fuel source (getItemBurnTime() > 0).
	 */
	public static boolean isItemFuel(final ItemStack par0ItemStack) {
		return TileEntityFurnace.getItemBurnTime(par0ItemStack) > 0;
	}

	/**
	 * Do not make give this method the name canInteractWith because it clashes
	 * with Container
	 */
	@Override
	public boolean isUseableByPlayer(final EntityPlayer par1EntityPlayer) {
		return worldObj.getBlockTileEntity(xCoord, yCoord, zCoord) != this ? false
				: par1EntityPlayer.getDistanceSq(xCoord + 0.5D, yCoord + 0.5D,
						zCoord + 0.5D) <= 64.0D;
	}

	@Override
	public void openChest() {
	}

	@Override
	public void closeChest() {
	}

	/**
	 * Returns true if automation is allowed to insert the given stack (ignoring
	 * stack size) into the given slot.
	 */
	@Override
	public boolean isStackValidForSlot(final int par1,
			final ItemStack par2ItemStack) {
		return par1 == 2 ? false : par1 == 1 ? TileEntityFurnace
				.isItemFuel(par2ItemStack) : true;
	}

	/**
	 * Returns an array containing the indices of the slots that can be accessed
	 * by automation on the given side of this block.
	 */
	@Override
	public int[] getAccessibleSlotsFromSide(final int par1) {
		return par1 == 0 ? TileEntityFurnace.field_102011_e
				: par1 == 1 ? TileEntityFurnace.field_102010_d
						: TileEntityFurnace.field_102009_f;
	}

	/**
	 * Returns true if automation can insert the given item in the given slot
	 * from the given side. Args: Slot, item, side
	 */
	@Override
	public boolean canInsertItem(final int par1, final ItemStack par2ItemStack,
			final int par3) {
		return isStackValidForSlot(par1, par2ItemStack);
	}

	/**
	 * Returns true if automation can extract the given item in the given slot
	 * from the given side. Args: Slot, item, side
	 */
	@Override
	public boolean canExtractItem(final int par1,
			final ItemStack par2ItemStack, final int par3) {
		return par3 != 0 || par1 != 1
				|| par2ItemStack.itemID == Item.bucketEmpty.itemID;
	}
}
