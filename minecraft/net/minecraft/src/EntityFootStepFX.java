package net.minecraft.src;

import org.lwjgl.opengl.GL11;

public class EntityFootStepFX extends EntityFX {
	private int field_70576_a = 0;
	private int field_70578_aq = 0;
	private final RenderEngine currentFootSteps;

	public EntityFootStepFX(final RenderEngine par1RenderEngine,
			final World par2World, final double par3, final double par5,
			final double par7) {
		super(par2World, par3, par5, par7, 0.0D, 0.0D, 0.0D);
		currentFootSteps = par1RenderEngine;
		motionX = motionY = motionZ = 0.0D;
		field_70578_aq = 200;
	}

	@Override
	public void renderParticle(final Tessellator par1Tessellator,
			final float par2, final float par3, final float par4,
			final float par5, final float par6, final float par7) {
		float var8 = (field_70576_a + par2) / field_70578_aq;
		var8 *= var8;
		float var9 = 2.0F - var8 * 2.0F;

		if (var9 > 1.0F) {
			var9 = 1.0F;
		}

		var9 *= 0.2F;
		GL11.glDisable(GL11.GL_LIGHTING);
		final float var10 = 0.125F;
		final float var11 = (float) (posX - EntityFX.interpPosX);
		final float var12 = (float) (posY - EntityFX.interpPosY);
		final float var13 = (float) (posZ - EntityFX.interpPosZ);
		final float var14 = worldObj.getLightBrightness(
				MathHelper.floor_double(posX), MathHelper.floor_double(posY),
				MathHelper.floor_double(posZ));
		currentFootSteps.bindTexture("/misc/footprint.png");
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		par1Tessellator.startDrawingQuads();
		par1Tessellator.setColorRGBA_F(var14, var14, var14, var9);
		par1Tessellator.addVertexWithUV(var11 - var10, var12, var13 + var10,
				0.0D, 1.0D);
		par1Tessellator.addVertexWithUV(var11 + var10, var12, var13 + var10,
				1.0D, 1.0D);
		par1Tessellator.addVertexWithUV(var11 + var10, var12, var13 - var10,
				1.0D, 0.0D);
		par1Tessellator.addVertexWithUV(var11 - var10, var12, var13 - var10,
				0.0D, 0.0D);
		par1Tessellator.draw();
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_LIGHTING);
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate() {
		++field_70576_a;

		if (field_70576_a == field_70578_aq) {
			setDead();
		}
	}

	@Override
	public int getFXLayer() {
		return 3;
	}
}
