package net.minecraft.src;

public class BlockJukeBox extends BlockContainer {
	private Icon theIcon;

	protected BlockJukeBox(final int par1) {
		super(par1, Material.wood);
		setCreativeTab(CreativeTabs.tabDecorations);
	}

	/**
	 * From the specified side and block metadata retrieves the blocks texture.
	 * Args: side, metadata
	 */
	@Override
	public Icon getIcon(final int par1, final int par2) {
		return par1 == 1 ? theIcon : blockIcon;
	}

	/**
	 * Called upon block activation (right click on the block.)
	 */
	@Override
	public boolean onBlockActivated(final World par1World, final int par2,
			final int par3, final int par4,
			final EntityPlayer par5EntityPlayer, final int par6,
			final float par7, final float par8, final float par9) {
		if (par1World.getBlockMetadata(par2, par3, par4) == 0) {
			return false;
		} else {
			ejectRecord(par1World, par2, par3, par4);
			return true;
		}
	}

	/**
	 * Insert the specified music disc in the jukebox at the given coordinates
	 */
	public void insertRecord(final World par1World, final int par2,
			final int par3, final int par4, final ItemStack par5ItemStack) {
		if (!par1World.isRemote) {
			final TileEntityRecordPlayer var6 = (TileEntityRecordPlayer) par1World
					.getBlockTileEntity(par2, par3, par4);

			if (var6 != null) {
				var6.func_96098_a(par5ItemStack.copy());
				par1World.setBlockMetadataWithNotify(par2, par3, par4, 1, 2);
			}
		}
	}

	/**
	 * Ejects the current record inside of the jukebox.
	 */
	public void ejectRecord(final World par1World, final int par2,
			final int par3, final int par4) {
		if (!par1World.isRemote) {
			final TileEntityRecordPlayer var5 = (TileEntityRecordPlayer) par1World
					.getBlockTileEntity(par2, par3, par4);

			if (var5 != null) {
				final ItemStack var6 = var5.func_96097_a();

				if (var6 != null) {
					par1World.playAuxSFX(1005, par2, par3, par4, 0);
					par1World.playRecord((String) null, par2, par3, par4);
					var5.func_96098_a((ItemStack) null);
					par1World
							.setBlockMetadataWithNotify(par2, par3, par4, 0, 2);
					final float var7 = 0.7F;
					final double var8 = par1World.rand.nextFloat() * var7
							+ (1.0F - var7) * 0.5D;
					final double var10 = par1World.rand.nextFloat() * var7
							+ (1.0F - var7) * 0.2D + 0.6D;
					final double var12 = par1World.rand.nextFloat() * var7
							+ (1.0F - var7) * 0.5D;
					final ItemStack var14 = var6.copy();
					final EntityItem var15 = new EntityItem(par1World, par2
							+ var8, par3 + var10, par4 + var12, var14);
					var15.delayBeforeCanPickup = 10;
					par1World.spawnEntityInWorld(var15);
				}
			}
		}
	}

	/**
	 * ejects contained items into the world, and notifies neighbours of an
	 * update, as appropriate
	 */
	@Override
	public void breakBlock(final World par1World, final int par2,
			final int par3, final int par4, final int par5, final int par6) {
		ejectRecord(par1World, par2, par3, par4);
		super.breakBlock(par1World, par2, par3, par4, par5, par6);
	}

	/**
	 * Drops the block items with a specified chance of dropping the specified
	 * items
	 */
	@Override
	public void dropBlockAsItemWithChance(final World par1World,
			final int par2, final int par3, final int par4, final int par5,
			final float par6, final int par7) {
		if (!par1World.isRemote) {
			super.dropBlockAsItemWithChance(par1World, par2, par3, par4, par5,
					par6, 0);
		}
	}

	/**
	 * Returns a new instance of a block's tile entity class. Called on placing
	 * the block.
	 */
	@Override
	public TileEntity createNewTileEntity(final World par1World) {
		return new TileEntityRecordPlayer();
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		blockIcon = par1IconRegister.registerIcon("musicBlock");
		theIcon = par1IconRegister.registerIcon("jukebox_top");
	}

	/**
	 * If this returns true, then comparators facing away from this block will
	 * use the value from getComparatorInputOverride instead of the actual
	 * redstone signal strength.
	 */
	@Override
	public boolean hasComparatorInputOverride() {
		return true;
	}

	/**
	 * If hasComparatorInputOverride returns true, the return value from this is
	 * used instead of the redstone signal strength when this block inputs to a
	 * comparator.
	 */
	@Override
	public int getComparatorInputOverride(final World par1World,
			final int par2, final int par3, final int par4, final int par5) {
		final ItemStack var6 = ((TileEntityRecordPlayer) par1World
				.getBlockTileEntity(par2, par3, par4)).func_96097_a();
		return var6 == null ? 0 : var6.itemID + 1 - Item.record13.itemID;
	}
}
