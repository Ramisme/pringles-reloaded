package net.minecraft.src;

import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class EntityAIMate extends EntityAIBase {
	private final EntityAnimal theAnimal;
	World theWorld;
	private EntityAnimal targetMate;

	/**
	 * Delay preventing a baby from spawning immediately when two mate-able
	 * animals find each other.
	 */
	int spawnBabyDelay = 0;

	/** The speed the creature moves at during mating behavior. */
	float moveSpeed;

	public EntityAIMate(final EntityAnimal par1EntityAnimal, final float par2) {
		theAnimal = par1EntityAnimal;
		theWorld = par1EntityAnimal.worldObj;
		moveSpeed = par2;
		setMutexBits(3);
	}

	/**
	 * Returns whether the EntityAIBase should begin execution.
	 */
	@Override
	public boolean shouldExecute() {
		if (!theAnimal.isInLove()) {
			return false;
		} else {
			targetMate = getNearbyMate();
			return targetMate != null;
		}
	}

	/**
	 * Returns whether an in-progress EntityAIBase should continue executing
	 */
	@Override
	public boolean continueExecuting() {
		return targetMate.isEntityAlive() && targetMate.isInLove()
				&& spawnBabyDelay < 60;
	}

	/**
	 * Resets the task
	 */
	@Override
	public void resetTask() {
		targetMate = null;
		spawnBabyDelay = 0;
	}

	/**
	 * Updates the task
	 */
	@Override
	public void updateTask() {
		theAnimal.getLookHelper().setLookPositionWithEntity(targetMate, 10.0F,
				theAnimal.getVerticalFaceSpeed());
		theAnimal.getNavigator().tryMoveToEntityLiving(targetMate, moveSpeed);
		++spawnBabyDelay;

		if (spawnBabyDelay >= 60
				&& theAnimal.getDistanceSqToEntity(targetMate) < 9.0D) {
			spawnBaby();
		}
	}

	/**
	 * Loops through nearby animals and finds another animal of the same type
	 * that can be mated with. Returns the first valid mate found.
	 */
	private EntityAnimal getNearbyMate() {
		final float var1 = 8.0F;
		final List var2 = theWorld.getEntitiesWithinAABB(theAnimal.getClass(),
				theAnimal.boundingBox.expand(var1, var1, var1));
		double var3 = Double.MAX_VALUE;
		EntityAnimal var5 = null;
		final Iterator var6 = var2.iterator();

		while (var6.hasNext()) {
			final EntityAnimal var7 = (EntityAnimal) var6.next();

			if (theAnimal.canMateWith(var7)
					&& theAnimal.getDistanceSqToEntity(var7) < var3) {
				var5 = var7;
				var3 = theAnimal.getDistanceSqToEntity(var7);
			}
		}

		return var5;
	}

	/**
	 * Spawns a baby animal of the same type.
	 */
	private void spawnBaby() {
		final EntityAgeable var1 = theAnimal.createChild(targetMate);

		if (var1 != null) {
			theAnimal.setGrowingAge(6000);
			targetMate.setGrowingAge(6000);
			theAnimal.resetInLove();
			targetMate.resetInLove();
			var1.setGrowingAge(-24000);
			var1.setLocationAndAngles(theAnimal.posX, theAnimal.posY,
					theAnimal.posZ, 0.0F, 0.0F);
			theWorld.spawnEntityInWorld(var1);
			final Random var2 = theAnimal.getRNG();

			for (int var3 = 0; var3 < 7; ++var3) {
				final double var4 = var2.nextGaussian() * 0.02D;
				final double var6 = var2.nextGaussian() * 0.02D;
				final double var8 = var2.nextGaussian() * 0.02D;
				theWorld.spawnParticle("heart",
						theAnimal.posX + var2.nextFloat() * theAnimal.width
								* 2.0F - theAnimal.width, theAnimal.posY + 0.5D
								+ var2.nextFloat() * theAnimal.height,
						theAnimal.posZ + var2.nextFloat() * theAnimal.width
								* 2.0F - theAnimal.width, var4, var6, var8);
			}

			theWorld.spawnEntityInWorld(new EntityXPOrb(theWorld,
					theAnimal.posX, theAnimal.posY, theAnimal.posZ, var2
							.nextInt(7) + 1));
		}
	}
}
