package net.minecraft.src;

public class GuiAnimationSettingsOF extends GuiScreen {
	private final GuiScreen prevScreen;
	protected String title = "Animation Settings";
	private final GameSettings settings;
	private static EnumOptions[] enumOptions = new EnumOptions[] {
			EnumOptions.ANIMATED_WATER, EnumOptions.ANIMATED_LAVA,
			EnumOptions.ANIMATED_FIRE, EnumOptions.ANIMATED_PORTAL,
			EnumOptions.ANIMATED_REDSTONE, EnumOptions.ANIMATED_EXPLOSION,
			EnumOptions.ANIMATED_FLAME, EnumOptions.ANIMATED_SMOKE,
			EnumOptions.VOID_PARTICLES, EnumOptions.WATER_PARTICLES,
			EnumOptions.RAIN_SPLASH, EnumOptions.PORTAL_PARTICLES,
			EnumOptions.POTION_PARTICLES, EnumOptions.DRIPPING_WATER_LAVA,
			EnumOptions.ANIMATED_TERRAIN, EnumOptions.ANIMATED_ITEMS,
			EnumOptions.ANIMATED_TEXTURES, EnumOptions.PARTICLES };

	public GuiAnimationSettingsOF(final GuiScreen var1, final GameSettings var2) {
		prevScreen = var1;
		settings = var2;
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui() {
		final StringTranslate var1 = StringTranslate.getInstance();
		int var2 = 0;
		final EnumOptions[] var3 = GuiAnimationSettingsOF.enumOptions;
		final int var4 = var3.length;

		for (int var5 = 0; var5 < var4; ++var5) {
			final EnumOptions var6 = var3[var5];
			final int var7 = width / 2 - 155 + var2 % 2 * 160;
			final int var8 = height / 6 + 21 * (var2 / 2) - 10;

			if (!var6.getEnumFloat()) {
				buttonList.add(new GuiSmallButton(var6.returnEnumOrdinal(),
						var7, var8, var6, settings.getKeyBinding(var6)));
			} else {
				buttonList.add(new GuiSlider(var6.returnEnumOrdinal(), var7,
						var8, var6, settings.getKeyBinding(var6), settings
								.getOptionFloatValue(var6)));
			}

			++var2;
		}

		buttonList.add(new GuiButton(210, width / 2 - 155,
				height / 6 + 168 + 11, 70, 20, "All ON"));
		buttonList.add(new GuiButton(211, width / 2 - 155 + 80,
				height / 6 + 168 + 11, 70, 20, "All OFF"));
		buttonList.add(new GuiSmallButton(200, width / 2 + 5,
				height / 6 + 168 + 11, var1.translateKey("gui.done")));
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	@Override
	protected void actionPerformed(final GuiButton var1) {
		if (var1.enabled) {
			if (var1.id < 100 && var1 instanceof GuiSmallButton) {
				settings.setOptionValue(
						((GuiSmallButton) var1).returnEnumOptions(), 1);
				var1.displayString = settings.getKeyBinding(EnumOptions
						.getEnumOptions(var1.id));
			}

			if (var1.id == 200) {
				mc.gameSettings.saveOptions();
				mc.displayGuiScreen(prevScreen);
			}

			if (var1.id == 210) {
				mc.gameSettings.setAllAnimations(true);
			}

			if (var1.id == 211) {
				mc.gameSettings.setAllAnimations(false);
			}

			if (var1.id != EnumOptions.CLOUD_HEIGHT.ordinal()) {
				final ScaledResolution var2 = new ScaledResolution(
						mc.gameSettings, mc.displayWidth, mc.displayHeight);
				final int var3 = var2.getScaledWidth();
				final int var4 = var2.getScaledHeight();
				setWorldAndResolution(mc, var3, var4);
			}
		}
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(final int var1, final int var2, final float var3) {
		drawDefaultBackground();
		drawCenteredString(fontRenderer, title, width / 2, 20, 16777215);
		super.drawScreen(var1, var2, var3);
	}
}
