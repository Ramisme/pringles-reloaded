package net.minecraft.src;

public class EntityGiantZombie extends EntityMob {
	public EntityGiantZombie(final World par1World) {
		super(par1World);
		texture = "/mob/zombie.png";
		moveSpeed = 0.5F;
		yOffset *= 6.0F;
		setSize(width * 6.0F, height * 6.0F);
	}

	@Override
	public int getMaxHealth() {
		return 100;
	}

	/**
	 * Takes a coordinate in and returns a weight to determine how likely this
	 * creature will try to path to the block. Args: x, y, z
	 */
	@Override
	public float getBlockPathWeight(final int par1, final int par2,
			final int par3) {
		return worldObj.getLightBrightness(par1, par2, par3) - 0.5F;
	}

	/**
	 * Returns the amount of damage a mob should deal.
	 */
	@Override
	public int getAttackStrength(final Entity par1Entity) {
		return 50;
	}
}
