package net.minecraft.src;

import java.util.Random;

public abstract class BlockRailBase extends Block {
	/** Power related rails have this field at true. */
	protected final boolean isPowered;

	/**
	 * Returns true if the block at the coordinates of world passed is a valid
	 * rail block (current is rail, powered or detector).
	 */
	public static final boolean isRailBlockAt(final World par0World,
			final int par1, final int par2, final int par3) {
		return BlockRailBase
				.isRailBlock(par0World.getBlockId(par1, par2, par3));
	}

	/**
	 * Return true if the parameter is a blockID for a valid rail block (current
	 * is rail, powered or detector).
	 */
	public static final boolean isRailBlock(final int par0) {
		return par0 == Block.rail.blockID || par0 == Block.railPowered.blockID
				|| par0 == Block.railDetector.blockID
				|| par0 == Block.railActivator.blockID;
	}

	protected BlockRailBase(final int par1, final boolean par2) {
		super(par1, Material.circuits);
		isPowered = par2;
		setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.125F, 1.0F);
		setCreativeTab(CreativeTabs.tabTransport);
	}

	/**
	 * Returns true if the block is power related rail.
	 */
	public boolean isPowered() {
		return isPowered;
	}

	/**
	 * Returns a bounding box from the pool of bounding boxes (this means this
	 * box can change after the pool has been cleared to be reused)
	 */
	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(final World par1World,
			final int par2, final int par3, final int par4) {
		return null;
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * Ray traces through the blocks collision from start vector to end vector
	 * returning a ray trace hit. Args: world, x, y, z, startVec, endVec
	 */
	@Override
	public MovingObjectPosition collisionRayTrace(final World par1World,
			final int par2, final int par3, final int par4,
			final Vec3 par5Vec3, final Vec3 par6Vec3) {
		setBlockBoundsBasedOnState(par1World, par2, par3, par4);
		return super.collisionRayTrace(par1World, par2, par3, par4, par5Vec3,
				par6Vec3);
	}

	/**
	 * Updates the blocks bounds based on its current state. Args: world, x, y,
	 * z
	 */
	@Override
	public void setBlockBoundsBasedOnState(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		final int var5 = par1IBlockAccess.getBlockMetadata(par2, par3, par4);

		if (var5 >= 2 && var5 <= 5) {
			setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.625F, 1.0F);
		} else {
			setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.125F, 1.0F);
		}
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return 9;
	}

	/**
	 * Returns the quantity of items to drop on block destruction.
	 */
	@Override
	public int quantityDropped(final Random par1Random) {
		return 1;
	}

	/**
	 * Checks to see if its valid to put this block at the specified
	 * coordinates. Args: world, x, y, z
	 */
	@Override
	public boolean canPlaceBlockAt(final World par1World, final int par2,
			final int par3, final int par4) {
		return par1World.doesBlockHaveSolidTopSurface(par2, par3 - 1, par4);
	}

	/**
	 * Called whenever the block is added into the world. Args: world, x, y, z
	 */
	@Override
	public void onBlockAdded(final World par1World, final int par2,
			final int par3, final int par4) {
		if (!par1World.isRemote) {
			refreshTrackShape(par1World, par2, par3, par4, true);

			if (isPowered) {
				onNeighborBlockChange(par1World, par2, par3, par4, blockID);
			}
		}
	}

	/**
	 * Lets the block know when one of its neighbor changes. Doesn't know which
	 * neighbor changed (coordinates passed are their own) Args: x, y, z,
	 * neighbor blockID
	 */
	@Override
	public void onNeighborBlockChange(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		if (!par1World.isRemote) {
			final int var6 = par1World.getBlockMetadata(par2, par3, par4);
			int var7 = var6;

			if (isPowered) {
				var7 = var6 & 7;
			}

			boolean var8 = false;

			if (!par1World.doesBlockHaveSolidTopSurface(par2, par3 - 1, par4)) {
				var8 = true;
			}

			if (var7 == 2
					&& !par1World.doesBlockHaveSolidTopSurface(par2 + 1, par3,
							par4)) {
				var8 = true;
			}

			if (var7 == 3
					&& !par1World.doesBlockHaveSolidTopSurface(par2 - 1, par3,
							par4)) {
				var8 = true;
			}

			if (var7 == 4
					&& !par1World.doesBlockHaveSolidTopSurface(par2, par3,
							par4 - 1)) {
				var8 = true;
			}

			if (var7 == 5
					&& !par1World.doesBlockHaveSolidTopSurface(par2, par3,
							par4 + 1)) {
				var8 = true;
			}

			if (var8) {
				dropBlockAsItem(par1World, par2, par3, par4,
						par1World.getBlockMetadata(par2, par3, par4), 0);
				par1World.setBlockToAir(par2, par3, par4);
			} else {
				func_94358_a(par1World, par2, par3, par4, var6, var7, par5);
			}
		}
	}

	protected void func_94358_a(final World par1World, final int par2,
			final int par3, final int par4, final int par5, final int par6,
			final int par7) {
	}

	/**
	 * Completely recalculates the track shape based on neighboring tracks
	 */
	protected void refreshTrackShape(final World par1World, final int par2,
			final int par3, final int par4, final boolean par5) {
		if (!par1World.isRemote) {
			new BlockBaseRailLogic(this, par1World, par2, par3, par4)
					.func_94511_a(par1World.isBlockIndirectlyGettingPowered(
							par2, par3, par4), par5);
		}
	}

	/**
	 * Returns the mobility information of the block, 0 = free, 1 = can't push
	 * but can move over, 2 = total immobility and stop pistons
	 */
	@Override
	public int getMobilityFlag() {
		return 0;
	}

	/**
	 * ejects contained items into the world, and notifies neighbours of an
	 * update, as appropriate
	 */
	@Override
	public void breakBlock(final World par1World, final int par2,
			final int par3, final int par4, final int par5, final int par6) {
		int var7 = par6;

		if (isPowered) {
			var7 = par6 & 7;
		}

		super.breakBlock(par1World, par2, par3, par4, par5, par6);

		if (var7 == 2 || var7 == 3 || var7 == 4 || var7 == 5) {
			par1World.notifyBlocksOfNeighborChange(par2, par3 + 1, par4, par5);
		}

		if (isPowered) {
			par1World.notifyBlocksOfNeighborChange(par2, par3, par4, par5);
			par1World.notifyBlocksOfNeighborChange(par2, par3 - 1, par4, par5);
		}
	}
}
