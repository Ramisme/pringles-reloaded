package net.minecraft.src;

import java.util.Iterator;

public class BlockPressurePlateWeighted extends BlockBasePressurePlate {
	/** The maximum number of items the plate weights. */
	private final int maxItemsWeighted;

	protected BlockPressurePlateWeighted(final int par1, final String par2Str,
			final Material par3Material, final int par4) {
		super(par1, par2Str, par3Material);
		maxItemsWeighted = par4;
	}

	/**
	 * Returns the current state of the pressure plate. Returns a value between
	 * 0 and 15 based on the number of items on it.
	 */
	@Override
	protected int getPlateState(final World par1World, final int par2,
			final int par3, final int par4) {
		int var5 = 0;
		final Iterator var6 = par1World.getEntitiesWithinAABB(EntityItem.class,
				getSensitiveAABB(par2, par3, par4)).iterator();

		while (var6.hasNext()) {
			final EntityItem var7 = (EntityItem) var6.next();
			var5 += var7.getEntityItem().stackSize;

			if (var5 >= maxItemsWeighted) {
				break;
			}
		}

		if (var5 <= 0) {
			return 0;
		} else {
			final float var8 = (float) Math.min(maxItemsWeighted, var5)
					/ (float) maxItemsWeighted;
			return MathHelper.ceiling_float_int(var8 * 15.0F);
		}
	}

	/**
	 * Argument is metadata. Returns power level (0-15)
	 */
	@Override
	protected int getPowerSupply(final int par1) {
		return par1;
	}

	/**
	 * Argument is weight (0-15). Return the metadata to be set because of it.
	 */
	@Override
	protected int getMetaFromWeight(final int par1) {
		return par1;
	}

	/**
	 * How many world ticks before ticking
	 */
	@Override
	public int tickRate(final World par1World) {
		return 10;
	}
}
