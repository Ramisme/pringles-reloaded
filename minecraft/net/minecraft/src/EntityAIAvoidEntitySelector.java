package net.minecraft.src;

class EntityAIAvoidEntitySelector implements IEntitySelector {
	final EntityAIAvoidEntity entityAvoiderAI;

	EntityAIAvoidEntitySelector(
			final EntityAIAvoidEntity par1EntityAIAvoidEntity) {
		entityAvoiderAI = par1EntityAIAvoidEntity;
	}

	/**
	 * Return whether the specified entity is applicable to this filter.
	 */
	@Override
	public boolean isEntityApplicable(final Entity par1Entity) {
		return par1Entity.isEntityAlive()
				&& EntityAIAvoidEntity.func_98217_a(entityAvoiderAI)
						.getEntitySenses().canSee(par1Entity);
	}
}
