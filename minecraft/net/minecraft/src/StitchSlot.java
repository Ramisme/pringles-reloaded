package net.minecraft.src;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class StitchSlot {
	private final int originX;
	private final int originY;
	private final int width;
	private final int height;
	private List subSlots;
	private StitchHolder holder;

	public StitchSlot(final int par1, final int par2, final int par3,
			final int par4) {
		originX = par1;
		originY = par2;
		width = par3;
		height = par4;
	}

	public StitchHolder getStitchHolder() {
		return holder;
	}

	public int getOriginX() {
		return originX;
	}

	public int getOriginY() {
		return originY;
	}

	public boolean func_94182_a(final StitchHolder par1StitchHolder) {
		if (holder != null) {
			return false;
		} else {
			final int var2 = par1StitchHolder.getWidth();
			final int var3 = par1StitchHolder.getHeight();

			if (var2 <= width && var3 <= height) {
				if (var2 == width && var3 == height) {
					holder = par1StitchHolder;
					return true;
				} else {
					if (subSlots == null) {
						subSlots = new ArrayList(1);
						subSlots.add(new StitchSlot(originX, originY, var2,
								var3));
						final int var4 = width - var2;
						final int var5 = height - var3;

						if (var5 > 0 && var4 > 0) {
							final int var6 = Math.max(height, var4);
							final int var7 = Math.max(width, var5);

							if (var6 >= var7) {
								subSlots.add(new StitchSlot(originX, originY
										+ var3, var2, var5));
								subSlots.add(new StitchSlot(originX + var2,
										originY, var4, height));
							} else {
								subSlots.add(new StitchSlot(originX + var2,
										originY, var4, var3));
								subSlots.add(new StitchSlot(originX, originY
										+ var3, width, var5));
							}
						} else if (var4 == 0) {
							subSlots.add(new StitchSlot(originX,
									originY + var3, var2, var5));
						} else if (var5 == 0) {
							subSlots.add(new StitchSlot(originX + var2,
									originY, var4, var3));
						}
					}

					final Iterator var8 = subSlots.iterator();
					StitchSlot var9;

					do {
						if (!var8.hasNext()) {
							return false;
						}

						var9 = (StitchSlot) var8.next();
					} while (!var9.func_94182_a(par1StitchHolder));

					return true;
				}
			} else {
				return false;
			}
		}
	}

	/**
	 * Gets the slot and all its subslots
	 */
	public void getAllStitchSlots(final List par1List) {
		if (holder != null) {
			par1List.add(this);
		} else if (subSlots != null) {
			final Iterator var2 = subSlots.iterator();

			while (var2.hasNext()) {
				final StitchSlot var3 = (StitchSlot) var2.next();
				var3.getAllStitchSlots(par1List);
			}
		}
	}

	@Override
	public String toString() {
		return "Slot{originX=" + originX + ", originY=" + originY + ", width="
				+ width + ", height=" + height + ", texture=" + holder
				+ ", subSlots=" + subSlots + '}';
	}
}
