package net.minecraft.src;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

import org.lwjgl.input.Keyboard;

public class GuiCommandBlock extends GuiScreen {
	/** Text field containing the command block's command. */
	private GuiTextField commandTextField;

	/** Command block being edited. */
	private final TileEntityCommandBlock commandBlock;
	private GuiButton doneBtn;
	private GuiButton cancelBtn;

	public GuiCommandBlock(final TileEntityCommandBlock par1) {
		commandBlock = par1;
	}

	/**
	 * Called from the main game loop to update the screen.
	 */
	@Override
	public void updateScreen() {
		commandTextField.updateCursorCounter();
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui() {
		final StringTranslate var1 = StringTranslate.getInstance();
		Keyboard.enableRepeatEvents(true);
		buttonList.clear();
		buttonList.add(doneBtn = new GuiButton(0, width / 2 - 100,
				height / 4 + 96 + 12, var1.translateKey("gui.done")));
		buttonList.add(cancelBtn = new GuiButton(1, width / 2 - 100,
				height / 4 + 120 + 12, var1.translateKey("gui.cancel")));
		commandTextField = new GuiTextField(fontRenderer, width / 2 - 150, 60,
				300, 20);
		commandTextField.setMaxStringLength(32767);
		commandTextField.setFocused(true);
		commandTextField.setText(commandBlock.getCommand());
		doneBtn.enabled = commandTextField.getText().trim().length() > 0;
	}

	/**
	 * Called when the screen is unloaded. Used to disable keyboard repeat
	 * events
	 */
	@Override
	public void onGuiClosed() {
		Keyboard.enableRepeatEvents(false);
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	@Override
	protected void actionPerformed(final GuiButton par1GuiButton) {
		if (par1GuiButton.enabled) {
			if (par1GuiButton.id == 1) {
				mc.displayGuiScreen((GuiScreen) null);
			} else if (par1GuiButton.id == 0) {
				final String var2 = "MC|AdvCdm";
				final ByteArrayOutputStream var3 = new ByteArrayOutputStream();
				final DataOutputStream var4 = new DataOutputStream(var3);

				try {
					var4.writeInt(commandBlock.xCoord);
					var4.writeInt(commandBlock.yCoord);
					var4.writeInt(commandBlock.zCoord);
					Packet.writeString(commandTextField.getText(), var4);
					mc.getNetHandler()
							.addToSendQueue(
									new Packet250CustomPayload(var2, var3
											.toByteArray()));
				} catch (final Exception var6) {
					var6.printStackTrace();
				}

				mc.displayGuiScreen((GuiScreen) null);
			}
		}
	}

	/**
	 * Fired when a key is typed. This is the equivalent of
	 * KeyListener.keyTyped(KeyEvent e).
	 */
	@Override
	protected void keyTyped(final char par1, final int par2) {
		commandTextField.textboxKeyTyped(par1, par2);
		doneBtn.enabled = commandTextField.getText().trim().length() > 0;

		if (par2 != 28 && par1 != 13) {
			if (par2 == 1) {
				actionPerformed(cancelBtn);
			}
		} else {
			actionPerformed(doneBtn);
		}
	}

	/**
	 * Called when the mouse is clicked.
	 */
	@Override
	protected void mouseClicked(final int par1, final int par2, final int par3) {
		super.mouseClicked(par1, par2, par3);
		commandTextField.mouseClicked(par1, par2, par3);
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(final int par1, final int par2, final float par3) {
		final StringTranslate var4 = StringTranslate.getInstance();
		drawDefaultBackground();
		drawCenteredString(fontRenderer,
				var4.translateKey("advMode.setCommand"), width / 2,
				height / 4 - 60 + 20, 16777215);
		drawString(fontRenderer, var4.translateKey("advMode.command"),
				width / 2 - 150, 47, 10526880);
		drawString(fontRenderer, var4.translateKey("advMode.nearestPlayer"),
				width / 2 - 150, 97, 10526880);
		drawString(fontRenderer, var4.translateKey("advMode.randomPlayer"),
				width / 2 - 150, 108, 10526880);
		drawString(fontRenderer, var4.translateKey("advMode.allPlayers"),
				width / 2 - 150, 119, 10526880);
		commandTextField.drawTextBox();
		super.drawScreen(par1, par2, par3);
	}
}
