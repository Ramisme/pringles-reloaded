package net.minecraft.src;

public class ChestItemRenderHelper {
	/** The static instance of ChestItemRenderHelper. */
	public static ChestItemRenderHelper instance = new ChestItemRenderHelper();

	/** Instance of Chest's Tile Entity. */
	private final TileEntityChest theChest = new TileEntityChest();

	/** Instance of Ender Chest's Tile Entity. */
	private final TileEntityEnderChest theEnderChest = new TileEntityEnderChest();

	/**
	 * Renders a chest at 0,0,0 - used for item rendering
	 */
	public void renderChest(final Block par1Block, final int par2,
			final float par3) {
		if (par1Block.blockID == Block.enderChest.blockID) {
			TileEntityRenderer.instance.renderTileEntityAt(theEnderChest, 0.0D,
					0.0D, 0.0D, 0.0F);
		} else {
			TileEntityRenderer.instance.renderTileEntityAt(theChest, 0.0D,
					0.0D, 0.0D, 0.0F);
		}
	}
}
