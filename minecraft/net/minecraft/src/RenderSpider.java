package net.minecraft.src;

import org.lwjgl.opengl.GL11;

public class RenderSpider extends RenderLiving {
	public RenderSpider() {
		super(new ModelSpider(), 1.0F);
		setRenderPassModel(new ModelSpider());
	}

	protected float setSpiderDeathMaxRotation(
			final EntitySpider par1EntitySpider) {
		return 180.0F;
	}

	/**
	 * Sets the spider's glowing eyes
	 */
	protected int setSpiderEyeBrightness(final EntitySpider par1EntitySpider,
			final int par2, final float par3) {
		if (par2 != 0) {
			return -1;
		} else {
			loadTexture("/mob/spider_eyes.png");
			final float var4 = 1.0F;
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glDisable(GL11.GL_ALPHA_TEST);
			GL11.glBlendFunc(GL11.GL_ONE, GL11.GL_ONE);

			if (par1EntitySpider.isInvisible()) {
				GL11.glDepthMask(false);
			} else {
				GL11.glDepthMask(true);
			}

			final char var5 = 61680;
			final int var6 = var5 % 65536;
			final int var7 = var5 / 65536;
			OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit,
					var6 / 1.0F, var7 / 1.0F);
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			GL11.glColor4f(1.0F, 1.0F, 1.0F, var4);
			return 1;
		}
	}

	protected void scaleSpider(final EntitySpider par1EntitySpider,
			final float par2) {
		final float var3 = par1EntitySpider.spiderScaleAmount();
		GL11.glScalef(var3, var3, var3);
	}

	/**
	 * Allows the render to do any OpenGL state modifications necessary before
	 * the model is rendered. Args: entityLiving, partialTickTime
	 */
	@Override
	protected void preRenderCallback(final EntityLiving par1EntityLiving,
			final float par2) {
		scaleSpider((EntitySpider) par1EntityLiving, par2);
	}

	@Override
	protected float getDeathMaxRotation(final EntityLiving par1EntityLiving) {
		return setSpiderDeathMaxRotation((EntitySpider) par1EntityLiving);
	}

	/**
	 * Queries whether should render the specified pass or not.
	 */
	@Override
	protected int shouldRenderPass(final EntityLiving par1EntityLiving,
			final int par2, final float par3) {
		return setSpiderEyeBrightness((EntitySpider) par1EntityLiving, par2,
				par3);
	}
}
