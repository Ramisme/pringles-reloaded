package net.minecraft.src;

import java.util.Random;

//TODO: MathHelper - FPS++
public class MathHelper {
	/**
	 * A table of sin values computed from 0 (inclusive) to 2*pi (exclusive),
	 * with steps of 2*PI / 65536.
	 */
	private static float[] SIN_TABLE = new float[4096];

	/**
	 * sin looked up in a table
	 */
	public static final float sin(final float par0) {
		return MathHelper.SIN_TABLE[(int) (par0 * 651.89862F) & 0xFFF];
	}

	/**
	 * cos looked up in the sin table with the appropriate offset
	 */
	public static final float cos(final float par0) {
		return MathHelper.SIN_TABLE[(int) (par0 * 651.89862F + 1024.0F) & 0xFFF];
	}

	public static final float sqrt_float(final float par0) {
		return (float) Math.sqrt(par0);
	}

	public static final float sqrt_double(final double par0) {
		return (float) Math.sqrt(par0);
	}

	/**
	 * Returns the greatest integer less than or equal to the float argument
	 */
	public static int floor_float(final float par0) {
		final int var1 = (int) par0;
		return par0 < var1 ? var1 - 1 : var1;
	}

	/**
	 * returns par0 cast as an int, and no greater than Integer.MAX_VALUE-1024
	 */
	public static int truncateDoubleToInt(final double par0) {
		return (int) (par0 + 1024.0D) - 1024;
	}

	/**
	 * Returns the greatest integer less than or equal to the double argument
	 */
	public static int floor_double(final double par0) {
		final int var2 = (int) par0;
		return par0 < var2 ? var2 - 1 : var2;
	}

	/**
	 * Long version of floor_double
	 */
	public static long floor_double_long(final double par0) {
		final long var2 = (long) par0;
		return par0 < var2 ? var2 - 1L : var2;
	}

	public static float abs(final float par0) {
		return par0 >= 0.0F ? par0 : -par0;
	}

	public static int abs_int(final int par0) {
		return par0 >= 0 ? par0 : -par0;
	}

	public static int ceiling_float_int(final float par0) {
		final int var1 = (int) par0;
		return par0 > var1 ? var1 + 1 : var1;
	}

	public static int ceiling_double_int(final double par0) {
		final int var2 = (int) par0;
		return par0 > var2 ? var2 + 1 : var2;
	}

	/**
	 * Returns the value of the first parameter, clamped to be within the lower
	 * and upper limits given by the second and third parameters.
	 */
	public static int clamp_int(final int par0, final int par1, final int par2) {
		return par0 < par1 ? par1 : par0 > par2 ? par2 : par0;
	}

	/**
	 * Returns the value of the first parameter, clamped to be within the lower
	 * and upper limits given by the second and third parameters
	 */
	public static float clamp_float(final float par0, final float par1,
			final float par2) {
		return par0 < par1 ? par1 : par0 > par2 ? par2 : par0;
	}

	/**
	 * Maximum of the absolute value of two numbers.
	 */
	public static double abs_max(double par0, double par2) {
		if (par0 < 0.0D) {
			par0 = -par0;
		}

		if (par2 < 0.0D) {
			par2 = -par2;
		}

		return par0 > par2 ? par0 : par2;
	}

	/**
	 * Buckets an integer with specifed bucket sizes. Args: i, bucketSize
	 */
	public static int bucketInt(final int par0, final int par1) {
		return par0 < 0 ? -((-par0 - 1) / par1) - 1 : par0 / par1;
	}

	/**
	 * Tests if a string is null or of length zero
	 */
	public static boolean stringNullOrLengthZero(final String par0Str) {
		return par0Str == null || par0Str.length() == 0;
	}

	public static int getRandomIntegerInRange(final Random par0Random,
			final int par1, final int par2) {
		return par1 >= par2 ? par1 : par0Random.nextInt(par2 - par1 + 1) + par1;
	}

	public static double getRandomDoubleInRange(final Random par0Random,
			final double par1, final double par3) {
		return par1 >= par3 ? par1 : par0Random.nextDouble() * (par3 - par1)
				+ par1;
	}

	public static double average(final long[] par0ArrayOfLong) {
		long var1 = 0L;
		final long[] var3 = par0ArrayOfLong;
		final int var4 = par0ArrayOfLong.length;

		for (int var5 = 0; var5 < var4; ++var5) {
			final long var6 = var3[var5];
			var1 += var6;
		}

		return (double) var1 / (double) par0ArrayOfLong.length;
	}

	/**
	 * the angle is reduced to an angle between -180 and +180 by mod, and a 360
	 * check
	 */
	public static float wrapAngleTo180_float(float par0) {
		par0 %= 360.0F;

		if (par0 >= 180.0F) {
			par0 -= 360.0F;
		}

		if (par0 < -180.0F) {
			par0 += 360.0F;
		}

		return par0;
	}

	/**
	 * the angle is reduced to an angle between -180 and +180 by mod, and a 360
	 * check
	 */
	public static double wrapAngleTo180_double(double par0) {
		par0 %= 360.0D;

		if (par0 >= 180.0D) {
			par0 -= 360.0D;
		}

		if (par0 < -180.0D) {
			par0 += 360.0D;
		}

		return par0;
	}

	/**
	 * parses the string as integer or returns the second parameter if it fails
	 */
	public static int parseIntWithDefault(final String par0Str, final int par1) {
		int var2 = par1;

		try {
			var2 = Integer.parseInt(par0Str);
		} catch (final Throwable var4) {
			;
		}

		return var2;
	}

	/**
	 * parses the string as integer or returns the second parameter if it fails.
	 * this value is capped to par2
	 */
	public static int parseIntWithDefaultAndMax(final String par0Str,
			final int par1, final int par2) {
		int var3 = par1;

		try {
			var3 = Integer.parseInt(par0Str);
		} catch (final Throwable var5) {
			;
		}

		if (var3 < par2) {
			var3 = par2;
		}

		return var3;
	}

	/**
	 * parses the string as double or returns the second parameter if it fails.
	 */
	public static double parseDoubleWithDefault(final String par0Str,
			final double par1) {
		double var3 = par1;

		try {
			var3 = Double.parseDouble(par0Str);
		} catch (final Throwable var6) {
			;
		}

		return var3;
	}

	public static double func_82713_a(final String par0Str, final double par1,
			final double par3) {
		double var5 = par1;

		try {
			var5 = Double.parseDouble(par0Str);
		} catch (final Throwable var8) {
			;
		}

		if (var5 < par3) {
			var5 = par3;
		}

		return var5;
	}

	static {
		for (int i = 0; i < 4096; i++) {
			MathHelper.SIN_TABLE[i] = (float) Math
					.sin(i * 3.141592653589793D * 2.0D / 4096.0D);
		}
	}
}