package net.minecraft.src;

final class EnchantmentModifierDamage implements IEnchantmentModifier {
	/**
	 * Used to calculate the damage modifier (extra armor) on enchantments that
	 * the player have on equipped armors.
	 */
	public int damageModifier;

	/**
	 * Used as parameter to calculate the damage modifier (extra armor) on
	 * enchantments that the player have on equipped armors.
	 */
	public DamageSource source;

	private EnchantmentModifierDamage() {
	}

	/**
	 * Generic method use to calculate modifiers of offensive or defensive
	 * enchantment values.
	 */
	@Override
	public void calculateModifier(final Enchantment par1Enchantment,
			final int par2) {
		damageModifier += par1Enchantment.calcModifierDamage(par2, source);
	}

	EnchantmentModifierDamage(final Empty3 par1Empty3) {
		this();
	}
}
