package net.minecraft.src;

public class EntityAIPanic extends EntityAIBase {
	private final EntityCreature theEntityCreature;
	private final float speed;
	private double randPosX;
	private double randPosY;
	private double randPosZ;

	public EntityAIPanic(final EntityCreature par1EntityCreature,
			final float par2) {
		theEntityCreature = par1EntityCreature;
		speed = par2;
		setMutexBits(1);
	}

	/**
	 * Returns whether the EntityAIBase should begin execution.
	 */
	@Override
	public boolean shouldExecute() {
		if (theEntityCreature.getAITarget() == null
				&& !theEntityCreature.isBurning()) {
			return false;
		} else {
			final Vec3 var1 = RandomPositionGenerator.findRandomTarget(
					theEntityCreature, 5, 4);

			if (var1 == null) {
				return false;
			} else {
				randPosX = var1.xCoord;
				randPosY = var1.yCoord;
				randPosZ = var1.zCoord;
				return true;
			}
		}
	}

	/**
	 * Execute a one shot task or start executing a continuous task
	 */
	@Override
	public void startExecuting() {
		theEntityCreature.getNavigator().tryMoveToXYZ(randPosX, randPosY,
				randPosZ, speed);
	}

	/**
	 * Returns whether an in-progress EntityAIBase should continue executing
	 */
	@Override
	public boolean continueExecuting() {
		return !theEntityCreature.getNavigator().noPath();
	}
}
