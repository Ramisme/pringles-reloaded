package net.minecraft.src;

public class ModelCreeper extends ModelBase {
	public ModelRenderer head;
	public ModelRenderer field_78133_b;
	public ModelRenderer body;
	public ModelRenderer leg1;
	public ModelRenderer leg2;
	public ModelRenderer leg3;
	public ModelRenderer leg4;

	public ModelCreeper() {
		this(0.0F);
	}

	public ModelCreeper(final float par1) {
		final byte var2 = 4;
		head = new ModelRenderer(this, 0, 0);
		head.addBox(-4.0F, -8.0F, -4.0F, 8, 8, 8, par1);
		head.setRotationPoint(0.0F, var2, 0.0F);
		field_78133_b = new ModelRenderer(this, 32, 0);
		field_78133_b.addBox(-4.0F, -8.0F, -4.0F, 8, 8, 8, par1 + 0.5F);
		field_78133_b.setRotationPoint(0.0F, var2, 0.0F);
		body = new ModelRenderer(this, 16, 16);
		body.addBox(-4.0F, 0.0F, -2.0F, 8, 12, 4, par1);
		body.setRotationPoint(0.0F, var2, 0.0F);
		leg1 = new ModelRenderer(this, 0, 16);
		leg1.addBox(-2.0F, 0.0F, -2.0F, 4, 6, 4, par1);
		leg1.setRotationPoint(-2.0F, 12 + var2, 4.0F);
		leg2 = new ModelRenderer(this, 0, 16);
		leg2.addBox(-2.0F, 0.0F, -2.0F, 4, 6, 4, par1);
		leg2.setRotationPoint(2.0F, 12 + var2, 4.0F);
		leg3 = new ModelRenderer(this, 0, 16);
		leg3.addBox(-2.0F, 0.0F, -2.0F, 4, 6, 4, par1);
		leg3.setRotationPoint(-2.0F, 12 + var2, -4.0F);
		leg4 = new ModelRenderer(this, 0, 16);
		leg4.addBox(-2.0F, 0.0F, -2.0F, 4, 6, 4, par1);
		leg4.setRotationPoint(2.0F, 12 + var2, -4.0F);
	}

	/**
	 * Sets the models various rotation angles then renders the model.
	 */
	@Override
	public void render(final Entity par1Entity, final float par2,
			final float par3, final float par4, final float par5,
			final float par6, final float par7) {
		setRotationAngles(par2, par3, par4, par5, par6, par7, par1Entity);
		head.render(par7);
		body.render(par7);
		leg1.render(par7);
		leg2.render(par7);
		leg3.render(par7);
		leg4.render(par7);
	}

	/**
	 * Sets the model's various rotation angles. For bipeds, par1 and par2 are
	 * used for animating the movement of arms and legs, where par1 represents
	 * the time(so that arms and legs swing back and forth) and par2 represents
	 * how "far" arms and legs can swing at most.
	 */
	@Override
	public void setRotationAngles(final float par1, final float par2,
			final float par3, final float par4, final float par5,
			final float par6, final Entity par7Entity) {
		head.rotateAngleY = par4 / (180F / (float) Math.PI);
		head.rotateAngleX = par5 / (180F / (float) Math.PI);
		leg1.rotateAngleX = MathHelper.cos(par1 * 0.6662F) * 1.4F * par2;
		leg2.rotateAngleX = MathHelper.cos(par1 * 0.6662F + (float) Math.PI)
				* 1.4F * par2;
		leg3.rotateAngleX = MathHelper.cos(par1 * 0.6662F + (float) Math.PI)
				* 1.4F * par2;
		leg4.rotateAngleX = MathHelper.cos(par1 * 0.6662F) * 1.4F * par2;
	}
}
