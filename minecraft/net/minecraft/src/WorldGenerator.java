package net.minecraft.src;

import java.util.Random;

public abstract class WorldGenerator {
	/**
	 * Sets wither or not the generator should notify blocks of blocks it
	 * changes. When the world is first generated, this is false, when saplings
	 * grow, this is true.
	 */
	private final boolean doBlockNotify;

	public WorldGenerator() {
		doBlockNotify = false;
	}

	public WorldGenerator(final boolean par1) {
		doBlockNotify = par1;
	}

	public abstract boolean generate(World var1, Random var2, int var3,
			int var4, int var5);

	/**
	 * Rescales the generator settings, only used in WorldGenBigTree
	 */
	public void setScale(final double par1, final double par3, final double par5) {
	}

	/**
	 * Sets the block without metadata in the world, notifying neighbors if
	 * enabled.
	 */
	protected void setBlock(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		setBlockAndMetadata(par1World, par2, par3, par4, par5, 0);
	}

	/**
	 * Sets the block in the world, notifying neighbors if enabled.
	 */
	protected void setBlockAndMetadata(final World par1World, final int par2,
			final int par3, final int par4, final int par5, final int par6) {
		if (doBlockNotify) {
			par1World.setBlock(par2, par3, par4, par5, par6, 3);
		} else {
			par1World.setBlock(par2, par3, par4, par5, par6, 2);
		}
	}
}
