package net.minecraft.src;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Arrays;

public class NBTTagByteArray extends NBTBase {
	/** The byte array stored in the tag. */
	public byte[] byteArray;

	public NBTTagByteArray(final String par1Str) {
		super(par1Str);
	}

	public NBTTagByteArray(final String par1Str, final byte[] par2ArrayOfByte) {
		super(par1Str);
		byteArray = par2ArrayOfByte;
	}

	/**
	 * Write the actual data contents of the tag, implemented in NBT extension
	 * classes
	 */
	@Override
	void write(final DataOutput par1DataOutput) throws IOException {
		par1DataOutput.writeInt(byteArray.length);
		par1DataOutput.write(byteArray);
	}

	/**
	 * Read the actual data contents of the tag, implemented in NBT extension
	 * classes
	 */
	@Override
	void load(final DataInput par1DataInput) throws IOException {
		final int var2 = par1DataInput.readInt();
		byteArray = new byte[var2];
		par1DataInput.readFully(byteArray);
	}

	/**
	 * Gets the type byte for the tag.
	 */
	@Override
	public byte getId() {
		return (byte) 7;
	}

	@Override
	public String toString() {
		return "[" + byteArray.length + " bytes]";
	}

	/**
	 * Creates a clone of the tag.
	 */
	@Override
	public NBTBase copy() {
		final byte[] var1 = new byte[byteArray.length];
		System.arraycopy(byteArray, 0, var1, 0, byteArray.length);
		return new NBTTagByteArray(getName(), var1);
	}

	@Override
	public boolean equals(final Object par1Obj) {
		return super.equals(par1Obj) ? Arrays.equals(byteArray,
				((NBTTagByteArray) par1Obj).byteArray) : false;
	}

	@Override
	public int hashCode() {
		return super.hashCode() ^ Arrays.hashCode(byteArray);
	}
}
