package net.minecraft.src;

public class ItemColored extends ItemBlock {
	private final Block blockRef;
	private String[] blockNames;

	public ItemColored(final int par1, final boolean par2) {
		super(par1);
		blockRef = Block.blocksList[getBlockID()];

		if (par2) {
			setMaxDamage(0);
			setHasSubtypes(true);
		}
	}

	@Override
	public int getColorFromItemStack(final ItemStack par1ItemStack,
			final int par2) {
		return blockRef.getRenderColor(par1ItemStack.getItemDamage());
	}

	/**
	 * Gets an icon index based on an item's damage value
	 */
	@Override
	public Icon getIconFromDamage(final int par1) {
		return blockRef.getIcon(0, par1);
	}

	/**
	 * Returns the metadata of the block which this Item (ItemBlock) can place
	 */
	@Override
	public int getMetadata(final int par1) {
		return par1;
	}

	/**
	 * Sets the array of strings to be used for name lookups from item damage to
	 * metadata
	 */
	public ItemColored setBlockNames(final String[] par1ArrayOfStr) {
		blockNames = par1ArrayOfStr;
		return this;
	}

	/**
	 * Returns the unlocalized name of this item. This version accepts an
	 * ItemStack so different stacks can have different names based on their
	 * damage or NBT.
	 */
	@Override
	public String getUnlocalizedName(final ItemStack par1ItemStack) {
		if (blockNames == null) {
			return super.getUnlocalizedName(par1ItemStack);
		} else {
			final int var2 = par1ItemStack.getItemDamage();
			return var2 >= 0 && var2 < blockNames.length ? super
					.getUnlocalizedName(par1ItemStack) + "." + blockNames[var2]
					: super.getUnlocalizedName(par1ItemStack);
		}
	}
}
