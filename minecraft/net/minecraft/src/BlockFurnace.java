package net.minecraft.src;

import java.util.Random;

public class BlockFurnace extends BlockContainer {
	/**
	 * Is the random generator used by furnace to drop the inventory contents in
	 * random directions.
	 */
	private final Random furnaceRand = new Random();

	/** True if this is an active furnace, false if idle */
	private final boolean isActive;

	/**
	 * This flag is used to prevent the furnace inventory to be dropped upon
	 * block removal, is used internally when the furnace block changes from
	 * idle to active and vice-versa.
	 */
	private static boolean keepFurnaceInventory = false;
	private Icon furnaceIconTop;
	private Icon furnaceIconFront;

	protected BlockFurnace(final int par1, final boolean par2) {
		super(par1, Material.rock);
		isActive = par2;
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return Block.furnaceIdle.blockID;
	}

	/**
	 * Called whenever the block is added into the world. Args: world, x, y, z
	 */
	@Override
	public void onBlockAdded(final World par1World, final int par2,
			final int par3, final int par4) {
		super.onBlockAdded(par1World, par2, par3, par4);
		setDefaultDirection(par1World, par2, par3, par4);
	}

	/**
	 * set a blocks direction
	 */
	private void setDefaultDirection(final World par1World, final int par2,
			final int par3, final int par4) {
		if (!par1World.isRemote) {
			final int var5 = par1World.getBlockId(par2, par3, par4 - 1);
			final int var6 = par1World.getBlockId(par2, par3, par4 + 1);
			final int var7 = par1World.getBlockId(par2 - 1, par3, par4);
			final int var8 = par1World.getBlockId(par2 + 1, par3, par4);
			byte var9 = 3;

			if (Block.opaqueCubeLookup[var5] && !Block.opaqueCubeLookup[var6]) {
				var9 = 3;
			}

			if (Block.opaqueCubeLookup[var6] && !Block.opaqueCubeLookup[var5]) {
				var9 = 2;
			}

			if (Block.opaqueCubeLookup[var7] && !Block.opaqueCubeLookup[var8]) {
				var9 = 5;
			}

			if (Block.opaqueCubeLookup[var8] && !Block.opaqueCubeLookup[var7]) {
				var9 = 4;
			}

			par1World.setBlockMetadataWithNotify(par2, par3, par4, var9, 2);
		}
	}

	/**
	 * From the specified side and block metadata retrieves the blocks texture.
	 * Args: side, metadata
	 */
	@Override
	public Icon getIcon(final int par1, final int par2) {
		return par1 == 1 ? furnaceIconTop : par1 == 0 ? furnaceIconTop
				: par1 != par2 ? blockIcon : furnaceIconFront;
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		blockIcon = par1IconRegister.registerIcon("furnace_side");
		furnaceIconFront = par1IconRegister
				.registerIcon(isActive ? "furnace_front_lit" : "furnace_front");
		furnaceIconTop = par1IconRegister.registerIcon("furnace_top");
	}

	/**
	 * A randomly called display update to be able to add particles or other
	 * items for display
	 */
	@Override
	public void randomDisplayTick(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		if (isActive) {
			final int var6 = par1World.getBlockMetadata(par2, par3, par4);
			final float var7 = par2 + 0.5F;
			final float var8 = par3 + 0.0F + par5Random.nextFloat() * 6.0F
					/ 16.0F;
			final float var9 = par4 + 0.5F;
			final float var10 = 0.52F;
			final float var11 = par5Random.nextFloat() * 0.6F - 0.3F;

			if (var6 == 4) {
				par1World.spawnParticle("smoke", var7 - var10, var8, var9
						+ var11, 0.0D, 0.0D, 0.0D);
				par1World.spawnParticle("flame", var7 - var10, var8, var9
						+ var11, 0.0D, 0.0D, 0.0D);
			} else if (var6 == 5) {
				par1World.spawnParticle("smoke", var7 + var10, var8, var9
						+ var11, 0.0D, 0.0D, 0.0D);
				par1World.spawnParticle("flame", var7 + var10, var8, var9
						+ var11, 0.0D, 0.0D, 0.0D);
			} else if (var6 == 2) {
				par1World.spawnParticle("smoke", var7 + var11, var8, var9
						- var10, 0.0D, 0.0D, 0.0D);
				par1World.spawnParticle("flame", var7 + var11, var8, var9
						- var10, 0.0D, 0.0D, 0.0D);
			} else if (var6 == 3) {
				par1World.spawnParticle("smoke", var7 + var11, var8, var9
						+ var10, 0.0D, 0.0D, 0.0D);
				par1World.spawnParticle("flame", var7 + var11, var8, var9
						+ var10, 0.0D, 0.0D, 0.0D);
			}
		}
	}

	/**
	 * Called upon block activation (right click on the block.)
	 */
	@Override
	public boolean onBlockActivated(final World par1World, final int par2,
			final int par3, final int par4,
			final EntityPlayer par5EntityPlayer, final int par6,
			final float par7, final float par8, final float par9) {
		if (par1World.isRemote) {
			return true;
		} else {
			final TileEntityFurnace var10 = (TileEntityFurnace) par1World
					.getBlockTileEntity(par2, par3, par4);

			if (var10 != null) {
				par5EntityPlayer.displayGUIFurnace(var10);
			}

			return true;
		}
	}

	/**
	 * Update which block ID the furnace is using depending on whether or not it
	 * is burning
	 */
	public static void updateFurnaceBlockState(final boolean par0,
			final World par1World, final int par2, final int par3,
			final int par4) {
		final int var5 = par1World.getBlockMetadata(par2, par3, par4);
		final TileEntity var6 = par1World.getBlockTileEntity(par2, par3, par4);
		BlockFurnace.keepFurnaceInventory = true;

		if (par0) {
			par1World.setBlock(par2, par3, par4, Block.furnaceBurning.blockID);
		} else {
			par1World.setBlock(par2, par3, par4, Block.furnaceIdle.blockID);
		}

		BlockFurnace.keepFurnaceInventory = false;
		par1World.setBlockMetadataWithNotify(par2, par3, par4, var5, 2);

		if (var6 != null) {
			var6.validate();
			par1World.setBlockTileEntity(par2, par3, par4, var6);
		}
	}

	/**
	 * Returns a new instance of a block's tile entity class. Called on placing
	 * the block.
	 */
	@Override
	public TileEntity createNewTileEntity(final World par1World) {
		return new TileEntityFurnace();
	}

	/**
	 * Called when the block is placed in the world.
	 */
	@Override
	public void onBlockPlacedBy(final World par1World, final int par2,
			final int par3, final int par4,
			final EntityLiving par5EntityLiving, final ItemStack par6ItemStack) {
		final int var7 = MathHelper
				.floor_double(par5EntityLiving.rotationYaw * 4.0F / 360.0F + 0.5D) & 3;

		if (var7 == 0) {
			par1World.setBlockMetadataWithNotify(par2, par3, par4, 2, 2);
		}

		if (var7 == 1) {
			par1World.setBlockMetadataWithNotify(par2, par3, par4, 5, 2);
		}

		if (var7 == 2) {
			par1World.setBlockMetadataWithNotify(par2, par3, par4, 3, 2);
		}

		if (var7 == 3) {
			par1World.setBlockMetadataWithNotify(par2, par3, par4, 4, 2);
		}

		if (par6ItemStack.hasDisplayName()) {
			((TileEntityFurnace) par1World.getBlockTileEntity(par2, par3, par4))
					.func_94129_a(par6ItemStack.getDisplayName());
		}
	}

	/**
	 * ejects contained items into the world, and notifies neighbours of an
	 * update, as appropriate
	 */
	@Override
	public void breakBlock(final World par1World, final int par2,
			final int par3, final int par4, final int par5, final int par6) {
		if (!BlockFurnace.keepFurnaceInventory) {
			final TileEntityFurnace var7 = (TileEntityFurnace) par1World
					.getBlockTileEntity(par2, par3, par4);

			if (var7 != null) {
				for (int var8 = 0; var8 < var7.getSizeInventory(); ++var8) {
					final ItemStack var9 = var7.getStackInSlot(var8);

					if (var9 != null) {
						final float var10 = furnaceRand.nextFloat() * 0.8F + 0.1F;
						final float var11 = furnaceRand.nextFloat() * 0.8F + 0.1F;
						final float var12 = furnaceRand.nextFloat() * 0.8F + 0.1F;

						while (var9.stackSize > 0) {
							int var13 = furnaceRand.nextInt(21) + 10;

							if (var13 > var9.stackSize) {
								var13 = var9.stackSize;
							}

							var9.stackSize -= var13;
							final EntityItem var14 = new EntityItem(par1World,
									par2 + var10, par3 + var11, par4 + var12,
									new ItemStack(var9.itemID, var13,
											var9.getItemDamage()));

							if (var9.hasTagCompound()) {
								var14.getEntityItem().setTagCompound(
										(NBTTagCompound) var9.getTagCompound()
												.copy());
							}

							final float var15 = 0.05F;
							var14.motionX = (float) furnaceRand.nextGaussian()
									* var15;
							var14.motionY = (float) furnaceRand.nextGaussian()
									* var15 + 0.2F;
							var14.motionZ = (float) furnaceRand.nextGaussian()
									* var15;
							par1World.spawnEntityInWorld(var14);
						}
					}
				}

				par1World.func_96440_m(par2, par3, par4, par5);
			}
		}

		super.breakBlock(par1World, par2, par3, par4, par5, par6);
	}

	/**
	 * If this returns true, then comparators facing away from this block will
	 * use the value from getComparatorInputOverride instead of the actual
	 * redstone signal strength.
	 */
	@Override
	public boolean hasComparatorInputOverride() {
		return true;
	}

	/**
	 * If hasComparatorInputOverride returns true, the return value from this is
	 * used instead of the redstone signal strength when this block inputs to a
	 * comparator.
	 */
	@Override
	public int getComparatorInputOverride(final World par1World,
			final int par2, final int par3, final int par4, final int par5) {
		return Container.calcRedstoneFromInventory((IInventory) par1World
				.getBlockTileEntity(par2, par3, par4));
	}

	/**
	 * only called by clickMiddleMouseButton , and passed to
	 * inventory.setCurrentItem (along with isCreative)
	 */
	@Override
	public int idPicked(final World par1World, final int par2, final int par3,
			final int par4) {
		return Block.furnaceIdle.blockID;
	}
}
