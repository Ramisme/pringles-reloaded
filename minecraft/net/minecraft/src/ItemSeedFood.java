package net.minecraft.src;

public class ItemSeedFood extends ItemFood {
	/** Block ID of the crop this seed food should place. */
	private final int cropId;

	/** Block ID of the soil this seed food should be planted on. */
	private final int soilId;

	public ItemSeedFood(final int par1, final int par2, final float par3,
			final int par4, final int par5) {
		super(par1, par2, par3, false);
		cropId = par4;
		soilId = par5;
	}

	/**
	 * Callback for item usage. If the item does something special on right
	 * clicking, he will have one of those. Return True if something happen and
	 * false if it don't. This is for ITEMS, not BLOCKS
	 */
	@Override
	public boolean onItemUse(final ItemStack par1ItemStack,
			final EntityPlayer par2EntityPlayer, final World par3World,
			final int par4, final int par5, final int par6, final int par7,
			final float par8, final float par9, final float par10) {
		if (par7 != 1) {
			return false;
		} else if (par2EntityPlayer.canPlayerEdit(par4, par5, par6, par7,
				par1ItemStack)
				&& par2EntityPlayer.canPlayerEdit(par4, par5 + 1, par6, par7,
						par1ItemStack)) {
			final int var11 = par3World.getBlockId(par4, par5, par6);

			if (var11 == soilId && par3World.isAirBlock(par4, par5 + 1, par6)) {
				par3World.setBlock(par4, par5 + 1, par6, cropId);
				--par1ItemStack.stackSize;
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}
