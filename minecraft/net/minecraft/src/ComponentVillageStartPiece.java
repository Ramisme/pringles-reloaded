package net.minecraft.src;

import java.util.ArrayList;
import java.util.Random;

public class ComponentVillageStartPiece extends ComponentVillageWell {
	public final WorldChunkManager worldChunkMngr;

	/** Boolean that determines if the village is in a desert or not. */
	public final boolean inDesert;

	/** World terrain type, 0 for normal, 1 for flap map */
	public final int terrainType;
	public StructureVillagePieceWeight structVillagePieceWeight;

	/**
	 * Contains List of all spawnable Structure Piece Weights. If no more Pieces
	 * of a type can be spawned, they are removed from this list
	 */
	public ArrayList structureVillageWeightedPieceList;
	public ArrayList field_74932_i = new ArrayList();
	public ArrayList field_74930_j = new ArrayList();

	public ComponentVillageStartPiece(
			final WorldChunkManager par1WorldChunkManager, final int par2,
			final Random par3Random, final int par4, final int par5,
			final ArrayList par6ArrayList, final int par7) {
		super((ComponentVillageStartPiece) null, 0, par3Random, par4, par5);
		worldChunkMngr = par1WorldChunkManager;
		structureVillageWeightedPieceList = par6ArrayList;
		terrainType = par7;
		final BiomeGenBase var8 = par1WorldChunkManager.getBiomeGenAt(par4,
				par5);
		inDesert = var8 == BiomeGenBase.desert
				|| var8 == BiomeGenBase.desertHills;
		startPiece = this;
	}

	public WorldChunkManager getWorldChunkManager() {
		return worldChunkMngr;
	}
}
