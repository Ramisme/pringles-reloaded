package net.minecraft.src;

import java.util.concurrent.Callable;

import net.minecraft.client.Minecraft;

import org.lwjgl.Sys;

public class CallableLWJGLVersion implements Callable {
	/** Reference to the Minecraft object. */
	final Minecraft mc;

	public CallableLWJGLVersion(final Minecraft par1Minecraft) {
		mc = par1Minecraft;
	}

	public String getType() {
		return Sys.getVersion();
	}

	@Override
	public Object call() {
		return getType();
	}
}
