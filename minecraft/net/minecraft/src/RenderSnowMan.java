package net.minecraft.src;

import org.lwjgl.opengl.GL11;

public class RenderSnowMan extends RenderLiving {
	/** A reference to the Snowman model in RenderSnowMan. */
	private final ModelSnowMan snowmanModel;

	public RenderSnowMan() {
		super(new ModelSnowMan(), 0.5F);
		snowmanModel = (ModelSnowMan) super.mainModel;
		setRenderPassModel(snowmanModel);
	}

	/**
	 * Renders this snowman's pumpkin.
	 */
	protected void renderSnowmanPumpkin(final EntitySnowman par1EntitySnowman,
			final float par2) {
		super.renderEquippedItems(par1EntitySnowman, par2);
		final ItemStack var3 = new ItemStack(Block.pumpkin, 1);

		if (var3 != null && var3.getItem().itemID < 256) {
			GL11.glPushMatrix();
			snowmanModel.head.postRender(0.0625F);

			if (RenderBlocks.renderItemIn3d(Block.blocksList[var3.itemID]
					.getRenderType())) {
				final float var4 = 0.625F;
				GL11.glTranslatef(0.0F, -0.34375F, 0.0F);
				GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
				GL11.glScalef(var4, -var4, var4);
			}

			renderManager.itemRenderer.renderItem(par1EntitySnowman, var3, 0);
			GL11.glPopMatrix();
		}
	}

	@Override
	protected void renderEquippedItems(final EntityLiving par1EntityLiving,
			final float par2) {
		renderSnowmanPumpkin((EntitySnowman) par1EntityLiving, par2);
	}
}
