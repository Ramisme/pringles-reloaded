package net.minecraft.src;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

public class GuiMerchant extends GuiContainer {
	/** Instance of IMerchant interface. */
	private final IMerchant theIMerchant;
	private GuiButtonMerchant nextRecipeButtonIndex;
	private GuiButtonMerchant previousRecipeButtonIndex;
	private int currentRecipeIndex = 0;
	private final String field_94082_v;

	public GuiMerchant(final InventoryPlayer par1, final IMerchant par2,
			final World par3World, final String par4) {
		super(new ContainerMerchant(par1, par2, par3World));
		theIMerchant = par2;
		field_94082_v = par4 != null && par4.length() >= 1 ? par4
				: StatCollector.translateToLocal("entity.Villager.name");
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui() {
		super.initGui();
		final int var1 = (width - xSize) / 2;
		final int var2 = (height - ySize) / 2;
		buttonList.add(nextRecipeButtonIndex = new GuiButtonMerchant(1,
				var1 + 120 + 27, var2 + 24 - 1, true));
		buttonList.add(previousRecipeButtonIndex = new GuiButtonMerchant(2,
				var1 + 36 - 19, var2 + 24 - 1, false));
		nextRecipeButtonIndex.enabled = false;
		previousRecipeButtonIndex.enabled = false;
	}

	/**
	 * Draw the foreground layer for the GuiContainer (everything in front of
	 * the items)
	 */
	@Override
	protected void drawGuiContainerForegroundLayer(final int par1,
			final int par2) {
		fontRenderer.drawString(field_94082_v,
				xSize / 2 - fontRenderer.getStringWidth(field_94082_v) / 2, 6,
				4210752);
		fontRenderer.drawString(
				StatCollector.translateToLocal("container.inventory"), 8,
				ySize - 96 + 2, 4210752);
	}

	/**
	 * Called from the main game loop to update the screen.
	 */
	@Override
	public void updateScreen() {
		super.updateScreen();
		final MerchantRecipeList var1 = theIMerchant.getRecipes(mc.thePlayer);

		if (var1 != null) {
			nextRecipeButtonIndex.enabled = currentRecipeIndex < var1.size() - 1;
			previousRecipeButtonIndex.enabled = currentRecipeIndex > 0;
		}
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	@Override
	protected void actionPerformed(final GuiButton par1GuiButton) {
		boolean var2 = false;

		if (par1GuiButton == nextRecipeButtonIndex) {
			++currentRecipeIndex;
			var2 = true;
		} else if (par1GuiButton == previousRecipeButtonIndex) {
			--currentRecipeIndex;
			var2 = true;
		}

		if (var2) {
			((ContainerMerchant) inventorySlots)
					.setCurrentRecipeIndex(currentRecipeIndex);
			final ByteArrayOutputStream var3 = new ByteArrayOutputStream();
			final DataOutputStream var4 = new DataOutputStream(var3);

			try {
				var4.writeInt(currentRecipeIndex);
				mc.getNetHandler().addToSendQueue(
						new Packet250CustomPayload("MC|TrSel", var3
								.toByteArray()));
			} catch (final Exception var6) {
				var6.printStackTrace();
			}
		}
	}

	/**
	 * Draw the background layer for the GuiContainer (everything behind the
	 * items)
	 */
	@Override
	protected void drawGuiContainerBackgroundLayer(final float par1,
			final int par2, final int par3) {
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		mc.renderEngine.bindTexture("/gui/trading.png");
		final int var4 = (width - xSize) / 2;
		final int var5 = (height - ySize) / 2;
		drawTexturedModalRect(var4, var5, 0, 0, xSize, ySize);
		final MerchantRecipeList var6 = theIMerchant.getRecipes(mc.thePlayer);

		if (var6 != null && !var6.isEmpty()) {
			final int var7 = currentRecipeIndex;
			final MerchantRecipe var8 = (MerchantRecipe) var6.get(var7);

			if (var8.func_82784_g()) {
				mc.renderEngine.bindTexture("/gui/trading.png");
				GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
				GL11.glDisable(GL11.GL_LIGHTING);
				drawTexturedModalRect(guiLeft + 83, guiTop + 21, 212, 0, 28, 21);
				drawTexturedModalRect(guiLeft + 83, guiTop + 51, 212, 0, 28, 21);
			}
		}
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(final int par1, final int par2, final float par3) {
		super.drawScreen(par1, par2, par3);
		final MerchantRecipeList var4 = theIMerchant.getRecipes(mc.thePlayer);

		if (var4 != null && !var4.isEmpty()) {
			final int var5 = (width - xSize) / 2;
			final int var6 = (height - ySize) / 2;
			final int var7 = currentRecipeIndex;
			final MerchantRecipe var8 = (MerchantRecipe) var4.get(var7);
			GL11.glPushMatrix();
			final ItemStack var9 = var8.getItemToBuy();
			final ItemStack var10 = var8.getSecondItemToBuy();
			final ItemStack var11 = var8.getItemToSell();
			RenderHelper.enableGUIStandardItemLighting();
			GL11.glDisable(GL11.GL_LIGHTING);
			GL11.glEnable(GL12.GL_RESCALE_NORMAL);
			GL11.glEnable(GL11.GL_COLOR_MATERIAL);
			GL11.glEnable(GL11.GL_LIGHTING);
			GuiContainer.itemRenderer.zLevel = 100.0F;
			GuiContainer.itemRenderer.renderItemAndEffectIntoGUI(fontRenderer,
					mc.renderEngine, var9, var5 + 36, var6 + 24);
			GuiContainer.itemRenderer.renderItemOverlayIntoGUI(fontRenderer,
					mc.renderEngine, var9, var5 + 36, var6 + 24);

			if (var10 != null) {
				GuiContainer.itemRenderer.renderItemAndEffectIntoGUI(
						fontRenderer, mc.renderEngine, var10, var5 + 62,
						var6 + 24);
				GuiContainer.itemRenderer.renderItemOverlayIntoGUI(
						fontRenderer, mc.renderEngine, var10, var5 + 62,
						var6 + 24);
			}

			GuiContainer.itemRenderer.renderItemAndEffectIntoGUI(fontRenderer,
					mc.renderEngine, var11, var5 + 120, var6 + 24);
			GuiContainer.itemRenderer.renderItemOverlayIntoGUI(fontRenderer,
					mc.renderEngine, var11, var5 + 120, var6 + 24);
			GuiContainer.itemRenderer.zLevel = 0.0F;
			GL11.glDisable(GL11.GL_LIGHTING);

			if (isPointInRegion(36, 24, 16, 16, par1, par2)) {
				drawItemStackTooltip(var9, par1, par2);
			} else if (var10 != null
					&& isPointInRegion(62, 24, 16, 16, par1, par2)) {
				drawItemStackTooltip(var10, par1, par2);
			} else if (isPointInRegion(120, 24, 16, 16, par1, par2)) {
				drawItemStackTooltip(var11, par1, par2);
			}

			GL11.glPopMatrix();
			GL11.glEnable(GL11.GL_LIGHTING);
			GL11.glEnable(GL11.GL_DEPTH_TEST);
			RenderHelper.enableStandardItemLighting();
		}
	}

	/**
	 * Gets the Instance of IMerchant interface.
	 */
	public IMerchant getIMerchant() {
		return theIMerchant;
	}
}
