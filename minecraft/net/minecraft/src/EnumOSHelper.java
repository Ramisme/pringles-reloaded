package net.minecraft.src;

public class EnumOSHelper {
	public static final int[] field_90049_a = new int[EnumOS.values().length];

	static {
		try {
			EnumOSHelper.field_90049_a[EnumOS.LINUX.ordinal()] = 1;
		} catch (final NoSuchFieldError var4) {
			;
		}

		try {
			EnumOSHelper.field_90049_a[EnumOS.SOLARIS.ordinal()] = 2;
		} catch (final NoSuchFieldError var3) {
			;
		}

		try {
			EnumOSHelper.field_90049_a[EnumOS.WINDOWS.ordinal()] = 3;
		} catch (final NoSuchFieldError var2) {
			;
		}

		try {
			EnumOSHelper.field_90049_a[EnumOS.MACOS.ordinal()] = 4;
		} catch (final NoSuchFieldError var1) {
			;
		}
	}
}
