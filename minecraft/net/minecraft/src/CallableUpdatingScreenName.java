package net.minecraft.src;

import java.util.concurrent.Callable;

import net.minecraft.client.Minecraft;

public class CallableUpdatingScreenName implements Callable {
	final Minecraft theMinecraft;

	public CallableUpdatingScreenName(final Minecraft par1Minecraft) {
		theMinecraft = par1Minecraft;
	}

	public String callUpdatingScreenName() {
		return theMinecraft.currentScreen.getClass().getCanonicalName();
	}

	@Override
	public Object call() {
		return callUpdatingScreenName();
	}
}
