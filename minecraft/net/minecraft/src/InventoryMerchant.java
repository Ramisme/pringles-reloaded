package net.minecraft.src;

public class InventoryMerchant implements IInventory {
	private final IMerchant theMerchant;
	private final ItemStack[] theInventory = new ItemStack[3];
	private final EntityPlayer thePlayer;
	private MerchantRecipe currentRecipe;
	private int currentRecipeIndex;

	public InventoryMerchant(final EntityPlayer par1EntityPlayer,
			final IMerchant par2IMerchant) {
		thePlayer = par1EntityPlayer;
		theMerchant = par2IMerchant;
	}

	/**
	 * Returns the number of slots in the inventory.
	 */
	@Override
	public int getSizeInventory() {
		return theInventory.length;
	}

	/**
	 * Returns the stack in slot i
	 */
	@Override
	public ItemStack getStackInSlot(final int par1) {
		return theInventory[par1];
	}

	/**
	 * Removes from an inventory slot (first arg) up to a specified number
	 * (second arg) of items and returns them in a new stack.
	 */
	@Override
	public ItemStack decrStackSize(final int par1, final int par2) {
		if (theInventory[par1] != null) {
			ItemStack var3;

			if (par1 == 2) {
				var3 = theInventory[par1];
				theInventory[par1] = null;
				return var3;
			} else if (theInventory[par1].stackSize <= par2) {
				var3 = theInventory[par1];
				theInventory[par1] = null;

				if (inventoryResetNeededOnSlotChange(par1)) {
					resetRecipeAndSlots();
				}

				return var3;
			} else {
				var3 = theInventory[par1].splitStack(par2);

				if (theInventory[par1].stackSize == 0) {
					theInventory[par1] = null;
				}

				if (inventoryResetNeededOnSlotChange(par1)) {
					resetRecipeAndSlots();
				}

				return var3;
			}
		} else {
			return null;
		}
	}

	/**
	 * if par1 slot has changed, does resetRecipeAndSlots need to be called?
	 */
	private boolean inventoryResetNeededOnSlotChange(final int par1) {
		return par1 == 0 || par1 == 1;
	}

	/**
	 * When some containers are closed they call this on each slot, then drop
	 * whatever it returns as an EntityItem - like when you close a workbench
	 * GUI.
	 */
	@Override
	public ItemStack getStackInSlotOnClosing(final int par1) {
		if (theInventory[par1] != null) {
			final ItemStack var2 = theInventory[par1];
			theInventory[par1] = null;
			return var2;
		} else {
			return null;
		}
	}

	/**
	 * Sets the given item stack to the specified slot in the inventory (can be
	 * crafting or armor sections).
	 */
	@Override
	public void setInventorySlotContents(final int par1,
			final ItemStack par2ItemStack) {
		theInventory[par1] = par2ItemStack;

		if (par2ItemStack != null
				&& par2ItemStack.stackSize > getInventoryStackLimit()) {
			par2ItemStack.stackSize = getInventoryStackLimit();
		}

		if (inventoryResetNeededOnSlotChange(par1)) {
			resetRecipeAndSlots();
		}
	}

	/**
	 * Returns the name of the inventory.
	 */
	@Override
	public String getInvName() {
		return "mob.villager";
	}

	/**
	 * If this returns false, the inventory name will be used as an unlocalized
	 * name, and translated into the player's language. Otherwise it will be
	 * used directly.
	 */
	@Override
	public boolean isInvNameLocalized() {
		return false;
	}

	/**
	 * Returns the maximum stack size for a inventory slot. Seems to always be
	 * 64, possibly will be extended. *Isn't this more of a set than a get?*
	 */
	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	/**
	 * Do not make give this method the name canInteractWith because it clashes
	 * with Container
	 */
	@Override
	public boolean isUseableByPlayer(final EntityPlayer par1EntityPlayer) {
		return theMerchant.getCustomer() == par1EntityPlayer;
	}

	@Override
	public void openChest() {
	}

	@Override
	public void closeChest() {
	}

	/**
	 * Returns true if automation is allowed to insert the given stack (ignoring
	 * stack size) into the given slot.
	 */
	@Override
	public boolean isStackValidForSlot(final int par1,
			final ItemStack par2ItemStack) {
		return true;
	}

	/**
	 * Called when an the contents of an Inventory change, usually
	 */
	@Override
	public void onInventoryChanged() {
		resetRecipeAndSlots();
	}

	public void resetRecipeAndSlots() {
		currentRecipe = null;
		ItemStack var1 = theInventory[0];
		ItemStack var2 = theInventory[1];

		if (var1 == null) {
			var1 = var2;
			var2 = null;
		}

		if (var1 == null) {
			setInventorySlotContents(2, (ItemStack) null);
		} else {
			final MerchantRecipeList var3 = theMerchant.getRecipes(thePlayer);

			if (var3 != null) {
				MerchantRecipe var4 = var3.canRecipeBeUsed(var1, var2,
						currentRecipeIndex);

				if (var4 != null && !var4.func_82784_g()) {
					currentRecipe = var4;
					setInventorySlotContents(2, var4.getItemToSell().copy());
				} else if (var2 != null) {
					var4 = var3.canRecipeBeUsed(var2, var1, currentRecipeIndex);

					if (var4 != null && !var4.func_82784_g()) {
						currentRecipe = var4;
						setInventorySlotContents(2, var4.getItemToSell().copy());
					} else {
						setInventorySlotContents(2, (ItemStack) null);
					}
				} else {
					setInventorySlotContents(2, (ItemStack) null);
				}
			}
		}
	}

	public MerchantRecipe getCurrentRecipe() {
		return currentRecipe;
	}

	public void setCurrentRecipeIndex(final int par1) {
		currentRecipeIndex = par1;
		resetRecipeAndSlots();
	}
}
