package net.minecraft.src;

import java.util.Iterator;
import java.util.List;

public class EntityWitch extends EntityMob implements IRangedAttackMob {
	/** List of items a witch should drop on death. */
	private static final int[] witchDrops = new int[] {
			Item.lightStoneDust.itemID, Item.sugar.itemID,
			Item.redstone.itemID, Item.spiderEye.itemID,
			Item.glassBottle.itemID, Item.gunpowder.itemID, Item.stick.itemID,
			Item.stick.itemID };

	/**
	 * Timer used as interval for a witch's attack, decremented every tick if
	 * aggressive and when reaches zero the witch will throw a potion at the
	 * target entity.
	 */
	private int witchAttackTimer = 0;

	public EntityWitch(final World par1World) {
		super(par1World);
		texture = "/mob/villager/witch.png";
		moveSpeed = 0.25F;
		tasks.addTask(1, new EntityAISwimming(this));
		tasks.addTask(2, new EntityAIArrowAttack(this, moveSpeed, 60, 10.0F));
		tasks.addTask(2, new EntityAIWander(this, moveSpeed));
		tasks.addTask(3, new EntityAIWatchClosest(this, EntityPlayer.class,
				8.0F));
		tasks.addTask(3, new EntityAILookIdle(this));
		targetTasks.addTask(1, new EntityAIHurtByTarget(this, false));
		targetTasks.addTask(2, new EntityAINearestAttackableTarget(this,
				EntityPlayer.class, 16.0F, 0, true));
	}

	@Override
	protected void entityInit() {
		super.entityInit();
		getDataWatcher().addObject(21, Byte.valueOf((byte) 0));
	}

	/**
	 * Returns the sound this mob makes while it's alive.
	 */
	@Override
	protected String getLivingSound() {
		return "mob.witch.idle";
	}

	/**
	 * Returns the sound this mob makes when it is hurt.
	 */
	@Override
	protected String getHurtSound() {
		return "mob.witch.hurt";
	}

	/**
	 * Returns the sound this mob makes on death.
	 */
	@Override
	protected String getDeathSound() {
		return "mob.witch.death";
	}

	/**
	 * Set whether this witch is aggressive at an entity.
	 */
	public void setAggressive(final boolean par1) {
		getDataWatcher().updateObject(21, Byte.valueOf((byte) (par1 ? 1 : 0)));
	}

	/**
	 * Return whether this witch is aggressive at an entity.
	 */
	public boolean getAggressive() {
		return getDataWatcher().getWatchableObjectByte(21) == 1;
	}

	@Override
	public int getMaxHealth() {
		return 26;
	}

	/**
	 * Returns true if the newer Entity AI code should be run
	 */
	@Override
	public boolean isAIEnabled() {
		return true;
	}

	/**
	 * Called frequently so the entity can update its state every tick as
	 * required. For example, zombies and skeletons use this to react to
	 * sunlight and start to burn.
	 */
	@Override
	public void onLivingUpdate() {
		if (!worldObj.isRemote) {
			if (getAggressive()) {
				if (witchAttackTimer-- <= 0) {
					setAggressive(false);
					final ItemStack var1 = getHeldItem();
					setCurrentItemOrArmor(0, (ItemStack) null);

					if (var1 != null && var1.itemID == Item.potion.itemID) {
						final List var2 = Item.potion.getEffects(var1);

						if (var2 != null) {
							final Iterator var3 = var2.iterator();

							while (var3.hasNext()) {
								final PotionEffect var4 = (PotionEffect) var3
										.next();
								addPotionEffect(new PotionEffect(var4));
							}
						}
					}
				}
			} else {
				short var5 = -1;

				if (rand.nextFloat() < 0.15F && isBurning()
						&& !this.isPotionActive(Potion.fireResistance)) {
					var5 = 16307;
				} else if (rand.nextFloat() < 0.05F && health < getMaxHealth()) {
					var5 = 16341;
				} else if (rand.nextFloat() < 0.25F
						&& getAttackTarget() != null
						&& !this.isPotionActive(Potion.moveSpeed)
						&& getAttackTarget().getDistanceSqToEntity(this) > 121.0D) {
					var5 = 16274;
				} else if (rand.nextFloat() < 0.25F
						&& getAttackTarget() != null
						&& !this.isPotionActive(Potion.moveSpeed)
						&& getAttackTarget().getDistanceSqToEntity(this) > 121.0D) {
					var5 = 16274;
				}

				if (var5 > -1) {
					setCurrentItemOrArmor(0,
							new ItemStack(Item.potion, 1, var5));
					witchAttackTimer = getHeldItem().getMaxItemUseDuration();
					setAggressive(true);
				}
			}

			if (rand.nextFloat() < 7.5E-4F) {
				worldObj.setEntityState(this, (byte) 15);
			}
		}

		super.onLivingUpdate();
	}

	@Override
	public void handleHealthUpdate(final byte par1) {
		if (par1 == 15) {
			for (int var2 = 0; var2 < rand.nextInt(35) + 10; ++var2) {
				worldObj.spawnParticle("witchMagic", posX + rand.nextGaussian()
						* 0.12999999523162842D,
						boundingBox.maxY + 0.5D + rand.nextGaussian()
								* 0.12999999523162842D,
						posZ + rand.nextGaussian() * 0.12999999523162842D,
						0.0D, 0.0D, 0.0D);
			}
		} else {
			super.handleHealthUpdate(par1);
		}
	}

	/**
	 * Reduces damage, depending on potions
	 */
	@Override
	protected int applyPotionDamageCalculations(
			final DamageSource par1DamageSource, int par2) {
		par2 = super.applyPotionDamageCalculations(par1DamageSource, par2);

		if (par1DamageSource.getEntity() == this) {
			par2 = 0;
		}

		if (par1DamageSource.isMagicDamage()) {
			par2 = (int) (par2 * 0.15D);
		}

		return par2;
	}

	/**
	 * This method returns a value to be applied directly to entity speed, this
	 * factor is less than 1 when a slowdown potion effect is applied, more than
	 * 1 when a haste potion effect is applied and 2 for fleeing entities.
	 */
	@Override
	public float getSpeedModifier() {
		float var1 = super.getSpeedModifier();

		if (getAggressive()) {
			var1 *= 0.75F;
		}

		return var1;
	}

	/**
	 * Drop 0-2 items of this living's type. @param par1 - Whether this entity
	 * has recently been hit by a player. @param par2 - Level of Looting used to
	 * kill this mob.
	 */
	@Override
	protected void dropFewItems(final boolean par1, final int par2) {
		final int var3 = rand.nextInt(3) + 1;

		for (int var4 = 0; var4 < var3; ++var4) {
			int var5 = rand.nextInt(3);
			final int var6 = EntityWitch.witchDrops[rand
					.nextInt(EntityWitch.witchDrops.length)];

			if (par2 > 0) {
				var5 += rand.nextInt(par2 + 1);
			}

			for (int var7 = 0; var7 < var5; ++var7) {
				dropItem(var6, 1);
			}
		}
	}

	/**
	 * Attack the specified entity using a ranged attack.
	 */
	@Override
	public void attackEntityWithRangedAttack(
			final EntityLiving par1EntityLiving, final float par2) {
		if (!getAggressive()) {
			final EntityPotion var3 = new EntityPotion(worldObj, this, 32732);
			var3.rotationPitch -= -20.0F;
			final double var4 = par1EntityLiving.posX
					+ par1EntityLiving.motionX - posX;
			final double var6 = par1EntityLiving.posY
					+ par1EntityLiving.getEyeHeight() - 1.100000023841858D
					- posY;
			final double var8 = par1EntityLiving.posZ
					+ par1EntityLiving.motionZ - posZ;
			final float var10 = MathHelper.sqrt_double(var4 * var4 + var8
					* var8);

			if (var10 >= 8.0F
					&& !par1EntityLiving.isPotionActive(Potion.moveSlowdown)) {
				var3.setPotionDamage(32698);
			} else if (par1EntityLiving.getHealth() >= 8
					&& !par1EntityLiving.isPotionActive(Potion.poison)) {
				var3.setPotionDamage(32660);
			} else if (var10 <= 3.0F
					&& !par1EntityLiving.isPotionActive(Potion.weakness)
					&& rand.nextFloat() < 0.25F) {
				var3.setPotionDamage(32696);
			}

			var3.setThrowableHeading(var4, var6 + var10 * 0.2F, var8, 0.75F,
					8.0F);
			worldObj.spawnEntityInWorld(var3);
		}
	}
}
