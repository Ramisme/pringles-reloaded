package net.minecraft.src;

public abstract class EntityMob extends EntityCreature implements IMob {
	public EntityMob(final World par1World) {
		super(par1World);
		experienceValue = 5;
	}

	/**
	 * Called frequently so the entity can update its state every tick as
	 * required. For example, zombies and skeletons use this to react to
	 * sunlight and start to burn.
	 */
	@Override
	public void onLivingUpdate() {
		updateArmSwingProgress();
		final float var1 = getBrightness(1.0F);

		if (var1 > 0.5F) {
			entityAge += 2;
		}

		super.onLivingUpdate();
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate() {
		super.onUpdate();

		if (!worldObj.isRemote && worldObj.difficultySetting == 0) {
			setDead();
		}
	}

	/**
	 * Finds the closest player within 16 blocks to attack, or null if this
	 * Entity isn't interested in attacking (Animals, Spiders at day, peaceful
	 * PigZombies).
	 */
	@Override
	protected Entity findPlayerToAttack() {
		final EntityPlayer var1 = worldObj.getClosestVulnerablePlayerToEntity(
				this, 16.0D);
		return var1 != null && canEntityBeSeen(var1) ? var1 : null;
	}

	/**
	 * Called when the entity is attacked.
	 */
	@Override
	public boolean attackEntityFrom(final DamageSource par1DamageSource,
			final int par2) {
		if (isEntityInvulnerable()) {
			return false;
		} else if (super.attackEntityFrom(par1DamageSource, par2)) {
			final Entity var3 = par1DamageSource.getEntity();

			if (riddenByEntity != var3 && ridingEntity != var3) {
				if (var3 != this) {
					entityToAttack = var3;
				}

				return true;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}

	@Override
	public boolean attackEntityAsMob(final Entity par1Entity) {
		int var2 = getAttackStrength(par1Entity);

		if (this.isPotionActive(Potion.damageBoost)) {
			var2 += 3 << getActivePotionEffect(Potion.damageBoost)
					.getAmplifier();
		}

		if (this.isPotionActive(Potion.weakness)) {
			var2 -= 2 << getActivePotionEffect(Potion.weakness).getAmplifier();
		}

		int var3 = 0;

		if (par1Entity instanceof EntityLiving) {
			var2 += EnchantmentHelper.getEnchantmentModifierLiving(this,
					(EntityLiving) par1Entity);
			var3 += EnchantmentHelper.getKnockbackModifier(this,
					(EntityLiving) par1Entity);
		}

		final boolean var4 = par1Entity.attackEntityFrom(
				DamageSource.causeMobDamage(this), var2);

		if (var4) {
			if (var3 > 0) {
				par1Entity.addVelocity(
						-MathHelper.sin(rotationYaw * (float) Math.PI / 180.0F)
								* var3 * 0.5F, 0.1D,
						MathHelper.cos(rotationYaw * (float) Math.PI / 180.0F)
								* var3 * 0.5F);
				motionX *= 0.6D;
				motionZ *= 0.6D;
			}

			final int var5 = EnchantmentHelper.getFireAspectModifier(this);

			if (var5 > 0) {
				par1Entity.setFire(var5 * 4);
			}

			if (par1Entity instanceof EntityLiving) {
				EnchantmentThorns.func_92096_a(this, (EntityLiving) par1Entity,
						rand);
			}
		}

		return var4;
	}

	/**
	 * Basic mob attack. Default to touch of death in EntityCreature. Overridden
	 * by each mob to define their attack.
	 */
	@Override
	protected void attackEntity(final Entity par1Entity, final float par2) {
		if (attackTime <= 0 && par2 < 2.0F
				&& par1Entity.boundingBox.maxY > boundingBox.minY
				&& par1Entity.boundingBox.minY < boundingBox.maxY) {
			attackTime = 20;
			attackEntityAsMob(par1Entity);
		}
	}

	/**
	 * Takes a coordinate in and returns a weight to determine how likely this
	 * creature will try to path to the block. Args: x, y, z
	 */
	@Override
	public float getBlockPathWeight(final int par1, final int par2,
			final int par3) {
		return 0.5F - worldObj.getLightBrightness(par1, par2, par3);
	}

	/**
	 * Checks to make sure the light is not too bright where the mob is spawning
	 */
	protected boolean isValidLightLevel() {
		final int var1 = MathHelper.floor_double(posX);
		final int var2 = MathHelper.floor_double(boundingBox.minY);
		final int var3 = MathHelper.floor_double(posZ);

		if (worldObj.getSavedLightValue(EnumSkyBlock.Sky, var1, var2, var3) > rand
				.nextInt(32)) {
			return false;
		} else {
			int var4 = worldObj.getBlockLightValue(var1, var2, var3);

			if (worldObj.isThundering()) {
				final int var5 = worldObj.skylightSubtracted;
				worldObj.skylightSubtracted = 10;
				var4 = worldObj.getBlockLightValue(var1, var2, var3);
				worldObj.skylightSubtracted = var5;
			}

			return var4 <= rand.nextInt(8);
		}
	}

	/**
	 * Checks if the entity's current position is a valid location to spawn this
	 * entity.
	 */
	@Override
	public boolean getCanSpawnHere() {
		return isValidLightLevel() && super.getCanSpawnHere();
	}

	/**
	 * Returns the amount of damage a mob should deal.
	 */
	public int getAttackStrength(final Entity par1Entity) {
		return 2;
	}
}
