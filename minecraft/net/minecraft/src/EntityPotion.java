package net.minecraft.src;

import java.util.Iterator;
import java.util.List;

public class EntityPotion extends EntityThrowable {
	/**
	 * The damage value of the thrown potion that this EntityPotion represents.
	 */
	private ItemStack potionDamage;

	public EntityPotion(final World par1World) {
		super(par1World);
	}

	public EntityPotion(final World par1World,
			final EntityLiving par2EntityLiving, final int par3) {
		this(par1World, par2EntityLiving, new ItemStack(Item.potion, 1, par3));
	}

	public EntityPotion(final World par1World,
			final EntityLiving par2EntityLiving, final ItemStack par3ItemStack) {
		super(par1World, par2EntityLiving);
		potionDamage = par3ItemStack;
	}

	public EntityPotion(final World par1World, final double par2,
			final double par4, final double par6, final int par8) {
		this(par1World, par2, par4, par6, new ItemStack(Item.potion, 1, par8));
	}

	public EntityPotion(final World par1World, final double par2,
			final double par4, final double par6, final ItemStack par8ItemStack) {
		super(par1World, par2, par4, par6);
		potionDamage = par8ItemStack;
	}

	/**
	 * Gets the amount of gravity to apply to the thrown entity with each tick.
	 */
	@Override
	protected float getGravityVelocity() {
		return 0.05F;
	}

	@Override
	protected float func_70182_d() {
		return 0.5F;
	}

	@Override
	protected float func_70183_g() {
		return -20.0F;
	}

	public void setPotionDamage(final int par1) {
		if (potionDamage == null) {
			potionDamage = new ItemStack(Item.potion, 1, 0);
		}

		potionDamage.setItemDamage(par1);
	}

	/**
	 * Returns the damage value of the thrown potion that this EntityPotion
	 * represents.
	 */
	public int getPotionDamage() {
		if (potionDamage == null) {
			potionDamage = new ItemStack(Item.potion, 1, 0);
		}

		return potionDamage.getItemDamage();
	}

	/**
	 * Called when this EntityThrowable hits a block or entity.
	 */
	@Override
	protected void onImpact(final MovingObjectPosition par1MovingObjectPosition) {
		if (!worldObj.isRemote) {
			final List var2 = Item.potion.getEffects(potionDamage);

			if (var2 != null && !var2.isEmpty()) {
				final AxisAlignedBB var3 = boundingBox.expand(4.0D, 2.0D, 4.0D);
				final List var4 = worldObj.getEntitiesWithinAABB(
						EntityLiving.class, var3);

				if (var4 != null && !var4.isEmpty()) {
					final Iterator var5 = var4.iterator();

					while (var5.hasNext()) {
						final EntityLiving var6 = (EntityLiving) var5.next();
						final double var7 = getDistanceSqToEntity(var6);

						if (var7 < 16.0D) {
							double var9 = 1.0D - Math.sqrt(var7) / 4.0D;

							if (var6 == par1MovingObjectPosition.entityHit) {
								var9 = 1.0D;
							}

							final Iterator var11 = var2.iterator();

							while (var11.hasNext()) {
								final PotionEffect var12 = (PotionEffect) var11
										.next();
								final int var13 = var12.getPotionID();

								if (Potion.potionTypes[var13].isInstant()) {
									Potion.potionTypes[var13].affectEntity(
											getThrower(), var6,
											var12.getAmplifier(), var9);
								} else {
									final int var14 = (int) (var9
											* var12.getDuration() + 0.5D);

									if (var14 > 20) {
										var6.addPotionEffect(new PotionEffect(
												var13, var14, var12
														.getAmplifier()));
									}
								}
							}
						}
					}
				}
			}

			worldObj.playAuxSFX(2002, (int) Math.round(posX),
					(int) Math.round(posY), (int) Math.round(posZ),
					getPotionDamage());
			setDead();
		}
	}

	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	@Override
	public void readEntityFromNBT(final NBTTagCompound par1NBTTagCompound) {
		super.readEntityFromNBT(par1NBTTagCompound);

		if (par1NBTTagCompound.hasKey("Potion")) {
			potionDamage = ItemStack.loadItemStackFromNBT(par1NBTTagCompound
					.getCompoundTag("Potion"));
		} else {
			setPotionDamage(par1NBTTagCompound.getInteger("potionValue"));
		}

		if (potionDamage == null) {
			setDead();
		}
	}

	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	@Override
	public void writeEntityToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeEntityToNBT(par1NBTTagCompound);

		if (potionDamage != null) {
			par1NBTTagCompound.setCompoundTag("Potion",
					potionDamage.writeToNBT(new NBTTagCompound()));
		}
	}
}
