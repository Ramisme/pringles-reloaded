package net.minecraft.src;

public class StepSound {
	public final String stepSoundName;
	public final float stepSoundVolume;
	public final float stepSoundPitch;

	public StepSound(final String par1Str, final float par2, final float par3) {
		stepSoundName = par1Str;
		stepSoundVolume = par2;
		stepSoundPitch = par3;
	}

	public float getVolume() {
		return stepSoundVolume;
	}

	public float getPitch() {
		return stepSoundPitch;
	}

	/**
	 * Used when a block breaks, EXA: Player break, Shep eating grass, etc..
	 */
	public String getBreakSound() {
		return "dig." + stepSoundName;
	}

	/**
	 * Used when a entity walks over, or otherwise interacts with the block.
	 */
	public String getStepSound() {
		return "step." + stepSoundName;
	}

	/**
	 * Used when a player places a block.
	 */
	public String getPlaceSound() {
		return getBreakSound();
	}
}
