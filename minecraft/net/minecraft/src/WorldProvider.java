package net.minecraft.src;

public abstract class WorldProvider {
	/** world object being used */
	public World worldObj;
	public WorldType terrainType;
	public String field_82913_c;

	/** World chunk manager being used to generate chunks */
	public WorldChunkManager worldChunkMgr;

	/**
	 * States whether the Hell world provider is used(true) or if the normal
	 * world provider is used(false)
	 */
	public boolean isHellWorld = false;

	/**
	 * A boolean that tells if a world does not have a sky. Used in calculating
	 * weather and skylight
	 */
	public boolean hasNoSky = false;

	/** Light to brightness conversion table */
	public float[] lightBrightnessTable = new float[16];

	/** The id for the dimension (ex. -1: Nether, 0: Overworld, 1: The End) */
	public int dimensionId = 0;

	/** Array for sunrise/sunset colors (RGBA) */
	private final float[] colorsSunriseSunset = new float[4];

	/**
	 * associate an existing world with a World provider, and setup its
	 * lightbrightness table
	 */
	public final void registerWorld(final World par1World) {
		worldObj = par1World;
		terrainType = par1World.getWorldInfo().getTerrainType();
		field_82913_c = par1World.getWorldInfo().getGeneratorOptions();
		registerWorldChunkManager();
		generateLightBrightnessTable();
	}

	/**
	 * Creates the light to brightness table
	 */
	protected void generateLightBrightnessTable() {
		final float var1 = 0.0F;

		for (int var2 = 0; var2 <= 15; ++var2) {
			final float var3 = 1.0F - var2 / 15.0F;
			lightBrightnessTable[var2] = (1.0F - var3) / (var3 * 3.0F + 1.0F)
					* (1.0F - var1) + var1;
		}
	}

	/**
	 * creates a new world chunk manager for WorldProvider
	 */
	protected void registerWorldChunkManager() {
		if (worldObj.getWorldInfo().getTerrainType() == WorldType.FLAT) {
			final FlatGeneratorInfo var1 = FlatGeneratorInfo
					.createFlatGeneratorFromString(worldObj.getWorldInfo()
							.getGeneratorOptions());
			worldChunkMgr = new WorldChunkManagerHell(
					BiomeGenBase.biomeList[var1.getBiome()], 0.5F, 0.5F);
		} else {
			worldChunkMgr = new WorldChunkManager(worldObj);
		}
	}

	/**
	 * Returns a new chunk provider which generates chunks for this world
	 */
	public IChunkProvider createChunkGenerator() {
		return terrainType == WorldType.FLAT ? new ChunkProviderFlat(worldObj,
				worldObj.getSeed(), worldObj.getWorldInfo()
						.isMapFeaturesEnabled(), field_82913_c)
				: new ChunkProviderGenerate(worldObj, worldObj.getSeed(),
						worldObj.getWorldInfo().isMapFeaturesEnabled());
	}

	/**
	 * Will check if the x, z position specified is alright to be set as the map
	 * spawn point
	 */
	public boolean canCoordinateBeSpawn(final int par1, final int par2) {
		final int var3 = worldObj.getFirstUncoveredBlock(par1, par2);
		return var3 == Block.grass.blockID;
	}

	/**
	 * Calculates the angle of sun and moon in the sky relative to a specified
	 * time (usually worldTime)
	 */
	public float calculateCelestialAngle(final long par1, final float par3) {
		final int var4 = (int) (par1 % 24000L);
		float var5 = (var4 + par3) / 24000.0F - 0.25F;

		if (var5 < 0.0F) {
			++var5;
		}

		if (var5 > 1.0F) {
			--var5;
		}

		final float var6 = var5;
		var5 = 1.0F - (float) ((Math.cos(var5 * Math.PI) + 1.0D) / 2.0D);
		var5 = var6 + (var5 - var6) / 3.0F;
		return var5;
	}

	public int getMoonPhase(final long par1) {
		return (int) (par1 / 24000L) % 8;
	}

	/**
	 * Returns 'true' if in the "main surface world", but 'false' if in the
	 * Nether or End dimensions.
	 */
	public boolean isSurfaceWorld() {
		return true;
	}

	/**
	 * Returns array with sunrise/sunset colors
	 */
	public float[] calcSunriseSunsetColors(final float par1, final float par2) {
		final float var3 = 0.4F;
		final float var4 = MathHelper.cos(par1 * (float) Math.PI * 2.0F) - 0.0F;
		final float var5 = -0.0F;

		if (var4 >= var5 - var3 && var4 <= var5 + var3) {
			final float var6 = (var4 - var5) / var3 * 0.5F + 0.5F;
			float var7 = 1.0F - (1.0F - MathHelper.sin(var6 * (float) Math.PI)) * 0.99F;
			var7 *= var7;
			colorsSunriseSunset[0] = var6 * 0.3F + 0.7F;
			colorsSunriseSunset[1] = var6 * var6 * 0.7F + 0.2F;
			colorsSunriseSunset[2] = var6 * var6 * 0.0F + 0.2F;
			colorsSunriseSunset[3] = var7;
			return colorsSunriseSunset;
		} else {
			return null;
		}
	}

	/**
	 * Return Vec3D with biome specific fog color
	 */
	public Vec3 getFogColor(final float par1, final float par2) {
		float var3 = MathHelper.cos(par1 * (float) Math.PI * 2.0F) * 2.0F + 0.5F;

		if (var3 < 0.0F) {
			var3 = 0.0F;
		}

		if (var3 > 1.0F) {
			var3 = 1.0F;
		}

		float var4 = 0.7529412F;
		float var5 = 0.84705883F;
		float var6 = 1.0F;
		var4 *= var3 * 0.94F + 0.06F;
		var5 *= var3 * 0.94F + 0.06F;
		var6 *= var3 * 0.91F + 0.09F;
		return worldObj.getWorldVec3Pool().getVecFromPool(var4, var5, var6);
	}

	/**
	 * True if the player can respawn in this dimension (true = overworld, false
	 * = nether).
	 */
	public boolean canRespawnHere() {
		return true;
	}

	public static WorldProvider getProviderForDimension(final int par0) {
		return par0 == -1 ? new WorldProviderHell()
				: par0 == 0 ? new WorldProviderSurface()
						: par0 == 1 ? new WorldProviderEnd() : null;
	}

	/**
	 * the y level at which clouds are rendered.
	 */
	public float getCloudHeight() {
		return 128.0F;
	}

	public boolean isSkyColored() {
		return true;
	}

	/**
	 * Gets the hard-coded portal location to use when entering this dimension.
	 */
	public ChunkCoordinates getEntrancePortalLocation() {
		return null;
	}

	public int getAverageGroundLevel() {
		return terrainType == WorldType.FLAT ? 4 : 64;
	}

	/**
	 * returns true if this dimension is supposed to display void particles and
	 * pull in the far plane based on the user's Y offset.
	 */
	public boolean getWorldHasVoidParticles() {
		return terrainType != WorldType.FLAT && !hasNoSky;
	}

	/**
	 * Returns a double value representing the Y value relative to the top of
	 * the map at which void fog is at its maximum. The default factor of
	 * 0.03125 relative to 256, for example, means the void fog will be at its
	 * maximum at (256*0.03125), or 8.
	 */
	public double getVoidFogYFactor() {
		return terrainType == WorldType.FLAT ? 1.0D : 0.03125D;
	}

	/**
	 * Returns true if the given X,Z coordinate should show environmental fog.
	 */
	public boolean doesXZShowFog(final int par1, final int par2) {
		return false;
	}

	/**
	 * Returns the dimension's name, e.g. "The End", "Nether", or "Overworld".
	 */
	public abstract String getDimensionName();
}
