package net.minecraft.src;

import java.util.Comparator;

public class PlayerPositionComparator implements Comparator {
	private final ChunkCoordinates theChunkCoordinates;

	public PlayerPositionComparator(final ChunkCoordinates par1ChunkCoordinates) {
		theChunkCoordinates = par1ChunkCoordinates;
	}

	/**
	 * Compare the position of two players.
	 */
	public int comparePlayers(final EntityPlayerMP par1EntityPlayerMP,
			final EntityPlayerMP par2EntityPlayerMP) {
		final double var3 = par1EntityPlayerMP.getDistanceSq(
				theChunkCoordinates.posX, theChunkCoordinates.posY,
				theChunkCoordinates.posZ);
		final double var5 = par2EntityPlayerMP.getDistanceSq(
				theChunkCoordinates.posX, theChunkCoordinates.posY,
				theChunkCoordinates.posZ);
		return var3 < var5 ? -1 : var3 > var5 ? 1 : 0;
	}

	@Override
	public int compare(final Object par1Obj, final Object par2Obj) {
		return comparePlayers((EntityPlayerMP) par1Obj,
				(EntityPlayerMP) par2Obj);
	}
}
