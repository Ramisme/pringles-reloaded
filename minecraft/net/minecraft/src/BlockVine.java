package net.minecraft.src;

import java.util.Random;

public class BlockVine extends Block {
	public BlockVine(final int par1) {
		super(par1, Material.vine);
		setTickRandomly(true);
		setCreativeTab(CreativeTabs.tabDecorations);
	}

	/**
	 * Sets the block's bounds for rendering it as an item
	 */
	@Override
	public void setBlockBoundsForItemRender() {
		setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return 20;
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	/**
	 * Updates the blocks bounds based on its current state. Args: world, x, y,
	 * z
	 */
	@Override
	public void setBlockBoundsBasedOnState(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		final int var6 = par1IBlockAccess.getBlockMetadata(par2, par3, par4);
		float var7 = 1.0F;
		float var8 = 1.0F;
		float var9 = 1.0F;
		float var10 = 0.0F;
		float var11 = 0.0F;
		float var12 = 0.0F;
		boolean var13 = var6 > 0;

		if ((var6 & 2) != 0) {
			var10 = Math.max(var10, 0.0625F);
			var7 = 0.0F;
			var8 = 0.0F;
			var11 = 1.0F;
			var9 = 0.0F;
			var12 = 1.0F;
			var13 = true;
		}

		if ((var6 & 8) != 0) {
			var7 = Math.min(var7, 0.9375F);
			var10 = 1.0F;
			var8 = 0.0F;
			var11 = 1.0F;
			var9 = 0.0F;
			var12 = 1.0F;
			var13 = true;
		}

		if ((var6 & 4) != 0) {
			var12 = Math.max(var12, 0.0625F);
			var9 = 0.0F;
			var7 = 0.0F;
			var10 = 1.0F;
			var8 = 0.0F;
			var11 = 1.0F;
			var13 = true;
		}

		if ((var6 & 1) != 0) {
			var9 = Math.min(var9, 0.9375F);
			var12 = 1.0F;
			var7 = 0.0F;
			var10 = 1.0F;
			var8 = 0.0F;
			var11 = 1.0F;
			var13 = true;
		}

		if (!var13
				&& canBePlacedOn(par1IBlockAccess.getBlockId(par2, par3 + 1,
						par4))) {
			var8 = Math.min(var8, 0.9375F);
			var11 = 1.0F;
			var7 = 0.0F;
			var10 = 1.0F;
			var9 = 0.0F;
			var12 = 1.0F;
		}

		setBlockBounds(var7, var8, var9, var10, var11, var12);
	}

	/**
	 * Returns a bounding box from the pool of bounding boxes (this means this
	 * box can change after the pool has been cleared to be reused)
	 */
	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(final World par1World,
			final int par2, final int par3, final int par4) {
		return null;
	}

	/**
	 * checks to see if you can place this block can be placed on that side of a
	 * block: BlockLever overrides
	 */
	@Override
	public boolean canPlaceBlockOnSide(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		switch (par5) {
		case 1:
			return canBePlacedOn(par1World.getBlockId(par2, par3 + 1, par4));

		case 2:
			return canBePlacedOn(par1World.getBlockId(par2, par3, par4 + 1));

		case 3:
			return canBePlacedOn(par1World.getBlockId(par2, par3, par4 - 1));

		case 4:
			return canBePlacedOn(par1World.getBlockId(par2 + 1, par3, par4));

		case 5:
			return canBePlacedOn(par1World.getBlockId(par2 - 1, par3, par4));

		default:
			return false;
		}
	}

	/**
	 * returns true if a vine can be placed on that block (checks for render as
	 * normal block and if it is solid)
	 */
	private boolean canBePlacedOn(final int par1) {
		if (par1 == 0) {
			return false;
		} else {
			final Block var2 = Block.blocksList[par1];
			return var2.renderAsNormalBlock()
					&& var2.blockMaterial.blocksMovement();
		}
	}

	/**
	 * Returns if the vine can stay in the world. It also changes the metadata
	 * according to neighboring blocks.
	 */
	private boolean canVineStay(final World par1World, final int par2,
			final int par3, final int par4) {
		final int var5 = par1World.getBlockMetadata(par2, par3, par4);
		int var6 = var5;

		if (var5 > 0) {
			for (int var7 = 0; var7 <= 3; ++var7) {
				final int var8 = 1 << var7;

				if ((var5 & var8) != 0
						&& !canBePlacedOn(par1World.getBlockId(par2
								+ Direction.offsetX[var7], par3, par4
								+ Direction.offsetZ[var7]))
						&& (par1World.getBlockId(par2, par3 + 1, par4) != blockID || (par1World
								.getBlockMetadata(par2, par3 + 1, par4) & var8) == 0)) {
					var6 &= ~var8;
				}
			}
		}

		if (var6 == 0
				&& !canBePlacedOn(par1World.getBlockId(par2, par3 + 1, par4))) {
			return false;
		} else {
			if (var6 != var5) {
				par1World.setBlockMetadataWithNotify(par2, par3, par4, var6, 2);
			}

			return true;
		}
	}

	@Override
	public int getBlockColor() {
		return ColorizerFoliage.getFoliageColorBasic();
	}

	/**
	 * Returns the color this block should be rendered. Used by leaves.
	 */
	@Override
	public int getRenderColor(final int par1) {
		return ColorizerFoliage.getFoliageColorBasic();
	}

	/**
	 * Returns a integer with hex for 0xrrggbb with this color multiplied
	 * against the blocks color. Note only called when first determining what to
	 * render.
	 */
	@Override
	public int colorMultiplier(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		return par1IBlockAccess.getBiomeGenForCoords(par2, par4)
				.getBiomeFoliageColor();
	}

	/**
	 * Lets the block know when one of its neighbor changes. Doesn't know which
	 * neighbor changed (coordinates passed are their own) Args: x, y, z,
	 * neighbor blockID
	 */
	@Override
	public void onNeighborBlockChange(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		if (!par1World.isRemote && !canVineStay(par1World, par2, par3, par4)) {
			dropBlockAsItem(par1World, par2, par3, par4,
					par1World.getBlockMetadata(par2, par3, par4), 0);
			par1World.setBlockToAir(par2, par3, par4);
		}
	}

	/**
	 * Ticks the block if it's been scheduled
	 */
	@Override
	public void updateTick(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		if (!par1World.isRemote && par1World.rand.nextInt(4) == 0) {
			final byte var6 = 4;
			int var7 = 5;
			boolean var8 = false;
			int var9;
			int var10;
			int var11;
			label138:

			for (var9 = par2 - var6; var9 <= par2 + var6; ++var9) {
				for (var10 = par4 - var6; var10 <= par4 + var6; ++var10) {
					for (var11 = par3 - 1; var11 <= par3 + 1; ++var11) {
						if (par1World.getBlockId(var9, var11, var10) == blockID) {
							--var7;

							if (var7 <= 0) {
								var8 = true;
								break label138;
							}
						}
					}
				}
			}

			var9 = par1World.getBlockMetadata(par2, par3, par4);
			var10 = par1World.rand.nextInt(6);
			var11 = Direction.facingToDirection[var10];
			int var12;
			int var13;

			if (var10 == 1 && par3 < 255
					&& par1World.isAirBlock(par2, par3 + 1, par4)) {
				if (var8) {
					return;
				}

				var12 = par1World.rand.nextInt(16) & var9;

				if (var12 > 0) {
					for (var13 = 0; var13 <= 3; ++var13) {
						if (!canBePlacedOn(par1World.getBlockId(par2
								+ Direction.offsetX[var13], par3 + 1, par4
								+ Direction.offsetZ[var13]))) {
							var12 &= ~(1 << var13);
						}
					}

					if (var12 > 0) {
						par1World.setBlock(par2, par3 + 1, par4, blockID,
								var12, 2);
					}
				}
			} else {
				int var14;

				if (var10 >= 2 && var10 <= 5 && (var9 & 1 << var11) == 0) {
					if (var8) {
						return;
					}

					var12 = par1World.getBlockId(par2
							+ Direction.offsetX[var11], par3, par4
							+ Direction.offsetZ[var11]);

					if (var12 != 0 && Block.blocksList[var12] != null) {
						if (Block.blocksList[var12].blockMaterial.isOpaque()
								&& Block.blocksList[var12]
										.renderAsNormalBlock()) {
							par1World.setBlockMetadataWithNotify(par2, par3,
									par4, var9 | 1 << var11, 2);
						}
					} else {
						var13 = var11 + 1 & 3;
						var14 = var11 + 3 & 3;

						if ((var9 & 1 << var13) != 0
								&& canBePlacedOn(par1World.getBlockId(par2
										+ Direction.offsetX[var11]
										+ Direction.offsetX[var13], par3, par4
										+ Direction.offsetZ[var11]
										+ Direction.offsetZ[var13]))) {
							par1World.setBlock(par2 + Direction.offsetX[var11],
									par3, par4 + Direction.offsetZ[var11],
									blockID, 1 << var13, 2);
						} else if ((var9 & 1 << var14) != 0
								&& canBePlacedOn(par1World.getBlockId(par2
										+ Direction.offsetX[var11]
										+ Direction.offsetX[var14], par3, par4
										+ Direction.offsetZ[var11]
										+ Direction.offsetZ[var14]))) {
							par1World.setBlock(par2 + Direction.offsetX[var11],
									par3, par4 + Direction.offsetZ[var11],
									blockID, 1 << var14, 2);
						} else if ((var9 & 1 << var13) != 0
								&& par1World.isAirBlock(par2
										+ Direction.offsetX[var11]
										+ Direction.offsetX[var13], par3, par4
										+ Direction.offsetZ[var11]
										+ Direction.offsetZ[var13])
								&& canBePlacedOn(par1World.getBlockId(par2
										+ Direction.offsetX[var13], par3, par4
										+ Direction.offsetZ[var13]))) {
							par1World.setBlock(par2 + Direction.offsetX[var11]
									+ Direction.offsetX[var13], par3, par4
									+ Direction.offsetZ[var11]
									+ Direction.offsetZ[var13], blockID,
									1 << (var11 + 2 & 3), 2);
						} else if ((var9 & 1 << var14) != 0
								&& par1World.isAirBlock(par2
										+ Direction.offsetX[var11]
										+ Direction.offsetX[var14], par3, par4
										+ Direction.offsetZ[var11]
										+ Direction.offsetZ[var14])
								&& canBePlacedOn(par1World.getBlockId(par2
										+ Direction.offsetX[var14], par3, par4
										+ Direction.offsetZ[var14]))) {
							par1World.setBlock(par2 + Direction.offsetX[var11]
									+ Direction.offsetX[var14], par3, par4
									+ Direction.offsetZ[var11]
									+ Direction.offsetZ[var14], blockID,
									1 << (var11 + 2 & 3), 2);
						} else if (canBePlacedOn(par1World.getBlockId(par2
								+ Direction.offsetX[var11], par3 + 1, par4
								+ Direction.offsetZ[var11]))) {
							par1World.setBlock(par2 + Direction.offsetX[var11],
									par3, par4 + Direction.offsetZ[var11],
									blockID, 0, 2);
						}
					}
				} else if (par3 > 1) {
					var12 = par1World.getBlockId(par2, par3 - 1, par4);

					if (var12 == 0) {
						var13 = par1World.rand.nextInt(16) & var9;

						if (var13 > 0) {
							par1World.setBlock(par2, par3 - 1, par4, blockID,
									var13, 2);
						}
					} else if (var12 == blockID) {
						var13 = par1World.rand.nextInt(16) & var9;
						var14 = par1World
								.getBlockMetadata(par2, par3 - 1, par4);

						if (var14 != (var14 | var13)) {
							par1World.setBlockMetadataWithNotify(par2,
									par3 - 1, par4, var14 | var13, 2);
						}
					}
				}
			}
		}
	}

	/**
	 * Called when a block is placed using its ItemBlock. Args: World, X, Y, Z,
	 * side, hitX, hitY, hitZ, block metadata
	 */
	@Override
	public int onBlockPlaced(final World par1World, final int par2,
			final int par3, final int par4, final int par5, final float par6,
			final float par7, final float par8, final int par9) {
		byte var10 = 0;

		switch (par5) {
		case 2:
			var10 = 1;
			break;

		case 3:
			var10 = 4;
			break;

		case 4:
			var10 = 8;
			break;

		case 5:
			var10 = 2;
		}

		return var10 != 0 ? var10 : par9;
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return 0;
	}

	/**
	 * Returns the quantity of items to drop on block destruction.
	 */
	@Override
	public int quantityDropped(final Random par1Random) {
		return 0;
	}

	/**
	 * Called when the player destroys a block with an item that can harvest it.
	 * (i, j, k) are the coordinates of the block and l is the block's
	 * subtype/damage.
	 */
	@Override
	public void harvestBlock(final World par1World,
			final EntityPlayer par2EntityPlayer, final int par3,
			final int par4, final int par5, final int par6) {
		if (!par1World.isRemote
				&& par2EntityPlayer.getCurrentEquippedItem() != null
				&& par2EntityPlayer.getCurrentEquippedItem().itemID == Item.shears.itemID) {
			par2EntityPlayer.addStat(StatList.mineBlockStatArray[blockID], 1);
			dropBlockAsItem_do(par1World, par3, par4, par5, new ItemStack(
					Block.vine, 1, 0));
		} else {
			super.harvestBlock(par1World, par2EntityPlayer, par3, par4, par5,
					par6);
		}
	}
}
