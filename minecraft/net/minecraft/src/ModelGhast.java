package net.minecraft.src;

import java.util.Random;

import org.lwjgl.opengl.GL11;

public class ModelGhast extends ModelBase {
	ModelRenderer body;
	ModelRenderer[] tentacles = new ModelRenderer[9];

	public ModelGhast() {
		final byte var1 = -16;
		body = new ModelRenderer(this, 0, 0);
		body.addBox(-8.0F, -8.0F, -8.0F, 16, 16, 16);
		body.rotationPointY += 24 + var1;
		final Random var2 = new Random(1660L);

		for (int var3 = 0; var3 < tentacles.length; ++var3) {
			tentacles[var3] = new ModelRenderer(this, 0, 0);
			final float var4 = ((var3 % 3 - var3 / 3 % 2 * 0.5F + 0.25F) / 2.0F * 2.0F - 1.0F) * 5.0F;
			final float var5 = (var3 / 3 / 2.0F * 2.0F - 1.0F) * 5.0F;
			final int var6 = var2.nextInt(7) + 8;
			tentacles[var3].addBox(-1.0F, 0.0F, -1.0F, 2, var6, 2);
			tentacles[var3].rotationPointX = var4;
			tentacles[var3].rotationPointZ = var5;
			tentacles[var3].rotationPointY = 31 + var1;
		}
	}

	/**
	 * Sets the model's various rotation angles. For bipeds, par1 and par2 are
	 * used for animating the movement of arms and legs, where par1 represents
	 * the time(so that arms and legs swing back and forth) and par2 represents
	 * how "far" arms and legs can swing at most.
	 */
	@Override
	public void setRotationAngles(final float par1, final float par2,
			final float par3, final float par4, final float par5,
			final float par6, final Entity par7Entity) {
		for (int var8 = 0; var8 < tentacles.length; ++var8) {
			tentacles[var8].rotateAngleX = 0.2F * MathHelper.sin(par3 * 0.3F
					+ var8) + 0.4F;
		}
	}

	/**
	 * Sets the models various rotation angles then renders the model.
	 */
	@Override
	public void render(final Entity par1Entity, final float par2,
			final float par3, final float par4, final float par5,
			final float par6, final float par7) {
		setRotationAngles(par2, par3, par4, par5, par6, par7, par1Entity);
		GL11.glPushMatrix();
		GL11.glTranslatef(0.0F, 0.6F, 0.0F);
		body.render(par7);
		final ModelRenderer[] var8 = tentacles;
		final int var9 = var8.length;

		for (int var10 = 0; var10 < var9; ++var10) {
			final ModelRenderer var11 = var8[var10];
			var11.render(par7);
		}

		GL11.glPopMatrix();
	}
}
