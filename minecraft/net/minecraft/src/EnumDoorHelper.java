package net.minecraft.src;

class EnumDoorHelper {
	static final int[] doorEnum = new int[EnumDoor.values().length];

	static {
		try {
			EnumDoorHelper.doorEnum[EnumDoor.OPENING.ordinal()] = 1;
		} catch (final NoSuchFieldError var4) {
			;
		}

		try {
			EnumDoorHelper.doorEnum[EnumDoor.WOOD_DOOR.ordinal()] = 2;
		} catch (final NoSuchFieldError var3) {
			;
		}

		try {
			EnumDoorHelper.doorEnum[EnumDoor.GRATES.ordinal()] = 3;
		} catch (final NoSuchFieldError var2) {
			;
		}

		try {
			EnumDoorHelper.doorEnum[EnumDoor.IRON_DOOR.ordinal()] = 4;
		} catch (final NoSuchFieldError var1) {
			;
		}
	}
}
