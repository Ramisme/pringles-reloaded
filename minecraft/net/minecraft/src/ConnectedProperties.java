package net.minecraft.src;

import java.util.ArrayList;
import java.util.Properties;

public class ConnectedProperties {
	public String name = null;
	public String basePath = null;
	public int[] matchBlocks = null;
	public String[] matchTiles = null;
	public int method = 0;
	public String[] tiles = null;
	public int connect = 0;
	public int faces = 63;
	public int[] metadatas = null;
	public BiomeGenBase[] biomes = null;
	public int minHeight = 0;
	public int maxHeight = 1024;
	public int renderPass = 0;
	public boolean innerSeams = false;
	public int width = 0;
	public int height = 0;
	public int[] weights = null;
	public int symmetry = 1;
	public int[] sumWeights = null;
	public int sumAllWeights = 0;
	public Icon[] matchTileIcons = null;
	public Icon[] tileIcons = null;
	public static final int METHOD_NONE = 0;
	public static final int METHOD_CTM = 1;
	public static final int METHOD_HORIZONTAL = 2;
	public static final int METHOD_TOP = 3;
	public static final int METHOD_RANDOM = 4;
	public static final int METHOD_REPEAT = 5;
	public static final int METHOD_VERTICAL = 6;
	public static final int METHOD_FIXED = 7;
	public static final int CONNECT_NONE = 0;
	public static final int CONNECT_BLOCK = 1;
	public static final int CONNECT_TILE = 2;
	public static final int CONNECT_MATERIAL = 3;
	public static final int CONNECT_UNKNOWN = 128;
	public static final int FACE_BOTTOM = 1;
	public static final int FACE_TOP = 2;
	public static final int FACE_EAST = 4;
	public static final int FACE_WEST = 8;
	public static final int FACE_NORTH = 16;
	public static final int FACE_SOUTH = 32;
	public static final int FACE_SIDES = 60;
	public static final int FACE_ALL = 63;
	public static final int FACE_UNKNOWN = 128;
	public static final int SYMMETRY_NONE = 1;
	public static final int SYMMETRY_OPPOSITE = 2;
	public static final int SYMMETRY_ALL = 6;
	public static final int SYMMETRY_UNKNOWN = 128;

	public ConnectedProperties(final Properties var1, final String var2) {
		name = ConnectedProperties.parseName(var2);
		basePath = ConnectedProperties.parseBasePath(var2);
		matchBlocks = ConnectedProperties.parseInts(var1
				.getProperty("matchBlocks"));
		matchTiles = parseMatchTiles(var1.getProperty("matchTiles"));
		method = ConnectedProperties.parseMethod(var1.getProperty("method"));
		tiles = parseTileNames(var1.getProperty("tiles"));
		connect = ConnectedProperties.parseConnect(var1.getProperty("connect"));
		faces = ConnectedProperties.parseFaces(var1.getProperty("faces"));
		metadatas = ConnectedProperties.parseInts(var1.getProperty("metadata"));
		biomes = ConnectedProperties.parseBiomes(var1.getProperty("biomes"));
		minHeight = ConnectedProperties.parseInt(var1.getProperty("minHeight"),
				-1);
		maxHeight = ConnectedProperties.parseInt(var1.getProperty("maxHeight"),
				1024);
		renderPass = ConnectedProperties.parseInt(var1
				.getProperty("renderPass"));
		innerSeams = ConnectedProperties.parseBoolean(var1
				.getProperty("innerSeams"));
		width = ConnectedProperties.parseInt(var1.getProperty("width"));
		height = ConnectedProperties.parseInt(var1.getProperty("height"));
		weights = ConnectedProperties.parseInts(var1.getProperty("weights"));
		symmetry = ConnectedProperties.parseSymmetry(var1
				.getProperty("symmetry"));
	}

	private String[] parseMatchTiles(final String var1) {
		if (var1 == null) {
			return null;
		} else {
			final String[] var2 = Config.tokenize(var1, " ");

			for (int var3 = 0; var3 < var2.length; ++var3) {
				String var4 = var2[var3];

				if (var4.endsWith(".png")) {
					var4 = var4.substring(0, var4.length() - 4);
				}

				if (var4.startsWith("/ctm/")) {
					var4 = var4.substring(1);
				}

				var2[var3] = var4;
			}

			return var2;
		}
	}

	private static String parseName(final String var0) {
		String var1 = var0;
		final int var2 = var0.lastIndexOf(47);

		if (var2 >= 0) {
			var1 = var0.substring(var2 + 1);
		}

		final int var3 = var1.lastIndexOf(46);

		if (var3 >= 0) {
			var1 = var1.substring(0, var3);
		}

		return var1;
	}

	private static String parseBasePath(final String var0) {
		final int var1 = var0.lastIndexOf(47);
		return var1 < 0 ? "" : var0.substring(0, var1);
	}

	private static BiomeGenBase[] parseBiomes(final String var0) {
		if (var0 == null) {
			return null;
		} else {
			final String[] var1 = Config.tokenize(var0, " ");
			final ArrayList var2 = new ArrayList();

			for (final String var4 : var1) {
				final BiomeGenBase var5 = ConnectedProperties.findBiome(var4);

				if (var5 == null) {
					Config.dbg("Biome not found: " + var4);
				} else {
					var2.add(var5);
				}
			}

			final BiomeGenBase[] var6 = (BiomeGenBase[]) var2
					.toArray(new BiomeGenBase[var2.size()]);
			return var6;
		}
	}

	private static BiomeGenBase findBiome(String var0) {
		var0 = var0.toLowerCase();

		for (final BiomeGenBase var2 : BiomeGenBase.biomeList) {
			if (var2 != null) {
				final String var3 = var2.biomeName.replace(" ", "")
						.toLowerCase();

				if (var3.equals(var0)) {
					return var2;
				}
			}
		}

		return null;
	}

	private String[] parseTileNames(final String var1) {
		if (var1 == null) {
			return null;
		} else {
			final ArrayList var2 = new ArrayList();
			final String[] var3 = Config.tokenize(var1, " ,");
			label63:

			for (int var4 = 0; var4 < var3.length; ++var4) {
				final String var5 = var3[var4];

				if (var5.contains("-")) {
					final String[] var6 = Config.tokenize(var5, "-");

					if (var6.length == 2) {
						final int var7 = Config.parseInt(var6[0], -1);
						final int var8 = Config.parseInt(var6[1], -1);

						if (var7 >= 0 && var8 >= 0) {
							if (var7 <= var8) {
								int var9 = var7;

								while (true) {
									if (var9 > var8) {
										continue label63;
									}

									var2.add(String.valueOf(var9));
									++var9;
								}
							}

							Config.dbg("Invalid interval: " + var5
									+ ", when parsing: " + var1);
							continue;
						}
					}
				}

				var2.add(var5);
			}

			final String[] var10 = (String[]) var2.toArray(new String[var2
					.size()]);

			for (int var11 = 0; var11 < var10.length; ++var11) {
				String var12 = var10[var11];

				if (!var12.startsWith("/")) {
					var12 = basePath + "/" + var12;
				}

				if (var12.endsWith(".png")) {
					var12 = var12.substring(0, var12.length() - 4);
				}

				final String var13 = "/textures/blocks/";

				if (var12.startsWith(var13)) {
					var12 = var12.substring(var13.length());
				}

				if (var12.startsWith("/")) {
					var12 = var12.substring(1);
				}

				var10[var11] = var12;
			}

			return var10;
		}
	}

	private static int parseInt(final String var0) {
		if (var0 == null) {
			return -1;
		} else {
			final int var1 = Config.parseInt(var0, -1);

			if (var1 < 0) {
				Config.dbg("Invalid number: " + var0);
			}

			return var1;
		}
	}

	private static int parseInt(final String var0, final int var1) {
		if (var0 == null) {
			return var1;
		} else {
			final int var2 = Config.parseInt(var0, -1);

			if (var2 < 0) {
				Config.dbg("Invalid number: " + var0);
				return var1;
			} else {
				return var2;
			}
		}
	}

	private static boolean parseBoolean(final String var0) {
		return var0 == null ? false : var0.toLowerCase().equals("true");
	}

	private static int parseSymmetry(final String var0) {
		if (var0 == null) {
			return 1;
		} else if (var0.equals("opposite")) {
			return 2;
		} else if (var0.equals("all")) {
			return 6;
		} else {
			Config.dbg("Unknown symmetry: " + var0);
			return 1;
		}
	}

	private static int parseFaces(final String var0) {
		if (var0 == null) {
			return 63;
		} else {
			final String[] var1 = Config.tokenize(var0, " ,");
			int var2 = 0;

			for (final String var4 : var1) {
				final int var5 = ConnectedProperties.parseFace(var4);
				var2 |= var5;
			}

			return var2;
		}
	}

	private static int parseFace(final String var0) {
		if (var0.equals("bottom")) {
			return 1;
		} else if (var0.equals("top")) {
			return 2;
		} else if (var0.equals("north")) {
			return 4;
		} else if (var0.equals("south")) {
			return 8;
		} else if (var0.equals("east")) {
			return 32;
		} else if (var0.equals("west")) {
			return 16;
		} else if (var0.equals("sides")) {
			return 60;
		} else if (var0.equals("all")) {
			return 63;
		} else {
			Config.dbg("Unknown face: " + var0);
			return 128;
		}
	}

	private static int parseConnect(final String var0) {
		if (var0 == null) {
			return 0;
		} else if (var0.equals("block")) {
			return 1;
		} else if (var0.equals("tile")) {
			return 2;
		} else if (var0.equals("material")) {
			return 3;
		} else {
			Config.dbg("Unknown connect: " + var0);
			return 128;
		}
	}

	private static int[] parseInts(final String var0) {
		if (var0 == null) {
			return null;
		} else {
			final ArrayList var1 = new ArrayList();
			final String[] var2 = Config.tokenize(var0, " ,");

			for (final String var4 : var2) {
				if (var4.contains("-")) {
					final String[] var5 = Config.tokenize(var4, "-");

					if (var5.length != 2) {
						Config.dbg("Invalid interval: " + var4
								+ ", when parsing: " + var0);
					} else {
						final int var6 = Config.parseInt(var5[0], -1);
						final int var7 = Config.parseInt(var5[1], -1);

						if (var6 >= 0 && var7 >= 0 && var6 <= var7) {
							for (int var8 = var6; var8 <= var7; ++var8) {
								var1.add(Integer.valueOf(var8));
							}
						} else {
							Config.dbg("Invalid interval: " + var4
									+ ", when parsing: " + var0);
						}
					}
				} else {
					final int var11 = Config.parseInt(var4, -1);

					if (var11 < 0) {
						Config.dbg("Invalid number: " + var4
								+ ", when parsing: " + var0);
					} else {
						var1.add(Integer.valueOf(var11));
					}
				}
			}

			final int[] var9 = new int[var1.size()];

			for (int var10 = 0; var10 < var9.length; ++var10) {
				var9[var10] = ((Integer) var1.get(var10)).intValue();
			}

			return var9;
		}
	}

	private static int parseMethod(final String var0) {
		if (var0 == null) {
			return 1;
		} else if (var0.equals("ctm")) {
			return 1;
		} else if (var0.equals("horizontal")) {
			return 2;
		} else if (var0.equals("vertical")) {
			return 6;
		} else if (var0.equals("top")) {
			return 3;
		} else if (var0.equals("random")) {
			return 4;
		} else if (var0.equals("repeat")) {
			return 5;
		} else if (var0.equals("fixed")) {
			return 7;
		} else {
			Config.dbg("Unknown method: " + var0);
			return 0;
		}
	}

	public boolean isValid(final String var1) {
		if (name != null && name.length() > 0) {
			if (basePath == null) {
				Config.dbg("No base path found: " + var1);
				return false;
			} else {
				if (matchBlocks == null) {
					matchBlocks = detectMatchBlocks();
				}

				if (matchTiles == null && matchBlocks == null) {
					matchTiles = detectMatchTiles();
				}

				if (matchBlocks == null && matchTiles == null) {
					Config.dbg("No matchBlocks or matchTiles specified: "
							+ var1);
					return false;
				} else if (method == 0) {
					Config.dbg("No method: " + var1);
					return false;
				} else if (tiles != null && tiles.length > 0) {
					if (connect == 0) {
						connect = detectConnect();
					}

					if (connect == 128) {
						Config.dbg("Invalid connect in: " + var1);
						return false;
					} else if (renderPass > 0) {
						Config.dbg("Render pass not supported: " + renderPass);
						return false;
					} else if ((faces & 128) != 0) {
						Config.dbg("Invalid faces in: " + var1);
						return false;
					} else if ((symmetry & 128) != 0) {
						Config.dbg("Invalid symmetry in: " + var1);
						return false;
					} else {
						switch (method) {
						case 1:
							return isValidCtm(var1);

						case 2:
							return isValidHorizontal(var1);

						case 3:
							return isValidTop(var1);

						case 4:
							return isValidRandom(var1);

						case 5:
							return isValidRepeat(var1);

						case 6:
							return isValidVertical(var1);

						case 7:
							return isValidFixed(var1);

						default:
							Config.dbg("Unknown method: " + var1);
							return false;
						}
					}
				} else {
					Config.dbg("No tiles specified: " + var1);
					return false;
				}
			}
		} else {
			Config.dbg("No name found: " + var1);
			return false;
		}
	}

	private int detectConnect() {
		return matchBlocks != null ? 1 : matchTiles != null ? 2 : 128;
	}

	private int[] detectMatchBlocks() {
		if (!name.startsWith("block")) {
			return null;
		} else {
			final int var1 = "block".length();
			int var2;

			for (var2 = var1; var2 < name.length(); ++var2) {
				final char var3 = name.charAt(var2);

				if (var3 < 48 || var3 > 57) {
					break;
				}
			}

			if (var2 == var1) {
				return null;
			} else {
				final String var5 = name.substring(var1, var2);
				final int var4 = Config.parseInt(var5, -1);
				return var4 < 0 ? null : new int[] { var4 };
			}
		}
	}

	private String[] detectMatchTiles() {
		final Icon var1 = ConnectedProperties.getIcon(name);
		return var1 == null ? null : new String[] { name };
	}

	private static Icon getIcon(final String var0) {
		final RenderEngine var1 = Config.getRenderEngine();
		return var1 == null ? null : var1.textureMapBlocks.getIconSafe(var0);
	}

	private boolean isValidCtm(final String var1) {
		if (tiles == null) {
			tiles = parseTileNames("0-11 16-27 32-43 48-58");
		}

		if (tiles.length < 47) {
			Config.dbg("Invalid tiles, must be at least 47: " + var1);
			return false;
		} else {
			return true;
		}
	}

	private boolean isValidHorizontal(final String var1) {
		if (tiles == null) {
			tiles = parseTileNames("12-15");
		}

		if (tiles.length != 4) {
			Config.dbg("Invalid tiles, must be exactly 4: " + var1);
			return false;
		} else {
			return true;
		}
	}

	private boolean isValidVertical(final String var1) {
		if (tiles == null) {
			Config.dbg("No tiles defined for vertical: " + var1);
			return false;
		} else if (tiles.length != 4) {
			Config.dbg("Invalid tiles, must be exactly 4: " + var1);
			return false;
		} else {
			return true;
		}
	}

	private boolean isValidRandom(final String var1) {
		if (tiles != null && tiles.length > 0) {
			if (weights != null && weights.length != tiles.length) {
				Config.dbg("Number of weights must equal the number of tiles: "
						+ var1);
				weights = null;
			}

			if (weights != null) {
				sumWeights = new int[weights.length];
				int var2 = 0;

				for (int var3 = 0; var3 < weights.length; ++var3) {
					var2 += weights[var3];
					sumWeights[var3] = var2;
				}

				sumAllWeights = var2;
			}

			return true;
		} else {
			Config.dbg("Tiles not defined: " + var1);
			return false;
		}
	}

	private boolean isValidRepeat(final String var1) {
		if (tiles == null) {
			Config.dbg("Tiles not defined: " + var1);
			return false;
		} else if (width > 0 && width <= 16) {
			if (height > 0 && height <= 16) {
				if (tiles.length != width * height) {
					Config.dbg("Number of tiles does not equal width x height: "
							+ var1);
					return false;
				} else {
					return true;
				}
			} else {
				Config.dbg("Invalid height: " + var1);
				return false;
			}
		} else {
			Config.dbg("Invalid width: " + var1);
			return false;
		}
	}

	private boolean isValidFixed(final String var1) {
		if (tiles == null) {
			Config.dbg("Tiles not defined: " + var1);
			return false;
		} else if (tiles.length != 1) {
			Config.dbg("Number of tiles should be 1 for method: fixed.");
			return false;
		} else {
			return true;
		}
	}

	private boolean isValidTop(final String var1) {
		if (tiles == null) {
			tiles = parseTileNames("66");
		}

		if (tiles.length != 1) {
			Config.dbg("Invalid tiles, must be exactly 1: " + var1);
			return false;
		} else {
			return true;
		}
	}

	public void updateIcons(final TextureMap var1) {
		if (matchTiles != null) {
			matchTileIcons = ConnectedProperties
					.registerIcons(matchTiles, var1);
		}

		if (tiles != null) {
			tileIcons = ConnectedProperties.registerIcons(tiles, var1);
		}
	}

	private static Icon[] registerIcons(final String[] var0,
			final TextureMap var1) {
		if (var0 == null) {
			return null;
		} else {
			final ITexturePack var2 = Config.getRenderEngine().getTexturePack()
					.getSelectedTexturePack();
			final ArrayList var3 = new ArrayList();

			for (final String element : var0) {
				final String var5 = element;
				String var6 = var5;

				if (!var5.contains("/")) {
					var6 = "textures/blocks/" + var5;
				}

				final String var7 = "/" + var6 + ".png";
				final boolean var8 = var2.func_98138_b(var7, true);

				if (!var8) {
					Config.dbg("File not found: " + var7);
				}

				final Icon var9 = var1.registerIcon(var5);
				var3.add(var9);
			}

			final Icon[] var10 = (Icon[]) var3.toArray(new Icon[var3.size()]);
			return var10;
		}
	}

	@Override
	public String toString() {
		return "CTM name: " + name + ", basePath: " + basePath
				+ ", matchBlocks: " + Config.arrayToString(matchBlocks)
				+ ", matchTiles: " + Config.arrayToString(matchTiles);
	}
}
