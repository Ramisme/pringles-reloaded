package net.minecraft.src;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ItemRecord extends Item {
	/** List of all record items and their names. */
	private static final Map records = new HashMap();

	/** The name of the record. */
	public final String recordName;

	protected ItemRecord(final int par1, final String par2Str) {
		super(par1);
		recordName = par2Str;
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord.records.put(par2Str, this);
	}

	/**
	 * Gets an icon index based on an item's damage value
	 */
	@Override
	public Icon getIconFromDamage(final int par1) {
		return itemIcon;
	}

	/**
	 * Callback for item usage. If the item does something special on right
	 * clicking, he will have one of those. Return True if something happen and
	 * false if it don't. This is for ITEMS, not BLOCKS
	 */
	@Override
	public boolean onItemUse(final ItemStack par1ItemStack,
			final EntityPlayer par2EntityPlayer, final World par3World,
			final int par4, final int par5, final int par6, final int par7,
			final float par8, final float par9, final float par10) {
		if (par3World.getBlockId(par4, par5, par6) == Block.jukebox.blockID
				&& par3World.getBlockMetadata(par4, par5, par6) == 0) {
			if (par3World.isRemote) {
				return true;
			} else {
				((BlockJukeBox) Block.jukebox).insertRecord(par3World, par4,
						par5, par6, par1ItemStack);
				par3World.playAuxSFXAtEntity((EntityPlayer) null, 1005, par4,
						par5, par6, itemID);
				--par1ItemStack.stackSize;
				return true;
			}
		} else {
			return false;
		}
	}

	/**
	 * allows items to add custom lines of information to the mouseover
	 * description
	 */
	@Override
	public void addInformation(final ItemStack par1ItemStack,
			final EntityPlayer par2EntityPlayer, final List par3List,
			final boolean par4) {
		par3List.add(getRecordTitle());
	}

	/**
	 * Return the title for this record.
	 */
	public String getRecordTitle() {
		return "C418 - " + recordName;
	}

	/**
	 * Return an item rarity from EnumRarity
	 */
	@Override
	public EnumRarity getRarity(final ItemStack par1ItemStack) {
		return EnumRarity.rare;
	}

	/**
	 * Return the record item corresponding to the given name.
	 */
	public static ItemRecord getRecord(final String par0Str) {
		return (ItemRecord) ItemRecord.records.get(par0Str);
	}

	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		itemIcon = par1IconRegister.registerIcon("record_" + recordName);
	}
}
