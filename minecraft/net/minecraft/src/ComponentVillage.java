package net.minecraft.src;

import java.util.List;
import java.util.Random;

abstract class ComponentVillage extends StructureComponent {
	/** The number of villagers that have been spawned in this component. */
	private int villagersSpawned;

	/** The starting piece of the village. */
	protected ComponentVillageStartPiece startPiece;

	protected ComponentVillage(
			final ComponentVillageStartPiece par1ComponentVillageStartPiece,
			final int par2) {
		super(par2);
		startPiece = par1ComponentVillageStartPiece;
	}

	/**
	 * Gets the next village component, with the bounding box shifted -1 in the
	 * X and Z direction.
	 */
	protected StructureComponent getNextComponentNN(
			final ComponentVillageStartPiece par1ComponentVillageStartPiece,
			final List par2List, final Random par3Random, final int par4,
			final int par5) {
		switch (coordBaseMode) {
		case 0:
			return StructureVillagePieces.getNextStructureComponent(
					par1ComponentVillageStartPiece, par2List, par3Random,
					boundingBox.minX - 1, boundingBox.minY + par4,
					boundingBox.minZ + par5, 1, getComponentType());

		case 1:
			return StructureVillagePieces.getNextStructureComponent(
					par1ComponentVillageStartPiece, par2List, par3Random,
					boundingBox.minX + par5, boundingBox.minY + par4,
					boundingBox.minZ - 1, 2, getComponentType());

		case 2:
			return StructureVillagePieces.getNextStructureComponent(
					par1ComponentVillageStartPiece, par2List, par3Random,
					boundingBox.minX - 1, boundingBox.minY + par4,
					boundingBox.minZ + par5, 1, getComponentType());

		case 3:
			return StructureVillagePieces.getNextStructureComponent(
					par1ComponentVillageStartPiece, par2List, par3Random,
					boundingBox.minX + par5, boundingBox.minY + par4,
					boundingBox.minZ - 1, 2, getComponentType());

		default:
			return null;
		}
	}

	/**
	 * Gets the next village component, with the bounding box shifted +1 in the
	 * X and Z direction.
	 */
	protected StructureComponent getNextComponentPP(
			final ComponentVillageStartPiece par1ComponentVillageStartPiece,
			final List par2List, final Random par3Random, final int par4,
			final int par5) {
		switch (coordBaseMode) {
		case 0:
			return StructureVillagePieces.getNextStructureComponent(
					par1ComponentVillageStartPiece, par2List, par3Random,
					boundingBox.maxX + 1, boundingBox.minY + par4,
					boundingBox.minZ + par5, 3, getComponentType());

		case 1:
			return StructureVillagePieces.getNextStructureComponent(
					par1ComponentVillageStartPiece, par2List, par3Random,
					boundingBox.minX + par5, boundingBox.minY + par4,
					boundingBox.maxZ + 1, 0, getComponentType());

		case 2:
			return StructureVillagePieces.getNextStructureComponent(
					par1ComponentVillageStartPiece, par2List, par3Random,
					boundingBox.maxX + 1, boundingBox.minY + par4,
					boundingBox.minZ + par5, 3, getComponentType());

		case 3:
			return StructureVillagePieces.getNextStructureComponent(
					par1ComponentVillageStartPiece, par2List, par3Random,
					boundingBox.minX + par5, boundingBox.minY + par4,
					boundingBox.maxZ + 1, 0, getComponentType());

		default:
			return null;
		}
	}

	/**
	 * Discover the y coordinate that will serve as the ground level of the
	 * supplied BoundingBox. (A median of all the levels in the BB's horizontal
	 * rectangle).
	 */
	protected int getAverageGroundLevel(final World par1World,
			final StructureBoundingBox par2StructureBoundingBox) {
		int var3 = 0;
		int var4 = 0;

		for (int var5 = boundingBox.minZ; var5 <= boundingBox.maxZ; ++var5) {
			for (int var6 = boundingBox.minX; var6 <= boundingBox.maxX; ++var6) {
				if (par2StructureBoundingBox.isVecInside(var6, 64, var5)) {
					var3 += Math.max(
							par1World.getTopSolidOrLiquidBlock(var6, var5),
							par1World.provider.getAverageGroundLevel());
					++var4;
				}
			}
		}

		if (var4 == 0) {
			return -1;
		} else {
			return var3 / var4;
		}
	}

	protected static boolean canVillageGoDeeper(
			final StructureBoundingBox par0StructureBoundingBox) {
		return par0StructureBoundingBox != null
				&& par0StructureBoundingBox.minY > 10;
	}

	/**
	 * Spawns a number of villagers in this component. Parameters: world,
	 * component bounding box, x offset, y offset, z offset, number of villagers
	 */
	protected void spawnVillagers(final World par1World,
			final StructureBoundingBox par2StructureBoundingBox,
			final int par3, final int par4, final int par5, final int par6) {
		if (villagersSpawned < par6) {
			for (int var7 = villagersSpawned; var7 < par6; ++var7) {
				final int var8 = getXWithOffset(par3 + var7, par5);
				final int var9 = getYWithOffset(par4);
				final int var10 = getZWithOffset(par3 + var7, par5);

				if (!par2StructureBoundingBox.isVecInside(var8, var9, var10)) {
					break;
				}

				++villagersSpawned;
				final EntityVillager var11 = new EntityVillager(par1World,
						getVillagerType(var7));
				var11.setLocationAndAngles(var8 + 0.5D, var9, var10 + 0.5D,
						0.0F, 0.0F);
				par1World.spawnEntityInWorld(var11);
			}
		}
	}

	/**
	 * Returns the villager type to spawn in this component, based on the number
	 * of villagers already spawned.
	 */
	protected int getVillagerType(final int par1) {
		return 0;
	}

	/**
	 * Gets the replacement block for the current biome
	 */
	protected int getBiomeSpecificBlock(final int par1, final int par2) {
		if (startPiece.inDesert) {
			if (par1 == Block.wood.blockID) {
				return Block.sandStone.blockID;
			}

			if (par1 == Block.cobblestone.blockID) {
				return Block.sandStone.blockID;
			}

			if (par1 == Block.planks.blockID) {
				return Block.sandStone.blockID;
			}

			if (par1 == Block.stairsWoodOak.blockID) {
				return Block.stairsSandStone.blockID;
			}

			if (par1 == Block.stairsCobblestone.blockID) {
				return Block.stairsSandStone.blockID;
			}

			if (par1 == Block.gravel.blockID) {
				return Block.sandStone.blockID;
			}
		}

		return par1;
	}

	/**
	 * Gets the replacement block metadata for the current biome
	 */
	protected int getBiomeSpecificBlockMetadata(final int par1, final int par2) {
		if (startPiece.inDesert) {
			if (par1 == Block.wood.blockID) {
				return 0;
			}

			if (par1 == Block.cobblestone.blockID) {
				return 0;
			}

			if (par1 == Block.planks.blockID) {
				return 2;
			}
		}

		return par2;
	}

	/**
	 * current Position depends on currently set Coordinates mode, is computed
	 * here
	 */
	@Override
	protected void placeBlockAtCurrentPosition(final World par1World,
			final int par2, final int par3, final int par4, final int par5,
			final int par6, final StructureBoundingBox par7StructureBoundingBox) {
		final int var8 = getBiomeSpecificBlock(par2, par3);
		final int var9 = getBiomeSpecificBlockMetadata(par2, par3);
		super.placeBlockAtCurrentPosition(par1World, var8, var9, par4, par5,
				par6, par7StructureBoundingBox);
	}

	/**
	 * arguments: (World worldObj, StructureBoundingBox structBB, int minX, int
	 * minY, int minZ, int maxX, int maxY, int maxZ, int placeBlockId, int
	 * replaceBlockId, boolean alwaysreplace)
	 */
	@Override
	protected void fillWithBlocks(final World par1World,
			final StructureBoundingBox par2StructureBoundingBox,
			final int par3, final int par4, final int par5, final int par6,
			final int par7, final int par8, final int par9, final int par10,
			final boolean par11) {
		final int var12 = getBiomeSpecificBlock(par9, 0);
		final int var13 = getBiomeSpecificBlockMetadata(par9, 0);
		final int var14 = getBiomeSpecificBlock(par10, 0);
		final int var15 = getBiomeSpecificBlockMetadata(par10, 0);
		super.fillWithMetadataBlocks(par1World, par2StructureBoundingBox, par3,
				par4, par5, par6, par7, par8, var12, var13, var14, var15, par11);
	}

	/**
	 * Overwrites air and liquids from selected position downwards, stops at
	 * hitting anything else.
	 */
	@Override
	protected void fillCurrentPositionBlocksDownwards(final World par1World,
			final int par2, final int par3, final int par4, final int par5,
			final int par6, final StructureBoundingBox par7StructureBoundingBox) {
		final int var8 = getBiomeSpecificBlock(par2, par3);
		final int var9 = getBiomeSpecificBlockMetadata(par2, par3);
		super.fillCurrentPositionBlocksDownwards(par1World, var8, var9, par4,
				par5, par6, par7StructureBoundingBox);
	}
}
