package net.minecraft.src;

class VillageAgressor {
	public EntityLiving agressor;
	public int agressionTime;

	final Village villageObj;

	VillageAgressor(final Village par1Village,
			final EntityLiving par2EntityLiving, final int par3) {
		villageObj = par1Village;
		agressor = par2EntityLiving;
		agressionTime = par3;
	}
}
