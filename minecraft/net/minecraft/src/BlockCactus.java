package net.minecraft.src;

import java.util.Random;

public class BlockCactus extends Block {
	private Icon cactusTopIcon;
	private Icon cactusBottomIcon;

	protected BlockCactus(final int par1) {
		super(par1, Material.cactus);
		setTickRandomly(true);
		setCreativeTab(CreativeTabs.tabDecorations);
	}

	/**
	 * Ticks the block if it's been scheduled
	 */
	@Override
	public void updateTick(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		if (par1World.isAirBlock(par2, par3 + 1, par4)) {
			int var6;

			for (var6 = 1; par1World.getBlockId(par2, par3 - var6, par4) == blockID; ++var6) {
				;
			}

			if (var6 < 3) {
				final int var7 = par1World.getBlockMetadata(par2, par3, par4);

				if (var7 == 15) {
					par1World.setBlock(par2, par3 + 1, par4, blockID);
					par1World
							.setBlockMetadataWithNotify(par2, par3, par4, 0, 4);
					onNeighborBlockChange(par1World, par2, par3 + 1, par4,
							blockID);
				} else {
					par1World.setBlockMetadataWithNotify(par2, par3, par4,
							var7 + 1, 4);
				}
			}
		}
	}

	/**
	 * Returns a bounding box from the pool of bounding boxes (this means this
	 * box can change after the pool has been cleared to be reused)
	 */
	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(final World par1World,
			final int par2, final int par3, final int par4) {
		final float var5 = 0.0625F;
		return AxisAlignedBB.getAABBPool().getAABB(par2 + var5, par3,
				par4 + var5, par2 + 1 - var5, par3 + 1 - var5, par4 + 1 - var5);
	}

	/**
	 * Returns the bounding box of the wired rectangular prism to render.
	 */
	@Override
	public AxisAlignedBB getSelectedBoundingBoxFromPool(final World par1World,
			final int par2, final int par3, final int par4) {
		final float var5 = 0.0625F;
		return AxisAlignedBB.getAABBPool().getAABB(par2 + var5, par3,
				par4 + var5, par2 + 1 - var5, par3 + 1, par4 + 1 - var5);
	}

	/**
	 * From the specified side and block metadata retrieves the blocks texture.
	 * Args: side, metadata
	 */
	@Override
	public Icon getIcon(final int par1, final int par2) {
		return par1 == 1 ? cactusTopIcon : par1 == 0 ? cactusBottomIcon
				: blockIcon;
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return 13;
	}

	/**
	 * Checks to see if its valid to put this block at the specified
	 * coordinates. Args: world, x, y, z
	 */
	@Override
	public boolean canPlaceBlockAt(final World par1World, final int par2,
			final int par3, final int par4) {
		return !super.canPlaceBlockAt(par1World, par2, par3, par4) ? false
				: canBlockStay(par1World, par2, par3, par4);
	}

	/**
	 * Lets the block know when one of its neighbor changes. Doesn't know which
	 * neighbor changed (coordinates passed are their own) Args: x, y, z,
	 * neighbor blockID
	 */
	@Override
	public void onNeighborBlockChange(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		if (!canBlockStay(par1World, par2, par3, par4)) {
			par1World.destroyBlock(par2, par3, par4, true);
		}
	}

	/**
	 * Can this block stay at this position. Similar to canPlaceBlockAt except
	 * gets checked often with plants.
	 */
	@Override
	public boolean canBlockStay(final World par1World, final int par2,
			final int par3, final int par4) {
		if (par1World.getBlockMaterial(par2 - 1, par3, par4).isSolid()) {
			return false;
		} else if (par1World.getBlockMaterial(par2 + 1, par3, par4).isSolid()) {
			return false;
		} else if (par1World.getBlockMaterial(par2, par3, par4 - 1).isSolid()) {
			return false;
		} else if (par1World.getBlockMaterial(par2, par3, par4 + 1).isSolid()) {
			return false;
		} else {
			final int var5 = par1World.getBlockId(par2, par3 - 1, par4);
			return var5 == Block.cactus.blockID || var5 == Block.sand.blockID;
		}
	}

	/**
	 * Triggered whenever an entity collides with this block (enters into the
	 * block). Args: world, x, y, z, entity
	 */
	@Override
	public void onEntityCollidedWithBlock(final World par1World,
			final int par2, final int par3, final int par4,
			final Entity par5Entity) {
		par5Entity.attackEntityFrom(DamageSource.cactus, 1);
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		blockIcon = par1IconRegister.registerIcon("cactus_side");
		cactusTopIcon = par1IconRegister.registerIcon("cactus_top");
		cactusBottomIcon = par1IconRegister.registerIcon("cactus_bottom");
	}
}
