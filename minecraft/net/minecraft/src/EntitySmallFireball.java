package net.minecraft.src;

public class EntitySmallFireball extends EntityFireball {
	public EntitySmallFireball(final World par1World) {
		super(par1World);
		setSize(0.3125F, 0.3125F);
	}

	public EntitySmallFireball(final World par1World,
			final EntityLiving par2EntityLiving, final double par3,
			final double par5, final double par7) {
		super(par1World, par2EntityLiving, par3, par5, par7);
		setSize(0.3125F, 0.3125F);
	}

	public EntitySmallFireball(final World par1World, final double par2,
			final double par4, final double par6, final double par8,
			final double par10, final double par12) {
		super(par1World, par2, par4, par6, par8, par10, par12);
		setSize(0.3125F, 0.3125F);
	}

	/**
	 * Called when this EntityFireball hits a block or entity.
	 */
	@Override
	protected void onImpact(final MovingObjectPosition par1MovingObjectPosition) {
		if (!worldObj.isRemote) {
			if (par1MovingObjectPosition.entityHit != null) {
				if (!par1MovingObjectPosition.entityHit.isImmuneToFire()
						&& par1MovingObjectPosition.entityHit.attackEntityFrom(
								DamageSource.causeFireballDamage(this,
										shootingEntity), 5)) {
					par1MovingObjectPosition.entityHit.setFire(5);
				}
			} else {
				int var2 = par1MovingObjectPosition.blockX;
				int var3 = par1MovingObjectPosition.blockY;
				int var4 = par1MovingObjectPosition.blockZ;

				switch (par1MovingObjectPosition.sideHit) {
				case 0:
					--var3;
					break;

				case 1:
					++var3;
					break;

				case 2:
					--var4;
					break;

				case 3:
					++var4;
					break;

				case 4:
					--var2;
					break;

				case 5:
					++var2;
				}

				if (worldObj.isAirBlock(var2, var3, var4)) {
					worldObj.setBlock(var2, var3, var4, Block.fire.blockID);
				}
			}

			setDead();
		}
	}

	/**
	 * Returns true if other Entities should be prevented from moving through
	 * this Entity.
	 */
	@Override
	public boolean canBeCollidedWith() {
		return false;
	}

	/**
	 * Called when the entity is attacked.
	 */
	@Override
	public boolean attackEntityFrom(final DamageSource par1DamageSource,
			final int par2) {
		return false;
	}
}
