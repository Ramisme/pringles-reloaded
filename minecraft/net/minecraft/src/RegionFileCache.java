package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class RegionFileCache {
	/** A map containing Files as keys and RegionFiles as values */
	private static final Map regionsByFilename = new HashMap();

	public static synchronized RegionFile createOrLoadRegionFile(
			final File par0File, final int par1, final int par2) {
		final File var3 = new File(par0File, "region");
		final File var4 = new File(var3, "r." + (par1 >> 5) + "." + (par2 >> 5)
				+ ".mca");
		final RegionFile var5 = (RegionFile) RegionFileCache.regionsByFilename
				.get(var4);

		if (var5 != null) {
			return var5;
		} else {
			if (!var3.exists()) {
				var3.mkdirs();
			}

			if (RegionFileCache.regionsByFilename.size() >= 256) {
				RegionFileCache.clearRegionFileReferences();
			}

			final RegionFile var6 = new RegionFile(var4);
			RegionFileCache.regionsByFilename.put(var4, var6);
			return var6;
		}
	}

	/**
	 * Saves the current Chunk Map Cache
	 */
	public static synchronized void clearRegionFileReferences() {
		final Iterator var0 = RegionFileCache.regionsByFilename.values()
				.iterator();

		while (var0.hasNext()) {
			final RegionFile var1 = (RegionFile) var0.next();

			try {
				if (var1 != null) {
					var1.close();
				}
			} catch (final IOException var3) {
				var3.printStackTrace();
			}
		}

		RegionFileCache.regionsByFilename.clear();
	}

	/**
	 * Returns an input stream for the specified chunk. Args: worldDir, chunkX,
	 * chunkZ
	 */
	public static DataInputStream getChunkInputStream(final File par0File,
			final int par1, final int par2) {
		final RegionFile var3 = RegionFileCache.createOrLoadRegionFile(
				par0File, par1, par2);
		return var3.getChunkDataInputStream(par1 & 31, par2 & 31);
	}

	/**
	 * Returns an output stream for the specified chunk. Args: worldDir, chunkX,
	 * chunkZ
	 */
	public static DataOutputStream getChunkOutputStream(final File par0File,
			final int par1, final int par2) {
		final RegionFile var3 = RegionFileCache.createOrLoadRegionFile(
				par0File, par1, par2);
		return var3.getChunkDataOutputStream(par1 & 31, par2 & 31);
	}
}
