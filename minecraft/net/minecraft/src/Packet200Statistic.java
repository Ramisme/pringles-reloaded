package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet200Statistic extends Packet {
	public int statisticId;
	public int amount;

	public Packet200Statistic() {
	}

	public Packet200Statistic(final int par1, final int par2) {
		statisticId = par1;
		amount = par2;
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleStatistic(this);
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		statisticId = par1DataInputStream.readInt();
		amount = par1DataInputStream.readByte();
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		par1DataOutputStream.writeInt(statisticId);
		par1DataOutputStream.writeByte(amount);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 6;
	}

	/**
	 * If this returns true, the packet may be processed on any thread;
	 * otherwise it is queued for the main thread to handle.
	 */
	@Override
	public boolean canProcessAsync() {
		return true;
	}
}
