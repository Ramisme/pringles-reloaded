package net.minecraft.src;

import net.minecraft.server.MinecraftServer;

public class CommandServerSaveOff extends CommandBase {
	@Override
	public String getCommandName() {
		return "save-off";
	}

	/**
	 * Return the required permission level for this command.
	 */
	@Override
	public int getRequiredPermissionLevel() {
		return 4;
	}

	@Override
	public void processCommand(final ICommandSender par1ICommandSender,
			final String[] par2ArrayOfStr) {
		final MinecraftServer var3 = MinecraftServer.getServer();

		for (final WorldServer worldServer : var3.worldServers) {
			if (worldServer != null) {
				final WorldServer var5 = worldServer;
				var5.canNotSave = true;
			}
		}

		CommandBase.notifyAdmins(par1ICommandSender, "commands.save.disabled",
				new Object[0]);
	}
}
