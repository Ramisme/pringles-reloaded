package net.minecraft.src;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;

public class GuiSelectWorld extends GuiScreen {
	/** simple date formater */
	private final DateFormat dateFormatter = new SimpleDateFormat();

	/**
	 * A reference to the screen object that created this. Used for navigating
	 * between screens.
	 */
	protected GuiScreen parentScreen;

	/** The title string that is displayed in the top-center of the screen. */
	protected String screenTitle = "Select world";

	/** True if a world has been selected. */
	private boolean selected = false;

	/** the currently selected world */
	private int selectedWorld;

	/** The save list for the world selection screen */
	private List saveList;
	private GuiWorldSlot worldSlotContainer;

	/** E.g. World, Welt, Monde, Mundo */
	private String localizedWorldText;
	private String localizedMustConvertText;

	/**
	 * The game mode text that is displayed with each world on the world
	 * selection list.
	 */
	private final String[] localizedGameModeText = new String[3];

	/** set to true if you arein the process of deleteing a world/save */
	private boolean deleting;

	/** The delete button in the world selection GUI */
	private GuiButton buttonDelete;

	/** the select button in the world selection gui */
	private GuiButton buttonSelect;

	/** The rename button in the world selection GUI */
	private GuiButton buttonRename;
	private GuiButton buttonRecreate;

	public GuiSelectWorld(final GuiScreen par1GuiScreen) {
		parentScreen = par1GuiScreen;
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui() {
		final StringTranslate var1 = StringTranslate.getInstance();
		screenTitle = var1.translateKey("selectWorld.title");

		try {
			loadSaves();
		} catch (final AnvilConverterException var3) {
			var3.printStackTrace();
			mc.displayGuiScreen(new GuiErrorScreen("Unable to load words", var3
					.getMessage()));
			return;
		}

		localizedWorldText = var1.translateKey("selectWorld.world");
		localizedMustConvertText = var1.translateKey("selectWorld.conversion");
		localizedGameModeText[EnumGameType.SURVIVAL.getID()] = var1
				.translateKey("gameMode.survival");
		localizedGameModeText[EnumGameType.CREATIVE.getID()] = var1
				.translateKey("gameMode.creative");
		localizedGameModeText[EnumGameType.ADVENTURE.getID()] = var1
				.translateKey("gameMode.adventure");
		worldSlotContainer = new GuiWorldSlot(this);
		worldSlotContainer.registerScrollButtons(buttonList, 4, 5);
		initButtons();
	}

	/**
	 * loads the saves
	 */
	private void loadSaves() throws AnvilConverterException {
		final ISaveFormat var1 = mc.getSaveLoader();
		saveList = var1.getSaveList();
		Collections.sort(saveList);
		selectedWorld = -1;
	}

	/**
	 * returns the file name of the specified save number
	 */
	protected String getSaveFileName(final int par1) {
		return ((SaveFormatComparator) saveList.get(par1)).getFileName();
	}

	/**
	 * returns the name of the saved game
	 */
	protected String getSaveName(final int par1) {
		String var2 = ((SaveFormatComparator) saveList.get(par1))
				.getDisplayName();

		if (var2 == null || MathHelper.stringNullOrLengthZero(var2)) {
			final StringTranslate var3 = StringTranslate.getInstance();
			var2 = var3.translateKey("selectWorld.world") + " " + (par1 + 1);
		}

		return var2;
	}

	/**
	 * intilize the buttons for this GUI
	 */
	public void initButtons() {
		final StringTranslate var1 = StringTranslate.getInstance();
		buttonList.add(buttonSelect = new GuiButton(1, width / 2 - 154,
				height - 52, 150, 20, var1.translateKey("selectWorld.select")));
		buttonList.add(new GuiButton(3, width / 2 + 4, height - 52, 150, 20,
				var1.translateKey("selectWorld.create")));
		buttonList.add(buttonRename = new GuiButton(6, width / 2 - 154,
				height - 28, 72, 20, var1.translateKey("selectWorld.rename")));
		buttonList.add(buttonDelete = new GuiButton(2, width / 2 - 76,
				height - 28, 72, 20, var1.translateKey("selectWorld.delete")));
		buttonList
				.add(buttonRecreate = new GuiButton(7, width / 2 + 4,
						height - 28, 72, 20, var1
								.translateKey("selectWorld.recreate")));
		buttonList.add(new GuiButton(0, width / 2 + 82, height - 28, 72, 20,
				var1.translateKey("gui.cancel")));
		buttonSelect.enabled = false;
		buttonDelete.enabled = false;
		buttonRename.enabled = false;
		buttonRecreate.enabled = false;
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	@Override
	protected void actionPerformed(final GuiButton par1GuiButton) {
		if (par1GuiButton.enabled) {
			if (par1GuiButton.id == 2) {
				final String var2 = getSaveName(selectedWorld);

				if (var2 != null) {
					deleting = true;
					final GuiYesNo var3 = GuiSelectWorld.getDeleteWorldScreen(
							this, var2, selectedWorld);
					mc.displayGuiScreen(var3);
				}
			} else if (par1GuiButton.id == 1) {
				selectWorld(selectedWorld);
			} else if (par1GuiButton.id == 3) {
				mc.displayGuiScreen(new GuiCreateWorld(this));
			} else if (par1GuiButton.id == 6) {
				mc.displayGuiScreen(new GuiRenameWorld(this,
						getSaveFileName(selectedWorld)));
			} else if (par1GuiButton.id == 0) {
				mc.displayGuiScreen(parentScreen);
			} else if (par1GuiButton.id == 7) {
				final GuiCreateWorld var5 = new GuiCreateWorld(this);
				final ISaveHandler var6 = mc.getSaveLoader().getSaveLoader(
						getSaveFileName(selectedWorld), false);
				final WorldInfo var4 = var6.loadWorldInfo();
				var6.flush();
				var5.func_82286_a(var4);
				mc.displayGuiScreen(var5);
			} else {
				worldSlotContainer.actionPerformed(par1GuiButton);
			}
		}
	}

	/**
	 * Gets the selected world.
	 */
	public void selectWorld(final int par1) {
		mc.displayGuiScreen((GuiScreen) null);

		if (!selected) {
			selected = true;
			String var2 = getSaveFileName(par1);

			if (var2 == null) {
				var2 = "World" + par1;
			}

			String var3 = getSaveName(par1);

			if (var3 == null) {
				var3 = "World" + par1;
			}

			if (mc.getSaveLoader().canLoadWorld(var2)) {
				mc.launchIntegratedServer(var2, var3, (WorldSettings) null);
			}
		}
	}

	@Override
	public void confirmClicked(final boolean par1, final int par2) {
		if (deleting) {
			deleting = false;

			if (par1) {
				final ISaveFormat var3 = mc.getSaveLoader();
				var3.flushCache();
				var3.deleteWorldDirectory(getSaveFileName(par2));

				try {
					loadSaves();
				} catch (final AnvilConverterException var5) {
					var5.printStackTrace();
				}
			}

			mc.displayGuiScreen(this);
		}
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(final int par1, final int par2, final float par3) {
		worldSlotContainer.drawScreen(par1, par2, par3);
		drawCenteredString(fontRenderer, screenTitle, width / 2, 20, 16777215);
		super.drawScreen(par1, par2, par3);
	}

	/**
	 * Gets a GuiYesNo screen with the warning, buttons, etc.
	 */
	public static GuiYesNo getDeleteWorldScreen(final GuiScreen par0GuiScreen,
			final String par1Str, final int par2) {
		final StringTranslate var3 = StringTranslate.getInstance();
		final String var4 = var3.translateKey("selectWorld.deleteQuestion");
		final String var5 = "\'" + par1Str + "\' "
				+ var3.translateKey("selectWorld.deleteWarning");
		final String var6 = var3.translateKey("selectWorld.deleteButton");
		final String var7 = var3.translateKey("gui.cancel");
		final GuiYesNo var8 = new GuiYesNo(par0GuiScreen, var4, var5, var6,
				var7, par2);
		return var8;
	}

	static List getSize(final GuiSelectWorld par0GuiSelectWorld) {
		return par0GuiSelectWorld.saveList;
	}

	/**
	 * called whenever an element in this gui is selected
	 */
	static int onElementSelected(final GuiSelectWorld par0GuiSelectWorld,
			final int par1) {
		return par0GuiSelectWorld.selectedWorld = par1;
	}

	/**
	 * returns the world currently selected
	 */
	static int getSelectedWorld(final GuiSelectWorld par0GuiSelectWorld) {
		return par0GuiSelectWorld.selectedWorld;
	}

	/**
	 * returns the select button
	 */
	static GuiButton getSelectButton(final GuiSelectWorld par0GuiSelectWorld) {
		return par0GuiSelectWorld.buttonSelect;
	}

	/**
	 * returns the rename button
	 */
	static GuiButton getRenameButton(final GuiSelectWorld par0GuiSelectWorld) {
		return par0GuiSelectWorld.buttonDelete;
	}

	/**
	 * returns the delete button
	 */
	static GuiButton getDeleteButton(final GuiSelectWorld par0GuiSelectWorld) {
		return par0GuiSelectWorld.buttonRename;
	}

	static GuiButton func_82312_f(final GuiSelectWorld par0GuiSelectWorld) {
		return par0GuiSelectWorld.buttonRecreate;
	}

	static String func_82313_g(final GuiSelectWorld par0GuiSelectWorld) {
		return par0GuiSelectWorld.localizedWorldText;
	}

	static DateFormat func_82315_h(final GuiSelectWorld par0GuiSelectWorld) {
		return par0GuiSelectWorld.dateFormatter;
	}

	static String func_82311_i(final GuiSelectWorld par0GuiSelectWorld) {
		return par0GuiSelectWorld.localizedMustConvertText;
	}

	static String[] func_82314_j(final GuiSelectWorld par0GuiSelectWorld) {
		return par0GuiSelectWorld.localizedGameModeText;
	}
}
