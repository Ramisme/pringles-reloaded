package net.minecraft.src;

import java.util.Random;

public class BlockSand extends Block {
	/** Do blocks fall instantly to where they stop or do they fall over time */
	public static boolean fallInstantly = false;

	public BlockSand(final int par1) {
		super(par1, Material.sand);
		setCreativeTab(CreativeTabs.tabBlock);
	}

	public BlockSand(final int par1, final Material par2Material) {
		super(par1, par2Material);
	}

	/**
	 * Called whenever the block is added into the world. Args: world, x, y, z
	 */
	@Override
	public void onBlockAdded(final World par1World, final int par2,
			final int par3, final int par4) {
		par1World.scheduleBlockUpdate(par2, par3, par4, blockID,
				tickRate(par1World));
	}

	/**
	 * Lets the block know when one of its neighbor changes. Doesn't know which
	 * neighbor changed (coordinates passed are their own) Args: x, y, z,
	 * neighbor blockID
	 */
	@Override
	public void onNeighborBlockChange(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		par1World.scheduleBlockUpdate(par2, par3, par4, blockID,
				tickRate(par1World));
	}

	/**
	 * Ticks the block if it's been scheduled
	 */
	@Override
	public void updateTick(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		if (!par1World.isRemote) {
			tryToFall(par1World, par2, par3, par4);
		}
	}

	/**
	 * If there is space to fall below will start this block falling
	 */
	private void tryToFall(final World par1World, final int par2, int par3,
			final int par4) {
		if (BlockSand.canFallBelow(par1World, par2, par3 - 1, par4)
				&& par3 >= 0) {
			final byte var8 = 32;

			if (!BlockSand.fallInstantly
					&& par1World.checkChunksExist(par2 - var8, par3 - var8,
							par4 - var8, par2 + var8, par3 + var8, par4 + var8)) {
				if (!par1World.isRemote) {
					final EntityFallingSand var9 = new EntityFallingSand(
							par1World, par2 + 0.5F, par3 + 0.5F, par4 + 0.5F,
							blockID, par1World.getBlockMetadata(par2, par3,
									par4));
					onStartFalling(var9);
					par1World.spawnEntityInWorld(var9);
				}
			} else {
				par1World.setBlockToAir(par2, par3, par4);

				while (BlockSand.canFallBelow(par1World, par2, par3 - 1, par4)
						&& par3 > 0) {
					--par3;
				}

				if (par3 > 0) {
					par1World.setBlock(par2, par3, par4, blockID);
				}
			}
		}
	}

	/**
	 * Called when the falling block entity for this block is created
	 */
	protected void onStartFalling(final EntityFallingSand par1EntityFallingSand) {
	}

	/**
	 * How many world ticks before ticking
	 */
	@Override
	public int tickRate(final World par1World) {
		return 2;
	}

	/**
	 * Checks to see if the sand can fall into the block below it
	 */
	public static boolean canFallBelow(final World par0World, final int par1,
			final int par2, final int par3) {
		final int var4 = par0World.getBlockId(par1, par2, par3);

		if (var4 == 0) {
			return true;
		} else if (var4 == Block.fire.blockID) {
			return true;
		} else {
			final Material var5 = Block.blocksList[var4].blockMaterial;
			return var5 == Material.water ? true : var5 == Material.lava;
		}
	}

	/**
	 * Called when the falling block entity for this block hits the ground and
	 * turns back into a block
	 */
	public void onFinishFalling(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
	}
}
