package net.minecraft.src;

import java.net.URL;

class RunnableTitleScreen implements Runnable {
	final GuiMainMenu field_104058_d;

	RunnableTitleScreen(final GuiMainMenu par1GuiMainMenu) {
		field_104058_d = par1GuiMainMenu;
	}

	@Override
	public void run() {
		try {
			String var1 = HttpUtil.func_104145_a(new URL(
					"http://assets.minecraft.net/1_6_has_been_released.flag"));

			if (var1 != null && var1.length() > 0) {
				var1 = var1.trim();

				synchronized (GuiMainMenu.func_104004_a(field_104058_d)) {
					GuiMainMenu
							.func_104005_a(
									field_104058_d,
									EnumChatFormatting.BOLD
											+ "Notice!"
											+ EnumChatFormatting.RESET
											+ " Minecraft 1.6 is available for manual download.");
					GuiMainMenu.func_104013_b(field_104058_d, var1);
					GuiMainMenu
							.func_104006_a(
									field_104058_d,
									GuiMainMenu
											.func_104022_c(field_104058_d)
											.getStringWidth(
													GuiMainMenu
															.func_104023_b(field_104058_d)));
					GuiMainMenu.func_104014_b(field_104058_d,
							GuiMainMenu.func_104007_d(field_104058_d)
									.getStringWidth(GuiMainMenu.field_96138_a));
					final int var3 = Math.max(
							GuiMainMenu.func_104016_e(field_104058_d),
							GuiMainMenu.func_104015_f(field_104058_d));
					GuiMainMenu.func_104008_c(field_104058_d,
							(field_104058_d.width - var3) / 2);
					GuiMainMenu.func_104009_d(
							field_104058_d,
							((GuiButton) GuiMainMenu.func_104019_g(
									field_104058_d).get(0)).yPosition - 24);
					GuiMainMenu.func_104011_e(field_104058_d,
							GuiMainMenu.func_104018_h(field_104058_d) + var3);
					GuiMainMenu.func_104012_f(field_104058_d,
							GuiMainMenu.func_104020_i(field_104058_d) + 24);
				}
			}
		} catch (final Throwable var6) {
			var6.printStackTrace();
		}
	}
}
