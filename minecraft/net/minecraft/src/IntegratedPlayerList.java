package net.minecraft.src;

import java.net.SocketAddress;

import net.minecraft.server.MinecraftServer;

public class IntegratedPlayerList extends ServerConfigurationManager {
	/**
	 * Holds the NBT data for the host player's save file, so this can be
	 * written to level.dat.
	 */
	private NBTTagCompound hostPlayerData = null;

	public IntegratedPlayerList(final IntegratedServer par1IntegratedServer) {
		super(par1IntegratedServer);
		viewDistance = 10;
	}

	/**
	 * also stores the NBTTags if this is an intergratedPlayerList
	 */
	@Override
	protected void writePlayerData(final EntityPlayerMP par1EntityPlayerMP) {
		if (par1EntityPlayerMP.getCommandSenderName().equals(
				getIntegratedServer().getServerOwner())) {
			hostPlayerData = new NBTTagCompound();
			par1EntityPlayerMP.writeToNBT(hostPlayerData);
		}

		super.writePlayerData(par1EntityPlayerMP);
	}

	/**
	 * checks ban-lists, then white-lists, then space for the server. Returns
	 * null on success, or an error message
	 */
	@Override
	public String allowUserToConnect(final SocketAddress par1SocketAddress,
			final String par2Str) {
		return par2Str.equalsIgnoreCase(getIntegratedServer().getServerOwner()) ? "That name is already taken."
				: super.allowUserToConnect(par1SocketAddress, par2Str);
	}

	/**
	 * get the associated Integrated Server
	 */
	public IntegratedServer getIntegratedServer() {
		return (IntegratedServer) super.getServerInstance();
	}

	/**
	 * On integrated servers, returns the host's player data to be written to
	 * level.dat.
	 */
	@Override
	public NBTTagCompound getHostPlayerData() {
		return hostPlayerData;
	}

	@Override
	public MinecraftServer getServerInstance() {
		return getIntegratedServer();
	}
}
