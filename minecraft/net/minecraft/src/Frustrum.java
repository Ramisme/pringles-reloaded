package net.minecraft.src;

public class Frustrum implements ICamera {
	private final ClippingHelper clippingHelper = ClippingHelperImpl
			.getInstance();
	private double xPosition;
	private double yPosition;
	private double zPosition;

	@Override
	public void setPosition(final double par1, final double par3,
			final double par5) {
		xPosition = par1;
		yPosition = par3;
		zPosition = par5;
	}

	/**
	 * Calls the clipping helper. Returns true if the box is inside all 6
	 * clipping planes, otherwise returns false.
	 */
	public boolean isBoxInFrustum(final double par1, final double par3,
			final double par5, final double par7, final double par9,
			final double par11) {
		return clippingHelper.isBoxInFrustum(par1 - xPosition,
				par3 - yPosition, par5 - zPosition, par7 - xPosition, par9
						- yPosition, par11 - zPosition);
	}

	/**
	 * Returns true if the bounding box is inside all 6 clipping planes,
	 * otherwise returns false.
	 */
	@Override
	public boolean isBoundingBoxInFrustum(final AxisAlignedBB par1AxisAlignedBB) {
		return isBoxInFrustum(par1AxisAlignedBB.minX, par1AxisAlignedBB.minY,
				par1AxisAlignedBB.minZ, par1AxisAlignedBB.maxX,
				par1AxisAlignedBB.maxY, par1AxisAlignedBB.maxZ);
	}

	public boolean isBoxInFrustumFully(final double var1, final double var3,
			final double var5, final double var7, final double var9,
			final double var11) {
		return clippingHelper.isBoxInFrustumFully(var1 - xPosition, var3
				- yPosition, var5 - zPosition, var7 - xPosition, var9
				- yPosition, var11 - zPosition);
	}

	@Override
	public boolean isBoundingBoxInFrustumFully(final AxisAlignedBB var1) {
		return isBoxInFrustumFully(var1.minX, var1.minY, var1.minZ, var1.maxX,
				var1.maxY, var1.maxZ);
	}
}
