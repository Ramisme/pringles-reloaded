package net.minecraft.src;

public class ServerCommand {
	/** The command string. */
	public final String command;
	public final ICommandSender sender;

	public ServerCommand(final String par1Str,
			final ICommandSender par2ICommandSender) {
		command = par1Str;
		sender = par2ICommandSender;
	}
}
