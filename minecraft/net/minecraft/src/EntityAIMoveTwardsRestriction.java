package net.minecraft.src;

public class EntityAIMoveTwardsRestriction extends EntityAIBase {
	private final EntityCreature theEntity;
	private double movePosX;
	private double movePosY;
	private double movePosZ;
	private final float movementSpeed;

	public EntityAIMoveTwardsRestriction(
			final EntityCreature par1EntityCreature, final float par2) {
		theEntity = par1EntityCreature;
		movementSpeed = par2;
		setMutexBits(1);
	}

	/**
	 * Returns whether the EntityAIBase should begin execution.
	 */
	@Override
	public boolean shouldExecute() {
		if (theEntity.isWithinHomeDistanceCurrentPosition()) {
			return false;
		} else {
			final ChunkCoordinates var1 = theEntity.getHomePosition();
			final Vec3 var2 = RandomPositionGenerator
					.findRandomTargetBlockTowards(
							theEntity,
							16,
							7,
							theEntity.worldObj.getWorldVec3Pool()
									.getVecFromPool(var1.posX, var1.posY,
											var1.posZ));

			if (var2 == null) {
				return false;
			} else {
				movePosX = var2.xCoord;
				movePosY = var2.yCoord;
				movePosZ = var2.zCoord;
				return true;
			}
		}
	}

	/**
	 * Returns whether an in-progress EntityAIBase should continue executing
	 */
	@Override
	public boolean continueExecuting() {
		return !theEntity.getNavigator().noPath();
	}

	/**
	 * Execute a one shot task or start executing a continuous task
	 */
	@Override
	public void startExecuting() {
		theEntity.getNavigator().tryMoveToXYZ(movePosX, movePosY, movePosZ,
				movementSpeed);
	}
}
