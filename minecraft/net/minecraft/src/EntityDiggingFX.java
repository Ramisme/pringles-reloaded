package net.minecraft.src;

public class EntityDiggingFX extends EntityFX {
	private final Block blockInstance;

	public EntityDiggingFX(final World par1World, final double par2,
			final double par4, final double par6, final double par8,
			final double par10, final double par12, final Block par14Block,
			final int par15, final int par16,
			final RenderEngine par17RenderEngine) {
		super(par1World, par2, par4, par6, par8, par10, par12);
		blockInstance = par14Block;
		setParticleIcon(par17RenderEngine, par14Block.getIcon(0, par16));
		particleGravity = par14Block.blockParticleGravity;
		particleRed = particleGreen = particleBlue = 0.6F;
		particleScale /= 2.0F;
	}

	public EntityDiggingFX func_70596_a(final int par1, final int par2,
			final int par3) {
		if (blockInstance == Block.grass) {
			return this;
		} else {
			final int var4 = blockInstance.colorMultiplier(worldObj, par1,
					par2, par3);
			particleRed *= (var4 >> 16 & 255) / 255.0F;
			particleGreen *= (var4 >> 8 & 255) / 255.0F;
			particleBlue *= (var4 & 255) / 255.0F;
			return this;
		}
	}

	/**
	 * Creates a new EntityDiggingFX with the block render color applied to the
	 * base particle color
	 */
	public EntityDiggingFX applyRenderColor(final int par1) {
		if (blockInstance == Block.grass) {
			return this;
		} else {
			final int var2 = blockInstance.getRenderColor(par1);
			particleRed *= (var2 >> 16 & 255) / 255.0F;
			particleGreen *= (var2 >> 8 & 255) / 255.0F;
			particleBlue *= (var2 & 255) / 255.0F;
			return this;
		}
	}

	@Override
	public int getFXLayer() {
		return 1;
	}

	@Override
	public void renderParticle(final Tessellator par1Tessellator,
			final float par2, final float par3, final float par4,
			final float par5, final float par6, final float par7) {
		float var8 = (particleTextureIndexX + particleTextureJitterX / 4.0F) / 16.0F;
		float var9 = var8 + 0.015609375F;
		float var10 = (particleTextureIndexY + particleTextureJitterY / 4.0F) / 16.0F;
		float var11 = var10 + 0.015609375F;
		final float var12 = 0.1F * particleScale;

		if (particleIcon != null) {
			var8 = particleIcon
					.getInterpolatedU(particleTextureJitterX / 4.0F * 16.0F);
			var9 = particleIcon
					.getInterpolatedU((particleTextureJitterX + 1.0F) / 4.0F * 16.0F);
			var10 = particleIcon
					.getInterpolatedV(particleTextureJitterY / 4.0F * 16.0F);
			var11 = particleIcon
					.getInterpolatedV((particleTextureJitterY + 1.0F) / 4.0F * 16.0F);
		}

		final float var13 = (float) (prevPosX + (posX - prevPosX) * par2 - EntityFX.interpPosX);
		final float var14 = (float) (prevPosY + (posY - prevPosY) * par2 - EntityFX.interpPosY);
		final float var15 = (float) (prevPosZ + (posZ - prevPosZ) * par2 - EntityFX.interpPosZ);
		final float var16 = 1.0F;
		par1Tessellator.setColorOpaque_F(var16 * particleRed, var16
				* particleGreen, var16 * particleBlue);
		par1Tessellator.addVertexWithUV(var13 - par3 * var12 - par6 * var12,
				var14 - par4 * var12, var15 - par5 * var12 - par7 * var12,
				var8, var11);
		par1Tessellator.addVertexWithUV(var13 - par3 * var12 + par6 * var12,
				var14 + par4 * var12, var15 - par5 * var12 + par7 * var12,
				var8, var10);
		par1Tessellator.addVertexWithUV(var13 + par3 * var12 + par6 * var12,
				var14 + par4 * var12, var15 + par5 * var12 + par7 * var12,
				var9, var10);
		par1Tessellator.addVertexWithUV(var13 + par3 * var12 - par6 * var12,
				var14 - par4 * var12, var15 + par5 * var12 - par7 * var12,
				var9, var11);
	}
}
