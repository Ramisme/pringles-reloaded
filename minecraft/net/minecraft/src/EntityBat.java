package net.minecraft.src;

import java.util.Calendar;

public class EntityBat extends EntityAmbientCreature {
	/**
	 * randomly selected ChunkCoordinates in a 7x6x7 box around the bat (y
	 * offset -2 to 4) towards which it will fly. upon getting close a new
	 * target will be selected
	 */
	private ChunkCoordinates currentFlightTarget;

	public EntityBat(final World par1World) {
		super(par1World);
		texture = "/mob/bat.png";
		setSize(0.5F, 0.9F);
		setIsBatHanging(true);
	}

	@Override
	protected void entityInit() {
		super.entityInit();
		dataWatcher.addObject(16, new Byte((byte) 0));
	}

	/**
	 * Returns the volume for the sounds this mob makes.
	 */
	@Override
	protected float getSoundVolume() {
		return 0.1F;
	}

	/**
	 * Gets the pitch of living sounds in living entities.
	 */
	@Override
	protected float getSoundPitch() {
		return super.getSoundPitch() * 0.95F;
	}

	/**
	 * Returns the sound this mob makes while it's alive.
	 */
	@Override
	protected String getLivingSound() {
		return getIsBatHanging() && rand.nextInt(4) != 0 ? null
				: "mob.bat.idle";
	}

	/**
	 * Returns the sound this mob makes when it is hurt.
	 */
	@Override
	protected String getHurtSound() {
		return "mob.bat.hurt";
	}

	/**
	 * Returns the sound this mob makes on death.
	 */
	@Override
	protected String getDeathSound() {
		return "mob.bat.death";
	}

	/**
	 * Returns true if this entity should push and be pushed by other entities
	 * when colliding.
	 */
	@Override
	public boolean canBePushed() {
		return false;
	}

	@Override
	protected void collideWithEntity(final Entity par1Entity) {
	}

	@Override
	protected void func_85033_bc() {
	}

	@Override
	public int getMaxHealth() {
		return 6;
	}

	public boolean getIsBatHanging() {
		return (dataWatcher.getWatchableObjectByte(16) & 1) != 0;
	}

	public void setIsBatHanging(final boolean par1) {
		final byte var2 = dataWatcher.getWatchableObjectByte(16);

		if (par1) {
			dataWatcher.updateObject(16, Byte.valueOf((byte) (var2 | 1)));
		} else {
			dataWatcher.updateObject(16, Byte.valueOf((byte) (var2 & -2)));
		}
	}

	/**
	 * Returns true if the newer Entity AI code should be run
	 */
	@Override
	protected boolean isAIEnabled() {
		return true;
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate() {
		super.onUpdate();

		if (getIsBatHanging()) {
			motionX = motionY = motionZ = 0.0D;
			posY = MathHelper.floor_double(posY) + 1.0D - height;
		} else {
			motionY *= 0.6000000238418579D;
		}
	}

	@Override
	protected void updateAITasks() {
		super.updateAITasks();

		if (getIsBatHanging()) {
			if (!worldObj.isBlockNormalCube(MathHelper.floor_double(posX),
					(int) posY + 1, MathHelper.floor_double(posZ))) {
				setIsBatHanging(false);
				worldObj.playAuxSFXAtEntity((EntityPlayer) null, 1015,
						(int) posX, (int) posY, (int) posZ, 0);
			} else {
				if (rand.nextInt(200) == 0) {
					rotationYawHead = rand.nextInt(360);
				}

				if (worldObj.getClosestPlayerToEntity(this, 4.0D) != null) {
					setIsBatHanging(false);
					worldObj.playAuxSFXAtEntity((EntityPlayer) null, 1015,
							(int) posX, (int) posY, (int) posZ, 0);
				}
			}
		} else {
			if (currentFlightTarget != null
					&& (!worldObj.isAirBlock(currentFlightTarget.posX,
							currentFlightTarget.posY, currentFlightTarget.posZ) || currentFlightTarget.posY < 1)) {
				currentFlightTarget = null;
			}

			if (currentFlightTarget == null
					|| rand.nextInt(30) == 0
					|| currentFlightTarget.getDistanceSquared((int) posX,
							(int) posY, (int) posZ) < 4.0F) {
				currentFlightTarget = new ChunkCoordinates((int) posX
						+ rand.nextInt(7) - rand.nextInt(7), (int) posY
						+ rand.nextInt(6) - 2, (int) posZ + rand.nextInt(7)
						- rand.nextInt(7));
			}

			final double var1 = currentFlightTarget.posX + 0.5D - posX;
			final double var3 = currentFlightTarget.posY + 0.1D - posY;
			final double var5 = currentFlightTarget.posZ + 0.5D - posZ;
			motionX += (Math.signum(var1) * 0.5D - motionX) * 0.10000000149011612D;
			motionY += (Math.signum(var3) * 0.699999988079071D - motionY) * 0.10000000149011612D;
			motionZ += (Math.signum(var5) * 0.5D - motionZ) * 0.10000000149011612D;
			final float var7 = (float) (Math.atan2(motionZ, motionX) * 180.0D / Math.PI) - 90.0F;
			final float var8 = MathHelper.wrapAngleTo180_float(var7
					- rotationYaw);
			moveForward = 0.5F;
			rotationYaw += var8;

			if (rand.nextInt(100) == 0
					&& worldObj.isBlockNormalCube(
							MathHelper.floor_double(posX), (int) posY + 1,
							MathHelper.floor_double(posZ))) {
				setIsBatHanging(true);
			}
		}
	}

	/**
	 * returns if this entity triggers Block.onEntityWalking on the blocks they
	 * walk on. used for spiders and wolves to prevent them from trampling crops
	 */
	@Override
	protected boolean canTriggerWalking() {
		return false;
	}

	/**
	 * Called when the mob is falling. Calculates and applies fall damage.
	 */
	@Override
	protected void fall(final float par1) {
	}

	/**
	 * Takes in the distance the entity has fallen this tick and whether its on
	 * the ground to update the fall distance and deal fall damage if landing on
	 * the ground. Args: distanceFallenThisTick, onGround
	 */
	@Override
	protected void updateFallState(final double par1, final boolean par3) {
	}

	/**
	 * Return whether this entity should NOT trigger a pressure plate or a
	 * tripwire.
	 */
	@Override
	public boolean doesEntityNotTriggerPressurePlate() {
		return true;
	}

	/**
	 * Called when the entity is attacked.
	 */
	@Override
	public boolean attackEntityFrom(final DamageSource par1DamageSource,
			final int par2) {
		if (isEntityInvulnerable()) {
			return false;
		} else {
			if (!worldObj.isRemote && getIsBatHanging()) {
				setIsBatHanging(false);
			}

			return super.attackEntityFrom(par1DamageSource, par2);
		}
	}

	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	@Override
	public void readEntityFromNBT(final NBTTagCompound par1NBTTagCompound) {
		super.readEntityFromNBT(par1NBTTagCompound);
		dataWatcher.updateObject(16,
				Byte.valueOf(par1NBTTagCompound.getByte("BatFlags")));
	}

	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	@Override
	public void writeEntityToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeEntityToNBT(par1NBTTagCompound);
		par1NBTTagCompound.setByte("BatFlags",
				dataWatcher.getWatchableObjectByte(16));
	}

	/**
	 * Checks if the entity's current position is a valid location to spawn this
	 * entity.
	 */
	@Override
	public boolean getCanSpawnHere() {
		final int var1 = MathHelper.floor_double(boundingBox.minY);

		if (var1 >= 63) {
			return false;
		} else {
			final int var2 = MathHelper.floor_double(posX);
			final int var3 = MathHelper.floor_double(posZ);
			final int var4 = worldObj.getBlockLightValue(var2, var1, var3);
			byte var5 = 4;
			final Calendar var6 = worldObj.getCurrentDate();

			if ((var6.get(2) + 1 != 10 || var6.get(5) < 20)
					&& (var6.get(2) + 1 != 11 || var6.get(5) > 3)) {
				if (rand.nextBoolean()) {
					return false;
				}
			} else {
				var5 = 7;
			}

			return var4 > rand.nextInt(var5) ? false : super.getCanSpawnHere();
		}
	}

	/**
	 * Initialize this creature.
	 */
	@Override
	public void initCreature() {
	}
}
