package net.minecraft.src;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.server.MinecraftServer;

public class TileEntity {
	/**
	 * A HashMap storing string names of classes mapping to the actual
	 * java.lang.Class type.
	 */
	private static Map nameToClassMap = new HashMap();

	/**
	 * A HashMap storing the classes and mapping to the string names (reverse of
	 * nameToClassMap).
	 */
	private static Map classToNameMap = new HashMap();

	/** The reference to the world. */
	protected World worldObj;

	/** The x coordinate of the tile entity. */
	public int xCoord;

	/** The y coordinate of the tile entity. */
	public int yCoord;

	/** The z coordinate of the tile entity. */
	public int zCoord;
	protected boolean tileEntityInvalid;
	public int blockMetadata = -1;

	/** the Block type that this TileEntity is contained within */
	public Block blockType;

	/**
	 * Adds a new two-way mapping between the class and its string name in both
	 * hashmaps.
	 */
	private static void addMapping(final Class par0Class, final String par1Str) {
		if (TileEntity.nameToClassMap.containsKey(par1Str)) {
			throw new IllegalArgumentException("Duplicate id: " + par1Str);
		} else {
			TileEntity.nameToClassMap.put(par1Str, par0Class);
			TileEntity.classToNameMap.put(par0Class, par1Str);
		}
	}

	/**
	 * Returns the worldObj for this tileEntity.
	 */
	public World getWorldObj() {
		return worldObj;
	}

	/**
	 * Sets the worldObj for this tileEntity.
	 */
	public void setWorldObj(final World par1World) {
		worldObj = par1World;
	}

	public boolean func_70309_m() {
		return worldObj != null;
	}

	/**
	 * Reads a tile entity from NBT.
	 */
	public void readFromNBT(final NBTTagCompound par1NBTTagCompound) {
		xCoord = par1NBTTagCompound.getInteger("x");
		yCoord = par1NBTTagCompound.getInteger("y");
		zCoord = par1NBTTagCompound.getInteger("z");
	}

	/**
	 * Writes a tile entity to NBT.
	 */
	public void writeToNBT(final NBTTagCompound par1NBTTagCompound) {
		final String var2 = (String) TileEntity.classToNameMap.get(this
				.getClass());

		if (var2 == null) {
			throw new RuntimeException(this.getClass()
					+ " is missing a mapping! This is a bug!");
		} else {
			par1NBTTagCompound.setString("id", var2);
			par1NBTTagCompound.setInteger("x", xCoord);
			par1NBTTagCompound.setInteger("y", yCoord);
			par1NBTTagCompound.setInteger("z", zCoord);
		}
	}

	/**
	 * Allows the entity to update its state. Overridden in most subclasses,
	 * e.g. the mob spawner uses this to count ticks and creates a new spawn
	 * inside its implementation.
	 */
	public void updateEntity() {
	}

	/**
	 * Creates a new entity and loads its data from the specified NBT.
	 */
	public static TileEntity createAndLoadEntity(
			final NBTTagCompound par0NBTTagCompound) {
		TileEntity var1 = null;

		try {
			final Class var2 = (Class) TileEntity.nameToClassMap
					.get(par0NBTTagCompound.getString("id"));

			if (var2 != null) {
				var1 = (TileEntity) var2.newInstance();
			}
		} catch (final Exception var3) {
			var3.printStackTrace();
		}

		if (var1 != null) {
			var1.readFromNBT(par0NBTTagCompound);
		} else {
			MinecraftServer
					.getServer()
					.getLogAgent()
					.logWarning(
							"Skipping TileEntity with id "
									+ par0NBTTagCompound.getString("id"));
		}

		return var1;
	}

	/**
	 * Returns block data at the location of this entity (client-only).
	 */
	public int getBlockMetadata() {
		if (blockMetadata == -1) {
			blockMetadata = worldObj.getBlockMetadata(xCoord, yCoord, zCoord);
		}

		return blockMetadata;
	}

	/**
	 * Called when an the contents of an Inventory change, usually
	 */
	public void onInventoryChanged() {
		if (worldObj != null) {
			blockMetadata = worldObj.getBlockMetadata(xCoord, yCoord, zCoord);
			worldObj.updateTileEntityChunkAndDoNothing(xCoord, yCoord, zCoord,
					this);

			if (getBlockType() != null) {
				worldObj.func_96440_m(xCoord, yCoord, zCoord,
						getBlockType().blockID);
			}
		}
	}

	/**
	 * Returns the square of the distance between this entity and the passed in
	 * coordinates.
	 */
	public double getDistanceFrom(final double par1, final double par3,
			final double par5) {
		final double var7 = xCoord + 0.5D - par1;
		final double var9 = yCoord + 0.5D - par3;
		final double var11 = zCoord + 0.5D - par5;
		return var7 * var7 + var9 * var9 + var11 * var11;
	}

	public double getMaxRenderDistanceSquared() {
		return 4096.0D;
	}

	/**
	 * Gets the block type at the location of this entity (client-only).
	 */
	public Block getBlockType() {
		if (blockType == null) {
			blockType = Block.blocksList[worldObj.getBlockId(xCoord, yCoord,
					zCoord)];
		}

		return blockType;
	}

	/**
	 * Overriden in a sign to provide the text.
	 */
	public Packet getDescriptionPacket() {
		return null;
	}

	/**
	 * returns true if tile entity is invalid, false otherwise
	 */
	public boolean isInvalid() {
		return tileEntityInvalid;
	}

	/**
	 * invalidates a tile entity
	 */
	public void invalidate() {
		tileEntityInvalid = true;
	}

	/**
	 * validates a tile entity
	 */
	public void validate() {
		tileEntityInvalid = false;
	}

	/**
	 * Called when a client event is received with the event number and
	 * argument, see World.sendClientEvent
	 */
	public boolean receiveClientEvent(final int par1, final int par2) {
		return false;
	}

	/**
	 * Causes the TileEntity to reset all it's cached values for it's container
	 * block, blockID, metaData and in the case of chests, the adjcacent chest
	 * check
	 */
	public void updateContainingBlockInfo() {
		blockType = null;
		blockMetadata = -1;
	}

	public void func_85027_a(final CrashReportCategory par1CrashReportCategory) {
		par1CrashReportCategory.addCrashSectionCallable("Name",
				new CallableTileEntityName(this));
		CrashReportCategory.func_85068_a(par1CrashReportCategory, xCoord,
				yCoord, zCoord, getBlockType().blockID, getBlockMetadata());
		par1CrashReportCategory.addCrashSectionCallable("Actual block type",
				new CallableTileEntityID(this));
		par1CrashReportCategory.addCrashSectionCallable(
				"Actual block data value", new CallableTileEntityData(this));
	}

	static Map getClassToNameMap() {
		return TileEntity.classToNameMap;
	}

	static {
		TileEntity.addMapping(TileEntityFurnace.class, "Furnace");
		TileEntity.addMapping(TileEntityChest.class, "Chest");
		TileEntity.addMapping(TileEntityEnderChest.class, "EnderChest");
		TileEntity.addMapping(TileEntityRecordPlayer.class, "RecordPlayer");
		TileEntity.addMapping(TileEntityDispenser.class, "Trap");
		TileEntity.addMapping(TileEntityDropper.class, "Dropper");
		TileEntity.addMapping(TileEntitySign.class, "Sign");
		TileEntity.addMapping(TileEntityMobSpawner.class, "MobSpawner");
		TileEntity.addMapping(TileEntityNote.class, "Music");
		TileEntity.addMapping(TileEntityPiston.class, "Piston");
		TileEntity.addMapping(TileEntityBrewingStand.class, "Cauldron");
		TileEntity.addMapping(TileEntityEnchantmentTable.class, "EnchantTable");
		TileEntity.addMapping(TileEntityEndPortal.class, "Airportal");
		TileEntity.addMapping(TileEntityCommandBlock.class, "Control");
		TileEntity.addMapping(TileEntityBeacon.class, "Beacon");
		TileEntity.addMapping(TileEntitySkull.class, "Skull");
		TileEntity.addMapping(TileEntityDaylightDetector.class, "DLDetector");
		TileEntity.addMapping(TileEntityHopper.class, "Hopper");
		TileEntity.addMapping(TileEntityComparator.class, "Comparator");
	}
}
