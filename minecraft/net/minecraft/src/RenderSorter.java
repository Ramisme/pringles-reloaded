package net.minecraft.src;

import java.util.Comparator;

public class RenderSorter implements Comparator {
	/** The entity (usually the player) that the camera is inside. */
	private final EntityLiving baseEntity;

	public RenderSorter(final EntityLiving par1EntityLiving) {
		baseEntity = par1EntityLiving;
	}

	public int doCompare(final WorldRenderer par1WorldRenderer,
			final WorldRenderer par2WorldRenderer) {
		if (par1WorldRenderer.isInFrustum && !par2WorldRenderer.isInFrustum) {
			return 1;
		} else if (par2WorldRenderer.isInFrustum
				&& !par1WorldRenderer.isInFrustum) {
			return -1;
		} else {
			final double var3 = par1WorldRenderer
					.distanceToEntitySquared(baseEntity);
			final double var5 = par2WorldRenderer
					.distanceToEntitySquared(baseEntity);
			return var3 < var5 ? 1
					: var3 > var5 ? -1
							: par1WorldRenderer.chunkIndex < par2WorldRenderer.chunkIndex ? 1
									: -1;
		}
	}

	@Override
	public int compare(final Object par1Obj, final Object par2Obj) {
		return doCompare((WorldRenderer) par1Obj, (WorldRenderer) par2Obj);
	}
}
