package net.minecraft.src;

import org.lwjgl.opengl.GL11;

public abstract class Render {
	protected RenderManager renderManager;
	private final ModelBase modelBase = new ModelBiped();
	protected RenderBlocks renderBlocks = new RenderBlocks();
	protected float shadowSize = 0.0F;

	/**
	 * Determines the darkness of the object's shadow. Higher value makes a
	 * darker shadow.
	 */
	protected float shadowOpaque = 1.0F;

	/**
	 * Actually renders the given argument. This is a synthetic bridge method,
	 * always casting down its argument and then handing it off to a worker
	 * function which does the actual work. In all probabilty, the class Render
	 * is generic (Render<T extends Entity) and this method has signature public
	 * void doRender(T entity, double d, double d1, double d2, float f, float
	 * f1). But JAD is pre 1.5 so doesn't do that.
	 */
	public abstract void doRender(Entity var1, double var2, double var4,
			double var6, float var8, float var9);

	/**
	 * loads the specified texture
	 */
	protected void loadTexture(final String par1Str) {
		renderManager.renderEngine.bindTexture(par1Str);
	}

	/**
	 * loads the specified downloadable texture or alternative built in texture
	 */
	protected boolean loadDownloadableImageTexture(final String par1Str,
			final String par2Str) {
		final RenderEngine var3 = renderManager.renderEngine;
		final int var4 = var3.getTextureForDownloadableImage(par1Str, par2Str);

		if (var4 >= 0) {
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, var4);
			var3.resetBoundTexture();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Renders fire on top of the entity. Args: entity, x, y, z, partialTickTime
	 */
	private void renderEntityOnFire(final Entity par1Entity, final double par2,
			final double par4, final double par6, final float par8) {
		GL11.glDisable(GL11.GL_LIGHTING);
		final Icon var9 = Block.fire.func_94438_c(0);
		final Icon var10 = Block.fire.func_94438_c(1);
		GL11.glPushMatrix();
		GL11.glTranslatef((float) par2, (float) par4, (float) par6);
		final float var11 = par1Entity.width * 1.4F;
		GL11.glScalef(var11, var11, var11);
		loadTexture("/terrain.png");
		final Tessellator var12 = Tessellator.instance;
		float var13 = 0.5F;
		final float var14 = 0.0F;
		float var15 = par1Entity.height / var11;
		float var16 = (float) (par1Entity.posY - par1Entity.boundingBox.minY);
		GL11.glRotatef(-renderManager.playerViewY, 0.0F, 1.0F, 0.0F);
		GL11.glTranslatef(0.0F, 0.0F, -0.3F + (int) var15 * 0.02F);
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		float var17 = 0.0F;
		int var18 = 0;
		var12.startDrawingQuads();

		while (var15 > 0.0F) {
			Icon var19;

			if (var18 % 2 == 0) {
				var19 = var9;
			} else {
				var19 = var10;
			}

			float var20 = var19.getMinU();
			final float var21 = var19.getMinV();
			float var22 = var19.getMaxU();
			final float var23 = var19.getMaxV();

			if (var18 / 2 % 2 == 0) {
				final float var24 = var22;
				var22 = var20;
				var20 = var24;
			}

			var12.addVertexWithUV(var13 - var14, 0.0F - var16, var17, var22,
					var23);
			var12.addVertexWithUV(-var13 - var14, 0.0F - var16, var17, var20,
					var23);
			var12.addVertexWithUV(-var13 - var14, 1.4F - var16, var17, var20,
					var21);
			var12.addVertexWithUV(var13 - var14, 1.4F - var16, var17, var22,
					var21);
			var15 -= 0.45F;
			var16 -= 0.45F;
			var13 *= 0.9F;
			var17 += 0.03F;
			++var18;
		}

		var12.draw();
		GL11.glPopMatrix();
		GL11.glEnable(GL11.GL_LIGHTING);
	}

	/**
	 * Renders the entity shadows at the position, shadow alpha and
	 * partialTickTime. Args: entity, x, y, z, shadowAlpha, partialTickTime
	 */
	private void renderShadow(final Entity par1Entity, final double par2,
			final double par4, final double par6, final float par8,
			final float par9) {
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		renderManager.renderEngine.bindTexture("%clamp%/misc/shadow.png");
		final World var10 = getWorldFromRenderManager();
		GL11.glDepthMask(false);
		float var11 = shadowSize;

		if (par1Entity instanceof EntityLiving) {
			final EntityLiving var12 = (EntityLiving) par1Entity;
			var11 *= var12.getRenderSizeModifier();

			if (var12.isChild()) {
				var11 *= 0.5F;
			}
		}

		final double var35 = par1Entity.lastTickPosX
				+ (par1Entity.posX - par1Entity.lastTickPosX) * par9;
		final double var14 = par1Entity.lastTickPosY
				+ (par1Entity.posY - par1Entity.lastTickPosY) * par9
				+ par1Entity.getShadowSize();
		final double var16 = par1Entity.lastTickPosZ
				+ (par1Entity.posZ - par1Entity.lastTickPosZ) * par9;
		final int var18 = MathHelper.floor_double(var35 - var11);
		final int var19 = MathHelper.floor_double(var35 + var11);
		final int var20 = MathHelper.floor_double(var14 - var11);
		final int var21 = MathHelper.floor_double(var14);
		final int var22 = MathHelper.floor_double(var16 - var11);
		final int var23 = MathHelper.floor_double(var16 + var11);
		final double var24 = par2 - var35;
		final double var26 = par4 - var14;
		final double var28 = par6 - var16;
		final Tessellator var30 = Tessellator.instance;
		var30.startDrawingQuads();

		for (int var31 = var18; var31 <= var19; ++var31) {
			for (int var32 = var20; var32 <= var21; ++var32) {
				for (int var33 = var22; var33 <= var23; ++var33) {
					final int var34 = var10.getBlockId(var31, var32 - 1, var33);

					if (var34 > 0
							&& var10.getBlockLightValue(var31, var32, var33) > 3) {
						renderShadowOnBlock(Block.blocksList[var34], par2, par4
								+ par1Entity.getShadowSize(), par6, var31,
								var32, var33, par8, var11, var24, var26
										+ par1Entity.getShadowSize(), var28);
					}
				}
			}
		}

		var30.draw();
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glDepthMask(true);
	}

	/**
	 * Returns the render manager's world object
	 */
	private World getWorldFromRenderManager() {
		return renderManager.worldObj;
	}

	/**
	 * Renders a shadow projected down onto the specified block. Brightness of
	 * the block plus how far away on the Y axis determines the alpha of the
	 * shadow. Args: block, centerX, centerY, centerZ, blockX, blockY, blockZ,
	 * baseAlpha, shadowSize, xOffset, yOffset, zOffset
	 */
	private void renderShadowOnBlock(final Block par1Block, final double par2,
			final double par4, final double par6, final int par8,
			final int par9, final int par10, final float par11,
			final float par12, final double par13, final double par15,
			final double par17) {
		final Tessellator var19 = Tessellator.instance;

		if (par1Block.renderAsNormalBlock()) {
			double var20 = (par11 - (par4 - (par9 + par15)) / 2.0D)
					* 0.5D
					* getWorldFromRenderManager().getLightBrightness(par8,
							par9, par10);

			if (var20 >= 0.0D) {
				if (var20 > 1.0D) {
					var20 = 1.0D;
				}

				var19.setColorRGBA_F(1.0F, 1.0F, 1.0F, (float) var20);
				final double var22 = par8 + par1Block.getBlockBoundsMinX()
						+ par13;
				final double var24 = par8 + par1Block.getBlockBoundsMaxX()
						+ par13;
				final double var26 = par9 + par1Block.getBlockBoundsMinY()
						+ par15 + 0.015625D;
				final double var28 = par10 + par1Block.getBlockBoundsMinZ()
						+ par17;
				final double var30 = par10 + par1Block.getBlockBoundsMaxZ()
						+ par17;
				final float var32 = (float) ((par2 - var22) / 2.0D / par12 + 0.5D);
				final float var33 = (float) ((par2 - var24) / 2.0D / par12 + 0.5D);
				final float var34 = (float) ((par6 - var28) / 2.0D / par12 + 0.5D);
				final float var35 = (float) ((par6 - var30) / 2.0D / par12 + 0.5D);
				var19.addVertexWithUV(var22, var26, var28, var32, var34);
				var19.addVertexWithUV(var22, var26, var30, var32, var35);
				var19.addVertexWithUV(var24, var26, var30, var33, var35);
				var19.addVertexWithUV(var24, var26, var28, var33, var34);
			}
		}
	}

	/**
	 * Renders a white box with the bounds of the AABB translated by the offset.
	 * Args: aabb, x, y, z
	 */
	public static void renderOffsetAABB(final AxisAlignedBB par0AxisAlignedBB,
			final double par1, final double par3, final double par5) {
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		final Tessellator var7 = Tessellator.instance;
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		var7.startDrawingQuads();
		var7.setTranslation(par1, par3, par5);
		var7.setNormal(0.0F, 0.0F, -1.0F);
		var7.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.maxY,
				par0AxisAlignedBB.minZ);
		var7.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.maxY,
				par0AxisAlignedBB.minZ);
		var7.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.minY,
				par0AxisAlignedBB.minZ);
		var7.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.minY,
				par0AxisAlignedBB.minZ);
		var7.setNormal(0.0F, 0.0F, 1.0F);
		var7.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.minY,
				par0AxisAlignedBB.maxZ);
		var7.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.minY,
				par0AxisAlignedBB.maxZ);
		var7.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.maxY,
				par0AxisAlignedBB.maxZ);
		var7.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.maxY,
				par0AxisAlignedBB.maxZ);
		var7.setNormal(0.0F, -1.0F, 0.0F);
		var7.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.minY,
				par0AxisAlignedBB.minZ);
		var7.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.minY,
				par0AxisAlignedBB.minZ);
		var7.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.minY,
				par0AxisAlignedBB.maxZ);
		var7.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.minY,
				par0AxisAlignedBB.maxZ);
		var7.setNormal(0.0F, 1.0F, 0.0F);
		var7.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.maxY,
				par0AxisAlignedBB.maxZ);
		var7.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.maxY,
				par0AxisAlignedBB.maxZ);
		var7.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.maxY,
				par0AxisAlignedBB.minZ);
		var7.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.maxY,
				par0AxisAlignedBB.minZ);
		var7.setNormal(-1.0F, 0.0F, 0.0F);
		var7.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.minY,
				par0AxisAlignedBB.maxZ);
		var7.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.maxY,
				par0AxisAlignedBB.maxZ);
		var7.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.maxY,
				par0AxisAlignedBB.minZ);
		var7.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.minY,
				par0AxisAlignedBB.minZ);
		var7.setNormal(1.0F, 0.0F, 0.0F);
		var7.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.minY,
				par0AxisAlignedBB.minZ);
		var7.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.maxY,
				par0AxisAlignedBB.minZ);
		var7.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.maxY,
				par0AxisAlignedBB.maxZ);
		var7.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.minY,
				par0AxisAlignedBB.maxZ);
		var7.setTranslation(0.0D, 0.0D, 0.0D);
		var7.draw();
		GL11.glEnable(GL11.GL_TEXTURE_2D);
	}

	/**
	 * Adds to the tesselator a box using the aabb for the bounds. Args: aabb
	 */
	public static void renderAABB(final AxisAlignedBB par0AxisAlignedBB) {
		final Tessellator var1 = Tessellator.instance;
		var1.startDrawingQuads();
		var1.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.maxY,
				par0AxisAlignedBB.minZ);
		var1.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.maxY,
				par0AxisAlignedBB.minZ);
		var1.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.minY,
				par0AxisAlignedBB.minZ);
		var1.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.minY,
				par0AxisAlignedBB.minZ);
		var1.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.minY,
				par0AxisAlignedBB.maxZ);
		var1.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.minY,
				par0AxisAlignedBB.maxZ);
		var1.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.maxY,
				par0AxisAlignedBB.maxZ);
		var1.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.maxY,
				par0AxisAlignedBB.maxZ);
		var1.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.minY,
				par0AxisAlignedBB.minZ);
		var1.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.minY,
				par0AxisAlignedBB.minZ);
		var1.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.minY,
				par0AxisAlignedBB.maxZ);
		var1.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.minY,
				par0AxisAlignedBB.maxZ);
		var1.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.maxY,
				par0AxisAlignedBB.maxZ);
		var1.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.maxY,
				par0AxisAlignedBB.maxZ);
		var1.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.maxY,
				par0AxisAlignedBB.minZ);
		var1.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.maxY,
				par0AxisAlignedBB.minZ);
		var1.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.minY,
				par0AxisAlignedBB.maxZ);
		var1.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.maxY,
				par0AxisAlignedBB.maxZ);
		var1.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.maxY,
				par0AxisAlignedBB.minZ);
		var1.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.minY,
				par0AxisAlignedBB.minZ);
		var1.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.minY,
				par0AxisAlignedBB.minZ);
		var1.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.maxY,
				par0AxisAlignedBB.minZ);
		var1.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.maxY,
				par0AxisAlignedBB.maxZ);
		var1.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.minY,
				par0AxisAlignedBB.maxZ);
		var1.draw();
	}

	/**
	 * Sets the RenderManager.
	 */
	public void setRenderManager(final RenderManager par1RenderManager) {
		renderManager = par1RenderManager;
	}

	/**
	 * Renders the entity's shadow and fire (if its on fire). Args: entity, x,
	 * y, z, yaw, partialTickTime
	 */
	public void doRenderShadowAndFire(final Entity par1Entity,
			final double par2, final double par4, final double par6,
			final float par8, final float par9) {
		if (renderManager.options.fancyGraphics && shadowSize > 0.0F
				&& !par1Entity.isInvisible()) {
			final double var10 = renderManager.getDistanceToCamera(
					par1Entity.posX, par1Entity.posY, par1Entity.posZ);
			final float var12 = (float) ((1.0D - var10 / 256.0D) * shadowOpaque);

			if (var12 > 0.0F) {
				renderShadow(par1Entity, par2, par4, par6, var12, par9);
			}
		}

		if (par1Entity.canRenderOnFire()) {
			renderEntityOnFire(par1Entity, par2, par4, par6, par9);
		}
	}

	/**
	 * Returns the font renderer from the set render manager
	 */
	public FontRenderer getFontRendererFromRenderManager() {
		return renderManager.getFontRenderer();
	}

	public void updateIcons(final IconRegister par1IconRegister) {
	}
}
