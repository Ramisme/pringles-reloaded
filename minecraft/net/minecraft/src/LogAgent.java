package net.minecraft.src;

import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LogAgent implements ILogAgent {
	private final Logger serverLogger;
	private final String logFile;
	private final String loggerName;
	private final String loggerPrefix;

	public LogAgent(final String par1Str, final String par2Str,
			final String par3Str) {
		serverLogger = Logger.getLogger(par1Str);
		loggerName = par1Str;
		loggerPrefix = par2Str;
		logFile = par3Str;
		setupLogger();
	}

	/**
	 * Sets up the logger for usage.
	 */
	private void setupLogger() {
		serverLogger.setUseParentHandlers(false);
		final Handler[] var1 = serverLogger.getHandlers();
		final int var2 = var1.length;

		for (int var3 = 0; var3 < var2; ++var3) {
			final Handler var4 = var1[var3];
			serverLogger.removeHandler(var4);
		}

		final LogFormatter var6 = new LogFormatter(this, (LogAgentINNER1) null);
		final ConsoleHandler var7 = new ConsoleHandler();
		var7.setFormatter(var6);
		serverLogger.addHandler(var7);

		try {
			final FileHandler var8 = new FileHandler(logFile, true);
			var8.setFormatter(var6);
			serverLogger.addHandler(var8);
		} catch (final Exception var5) {
			serverLogger.log(Level.WARNING, "Failed to log " + loggerName
					+ " to " + logFile, var5);
		}
	}

	@Override
	public void logInfo(final String par1Str) {
		serverLogger.log(Level.INFO, par1Str);
	}

	@Override
	public void logWarning(final String par1Str) {
		serverLogger.log(Level.WARNING, par1Str);
	}

	@Override
	public void logWarningFormatted(final String par1Str,
			final Object... par2ArrayOfObj) {
		serverLogger.log(Level.WARNING, par1Str, par2ArrayOfObj);
	}

	@Override
	public void logWarningException(final String par1Str,
			final Throwable par2Throwable) {
		serverLogger.log(Level.WARNING, par1Str, par2Throwable);
	}

	@Override
	public void logSevere(final String par1Str) {
		serverLogger.log(Level.SEVERE, par1Str);
	}

	@Override
	public void logSevereException(final String par1Str,
			final Throwable par2Throwable) {
		serverLogger.log(Level.SEVERE, par1Str, par2Throwable);
	}

	@Override
	public void logFine(final String par1Str) {
		serverLogger.log(Level.FINE, par1Str);
	}

	static String func_98237_a(final LogAgent par0LogAgent) {
		return par0LogAgent.loggerPrefix;
	}
}
