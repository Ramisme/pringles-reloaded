package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.security.PublicKey;

public class Packet253ServerAuthData extends Packet {
	private String serverId;
	private PublicKey publicKey;
	private byte[] verifyToken = new byte[0];

	public Packet253ServerAuthData() {
	}

	public Packet253ServerAuthData(final String par1Str,
			final PublicKey par2PublicKey, final byte[] par3ArrayOfByte) {
		serverId = par1Str;
		publicKey = par2PublicKey;
		verifyToken = par3ArrayOfByte;
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		serverId = Packet.readString(par1DataInputStream, 20);
		publicKey = CryptManager.decodePublicKey(Packet
				.readBytesFromStream(par1DataInputStream));
		verifyToken = Packet.readBytesFromStream(par1DataInputStream);
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		Packet.writeString(serverId, par1DataOutputStream);
		Packet.writeByteArray(par1DataOutputStream, publicKey.getEncoded());
		Packet.writeByteArray(par1DataOutputStream, verifyToken);
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleServerAuthData(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 2 + serverId.length() * 2 + 2 + publicKey.getEncoded().length
				+ 2 + verifyToken.length;
	}

	public String getServerId() {
		return serverId;
	}

	public PublicKey getPublicKey() {
		return publicKey;
	}

	public byte[] getVerifyToken() {
		return verifyToken;
	}
}
