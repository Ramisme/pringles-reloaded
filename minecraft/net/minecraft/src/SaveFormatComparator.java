package net.minecraft.src;

public class SaveFormatComparator implements Comparable {
	/** the file name of this save */
	private final String fileName;

	/** the displayed name of this save file */
	private final String displayName;
	private final long lastTimePlayed;
	private final boolean requiresConversion;

	/** Instance of EnumGameType. */
	private final EnumGameType theEnumGameType;
	private final boolean hardcore;
	private final boolean cheatsEnabled;

	public SaveFormatComparator(final String par1Str, final String par2Str,
			final long par3, final long par5,
			final EnumGameType par7EnumGameType, final boolean par8,
			final boolean par9, final boolean par10) {
		fileName = par1Str;
		displayName = par2Str;
		lastTimePlayed = par3;
		theEnumGameType = par7EnumGameType;
		requiresConversion = par8;
		hardcore = par9;
		cheatsEnabled = par10;
	}

	/**
	 * return the file name
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * return the display name of the save
	 */
	public String getDisplayName() {
		return displayName;
	}

	public boolean requiresConversion() {
		return requiresConversion;
	}

	public long getLastTimePlayed() {
		return lastTimePlayed;
	}

	public int compareTo(final SaveFormatComparator par1SaveFormatComparator) {
		return lastTimePlayed < par1SaveFormatComparator.lastTimePlayed ? 1
				: lastTimePlayed > par1SaveFormatComparator.lastTimePlayed ? -1
						: fileName.compareTo(par1SaveFormatComparator.fileName);
	}

	/**
	 * Gets the EnumGameType.
	 */
	public EnumGameType getEnumGameType() {
		return theEnumGameType;
	}

	public boolean isHardcoreModeEnabled() {
		return hardcore;
	}

	/**
	 * @return {@code true} if cheats are enabled for this world
	 */
	public boolean getCheatsEnabled() {
		return cheatsEnabled;
	}

	@Override
	public int compareTo(final Object par1Obj) {
		return this.compareTo((SaveFormatComparator) par1Obj);
	}
}
