package net.minecraft.src;

import java.util.Random;

public class BlockMushroomCap extends Block {
	private static final String[] field_94429_a = new String[] {
			"mushroom_skin_brown", "mushroom_skin_red" };

	/** The mushroom type. 0 for brown, 1 for red. */
	private final int mushroomType;
	private Icon[] iconArray;
	private Icon field_94426_cO;
	private Icon field_94427_cP;

	public BlockMushroomCap(final int par1, final Material par2Material,
			final int par3) {
		super(par1, par2Material);
		mushroomType = par3;
	}

	/**
	 * From the specified side and block metadata retrieves the blocks texture.
	 * Args: side, metadata
	 */
	@Override
	public Icon getIcon(final int par1, final int par2) {
		return par2 == 10 && par1 > 1 ? field_94426_cO : par2 >= 1 && par2 <= 9
				&& par1 == 1 ? iconArray[mushroomType] : par2 >= 1 && par2 <= 3
				&& par1 == 2 ? iconArray[mushroomType] : par2 >= 7 && par2 <= 9
				&& par1 == 3 ? iconArray[mushroomType] : (par2 == 1
				|| par2 == 4 || par2 == 7)
				&& par1 == 4 ? iconArray[mushroomType] : (par2 == 3
				|| par2 == 6 || par2 == 9)
				&& par1 == 5 ? iconArray[mushroomType]
				: par2 == 14 ? iconArray[mushroomType]
						: par2 == 15 ? field_94426_cO : field_94427_cP;
	}

	/**
	 * Returns the quantity of items to drop on block destruction.
	 */
	@Override
	public int quantityDropped(final Random par1Random) {
		int var2 = par1Random.nextInt(10) - 7;

		if (var2 < 0) {
			var2 = 0;
		}

		return var2;
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return Block.mushroomBrown.blockID + mushroomType;
	}

	/**
	 * only called by clickMiddleMouseButton , and passed to
	 * inventory.setCurrentItem (along with isCreative)
	 */
	@Override
	public int idPicked(final World par1World, final int par2, final int par3,
			final int par4) {
		return Block.mushroomBrown.blockID + mushroomType;
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		iconArray = new Icon[BlockMushroomCap.field_94429_a.length];

		for (int var2 = 0; var2 < iconArray.length; ++var2) {
			iconArray[var2] = par1IconRegister
					.registerIcon(BlockMushroomCap.field_94429_a[var2]);
		}

		field_94427_cP = par1IconRegister.registerIcon("mushroom_inside");
		field_94426_cO = par1IconRegister.registerIcon("mushroom_skin_stem");
	}
}
