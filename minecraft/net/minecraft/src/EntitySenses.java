package net.minecraft.src;

import java.util.ArrayList;
import java.util.List;

public class EntitySenses {
	EntityLiving entityObj;

	/** Cache of entities which we can see */
	List seenEntities = new ArrayList();

	/** Cache of entities which we cannot see */
	List unseenEntities = new ArrayList();

	public EntitySenses(final EntityLiving par1EntityLiving) {
		entityObj = par1EntityLiving;
	}

	/**
	 * Clears canSeeCachePositive and canSeeCacheNegative.
	 */
	public void clearSensingCache() {
		seenEntities.clear();
		unseenEntities.clear();
	}

	/**
	 * Checks, whether 'our' entity can see the entity given as argument (true)
	 * or not (false), caching the result.
	 */
	public boolean canSee(final Entity par1Entity) {
		if (seenEntities.contains(par1Entity)) {
			return true;
		} else if (unseenEntities.contains(par1Entity)) {
			return false;
		} else {
			entityObj.worldObj.theProfiler.startSection("canSee");
			final boolean var2 = entityObj.canEntityBeSeen(par1Entity);
			entityObj.worldObj.theProfiler.endSection();

			if (var2) {
				seenEntities.add(par1Entity);
			} else {
				unseenEntities.add(par1Entity);
			}

			return var2;
		}
	}
}
