package net.minecraft.src;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.awt.image.ImageObserver;

public class ImageBufferDownload implements IImageBuffer {
	private int[] imageData;
	private int imageWidth;
	private int imageHeight;

	@Override
	public BufferedImage parseUserSkin(final BufferedImage par1BufferedImage) {
		if (par1BufferedImage == null) {
			return null;
		} else {
			imageWidth = 64;
			imageHeight = 32;

			for (final BufferedImage var2 = par1BufferedImage; imageWidth < var2
					.getWidth() || imageHeight < var2.getHeight(); imageHeight *= 2) {
				imageWidth *= 2;
			}

			final BufferedImage var3 = new BufferedImage(imageWidth,
					imageHeight, 2);
			final Graphics var4 = var3.getGraphics();
			var4.drawImage(par1BufferedImage, 0, 0, (ImageObserver) null);
			var4.dispose();
			imageData = ((DataBufferInt) var3.getRaster().getDataBuffer())
					.getData();
			final int var5 = imageWidth;
			final int var6 = imageHeight;
			setAreaOpaque(0, 0, var5 / 2, var6 / 2);
			setAreaTransparent(var5 / 2, 0, var5, var6);
			setAreaOpaque(0, var6 / 2, var5, var6);
			boolean var7 = false;
			int var8;
			int var9;
			int var10;

			for (var8 = var5 / 2; var8 < var5; ++var8) {
				for (var9 = 0; var9 < var6 / 2; ++var9) {
					var10 = imageData[var8 + var9 * var5];

					if ((var10 >> 24 & 255) < 128) {
						var7 = true;
					}
				}
			}

			if (!var7) {
				for (var8 = var5 / 2; var8 < var5; ++var8) {
					for (var9 = 0; var9 < var6 / 2; ++var9) {
						var10 = imageData[var8 + var9 * var5];

						if ((var10 >> 24 & 255) < 128) {
						}
					}
				}
			}

			return var3;
		}
	}

	/**
	 * Makes the given area of the image transparent if it was previously
	 * completely opaque (used to remove the outer layer of a skin around the
	 * head if it was saved all opaque; this would be redundant so it's assumed
	 * that the skin maker is just using an image editor without an alpha
	 * channel)
	 */
	private void setAreaTransparent(final int par1, final int par2,
			final int par3, final int par4) {
		if (!hasTransparency(par1, par2, par3, par4)) {
			for (int var5 = par1; var5 < par3; ++var5) {
				for (int var6 = par2; var6 < par4; ++var6) {
					imageData[var5 + var6 * imageWidth] &= 16777215;
				}
			}
		}
	}

	/**
	 * Makes the given area of the image opaque
	 */
	private void setAreaOpaque(final int par1, final int par2, final int par3,
			final int par4) {
		for (int var5 = par1; var5 < par3; ++var5) {
			for (int var6 = par2; var6 < par4; ++var6) {
				imageData[var5 + var6 * imageWidth] |= -16777216;
			}
		}
	}

	/**
	 * Returns true if the given area of the image contains transparent pixels
	 */
	private boolean hasTransparency(final int par1, final int par2,
			final int par3, final int par4) {
		for (int var5 = par1; var5 < par3; ++var5) {
			for (int var6 = par2; var6 < par4; ++var6) {
				final int var7 = imageData[var5 + var6 * imageWidth];

				if ((var7 >> 24 & 255) < 128) {
					return true;
				}
			}
		}

		return false;
	}
}
