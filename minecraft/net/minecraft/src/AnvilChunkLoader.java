package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class AnvilChunkLoader implements IChunkLoader, IThreadedFileIO {
	private final List chunksToRemove = new ArrayList();
	private final Set pendingAnvilChunksCoordinates = new HashSet();
	private final Object syncLockObject = new Object();

	/** Save directory for chunks using the Anvil format */
	private final File chunkSaveLocation;

	public AnvilChunkLoader(final File par1File) {
		chunkSaveLocation = par1File;
	}

	/**
	 * Loads the specified(XZ) chunk into the specified world.
	 */
	@Override
	public Chunk loadChunk(final World par1World, final int par2, final int par3)
			throws IOException {
		NBTTagCompound var4 = null;
		final ChunkCoordIntPair var5 = new ChunkCoordIntPair(par2, par3);
		synchronized (syncLockObject) {
			if (pendingAnvilChunksCoordinates.contains(var5)) {
				for (int var7 = 0; var7 < chunksToRemove.size(); ++var7) {
					if (((AnvilChunkLoaderPending) chunksToRemove.get(var7)).chunkCoordinate
							.equals(var5)) {
						var4 = ((AnvilChunkLoaderPending) chunksToRemove
								.get(var7)).nbtTags;
						break;
					}
				}
			}
		}

		if (var4 == null) {
			final DataInputStream var10 = RegionFileCache.getChunkInputStream(
					chunkSaveLocation, par2, par3);

			if (var10 == null) {
				return null;
			}

			var4 = CompressedStreamTools.read(var10);
		}

		return checkedReadChunkFromNBT(par1World, par2, par3, var4);
	}

	/**
	 * Wraps readChunkFromNBT. Checks the coordinates and several NBT tags.
	 */
	protected Chunk checkedReadChunkFromNBT(final World par1World,
			final int par2, final int par3,
			final NBTTagCompound par4NBTTagCompound) {
		if (!par4NBTTagCompound.hasKey("Level")) {
			par1World.getWorldLogAgent().logSevere(
					"Chunk file at " + par2 + "," + par3
							+ " is missing level data, skipping");
			return null;
		} else if (!par4NBTTagCompound.getCompoundTag("Level").hasKey(
				"Sections")) {
			par1World.getWorldLogAgent().logSevere(
					"Chunk file at " + par2 + "," + par3
							+ " is missing block data, skipping");
			return null;
		} else {
			Chunk var5 = readChunkFromNBT(par1World,
					par4NBTTagCompound.getCompoundTag("Level"));

			if (!var5.isAtLocation(par2, par3)) {
				par1World
						.getWorldLogAgent()
						.logSevere(
								"Chunk file at "
										+ par2
										+ ","
										+ par3
										+ " is in the wrong location; relocating. (Expected "
										+ par2 + ", " + par3 + ", got "
										+ var5.xPosition + ", "
										+ var5.zPosition + ")");
				par4NBTTagCompound.setInteger("xPos", par2);
				par4NBTTagCompound.setInteger("zPos", par3);
				var5 = readChunkFromNBT(par1World,
						par4NBTTagCompound.getCompoundTag("Level"));
			}

			return var5;
		}
	}

	@Override
	public void saveChunk(final World par1World, final Chunk par2Chunk)
			throws MinecraftException, IOException {
		par1World.checkSessionLock();

		try {
			final NBTTagCompound var3 = new NBTTagCompound();
			final NBTTagCompound var4 = new NBTTagCompound();
			var3.setTag("Level", var4);
			writeChunkToNBT(par2Chunk, par1World, var4);
			addChunkToPending(par2Chunk.getChunkCoordIntPair(), var3);
		} catch (final Exception var5) {
			var5.printStackTrace();
		}
	}

	protected void addChunkToPending(
			final ChunkCoordIntPair par1ChunkCoordIntPair,
			final NBTTagCompound par2NBTTagCompound) {
		synchronized (syncLockObject) {
			if (pendingAnvilChunksCoordinates.contains(par1ChunkCoordIntPair)) {
				for (int var4 = 0; var4 < chunksToRemove.size(); ++var4) {
					if (((AnvilChunkLoaderPending) chunksToRemove.get(var4)).chunkCoordinate
							.equals(par1ChunkCoordIntPair)) {
						chunksToRemove.set(var4, new AnvilChunkLoaderPending(
								par1ChunkCoordIntPair, par2NBTTagCompound));
						return;
					}
				}
			}

			chunksToRemove.add(new AnvilChunkLoaderPending(
					par1ChunkCoordIntPair, par2NBTTagCompound));
			pendingAnvilChunksCoordinates.add(par1ChunkCoordIntPair);
			ThreadedFileIOBase.threadedIOInstance.queueIO(this);
		}
	}

	/**
	 * Returns a boolean stating if the write was unsuccessful.
	 */
	@Override
	public boolean writeNextIO() {
		AnvilChunkLoaderPending var1 = null;
		synchronized (syncLockObject) {
			if (chunksToRemove.isEmpty()) {
				return false;
			}

			var1 = (AnvilChunkLoaderPending) chunksToRemove.remove(0);
			pendingAnvilChunksCoordinates.remove(var1.chunkCoordinate);
		}

		if (var1 != null) {
			try {
				writeChunkNBTTags(var1);
			} catch (final Exception var4) {
				var4.printStackTrace();
			}
		}

		return true;
	}

	private void writeChunkNBTTags(
			final AnvilChunkLoaderPending par1AnvilChunkLoaderPending)
			throws IOException {
		final DataOutputStream var2 = RegionFileCache.getChunkOutputStream(
				chunkSaveLocation,
				par1AnvilChunkLoaderPending.chunkCoordinate.chunkXPos,
				par1AnvilChunkLoaderPending.chunkCoordinate.chunkZPos);
		CompressedStreamTools.write(par1AnvilChunkLoaderPending.nbtTags, var2);
		var2.close();
	}

	/**
	 * Save extra data associated with this Chunk not normally saved during
	 * autosave, only during chunk unload. Currently unused.
	 */
	@Override
	public void saveExtraChunkData(final World par1World, final Chunk par2Chunk) {
	}

	/**
	 * Called every World.tick()
	 */
	@Override
	public void chunkTick() {
	}

	/**
	 * Save extra data not associated with any Chunk. Not saved during autosave,
	 * only during world unload. Currently unused.
	 */
	@Override
	public void saveExtraData() {
		while (writeNextIO()) {
			;
		}
	}

	/**
	 * Writes the Chunk passed as an argument to the NBTTagCompound also passed,
	 * using the World argument to retrieve the Chunk's last update time.
	 */
	private void writeChunkToNBT(final Chunk par1Chunk, final World par2World,
			final NBTTagCompound par3NBTTagCompound) {
		par3NBTTagCompound.setInteger("xPos", par1Chunk.xPosition);
		par3NBTTagCompound.setInteger("zPos", par1Chunk.zPosition);
		par3NBTTagCompound.setLong("LastUpdate", par2World.getTotalWorldTime());
		par3NBTTagCompound.setIntArray("HeightMap", par1Chunk.heightMap);
		par3NBTTagCompound.setBoolean("TerrainPopulated",
				par1Chunk.isTerrainPopulated);
		final ExtendedBlockStorage[] var4 = par1Chunk.getBlockStorageArray();
		final NBTTagList var5 = new NBTTagList("Sections");
		final boolean var6 = !par2World.provider.hasNoSky;
		final ExtendedBlockStorage[] var7 = var4;
		int var8 = var4.length;
		NBTTagCompound var11;

		for (int var9 = 0; var9 < var8; ++var9) {
			final ExtendedBlockStorage var10 = var7[var9];

			if (var10 != null) {
				var11 = new NBTTagCompound();
				var11.setByte("Y", (byte) (var10.getYLocation() >> 4 & 255));
				var11.setByteArray("Blocks", var10.getBlockLSBArray());

				if (var10.getBlockMSBArray() != null) {
					var11.setByteArray("Add", var10.getBlockMSBArray().data);
				}

				var11.setByteArray("Data", var10.getMetadataArray().data);
				var11.setByteArray("BlockLight",
						var10.getBlocklightArray().data);

				if (var6) {
					var11.setByteArray("SkyLight",
							var10.getSkylightArray().data);
				} else {
					var11.setByteArray("SkyLight",
							new byte[var10.getBlocklightArray().data.length]);
				}

				var5.appendTag(var11);
			}
		}

		par3NBTTagCompound.setTag("Sections", var5);
		par3NBTTagCompound.setByteArray("Biomes", par1Chunk.getBiomeArray());
		par1Chunk.hasEntities = false;
		final NBTTagList var16 = new NBTTagList();
		Iterator var18;

		for (var8 = 0; var8 < par1Chunk.entityLists.length; ++var8) {
			var18 = par1Chunk.entityLists[var8].iterator();

			while (var18.hasNext()) {
				final Entity var21 = (Entity) var18.next();
				var11 = new NBTTagCompound();

				if (var21.addEntityID(var11)) {
					par1Chunk.hasEntities = true;
					var16.appendTag(var11);
				}
			}
		}

		par3NBTTagCompound.setTag("Entities", var16);
		final NBTTagList var17 = new NBTTagList();
		var18 = par1Chunk.chunkTileEntityMap.values().iterator();

		while (var18.hasNext()) {
			final TileEntity var22 = (TileEntity) var18.next();
			var11 = new NBTTagCompound();
			var22.writeToNBT(var11);
			var17.appendTag(var11);
		}

		par3NBTTagCompound.setTag("TileEntities", var17);
		final List var20 = par2World.getPendingBlockUpdates(par1Chunk, false);

		if (var20 != null) {
			final long var19 = par2World.getTotalWorldTime();
			final NBTTagList var12 = new NBTTagList();
			final Iterator var13 = var20.iterator();

			while (var13.hasNext()) {
				final NextTickListEntry var14 = (NextTickListEntry) var13
						.next();
				final NBTTagCompound var15 = new NBTTagCompound();
				var15.setInteger("i", var14.blockID);
				var15.setInteger("x", var14.xCoord);
				var15.setInteger("y", var14.yCoord);
				var15.setInteger("z", var14.zCoord);
				var15.setInteger("t", (int) (var14.scheduledTime - var19));
				var15.setInteger("p", var14.field_82754_f);
				var12.appendTag(var15);
			}

			par3NBTTagCompound.setTag("TileTicks", var12);
		}
	}

	/**
	 * Reads the data stored in the passed NBTTagCompound and creates a Chunk
	 * with that data in the passed World. Returns the created Chunk.
	 */
	private Chunk readChunkFromNBT(final World par1World,
			final NBTTagCompound par2NBTTagCompound) {
		final int var3 = par2NBTTagCompound.getInteger("xPos");
		final int var4 = par2NBTTagCompound.getInteger("zPos");
		final Chunk var5 = new Chunk(par1World, var3, var4);
		var5.heightMap = par2NBTTagCompound.getIntArray("HeightMap");
		var5.isTerrainPopulated = par2NBTTagCompound
				.getBoolean("TerrainPopulated");
		final NBTTagList var6 = par2NBTTagCompound.getTagList("Sections");
		final byte var7 = 16;
		final ExtendedBlockStorage[] var8 = new ExtendedBlockStorage[var7];
		final boolean var9 = !par1World.provider.hasNoSky;

		for (int var10 = 0; var10 < var6.tagCount(); ++var10) {
			final NBTTagCompound var11 = (NBTTagCompound) var6.tagAt(var10);
			final byte var12 = var11.getByte("Y");
			final ExtendedBlockStorage var13 = new ExtendedBlockStorage(
					var12 << 4, var9);
			var13.setBlockLSBArray(var11.getByteArray("Blocks"));

			if (var11.hasKey("Add")) {
				var13.setBlockMSBArray(new NibbleArray(var11
						.getByteArray("Add"), 4));
			}

			var13.setBlockMetadataArray(new NibbleArray(var11
					.getByteArray("Data"), 4));
			var13.setBlocklightArray(new NibbleArray(var11
					.getByteArray("BlockLight"), 4));

			if (var9) {
				var13.setSkylightArray(new NibbleArray(var11
						.getByteArray("SkyLight"), 4));
			}

			var13.removeInvalidBlocks();
			var8[var12] = var13;
		}

		var5.setStorageArrays(var8);

		if (par2NBTTagCompound.hasKey("Biomes")) {
			var5.setBiomeArray(par2NBTTagCompound.getByteArray("Biomes"));
		}

		final NBTTagList var18 = par2NBTTagCompound.getTagList("Entities");

		if (var18 != null) {
			for (int var17 = 0; var17 < var18.tagCount(); ++var17) {
				final NBTTagCompound var19 = (NBTTagCompound) var18
						.tagAt(var17);
				final Entity var25 = EntityList.createEntityFromNBT(var19,
						par1World);
				var5.hasEntities = true;

				if (var25 != null) {
					var5.addEntity(var25);
					Entity var14 = var25;

					for (NBTTagCompound var15 = var19; var15.hasKey("Riding"); var15 = var15
							.getCompoundTag("Riding")) {
						final Entity var16 = EntityList.createEntityFromNBT(
								var15.getCompoundTag("Riding"), par1World);

						if (var16 != null) {
							var5.addEntity(var16);
							var14.mountEntity(var16);
						}

						var14 = var16;
					}
				}
			}
		}

		final NBTTagList var21 = par2NBTTagCompound.getTagList("TileEntities");

		if (var21 != null) {
			for (int var20 = 0; var20 < var21.tagCount(); ++var20) {
				final NBTTagCompound var22 = (NBTTagCompound) var21
						.tagAt(var20);
				final TileEntity var27 = TileEntity.createAndLoadEntity(var22);

				if (var27 != null) {
					var5.addTileEntity(var27);
				}
			}
		}

		if (par2NBTTagCompound.hasKey("TileTicks")) {
			final NBTTagList var24 = par2NBTTagCompound.getTagList("TileTicks");

			if (var24 != null) {
				for (int var23 = 0; var23 < var24.tagCount(); ++var23) {
					final NBTTagCompound var26 = (NBTTagCompound) var24
							.tagAt(var23);
					par1World.scheduleBlockUpdateFromLoad(
							var26.getInteger("x"), var26.getInteger("y"),
							var26.getInteger("z"), var26.getInteger("i"),
							var26.getInteger("t"), var26.getInteger("p"));
				}
			}
		}

		return var5;
	}
}
