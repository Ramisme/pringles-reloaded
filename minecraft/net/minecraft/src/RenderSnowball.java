package net.minecraft.src;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

public class RenderSnowball extends Render {
	private final Item field_94151_a;
	private final int field_94150_f;

	public RenderSnowball(final Item par1, final int par2) {
		field_94151_a = par1;
		field_94150_f = par2;
	}

	public RenderSnowball(final Item par1) {
		this(par1, 0);
	}

	/**
	 * Actually renders the given argument. This is a synthetic bridge method,
	 * always casting down its argument and then handing it off to a worker
	 * function which does the actual work. In all probabilty, the class Render
	 * is generic (Render<T extends Entity) and this method has signature public
	 * void doRender(T entity, double d, double d1, double d2, float f, float
	 * f1). But JAD is pre 1.5 so doesn't do that.
	 */
	@Override
	public void doRender(final Entity par1Entity, final double par2,
			final double par4, final double par6, final float par8,
			final float par9) {
		final Icon var10 = field_94151_a.getIconFromDamage(field_94150_f);

		if (var10 != null) {
			GL11.glPushMatrix();
			GL11.glTranslatef((float) par2, (float) par4, (float) par6);
			GL11.glEnable(GL12.GL_RESCALE_NORMAL);
			GL11.glScalef(0.5F, 0.5F, 0.5F);
			loadTexture("/gui/items.png");
			final Tessellator var11 = Tessellator.instance;

			if (var10 == ItemPotion.func_94589_d("potion_splash")) {
				final int var12 = PotionHelper.func_77915_a(
						((EntityPotion) par1Entity).getPotionDamage(), false);
				final float var13 = (var12 >> 16 & 255) / 255.0F;
				final float var14 = (var12 >> 8 & 255) / 255.0F;
				final float var15 = (var12 & 255) / 255.0F;
				GL11.glColor3f(var13, var14, var15);
				GL11.glPushMatrix();
				func_77026_a(var11, ItemPotion.func_94589_d("potion_contents"));
				GL11.glPopMatrix();
				GL11.glColor3f(1.0F, 1.0F, 1.0F);
			}

			func_77026_a(var11, var10);
			GL11.glDisable(GL12.GL_RESCALE_NORMAL);
			GL11.glPopMatrix();
		}
	}

	private void func_77026_a(final Tessellator par1Tessellator,
			final Icon par2Icon) {
		final float var3 = par2Icon.getMinU();
		final float var4 = par2Icon.getMaxU();
		final float var5 = par2Icon.getMinV();
		final float var6 = par2Icon.getMaxV();
		final float var7 = 1.0F;
		final float var8 = 0.5F;
		final float var9 = 0.25F;
		GL11.glRotatef(180.0F - renderManager.playerViewY, 0.0F, 1.0F, 0.0F);
		GL11.glRotatef(-renderManager.playerViewX, 1.0F, 0.0F, 0.0F);
		par1Tessellator.startDrawingQuads();
		par1Tessellator.setNormal(0.0F, 1.0F, 0.0F);
		par1Tessellator.addVertexWithUV(0.0F - var8, 0.0F - var9, 0.0D, var3,
				var6);
		par1Tessellator.addVertexWithUV(var7 - var8, 0.0F - var9, 0.0D, var4,
				var6);
		par1Tessellator.addVertexWithUV(var7 - var8, var7 - var9, 0.0D, var4,
				var5);
		par1Tessellator.addVertexWithUV(0.0F - var8, var7 - var9, 0.0D, var3,
				var5);
		par1Tessellator.draw();
	}
}
