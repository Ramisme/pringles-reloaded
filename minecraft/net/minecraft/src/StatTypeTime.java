package net.minecraft.src;

final class StatTypeTime implements IStatType {
	/**
	 * Formats a given stat for human consumption.
	 */
	@Override
	public String format(final int par1) {
		final double var2 = par1 / 20.0D;
		final double var4 = var2 / 60.0D;
		final double var6 = var4 / 60.0D;
		final double var8 = var6 / 24.0D;
		final double var10 = var8 / 365.0D;
		return var10 > 0.5D ? StatBase.getDecimalFormat().format(var10) + " y"
				: var8 > 0.5D ? StatBase.getDecimalFormat().format(var8) + " d"
						: var6 > 0.5D ? StatBase.getDecimalFormat()
								.format(var6) + " h" : var4 > 0.5D ? StatBase
								.getDecimalFormat().format(var4) + " m" : var2
								+ " s";
	}
}
