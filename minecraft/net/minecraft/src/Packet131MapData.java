package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet131MapData extends Packet {
	public short itemID;

	/**
	 * Contains a unique ID for the item that this packet will be populating.
	 */
	public short uniqueID;

	/**
	 * Contains a buffer of arbitrary data with which to populate an individual
	 * item in the world.
	 */
	public byte[] itemData;

	public Packet131MapData() {
		isChunkDataPacket = true;
	}

	public Packet131MapData(final short par1, final short par2,
			final byte[] par3ArrayOfByte) {
		isChunkDataPacket = true;
		itemID = par1;
		uniqueID = par2;
		itemData = par3ArrayOfByte;
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		itemID = par1DataInputStream.readShort();
		uniqueID = par1DataInputStream.readShort();
		itemData = new byte[par1DataInputStream.readUnsignedShort()];
		par1DataInputStream.readFully(itemData);
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		par1DataOutputStream.writeShort(itemID);
		par1DataOutputStream.writeShort(uniqueID);
		par1DataOutputStream.writeShort(itemData.length);
		par1DataOutputStream.write(itemData);
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleMapData(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 4 + itemData.length;
	}
}
