package net.minecraft.src;

import org.lwjgl.opengl.GL11;

public class GuiHopper extends GuiContainer {
	private final IInventory field_94081_r;
	private final IInventory field_94080_s;

	public GuiHopper(final InventoryPlayer par1InventoryPlayer,
			final IInventory par2IInventory) {
		super(new ContainerHopper(par1InventoryPlayer, par2IInventory));
		field_94081_r = par1InventoryPlayer;
		field_94080_s = par2IInventory;
		allowUserInput = false;
		ySize = 133;
	}

	/**
	 * Draw the foreground layer for the GuiContainer (everything in front of
	 * the items)
	 */
	@Override
	protected void drawGuiContainerForegroundLayer(final int par1,
			final int par2) {
		fontRenderer.drawString(
				field_94080_s.isInvNameLocalized() ? field_94080_s.getInvName()
						: StatCollector.translateToLocal(field_94080_s
								.getInvName()), 8, 6, 4210752);
		fontRenderer.drawString(
				field_94081_r.isInvNameLocalized() ? field_94081_r.getInvName()
						: StatCollector.translateToLocal(field_94081_r
								.getInvName()), 8, ySize - 96 + 2, 4210752);
	}

	/**
	 * Draw the background layer for the GuiContainer (everything behind the
	 * items)
	 */
	@Override
	protected void drawGuiContainerBackgroundLayer(final float par1,
			final int par2, final int par3) {
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		mc.renderEngine.bindTexture("/gui/hopper.png");
		final int var4 = (width - xSize) / 2;
		final int var5 = (height - ySize) / 2;
		drawTexturedModalRect(var4, var5, 0, 0, xSize, ySize);
	}
}
