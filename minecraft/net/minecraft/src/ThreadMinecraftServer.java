package net.minecraft.src;

import net.minecraft.server.MinecraftServer;

public class ThreadMinecraftServer extends Thread {
	/** Instance of MinecraftServer. */
	final MinecraftServer theServer;

	public ThreadMinecraftServer(final MinecraftServer par1MinecraftServer,
			final String par2Str) {
		super(par2Str);
		theServer = par1MinecraftServer;
	}

	@Override
	public void run() {
		theServer.run();
	}
}
