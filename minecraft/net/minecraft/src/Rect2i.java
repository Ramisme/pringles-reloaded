package net.minecraft.src;

public class Rect2i {
	private int rectX;
	private int rectY;
	private int rectWidth;
	private int rectHeight;

	public Rect2i(final int par1, final int par2, final int par3, final int par4) {
		rectX = par1;
		rectY = par2;
		rectWidth = par3;
		rectHeight = par4;
	}

	public Rect2i intersection(final Rect2i par1Rect2i) {
		final int var2 = rectX;
		final int var3 = rectY;
		final int var4 = rectX + rectWidth;
		final int var5 = rectY + rectHeight;
		final int var6 = par1Rect2i.getRectX();
		final int var7 = par1Rect2i.getRectY();
		final int var8 = var6 + par1Rect2i.getRectWidth();
		final int var9 = var7 + par1Rect2i.getRectHeight();
		rectX = Math.max(var2, var6);
		rectY = Math.max(var3, var7);
		rectWidth = Math.max(0, Math.min(var4, var8) - rectX);
		rectHeight = Math.max(0, Math.min(var5, var9) - rectY);
		return this;
	}

	public int getRectX() {
		return rectX;
	}

	public int getRectY() {
		return rectY;
	}

	public int getRectWidth() {
		return rectWidth;
	}

	public int getRectHeight() {
		return rectHeight;
	}
}
