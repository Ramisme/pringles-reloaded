package net.minecraft.src;

import java.util.Random;

public class BlockCommandBlock extends BlockContainer {
	public BlockCommandBlock(final int par1) {
		super(par1, Material.iron);
	}

	/**
	 * Returns a new instance of a block's tile entity class. Called on placing
	 * the block.
	 */
	@Override
	public TileEntity createNewTileEntity(final World par1World) {
		return new TileEntityCommandBlock();
	}

	/**
	 * Lets the block know when one of its neighbor changes. Doesn't know which
	 * neighbor changed (coordinates passed are their own) Args: x, y, z,
	 * neighbor blockID
	 */
	@Override
	public void onNeighborBlockChange(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		if (!par1World.isRemote) {
			final boolean var6 = par1World.isBlockIndirectlyGettingPowered(
					par2, par3, par4);
			final int var7 = par1World.getBlockMetadata(par2, par3, par4);
			final boolean var8 = (var7 & 1) != 0;

			if (var6 && !var8) {
				par1World.setBlockMetadataWithNotify(par2, par3, par4,
						var7 | 1, 4);
				par1World.scheduleBlockUpdate(par2, par3, par4, blockID,
						tickRate(par1World));
			} else if (!var6 && var8) {
				par1World.setBlockMetadataWithNotify(par2, par3, par4, var7
						& -2, 4);
			}
		}
	}

	/**
	 * Ticks the block if it's been scheduled
	 */
	@Override
	public void updateTick(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		final TileEntity var6 = par1World.getBlockTileEntity(par2, par3, par4);

		if (var6 != null && var6 instanceof TileEntityCommandBlock) {
			final TileEntityCommandBlock var7 = (TileEntityCommandBlock) var6;
			var7.func_96102_a(var7.executeCommandOnPowered(par1World));
			par1World.func_96440_m(par2, par3, par4, blockID);
		}
	}

	/**
	 * How many world ticks before ticking
	 */
	@Override
	public int tickRate(final World par1World) {
		return 1;
	}

	/**
	 * Called upon block activation (right click on the block.)
	 */
	@Override
	public boolean onBlockActivated(final World par1World, final int par2,
			final int par3, final int par4,
			final EntityPlayer par5EntityPlayer, final int par6,
			final float par7, final float par8, final float par9) {
		final TileEntityCommandBlock var10 = (TileEntityCommandBlock) par1World
				.getBlockTileEntity(par2, par3, par4);

		if (var10 != null) {
			par5EntityPlayer.displayGUIEditSign(var10);
		}

		return true;
	}

	/**
	 * If this returns true, then comparators facing away from this block will
	 * use the value from getComparatorInputOverride instead of the actual
	 * redstone signal strength.
	 */
	@Override
	public boolean hasComparatorInputOverride() {
		return true;
	}

	/**
	 * If hasComparatorInputOverride returns true, the return value from this is
	 * used instead of the redstone signal strength when this block inputs to a
	 * comparator.
	 */
	@Override
	public int getComparatorInputOverride(final World par1World,
			final int par2, final int par3, final int par4, final int par5) {
		final TileEntity var6 = par1World.getBlockTileEntity(par2, par3, par4);
		return var6 != null && var6 instanceof TileEntityCommandBlock ? ((TileEntityCommandBlock) var6)
				.func_96103_d() : 0;
	}

	/**
	 * Called when the block is placed in the world.
	 */
	@Override
	public void onBlockPlacedBy(final World par1World, final int par2,
			final int par3, final int par4,
			final EntityLiving par5EntityLiving, final ItemStack par6ItemStack) {
		final TileEntityCommandBlock var7 = (TileEntityCommandBlock) par1World
				.getBlockTileEntity(par2, par3, par4);

		if (par6ItemStack.hasDisplayName()) {
			var7.setCommandSenderName(par6ItemStack.getDisplayName());
		}
	}
}
