package net.minecraft.src;

import java.util.concurrent.Callable;

class CallableIntCache implements Callable {
	final CrashReport theCrashReport;

	CallableIntCache(final CrashReport par1CrashReport) {
		theCrashReport = par1CrashReport;
	}

	public String func_85083_a() {
		return IntCache.func_85144_b();
	}

	@Override
	public Object call() {
		return func_85083_a();
	}
}
