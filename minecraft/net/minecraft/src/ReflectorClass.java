package net.minecraft.src;

public class ReflectorClass {
	private String targetClassName = null;
	private boolean checked = false;
	private Class targetClass = null;

	public ReflectorClass(final String var1) {
		targetClassName = var1;
		getTargetClass();
	}

	public ReflectorClass(final Class var1) {
		targetClass = var1;
		targetClassName = var1.getName();
		checked = true;
	}

	public Class getTargetClass() {
		if (checked) {
			return targetClass;
		} else {
			checked = true;

			try {
				targetClass = Class.forName(targetClassName);
			} catch (final ClassNotFoundException var2) {
				Config.log("(Reflector) Class not present: " + targetClassName);
			} catch (final Throwable var3) {
				var3.printStackTrace();
			}

			return targetClass;
		}
	}

	public boolean exists() {
		return getTargetClass() != null;
	}

	public String getTargetClassName() {
		return targetClassName;
	}
}
