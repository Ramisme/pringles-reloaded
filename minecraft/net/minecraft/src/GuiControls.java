package net.minecraft.src;

public class GuiControls extends GuiScreen {
	/**
	 * A reference to the screen object that created this. Used for navigating
	 * between screens.
	 */
	private final GuiScreen parentScreen;

	/** The title string that is displayed in the top-center of the screen. */
	protected String screenTitle = "Controls";

	/** Reference to the GameSettings object. */
	private final GameSettings options;

	/** The ID of the button that has been pressed. */
	private int buttonId = -1;

	public GuiControls(final GuiScreen par1GuiScreen,
			final GameSettings par2GameSettings) {
		parentScreen = par1GuiScreen;
		options = par2GameSettings;
	}

	/**
	 * Gets the distance from the left border of the window to left border of
	 * the controls screen
	 */
	private int getLeftBorder() {
		return width / 2 - 155;
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui() {
		final StringTranslate var1 = StringTranslate.getInstance();
		final int var2 = getLeftBorder();

		for (int var3 = 0; var3 < options.keyBindings.length; ++var3) {
			buttonList.add(new GuiSmallButton(var3, var2 + var3 % 2 * 160,
					height / 6 + 24 * (var3 >> 1), 70, 20, options
							.getOptionDisplayString(var3)));
		}

		buttonList.add(new GuiButton(200, width / 2 - 100, height / 6 + 168,
				var1.translateKey("gui.done")));
		screenTitle = var1.translateKey("controls.title");
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	@Override
	protected void actionPerformed(final GuiButton par1GuiButton) {
		for (int var2 = 0; var2 < options.keyBindings.length; ++var2) {
			((GuiButton) buttonList.get(var2)).displayString = options
					.getOptionDisplayString(var2);
		}

		if (par1GuiButton.id == 200) {
			mc.displayGuiScreen(parentScreen);
		} else {
			buttonId = par1GuiButton.id;
			par1GuiButton.displayString = "> "
					+ options.getOptionDisplayString(par1GuiButton.id) + " <";
		}
	}

	/**
	 * Called when the mouse is clicked.
	 */
	@Override
	protected void mouseClicked(final int par1, final int par2, final int par3) {
		if (buttonId >= 0) {
			options.setKeyBinding(buttonId, -100 + par3);
			((GuiButton) buttonList.get(buttonId)).displayString = options
					.getOptionDisplayString(buttonId);
			buttonId = -1;
			KeyBinding.resetKeyBindingArrayAndHash();
		} else {
			super.mouseClicked(par1, par2, par3);
		}
	}

	/**
	 * Fired when a key is typed. This is the equivalent of
	 * KeyListener.keyTyped(KeyEvent e).
	 */
	@Override
	protected void keyTyped(final char par1, final int par2) {
		if (buttonId >= 0) {
			options.setKeyBinding(buttonId, par2);
			((GuiButton) buttonList.get(buttonId)).displayString = options
					.getOptionDisplayString(buttonId);
			buttonId = -1;
			KeyBinding.resetKeyBindingArrayAndHash();
		} else {
			super.keyTyped(par1, par2);
		}
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(final int par1, final int par2, final float par3) {
		drawDefaultBackground();
		drawCenteredString(fontRenderer, screenTitle, width / 2, 20, 16777215);
		final int var4 = getLeftBorder();
		int var5 = 0;

		while (var5 < options.keyBindings.length) {
			boolean var6 = false;
			int var7 = 0;

			while (true) {
				if (var7 < options.keyBindings.length) {
					if (var7 == var5
							|| options.keyBindings[var5].keyCode != options.keyBindings[var7].keyCode) {
						++var7;
						continue;
					}

					var6 = true;
				}

				if (buttonId == var5) {
					((GuiButton) buttonList.get(var5)).displayString = ""
							+ EnumChatFormatting.WHITE + "> "
							+ EnumChatFormatting.YELLOW + "??? "
							+ EnumChatFormatting.WHITE + "<";
				} else if (var6) {
					((GuiButton) buttonList.get(var5)).displayString = EnumChatFormatting.RED
							+ options.getOptionDisplayString(var5);
				} else {
					((GuiButton) buttonList.get(var5)).displayString = options
							.getOptionDisplayString(var5);
				}

				drawString(fontRenderer,
						options.getKeyBindingDescription(var5), var4 + var5 % 2
								* 160 + 70 + 6, height / 6 + 24 * (var5 >> 1)
								+ 7, -1);
				++var5;
				break;
			}
		}

		super.drawScreen(par1, par2, par3);
	}
}
