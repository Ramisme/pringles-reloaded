package net.minecraft.src;

import org.lwjgl.opengl.GL11;

public class RenderPlayer extends RenderLiving {
	private final ModelBiped modelBipedMain;
	private final ModelBiped modelArmorChestplate;
	private final ModelBiped modelArmor;
	private static final String[] armorFilenamePrefix = new String[] { "cloth",
			"chain", "iron", "diamond", "gold" };

	public RenderPlayer() {
		super(new ModelBiped(0.0F), 0.5F);
		modelBipedMain = (ModelBiped) mainModel;
		modelArmorChestplate = new ModelBiped(1.0F);
		modelArmor = new ModelBiped(0.5F);
	}

	protected void func_98191_a(final EntityPlayer par1EntityPlayer) {
		loadDownloadableImageTexture(par1EntityPlayer.skinUrl,
				par1EntityPlayer.getTexture());
	}

	/**
	 * Set the specified armor model as the player model. Args: player,
	 * armorSlot, partialTick
	 */
	protected int setArmorModel(final EntityPlayer par1EntityPlayer,
			final int par2, final float par3) {
		final ItemStack var4 = par1EntityPlayer.inventory
				.armorItemInSlot(3 - par2);

		if (var4 != null) {
			final Item var5 = var4.getItem();

			if (var5 instanceof ItemArmor) {
				final ItemArmor var6 = (ItemArmor) var5;
				loadTexture("/armor/"
						+ RenderPlayer.armorFilenamePrefix[var6.renderIndex]
						+ "_" + (par2 == 2 ? 2 : 1) + ".png");
				final ModelBiped var7 = par2 == 2 ? modelArmor
						: modelArmorChestplate;
				var7.bipedHead.showModel = par2 == 0;
				var7.bipedHeadwear.showModel = par2 == 0;
				var7.bipedBody.showModel = par2 == 1 || par2 == 2;
				var7.bipedRightArm.showModel = par2 == 1;
				var7.bipedLeftArm.showModel = par2 == 1;
				var7.bipedRightLeg.showModel = par2 == 2 || par2 == 3;
				var7.bipedLeftLeg.showModel = par2 == 2 || par2 == 3;
				setRenderPassModel(var7);

				if (var7 != null) {
					var7.onGround = mainModel.onGround;
				}

				if (var7 != null) {
					var7.isRiding = mainModel.isRiding;
				}

				if (var7 != null) {
					var7.isChild = mainModel.isChild;
				}

				final float var8 = 1.0F;

				if (var6.getArmorMaterial() == EnumArmorMaterial.CLOTH) {
					final int var9 = var6.getColor(var4);
					final float var10 = (var9 >> 16 & 255) / 255.0F;
					final float var11 = (var9 >> 8 & 255) / 255.0F;
					final float var12 = (var9 & 255) / 255.0F;
					GL11.glColor3f(var8 * var10, var8 * var11, var8 * var12);

					if (var4.isItemEnchanted()) {
						return 31;
					}

					return 16;
				}

				GL11.glColor3f(var8, var8, var8);

				if (var4.isItemEnchanted()) {
					return 15;
				}

				return 1;
			}
		}

		return -1;
	}

	protected void func_82439_b(final EntityPlayer par1EntityPlayer,
			final int par2, final float par3) {
		final ItemStack var4 = par1EntityPlayer.inventory
				.armorItemInSlot(3 - par2);

		if (var4 != null) {
			final Item var5 = var4.getItem();

			if (var5 instanceof ItemArmor) {
				final ItemArmor var6 = (ItemArmor) var5;
				loadTexture("/armor/"
						+ RenderPlayer.armorFilenamePrefix[var6.renderIndex]
						+ "_" + (par2 == 2 ? 2 : 1) + "_b.png");
				final float var7 = 1.0F;
				GL11.glColor3f(var7, var7, var7);
			}
		}
	}

	public void renderPlayer(final EntityPlayer par1EntityPlayer,
			final double par2, final double par4, final double par6,
			final float par8, final float par9) {
		final float var10 = 1.0F;
		GL11.glColor3f(var10, var10, var10);
		final ItemStack var11 = par1EntityPlayer.inventory.getCurrentItem();
		modelArmorChestplate.heldItemRight = modelArmor.heldItemRight = modelBipedMain.heldItemRight = var11 != null ? 1
				: 0;

		if (var11 != null && par1EntityPlayer.getItemInUseCount() > 0) {
			final EnumAction var12 = var11.getItemUseAction();

			if (var12 == EnumAction.block) {
				modelArmorChestplate.heldItemRight = modelArmor.heldItemRight = modelBipedMain.heldItemRight = 3;
			} else if (var12 == EnumAction.bow) {
				modelArmorChestplate.aimedBow = modelArmor.aimedBow = modelBipedMain.aimedBow = true;
			}
		}

		modelArmorChestplate.isSneak = modelArmor.isSneak = modelBipedMain.isSneak = par1EntityPlayer
				.isSneaking();
		double var14 = par4 - par1EntityPlayer.yOffset;

		if (par1EntityPlayer.isSneaking()
				&& !(par1EntityPlayer instanceof EntityPlayerSP)) {
			var14 -= 0.125D;
		}

		super.doRenderLiving(par1EntityPlayer, par2, var14, par6, par8, par9);
		modelArmorChestplate.aimedBow = modelArmor.aimedBow = modelBipedMain.aimedBow = false;
		modelArmorChestplate.isSneak = modelArmor.isSneak = modelBipedMain.isSneak = false;
		modelArmorChestplate.heldItemRight = modelArmor.heldItemRight = modelBipedMain.heldItemRight = 0;
	}

	/**
	 * Method for adding special render rules
	 */
	protected void renderSpecials(final EntityPlayer par1EntityPlayer,
			final float par2) {
		final float var3 = 1.0F;
		GL11.glColor3f(var3, var3, var3);
		super.renderEquippedItems(par1EntityPlayer, par2);
		super.renderArrowsStuckInEntity(par1EntityPlayer, par2);
		final ItemStack var4 = par1EntityPlayer.inventory.armorItemInSlot(3);

		if (var4 != null) {
			GL11.glPushMatrix();
			modelBipedMain.bipedHead.postRender(0.0625F);
			float var5;

			if (var4.getItem().itemID < 256) {
				if (RenderBlocks.renderItemIn3d(Block.blocksList[var4.itemID]
						.getRenderType())) {
					var5 = 0.625F;
					GL11.glTranslatef(0.0F, -0.25F, 0.0F);
					GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
					GL11.glScalef(var5, -var5, -var5);
				}

				renderManager.itemRenderer
						.renderItem(par1EntityPlayer, var4, 0);
			} else if (var4.getItem().itemID == Item.skull.itemID) {
				var5 = 1.0625F;
				GL11.glScalef(var5, -var5, -var5);
				String var6 = "";

				if (var4.hasTagCompound()
						&& var4.getTagCompound().hasKey("SkullOwner")) {
					var6 = var4.getTagCompound().getString("SkullOwner");
				}

				TileEntitySkullRenderer.skullRenderer.func_82393_a(-0.5F, 0.0F,
						-0.5F, 1, 180.0F, var4.getItemDamage(), var6);
			}

			GL11.glPopMatrix();
		}

		float var7;
		float var8;

		if (par1EntityPlayer.username.equals("deadmau5")
				&& loadDownloadableImageTexture(par1EntityPlayer.skinUrl,
						(String) null)) {
			for (int var20 = 0; var20 < 2; ++var20) {
				final float var25 = par1EntityPlayer.prevRotationYaw
						+ (par1EntityPlayer.rotationYaw - par1EntityPlayer.prevRotationYaw)
						* par2
						- (par1EntityPlayer.prevRenderYawOffset + (par1EntityPlayer.renderYawOffset - par1EntityPlayer.prevRenderYawOffset)
								* par2);
				var7 = par1EntityPlayer.prevRotationPitch
						+ (par1EntityPlayer.rotationPitch - par1EntityPlayer.prevRotationPitch)
						* par2;
				GL11.glPushMatrix();
				GL11.glRotatef(var25, 0.0F, 1.0F, 0.0F);
				GL11.glRotatef(var7, 1.0F, 0.0F, 0.0F);
				GL11.glTranslatef(0.375F * (var20 * 2 - 1), 0.0F, 0.0F);
				GL11.glTranslatef(0.0F, -0.375F, 0.0F);
				GL11.glRotatef(-var7, 1.0F, 0.0F, 0.0F);
				GL11.glRotatef(-var25, 0.0F, 1.0F, 0.0F);
				var8 = 1.3333334F;
				GL11.glScalef(var8, var8, var8);
				modelBipedMain.renderEars(0.0625F);
				GL11.glPopMatrix();
			}
		}

		float var11;

		if (loadDownloadableImageTexture(par1EntityPlayer.cloakUrl,
				(String) null)
				&& !par1EntityPlayer.isInvisible()
				&& !par1EntityPlayer.getHideCape()) {
			GL11.glPushMatrix();
			GL11.glTranslatef(0.0F, 0.0F, 0.125F);
			final double var22 = par1EntityPlayer.field_71091_bM
					+ (par1EntityPlayer.field_71094_bP - par1EntityPlayer.field_71091_bM)
					* par2
					- (par1EntityPlayer.prevPosX + (par1EntityPlayer.posX - par1EntityPlayer.prevPosX)
							* par2);
			final double var24 = par1EntityPlayer.field_71096_bN
					+ (par1EntityPlayer.field_71095_bQ - par1EntityPlayer.field_71096_bN)
					* par2
					- (par1EntityPlayer.prevPosY + (par1EntityPlayer.posY - par1EntityPlayer.prevPosY)
							* par2);
			final double var9 = par1EntityPlayer.field_71097_bO
					+ (par1EntityPlayer.field_71085_bR - par1EntityPlayer.field_71097_bO)
					* par2
					- (par1EntityPlayer.prevPosZ + (par1EntityPlayer.posZ - par1EntityPlayer.prevPosZ)
							* par2);
			var11 = par1EntityPlayer.prevRenderYawOffset
					+ (par1EntityPlayer.renderYawOffset - par1EntityPlayer.prevRenderYawOffset)
					* par2;
			final double var12 = MathHelper.sin(var11 * (float) Math.PI
					/ 180.0F);
			final double var14 = -MathHelper.cos(var11 * (float) Math.PI
					/ 180.0F);
			float var16 = (float) var24 * 10.0F;

			if (var16 < -6.0F) {
				var16 = -6.0F;
			}

			if (var16 > 32.0F) {
				var16 = 32.0F;
			}

			float var17 = (float) (var22 * var12 + var9 * var14) * 100.0F;
			final float var18 = (float) (var22 * var14 - var9 * var12) * 100.0F;

			if (var17 < 0.0F) {
				var17 = 0.0F;
			}

			final float var19 = par1EntityPlayer.prevCameraYaw
					+ (par1EntityPlayer.cameraYaw - par1EntityPlayer.prevCameraYaw)
					* par2;
			var16 += MathHelper
					.sin((par1EntityPlayer.prevDistanceWalkedModified + (par1EntityPlayer.distanceWalkedModified - par1EntityPlayer.prevDistanceWalkedModified)
							* par2) * 6.0F)
					* 32.0F * var19;

			if (par1EntityPlayer.isSneaking()) {
				var16 += 25.0F;
			}

			GL11.glRotatef(6.0F + var17 / 2.0F + var16, 1.0F, 0.0F, 0.0F);
			GL11.glRotatef(var18 / 2.0F, 0.0F, 0.0F, 1.0F);
			GL11.glRotatef(-var18 / 2.0F, 0.0F, 1.0F, 0.0F);
			GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
			modelBipedMain.renderCloak(0.0625F);
			GL11.glPopMatrix();
		}

		ItemStack var21 = par1EntityPlayer.inventory.getCurrentItem();

		if (var21 != null) {
			GL11.glPushMatrix();
			modelBipedMain.bipedRightArm.postRender(0.0625F);
			GL11.glTranslatef(-0.0625F, 0.4375F, 0.0625F);

			if (par1EntityPlayer.fishEntity != null) {
				var21 = new ItemStack(Item.stick);
			}

			EnumAction var23 = null;

			if (par1EntityPlayer.getItemInUseCount() > 0) {
				var23 = var21.getItemUseAction();
			}

			if (var21.itemID < 256
					&& RenderBlocks
							.renderItemIn3d(Block.blocksList[var21.itemID]
									.getRenderType())) {
				var7 = 0.5F;
				GL11.glTranslatef(0.0F, 0.1875F, -0.3125F);
				var7 *= 0.75F;
				GL11.glRotatef(20.0F, 1.0F, 0.0F, 0.0F);
				GL11.glRotatef(45.0F, 0.0F, 1.0F, 0.0F);
				GL11.glScalef(-var7, -var7, var7);
			} else if (var21.itemID == Item.bow.itemID) {
				var7 = 0.625F;
				GL11.glTranslatef(0.0F, 0.125F, 0.3125F);
				GL11.glRotatef(-20.0F, 0.0F, 1.0F, 0.0F);
				GL11.glScalef(var7, -var7, var7);
				GL11.glRotatef(-100.0F, 1.0F, 0.0F, 0.0F);
				GL11.glRotatef(45.0F, 0.0F, 1.0F, 0.0F);
			} else if (Item.itemsList[var21.itemID].isFull3D()) {
				var7 = 0.625F;

				if (Item.itemsList[var21.itemID]
						.shouldRotateAroundWhenRendering()) {
					GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
					GL11.glTranslatef(0.0F, -0.125F, 0.0F);
				}

				if (par1EntityPlayer.getItemInUseCount() > 0
						&& var23 == EnumAction.block) {
					GL11.glTranslatef(0.05F, 0.0F, -0.1F);
					GL11.glRotatef(-50.0F, 0.0F, 1.0F, 0.0F);
					GL11.glRotatef(-10.0F, 1.0F, 0.0F, 0.0F);
					GL11.glRotatef(-60.0F, 0.0F, 0.0F, 1.0F);
				}

				GL11.glTranslatef(0.0F, 0.1875F, 0.0F);
				GL11.glScalef(var7, -var7, var7);
				GL11.glRotatef(-100.0F, 1.0F, 0.0F, 0.0F);
				GL11.glRotatef(45.0F, 0.0F, 1.0F, 0.0F);
			} else {
				var7 = 0.375F;
				GL11.glTranslatef(0.25F, 0.1875F, -0.1875F);
				GL11.glScalef(var7, var7, var7);
				GL11.glRotatef(60.0F, 0.0F, 0.0F, 1.0F);
				GL11.glRotatef(-90.0F, 1.0F, 0.0F, 0.0F);
				GL11.glRotatef(20.0F, 0.0F, 0.0F, 1.0F);
			}

			float var10;
			int var27;
			float var28;

			if (var21.getItem().requiresMultipleRenderPasses()) {
				for (var27 = 0; var27 <= 1; ++var27) {
					final int var26 = var21.getItem().getColorFromItemStack(
							var21, var27);
					var28 = (var26 >> 16 & 255) / 255.0F;
					var10 = (var26 >> 8 & 255) / 255.0F;
					var11 = (var26 & 255) / 255.0F;
					GL11.glColor4f(var28, var10, var11, 1.0F);
					renderManager.itemRenderer.renderItem(par1EntityPlayer,
							var21, var27);
				}
			} else {
				var27 = var21.getItem().getColorFromItemStack(var21, 0);
				var8 = (var27 >> 16 & 255) / 255.0F;
				var28 = (var27 >> 8 & 255) / 255.0F;
				var10 = (var27 & 255) / 255.0F;
				GL11.glColor4f(var8, var28, var10, 1.0F);
				renderManager.itemRenderer.renderItem(par1EntityPlayer, var21,
						0);
			}

			GL11.glPopMatrix();
		}
	}

	protected void renderPlayerScale(final EntityPlayer par1EntityPlayer,
			final float par2) {
		final float var3 = 0.9375F;
		GL11.glScalef(var3, var3, var3);
	}

	protected void func_96450_a(final EntityPlayer par1EntityPlayer,
			final double par2, double par4, final double par6,
			final String par8Str, final float par9, final double par10) {
		super.func_96449_a(par1EntityPlayer, par2, par4, par6, par8Str, par9,
				200);
	}

	public void renderFirstPersonArm(final EntityPlayer par1EntityPlayer) {
		final float var2 = 1.0F;
		GL11.glColor3f(var2, var2, var2);
		modelBipedMain.onGround = 0.0F;
		modelBipedMain.setRotationAngles(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F,
				par1EntityPlayer);
		modelBipedMain.bipedRightArm.render(0.0625F);
	}

	/**
	 * Renders player with sleeping offset if sleeping
	 */
	protected void renderPlayerSleep(final EntityPlayer par1EntityPlayer,
			final double par2, final double par4, final double par6) {
		if (par1EntityPlayer.isEntityAlive()
				&& par1EntityPlayer.isPlayerSleeping()) {
			super.renderLivingAt(par1EntityPlayer, par2
					+ par1EntityPlayer.field_71079_bU, par4
					+ par1EntityPlayer.field_71082_cx, par6
					+ par1EntityPlayer.field_71089_bV);
		} else {
			super.renderLivingAt(par1EntityPlayer, par2, par4, par6);
		}
	}

	/**
	 * Rotates the player if the player is sleeping. This method is called in
	 * rotateCorpse.
	 */
	protected void rotatePlayer(final EntityPlayer par1EntityPlayer,
			final float par2, final float par3, final float par4) {
		if (par1EntityPlayer.isEntityAlive()
				&& par1EntityPlayer.isPlayerSleeping()) {
			GL11.glRotatef(par1EntityPlayer.getBedOrientationInDegrees(), 0.0F,
					1.0F, 0.0F);
			GL11.glRotatef(getDeathMaxRotation(par1EntityPlayer), 0.0F, 0.0F,
					1.0F);
			GL11.glRotatef(270.0F, 0.0F, 1.0F, 0.0F);
		} else {
			super.rotateCorpse(par1EntityPlayer, par2, par3, par4);
		}
	}

	@Override
	protected void func_96449_a(final EntityLiving par1EntityLiving,
			final double par2, final double par4, final double par6,
			final String par8Str, final float par9, final double par10) {
		func_96450_a((EntityPlayer) par1EntityLiving, par2, par4, par6,
				par8Str, par9, par10);
	}

	/**
	 * Allows the render to do any OpenGL state modifications necessary before
	 * the model is rendered. Args: entityLiving, partialTickTime
	 */
	@Override
	protected void preRenderCallback(final EntityLiving par1EntityLiving,
			final float par2) {
		renderPlayerScale((EntityPlayer) par1EntityLiving, par2);
	}

	@Override
	protected void func_82408_c(final EntityLiving par1EntityLiving,
			final int par2, final float par3) {
		func_82439_b((EntityPlayer) par1EntityLiving, par2, par3);
	}

	/**
	 * Queries whether should render the specified pass or not.
	 */
	@Override
	protected int shouldRenderPass(final EntityLiving par1EntityLiving,
			final int par2, final float par3) {
		return setArmorModel((EntityPlayer) par1EntityLiving, par2, par3);
	}

	@Override
	protected void renderEquippedItems(final EntityLiving par1EntityLiving,
			final float par2) {
		renderSpecials((EntityPlayer) par1EntityLiving, par2);
	}

	@Override
	protected void rotateCorpse(final EntityLiving par1EntityLiving,
			final float par2, final float par3, final float par4) {
		rotatePlayer((EntityPlayer) par1EntityLiving, par2, par3, par4);
	}

	/**
	 * Sets a simple glTranslate on a LivingEntity.
	 */
	@Override
	protected void renderLivingAt(final EntityLiving par1EntityLiving,
			final double par2, final double par4, final double par6) {
		renderPlayerSleep((EntityPlayer) par1EntityLiving, par2, par4, par6);
	}

	@Override
	protected void func_98190_a(final EntityLiving par1EntityLiving) {
		func_98191_a((EntityPlayer) par1EntityLiving);
	}

	@Override
	public void doRenderLiving(final EntityLiving par1EntityLiving,
			final double par2, final double par4, final double par6,
			final float par8, final float par9) {
		renderPlayer((EntityPlayer) par1EntityLiving, par2, par4, par6, par8,
				par9);
	}

	/**
	 * Actually renders the given argument. This is a synthetic bridge method,
	 * always casting down its argument and then handing it off to a worker
	 * function which does the actual work. In all probabilty, the class Render
	 * is generic (Render<T extends Entity) and this method has signature public
	 * void doRender(T entity, double d, double d1, double d2, float f, float
	 * f1). But JAD is pre 1.5 so doesn't do that.
	 */
	@Override
	public void doRender(final Entity par1Entity, final double par2,
			final double par4, final double par6, final float par8,
			final float par9) {
		renderPlayer((EntityPlayer) par1Entity, par2, par4, par6, par8, par9);
	}
}
