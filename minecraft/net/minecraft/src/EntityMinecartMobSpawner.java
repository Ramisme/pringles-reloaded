package net.minecraft.src;

public class EntityMinecartMobSpawner extends EntityMinecart {
	/** Mob spawner logic for this spawner minecart. */
	private final MobSpawnerBaseLogic mobSpawnerLogic = new EntityMinecartMobSpawnerLogic(
			this);

	public EntityMinecartMobSpawner(final World par1World) {
		super(par1World);
	}

	public EntityMinecartMobSpawner(final World par1World, final double par2,
			final double par4, final double par6) {
		super(par1World, par2, par4, par6);
	}

	@Override
	public int getMinecartType() {
		return 4;
	}

	@Override
	public Block getDefaultDisplayTile() {
		return Block.mobSpawner;
	}

	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	@Override
	protected void readEntityFromNBT(final NBTTagCompound par1NBTTagCompound) {
		super.readEntityFromNBT(par1NBTTagCompound);
		mobSpawnerLogic.readFromNBT(par1NBTTagCompound);
	}

	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	@Override
	protected void writeEntityToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeEntityToNBT(par1NBTTagCompound);
		mobSpawnerLogic.writeToNBT(par1NBTTagCompound);
	}

	@Override
	public void handleHealthUpdate(final byte par1) {
		mobSpawnerLogic.setDelayToMin(par1);
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate() {
		super.onUpdate();
		mobSpawnerLogic.updateSpawner();
	}

	public MobSpawnerBaseLogic func_98039_d() {
		return mobSpawnerLogic;
	}
}
