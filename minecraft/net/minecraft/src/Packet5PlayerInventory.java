package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet5PlayerInventory extends Packet {
	/** Entity ID of the object. */
	public int entityID;

	/** Equipment slot: 0=held, 1-4=armor slot */
	public int slot;

	/** The item in the slot format (an ItemStack) */
	private ItemStack itemSlot;

	public Packet5PlayerInventory() {
	}

	public Packet5PlayerInventory(final int par1, final int par2,
			final ItemStack par3ItemStack) {
		entityID = par1;
		slot = par2;
		itemSlot = par3ItemStack == null ? null : par3ItemStack.copy();
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		entityID = par1DataInputStream.readInt();
		slot = par1DataInputStream.readShort();
		itemSlot = Packet.readItemStack(par1DataInputStream);
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		par1DataOutputStream.writeInt(entityID);
		par1DataOutputStream.writeShort(slot);
		Packet.writeItemStack(itemSlot, par1DataOutputStream);
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handlePlayerInventory(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 8;
	}

	/**
	 * Gets the item in the slot format (an ItemStack)
	 */
	public ItemStack getItemSlot() {
		return itemSlot;
	}

	/**
	 * only false for the abstract Packet class, all real packets return true
	 */
	@Override
	public boolean isRealPacket() {
		return true;
	}

	/**
	 * eg return packet30entity.entityId == entityId; WARNING : will throw if
	 * you compare a packet to a different packet class
	 */
	@Override
	public boolean containsSameEntityIDAs(final Packet par1Packet) {
		final Packet5PlayerInventory var2 = (Packet5PlayerInventory) par1Packet;
		return var2.entityID == entityID && var2.slot == slot;
	}
}
