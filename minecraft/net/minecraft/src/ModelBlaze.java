package net.minecraft.src;

public class ModelBlaze extends ModelBase {
	/** The sticks that fly around the Blaze. */
	private final ModelRenderer[] blazeSticks = new ModelRenderer[12];
	private final ModelRenderer blazeHead;

	public ModelBlaze() {
		for (int var1 = 0; var1 < blazeSticks.length; ++var1) {
			blazeSticks[var1] = new ModelRenderer(this, 0, 16);
			blazeSticks[var1].addBox(0.0F, 0.0F, 0.0F, 2, 8, 2);
		}

		blazeHead = new ModelRenderer(this, 0, 0);
		blazeHead.addBox(-4.0F, -4.0F, -4.0F, 8, 8, 8);
	}

	public int func_78104_a() {
		return 8;
	}

	/**
	 * Sets the models various rotation angles then renders the model.
	 */
	@Override
	public void render(final Entity par1Entity, final float par2,
			final float par3, final float par4, final float par5,
			final float par6, final float par7) {
		setRotationAngles(par2, par3, par4, par5, par6, par7, par1Entity);
		blazeHead.render(par7);

		for (final ModelRenderer blazeStick : blazeSticks) {
			blazeStick.render(par7);
		}
	}

	/**
	 * Sets the model's various rotation angles. For bipeds, par1 and par2 are
	 * used for animating the movement of arms and legs, where par1 represents
	 * the time(so that arms and legs swing back and forth) and par2 represents
	 * how "far" arms and legs can swing at most.
	 */
	@Override
	public void setRotationAngles(final float par1, final float par2,
			final float par3, final float par4, final float par5,
			final float par6, final Entity par7Entity) {
		float var8 = par3 * (float) Math.PI * -0.1F;
		int var9;

		for (var9 = 0; var9 < 4; ++var9) {
			blazeSticks[var9].rotationPointY = -2.0F
					+ MathHelper.cos((var9 * 2 + par3) * 0.25F);
			blazeSticks[var9].rotationPointX = MathHelper.cos(var8) * 9.0F;
			blazeSticks[var9].rotationPointZ = MathHelper.sin(var8) * 9.0F;
			++var8;
		}

		var8 = (float) Math.PI / 4F + par3 * (float) Math.PI * 0.03F;

		for (var9 = 4; var9 < 8; ++var9) {
			blazeSticks[var9].rotationPointY = 2.0F + MathHelper
					.cos((var9 * 2 + par3) * 0.25F);
			blazeSticks[var9].rotationPointX = MathHelper.cos(var8) * 7.0F;
			blazeSticks[var9].rotationPointZ = MathHelper.sin(var8) * 7.0F;
			++var8;
		}

		var8 = 0.47123894F + par3 * (float) Math.PI * -0.05F;

		for (var9 = 8; var9 < 12; ++var9) {
			blazeSticks[var9].rotationPointY = 11.0F + MathHelper
					.cos((var9 * 1.5F + par3) * 0.5F);
			blazeSticks[var9].rotationPointX = MathHelper.cos(var8) * 5.0F;
			blazeSticks[var9].rotationPointZ = MathHelper.sin(var8) * 5.0F;
			++var8;
		}

		blazeHead.rotateAngleY = par4 / (180F / (float) Math.PI);
		blazeHead.rotateAngleX = par5 / (180F / (float) Math.PI);
	}
}
