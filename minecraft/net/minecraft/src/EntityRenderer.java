package net.minecraft.src;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.lang.reflect.Field;
import java.nio.FloatBuffer;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import net.minecraft.client.Minecraft;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLContext;
import org.lwjgl.util.glu.GLU;
import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.events.render.RenderWorldEvent;

public class EntityRenderer {
	public static boolean anaglyphEnable = false;

	/** Anaglyph field (0=R, 1=GB) */
	public static int anaglyphField;

	/** A reference to the Minecraft object. */
	private final Minecraft mc;
	private float farPlaneDistance = 0.0F;
	public ItemRenderer itemRenderer;

	/** Entity renderer update count */
	private int rendererUpdateCount;

	/** Pointed entity */
	private Entity pointedEntity = null;
	private MouseFilter mouseFilterXAxis = new MouseFilter();
	private MouseFilter mouseFilterYAxis = new MouseFilter();

	/** Mouse filter dummy 1 */
	private final MouseFilter mouseFilterDummy1 = new MouseFilter();

	/** Mouse filter dummy 2 */
	private final MouseFilter mouseFilterDummy2 = new MouseFilter();

	/** Mouse filter dummy 3 */
	private final MouseFilter mouseFilterDummy3 = new MouseFilter();

	/** Mouse filter dummy 4 */
	private final MouseFilter mouseFilterDummy4 = new MouseFilter();
	private final float thirdPersonDistance = 4.0F;

	/** Third person distance temp */
	private float thirdPersonDistanceTemp = 4.0F;
	private final float debugCamYaw = 0.0F;
	private float prevDebugCamYaw = 0.0F;
	private final float debugCamPitch = 0.0F;
	private float prevDebugCamPitch = 0.0F;

	/** Smooth cam yaw */
	private float smoothCamYaw;

	/** Smooth cam pitch */
	private float smoothCamPitch;

	/** Smooth cam filter X */
	private float smoothCamFilterX;

	/** Smooth cam filter Y */
	private float smoothCamFilterY;

	/** Smooth cam partial ticks */
	private float smoothCamPartialTicks;
	private final float debugCamFOV = 0.0F;
	private float prevDebugCamFOV = 0.0F;
	private final float camRoll = 0.0F;
	private float prevCamRoll = 0.0F;

	/**
	 * The texture id of the blocklight/skylight texture used for lighting
	 * effects
	 */
	public int lightmapTexture;

	/**
	 * Colors computed in updateLightmap() and loaded into the lightmap
	 * emptyTexture
	 */
	private final int[] lightmapColors;

	/** FOV modifier hand */
	private float fovModifierHand;

	/** FOV modifier hand prev */
	private float fovModifierHandPrev;

	/** FOV multiplier temp */
	private float fovMultiplierTemp;
	private float field_82831_U;
	private float field_82832_V;

	/** Cloud fog mode */
	private boolean cloudFog = false;
	private double cameraZoom = 1.0D;
	private final double cameraYaw = 0.0D;
	private final double cameraPitch = 0.0D;

	/** Previous frame time in milliseconds */
	private long prevFrameTime = Minecraft.getSystemTime();

	/** End time of last render (ns) */
	private long renderEndNanoTime = 0L;

	/**
	 * Is set, updateCameraAndRender() calls updateLightmap(); set by
	 * updateTorchFlicker()
	 */
	private boolean lightmapUpdateNeeded = false;

	/** Torch flicker X */
	float torchFlickerX = 0.0F;

	/** Torch flicker DX */
	float torchFlickerDX = 0.0F;

	/** Torch flicker Y */
	float torchFlickerY = 0.0F;

	/** Torch flicker DY */
	float torchFlickerDY = 0.0F;
	private final Random random = new Random();

	/** Rain sound counter */
	private int rainSoundCounter = 0;

	/** Rain X coords */
	float[] rainXCoords;

	/** Rain Y coords */
	float[] rainYCoords;
	volatile int field_78523_k = 0;
	volatile int field_78520_l = 0;

	/** Fog color buffer */
	FloatBuffer fogColorBuffer = GLAllocation.createDirectFloatBuffer(16);

	/** red component of the fog color */
	float fogColorRed;

	/** green component of the fog color */
	float fogColorGreen;

	/** blue component of the fog color */
	float fogColorBlue;

	/** Fog color 2 */
	private float fogColor2;

	/** Fog color 1 */
	private float fogColor1;

	/**
	 * Debug view direction (0=OFF, 1=Front, 2=Right, 3=Back, 4=Left,
	 * 5=TiltLeft, 6=TiltRight)
	 */
	public int debugViewDirection;
	private World updatedWorld = null;
	private boolean fullscreenModeChecked = false;
	private boolean desktopModeChecked = false;
	private String lastTexturePack = null;
	private long lastServerTime = 0L;
	private int lastServerTicks = 0;
	private int serverWaitTime = 0;
	private int serverWaitTimeCurrent = 0;
	public long[] frameTimes = new long[512];
	public long[] tickTimes = new long[512];
	public long[] chunkTimes = new long[512];
	public long[] serverTimes = new long[512];
	public int numRecordedFrameTimes = 0;
	public long prevFrameTimeNano = -1L;
	private boolean lastShowDebugInfo = false;
	private boolean showExtendedDebugInfo = false;

	public EntityRenderer(final Minecraft par1Minecraft) {
		mc = par1Minecraft;
		itemRenderer = new ItemRenderer(par1Minecraft);
		lightmapTexture = par1Minecraft.renderEngine
				.allocateAndSetupTexture(new BufferedImage(16, 16, 1));
		lightmapColors = new int[256];
	}

	/**
	 * Updates the entity renderer
	 */
	public void updateRenderer() {
		updateFovModifierHand();
		updateTorchFlicker();
		fogColor2 = fogColor1;
		thirdPersonDistanceTemp = thirdPersonDistance;
		prevDebugCamYaw = debugCamYaw;
		prevDebugCamPitch = debugCamPitch;
		prevDebugCamFOV = debugCamFOV;
		prevCamRoll = camRoll;
		float var1;
		float var2;

		if (mc.gameSettings.smoothCamera) {
			var1 = mc.gameSettings.mouseSensitivity * 0.6F + 0.2F;
			var2 = var1 * var1 * var1 * 8.0F;
			smoothCamFilterX = mouseFilterXAxis.smooth(smoothCamYaw,
					0.05F * var2);
			smoothCamFilterY = mouseFilterYAxis.smooth(smoothCamPitch,
					0.05F * var2);
			smoothCamPartialTicks = 0.0F;
			smoothCamYaw = 0.0F;
			smoothCamPitch = 0.0F;
		}

		if (mc.renderViewEntity == null) {
			mc.renderViewEntity = mc.thePlayer;
		}

		var1 = mc.theWorld.getLightBrightness(
				MathHelper.floor_double(mc.renderViewEntity.posX),
				MathHelper.floor_double(mc.renderViewEntity.posY),
				MathHelper.floor_double(mc.renderViewEntity.posZ));
		var2 = (3 - mc.gameSettings.renderDistance) / 3.0F;
		final float var3 = var1 * (1.0F - var2) + var2;
		fogColor1 += (var3 - fogColor1) * 0.1F;
		++rendererUpdateCount;
		itemRenderer.updateEquippedItem();
		addRainParticles();
		field_82832_V = field_82831_U;

		if (BossStatus.field_82825_d) {
			field_82831_U += 0.05F;

			if (field_82831_U > 1.0F) {
				field_82831_U = 1.0F;
			}

			BossStatus.field_82825_d = false;
		} else if (field_82831_U > 0.0F) {
			field_82831_U -= 0.0125F;
		}
	}

	/**
	 * Finds what block or object the mouse is over at the specified partial
	 * tick time. Args: partialTickTime
	 */
	public void getMouseOver(final float par1) {
		if (mc.renderViewEntity != null && mc.theWorld != null) {
			mc.pointedEntityLiving = null;
			double var2 = mc.playerController.getBlockReachDistance();
			mc.objectMouseOver = mc.renderViewEntity.rayTrace(var2, par1);
			double var4 = var2;
			final Vec3 var6 = mc.renderViewEntity.getPosition(par1);

			if (mc.playerController.extendedReach()) {
				var2 = 6.0D;
				var4 = 6.0D;
			} else {
				if (var2 > 3.0D) {
					var4 = 3.0D;
				}

				var2 = var4;
			}

			if (mc.objectMouseOver != null) {
				var4 = mc.objectMouseOver.hitVec.distanceTo(var6);
			}

			final Vec3 var7 = mc.renderViewEntity.getLook(par1);
			final Vec3 var8 = var6.addVector(var7.xCoord * var2, var7.yCoord
					* var2, var7.zCoord * var2);
			pointedEntity = null;
			final float var9 = 1.0F;
			final List var10 = mc.theWorld
					.getEntitiesWithinAABBExcludingEntity(
							mc.renderViewEntity,
							mc.renderViewEntity.boundingBox.addCoord(
									var7.xCoord * var2, var7.yCoord * var2,
									var7.zCoord * var2)
									.expand(var9, var9, var9));
			double var11 = var4;

			for (int var13 = 0; var13 < var10.size(); ++var13) {
				final Entity var14 = (Entity) var10.get(var13);

				if (var14.canBeCollidedWith()) {
					final float var15 = var14.getCollisionBorderSize();
					final AxisAlignedBB var16 = var14.boundingBox.expand(var15,
							var15, var15);
					final MovingObjectPosition var17 = var16
							.calculateIntercept(var6, var8);

					if (var16.isVecInside(var6)) {
						if (0.0D < var11 || var11 == 0.0D) {
							pointedEntity = var14;
							var11 = 0.0D;
						}
					} else if (var17 != null) {
						final double var18 = var6.distanceTo(var17.hitVec);

						if (var18 < var11 || var11 == 0.0D) {
							pointedEntity = var14;
							var11 = var18;
						}
					}
				}
			}

			if (pointedEntity != null
					&& (var11 < var4 || mc.objectMouseOver == null)) {
				mc.objectMouseOver = new MovingObjectPosition(pointedEntity);

				if (pointedEntity instanceof EntityLiving) {
					mc.pointedEntityLiving = (EntityLiving) pointedEntity;
				}
			}
		}
	}

	/**
	 * Update FOV modifier hand
	 */
	private void updateFovModifierHand() {
		if (mc.renderViewEntity instanceof EntityPlayerSP) {
			final EntityPlayerSP var1 = (EntityPlayerSP) mc.renderViewEntity;
			fovMultiplierTemp = var1.getFOVMultiplier();
		} else {
			fovMultiplierTemp = mc.thePlayer.getFOVMultiplier();
		}

		fovModifierHandPrev = fovModifierHand;
		fovModifierHand += (fovMultiplierTemp - fovModifierHand) * 0.5F;

		if (fovModifierHand > 1.5F) {
			fovModifierHand = 1.5F;
		}

		if (fovModifierHand < 0.1F) {
			fovModifierHand = 0.1F;
		}
	}

	/**
	 * Changes the field of view of the player depending on if they are
	 * underwater or not
	 */
	private float getFOVModifier(final float par1, final boolean par2) {
		if (debugViewDirection > 0) {
			return 90.0F;
		} else {
			final EntityLiving var3 = mc.renderViewEntity;
			float var4 = 70.0F;

			if (par2) {
				var4 += mc.gameSettings.fovSetting * 40.0F;
				var4 *= fovModifierHandPrev
						+ (fovModifierHand - fovModifierHandPrev) * par1;
			}

			boolean var5 = false;

			if (mc.currentScreen == null) {
				if (mc.gameSettings.ofKeyBindZoom.keyCode < 0) {
					var5 = Mouse
							.isButtonDown(mc.gameSettings.ofKeyBindZoom.keyCode + 100);
				} else {
					var5 = Keyboard
							.isKeyDown(mc.gameSettings.ofKeyBindZoom.keyCode);
				}
			}

			if (var5) {
				if (!Config.zoomMode) {
					Config.zoomMode = true;
					mc.gameSettings.smoothCamera = true;
				}

				if (Config.zoomMode) {
					var4 /= 4.0F;
				}
			} else if (Config.zoomMode) {
				Config.zoomMode = false;
				mc.gameSettings.smoothCamera = false;
				mouseFilterXAxis = new MouseFilter();
				mouseFilterYAxis = new MouseFilter();
			}

			if (var3.getHealth() <= 0) {
				final float var6 = var3.deathTime + par1;
				var4 /= (1.0F - 500.0F / (var6 + 500.0F)) * 2.0F + 1.0F;
			}

			final int var7 = ActiveRenderInfo.getBlockIdAtEntityViewpoint(
					mc.theWorld, var3, par1);

			if (var7 != 0
					&& Block.blocksList[var7].blockMaterial == Material.water) {
				var4 = var4 * 60.0F / 70.0F;
			}

			return var4 + prevDebugCamFOV + (debugCamFOV - prevDebugCamFOV)
					* par1;
		}
	}

	private void hurtCameraEffect(final float par1) {
		final EntityLiving var2 = mc.renderViewEntity;
		float var3 = var2.hurtTime - par1;
		float var4;

		if (var2.getHealth() <= 0) {
			var4 = var2.deathTime + par1;
			GL11.glRotatef(40.0F - 8000.0F / (var4 + 200.0F), 0.0F, 0.0F, 1.0F);
		}

		if (var3 >= 0.0F) {
			var3 /= var2.maxHurtTime;
			var3 = MathHelper.sin(var3 * var3 * var3 * var3 * (float) Math.PI);
			var4 = var2.attackedAtYaw;
			GL11.glRotatef(-var4, 0.0F, 1.0F, 0.0F);
			GL11.glRotatef(-var3 * 14.0F, 0.0F, 0.0F, 1.0F);
			GL11.glRotatef(var4, 0.0F, 1.0F, 0.0F);
		}
	}

	/**
	 * Setups all the GL settings for view bobbing. Args: partialTickTime
	 */
	private void setupViewBobbing(final float par1) {
		if (mc.renderViewEntity instanceof EntityPlayer) {
			final EntityPlayer var2 = (EntityPlayer) mc.renderViewEntity;
			final float var3 = var2.distanceWalkedModified
					- var2.prevDistanceWalkedModified;
			final float var4 = -(var2.distanceWalkedModified + var3 * par1);
			final float var5 = var2.prevCameraYaw
					+ (var2.cameraYaw - var2.prevCameraYaw) * par1;
			final float var6 = var2.prevCameraPitch
					+ (var2.cameraPitch - var2.prevCameraPitch) * par1;
			GL11.glTranslatef(MathHelper.sin(var4 * (float) Math.PI) * var5
					* 0.5F,
					-Math.abs(MathHelper.cos(var4 * (float) Math.PI) * var5),
					0.0F);
			GL11.glRotatef(
					MathHelper.sin(var4 * (float) Math.PI) * var5 * 3.0F, 0.0F,
					0.0F, 1.0F);
			GL11.glRotatef(
					Math.abs(MathHelper.cos(var4 * (float) Math.PI - 0.2F)
							* var5) * 5.0F, 1.0F, 0.0F, 0.0F);
			GL11.glRotatef(var6, 1.0F, 0.0F, 0.0F);
		}
	}

	/**
	 * sets up player's eye (or camera in third person mode)
	 */
	private void orientCamera(final float par1) {
		final EntityLiving var2 = mc.renderViewEntity;
		float var3 = var2.yOffset - 1.62F;
		double var4 = var2.prevPosX + (var2.posX - var2.prevPosX) * par1;
		double var6 = var2.prevPosY + (var2.posY - var2.prevPosY) * par1 - var3;
		double var8 = var2.prevPosZ + (var2.posZ - var2.prevPosZ) * par1;
		GL11.glRotatef(prevCamRoll + (camRoll - prevCamRoll) * par1, 0.0F,
				0.0F, 1.0F);

		if (var2.isPlayerSleeping()) {
			var3 = (float) (var3 + 1.0D);
			GL11.glTranslatef(0.0F, 0.3F, 0.0F);

			if (!mc.gameSettings.debugCamEnable) {
				final int var10 = mc.theWorld.getBlockId(
						MathHelper.floor_double(var2.posX),
						MathHelper.floor_double(var2.posY),
						MathHelper.floor_double(var2.posZ));

				if (Reflector.ForgeHooksClient_orientBedCamera.exists()) {
					Reflector.callVoid(
							Reflector.ForgeHooksClient_orientBedCamera,
							new Object[] { mc, var2 });
				} else if (var10 == Block.bed.blockID) {
					final int var11 = mc.theWorld.getBlockMetadata(
							MathHelper.floor_double(var2.posX),
							MathHelper.floor_double(var2.posY),
							MathHelper.floor_double(var2.posZ));
					final int var12 = var11 & 3;
					GL11.glRotatef(var12 * 90, 0.0F, 1.0F, 0.0F);
				}

				GL11.glRotatef(var2.prevRotationYaw
						+ (var2.rotationYaw - var2.prevRotationYaw) * par1
						+ 180.0F, 0.0F, -1.0F, 0.0F);
				GL11.glRotatef(var2.prevRotationPitch
						+ (var2.rotationPitch - var2.prevRotationPitch) * par1,
						-1.0F, 0.0F, 0.0F);
			}
		} else if (mc.gameSettings.thirdPersonView > 0) {
			double var27 = thirdPersonDistanceTemp
					+ (thirdPersonDistance - thirdPersonDistanceTemp) * par1;
			float var13;
			float var28;

			if (mc.gameSettings.debugCamEnable) {
				var13 = prevDebugCamYaw + (debugCamYaw - prevDebugCamYaw)
						* par1;
				var28 = prevDebugCamPitch + (debugCamPitch - prevDebugCamPitch)
						* par1;
				GL11.glTranslatef(0.0F, 0.0F, (float) -var27);
				GL11.glRotatef(var28, 1.0F, 0.0F, 0.0F);
				GL11.glRotatef(var13, 0.0F, 1.0F, 0.0F);
			} else {
				var13 = var2.rotationYaw;
				var28 = var2.rotationPitch;

				if (mc.gameSettings.thirdPersonView == 2) {
					var28 += 180.0F;
				}

				final double var14 = -MathHelper.sin(var13 / 180.0F
						* (float) Math.PI)
						* MathHelper.cos(var28 / 180.0F * (float) Math.PI)
						* var27;
				final double var16 = MathHelper.cos(var13 / 180.0F
						* (float) Math.PI)
						* MathHelper.cos(var28 / 180.0F * (float) Math.PI)
						* var27;
				final double var18 = -MathHelper.sin(var28 / 180.0F
						* (float) Math.PI)
						* var27;

				for (int var20 = 0; var20 < 8; ++var20) {
					float var21 = (var20 & 1) * 2 - 1;
					float var22 = (var20 >> 1 & 1) * 2 - 1;
					float var23 = (var20 >> 2 & 1) * 2 - 1;
					var21 *= 0.1F;
					var22 *= 0.1F;
					var23 *= 0.1F;
					final MovingObjectPosition var24 = mc.theWorld
							.rayTraceBlocks(
									mc.theWorld.getWorldVec3Pool()
											.getVecFromPool(var4 + var21,
													var6 + var22, var8 + var23),
									mc.theWorld.getWorldVec3Pool()
											.getVecFromPool(
													var4 - var14 + var21
															+ var23,
													var6 - var18 + var22,
													var8 - var16 + var23));

					if (var24 != null) {
						final double var25 = var24.hitVec
								.distanceTo(mc.theWorld.getWorldVec3Pool()
										.getVecFromPool(var4, var6, var8));

						if (var25 < var27) {
							var27 = var25;
						}
					}
				}

				if (mc.gameSettings.thirdPersonView == 2) {
					GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
				}

				GL11.glRotatef(var2.rotationPitch - var28, 1.0F, 0.0F, 0.0F);
				GL11.glRotatef(var2.rotationYaw - var13, 0.0F, 1.0F, 0.0F);
				GL11.glTranslatef(0.0F, 0.0F, (float) -var27);
				GL11.glRotatef(var13 - var2.rotationYaw, 0.0F, 1.0F, 0.0F);
				GL11.glRotatef(var28 - var2.rotationPitch, 1.0F, 0.0F, 0.0F);
			}
		} else {
			GL11.glTranslatef(0.0F, 0.0F, -0.1F);
		}

		if (!mc.gameSettings.debugCamEnable) {
			GL11.glRotatef(var2.prevRotationPitch
					+ (var2.rotationPitch - var2.prevRotationPitch) * par1,
					1.0F, 0.0F, 0.0F);
			GL11.glRotatef(
					var2.prevRotationYaw
							+ (var2.rotationYaw - var2.prevRotationYaw) * par1
							+ 180.0F, 0.0F, 1.0F, 0.0F);
		}

		GL11.glTranslatef(0.0F, var3, 0.0F);
		var4 = var2.prevPosX + (var2.posX - var2.prevPosX) * par1;
		var6 = var2.prevPosY + (var2.posY - var2.prevPosY) * par1 - var3;
		var8 = var2.prevPosZ + (var2.posZ - var2.prevPosZ) * par1;
		cloudFog = mc.renderGlobal.hasCloudFog(var4, var6, var8, par1);
	}

	/**
	 * sets up projection, view effects, camera position/rotation
	 */
	private void setupCameraTransform(final float par1, final int par2) {
		farPlaneDistance = 32 << 3 - mc.gameSettings.renderDistance;
		farPlaneDistance = mc.gameSettings.ofRenderDistanceFine;

		if (Config.isFogFancy()) {
			farPlaneDistance *= 0.95F;
		}

		if (Config.isFogFast()) {
			farPlaneDistance *= 0.83F;
		}

		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		final float var3 = 0.07F;

		if (mc.gameSettings.anaglyph) {
			GL11.glTranslatef(-(par2 * 2 - 1) * var3, 0.0F, 0.0F);
		}

		float var4 = farPlaneDistance * 2.0F;

		if (var4 < 128.0F) {
			var4 = 128.0F;
		}

		if (cameraZoom != 1.0D) {
			GL11.glTranslatef((float) cameraYaw, (float) -cameraPitch, 0.0F);
			GL11.glScaled(cameraZoom, cameraZoom, 1.0D);
		}

		GLU.gluPerspective(getFOVModifier(par1, true), (float) mc.displayWidth
				/ (float) mc.displayHeight, 0.05F, var4);
		float var5;

		if (mc.playerController.enableEverythingIsScrewedUpMode()) {
			var5 = 0.6666667F;
			GL11.glScalef(1.0F, var5, 1.0F);
		}

		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glLoadIdentity();

		if (mc.gameSettings.anaglyph) {
			GL11.glTranslatef((par2 * 2 - 1) * 0.1F, 0.0F, 0.0F);
		}

		hurtCameraEffect(par1);

		var5 = mc.thePlayer.prevTimeInPortal
				+ (mc.thePlayer.timeInPortal - mc.thePlayer.prevTimeInPortal)
				* par1;

		if (var5 > 0.0F) {
			byte var6 = 20;

			if (mc.thePlayer.isPotionActive(Potion.confusion)) {
				var6 = 7;
			}

			float var7 = 5.0F / (var5 * var5 + 5.0F) - var5 * 0.04F;
			var7 *= var7;
			GL11.glRotatef((rendererUpdateCount + par1) * var6, 0.0F, 1.0F,
					1.0F);
			GL11.glScalef(1.0F / var7, 1.0F, 1.0F);
			GL11.glRotatef(-(rendererUpdateCount + par1) * var6, 0.0F, 1.0F,
					1.0F);
		}

		orientCamera(par1);

		if (debugViewDirection > 0) {
			final int var8 = debugViewDirection - 1;

			if (var8 == 1) {
				GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
			}

			if (var8 == 2) {
				GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
			}

			if (var8 == 3) {
				GL11.glRotatef(-90.0F, 0.0F, 1.0F, 0.0F);
			}

			if (var8 == 4) {
				GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
			}

			if (var8 == 5) {
				GL11.glRotatef(-90.0F, 1.0F, 0.0F, 0.0F);
			}
		}
	}

	/**
	 * Render player hand
	 */
	private void renderHand(final float par1, final int par2) {
		if (debugViewDirection <= 0) {
			GL11.glMatrixMode(GL11.GL_PROJECTION);
			GL11.glLoadIdentity();
			final float var3 = 0.07F;

			if (mc.gameSettings.anaglyph) {
				GL11.glTranslatef(-(par2 * 2 - 1) * var3, 0.0F, 0.0F);
			}

			if (cameraZoom != 1.0D) {
				GL11.glTranslatef((float) cameraYaw, (float) -cameraPitch, 0.0F);
				GL11.glScaled(cameraZoom, cameraZoom, 1.0D);
			}

			GLU.gluPerspective(getFOVModifier(par1, false),
					(float) mc.displayWidth / (float) mc.displayHeight, 0.05F,
					farPlaneDistance * 2.0F);

			if (mc.playerController.enableEverythingIsScrewedUpMode()) {
				final float var4 = 0.6666667F;
				GL11.glScalef(1.0F, var4, 1.0F);
			}

			GL11.glMatrixMode(GL11.GL_MODELVIEW);
			GL11.glLoadIdentity();

			if (mc.gameSettings.anaglyph) {
				GL11.glTranslatef((par2 * 2 - 1) * 0.1F, 0.0F, 0.0F);
			}

			GL11.glPushMatrix();
			hurtCameraEffect(par1);

			if (mc.gameSettings.viewBobbing) {
				setupViewBobbing(par1);
			}

			if (mc.gameSettings.thirdPersonView == 0
					&& !mc.renderViewEntity.isPlayerSleeping()
					&& !mc.gameSettings.hideGUI
					&& !mc.playerController.enableEverythingIsScrewedUpMode()) {
				enableLightmap(par1);
				itemRenderer.renderItemInFirstPerson(par1);
				disableLightmap(par1);
			}

			GL11.glPopMatrix();

			if (mc.gameSettings.thirdPersonView == 0
					&& !mc.renderViewEntity.isPlayerSleeping()) {
				itemRenderer.renderOverlays(par1);
				hurtCameraEffect(par1);
			}

			if (mc.gameSettings.viewBobbing) {
				setupViewBobbing(par1);
			}
		}
	}

	/**
	 * Disable secondary texture unit used by lightmap
	 */
	public void disableLightmap(final double par1) {
		OpenGlHelper.setActiveTexture(OpenGlHelper.lightmapTexUnit);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		OpenGlHelper.setActiveTexture(OpenGlHelper.defaultTexUnit);
	}

	/**
	 * Enable lightmap in secondary texture unit
	 */
	public void enableLightmap(final double par1) {
		OpenGlHelper.setActiveTexture(OpenGlHelper.lightmapTexUnit);
		GL11.glMatrixMode(GL11.GL_TEXTURE);
		GL11.glLoadIdentity();
		final float var3 = 0.00390625F;
		GL11.glScalef(var3, var3, var3);
		GL11.glTranslatef(8.0F, 8.0F, 8.0F);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, lightmapTexture);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER,
				GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER,
				GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S,
				GL11.GL_CLAMP);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T,
				GL11.GL_CLAMP);
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		mc.renderEngine.resetBoundTexture();
		OpenGlHelper.setActiveTexture(OpenGlHelper.defaultTexUnit);
	}

	/**
	 * Recompute a random value that is applied to block color in
	 * updateLightmap()
	 */
	private void updateTorchFlicker() {
		torchFlickerDX = (float) (torchFlickerDX + (Math.random() - Math
				.random()) * Math.random() * Math.random());
		torchFlickerDY = (float) (torchFlickerDY + (Math.random() - Math
				.random()) * Math.random() * Math.random());
		torchFlickerDX = (float) (torchFlickerDX * 0.9D);
		torchFlickerDY = (float) (torchFlickerDY * 0.9D);
		torchFlickerX += (torchFlickerDX - torchFlickerX) * 1.0F;
		torchFlickerY += (torchFlickerDY - torchFlickerY) * 1.0F;
		lightmapUpdateNeeded = true;
	}

	private void updateLightmap(final float par1) {
		final WorldClient var2 = mc.theWorld;

		if (var2 != null) {
			if (CustomColorizer.updateLightmap(var2, this, lightmapColors,
					mc.thePlayer.isPotionActive(Potion.nightVision))) {
				mc.renderEngine.createTextureFromBytes(lightmapColors, 16, 16,
						lightmapTexture);
				return;
			}

			for (int var3 = 0; var3 < 256; ++var3) {
				final float var4 = var2.getSunBrightness(1.0F) * 0.95F + 0.05F;
				float var5 = var2.provider.lightBrightnessTable[var3 / 16]
						* var4;
				final float var6 = var2.provider.lightBrightnessTable[var3 % 16]
						* (torchFlickerX * 0.1F + 1.5F);

				if (var2.lastLightningBolt > 0) {
					var5 = var2.provider.lightBrightnessTable[var3 / 16];
				}

				final float var7 = var5
						* (var2.getSunBrightness(1.0F) * 0.65F + 0.35F);
				final float var8 = var5
						* (var2.getSunBrightness(1.0F) * 0.65F + 0.35F);
				final float var9 = var6 * ((var6 * 0.6F + 0.4F) * 0.6F + 0.4F);
				final float var10 = var6 * (var6 * var6 * 0.6F + 0.4F);
				float var11 = var7 + var6;
				float var12 = var8 + var9;
				float var13 = var5 + var10;
				var11 = var11 * 0.96F + 0.03F;
				var12 = var12 * 0.96F + 0.03F;
				var13 = var13 * 0.96F + 0.03F;
				float var14;

				if (field_82831_U > 0.0F) {
					var14 = field_82832_V + (field_82831_U - field_82832_V)
							* par1;
					var11 = var11 * (1.0F - var14) + var11 * 0.7F * var14;
					var12 = var12 * (1.0F - var14) + var12 * 0.6F * var14;
					var13 = var13 * (1.0F - var14) + var13 * 0.6F * var14;
				}

				if (var2.provider.dimensionId == 1) {
					var11 = 0.22F + var6 * 0.75F;
					var12 = 0.28F + var9 * 0.75F;
					var13 = 0.25F + var10 * 0.75F;
				}

				float var15;

				// TODO: EntityRenderer#updateLightmap
				final boolean moduleBrightness = Pringles.getInstance()
						.getFactory().getModuleManager()
						.getModule("brightness").isEnabled();

				if (mc.thePlayer.isPotionActive(Potion.nightVision)
						|| moduleBrightness) {
					var14 = moduleBrightness ? 1 : getNightVisionBrightness(
							mc.thePlayer, par1);
					var15 = 1.0F / var11;

					if (var15 > 1.0F / var12) {
						var15 = 1.0F / var12;
					}

					if (var15 > 1.0F / var13) {
						var15 = 1.0F / var13;
					}

					var11 = var11 * (1.0F - var14) + var11 * var15 * var14;
					var12 = var12 * (1.0F - var14) + var12 * var15 * var14;
					var13 = var13 * (1.0F - var14) + var13 * var15 * var14;
				}

				if (var11 > 1.0F) {
					var11 = 1.0F;
				}

				if (var12 > 1.0F) {
					var12 = 1.0F;
				}

				if (var13 > 1.0F) {
					var13 = 1.0F;
				}

				var14 = mc.gameSettings.gammaSetting;
				var15 = 1.0F - var11;
				float var16 = 1.0F - var12;
				float var17 = 1.0F - var13;
				var15 = 1.0F - var15 * var15 * var15 * var15;
				var16 = 1.0F - var16 * var16 * var16 * var16;
				var17 = 1.0F - var17 * var17 * var17 * var17;
				var11 = var11 * (1.0F - var14) + var15 * var14;
				var12 = var12 * (1.0F - var14) + var16 * var14;
				var13 = var13 * (1.0F - var14) + var17 * var14;
				var11 = var11 * 0.96F + 0.03F;
				var12 = var12 * 0.96F + 0.03F;
				var13 = var13 * 0.96F + 0.03F;

				if (var11 > 1.0F) {
					var11 = 1.0F;
				}

				if (var12 > 1.0F) {
					var12 = 1.0F;
				}

				if (var13 > 1.0F) {
					var13 = 1.0F;
				}

				if (var11 < 0.0F) {
					var11 = 0.0F;
				}

				if (var12 < 0.0F) {
					var12 = 0.0F;
				}

				if (var13 < 0.0F) {
					var13 = 0.0F;
				}

				final short var18 = 255;
				final int var19 = (int) (var11 * 255.0F);
				final int var20 = (int) (var12 * 255.0F);
				final int var21 = (int) (var13 * 255.0F);
				lightmapColors[var3] = var18 << 24 | var19 << 16 | var20 << 8
						| var21;
			}

			mc.renderEngine.createTextureFromBytes(lightmapColors, 16, 16,
					lightmapTexture);
		}
	}

	/**
	 * Gets the night vision brightness
	 */
	private float getNightVisionBrightness(final EntityPlayer par1EntityPlayer,
			final float par2) {
		final int var3 = par1EntityPlayer.getActivePotionEffect(
				Potion.nightVision).getDuration();
		return var3 > 200 ? 1.0F : 0.7F + MathHelper.sin((var3 - par2)
				* (float) Math.PI * 0.2F) * 0.3F;
	}

	/**
	 * Will update any inputs that effect the camera angle (mouse) and then
	 * render the world and GUI
	 */
	public void updateCameraAndRender(final float par1) {
		mc.mcProfiler.startSection("lightTex");
		final WorldClient var2 = mc.theWorld;
		checkDisplayMode();

		if (var2 != null && Config.getNewRelease() != null) {
			final String var3 = "HD " + Config.getNewRelease();
			mc.ingameGUI.getChatGUI().printChatMessage(
					"A new \u00a7eOptiFine\u00a7f version is available: \u00a7e"
							+ var3 + "\u00a7f");
			Config.setNewRelease((String) null);
		}

		if (mc.currentScreen instanceof GuiMainMenu) {
			updateMainMenu((GuiMainMenu) mc.currentScreen);
		}

		if (updatedWorld != var2) {
			RandomMobs.worldChanged(updatedWorld, var2);
			Config.updateThreadPriorities();
			lastServerTime = 0L;
			lastServerTicks = 0;
			updatedWorld = var2;
		}

		if (lastTexturePack == null) {
			lastTexturePack = mc.texturePackList.getSelectedTexturePack()
					.getTexturePackFileName();
		}

		if (!lastTexturePack.equals(mc.texturePackList.getSelectedTexturePack()
				.getTexturePackFileName())) {
			mc.renderGlobal.loadRenderers();
			lastTexturePack = mc.texturePackList.getSelectedTexturePack()
					.getTexturePackFileName();
		}

		RenderBlocks.fancyGrass = Config.isGrassFancy()
				|| Config.isBetterGrassFancy();
		Block.leaves.setGraphicsLevel(Config.isTreesFancy());

		if (lightmapUpdateNeeded) {
			updateLightmap(par1);
		}

		mc.mcProfiler.endSection();
		final boolean var14 = Display.isActive();

		if (!var14 && mc.gameSettings.pauseOnLostFocus
				&& (!mc.gameSettings.touchscreen || !Mouse.isButtonDown(1))) {
			if (Minecraft.getSystemTime() - prevFrameTime > 500L) {
				mc.displayInGameMenu();
			}
		} else {
			prevFrameTime = Minecraft.getSystemTime();
		}

		mc.mcProfiler.startSection("mouse");

		if (mc.inGameHasFocus && var14) {
			mc.mouseHelper.mouseXYChange();
			final float var4 = mc.gameSettings.mouseSensitivity * 0.6F + 0.2F;
			final float var5 = var4 * var4 * var4 * 8.0F;
			float var6 = mc.mouseHelper.deltaX * var5;
			float var7 = mc.mouseHelper.deltaY * var5;
			byte var8 = 1;

			if (mc.gameSettings.invertMouse) {
				var8 = -1;
			}

			if (mc.gameSettings.smoothCamera) {
				smoothCamYaw += var6;
				smoothCamPitch += var7;
				final float var9 = par1 - smoothCamPartialTicks;
				smoothCamPartialTicks = par1;
				var6 = smoothCamFilterX * var9;
				var7 = smoothCamFilterY * var9;
				mc.thePlayer.setAngles(var6, var7 * var8);
			} else {
				mc.thePlayer.setAngles(var6, var7 * var8);
			}
		}

		mc.mcProfiler.endSection();

		if (!mc.skipRenderWorld) {
			EntityRenderer.anaglyphEnable = mc.gameSettings.anaglyph;
			final ScaledResolution var15 = new ScaledResolution(
					mc.gameSettings, mc.displayWidth, mc.displayHeight);
			final int var16 = var15.getScaledWidth();
			final int var17 = var15.getScaledHeight();
			final int var18 = Mouse.getX() * var16 / mc.displayWidth;
			final int var20 = var17 - Mouse.getY() * var17 / mc.displayHeight
					- 1;
			final int var19 = EntityRenderer
					.performanceToFps(mc.gameSettings.limitFramerate);

			if (mc.theWorld != null) {
				mc.mcProfiler.startSection("level");

				if (mc.gameSettings.limitFramerate == 0) {
					renderWorld(par1, 0L);
				} else {
					renderWorld(par1, renderEndNanoTime + 1000000000 / var19);
				}

				renderEndNanoTime = System.nanoTime();
				mc.mcProfiler.endStartSection("gui");

				if (!mc.gameSettings.hideGUI || mc.currentScreen != null) {
					mc.ingameGUI.renderGameOverlay(par1,
							mc.currentScreen != null, var18, var20);
				}

				mc.mcProfiler.endSection();
			} else {
				GL11.glViewport(0, 0, mc.displayWidth, mc.displayHeight);
				GL11.glMatrixMode(GL11.GL_PROJECTION);
				GL11.glLoadIdentity();
				GL11.glMatrixMode(GL11.GL_MODELVIEW);
				GL11.glLoadIdentity();
				setupOverlayRendering();
				renderEndNanoTime = System.nanoTime();
			}

			if (mc.currentScreen != null) {
				GL11.glClear(GL11.GL_DEPTH_BUFFER_BIT);

				try {
					mc.currentScreen.drawScreen(var18, var20, par1);
				} catch (final Throwable var13) {
					final CrashReport var11 = CrashReport.makeCrashReport(
							var13, "Rendering screen");
					final CrashReportCategory var12 = var11
							.makeCategory("Screen render details");
					var12.addCrashSectionCallable("Screen name",
							new CallableScreenName(this));
					var12.addCrashSectionCallable("Mouse location",
							new CallableMouseLocation(this, var18, var20));
					var12.addCrashSectionCallable("Screen size",
							new CallableScreenSize(this, var15));
					throw new ReportedException(var11);
				}

				if (mc.currentScreen != null
						&& mc.currentScreen.guiParticles != null) {
					mc.currentScreen.guiParticles.draw(par1);
				}
			}
		}

		waitForServerThread();

		if (mc.gameSettings.showDebugInfo != lastShowDebugInfo) {
			showExtendedDebugInfo = mc.gameSettings.showDebugProfilerChart;
			lastShowDebugInfo = mc.gameSettings.showDebugInfo;
		}

		if (mc.gameSettings.showDebugInfo) {
			showLagometer(mc.mcProfiler.timeTickNano,
					mc.mcProfiler.timeUpdateChunksNano);
		}

		if (mc.gameSettings.ofProfiler) {
			mc.gameSettings.showDebugProfilerChart = true;
		}
	}

	private void waitForServerThread() {
		serverWaitTimeCurrent = 0;

		if (!Config.isSmoothWorld()) {
			lastServerTime = 0L;
			lastServerTicks = 0;
		} else if (mc.getIntegratedServer() != null) {
			final IntegratedServer var1 = mc.getIntegratedServer();
			final boolean var2 = var1.getServerListeningThread().isGamePaused();

			if (var2) {
				if (mc.currentScreen instanceof GuiDownloadTerrain) {
					Config.sleep(20L);
				}

				lastServerTime = 0L;
				lastServerTicks = 0;
			} else {
				if (serverWaitTime > 0) {
					Config.sleep(serverWaitTime);
					serverWaitTimeCurrent = serverWaitTime;
				}

				final long var3 = System.nanoTime() / 1000000L;

				if (lastServerTime != 0L && lastServerTicks != 0) {
					long var5 = var3 - lastServerTime;

					if (var5 < 0L) {
						lastServerTime = var3;
						var5 = 0L;
					}

					if (var5 >= 50L) {
						lastServerTime = var3;
						final int var7 = var1.getTickCounter();
						int var8 = var7 - lastServerTicks;

						if (var8 < 0) {
							lastServerTicks = var7;
							var8 = 0;
						}

						if (var8 < 1 && serverWaitTime < 100) {
							serverWaitTime += 2;
						}

						if (var8 > 1 && serverWaitTime > 0) {
							--serverWaitTime;
						}

						lastServerTicks = var7;
					}
				} else {
					lastServerTime = var3;
					lastServerTicks = var1.getTickCounter();
				}
			}
		}
	}

	private void showLagometer(final long var1, final long var3) {
		if (mc.gameSettings.ofLagometer || showExtendedDebugInfo) {
			if (prevFrameTimeNano == -1L) {
				prevFrameTimeNano = System.nanoTime();
			}

			final long var5 = System.nanoTime();
			final int var7 = numRecordedFrameTimes & frameTimes.length - 1;
			tickTimes[var7] = var1;
			chunkTimes[var7] = var3;
			serverTimes[var7] = serverWaitTimeCurrent;
			frameTimes[var7] = var5 - prevFrameTimeNano;
			++numRecordedFrameTimes;
			prevFrameTimeNano = var5;
			GL11.glClear(GL11.GL_DEPTH_BUFFER_BIT);
			GL11.glMatrixMode(GL11.GL_PROJECTION);
			GL11.glEnable(GL11.GL_COLOR_MATERIAL);
			GL11.glLoadIdentity();
			GL11.glOrtho(0.0D, mc.displayWidth, mc.displayHeight, 0.0D,
					1000.0D, 3000.0D);
			GL11.glMatrixMode(GL11.GL_MODELVIEW);
			GL11.glLoadIdentity();
			GL11.glTranslatef(0.0F, 0.0F, -2000.0F);
			GL11.glLineWidth(1.0F);
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			final Tessellator var8 = Tessellator.instance;
			var8.startDrawing(1);

			for (int var9 = 0; var9 < frameTimes.length; ++var9) {
				final int var10 = (var9 - numRecordedFrameTimes & frameTimes.length - 1)
						* 255 / frameTimes.length;
				final long var11 = frameTimes[var9] / 200000L;
				float var13 = mc.displayHeight;
				var8.setColorOpaque_I(-16777216 + var10 * 256);
				var8.addVertex(var9 + 0.5F, var13 - var11 + 0.5F, 0.0D);
				var8.addVertex(var9 + 0.5F, var13 + 0.5F, 0.0D);
				var13 -= var11;
				final long var14 = tickTimes[var9] / 200000L;
				var8.setColorOpaque_I(-16777216 + var10 * 65536 + var10 * 256
						+ var10 * 1);
				var8.addVertex(var9 + 0.5F, var13 + 0.5F, 0.0D);
				var8.addVertex(var9 + 0.5F, var13 + var14 + 0.5F, 0.0D);
				var13 += var14;
				final long var16 = chunkTimes[var9] / 200000L;
				var8.setColorOpaque_I(-16777216 + var10 * 65536);
				var8.addVertex(var9 + 0.5F, var13 + 0.5F, 0.0D);
				var8.addVertex(var9 + 0.5F, var13 + var16 + 0.5F, 0.0D);
				var13 += var16;
				final long var18 = serverTimes[var9];

				if (var18 > 0L) {
					final long var20 = var18 * 1000000L / 200000L;
					var8.setColorOpaque_I(-16777216 + var10 * 1);
					var8.addVertex(var9 + 0.5F, var13 + 0.5F, 0.0D);
					var8.addVertex(var9 + 0.5F, var13 + var20 + 0.5F, 0.0D);
				}
			}

			var8.draw();
		}
	}

	private void updateMainMenu(final GuiMainMenu var1) {
		try {
			String var2 = null;
			final Calendar var3 = Calendar.getInstance();
			var3.setTime(new Date());
			final int var4 = var3.get(5);
			final int var5 = var3.get(2) + 1;

			if (var4 == 8 && var5 == 4) {
				var2 = "Happy birthday, OptiFine!";
			}

			if (var4 == 14 && var5 == 8) {
				var2 = "Happy birthday, sp614x!";
			}

			if (var2 == null) {
				return;
			}

			final Field[] var6 = GuiMainMenu.class.getDeclaredFields();

			for (final Field element : var6) {
				if (element.getType() == String.class) {
					element.setAccessible(true);
					element.set(var1, var2);
					break;
				}
			}
		} catch (final Throwable var8) {
			;
		}
	}

	private void checkDisplayMode() {
		try {
			DisplayMode var1;

			if (Display.isFullscreen()) {
				if (fullscreenModeChecked) {
					return;
				}

				fullscreenModeChecked = true;
				desktopModeChecked = false;
				var1 = Display.getDisplayMode();
				final Dimension var2 = Config.getFullscreenDimension();

				if (var2 == null) {
					return;
				}

				if (var1.getWidth() == var2.width
						&& var1.getHeight() == var2.height) {
					return;
				}

				final DisplayMode var3 = Config.getDisplayMode(var2);
				Display.setDisplayMode(var3);
				mc.displayWidth = Display.getDisplayMode().getWidth();
				mc.displayHeight = Display.getDisplayMode().getHeight();

				if (mc.displayWidth <= 0) {
					mc.displayWidth = 1;
				}

				if (mc.displayHeight <= 0) {
					mc.displayHeight = 1;
				}

				Display.setFullscreen(true);
				mc.gameSettings.updateVSync();
				Display.update();
				GL11.glEnable(GL11.GL_TEXTURE_2D);
			} else {
				if (desktopModeChecked) {
					return;
				}

				desktopModeChecked = true;
				fullscreenModeChecked = false;

				if (Config.getDesktopDisplayMode() == null) {
					Config.setDesktopDisplayMode(Display
							.getDesktopDisplayMode());
				}

				var1 = Display.getDisplayMode();

				if (var1.equals(Config.getDesktopDisplayMode())) {
					return;
				}

				Display.setDisplayMode(Config.getDesktopDisplayMode());

				if (mc.mcCanvas != null) {
					mc.displayWidth = mc.mcCanvas.getWidth();
					mc.displayHeight = mc.mcCanvas.getHeight();
				}

				if (mc.displayWidth <= 0) {
					mc.displayWidth = 1;
				}

				if (mc.displayHeight <= 0) {
					mc.displayHeight = 1;
				}

				Display.setFullscreen(false);
				mc.gameSettings.updateVSync();
				Display.update();
				GL11.glEnable(GL11.GL_TEXTURE_2D);
			}
		} catch (final Exception var4) {
			var4.printStackTrace();
		}
	}

	public void renderWorld(final float par1, final long par2) {
		mc.mcProfiler.startSection("lightTex");

		if (lightmapUpdateNeeded) {
			updateLightmap(par1);
		}

		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glEnable(GL11.GL_DEPTH_TEST);

		if (mc.renderViewEntity == null) {
			mc.renderViewEntity = mc.thePlayer;
		}

		mc.mcProfiler.endStartSection("pick");
		getMouseOver(par1);
		final EntityLiving var4 = mc.renderViewEntity;
		final RenderGlobal var5 = mc.renderGlobal;
		final EffectRenderer var6 = mc.effectRenderer;
		final double var7 = var4.lastTickPosX + (var4.posX - var4.lastTickPosX)
				* par1;
		final double var9 = var4.lastTickPosY + (var4.posY - var4.lastTickPosY)
				* par1;
		final double var11 = var4.lastTickPosZ
				+ (var4.posZ - var4.lastTickPosZ) * par1;
		mc.mcProfiler.endStartSection("center");

		for (int var13 = 0; var13 < 2; ++var13) {
			if (mc.gameSettings.anaglyph) {
				EntityRenderer.anaglyphField = var13;

				if (EntityRenderer.anaglyphField == 0) {
					GL11.glColorMask(false, true, true, false);
				} else {
					GL11.glColorMask(true, false, false, false);
				}
			}

			mc.mcProfiler.endStartSection("clear");
			GL11.glViewport(0, 0, mc.displayWidth, mc.displayHeight);
			updateFogColor(par1);
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
			GL11.glEnable(GL11.GL_CULL_FACE);
			mc.mcProfiler.endStartSection("camera");
			setupCameraTransform(par1, var13);
			ActiveRenderInfo.updateRenderInfo(mc.thePlayer,
					mc.gameSettings.thirdPersonView == 2);
			mc.mcProfiler.endStartSection("frustrum");
			ClippingHelperImpl.getInstance();

			if (!Config.isSkyEnabled() && !Config.isSunMoonEnabled()
					&& !Config.isStarsEnabled()) {
				GL11.glDisable(GL11.GL_BLEND);
			} else {
				setupFog(-1, par1);
				mc.mcProfiler.endStartSection("sky");
				var5.renderSky(par1);
			}

			GL11.glEnable(GL11.GL_FOG);
			setupFog(1, par1);

			if (mc.gameSettings.ambientOcclusion != 0) {
				GL11.glShadeModel(GL11.GL_SMOOTH);
			}

			mc.mcProfiler.endStartSection("culling");
			final Frustrum var14 = new Frustrum();
			var14.setPosition(var7, var9, var11);
			mc.renderGlobal.clipRenderersByFrustum(var14, par1);

			if (var13 == 0) {
				mc.mcProfiler.endStartSection("updatechunks");

				while (!mc.renderGlobal.updateRenderers(var4, false)
						&& par2 != 0L) {
					final long var15 = par2 - System.nanoTime();

					if (var15 < 0L || var15 > 1000000000L) {
						break;
					}
				}
			}

			if (var4.posY < 128.0D) {
				renderCloudsCheck(var5, par1);
			}

			mc.mcProfiler.endStartSection("prepareterrain");
			setupFog(0, par1);
			GL11.glEnable(GL11.GL_FOG);
			mc.renderEngine.bindTexture("/terrain.png");
			RenderHelper.disableStandardItemLighting();
			mc.mcProfiler.endStartSection("terrain");
			var5.sortAndRender(var4, 0, par1);
			GL11.glShadeModel(GL11.GL_FLAT);
			final boolean var16 = Reflector.ForgeHooksClient.exists();
			EntityPlayer var18;

			if (debugViewDirection == 0) {
				RenderHelper.enableStandardItemLighting();
				mc.mcProfiler.endStartSection("entities");

				if (var16) {
					Reflector.callVoid(
							Reflector.ForgeHooksClient_setRenderPass,
							new Object[] { Integer.valueOf(0) });
				}

				var5.renderEntities(var4.getPosition(par1), var14, par1);

				if (var16) {
					Reflector.callVoid(
							Reflector.ForgeHooksClient_setRenderPass,
							new Object[] { Integer.valueOf(-1) });
				}

				enableLightmap(par1);
				mc.mcProfiler.endStartSection("litParticles");
				var6.renderLitParticles(var4, par1);
				RenderHelper.disableStandardItemLighting();
				setupFog(0, par1);
				mc.mcProfiler.endStartSection("particles");
				var6.renderParticles(var4, par1);
				disableLightmap(par1);

				if (mc.objectMouseOver != null
						&& var4.isInsideOfMaterial(Material.water)
						&& var4 instanceof EntityPlayer
						&& !mc.gameSettings.hideGUI) {
					var18 = (EntityPlayer) var4;
					GL11.glDisable(GL11.GL_ALPHA_TEST);
					mc.mcProfiler.endStartSection("outline");

					if (!var16
							|| !Reflector
									.callBoolean(
											Reflector.ForgeHooksClient_onDrawBlockHighlight,
											new Object[] {
													var5,
													var18,
													mc.objectMouseOver,
													Integer.valueOf(0),
													var18.inventory
															.getCurrentItem(),
													Float.valueOf(par1) })) {
						var5.drawBlockBreaking(var18, mc.objectMouseOver, 0,
								var18.inventory.getCurrentItem(), par1);

						if (!mc.gameSettings.hideGUI) {
							var5.drawSelectionBox(var18, mc.objectMouseOver, 0,
									var18.inventory.getCurrentItem(), par1);
						}
					}
					GL11.glEnable(GL11.GL_ALPHA_TEST);
				}
			}

			GL11.glDisable(GL11.GL_BLEND);
			GL11.glEnable(GL11.GL_CULL_FACE);
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
			GL11.glDepthMask(true);
			setupFog(0, par1);
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glDisable(GL11.GL_CULL_FACE);
			mc.renderEngine.bindTexture("/terrain.png");

			if (Config.isWaterFancy()) {
				mc.mcProfiler.endStartSection("water");

				if (mc.gameSettings.ambientOcclusion != 0) {
					GL11.glShadeModel(GL11.GL_SMOOTH);
				}

				GL11.glColorMask(false, false, false, false);
				final int var17 = var5.renderAllSortedRenderers(1, par1);

				if (mc.gameSettings.anaglyph) {
					if (EntityRenderer.anaglyphField == 0) {
						GL11.glColorMask(false, true, true, true);
					} else {
						GL11.glColorMask(true, false, false, true);
					}
				} else {
					GL11.glColorMask(true, true, true, true);
				}

				if (var17 > 0) {
					var5.renderAllSortedRenderers(1, par1);
				}

				GL11.glShadeModel(GL11.GL_FLAT);
			} else {
				mc.mcProfiler.endStartSection("water");
				var5.renderAllSortedRenderers(1, par1);
			}

			if (var16) {
				RenderHelper.enableStandardItemLighting();
				mc.mcProfiler.endStartSection("entities");
				Reflector.callVoid(Reflector.ForgeHooksClient_setRenderPass,
						new Object[] { Integer.valueOf(1) });
				var5.renderEntities(var4.getPosition(par1), var14, par1);
				Reflector.callVoid(Reflector.ForgeHooksClient_setRenderPass,
						new Object[] { Integer.valueOf(-1) });
				RenderHelper.disableStandardItemLighting();
			}

			GL11.glDepthMask(true);
			GL11.glEnable(GL11.GL_CULL_FACE);
			GL11.glDisable(GL11.GL_BLEND);

			if (cameraZoom == 1.0D && var4 instanceof EntityPlayer
					&& !mc.gameSettings.hideGUI && mc.objectMouseOver != null
					&& !var4.isInsideOfMaterial(Material.water)) {
				var18 = (EntityPlayer) var4;
				GL11.glDisable(GL11.GL_ALPHA_TEST);
				mc.mcProfiler.endStartSection("outline");

				if (!var16
						|| !Reflector
								.callBoolean(
										Reflector.ForgeHooksClient_onDrawBlockHighlight,
										new Object[] {
												var5,
												var18,
												mc.objectMouseOver,
												Integer.valueOf(0),
												var18.inventory
														.getCurrentItem(),
												Float.valueOf(par1) })) {
					var5.drawBlockBreaking(var18, mc.objectMouseOver, 0,
							var18.inventory.getCurrentItem(), par1);

					if (!mc.gameSettings.hideGUI) {
						var5.drawSelectionBox(var18, mc.objectMouseOver, 0,
								var18.inventory.getCurrentItem(), par1);
					}
				}
				GL11.glEnable(GL11.GL_ALPHA_TEST);
			}

			mc.mcProfiler.endStartSection("destroyProgress");
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE);
			var5.drawBlockDamageTexture(Tessellator.instance, var4, par1);
			GL11.glDisable(GL11.GL_BLEND);
			mc.mcProfiler.endStartSection("weather");
			renderRainSnow(par1);
			GL11.glDisable(GL11.GL_FOG);

			if (var4.posY >= 128.0D) {
				renderCloudsCheck(var5, par1);
			}

			if (var16) {
				mc.mcProfiler.endStartSection("FRenderLast");
				Reflector.callVoid(
						Reflector.ForgeHooksClient_dispatchRenderLast,
						new Object[] { var5, Float.valueOf(par1) });
			}

			mc.mcProfiler.endStartSection("hand");
			// TODO: EntityRenderer#renderWorld
			final RenderWorldEvent event = new RenderWorldEvent();
			Pringles.getInstance().getFactory().getEventManager()
					.sendEvent(event);

			if (cameraZoom == 1.0D) {
				GL11.glClear(GL11.GL_DEPTH_BUFFER_BIT);
				renderHand(par1, var13);
			}

			if (!mc.gameSettings.anaglyph) {
				mc.mcProfiler.endSection();
				return;
			}
		}

		GL11.glColorMask(true, true, true, false);
		mc.mcProfiler.endSection();
	}

	/**
	 * Render clouds if enabled
	 */
	private void renderCloudsCheck(final RenderGlobal par1RenderGlobal,
			final float par2) {
		if (mc.gameSettings.shouldRenderClouds()) {
			mc.mcProfiler.endStartSection("clouds");
			GL11.glPushMatrix();
			setupFog(0, par2);
			GL11.glEnable(GL11.GL_FOG);
			par1RenderGlobal.renderClouds(par2);
			GL11.glDisable(GL11.GL_FOG);
			setupFog(1, par2);
			GL11.glPopMatrix();
		}
	}

	private void addRainParticles() {
		// TODO: EntityRenderer#addRainParticles
		final boolean moduleNoWeather = Pringles.getInstance().getFactory()
				.getModuleManager().getModule("noweather").isEnabled();
		if (moduleNoWeather) {
			return;
		}

		float var1 = mc.theWorld.getRainStrength(1.0F);

		if (!Config.isRainFancy()) {
			var1 /= 2.0F;
		}

		if (Config.isRainSplash()) {
			random.setSeed(rendererUpdateCount * 312987231L);
			final EntityLiving var2 = mc.renderViewEntity;
			final WorldClient var3 = mc.theWorld;
			final int var4 = MathHelper.floor_double(var2.posX);
			final int var5 = MathHelper.floor_double(var2.posY);
			final int var6 = MathHelper.floor_double(var2.posZ);
			final byte var7 = 10;
			double var8 = 0.0D;
			double var10 = 0.0D;
			double var12 = 0.0D;
			int var14 = 0;
			int var15 = (int) (100.0F * var1 * var1);

			if (mc.gameSettings.particleSetting == 1) {
				var15 >>= 1;
			} else if (mc.gameSettings.particleSetting == 2) {
				var15 = 0;
			}

			for (int var16 = 0; var16 < var15; ++var16) {
				final int var17 = var4 + random.nextInt(var7)
						- random.nextInt(var7);
				final int var18 = var6 + random.nextInt(var7)
						- random.nextInt(var7);
				final int var19 = var3.getPrecipitationHeight(var17, var18);
				final int var20 = var3.getBlockId(var17, var19 - 1, var18);
				final BiomeGenBase var21 = var3.getBiomeGenForCoords(var17,
						var18);

				if (var19 <= var5 + var7 && var19 >= var5 - var7
						&& var21.canSpawnLightningBolt()
						&& var21.getFloatTemperature() >= 0.2F) {
					final float var22 = random.nextFloat();
					final float var23 = random.nextFloat();

					if (var20 > 0) {
						if (Block.blocksList[var20].blockMaterial == Material.lava) {
							mc.effectRenderer.addEffect(new EntitySmokeFX(var3,
									var17 + var22, var19
											+ 0.1F
											- Block.blocksList[var20]
													.getBlockBoundsMinY(),
									var18 + var23, 0.0D, 0.0D, 0.0D));
						} else {
							++var14;

							if (random.nextInt(var14) == 0) {
								var8 = var17 + var22;
								var10 = var19
										+ 0.1F
										- Block.blocksList[var20]
												.getBlockBoundsMinY();
								var12 = var18 + var23;
							}

							final EntityRainFX var24 = new EntityRainFX(var3,
									var17 + var22,
									var19
											+ 0.1F
											- Block.blocksList[var20]
													.getBlockBoundsMinY(),
									var18 + var23);
							CustomColorizer.updateWaterFX(var24, var3);
							mc.effectRenderer.addEffect(var24);
						}
					}
				}
			}

			if (var14 > 0 && random.nextInt(3) < rainSoundCounter++) {
				rainSoundCounter = 0;

				if (var10 > var2.posY + 1.0D
						&& var3.getPrecipitationHeight(
								MathHelper.floor_double(var2.posX),
								MathHelper.floor_double(var2.posZ)) > MathHelper
								.floor_double(var2.posY)) {
					mc.theWorld.playSound(var8, var10, var12,
							"ambient.weather.rain", 0.1F, 0.5F, false);
				} else {
					mc.theWorld.playSound(var8, var10, var12,
							"ambient.weather.rain", 0.2F, 1.0F, false);
				}
			}
		}
	}

	/**
	 * Render rain and snow
	 */
	protected void renderRainSnow(final float par1) {
		// TODO: EntityRenderer#renderRainSnow
		final boolean moduleNoWeather = Pringles.getInstance().getFactory()
				.getModuleManager().getModule("noweather").isEnabled();
		if (moduleNoWeather) {
			return;
		}

		final float var2 = mc.theWorld.getRainStrength(par1);

		if (var2 > 0.0F) {
			enableLightmap(par1);

			if (rainXCoords == null) {
				rainXCoords = new float[1024];
				rainYCoords = new float[1024];

				for (int var3 = 0; var3 < 32; ++var3) {
					for (int var4 = 0; var4 < 32; ++var4) {
						final float var5 = var4 - 16;
						final float var6 = var3 - 16;
						final float var7 = MathHelper.sqrt_float(var5 * var5
								+ var6 * var6);
						rainXCoords[var3 << 5 | var4] = -var6 / var7;
						rainYCoords[var3 << 5 | var4] = var5 / var7;
					}
				}
			}

			if (Config.isRainOff()) {
				return;
			}

			final EntityLiving var41 = mc.renderViewEntity;
			final WorldClient var42 = mc.theWorld;
			final int var43 = MathHelper.floor_double(var41.posX);
			final int var44 = MathHelper.floor_double(var41.posY);
			final int var45 = MathHelper.floor_double(var41.posZ);
			final Tessellator var8 = Tessellator.instance;
			GL11.glDisable(GL11.GL_CULL_FACE);
			GL11.glNormal3f(0.0F, 1.0F, 0.0F);
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
			GL11.glAlphaFunc(GL11.GL_GREATER, 0.01F);
			mc.renderEngine.bindTexture("/environment/snow.png");
			final double var9 = var41.lastTickPosX
					+ (var41.posX - var41.lastTickPosX) * par1;
			final double var11 = var41.lastTickPosY
					+ (var41.posY - var41.lastTickPosY) * par1;
			final double var13 = var41.lastTickPosZ
					+ (var41.posZ - var41.lastTickPosZ) * par1;
			final int var15 = MathHelper.floor_double(var11);
			byte var16 = 5;

			if (Config.isRainFancy()) {
				var16 = 10;
			}

			byte var18 = -1;
			final float var19 = rendererUpdateCount + par1;

			if (Config.isRainFancy()) {
				var16 = 10;
			}

			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			for (int var20 = var45 - var16; var20 <= var45 + var16; ++var20) {
				for (int var21 = var43 - var16; var21 <= var43 + var16; ++var21) {
					final int var22 = (var20 - var45 + 16) * 32 + var21 - var43
							+ 16;
					final float var23 = rainXCoords[var22] * 0.5F;
					final float var24 = rainYCoords[var22] * 0.5F;
					final BiomeGenBase var25 = var42.getBiomeGenForCoords(
							var21, var20);

					if (var25.canSpawnLightningBolt() || var25.getEnableSnow()) {
						final int var26 = var42.getPrecipitationHeight(var21,
								var20);
						int var27 = var44 - var16;
						int var28 = var44 + var16;

						if (var27 < var26) {
							var27 = var26;
						}

						if (var28 < var26) {
							var28 = var26;
						}

						final float var29 = 1.0F;
						int var30 = var26;

						if (var26 < var15) {
							var30 = var15;
						}

						if (var27 != var28) {
							random.setSeed(var21 * var21 * 3121 + var21
									* 45238971 ^ var20 * var20 * 418711 + var20
									* 13761);
							final float var31 = var25.getFloatTemperature();
							float var34;
							double var32;

							if (var42.getWorldChunkManager()
									.getTemperatureAtHeight(var31, var26) >= 0.15F) {
								if (var18 != 0) {
									if (var18 >= 0) {
										var8.draw();
									}

									var18 = 0;
									mc.renderEngine
											.bindTexture("/environment/rain.png");
									var8.startDrawingQuads();
								}

								var34 = ((rendererUpdateCount + var21 * var21
										* 3121 + var21 * 45238971 + var20
										* var20 * 418711 + var20 * 13761 & 31) + par1)
										/ 32.0F * (3.0F + random.nextFloat());
								final double var35 = var21 + 0.5F - var41.posX;
								var32 = var20 + 0.5F - var41.posZ;
								final float var37 = MathHelper
										.sqrt_double(var35 * var35 + var32
												* var32)
										/ var16;
								final float var38 = 1.0F;
								var8.setBrightness(var42
										.getLightBrightnessForSkyBlocks(var21,
												var30, var20, 0));
								var8.setColorRGBA_F(var38, var38, var38,
										((1.0F - var37 * var37) * 0.5F + 0.5F)
												* var2);
								var8.setTranslation(-var9 * 1.0D,
										-var11 * 1.0D, -var13 * 1.0D);
								var8.addVertexWithUV(var21 - var23 + 0.5D,
										var27, var20 - var24 + 0.5D,
										0.0F * var29, var27 * var29 / 4.0F
												+ var34 * var29);
								var8.addVertexWithUV(var21 + var23 + 0.5D,
										var27, var20 + var24 + 0.5D,
										1.0F * var29, var27 * var29 / 4.0F
												+ var34 * var29);
								var8.addVertexWithUV(var21 + var23 + 0.5D,
										var28, var20 + var24 + 0.5D,
										1.0F * var29, var28 * var29 / 4.0F
												+ var34 * var29);
								var8.addVertexWithUV(var21 - var23 + 0.5D,
										var28, var20 - var24 + 0.5D,
										0.0F * var29, var28 * var29 / 4.0F
												+ var34 * var29);
								var8.setTranslation(0.0D, 0.0D, 0.0D);
							} else {
								if (var18 != 1) {
									if (var18 >= 0) {
										var8.draw();
									}

									var18 = 1;
									mc.renderEngine
											.bindTexture("/environment/snow.png");
									var8.startDrawingQuads();
								}

								var34 = ((rendererUpdateCount & 511) + par1) / 512.0F;
								final float var46 = random.nextFloat() + var19
										* 0.01F * (float) random.nextGaussian();
								final float var36 = random.nextFloat() + var19
										* (float) random.nextGaussian()
										* 0.001F;
								var32 = var21 + 0.5F - var41.posX;
								final double var47 = var20 + 0.5F - var41.posZ;
								final float var39 = MathHelper
										.sqrt_double(var32 * var32 + var47
												* var47)
										/ var16;
								final float var40 = 1.0F;
								var8.setBrightness((var42
										.getLightBrightnessForSkyBlocks(var21,
												var30, var20, 0) * 3 + 15728880) / 4);
								var8.setColorRGBA_F(var40, var40, var40,
										((1.0F - var39 * var39) * 0.3F + 0.5F)
												* var2);
								var8.setTranslation(-var9 * 1.0D,
										-var11 * 1.0D, -var13 * 1.0D);
								var8.addVertexWithUV(var21 - var23 + 0.5D,
										var27, var20 - var24 + 0.5D, 0.0F
												* var29 + var46, var27 * var29
												/ 4.0F + var34 * var29 + var36);
								var8.addVertexWithUV(var21 + var23 + 0.5D,
										var27, var20 + var24 + 0.5D, 1.0F
												* var29 + var46, var27 * var29
												/ 4.0F + var34 * var29 + var36);
								var8.addVertexWithUV(var21 + var23 + 0.5D,
										var28, var20 + var24 + 0.5D, 1.0F
												* var29 + var46, var28 * var29
												/ 4.0F + var34 * var29 + var36);
								var8.addVertexWithUV(var21 - var23 + 0.5D,
										var28, var20 - var24 + 0.5D, 0.0F
												* var29 + var46, var28 * var29
												/ 4.0F + var34 * var29 + var36);
								var8.setTranslation(0.0D, 0.0D, 0.0D);
							}
						}
					}
				}
			}

			if (var18 >= 0) {
				var8.draw();
			}

			GL11.glEnable(GL11.GL_CULL_FACE);
			GL11.glDisable(GL11.GL_BLEND);
			GL11.glAlphaFunc(GL11.GL_GREATER, 0.1F);
			disableLightmap(par1);
		}
	}

	/**
	 * Setup orthogonal projection for rendering GUI screen overlays
	 */
	public void setupOverlayRendering() {
		final ScaledResolution var1 = new ScaledResolution(mc.gameSettings,
				mc.displayWidth, mc.displayHeight);
		GL11.glClear(GL11.GL_DEPTH_BUFFER_BIT);
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glOrtho(0.0D, var1.getScaledWidth_double(),
				var1.getScaledHeight_double(), 0.0D, 1000.0D, 3000.0D);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glLoadIdentity();
		GL11.glTranslatef(0.0F, 0.0F, -2000.0F);
	}

	/**
	 * calculates fog and calls glClearColor
	 */
	private void updateFogColor(final float par1) {
		final WorldClient var2 = mc.theWorld;
		final EntityLiving var3 = mc.renderViewEntity;
		float var4 = 1.0F / (4 - mc.gameSettings.renderDistance);
		var4 = 1.0F - (float) Math.pow(var4, 0.25D);
		Vec3 var5 = var2.getSkyColor(mc.renderViewEntity, par1);
		final int var6 = var2.provider.dimensionId;

		switch (var6) {
		case 0:
			var5 = CustomColorizer.getSkyColor(var5, mc.theWorld,
					mc.renderViewEntity.posX, mc.renderViewEntity.posY + 1.0D,
					mc.renderViewEntity.posZ);
			break;

		case 1:
			var5 = CustomColorizer.getSkyColorEnd(var5);
		}

		final float var7 = (float) var5.xCoord;
		final float var8 = (float) var5.yCoord;
		final float var9 = (float) var5.zCoord;
		Vec3 var10 = var2.getFogColor(par1);

		switch (var6) {
		case -1:
			var10 = CustomColorizer.getFogColorNether(var10);
			break;

		case 0:
			var10 = CustomColorizer.getFogColor(var10, mc.theWorld,
					mc.renderViewEntity.posX, mc.renderViewEntity.posY + 1.0D,
					mc.renderViewEntity.posZ);
			break;

		case 1:
			var10 = CustomColorizer.getFogColorEnd(var10);
		}

		fogColorRed = (float) var10.xCoord;
		fogColorGreen = (float) var10.yCoord;
		fogColorBlue = (float) var10.zCoord;
		float var11;

		if (mc.gameSettings.renderDistance < 2) {
			final Vec3 var12 = MathHelper.sin(var2
					.getCelestialAngleRadians(par1)) > 0.0F ? var2
					.getWorldVec3Pool().getVecFromPool(-1.0D, 0.0D, 0.0D)
					: var2.getWorldVec3Pool().getVecFromPool(1.0D, 0.0D, 0.0D);
			var11 = (float) var3.getLook(par1).dotProduct(var12);

			if (var11 < 0.0F) {
				var11 = 0.0F;
			}

			if (var11 > 0.0F) {
				final float[] var13 = var2.provider.calcSunriseSunsetColors(
						var2.getCelestialAngle(par1), par1);

				if (var13 != null) {
					var11 *= var13[3];
					fogColorRed = fogColorRed * (1.0F - var11) + var13[0]
							* var11;
					fogColorGreen = fogColorGreen * (1.0F - var11) + var13[1]
							* var11;
					fogColorBlue = fogColorBlue * (1.0F - var11) + var13[2]
							* var11;
				}
			}
		}

		fogColorRed += (var7 - fogColorRed) * var4;
		fogColorGreen += (var8 - fogColorGreen) * var4;
		fogColorBlue += (var9 - fogColorBlue) * var4;
		final float var23 = var2.getRainStrength(par1);
		float var24;

		if (var23 > 0.0F) {
			var11 = 1.0F - var23 * 0.5F;
			var24 = 1.0F - var23 * 0.4F;
			fogColorRed *= var11;
			fogColorGreen *= var11;
			fogColorBlue *= var24;
		}

		var11 = var2.getWeightedThunderStrength(par1);

		if (var11 > 0.0F) {
			var24 = 1.0F - var11 * 0.5F;
			fogColorRed *= var24;
			fogColorGreen *= var24;
			fogColorBlue *= var24;
		}

		final int var14 = ActiveRenderInfo.getBlockIdAtEntityViewpoint(
				mc.theWorld, var3, par1);
		Vec3 var15;

		if (cloudFog) {
			var15 = var2.getCloudColour(par1);
			fogColorRed = (float) var15.xCoord;
			fogColorGreen = (float) var15.yCoord;
			fogColorBlue = (float) var15.zCoord;
		} else if (var14 != 0
				&& Block.blocksList[var14].blockMaterial == Material.water) {
			fogColorRed = 0.02F;
			fogColorGreen = 0.02F;
			fogColorBlue = 0.2F;
			var15 = CustomColorizer.getUnderwaterColor(mc.theWorld,
					mc.renderViewEntity.posX, mc.renderViewEntity.posY + 1.0D,
					mc.renderViewEntity.posZ);

			if (var15 != null) {
				fogColorRed = (float) var15.xCoord;
				fogColorGreen = (float) var15.yCoord;
				fogColorBlue = (float) var15.zCoord;
			}
		} else if (var14 != 0
				&& Block.blocksList[var14].blockMaterial == Material.lava) {
			fogColorRed = 0.6F;
			fogColorGreen = 0.1F;
			fogColorBlue = 0.0F;
		}

		final float var25 = fogColor2 + (fogColor1 - fogColor2) * par1;
		fogColorRed *= var25;
		fogColorGreen *= var25;
		fogColorBlue *= var25;
		double var16 = var2.provider.getVoidFogYFactor();

		if (!Config.isDepthFog()) {
			var16 = 1.0D;
		}

		double var18 = (var3.lastTickPosY + (var3.posY - var3.lastTickPosY)
				* par1)
				* var16;

		if (var3.isPotionActive(Potion.blindness)) {
			final int var20 = var3.getActivePotionEffect(Potion.blindness)
					.getDuration();

			if (var20 < 20) {
				var18 *= 1.0F - var20 / 20.0F;
			} else {
				var18 = 0.0D;
			}
		}

		if (var18 < 1.0D) {
			if (var18 < 0.0D) {
				var18 = 0.0D;
			}

			var18 *= var18;
			fogColorRed = (float) (fogColorRed * var18);
			fogColorGreen = (float) (fogColorGreen * var18);
			fogColorBlue = (float) (fogColorBlue * var18);
		}

		float var26;

		if (field_82831_U > 0.0F) {
			var26 = field_82832_V + (field_82831_U - field_82832_V) * par1;
			fogColorRed = fogColorRed * (1.0F - var26) + fogColorRed * 0.7F
					* var26;
			fogColorGreen = fogColorGreen * (1.0F - var26) + fogColorGreen
					* 0.6F * var26;
			fogColorBlue = fogColorBlue * (1.0F - var26) + fogColorBlue * 0.6F
					* var26;
		}

		float var21;

		// TODO: EntityRenderer#setupOverlayRendering
		final boolean moduleBrightness = Pringles.getInstance().getFactory()
				.getModuleManager().getModule("brightness").isEnabled();
		if (var3.isPotionActive(Potion.nightVision) || moduleBrightness) {
			var26 = moduleBrightness ? 1 : getNightVisionBrightness(
					mc.thePlayer, par1);
			var21 = 1.0F / fogColorRed;

			if (var21 > 1.0F / fogColorGreen) {
				var21 = 1.0F / fogColorGreen;
			}

			if (var21 > 1.0F / fogColorBlue) {
				var21 = 1.0F / fogColorBlue;
			}

			fogColorRed = fogColorRed * (1.0F - var26) + fogColorRed * var21
					* var26;
			fogColorGreen = fogColorGreen * (1.0F - var26) + fogColorGreen
					* var21 * var26;
			fogColorBlue = fogColorBlue * (1.0F - var26) + fogColorBlue * var21
					* var26;
		}

		if (mc.gameSettings.anaglyph) {
			var26 = (fogColorRed * 30.0F + fogColorGreen * 59.0F + fogColorBlue * 11.0F) / 100.0F;
			var21 = (fogColorRed * 30.0F + fogColorGreen * 70.0F) / 100.0F;
			final float var22 = (fogColorRed * 30.0F + fogColorBlue * 70.0F) / 100.0F;
			fogColorRed = var26;
			fogColorGreen = var21;
			fogColorBlue = var22;
		}

		GL11.glClearColor(fogColorRed, fogColorGreen, fogColorBlue, 0.0F);
	}

	/**
	 * Sets up the fog to be rendered. If the arg passed in is -1 the fog starts
	 * at 0 and goes to 80% of far plane distance and is used for sky rendering.
	 */
	private void setupFog(final int par1, final float par2) {
		final EntityLiving var3 = mc.renderViewEntity;
		boolean var4 = false;

		if (var3 instanceof EntityPlayer) {
			var4 = ((EntityPlayer) var3).capabilities.isCreativeMode;
		}

		if (par1 == 999) {
			GL11.glFog(GL11.GL_FOG_COLOR,
					setFogColorBuffer(0.0F, 0.0F, 0.0F, 1.0F));
			GL11.glFogi(GL11.GL_FOG_MODE, GL11.GL_LINEAR);
			GL11.glFogf(GL11.GL_FOG_START, 0.0F);
			GL11.glFogf(GL11.GL_FOG_END, 8.0F);

			if (GLContext.getCapabilities().GL_NV_fog_distance) {
				GL11.glFogi(34138, 34139);
			}

			GL11.glFogf(GL11.GL_FOG_START, 0.0F);
		} else {
			GL11.glFog(
					GL11.GL_FOG_COLOR,
					setFogColorBuffer(fogColorRed, fogColorGreen, fogColorBlue,
							1.0F));
			GL11.glNormal3f(0.0F, -1.0F, 0.0F);
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			final int var5 = ActiveRenderInfo.getBlockIdAtEntityViewpoint(
					mc.theWorld, var3, par2);
			float var6;

			if (var3.isPotionActive(Potion.blindness)) {
				var6 = 5.0F;
				final int var7 = var3.getActivePotionEffect(Potion.blindness)
						.getDuration();

				if (var7 < 20) {
					var6 = 5.0F + (farPlaneDistance - 5.0F)
							* (1.0F - var7 / 20.0F);
				}

				GL11.glFogi(GL11.GL_FOG_MODE, GL11.GL_LINEAR);

				if (par1 < 0) {
					GL11.glFogf(GL11.GL_FOG_START, 0.0F);
					GL11.glFogf(GL11.GL_FOG_END, var6 * 0.8F);
				} else {
					GL11.glFogf(GL11.GL_FOG_START, var6 * 0.25F);
					GL11.glFogf(GL11.GL_FOG_END, var6);
				}

				if (Config.isFogFancy()) {
					GL11.glFogi(34138, 34139);
				}
			} else {
				float var8;
				float var11;
				float var14;

				if (cloudFog) {
					GL11.glFogi(GL11.GL_FOG_MODE, GL11.GL_EXP);
					GL11.glFogf(GL11.GL_FOG_DENSITY, 0.1F);
					var6 = 1.0F;
					var11 = 1.0F;
					var14 = 1.0F;

					if (mc.gameSettings.anaglyph) {
						var8 = (var6 * 30.0F + var11 * 59.0F + var14 * 11.0F) / 100.0F;
					}
				} else {
					float var15;

					if (var5 > 0
							&& Block.blocksList[var5].blockMaterial == Material.water) {
						GL11.glFogi(GL11.GL_FOG_MODE, GL11.GL_EXP);
						var15 = 0.1F;

						if (var3.isPotionActive(Potion.waterBreathing)) {
							var15 = 0.05F;
						}

						if (Config.isClearWater()) {
							var15 /= 5.0F;
						}

						GL11.glFogf(GL11.GL_FOG_DENSITY, var15);
						var6 = 0.4F;
						var11 = 0.4F;
						var14 = 0.9F;

						if (mc.gameSettings.anaglyph) {
							var8 = (var6 * 30.0F + var11 * 59.0F + var14 * 11.0F) / 100.0F;
						}
					} else if (var5 > 0
							&& Block.blocksList[var5].blockMaterial == Material.lava) {
						GL11.glFogi(GL11.GL_FOG_MODE, GL11.GL_EXP);
						GL11.glFogf(GL11.GL_FOG_DENSITY, 2.0F);
						var6 = 0.4F;
						var11 = 0.3F;
						var14 = 0.3F;

						if (mc.gameSettings.anaglyph) {
							var8 = (var6 * 30.0F + var11 * 59.0F + var14 * 11.0F) / 100.0F;
						}
					} else {
						var6 = farPlaneDistance;

						if (Config.isDepthFog()
								&& mc.theWorld.provider
										.getWorldHasVoidParticles() && !var4) {
							double var12 = ((var3.getBrightnessForRender(par2) & 15728640) >> 20)
									/ 16.0D
									+ (var3.lastTickPosY
											+ (var3.posY - var3.lastTickPosY)
											* par2 + 4.0D) / 32.0D;

							if (var12 < 1.0D) {
								if (var12 < 0.0D) {
									var12 = 0.0D;
								}

								var12 *= var12;
								var8 = 100.0F * (float) var12;

								if (var8 < 5.0F) {
									var8 = 5.0F;
								}

								if (var6 > var8) {
									var6 = var8;
								}
							}
						}

						GL11.glFogi(GL11.GL_FOG_MODE, GL11.GL_LINEAR);

						if (GLContext.getCapabilities().GL_NV_fog_distance) {
							if (Config.isFogFancy()) {
								GL11.glFogi(34138, 34139);
							}

							if (Config.isFogFast()) {
								GL11.glFogi(34138, 34140);
							}
						}

						var15 = Config.getFogStart();
						float var13 = 1.0F;

						if (par1 < 0) {
							var15 = 0.0F;
							var13 = 0.8F;
						}

						if (mc.theWorld.provider.doesXZShowFog((int) var3.posX,
								(int) var3.posZ)) {
							var15 = 0.05F;
							var13 = 1.0F;
							var6 = farPlaneDistance;
						}

						GL11.glFogf(GL11.GL_FOG_START, var6 * var15);
						GL11.glFogf(GL11.GL_FOG_END, var6 * var13);
					}
				}
			}

			GL11.glEnable(GL11.GL_COLOR_MATERIAL);
			GL11.glColorMaterial(GL11.GL_FRONT, GL11.GL_AMBIENT);
		}
	}

	/**
	 * Update and return fogColorBuffer with the RGBA values passed as arguments
	 */
	private FloatBuffer setFogColorBuffer(final float par1, final float par2,
			final float par3, final float par4) {
		fogColorBuffer.clear();
		fogColorBuffer.put(par1).put(par2).put(par3).put(par4);
		fogColorBuffer.flip();
		return fogColorBuffer;
	}

	/**
	 * Converts performance value (0-2) to FPS (35-200)
	 */
	public static int performanceToFps(final int par0) {
		final Minecraft var1 = Config.getMinecraft();

		if (var1.currentScreen != null
				&& var1.currentScreen instanceof GuiMainMenu) {
			return 35;
		} else if (var1.theWorld == null) {
			return 35;
		} else {
			int var2 = Config.getGameSettings().ofLimitFramerateFine;

			if (var2 <= 0) {
				var2 = 10000;
			}

			return var2;
		}
	}

	/**
	 * Get minecraft reference from the EntityRenderer
	 */
	static Minecraft getRendererMinecraft(
			final EntityRenderer par0EntityRenderer) {
		return par0EntityRenderer.mc;
	}
}
