package net.minecraft.src;

public class RegistryDefaulted extends RegistrySimple {
	/**
	 * Default object for this registry, returned when an object is not found.
	 */
	private final Object defaultObject;

	public RegistryDefaulted(final Object par1Obj) {
		defaultObject = par1Obj;
	}

	@Override
	public Object func_82594_a(final Object par1Obj) {
		final Object var2 = super.func_82594_a(par1Obj);
		return var2 == null ? defaultObject : var2;
	}
}
