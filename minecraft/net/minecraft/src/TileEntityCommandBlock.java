package net.minecraft.src;

import net.minecraft.server.MinecraftServer;

public class TileEntityCommandBlock extends TileEntity implements
		ICommandSender {
	private int succesCount = 0;

	/** The command this block will execute when powered. */
	private String command = "";

	/** The name of command sender (usually username, but possibly "Rcon") */
	private String commandSenderName = "@";

	/**
	 * Sets the command this block will execute when powered.
	 */
	public void setCommand(final String par1Str) {
		command = par1Str;
		onInventoryChanged();
	}

	/**
	 * Return the command this command block is set to execute.
	 */
	public String getCommand() {
		return command;
	}

	/**
	 * Execute the command, called when the command block is powered.
	 */
	public int executeCommandOnPowered(final World par1World) {
		if (par1World.isRemote) {
			return 0;
		} else {
			final MinecraftServer var2 = MinecraftServer.getServer();

			if (var2 != null && var2.isCommandBlockEnabled()) {
				final ICommandManager var3 = var2.getCommandManager();
				return var3.executeCommand(this, command);
			} else {
				return 0;
			}
		}
	}

	/**
	 * Gets the name of this command sender (usually username, but possibly
	 * "Rcon")
	 */
	@Override
	public String getCommandSenderName() {
		return commandSenderName;
	}

	/**
	 * Sets the name of the command sender
	 */
	public void setCommandSenderName(final String par1Str) {
		commandSenderName = par1Str;
	}

	@Override
	public void sendChatToPlayer(final String par1Str) {
	}

	/**
	 * Returns true if the command sender is allowed to use the given command.
	 */
	@Override
	public boolean canCommandSenderUseCommand(final int par1,
			final String par2Str) {
		return par1 <= 2;
	}

	/**
	 * Translates and formats the given string key with the given arguments.
	 */
	@Override
	public String translateString(final String par1Str,
			final Object... par2ArrayOfObj) {
		return par1Str;
	}

	/**
	 * Writes a tile entity to NBT.
	 */
	@Override
	public void writeToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeToNBT(par1NBTTagCompound);
		par1NBTTagCompound.setString("Command", command);
		par1NBTTagCompound.setInteger("SuccessCount", succesCount);
		par1NBTTagCompound.setString("CustomName", commandSenderName);
	}

	/**
	 * Reads a tile entity from NBT.
	 */
	@Override
	public void readFromNBT(final NBTTagCompound par1NBTTagCompound) {
		super.readFromNBT(par1NBTTagCompound);
		command = par1NBTTagCompound.getString("Command");
		succesCount = par1NBTTagCompound.getInteger("SuccessCount");

		if (par1NBTTagCompound.hasKey("CustomName")) {
			commandSenderName = par1NBTTagCompound.getString("CustomName");
		}
	}

	/**
	 * Return the position for this command sender.
	 */
	@Override
	public ChunkCoordinates getPlayerCoordinates() {
		return new ChunkCoordinates(xCoord, yCoord, zCoord);
	}

	/**
	 * Overriden in a sign to provide the text.
	 */
	@Override
	public Packet getDescriptionPacket() {
		final NBTTagCompound var1 = new NBTTagCompound();
		writeToNBT(var1);
		return new Packet132TileEntityData(xCoord, yCoord, zCoord, 2, var1);
	}

	public int func_96103_d() {
		return succesCount;
	}

	public void func_96102_a(final int par1) {
		succesCount = par1;
	}
}
