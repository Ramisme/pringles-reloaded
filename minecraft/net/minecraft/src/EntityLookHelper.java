package net.minecraft.src;

public class EntityLookHelper {
	private final EntityLiving entity;

	/**
	 * The amount of change that is made each update for an entity facing a
	 * direction.
	 */
	private float deltaLookYaw;

	/**
	 * The amount of change that is made each update for an entity facing a
	 * direction.
	 */
	private float deltaLookPitch;

	/** Whether or not the entity is trying to look at something. */
	private boolean isLooking = false;
	private double posX;
	private double posY;
	private double posZ;

	public EntityLookHelper(final EntityLiving par1EntityLiving) {
		entity = par1EntityLiving;
	}

	/**
	 * Sets position to look at using entity
	 */
	public void setLookPositionWithEntity(final Entity par1Entity,
			final float par2, final float par3) {
		posX = par1Entity.posX;

		if (par1Entity instanceof EntityLiving) {
			posY = par1Entity.posY + par1Entity.getEyeHeight();
		} else {
			posY = (par1Entity.boundingBox.minY + par1Entity.boundingBox.maxY) / 2.0D;
		}

		posZ = par1Entity.posZ;
		deltaLookYaw = par2;
		deltaLookPitch = par3;
		isLooking = true;
	}

	/**
	 * Sets position to look at
	 */
	public void setLookPosition(final double par1, final double par3,
			final double par5, final float par7, final float par8) {
		posX = par1;
		posY = par3;
		posZ = par5;
		deltaLookYaw = par7;
		deltaLookPitch = par8;
		isLooking = true;
	}

	/**
	 * Updates look
	 */
	public void onUpdateLook() {
		entity.rotationPitch = 0.0F;

		if (isLooking) {
			isLooking = false;
			final double var1 = posX - entity.posX;
			final double var3 = posY - (entity.posY + entity.getEyeHeight());
			final double var5 = posZ - entity.posZ;
			final double var7 = MathHelper.sqrt_double(var1 * var1 + var5
					* var5);
			final float var9 = (float) (Math.atan2(var5, var1) * 180.0D / Math.PI) - 90.0F;
			final float var10 = (float) -(Math.atan2(var3, var7) * 180.0D / Math.PI);
			entity.rotationPitch = updateRotation(entity.rotationPitch, var10,
					deltaLookPitch);
			entity.rotationYawHead = updateRotation(entity.rotationYawHead,
					var9, deltaLookYaw);
		} else {
			entity.rotationYawHead = updateRotation(entity.rotationYawHead,
					entity.renderYawOffset, 10.0F);
		}

		final float var11 = MathHelper
				.wrapAngleTo180_float(entity.rotationYawHead
						- entity.renderYawOffset);

		if (!entity.getNavigator().noPath()) {
			if (var11 < -75.0F) {
				entity.rotationYawHead = entity.renderYawOffset - 75.0F;
			}

			if (var11 > 75.0F) {
				entity.rotationYawHead = entity.renderYawOffset + 75.0F;
			}
		}
	}

	private float updateRotation(final float par1, final float par2,
			final float par3) {
		float var4 = MathHelper.wrapAngleTo180_float(par2 - par1);

		if (var4 > par3) {
			var4 = par3;
		}

		if (var4 < -par3) {
			var4 = -par3;
		}

		return par1 + var4;
	}
}
