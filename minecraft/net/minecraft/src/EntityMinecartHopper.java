package net.minecraft.src;

import java.util.List;

public class EntityMinecartHopper extends EntityMinecartContainer implements
		Hopper {
	/** Whether this hopper minecart is being blocked by an activator rail. */
	private boolean isBlocked = true;
	private int transferTicker = -1;

	public EntityMinecartHopper(final World par1World) {
		super(par1World);
	}

	public EntityMinecartHopper(final World par1World, final double par2,
			final double par4, final double par6) {
		super(par1World, par2, par4, par6);
	}

	@Override
	public int getMinecartType() {
		return 5;
	}

	@Override
	public Block getDefaultDisplayTile() {
		return Block.hopperBlock;
	}

	@Override
	public int getDefaultDisplayTileOffset() {
		return 1;
	}

	/**
	 * Returns the number of slots in the inventory.
	 */
	@Override
	public int getSizeInventory() {
		return 5;
	}

	/**
	 * Called when a player interacts with a mob. e.g. gets milk from a cow,
	 * gets into the saddle on a pig.
	 */
	@Override
	public boolean interact(final EntityPlayer par1EntityPlayer) {
		if (!worldObj.isRemote) {
			par1EntityPlayer.displayGUIHopperMinecart(this);
		}

		return true;
	}

	/**
	 * Called every tick the minecart is on an activator rail.
	 */
	@Override
	public void onActivatorRailPass(final int par1, final int par2,
			final int par3, final boolean par4) {
		final boolean var5 = !par4;

		if (var5 != getBlocked()) {
			setBlocked(var5);
		}
	}

	/**
	 * Get whether this hopper minecart is being blocked by an activator rail.
	 */
	public boolean getBlocked() {
		return isBlocked;
	}

	/**
	 * Set whether this hopper minecart is being blocked by an activator rail.
	 */
	public void setBlocked(final boolean par1) {
		isBlocked = par1;
	}

	/**
	 * Returns the worldObj for this tileEntity.
	 */
	@Override
	public World getWorldObj() {
		return worldObj;
	}

	/**
	 * Gets the world X position for this hopper entity.
	 */
	@Override
	public double getXPos() {
		return posX;
	}

	/**
	 * Gets the world Y position for this hopper entity.
	 */
	@Override
	public double getYPos() {
		return posY;
	}

	/**
	 * Gets the world Z position for this hopper entity.
	 */
	@Override
	public double getZPos() {
		return posZ;
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate() {
		super.onUpdate();

		if (!worldObj.isRemote && isEntityAlive() && getBlocked()) {
			--transferTicker;

			if (!canTransfer()) {
				setTransferTicker(0);

				if (func_96112_aD()) {
					setTransferTicker(4);
					onInventoryChanged();
				}
			}
		}
	}

	public boolean func_96112_aD() {
		if (TileEntityHopper.suckItemsIntoHopper(this)) {
			return true;
		} else {
			final List var1 = worldObj.selectEntitiesWithinAABB(
					EntityItem.class, boundingBox.expand(0.25D, 0.0D, 0.25D),
					IEntitySelector.selectAnything);

			if (var1.size() > 0) {
				TileEntityHopper.func_96114_a(this, (EntityItem) var1.get(0));
			}

			return false;
		}
	}

	@Override
	public void killMinecart(final DamageSource par1DamageSource) {
		super.killMinecart(par1DamageSource);
		dropItemWithOffset(Block.hopperBlock.blockID, 1, 0.0F);
	}

	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	@Override
	protected void writeEntityToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeEntityToNBT(par1NBTTagCompound);
		par1NBTTagCompound.setInteger("TransferCooldown", transferTicker);
	}

	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	@Override
	protected void readEntityFromNBT(final NBTTagCompound par1NBTTagCompound) {
		super.readEntityFromNBT(par1NBTTagCompound);
		transferTicker = par1NBTTagCompound.getInteger("TransferCooldown");
	}

	/**
	 * Sets the transfer ticker, used to determine the delay between transfers.
	 */
	public void setTransferTicker(final int par1) {
		transferTicker = par1;
	}

	/**
	 * Returns whether the hopper cart can currently transfer an item.
	 */
	public boolean canTransfer() {
		return transferTicker > 0;
	}
}
