package net.minecraft.src;

class GuiSlotStatsGeneral extends GuiSlot {
	final GuiStats statsGui;

	public GuiSlotStatsGeneral(final GuiStats par1GuiStats) {
		super(GuiStats.getMinecraft(par1GuiStats), par1GuiStats.width,
				par1GuiStats.height, 32, par1GuiStats.height - 64, 10);
		statsGui = par1GuiStats;
		setShowSelectionBox(false);
	}

	/**
	 * Gets the size of the current slot list.
	 */
	@Override
	protected int getSize() {
		return StatList.generalStats.size();
	}

	/**
	 * the element in the slot that was clicked, boolean for wether it was
	 * double clicked or not
	 */
	@Override
	protected void elementClicked(final int par1, final boolean par2) {
	}

	/**
	 * returns true if the element passed in is currently selected
	 */
	@Override
	protected boolean isSelected(final int par1) {
		return false;
	}

	/**
	 * return the height of the content being scrolled
	 */
	@Override
	protected int getContentHeight() {
		return getSize() * 10;
	}

	@Override
	protected void drawBackground() {
		statsGui.drawDefaultBackground();
	}

	@Override
	protected void drawSlot(final int par1, final int par2, final int par3,
			final int par4, final Tessellator par5Tessellator) {
		final StatBase var6 = (StatBase) StatList.generalStats.get(par1);
		statsGui.drawString(GuiStats.getFontRenderer1(statsGui),
				StatCollector.translateToLocal(var6.getName()), par2 + 2,
				par3 + 1, par1 % 2 == 0 ? 16777215 : 9474192);
		final String var7 = var6.func_75968_a(GuiStats.getStatsFileWriter(
				statsGui).writeStat(var6));
		statsGui.drawString(GuiStats.getFontRenderer2(statsGui), var7, par2 + 2
				+ 213
				- GuiStats.getFontRenderer3(statsGui).getStringWidth(var7),
				par3 + 1, par1 % 2 == 0 ? 16777215 : 9474192);
	}
}
