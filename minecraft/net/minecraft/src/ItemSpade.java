package net.minecraft.src;

public class ItemSpade extends ItemTool {
	/** an array of the blocks this spade is effective against */
	private static Block[] blocksEffectiveAgainst = new Block[] { Block.grass,
			Block.dirt, Block.sand, Block.gravel, Block.snow, Block.blockSnow,
			Block.blockClay, Block.tilledField, Block.slowSand, Block.mycelium };

	public ItemSpade(final int par1, final EnumToolMaterial par2EnumToolMaterial) {
		super(par1, 1, par2EnumToolMaterial, ItemSpade.blocksEffectiveAgainst);
	}

	/**
	 * Returns if the item (tool) can harvest results from the block type.
	 */
	@Override
	public boolean canHarvestBlock(final Block par1Block) {
		return par1Block == Block.snow ? true : par1Block == Block.blockSnow;
	}
}
