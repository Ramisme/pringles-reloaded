package net.minecraft.src;

import java.util.Random;

public class BlockClay extends Block {
	public BlockClay(final int par1) {
		super(par1, Material.clay);
		setCreativeTab(CreativeTabs.tabBlock);
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return Item.clay.itemID;
	}

	/**
	 * Returns the quantity of items to drop on block destruction.
	 */
	@Override
	public int quantityDropped(final Random par1Random) {
		return 4;
	}
}
