package net.minecraft.src;

public class TileEntityComparator extends TileEntity {
	private int field_96101_a = 0;

	/**
	 * Writes a tile entity to NBT.
	 */
	@Override
	public void writeToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeToNBT(par1NBTTagCompound);
		par1NBTTagCompound.setInteger("OutputSignal", field_96101_a);
	}

	/**
	 * Reads a tile entity from NBT.
	 */
	@Override
	public void readFromNBT(final NBTTagCompound par1NBTTagCompound) {
		super.readFromNBT(par1NBTTagCompound);
		field_96101_a = par1NBTTagCompound.getInteger("OutputSignal");
	}

	public int func_96100_a() {
		return field_96101_a;
	}

	public void func_96099_a(final int par1) {
		field_96101_a = par1;
	}
}
