package net.minecraft.src;

import java.util.concurrent.Callable;

class CallableServerType implements Callable {
	final DedicatedServer theDedicatedServer;

	CallableServerType(final DedicatedServer par1DedicatedServer) {
		theDedicatedServer = par1DedicatedServer;
	}

	public String callServerType() {
		return "Dedicated Server (map_server.txt)";
	}

	@Override
	public Object call() {
		return callServerType();
	}
}
