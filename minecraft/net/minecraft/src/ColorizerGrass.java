package net.minecraft.src;

public class ColorizerGrass {
	/** Color buffer for grass */
	private static int[] grassBuffer = new int[65536];

	public static void setGrassBiomeColorizer(final int[] par0ArrayOfInteger) {
		ColorizerGrass.grassBuffer = par0ArrayOfInteger;
	}

	/**
	 * Gets grass color from temperature and humidity. Args: temperature,
	 * humidity
	 */
	public static int getGrassColor(final double par0, double par2) {
		par2 *= par0;
		final int var4 = (int) ((1.0D - par0) * 255.0D);
		final int var5 = (int) ((1.0D - par2) * 255.0D);
		return ColorizerGrass.grassBuffer[var5 << 8 | var4];
	}
}
