package net.minecraft.src;

import net.minecraft.client.Minecraft;

import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.events.chat.ChatSendEvent;
//import org.ramisme.pringles.events.player.PlayerDamageTakenEvent;
import org.ramisme.pringles.events.player.update.MotionUpdateEvent;
import org.ramisme.pringles.events.player.update.UpdateEvent;
import org.ramisme.pringles.modules.combat.api.PositionManager;

public class EntityClientPlayerMP extends EntityPlayerSP {
	public NetClientHandler sendQueue;
	private double oldPosX;

	/** Old Minimum Y of the bounding box */
	private double oldMinY;
	private double oldPosZ;
	private float oldRotationYaw;
	private float oldRotationPitch;

	/** should the player stop sneaking? */
	private boolean shouldStopSneaking = false;
	private boolean wasSneaking = false;
	private int field_71168_co = 0;

	/** has the client player's health been set? */
	private boolean hasSetHealth = false;

	public EntityClientPlayerMP(final Minecraft par1Minecraft,
			final World par2World, final Session par3Session,
			final NetClientHandler par4NetClientHandler) {
		super(par1Minecraft, par2World, par3Session, 0);
		sendQueue = par4NetClientHandler;
	}

	/**
	 * Called when the entity is attacked.
	 */
	@Override
	public boolean attackEntityFrom(final DamageSource par1DamageSource,
			final int par2) {
		return false;
	}

	/**
	 * Heal living entity (param: amount of half-hearts)
	 */
	@Override
	public void heal(final int par1) {
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate() {
		if (worldObj.blockExists(MathHelper.floor_double(posX), 0,
				MathHelper.floor_double(posZ))) {
			super.onUpdate();
			sendMotionUpdates();

			// TODO: EntityClientPlayerMP#sendMotionUpdates
			final UpdateEvent update = new UpdateEvent();
			Pringles.getInstance().getFactory().getEventManager()
					.sendEvent(update);
		}
	}

	/**
	 * Send updated motion and position information to the server
	 */
	public void sendMotionUpdates() {
		// TODO: EntityClientPlayerMP#sendMotionUpdates
		PositionManager.getInstance().setOnGround(onGround);
		final MotionUpdateEvent event = new MotionUpdateEvent(rotationYaw,
				rotationYawHead, rotationPitch);
		Pringles.getInstance().getFactory().getEventManager().sendEvent(event);
		if (event.isCancelled()) {
			return;
		}

		final float tempRotationYaw = event.getRotationYaw();
		final float tempRotationPitch = event.getRotationPitch();

		final boolean var1 = isSprinting();

		if (var1 != wasSneaking) {
			if (var1) {
				sendQueue.addToSendQueue(new Packet19EntityAction(this, 4));
			} else {
				sendQueue.addToSendQueue(new Packet19EntityAction(this, 5));
			}

			wasSneaking = var1;
		}

		final boolean var2 = isSneaking();

		if (var2 != shouldStopSneaking) {
			if (var2) {
				sendQueue.addToSendQueue(new Packet19EntityAction(this, 1));
			} else {
				sendQueue.addToSendQueue(new Packet19EntityAction(this, 2));
			}

			shouldStopSneaking = var2;
		}

		final double var3 = posX - oldPosX;
		final double var5 = boundingBox.minY - oldMinY;
		final double var7 = posZ - oldPosZ;
		final double var9 = tempRotationYaw - oldRotationYaw;
		final double var11 = tempRotationPitch - oldRotationPitch;
		boolean var13 = var3 * var3 + var5 * var5 + var7 * var7 > 9.0E-4D
				|| field_71168_co >= 20;
		final boolean var14 = var9 != 0.0D || var11 != 0.0D;

		if (ridingEntity != null) {
			sendQueue.addToSendQueue(new Packet13PlayerLookMove(motionX,
					-999.0D, -999.0D, motionZ, tempRotationYaw,
					tempRotationPitch, PositionManager.getInstance()
							.getOnGround()));
			var13 = false;
		} else if (var13 && var14) {
			sendQueue.addToSendQueue(new Packet13PlayerLookMove(posX,
					boundingBox.minY, posY, posZ, tempRotationYaw,
					tempRotationPitch, PositionManager.getInstance()
							.getOnGround()));
		} else if (var13) {
			sendQueue.addToSendQueue(new Packet11PlayerPosition(posX,
					boundingBox.minY, posY, posZ, PositionManager.getInstance()
							.getOnGround()));
		} else if (var14) {
			sendQueue.addToSendQueue(new Packet12PlayerLook(tempRotationYaw,
					tempRotationPitch, PositionManager.getInstance()
							.getOnGround()));
		} else {
			sendQueue.addToSendQueue(new Packet10Flying(PositionManager
					.getInstance().getOnGround()));
		}

		++field_71168_co;
		if (var13) {
			oldPosX = posX;
			oldMinY = boundingBox.minY;
			oldPosZ = posZ;
			field_71168_co = 0;
		}

		if (var14) {
			oldRotationYaw = rotationYaw;
			oldRotationPitch = rotationPitch;
		}
	}

	/**
	 * Called when player presses the drop item key
	 */
	@Override
	public EntityItem dropOneItem(final boolean par1) {
		final int var2 = par1 ? 3 : 4;
		sendQueue.addToSendQueue(new Packet14BlockDig(var2, 0, 0, 0, 0));
		return null;
	}

	/**
	 * Joins the passed in entity item with the world. Args: entityItem
	 */
	@Override
	protected void joinEntityItemWithWorld(final EntityItem par1EntityItem) {
	}

	/**
	 * Sends a chat message from the player. Args: chatMessage
	 */
	public void sendChatMessage(final String par1Str) {
		// TODO: EntityClientPlayerMP#sendChatMessage
		final ChatSendEvent event = new ChatSendEvent(par1Str);
		Pringles.getInstance().getFactory().getEventManager().sendEvent(event);
		if (event.isCancelled()) {
			return;
		}

		sendQueue.addToSendQueue(new Packet3Chat(event.getMessage()));
	}

	/**
	 * Swings the item the player is holding.
	 */
	@Override
	public void swingItem() {
		super.swingItem();
		sendQueue.addToSendQueue(new Packet18Animation(this, 1));
	}

	@Override
	public void respawnPlayer() {
		sendQueue.addToSendQueue(new Packet205ClientCommand(1));
	}

	/**
	 * Deals damage to the entity. If its a EntityPlayer then will take damage
	 * from the armor first and then health second with the reduced value. Args:
	 * damageAmount
	 */
	@Override
	protected void damageEntity(final DamageSource par1DamageSource, final int par2) {
		if (!isEntityInvulnerable()) {	
			setEntityHealth(getHealth() - par2);
			//TODO EntityClientPlayerMP#damageEntity
			/*final PlayerDamageTakenEvent event = 
					new PlayerDamageTakenEvent(
							(EntityLiving)par1DamageSource.getEntity(),
							par2);
			Pringles.getInstance().getFactory().getEventManager().sendEvent(
					event);*/
		}
	}

	/**
	 * sets current screen to null (used on escape buttons of GUIs)
	 */
	@Override
	public void closeScreen() {
		sendQueue.addToSendQueue(new Packet101CloseWindow(
				openContainer.windowId));
		func_92015_f();
	}

	public void func_92015_f() {
		inventory.setItemStack((ItemStack) null);
		super.closeScreen();
	}

	/**
	 * Updates health locally.
	 */
	@Override
	public void setHealth(final int par1) {
		if (hasSetHealth) {
			super.setHealth(par1);
		} else {
			setEntityHealth(par1);
			hasSetHealth = true;
		}
	}

	/**
	 * Adds a value to a statistic field.
	 */
	@Override
	public void addStat(final StatBase par1StatBase, final int par2) {
		if (par1StatBase != null) {
			if (par1StatBase.isIndependent) {
				super.addStat(par1StatBase, par2);
			}
		}
	}

	/**
	 * Used by NetClientHandler.handleStatistic
	 */
	public void incrementStat(final StatBase par1StatBase, final int par2) {
		if (par1StatBase != null) {
			if (!par1StatBase.isIndependent) {
				super.addStat(par1StatBase, par2);
			}
		}
	}

	/**
	 * Sends the player's abilities to the server (if there is one).
	 */
	@Override
	public void sendPlayerAbilities() {
		sendQueue.addToSendQueue(new Packet202PlayerAbilities(capabilities));
	}

	@Override
	public boolean func_71066_bF() {
		return true;
	}
}
