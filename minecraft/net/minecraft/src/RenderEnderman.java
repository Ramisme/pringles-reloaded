package net.minecraft.src;

import java.util.Random;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

public class RenderEnderman extends RenderLiving {
	/** The model of the enderman */
	private final ModelEnderman endermanModel;
	private final Random rnd = new Random();

	public RenderEnderman() {
		super(new ModelEnderman(), 0.5F);
		endermanModel = (ModelEnderman) super.mainModel;
		setRenderPassModel(endermanModel);
	}

	/**
	 * Renders the enderman
	 */
	public void renderEnderman(final EntityEnderman par1EntityEnderman,
			double par2, final double par4, double par6, final float par8,
			final float par9) {
		endermanModel.isCarrying = par1EntityEnderman.getCarried() > 0;
		endermanModel.isAttacking = par1EntityEnderman.isScreaming();

		if (par1EntityEnderman.isScreaming()) {
			final double var10 = 0.02D;
			par2 += rnd.nextGaussian() * var10;
			par6 += rnd.nextGaussian() * var10;
		}

		super.doRenderLiving(par1EntityEnderman, par2, par4, par6, par8, par9);
	}

	/**
	 * Render the block an enderman is carrying
	 */
	protected void renderCarrying(final EntityEnderman par1EntityEnderman,
			final float par2) {
		super.renderEquippedItems(par1EntityEnderman, par2);

		if (par1EntityEnderman.getCarried() > 0) {
			GL11.glEnable(GL12.GL_RESCALE_NORMAL);
			GL11.glPushMatrix();
			float var3 = 0.5F;
			GL11.glTranslatef(0.0F, 0.6875F, -0.75F);
			var3 *= 1.0F;
			GL11.glRotatef(20.0F, 1.0F, 0.0F, 0.0F);
			GL11.glRotatef(45.0F, 0.0F, 1.0F, 0.0F);
			GL11.glScalef(-var3, -var3, var3);
			final int var4 = par1EntityEnderman.getBrightnessForRender(par2);
			final int var5 = var4 % 65536;
			final int var6 = var4 / 65536;
			OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit,
					var5 / 1.0F, var6 / 1.0F);
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			loadTexture("/terrain.png");
			renderBlocks.renderBlockAsItem(
					Block.blocksList[par1EntityEnderman.getCarried()],
					par1EntityEnderman.getCarryingData(), 1.0F);
			GL11.glPopMatrix();
			GL11.glDisable(GL12.GL_RESCALE_NORMAL);
		}
	}

	/**
	 * Render the endermans eyes
	 */
	protected int renderEyes(final EntityEnderman par1EntityEnderman,
			final int par2, final float par3) {
		if (par2 != 0) {
			return -1;
		} else {
			loadTexture("/mob/enderman_eyes.png");
			final float var4 = 1.0F;
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glDisable(GL11.GL_ALPHA_TEST);
			GL11.glBlendFunc(GL11.GL_ONE, GL11.GL_ONE);
			GL11.glDisable(GL11.GL_LIGHTING);

			if (par1EntityEnderman.isInvisible()) {
				GL11.glDepthMask(false);
			} else {
				GL11.glDepthMask(true);
			}

			final char var5 = 61680;
			final int var6 = var5 % 65536;
			final int var7 = var5 / 65536;
			OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit,
					var6 / 1.0F, var7 / 1.0F);
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			GL11.glEnable(GL11.GL_LIGHTING);
			GL11.glColor4f(1.0F, 1.0F, 1.0F, var4);
			return 1;
		}
	}

	/**
	 * Queries whether should render the specified pass or not.
	 */
	@Override
	protected int shouldRenderPass(final EntityLiving par1EntityLiving,
			final int par2, final float par3) {
		return renderEyes((EntityEnderman) par1EntityLiving, par2, par3);
	}

	@Override
	protected void renderEquippedItems(final EntityLiving par1EntityLiving,
			final float par2) {
		renderCarrying((EntityEnderman) par1EntityLiving, par2);
	}

	@Override
	public void doRenderLiving(final EntityLiving par1EntityLiving,
			final double par2, final double par4, final double par6,
			final float par8, final float par9) {
		renderEnderman((EntityEnderman) par1EntityLiving, par2, par4, par6,
				par8, par9);
	}

	/**
	 * Actually renders the given argument. This is a synthetic bridge method,
	 * always casting down its argument and then handing it off to a worker
	 * function which does the actual work. In all probabilty, the class Render
	 * is generic (Render<T extends Entity) and this method has signature public
	 * void doRender(T entity, double d, double d1, double d2, float f, float
	 * f1). But JAD is pre 1.5 so doesn't do that.
	 */
	@Override
	public void doRender(final Entity par1Entity, final double par2,
			final double par4, final double par6, final float par8,
			final float par9) {
		renderEnderman((EntityEnderman) par1Entity, par2, par4, par6, par8,
				par9);
	}
}
