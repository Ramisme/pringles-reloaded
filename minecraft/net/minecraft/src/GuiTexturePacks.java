package net.minecraft.src;

import java.io.File;
import java.io.IOException;
import java.net.URI;

import net.minecraft.client.Minecraft;

import org.lwjgl.Sys;

public class GuiTexturePacks extends GuiScreen {
	protected GuiScreen guiScreen;
	private int refreshTimer = -1;

	/** the absolute location of this texture pack */
	private String fileLocation = "";

	/**
	 * the GuiTexturePackSlot that contains all the texture packs and their
	 * descriptions
	 */
	private GuiTexturePackSlot guiTexturePackSlot;

	public GuiTexturePacks(final GuiScreen par1, final GameSettings par2) {
		guiScreen = par1;
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui() {
		final StringTranslate var1 = StringTranslate.getInstance();
		buttonList.add(new GuiSmallButton(5, width / 2 - 154, height - 48, var1
				.translateKey("texturePack.openFolder")));
		buttonList.add(new GuiSmallButton(6, width / 2 + 4, height - 48, var1
				.translateKey("gui.done")));
		mc.texturePackList.updateAvaliableTexturePacks();
		fileLocation = new File(Minecraft.getMinecraftDir(), "texturepacks")
				.getAbsolutePath();
		guiTexturePackSlot = new GuiTexturePackSlot(this);
		guiTexturePackSlot.registerScrollButtons(buttonList, 7, 8);
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	@Override
	protected void actionPerformed(final GuiButton par1GuiButton) {
		if (par1GuiButton.enabled) {
			if (par1GuiButton.id == 5) {
				if (Minecraft.getOs() == EnumOS.MACOS) {
					try {
						mc.getLogAgent().logInfo(fileLocation);
						Runtime.getRuntime().exec(
								new String[] { "/usr/bin/open", fileLocation });
						return;
					} catch (final IOException var7) {
						var7.printStackTrace();
					}
				} else if (Minecraft.getOs() == EnumOS.WINDOWS) {
					final String var2 = String.format(
							"cmd.exe /C start \"Open file\" \"%s\"",
							new Object[] { fileLocation });

					try {
						Runtime.getRuntime().exec(var2);
						return;
					} catch (final IOException var6) {
						var6.printStackTrace();
					}
				}

				boolean var8 = false;

				try {
					final Class var3 = Class.forName("java.awt.Desktop");
					final Object var4 = var3.getMethod("getDesktop",
							new Class[0]).invoke((Object) null, new Object[0]);
					var3.getMethod("browse", new Class[] { URI.class })
							.invoke(var4,
									new Object[] { new File(Minecraft
											.getMinecraftDir(), "texturepacks")
											.toURI() });
				} catch (final Throwable var5) {
					var5.printStackTrace();
					var8 = true;
				}

				if (var8) {
					mc.getLogAgent().logInfo("Opening via system class!");
					Sys.openURL("file://" + fileLocation);
				}
			} else if (par1GuiButton.id == 6) {
				mc.displayGuiScreen(guiScreen);
			} else {
				guiTexturePackSlot.actionPerformed(par1GuiButton);
			}
		}
	}

	/**
	 * Called when the mouse is clicked.
	 */
	@Override
	protected void mouseClicked(final int par1, final int par2, final int par3) {
		super.mouseClicked(par1, par2, par3);
	}

	/**
	 * Called when the mouse is moved or a mouse button is released. Signature:
	 * (mouseX, mouseY, which) which==-1 is mouseMove, which==0 or which==1 is
	 * mouseUp
	 */
	@Override
	protected void mouseMovedOrUp(final int par1, final int par2, final int par3) {
		super.mouseMovedOrUp(par1, par2, par3);
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(final int par1, final int par2, final float par3) {
		guiTexturePackSlot.drawScreen(par1, par2, par3);

		if (refreshTimer <= 0) {
			mc.texturePackList.updateAvaliableTexturePacks();
			refreshTimer += 20;
		}

		final StringTranslate var4 = StringTranslate.getInstance();
		drawCenteredString(fontRenderer,
				var4.translateKey("texturePack.title"), width / 2, 16, 16777215);
		drawCenteredString(fontRenderer,
				var4.translateKey("texturePack.folderInfo"), width / 2 - 77,
				height - 26, 8421504);
		super.drawScreen(par1, par2, par3);
	}

	/**
	 * Called from the main game loop to update the screen.
	 */
	@Override
	public void updateScreen() {
		super.updateScreen();
		--refreshTimer;
	}

	static Minecraft func_73950_a(final GuiTexturePacks par0GuiTexturePacks) {
		return par0GuiTexturePacks.mc;
	}

	static Minecraft func_73955_b(final GuiTexturePacks par0GuiTexturePacks) {
		return par0GuiTexturePacks.mc;
	}

	static Minecraft func_73958_c(final GuiTexturePacks par0GuiTexturePacks) {
		return par0GuiTexturePacks.mc;
	}

	static Minecraft func_73951_d(final GuiTexturePacks par0GuiTexturePacks) {
		return par0GuiTexturePacks.mc;
	}

	static Minecraft func_73952_e(final GuiTexturePacks par0GuiTexturePacks) {
		return par0GuiTexturePacks.mc;
	}

	static Minecraft func_73962_f(final GuiTexturePacks par0GuiTexturePacks) {
		return par0GuiTexturePacks.mc;
	}

	static Minecraft func_73959_g(final GuiTexturePacks par0GuiTexturePacks) {
		return par0GuiTexturePacks.mc;
	}

	static Minecraft func_73957_h(final GuiTexturePacks par0GuiTexturePacks) {
		return par0GuiTexturePacks.mc;
	}

	static Minecraft func_73956_i(final GuiTexturePacks par0GuiTexturePacks) {
		return par0GuiTexturePacks.mc;
	}

	static Minecraft func_73953_j(final GuiTexturePacks par0GuiTexturePacks) {
		return par0GuiTexturePacks.mc;
	}

	static Minecraft func_73961_k(final GuiTexturePacks par0GuiTexturePacks) {
		return par0GuiTexturePacks.mc;
	}

	static Minecraft func_96143_l(final GuiTexturePacks par0GuiTexturePacks) {
		return par0GuiTexturePacks.mc;
	}

	static Minecraft func_96142_m(final GuiTexturePacks par0GuiTexturePacks) {
		return par0GuiTexturePacks.mc;
	}

	static FontRenderer func_73954_n(final GuiTexturePacks par0GuiTexturePacks) {
		return par0GuiTexturePacks.fontRenderer;
	}

	static FontRenderer func_96145_o(final GuiTexturePacks par0GuiTexturePacks) {
		return par0GuiTexturePacks.fontRenderer;
	}

	static FontRenderer func_96144_p(final GuiTexturePacks par0GuiTexturePacks) {
		return par0GuiTexturePacks.fontRenderer;
	}
}
