package net.minecraft.src;

import java.util.concurrent.Callable;

class CallableChunkPosHash implements Callable {
	final int field_85165_a;

	final int field_85163_b;

	final MapGenStructure theMapStructureGenerator;

	CallableChunkPosHash(final MapGenStructure par1MapGenStructure,
			final int par2, final int par3) {
		theMapStructureGenerator = par1MapGenStructure;
		field_85165_a = par2;
		field_85163_b = par3;
	}

	public String callChunkPositionHash() {
		return String.valueOf(ChunkCoordIntPair.chunkXZ2Int(field_85165_a,
				field_85163_b));
	}

	@Override
	public Object call() {
		return callChunkPositionHash();
	}
}
