package net.minecraft.src;

import java.util.Random;

public class BlockGlowStone extends Block {
	public BlockGlowStone(final int par1, final Material par2Material) {
		super(par1, par2Material);
		setCreativeTab(CreativeTabs.tabBlock);
	}

	/**
	 * Returns the usual quantity dropped by the block plus a bonus of 1 to 'i'
	 * (inclusive).
	 */
	@Override
	public int quantityDroppedWithBonus(final int par1, final Random par2Random) {
		return MathHelper.clamp_int(
				quantityDropped(par2Random) + par2Random.nextInt(par1 + 1), 1,
				4);
	}

	/**
	 * Returns the quantity of items to drop on block destruction.
	 */
	@Override
	public int quantityDropped(final Random par1Random) {
		return 2 + par1Random.nextInt(3);
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return Item.lightStoneDust.itemID;
	}
}
