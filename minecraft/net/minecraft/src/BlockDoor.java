package net.minecraft.src;

import java.util.Random;

public class BlockDoor extends Block {
	private static final String[] doorIconNames = new String[] {
			"doorWood_lower", "doorWood_upper", "doorIron_lower",
			"doorIron_upper" };

	/** Used for pointing at icon names. */
	private final int doorTypeForIcon;
	private Icon[] iconArray;

	protected BlockDoor(final int par1, final Material par2Material) {
		super(par1, par2Material);

		if (par2Material == Material.iron) {
			doorTypeForIcon = 2;
		} else {
			doorTypeForIcon = 0;
		}

		final float var3 = 0.5F;
		final float var4 = 1.0F;
		setBlockBounds(0.5F - var3, 0.0F, 0.5F - var3, 0.5F + var3, var4,
				0.5F + var3);
	}

	/**
	 * From the specified side and block metadata retrieves the blocks texture.
	 * Args: side, metadata
	 */
	@Override
	public Icon getIcon(final int par1, final int par2) {
		return iconArray[doorTypeForIcon];
	}

	/**
	 * Retrieves the block texture to use based on the display side. Args:
	 * iBlockAccess, x, y, z, side
	 */
	@Override
	public Icon getBlockTexture(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		if (par5 != 1 && par5 != 0) {
			final int var6 = getFullMetadata(par1IBlockAccess, par2, par3, par4);
			final int var7 = var6 & 3;
			final boolean var8 = (var6 & 4) != 0;
			boolean var9 = false;
			final boolean var10 = (var6 & 8) != 0;

			if (var8) {
				if (var7 == 0 && par5 == 2) {
					var9 = !var9;
				} else if (var7 == 1 && par5 == 5) {
					var9 = !var9;
				} else if (var7 == 2 && par5 == 3) {
					var9 = !var9;
				} else if (var7 == 3 && par5 == 4) {
					var9 = !var9;
				}
			} else {
				if (var7 == 0 && par5 == 5) {
					var9 = !var9;
				} else if (var7 == 1 && par5 == 3) {
					var9 = !var9;
				} else if (var7 == 2 && par5 == 4) {
					var9 = !var9;
				} else if (var7 == 3 && par5 == 2) {
					var9 = !var9;
				}

				if ((var6 & 16) != 0) {
					var9 = !var9;
				}
			}

			return iconArray[doorTypeForIcon
					+ (var9 ? BlockDoor.doorIconNames.length : 0)
					+ (var10 ? 1 : 0)];
		} else {
			return iconArray[doorTypeForIcon];
		}
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		iconArray = new Icon[BlockDoor.doorIconNames.length * 2];

		for (int var2 = 0; var2 < BlockDoor.doorIconNames.length; ++var2) {
			iconArray[var2] = par1IconRegister
					.registerIcon(BlockDoor.doorIconNames[var2]);
			iconArray[var2 + BlockDoor.doorIconNames.length] = new IconFlipped(
					iconArray[var2], true, false);
		}
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	@Override
	public boolean getBlocksMovement(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		final int var5 = getFullMetadata(par1IBlockAccess, par2, par3, par4);
		return (var5 & 4) != 0;
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return 7;
	}

	/**
	 * Returns the bounding box of the wired rectangular prism to render.
	 */
	@Override
	public AxisAlignedBB getSelectedBoundingBoxFromPool(final World par1World,
			final int par2, final int par3, final int par4) {
		setBlockBoundsBasedOnState(par1World, par2, par3, par4);
		return super
				.getSelectedBoundingBoxFromPool(par1World, par2, par3, par4);
	}

	/**
	 * Returns a bounding box from the pool of bounding boxes (this means this
	 * box can change after the pool has been cleared to be reused)
	 */
	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(final World par1World,
			final int par2, final int par3, final int par4) {
		setBlockBoundsBasedOnState(par1World, par2, par3, par4);
		return super.getCollisionBoundingBoxFromPool(par1World, par2, par3,
				par4);
	}

	/**
	 * Updates the blocks bounds based on its current state. Args: world, x, y,
	 * z
	 */
	@Override
	public void setBlockBoundsBasedOnState(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		setDoorRotation(getFullMetadata(par1IBlockAccess, par2, par3, par4));
	}

	/**
	 * Returns 0, 1, 2 or 3 depending on where the hinge is.
	 */
	public int getDoorOrientation(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		return getFullMetadata(par1IBlockAccess, par2, par3, par4) & 3;
	}

	public boolean isDoorOpen(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		return (getFullMetadata(par1IBlockAccess, par2, par3, par4) & 4) != 0;
	}

	private void setDoorRotation(final int par1) {
		final float var2 = 0.1875F;
		setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 2.0F, 1.0F);
		final int var3 = par1 & 3;
		final boolean var4 = (par1 & 4) != 0;
		final boolean var5 = (par1 & 16) != 0;

		if (var3 == 0) {
			if (var4) {
				if (!var5) {
					setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, var2);
				} else {
					setBlockBounds(0.0F, 0.0F, 1.0F - var2, 1.0F, 1.0F, 1.0F);
				}
			} else {
				setBlockBounds(0.0F, 0.0F, 0.0F, var2, 1.0F, 1.0F);
			}
		} else if (var3 == 1) {
			if (var4) {
				if (!var5) {
					setBlockBounds(1.0F - var2, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
				} else {
					setBlockBounds(0.0F, 0.0F, 0.0F, var2, 1.0F, 1.0F);
				}
			} else {
				setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, var2);
			}
		} else if (var3 == 2) {
			if (var4) {
				if (!var5) {
					setBlockBounds(0.0F, 0.0F, 1.0F - var2, 1.0F, 1.0F, 1.0F);
				} else {
					setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, var2);
				}
			} else {
				setBlockBounds(1.0F - var2, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
			}
		} else if (var3 == 3) {
			if (var4) {
				if (!var5) {
					setBlockBounds(0.0F, 0.0F, 0.0F, var2, 1.0F, 1.0F);
				} else {
					setBlockBounds(1.0F - var2, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
				}
			} else {
				setBlockBounds(0.0F, 0.0F, 1.0F - var2, 1.0F, 1.0F, 1.0F);
			}
		}
	}

	/**
	 * Called when the block is clicked by a player. Args: x, y, z, entityPlayer
	 */
	@Override
	public void onBlockClicked(final World par1World, final int par2,
			final int par3, final int par4, final EntityPlayer par5EntityPlayer) {
	}

	/**
	 * Called upon block activation (right click on the block.)
	 */
	@Override
	public boolean onBlockActivated(final World par1World, final int par2,
			final int par3, final int par4,
			final EntityPlayer par5EntityPlayer, final int par6,
			final float par7, final float par8, final float par9) {
		if (blockMaterial == Material.iron) {
			return true;
		} else {
			final int var10 = getFullMetadata(par1World, par2, par3, par4);
			int var11 = var10 & 7;
			var11 ^= 4;

			if ((var10 & 8) == 0) {
				par1World
						.setBlockMetadataWithNotify(par2, par3, par4, var11, 2);
				par1World.markBlockRangeForRenderUpdate(par2, par3, par4, par2,
						par3, par4);
			} else {
				par1World.setBlockMetadataWithNotify(par2, par3 - 1, par4,
						var11, 2);
				par1World.markBlockRangeForRenderUpdate(par2, par3 - 1, par4,
						par2, par3, par4);
			}

			par1World.playAuxSFXAtEntity(par5EntityPlayer, 1003, par2, par3,
					par4, 0);
			return true;
		}
	}

	/**
	 * A function to open a door.
	 */
	public void onPoweredBlockChange(final World par1World, final int par2,
			final int par3, final int par4, final boolean par5) {
		final int var6 = getFullMetadata(par1World, par2, par3, par4);
		final boolean var7 = (var6 & 4) != 0;

		if (var7 != par5) {
			int var8 = var6 & 7;
			var8 ^= 4;

			if ((var6 & 8) == 0) {
				par1World.setBlockMetadataWithNotify(par2, par3, par4, var8, 2);
				par1World.markBlockRangeForRenderUpdate(par2, par3, par4, par2,
						par3, par4);
			} else {
				par1World.setBlockMetadataWithNotify(par2, par3 - 1, par4,
						var8, 2);
				par1World.markBlockRangeForRenderUpdate(par2, par3 - 1, par4,
						par2, par3, par4);
			}

			par1World.playAuxSFXAtEntity((EntityPlayer) null, 1003, par2, par3,
					par4, 0);
		}
	}

	/**
	 * Lets the block know when one of its neighbor changes. Doesn't know which
	 * neighbor changed (coordinates passed are their own) Args: x, y, z,
	 * neighbor blockID
	 */
	@Override
	public void onNeighborBlockChange(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		final int var6 = par1World.getBlockMetadata(par2, par3, par4);

		if ((var6 & 8) == 0) {
			boolean var7 = false;

			if (par1World.getBlockId(par2, par3 + 1, par4) != blockID) {
				par1World.setBlockToAir(par2, par3, par4);
				var7 = true;
			}

			if (!par1World.doesBlockHaveSolidTopSurface(par2, par3 - 1, par4)) {
				par1World.setBlockToAir(par2, par3, par4);
				var7 = true;

				if (par1World.getBlockId(par2, par3 + 1, par4) == blockID) {
					par1World.setBlockToAir(par2, par3 + 1, par4);
				}
			}

			if (var7) {
				if (!par1World.isRemote) {
					dropBlockAsItem(par1World, par2, par3, par4, var6, 0);
				}
			} else {
				final boolean var8 = par1World.isBlockIndirectlyGettingPowered(
						par2, par3, par4)
						|| par1World.isBlockIndirectlyGettingPowered(par2,
								par3 + 1, par4);

				if ((var8 || par5 > 0
						&& Block.blocksList[par5].canProvidePower())
						&& par5 != blockID) {
					onPoweredBlockChange(par1World, par2, par3, par4, var8);
				}
			}
		} else {
			if (par1World.getBlockId(par2, par3 - 1, par4) != blockID) {
				par1World.setBlockToAir(par2, par3, par4);
			}

			if (par5 > 0 && par5 != blockID) {
				onNeighborBlockChange(par1World, par2, par3 - 1, par4, par5);
			}
		}
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return (par1 & 8) != 0 ? 0
				: blockMaterial == Material.iron ? Item.doorIron.itemID
						: Item.doorWood.itemID;
	}

	/**
	 * Ray traces through the blocks collision from start vector to end vector
	 * returning a ray trace hit. Args: world, x, y, z, startVec, endVec
	 */
	@Override
	public MovingObjectPosition collisionRayTrace(final World par1World,
			final int par2, final int par3, final int par4,
			final Vec3 par5Vec3, final Vec3 par6Vec3) {
		setBlockBoundsBasedOnState(par1World, par2, par3, par4);
		return super.collisionRayTrace(par1World, par2, par3, par4, par5Vec3,
				par6Vec3);
	}

	/**
	 * Checks to see if its valid to put this block at the specified
	 * coordinates. Args: world, x, y, z
	 */
	@Override
	public boolean canPlaceBlockAt(final World par1World, final int par2,
			final int par3, final int par4) {
		return par3 >= 255 ? false : par1World.doesBlockHaveSolidTopSurface(
				par2, par3 - 1, par4)
				&& super.canPlaceBlockAt(par1World, par2, par3, par4)
				&& super.canPlaceBlockAt(par1World, par2, par3 + 1, par4);
	}

	/**
	 * Returns the mobility information of the block, 0 = free, 1 = can't push
	 * but can move over, 2 = total immobility and stop pistons
	 */
	@Override
	public int getMobilityFlag() {
		return 1;
	}

	/**
	 * Returns the full metadata value created by combining the metadata of both
	 * blocks the door takes up.
	 */
	public int getFullMetadata(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		final int var5 = par1IBlockAccess.getBlockMetadata(par2, par3, par4);
		final boolean var6 = (var5 & 8) != 0;
		int var7;
		int var8;

		if (var6) {
			var7 = par1IBlockAccess.getBlockMetadata(par2, par3 - 1, par4);
			var8 = var5;
		} else {
			var7 = var5;
			var8 = par1IBlockAccess.getBlockMetadata(par2, par3 + 1, par4);
		}

		final boolean var9 = (var8 & 1) != 0;
		return var7 & 7 | (var6 ? 8 : 0) | (var9 ? 16 : 0);
	}

	/**
	 * only called by clickMiddleMouseButton , and passed to
	 * inventory.setCurrentItem (along with isCreative)
	 */
	@Override
	public int idPicked(final World par1World, final int par2, final int par3,
			final int par4) {
		return blockMaterial == Material.iron ? Item.doorIron.itemID
				: Item.doorWood.itemID;
	}

	/**
	 * Called when the block is attempted to be harvested
	 */
	@Override
	public void onBlockHarvested(final World par1World, final int par2,
			final int par3, final int par4, final int par5,
			final EntityPlayer par6EntityPlayer) {
		if (par6EntityPlayer.capabilities.isCreativeMode && (par5 & 8) != 0
				&& par1World.getBlockId(par2, par3 - 1, par4) == blockID) {
			par1World.setBlockToAir(par2, par3 - 1, par4);
		}
	}
}
