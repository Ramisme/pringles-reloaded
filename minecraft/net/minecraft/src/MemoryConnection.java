package net.minecraft.src;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MemoryConnection implements INetworkManager {
	private static final SocketAddress mySocketAddress = new InetSocketAddress(
			"127.0.0.1", 0);
	private final List readPacketCache = Collections
			.synchronizedList(new ArrayList());
	private final ILogAgent field_98214_c;
	private MemoryConnection pairedConnection;
	private NetHandler myNetHandler;

	/** set to true by {server,network}Shutdown */
	private boolean shuttingDown = false;
	private String shutdownReason = "";
	private Object[] field_74439_g;
	private boolean gamePaused = false;

	public MemoryConnection(final ILogAgent par1ILogAgent,
			final NetHandler par2NetHandler) {
		myNetHandler = par2NetHandler;
		field_98214_c = par1ILogAgent;
	}

	/**
	 * Sets the NetHandler for this NetworkManager. Server-only.
	 */
	@Override
	public void setNetHandler(final NetHandler par1NetHandler) {
		myNetHandler = par1NetHandler;
	}

	/**
	 * Adds the packet to the correct send queue (chunk data packets go to a
	 * separate queue).
	 */
	@Override
	public void addToSendQueue(final Packet par1Packet) {
		if (!shuttingDown) {
			pairedConnection.processOrCachePacket(par1Packet);
		}
	}

	@Override
	public void closeConnections() {
		pairedConnection = null;
		myNetHandler = null;
	}

	public boolean isConnectionActive() {
		return !shuttingDown && pairedConnection != null;
	}

	/**
	 * Wakes reader and writer threads
	 */
	@Override
	public void wakeThreads() {
	}

	/**
	 * Checks timeouts and processes all pending read packets.
	 */
	@Override
	public void processReadPackets() {
		int var1 = 2500;

		while (var1-- >= 0 && !readPacketCache.isEmpty()) {
			final Packet var2 = (Packet) readPacketCache.remove(0);
			var2.processPacket(myNetHandler);
		}

		if (readPacketCache.size() > var1) {
			field_98214_c
					.logWarning("Memory connection overburdened; after processing 2500 packets, we still have "
							+ readPacketCache.size() + " to go!");
		}

		if (shuttingDown && readPacketCache.isEmpty()) {
			myNetHandler.handleErrorMessage(shutdownReason, field_74439_g);
		}
	}

	/**
	 * Return the InetSocketAddress of the remote endpoint
	 */
	@Override
	public SocketAddress getSocketAddress() {
		return MemoryConnection.mySocketAddress;
	}

	/**
	 * Shuts down the server. (Only actually used on the server)
	 */
	@Override
	public void serverShutdown() {
		shuttingDown = true;
	}

	/**
	 * Shuts down the network with the specified reason. Closes all streams and
	 * sockets, spawns NetworkMasterThread to stop reading and writing threads.
	 */
	@Override
	public void networkShutdown(final String par1Str,
			final Object... par2ArrayOfObj) {
		shuttingDown = true;
		shutdownReason = par1Str;
		field_74439_g = par2ArrayOfObj;
	}

	/**
	 * returns 0 for memoryConnections
	 */
	@Override
	public int packetSize() {
		return 0;
	}

	public void pairWith(final MemoryConnection par1MemoryConnection) {
		pairedConnection = par1MemoryConnection;
		par1MemoryConnection.pairedConnection = this;
	}

	public boolean isGamePaused() {
		return gamePaused;
	}

	public void setGamePaused(final boolean par1) {
		gamePaused = par1;
	}

	public MemoryConnection getPairedConnection() {
		return pairedConnection;
	}

	/**
	 * acts immiditally if isWritePacket, otherwise adds it to the readCache to
	 * be processed next tick
	 */
	public void processOrCachePacket(final Packet par1Packet) {
		if (par1Packet.canProcessAsync()
				&& myNetHandler.canProcessPacketsAsync()) {
			par1Packet.processPacket(myNetHandler);
		} else {
			readPacketCache.add(par1Packet);
		}
	}
}
