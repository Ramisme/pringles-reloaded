package net.minecraft.src;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class BlockRedstoneWire extends Block {
	/**
	 * When false, power transmission methods do not look at other redstone
	 * wires. Used internally during updateCurrentStrength.
	 */
	private boolean wiresProvidePower = true;
	private final Set blocksNeedingUpdate = new HashSet();
	private Icon field_94413_c;
	private Icon field_94410_cO;
	private Icon field_94411_cP;
	private Icon field_94412_cQ;

	public BlockRedstoneWire(final int par1) {
		super(par1, Material.circuits);
		setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.0625F, 1.0F);
	}

	/**
	 * Returns a bounding box from the pool of bounding boxes (this means this
	 * box can change after the pool has been cleared to be reused)
	 */
	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(final World par1World,
			final int par2, final int par3, final int par4) {
		return null;
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return 5;
	}

	/**
	 * Returns a integer with hex for 0xrrggbb with this color multiplied
	 * against the blocks color. Note only called when first determining what to
	 * render.
	 */
	@Override
	public int colorMultiplier(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		return 8388608;
	}

	/**
	 * Checks to see if its valid to put this block at the specified
	 * coordinates. Args: world, x, y, z
	 */
	@Override
	public boolean canPlaceBlockAt(final World par1World, final int par2,
			final int par3, final int par4) {
		return par1World.doesBlockHaveSolidTopSurface(par2, par3 - 1, par4)
				|| par1World.getBlockId(par2, par3 - 1, par4) == Block.glowStone.blockID;
	}

	/**
	 * Sets the strength of the wire current (0-15) for this block based on
	 * neighboring blocks and propagates to neighboring redstone wires
	 */
	private void updateAndPropagateCurrentStrength(final World par1World,
			final int par2, final int par3, final int par4) {
		calculateCurrentChanges(par1World, par2, par3, par4, par2, par3, par4);
		final ArrayList var5 = new ArrayList(blocksNeedingUpdate);
		blocksNeedingUpdate.clear();

		for (int var6 = 0; var6 < var5.size(); ++var6) {
			final ChunkPosition var7 = (ChunkPosition) var5.get(var6);
			par1World.notifyBlocksOfNeighborChange(var7.x, var7.y, var7.z,
					blockID);
		}
	}

	private void calculateCurrentChanges(final World par1World, final int par2,
			final int par3, final int par4, final int par5, final int par6,
			final int par7) {
		final int var8 = par1World.getBlockMetadata(par2, par3, par4);
		final byte var9 = 0;
		int var15 = getMaxCurrentStrength(par1World, par5, par6, par7, var9);
		wiresProvidePower = false;
		final int var10 = par1World.getStrongestIndirectPower(par2, par3, par4);
		wiresProvidePower = true;

		if (var10 > 0 && var10 > var15 - 1) {
			var15 = var10;
		}

		int var11 = 0;

		for (int var12 = 0; var12 < 4; ++var12) {
			int var13 = par2;
			int var14 = par4;

			if (var12 == 0) {
				var13 = par2 - 1;
			}

			if (var12 == 1) {
				++var13;
			}

			if (var12 == 2) {
				var14 = par4 - 1;
			}

			if (var12 == 3) {
				++var14;
			}

			if (var13 != par5 || var14 != par7) {
				var11 = getMaxCurrentStrength(par1World, var13, par3, var14,
						var11);
			}

			if (par1World.isBlockNormalCube(var13, par3, var14)
					&& !par1World.isBlockNormalCube(par2, par3 + 1, par4)) {
				if ((var13 != par5 || var14 != par7) && par3 >= par6) {
					var11 = getMaxCurrentStrength(par1World, var13, par3 + 1,
							var14, var11);
				}
			} else if (!par1World.isBlockNormalCube(var13, par3, var14)
					&& (var13 != par5 || var14 != par7) && par3 <= par6) {
				var11 = getMaxCurrentStrength(par1World, var13, par3 - 1,
						var14, var11);
			}
		}

		if (var11 > var15) {
			var15 = var11 - 1;
		} else if (var15 > 0) {
			--var15;
		} else {
			var15 = 0;
		}

		if (var10 > var15 - 1) {
			var15 = var10;
		}

		if (var8 != var15) {
			par1World.setBlockMetadataWithNotify(par2, par3, par4, var15, 2);
			blocksNeedingUpdate.add(new ChunkPosition(par2, par3, par4));
			blocksNeedingUpdate.add(new ChunkPosition(par2 - 1, par3, par4));
			blocksNeedingUpdate.add(new ChunkPosition(par2 + 1, par3, par4));
			blocksNeedingUpdate.add(new ChunkPosition(par2, par3 - 1, par4));
			blocksNeedingUpdate.add(new ChunkPosition(par2, par3 + 1, par4));
			blocksNeedingUpdate.add(new ChunkPosition(par2, par3, par4 - 1));
			blocksNeedingUpdate.add(new ChunkPosition(par2, par3, par4 + 1));
		}
	}

	/**
	 * Calls World.notifyBlocksOfNeighborChange() for all neighboring blocks,
	 * but only if the given block is a redstone wire.
	 */
	private void notifyWireNeighborsOfNeighborChange(final World par1World,
			final int par2, final int par3, final int par4) {
		if (par1World.getBlockId(par2, par3, par4) == blockID) {
			par1World.notifyBlocksOfNeighborChange(par2, par3, par4, blockID);
			par1World.notifyBlocksOfNeighborChange(par2 - 1, par3, par4,
					blockID);
			par1World.notifyBlocksOfNeighborChange(par2 + 1, par3, par4,
					blockID);
			par1World.notifyBlocksOfNeighborChange(par2, par3, par4 - 1,
					blockID);
			par1World.notifyBlocksOfNeighborChange(par2, par3, par4 + 1,
					blockID);
			par1World.notifyBlocksOfNeighborChange(par2, par3 - 1, par4,
					blockID);
			par1World.notifyBlocksOfNeighborChange(par2, par3 + 1, par4,
					blockID);
		}
	}

	/**
	 * Called whenever the block is added into the world. Args: world, x, y, z
	 */
	@Override
	public void onBlockAdded(final World par1World, final int par2,
			final int par3, final int par4) {
		super.onBlockAdded(par1World, par2, par3, par4);

		if (!par1World.isRemote) {
			updateAndPropagateCurrentStrength(par1World, par2, par3, par4);
			par1World.notifyBlocksOfNeighborChange(par2, par3 + 1, par4,
					blockID);
			par1World.notifyBlocksOfNeighborChange(par2, par3 - 1, par4,
					blockID);
			notifyWireNeighborsOfNeighborChange(par1World, par2 - 1, par3, par4);
			notifyWireNeighborsOfNeighborChange(par1World, par2 + 1, par3, par4);
			notifyWireNeighborsOfNeighborChange(par1World, par2, par3, par4 - 1);
			notifyWireNeighborsOfNeighborChange(par1World, par2, par3, par4 + 1);

			if (par1World.isBlockNormalCube(par2 - 1, par3, par4)) {
				notifyWireNeighborsOfNeighborChange(par1World, par2 - 1,
						par3 + 1, par4);
			} else {
				notifyWireNeighborsOfNeighborChange(par1World, par2 - 1,
						par3 - 1, par4);
			}

			if (par1World.isBlockNormalCube(par2 + 1, par3, par4)) {
				notifyWireNeighborsOfNeighborChange(par1World, par2 + 1,
						par3 + 1, par4);
			} else {
				notifyWireNeighborsOfNeighborChange(par1World, par2 + 1,
						par3 - 1, par4);
			}

			if (par1World.isBlockNormalCube(par2, par3, par4 - 1)) {
				notifyWireNeighborsOfNeighborChange(par1World, par2, par3 + 1,
						par4 - 1);
			} else {
				notifyWireNeighborsOfNeighborChange(par1World, par2, par3 - 1,
						par4 - 1);
			}

			if (par1World.isBlockNormalCube(par2, par3, par4 + 1)) {
				notifyWireNeighborsOfNeighborChange(par1World, par2, par3 + 1,
						par4 + 1);
			} else {
				notifyWireNeighborsOfNeighborChange(par1World, par2, par3 - 1,
						par4 + 1);
			}
		}
	}

	/**
	 * ejects contained items into the world, and notifies neighbours of an
	 * update, as appropriate
	 */
	@Override
	public void breakBlock(final World par1World, final int par2,
			final int par3, final int par4, final int par5, final int par6) {
		super.breakBlock(par1World, par2, par3, par4, par5, par6);

		if (!par1World.isRemote) {
			par1World.notifyBlocksOfNeighborChange(par2, par3 + 1, par4,
					blockID);
			par1World.notifyBlocksOfNeighborChange(par2, par3 - 1, par4,
					blockID);
			par1World.notifyBlocksOfNeighborChange(par2 + 1, par3, par4,
					blockID);
			par1World.notifyBlocksOfNeighborChange(par2 - 1, par3, par4,
					blockID);
			par1World.notifyBlocksOfNeighborChange(par2, par3, par4 + 1,
					blockID);
			par1World.notifyBlocksOfNeighborChange(par2, par3, par4 - 1,
					blockID);
			updateAndPropagateCurrentStrength(par1World, par2, par3, par4);
			notifyWireNeighborsOfNeighborChange(par1World, par2 - 1, par3, par4);
			notifyWireNeighborsOfNeighborChange(par1World, par2 + 1, par3, par4);
			notifyWireNeighborsOfNeighborChange(par1World, par2, par3, par4 - 1);
			notifyWireNeighborsOfNeighborChange(par1World, par2, par3, par4 + 1);

			if (par1World.isBlockNormalCube(par2 - 1, par3, par4)) {
				notifyWireNeighborsOfNeighborChange(par1World, par2 - 1,
						par3 + 1, par4);
			} else {
				notifyWireNeighborsOfNeighborChange(par1World, par2 - 1,
						par3 - 1, par4);
			}

			if (par1World.isBlockNormalCube(par2 + 1, par3, par4)) {
				notifyWireNeighborsOfNeighborChange(par1World, par2 + 1,
						par3 + 1, par4);
			} else {
				notifyWireNeighborsOfNeighborChange(par1World, par2 + 1,
						par3 - 1, par4);
			}

			if (par1World.isBlockNormalCube(par2, par3, par4 - 1)) {
				notifyWireNeighborsOfNeighborChange(par1World, par2, par3 + 1,
						par4 - 1);
			} else {
				notifyWireNeighborsOfNeighborChange(par1World, par2, par3 - 1,
						par4 - 1);
			}

			if (par1World.isBlockNormalCube(par2, par3, par4 + 1)) {
				notifyWireNeighborsOfNeighborChange(par1World, par2, par3 + 1,
						par4 + 1);
			} else {
				notifyWireNeighborsOfNeighborChange(par1World, par2, par3 - 1,
						par4 + 1);
			}
		}
	}

	/**
	 * Returns the current strength at the specified block if it is greater than
	 * the passed value, or the passed value otherwise. Signature: (world, x, y,
	 * z, strength)
	 */
	private int getMaxCurrentStrength(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		if (par1World.getBlockId(par2, par3, par4) != blockID) {
			return par5;
		} else {
			final int var6 = par1World.getBlockMetadata(par2, par3, par4);
			return var6 > par5 ? var6 : par5;
		}
	}

	/**
	 * Lets the block know when one of its neighbor changes. Doesn't know which
	 * neighbor changed (coordinates passed are their own) Args: x, y, z,
	 * neighbor blockID
	 */
	@Override
	public void onNeighborBlockChange(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		if (!par1World.isRemote) {
			final boolean var6 = canPlaceBlockAt(par1World, par2, par3, par4);

			if (var6) {
				updateAndPropagateCurrentStrength(par1World, par2, par3, par4);
			} else {
				dropBlockAsItem(par1World, par2, par3, par4, 0, 0);
				par1World.setBlockToAir(par2, par3, par4);
			}

			super.onNeighborBlockChange(par1World, par2, par3, par4, par5);
		}
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return Item.redstone.itemID;
	}

	/**
	 * Returns true if the block is emitting direct/strong redstone power on the
	 * specified side. Args: World, X, Y, Z, side. Note that the side is
	 * reversed - eg it is 1 (up) when checking the bottom of the block.
	 */
	@Override
	public int isProvidingStrongPower(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		return !wiresProvidePower ? 0 : isProvidingWeakPower(par1IBlockAccess,
				par2, par3, par4, par5);
	}

	/**
	 * Returns true if the block is emitting indirect/weak redstone power on the
	 * specified side. If isBlockNormalCube returns true, standard redstone
	 * propagation rules will apply instead and this will not be called. Args:
	 * World, X, Y, Z, side. Note that the side is reversed - eg it is 1 (up)
	 * when checking the bottom of the block.
	 */
	@Override
	public int isProvidingWeakPower(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		if (!wiresProvidePower) {
			return 0;
		} else {
			final int var6 = par1IBlockAccess
					.getBlockMetadata(par2, par3, par4);

			if (var6 == 0) {
				return 0;
			} else if (par5 == 1) {
				return var6;
			} else {
				boolean var7 = BlockRedstoneWire.isPoweredOrRepeater(
						par1IBlockAccess, par2 - 1, par3, par4, 1)
						|| !par1IBlockAccess.isBlockNormalCube(par2 - 1, par3,
								par4)
						&& BlockRedstoneWire.isPoweredOrRepeater(
								par1IBlockAccess, par2 - 1, par3 - 1, par4, -1);
				boolean var8 = BlockRedstoneWire.isPoweredOrRepeater(
						par1IBlockAccess, par2 + 1, par3, par4, 3)
						|| !par1IBlockAccess.isBlockNormalCube(par2 + 1, par3,
								par4)
						&& BlockRedstoneWire.isPoweredOrRepeater(
								par1IBlockAccess, par2 + 1, par3 - 1, par4, -1);
				boolean var9 = BlockRedstoneWire.isPoweredOrRepeater(
						par1IBlockAccess, par2, par3, par4 - 1, 2)
						|| !par1IBlockAccess.isBlockNormalCube(par2, par3,
								par4 - 1)
						&& BlockRedstoneWire.isPoweredOrRepeater(
								par1IBlockAccess, par2, par3 - 1, par4 - 1, -1);
				boolean var10 = BlockRedstoneWire.isPoweredOrRepeater(
						par1IBlockAccess, par2, par3, par4 + 1, 0)
						|| !par1IBlockAccess.isBlockNormalCube(par2, par3,
								par4 + 1)
						&& BlockRedstoneWire.isPoweredOrRepeater(
								par1IBlockAccess, par2, par3 - 1, par4 + 1, -1);

				if (!par1IBlockAccess.isBlockNormalCube(par2, par3 + 1, par4)) {
					if (par1IBlockAccess
							.isBlockNormalCube(par2 - 1, par3, par4)
							&& BlockRedstoneWire.isPoweredOrRepeater(
									par1IBlockAccess, par2 - 1, par3 + 1, par4,
									-1)) {
						var7 = true;
					}

					if (par1IBlockAccess
							.isBlockNormalCube(par2 + 1, par3, par4)
							&& BlockRedstoneWire.isPoweredOrRepeater(
									par1IBlockAccess, par2 + 1, par3 + 1, par4,
									-1)) {
						var8 = true;
					}

					if (par1IBlockAccess
							.isBlockNormalCube(par2, par3, par4 - 1)
							&& BlockRedstoneWire.isPoweredOrRepeater(
									par1IBlockAccess, par2, par3 + 1, par4 - 1,
									-1)) {
						var9 = true;
					}

					if (par1IBlockAccess
							.isBlockNormalCube(par2, par3, par4 + 1)
							&& BlockRedstoneWire.isPoweredOrRepeater(
									par1IBlockAccess, par2, par3 + 1, par4 + 1,
									-1)) {
						var10 = true;
					}
				}

				return !var9 && !var8 && !var7 && !var10 && par5 >= 2
						&& par5 <= 5 ? var6
						: par5 == 2 && var9 && !var7 && !var8 ? var6
								: par5 == 3 && var10 && !var7 && !var8 ? var6
										: par5 == 4 && var7 && !var9 && !var10 ? var6
												: par5 == 5 && var8 && !var9
														&& !var10 ? var6 : 0;
			}
		}
	}

	/**
	 * Can this block provide power. Only wire currently seems to have this
	 * change based on its state.
	 */
	@Override
	public boolean canProvidePower() {
		return wiresProvidePower;
	}

	/**
	 * A randomly called display update to be able to add particles or other
	 * items for display
	 */
	@Override
	public void randomDisplayTick(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		final int var6 = par1World.getBlockMetadata(par2, par3, par4);

		if (var6 > 0) {
			final double var7 = par2 + 0.5D + (par5Random.nextFloat() - 0.5D)
					* 0.2D;
			final double var9 = par3 + 0.0625F;
			final double var11 = par4 + 0.5D + (par5Random.nextFloat() - 0.5D)
					* 0.2D;
			final float var13 = var6 / 15.0F;
			float var14 = var13 * 0.6F + 0.4F;

			if (var6 == 0) {
				var14 = 0.0F;
			}

			float var15 = var13 * var13 * 0.7F - 0.5F;
			float var16 = var13 * var13 * 0.6F - 0.7F;

			if (var15 < 0.0F) {
				var15 = 0.0F;
			}

			if (var16 < 0.0F) {
				var16 = 0.0F;
			}

			par1World.spawnParticle("reddust", var7, var9, var11, var14, var15,
					var16);
		}
	}

	/**
	 * Returns true if redstone wire can connect to the specified block. Params:
	 * World, X, Y, Z, side (not a normal notch-side, this can be 0, 1, 2, 3 or
	 * -1)
	 */
	public static boolean isPowerProviderOrWire(
			final IBlockAccess par0IBlockAccess, final int par1,
			final int par2, final int par3, final int par4) {
		final int var5 = par0IBlockAccess.getBlockId(par1, par2, par3);

		if (var5 == Block.redstoneWire.blockID) {
			return true;
		} else if (var5 == 0) {
			return false;
		} else if (!Block.redstoneRepeaterIdle.func_94487_f(var5)) {
			return Block.blocksList[var5].canProvidePower() && par4 != -1;
		} else {
			final int var6 = par0IBlockAccess
					.getBlockMetadata(par1, par2, par3);
			return par4 == (var6 & 3)
					|| par4 == Direction.rotateOpposite[var6 & 3];
		}
	}

	/**
	 * Returns true if the block coordinate passed can provide power, or is a
	 * redstone wire, or if its a repeater that is powered.
	 */
	public static boolean isPoweredOrRepeater(
			final IBlockAccess par0IBlockAccess, final int par1,
			final int par2, final int par3, final int par4) {
		if (BlockRedstoneWire.isPowerProviderOrWire(par0IBlockAccess, par1,
				par2, par3, par4)) {
			return true;
		} else {
			final int var5 = par0IBlockAccess.getBlockId(par1, par2, par3);

			if (var5 == Block.redstoneRepeaterActive.blockID) {
				final int var6 = par0IBlockAccess.getBlockMetadata(par1, par2,
						par3);
				return par4 == (var6 & 3);
			} else {
				return false;
			}
		}
	}

	/**
	 * only called by clickMiddleMouseButton , and passed to
	 * inventory.setCurrentItem (along with isCreative)
	 */
	@Override
	public int idPicked(final World par1World, final int par2, final int par3,
			final int par4) {
		return Item.redstone.itemID;
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		field_94413_c = par1IconRegister.registerIcon("redstoneDust_cross");
		field_94410_cO = par1IconRegister.registerIcon("redstoneDust_line");
		field_94411_cP = par1IconRegister
				.registerIcon("redstoneDust_cross_overlay");
		field_94412_cQ = par1IconRegister
				.registerIcon("redstoneDust_line_overlay");
		blockIcon = field_94413_c;
	}

	public static Icon func_94409_b(final String par0Str) {
		return par0Str == "redstoneDust_cross" ? Block.redstoneWire.field_94413_c
				: par0Str == "redstoneDust_line" ? Block.redstoneWire.field_94410_cO
						: par0Str == "redstoneDust_cross_overlay" ? Block.redstoneWire.field_94411_cP
								: par0Str == "redstoneDust_line_overlay" ? Block.redstoneWire.field_94412_cQ
										: null;
	}
}
