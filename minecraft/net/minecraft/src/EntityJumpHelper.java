package net.minecraft.src;

public class EntityJumpHelper {
	private final EntityLiving entity;
	private boolean isJumping = false;

	public EntityJumpHelper(final EntityLiving par1EntityLiving) {
		entity = par1EntityLiving;
	}

	public void setJumping() {
		isJumping = true;
	}

	/**
	 * Called to actually make the entity jump if isJumping is true.
	 */
	public void doJump() {
		entity.setJumping(isJumping);
		isJumping = false;
	}
}
