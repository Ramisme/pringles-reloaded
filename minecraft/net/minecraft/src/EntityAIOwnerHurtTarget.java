package net.minecraft.src;

public class EntityAIOwnerHurtTarget extends EntityAITarget {
	EntityTameable theEntityTameable;
	EntityLiving theTarget;

	public EntityAIOwnerHurtTarget(final EntityTameable par1EntityTameable) {
		super(par1EntityTameable, 32.0F, false);
		theEntityTameable = par1EntityTameable;
		setMutexBits(1);
	}

	/**
	 * Returns whether the EntityAIBase should begin execution.
	 */
	@Override
	public boolean shouldExecute() {
		if (!theEntityTameable.isTamed()) {
			return false;
		} else {
			final EntityLiving var1 = theEntityTameable.getOwner();

			if (var1 == null) {
				return false;
			} else {
				theTarget = var1.getLastAttackingEntity();
				return isSuitableTarget(theTarget, false);
			}
		}
	}

	/**
	 * Execute a one shot task or start executing a continuous task
	 */
	@Override
	public void startExecuting() {
		taskOwner.setAttackTarget(theTarget);
		super.startExecuting();
	}
}
