package net.minecraft.src;

import java.util.List;
import java.util.Random;

public class ComponentStrongholdStraight extends ComponentStronghold {
	private final EnumDoor doorType;
	private final boolean expandsX;
	private final boolean expandsZ;

	public ComponentStrongholdStraight(final int par1, final Random par2Random,
			final StructureBoundingBox par3StructureBoundingBox, final int par4) {
		super(par1);
		coordBaseMode = par4;
		doorType = getRandomDoor(par2Random);
		boundingBox = par3StructureBoundingBox;
		expandsX = par2Random.nextInt(2) == 0;
		expandsZ = par2Random.nextInt(2) == 0;
	}

	/**
	 * Initiates construction of the Structure Component picked, at the current
	 * Location of StructGen
	 */
	@Override
	public void buildComponent(final StructureComponent par1StructureComponent,
			final List par2List, final Random par3Random) {
		getNextComponentNormal(
				(ComponentStrongholdStairs2) par1StructureComponent, par2List,
				par3Random, 1, 1);

		if (expandsX) {
			getNextComponentX(
					(ComponentStrongholdStairs2) par1StructureComponent,
					par2List, par3Random, 1, 2);
		}

		if (expandsZ) {
			getNextComponentZ(
					(ComponentStrongholdStairs2) par1StructureComponent,
					par2List, par3Random, 1, 2);
		}
	}

	public static ComponentStrongholdStraight findValidPlacement(
			final List par0List, final Random par1Random, final int par2,
			final int par3, final int par4, final int par5, final int par6) {
		final StructureBoundingBox var7 = StructureBoundingBox
				.getComponentToAddBoundingBox(par2, par3, par4, -1, -1, 0, 5,
						5, 7, par5);
		return ComponentStronghold.canStrongholdGoDeeper(var7)
				&& StructureComponent.findIntersecting(par0List, var7) == null ? new ComponentStrongholdStraight(
				par6, par1Random, var7, par5) : null;
	}

	/**
	 * second Part of Structure generating, this for example places Spiderwebs,
	 * Mob Spawners, it closes Mineshafts at the end, it adds Fences...
	 */
	@Override
	public boolean addComponentParts(final World par1World,
			final Random par2Random,
			final StructureBoundingBox par3StructureBoundingBox) {
		if (isLiquidInStructureBoundingBox(par1World, par3StructureBoundingBox)) {
			return false;
		} else {
			fillWithRandomizedBlocks(par1World, par3StructureBoundingBox, 0, 0,
					0, 4, 4, 6, true, par2Random,
					StructureStrongholdPieces.getStrongholdStones());
			placeDoor(par1World, par2Random, par3StructureBoundingBox,
					doorType, 1, 1, 0);
			placeDoor(par1World, par2Random, par3StructureBoundingBox,
					EnumDoor.OPENING, 1, 1, 6);
			randomlyPlaceBlock(par1World, par3StructureBoundingBox, par2Random,
					0.1F, 1, 2, 1, Block.torchWood.blockID, 0);
			randomlyPlaceBlock(par1World, par3StructureBoundingBox, par2Random,
					0.1F, 3, 2, 1, Block.torchWood.blockID, 0);
			randomlyPlaceBlock(par1World, par3StructureBoundingBox, par2Random,
					0.1F, 1, 2, 5, Block.torchWood.blockID, 0);
			randomlyPlaceBlock(par1World, par3StructureBoundingBox, par2Random,
					0.1F, 3, 2, 5, Block.torchWood.blockID, 0);

			if (expandsX) {
				fillWithBlocks(par1World, par3StructureBoundingBox, 0, 1, 2, 0,
						3, 4, 0, 0, false);
			}

			if (expandsZ) {
				fillWithBlocks(par1World, par3StructureBoundingBox, 4, 1, 2, 4,
						3, 4, 0, 0, false);
			}

			return true;
		}
	}
}
