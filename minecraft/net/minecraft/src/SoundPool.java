package net.minecraft.src;

import java.io.File;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class SoundPool {
	/** The RNG used by SoundPool. */
	private final Random rand = new Random();

	/**
	 * Maps a name (can be sound/newsound/streaming/music/newmusic) to a list of
	 * SoundPoolEntry's.
	 */
	private final Map nameToSoundPoolEntriesMapping = new HashMap();

	/** A list of all SoundPoolEntries that have been loaded. */
	private final List allSoundPoolEntries = new ArrayList();

	/**
	 * The number of soundPoolEntry's. This value is computed but never used
	 * (should be equal to allSoundPoolEntries.size()).
	 */
	public int numberOfSoundPoolEntries = 0;
	public boolean isGetRandomSound = true;

	/**
	 * Adds a sound to this sound pool.
	 */
	public SoundPoolEntry addSound(String par1Str, final File par2File) {
		try {
			final String var3 = par1Str;
			par1Str = par1Str.substring(0, par1Str.indexOf("."));

			if (isGetRandomSound) {
				while (Character.isDigit(par1Str.charAt(par1Str.length() - 1))) {
					par1Str = par1Str.substring(0, par1Str.length() - 1);
				}
			}

			par1Str = par1Str.replaceAll("/", ".");

			if (!nameToSoundPoolEntriesMapping.containsKey(par1Str)) {
				nameToSoundPoolEntriesMapping.put(par1Str, new ArrayList());
			}

			final SoundPoolEntry var4 = new SoundPoolEntry(var3, par2File
					.toURI().toURL());
			((List) nameToSoundPoolEntriesMapping.get(par1Str)).add(var4);
			allSoundPoolEntries.add(var4);
			++numberOfSoundPoolEntries;
			return var4;
		} catch (final MalformedURLException var5) {
			var5.printStackTrace();
			throw new RuntimeException(var5);
		}
	}

	/**
	 * gets a random sound from the specified (by name, can be
	 * sound/newsound/streaming/music/newmusic) sound pool.
	 */
	public SoundPoolEntry getRandomSoundFromSoundPool(final String par1Str) {
		final List var2 = (List) nameToSoundPoolEntriesMapping.get(par1Str);
		return var2 == null ? null : (SoundPoolEntry) var2.get(rand
				.nextInt(var2.size()));
	}

	/**
	 * Gets a random SoundPoolEntry.
	 */
	public SoundPoolEntry getRandomSound() {
		return allSoundPoolEntries.isEmpty() ? null
				: (SoundPoolEntry) allSoundPoolEntries.get(rand
						.nextInt(allSoundPoolEntries.size()));
	}
}
