package net.minecraft.src;

import java.util.List;
import java.util.Random;

public class BlockPistonExtension extends Block {
	/** The texture for the 'head' of the piston. Sticky or normal. */
	private Icon headTexture = null;

	public BlockPistonExtension(final int par1) {
		super(par1, Material.piston);
		setStepSound(Block.soundStoneFootstep);
		setHardness(0.5F);
	}

	public void setHeadTexture(final Icon par1Icon) {
		headTexture = par1Icon;
	}

	public void clearHeadTexture() {
		headTexture = null;
	}

	/**
	 * ejects contained items into the world, and notifies neighbours of an
	 * update, as appropriate
	 */
	@Override
	public void breakBlock(final World par1World, int par2, int par3, int par4,
			final int par5, int par6) {
		super.breakBlock(par1World, par2, par3, par4, par5, par6);
		final int var7 = Facing.oppositeSide[BlockPistonExtension
				.getDirectionMeta(par6)];
		par2 += Facing.offsetsXForSide[var7];
		par3 += Facing.offsetsYForSide[var7];
		par4 += Facing.offsetsZForSide[var7];
		final int var8 = par1World.getBlockId(par2, par3, par4);

		if (var8 == Block.pistonBase.blockID
				|| var8 == Block.pistonStickyBase.blockID) {
			par6 = par1World.getBlockMetadata(par2, par3, par4);

			if (BlockPistonBase.isExtended(par6)) {
				Block.blocksList[var8].dropBlockAsItem(par1World, par2, par3,
						par4, par6, 0);
				par1World.setBlockToAir(par2, par3, par4);
			}
		}
	}

	/**
	 * From the specified side and block metadata retrieves the blocks texture.
	 * Args: side, metadata
	 */
	@Override
	public Icon getIcon(final int par1, final int par2) {
		final int var3 = BlockPistonExtension.getDirectionMeta(par2);
		return par1 == var3 ? headTexture != null ? headTexture
				: (par2 & 8) != 0 ? BlockPistonBase
						.func_94496_b("piston_top_sticky") : BlockPistonBase
						.func_94496_b("piston_top") : var3 < 6
				&& par1 == Facing.oppositeSide[var3] ? BlockPistonBase
				.func_94496_b("piston_top") : BlockPistonBase
				.func_94496_b("piston_side");
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return 17;
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	/**
	 * Checks to see if its valid to put this block at the specified
	 * coordinates. Args: world, x, y, z
	 */
	@Override
	public boolean canPlaceBlockAt(final World par1World, final int par2,
			final int par3, final int par4) {
		return false;
	}

	/**
	 * checks to see if you can place this block can be placed on that side of a
	 * block: BlockLever overrides
	 */
	@Override
	public boolean canPlaceBlockOnSide(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		return false;
	}

	/**
	 * Returns the quantity of items to drop on block destruction.
	 */
	@Override
	public int quantityDropped(final Random par1Random) {
		return 0;
	}

	/**
	 * Adds all intersecting collision boxes to a list. (Be sure to only add
	 * boxes to the list if they intersect the mask.) Parameters: World, X, Y,
	 * Z, mask, list, colliding entity
	 */
	@Override
	public void addCollisionBoxesToList(final World par1World, final int par2,
			final int par3, final int par4,
			final AxisAlignedBB par5AxisAlignedBB, final List par6List,
			final Entity par7Entity) {
		final int var8 = par1World.getBlockMetadata(par2, par3, par4);

		switch (BlockPistonExtension.getDirectionMeta(var8)) {
		case 0:
			setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.25F, 1.0F);
			super.addCollisionBoxesToList(par1World, par2, par3, par4,
					par5AxisAlignedBB, par6List, par7Entity);
			setBlockBounds(0.375F, 0.25F, 0.375F, 0.625F, 1.0F, 0.625F);
			super.addCollisionBoxesToList(par1World, par2, par3, par4,
					par5AxisAlignedBB, par6List, par7Entity);
			break;

		case 1:
			setBlockBounds(0.0F, 0.75F, 0.0F, 1.0F, 1.0F, 1.0F);
			super.addCollisionBoxesToList(par1World, par2, par3, par4,
					par5AxisAlignedBB, par6List, par7Entity);
			setBlockBounds(0.375F, 0.0F, 0.375F, 0.625F, 0.75F, 0.625F);
			super.addCollisionBoxesToList(par1World, par2, par3, par4,
					par5AxisAlignedBB, par6List, par7Entity);
			break;

		case 2:
			setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 0.25F);
			super.addCollisionBoxesToList(par1World, par2, par3, par4,
					par5AxisAlignedBB, par6List, par7Entity);
			setBlockBounds(0.25F, 0.375F, 0.25F, 0.75F, 0.625F, 1.0F);
			super.addCollisionBoxesToList(par1World, par2, par3, par4,
					par5AxisAlignedBB, par6List, par7Entity);
			break;

		case 3:
			setBlockBounds(0.0F, 0.0F, 0.75F, 1.0F, 1.0F, 1.0F);
			super.addCollisionBoxesToList(par1World, par2, par3, par4,
					par5AxisAlignedBB, par6List, par7Entity);
			setBlockBounds(0.25F, 0.375F, 0.0F, 0.75F, 0.625F, 0.75F);
			super.addCollisionBoxesToList(par1World, par2, par3, par4,
					par5AxisAlignedBB, par6List, par7Entity);
			break;

		case 4:
			setBlockBounds(0.0F, 0.0F, 0.0F, 0.25F, 1.0F, 1.0F);
			super.addCollisionBoxesToList(par1World, par2, par3, par4,
					par5AxisAlignedBB, par6List, par7Entity);
			setBlockBounds(0.375F, 0.25F, 0.25F, 0.625F, 0.75F, 1.0F);
			super.addCollisionBoxesToList(par1World, par2, par3, par4,
					par5AxisAlignedBB, par6List, par7Entity);
			break;

		case 5:
			setBlockBounds(0.75F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
			super.addCollisionBoxesToList(par1World, par2, par3, par4,
					par5AxisAlignedBB, par6List, par7Entity);
			setBlockBounds(0.0F, 0.375F, 0.25F, 0.75F, 0.625F, 0.75F);
			super.addCollisionBoxesToList(par1World, par2, par3, par4,
					par5AxisAlignedBB, par6List, par7Entity);
		}

		setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
	}

	/**
	 * Updates the blocks bounds based on its current state. Args: world, x, y,
	 * z
	 */
	@Override
	public void setBlockBoundsBasedOnState(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		final int var5 = par1IBlockAccess.getBlockMetadata(par2, par3, par4);

		switch (BlockPistonExtension.getDirectionMeta(var5)) {
		case 0:
			setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.25F, 1.0F);
			break;

		case 1:
			setBlockBounds(0.0F, 0.75F, 0.0F, 1.0F, 1.0F, 1.0F);
			break;

		case 2:
			setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 0.25F);
			break;

		case 3:
			setBlockBounds(0.0F, 0.0F, 0.75F, 1.0F, 1.0F, 1.0F);
			break;

		case 4:
			setBlockBounds(0.0F, 0.0F, 0.0F, 0.25F, 1.0F, 1.0F);
			break;

		case 5:
			setBlockBounds(0.75F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
		}
	}

	/**
	 * Lets the block know when one of its neighbor changes. Doesn't know which
	 * neighbor changed (coordinates passed are their own) Args: x, y, z,
	 * neighbor blockID
	 */
	@Override
	public void onNeighborBlockChange(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		final int var6 = BlockPistonExtension.getDirectionMeta(par1World
				.getBlockMetadata(par2, par3, par4));
		final int var7 = par1World.getBlockId(par2
				- Facing.offsetsXForSide[var6], par3
				- Facing.offsetsYForSide[var6], par4
				- Facing.offsetsZForSide[var6]);

		if (var7 != Block.pistonBase.blockID
				&& var7 != Block.pistonStickyBase.blockID) {
			par1World.setBlockToAir(par2, par3, par4);
		} else {
			Block.blocksList[var7].onNeighborBlockChange(par1World, par2
					- Facing.offsetsXForSide[var6], par3
					- Facing.offsetsYForSide[var6], par4
					- Facing.offsetsZForSide[var6], par5);
		}
	}

	public static int getDirectionMeta(final int par0) {
		return par0 & 7;
	}

	/**
	 * only called by clickMiddleMouseButton , and passed to
	 * inventory.setCurrentItem (along with isCreative)
	 */
	@Override
	public int idPicked(final World par1World, final int par2, final int par3,
			final int par4) {
		return 0;
	}
}
