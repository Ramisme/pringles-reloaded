package net.minecraft.src;

import java.util.concurrent.Callable;

class CallableLvl3 implements Callable {
	/** Reference to the World object. */
	final World theWorld;

	CallableLvl3(final World par1World) {
		theWorld = par1World;
	}

	/**
	 * Returns the result of the ChunkProvider's makeString
	 */
	public String getChunkProvider() {
		return theWorld.chunkProvider.makeString();
	}

	@Override
	public Object call() {
		return getChunkProvider();
	}
}
