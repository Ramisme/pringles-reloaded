package net.minecraft.src;

public class ModelBook extends ModelBase {
	/** Right cover renderer (when facing the book) */
	public ModelRenderer coverRight = new ModelRenderer(this).setTextureOffset(
			0, 0).addBox(-6.0F, -5.0F, 0.0F, 6, 10, 0);

	/** Left cover renderer (when facing the book) */
	public ModelRenderer coverLeft = new ModelRenderer(this).setTextureOffset(
			16, 0).addBox(0.0F, -5.0F, 0.0F, 6, 10, 0);

	/** The right pages renderer (when facing the book) */
	public ModelRenderer pagesRight = new ModelRenderer(this).setTextureOffset(
			0, 10).addBox(0.0F, -4.0F, -0.99F, 5, 8, 1);

	/** The left pages renderer (when facing the book) */
	public ModelRenderer pagesLeft = new ModelRenderer(this).setTextureOffset(
			12, 10).addBox(0.0F, -4.0F, -0.01F, 5, 8, 1);

	/** Right cover renderer (when facing the book) */
	public ModelRenderer flippingPageRight = new ModelRenderer(this)
			.setTextureOffset(24, 10).addBox(0.0F, -4.0F, 0.0F, 5, 8, 0);

	/** Right cover renderer (when facing the book) */
	public ModelRenderer flippingPageLeft = new ModelRenderer(this)
			.setTextureOffset(24, 10).addBox(0.0F, -4.0F, 0.0F, 5, 8, 0);

	/** The renderer of spine of the book */
	public ModelRenderer bookSpine = new ModelRenderer(this).setTextureOffset(
			12, 0).addBox(-1.0F, -5.0F, 0.0F, 2, 10, 0);

	public ModelBook() {
		coverRight.setRotationPoint(0.0F, 0.0F, -1.0F);
		coverLeft.setRotationPoint(0.0F, 0.0F, 1.0F);
		bookSpine.rotateAngleY = (float) Math.PI / 2F;
	}

	/**
	 * Sets the models various rotation angles then renders the model.
	 */
	@Override
	public void render(final Entity par1Entity, final float par2,
			final float par3, final float par4, final float par5,
			final float par6, final float par7) {
		setRotationAngles(par2, par3, par4, par5, par6, par7, par1Entity);
		coverRight.render(par7);
		coverLeft.render(par7);
		bookSpine.render(par7);
		pagesRight.render(par7);
		pagesLeft.render(par7);
		flippingPageRight.render(par7);
		flippingPageLeft.render(par7);
	}

	/**
	 * Sets the model's various rotation angles. For bipeds, par1 and par2 are
	 * used for animating the movement of arms and legs, where par1 represents
	 * the time(so that arms and legs swing back and forth) and par2 represents
	 * how "far" arms and legs can swing at most.
	 */
	@Override
	public void setRotationAngles(final float par1, final float par2,
			final float par3, final float par4, final float par5,
			final float par6, final Entity par7Entity) {
		final float var8 = (MathHelper.sin(par1 * 0.02F) * 0.1F + 1.25F) * par4;
		coverRight.rotateAngleY = (float) Math.PI + var8;
		coverLeft.rotateAngleY = -var8;
		pagesRight.rotateAngleY = var8;
		pagesLeft.rotateAngleY = -var8;
		flippingPageRight.rotateAngleY = var8 - var8 * 2.0F * par2;
		flippingPageLeft.rotateAngleY = var8 - var8 * 2.0F * par3;
		pagesRight.rotationPointX = MathHelper.sin(var8);
		pagesLeft.rotationPointX = MathHelper.sin(var8);
		flippingPageRight.rotationPointX = MathHelper.sin(var8);
		flippingPageLeft.rotationPointX = MathHelper.sin(var8);
	}
}
