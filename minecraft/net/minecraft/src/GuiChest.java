package net.minecraft.src;

import org.lwjgl.opengl.GL11;

public class GuiChest extends GuiContainer {
	private final IInventory upperChestInventory;
	private final IInventory lowerChestInventory;

	/**
	 * window height is calculated with this values, the more rows, the heigher
	 */
	private int inventoryRows = 0;

	public GuiChest(final IInventory par1IInventory,
			final IInventory par2IInventory) {
		super(new ContainerChest(par1IInventory, par2IInventory));
		upperChestInventory = par1IInventory;
		lowerChestInventory = par2IInventory;
		allowUserInput = false;
		final short var3 = 222;
		final int var4 = var3 - 108;
		inventoryRows = par2IInventory.getSizeInventory() / 9;
		ySize = var4 + inventoryRows * 18;
	}

	/**
	 * Draw the foreground layer for the GuiContainer (everything in front of
	 * the items)
	 */
	@Override
	protected void drawGuiContainerForegroundLayer(final int par1,
			final int par2) {
		fontRenderer.drawString(
				lowerChestInventory.isInvNameLocalized() ? lowerChestInventory
						.getInvName() : StatCollector
						.translateToLocal(lowerChestInventory.getInvName()), 8,
				6, 4210752);
		fontRenderer.drawString(
				upperChestInventory.isInvNameLocalized() ? upperChestInventory
						.getInvName() : StatCollector
						.translateToLocal(upperChestInventory.getInvName()), 8,
				ySize - 96 + 2, 4210752);
	}

	/**
	 * Draw the background layer for the GuiContainer (everything behind the
	 * items)
	 */
	@Override
	protected void drawGuiContainerBackgroundLayer(final float par1,
			final int par2, final int par3) {
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		mc.renderEngine.bindTexture("/gui/container.png");
		final int var4 = (width - xSize) / 2;
		final int var5 = (height - ySize) / 2;
		drawTexturedModalRect(var4, var5, 0, 0, xSize, inventoryRows * 18 + 17);
		drawTexturedModalRect(var4, var5 + inventoryRows * 18 + 17, 0, 126,
				xSize, 96);
	}
}
