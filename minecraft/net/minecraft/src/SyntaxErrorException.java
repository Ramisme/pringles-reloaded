package net.minecraft.src;

public class SyntaxErrorException extends CommandException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6604852767644194325L;

	public SyntaxErrorException() {
		this("commands.generic.snytax", new Object[0]);
	}

	public SyntaxErrorException(final String par1Str,
			final Object... par2ArrayOfObj) {
		super(par1Str, par2ArrayOfObj);
	}
}
