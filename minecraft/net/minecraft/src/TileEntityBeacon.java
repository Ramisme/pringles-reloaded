package net.minecraft.src;

import java.util.Iterator;
import java.util.List;

public class TileEntityBeacon extends TileEntity implements IInventory {
	/** List of effects that Beacon can apply */
	public static final Potion[][] effectsList = new Potion[][] {
			{ Potion.moveSpeed, Potion.digSpeed },
			{ Potion.resistance, Potion.jump }, { Potion.damageBoost },
			{ Potion.regeneration } };
	private long field_82137_b;
	private float field_82138_c;
	private boolean isBeaconActive;

	/** Level of this beacon's pyramid. */
	private int levels = -1;

	/** Primary potion effect given by this beacon. */
	private int primaryEffect;

	/** Secondary potion effect given by this beacon. */
	private int secondaryEffect;

	/** Item given to this beacon as payment. */
	private ItemStack payment;
	private String field_94048_i;

	/**
	 * Allows the entity to update its state. Overridden in most subclasses,
	 * e.g. the mob spawner uses this to count ticks and creates a new spawn
	 * inside its implementation.
	 */
	@Override
	public void updateEntity() {
		if (worldObj.getTotalWorldTime() % 80L == 0L) {
			updateState();
			addEffectsToPlayers();
		}
	}

	private void addEffectsToPlayers() {
		if (isBeaconActive && levels > 0 && !worldObj.isRemote
				&& primaryEffect > 0) {
			final double var1 = levels * 10 + 10;
			byte var3 = 0;

			if (levels >= 4 && primaryEffect == secondaryEffect) {
				var3 = 1;
			}

			final AxisAlignedBB var4 = AxisAlignedBB
					.getAABBPool()
					.getAABB(xCoord, yCoord, zCoord, xCoord + 1, yCoord + 1,
							zCoord + 1).expand(var1, var1, var1);
			var4.maxY = worldObj.getHeight();
			final List var5 = worldObj.getEntitiesWithinAABB(
					EntityPlayer.class, var4);
			Iterator var6 = var5.iterator();
			EntityPlayer var7;

			while (var6.hasNext()) {
				var7 = (EntityPlayer) var6.next();
				var7.addPotionEffect(new PotionEffect(primaryEffect, 180, var3,
						true));
			}

			if (levels >= 4 && primaryEffect != secondaryEffect
					&& secondaryEffect > 0) {
				var6 = var5.iterator();

				while (var6.hasNext()) {
					var7 = (EntityPlayer) var6.next();
					var7.addPotionEffect(new PotionEffect(secondaryEffect, 180,
							0, true));
				}
			}
		}
	}

	/**
	 * Checks if the Beacon has a valid pyramid underneath and direct sunlight
	 * above
	 */
	private void updateState() {
		if (!worldObj.canBlockSeeTheSky(xCoord, yCoord + 1, zCoord)) {
			isBeaconActive = false;
			levels = 0;
		} else {
			isBeaconActive = true;
			levels = 0;

			for (int var1 = 1; var1 <= 4; levels = var1++) {
				final int var2 = yCoord - var1;

				if (var2 < 0) {
					break;
				}

				boolean var3 = true;

				for (int var4 = xCoord - var1; var4 <= xCoord + var1 && var3; ++var4) {
					for (int var5 = zCoord - var1; var5 <= zCoord + var1; ++var5) {
						final int var6 = worldObj.getBlockId(var4, var2, var5);

						if (var6 != Block.blockEmerald.blockID
								&& var6 != Block.blockGold.blockID
								&& var6 != Block.blockDiamond.blockID
								&& var6 != Block.blockIron.blockID) {
							var3 = false;
							break;
						}
					}
				}

				if (!var3) {
					break;
				}
			}

			if (levels == 0) {
				isBeaconActive = false;
			}
		}
	}

	public float func_82125_v_() {
		if (!isBeaconActive) {
			return 0.0F;
		} else {
			final int var1 = (int) (worldObj.getTotalWorldTime() - field_82137_b);
			field_82137_b = worldObj.getTotalWorldTime();

			if (var1 > 1) {
				field_82138_c -= var1 / 40.0F;

				if (field_82138_c < 0.0F) {
					field_82138_c = 0.0F;
				}
			}

			field_82138_c += 0.025F;

			if (field_82138_c > 1.0F) {
				field_82138_c = 1.0F;
			}

			return field_82138_c;
		}
	}

	/**
	 * Return the primary potion effect given by this beacon.
	 */
	public int getPrimaryEffect() {
		return primaryEffect;
	}

	/**
	 * Return the secondary potion effect given by this beacon.
	 */
	public int getSecondaryEffect() {
		return secondaryEffect;
	}

	/**
	 * Return the levels of this beacon's pyramid.
	 */
	public int getLevels() {
		return levels;
	}

	/**
	 * Set the levels of this beacon's pyramid.
	 */
	public void setLevels(final int par1) {
		levels = par1;
	}

	public void setPrimaryEffect(final int par1) {
		primaryEffect = 0;

		for (int var2 = 0; var2 < levels && var2 < 3; ++var2) {
			final Potion[] var3 = TileEntityBeacon.effectsList[var2];
			final int var4 = var3.length;

			for (int var5 = 0; var5 < var4; ++var5) {
				final Potion var6 = var3[var5];

				if (var6.id == par1) {
					primaryEffect = par1;
					return;
				}
			}
		}
	}

	public void setSecondaryEffect(final int par1) {
		secondaryEffect = 0;

		if (levels >= 4) {
			for (int var2 = 0; var2 < 4; ++var2) {
				final Potion[] var3 = TileEntityBeacon.effectsList[var2];
				final int var4 = var3.length;

				for (int var5 = 0; var5 < var4; ++var5) {
					final Potion var6 = var3[var5];

					if (var6.id == par1) {
						secondaryEffect = par1;
						return;
					}
				}
			}
		}
	}

	/**
	 * Overriden in a sign to provide the text.
	 */
	@Override
	public Packet getDescriptionPacket() {
		final NBTTagCompound var1 = new NBTTagCompound();
		writeToNBT(var1);
		return new Packet132TileEntityData(xCoord, yCoord, zCoord, 3, var1);
	}

	@Override
	public double getMaxRenderDistanceSquared() {
		return 65536.0D;
	}

	/**
	 * Reads a tile entity from NBT.
	 */
	@Override
	public void readFromNBT(final NBTTagCompound par1NBTTagCompound) {
		super.readFromNBT(par1NBTTagCompound);
		primaryEffect = par1NBTTagCompound.getInteger("Primary");
		secondaryEffect = par1NBTTagCompound.getInteger("Secondary");
		levels = par1NBTTagCompound.getInteger("Levels");
	}

	/**
	 * Writes a tile entity to NBT.
	 */
	@Override
	public void writeToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeToNBT(par1NBTTagCompound);
		par1NBTTagCompound.setInteger("Primary", primaryEffect);
		par1NBTTagCompound.setInteger("Secondary", secondaryEffect);
		par1NBTTagCompound.setInteger("Levels", levels);
	}

	/**
	 * Returns the number of slots in the inventory.
	 */
	@Override
	public int getSizeInventory() {
		return 1;
	}

	/**
	 * Returns the stack in slot i
	 */
	@Override
	public ItemStack getStackInSlot(final int par1) {
		return par1 == 0 ? payment : null;
	}

	/**
	 * Removes from an inventory slot (first arg) up to a specified number
	 * (second arg) of items and returns them in a new stack.
	 */
	@Override
	public ItemStack decrStackSize(final int par1, final int par2) {
		if (par1 == 0 && payment != null) {
			if (par2 >= payment.stackSize) {
				final ItemStack var3 = payment;
				payment = null;
				return var3;
			} else {
				payment.stackSize -= par2;
				return new ItemStack(payment.itemID, par2,
						payment.getItemDamage());
			}
		} else {
			return null;
		}
	}

	/**
	 * When some containers are closed they call this on each slot, then drop
	 * whatever it returns as an EntityItem - like when you close a workbench
	 * GUI.
	 */
	@Override
	public ItemStack getStackInSlotOnClosing(final int par1) {
		if (par1 == 0 && payment != null) {
			final ItemStack var2 = payment;
			payment = null;
			return var2;
		} else {
			return null;
		}
	}

	/**
	 * Sets the given item stack to the specified slot in the inventory (can be
	 * crafting or armor sections).
	 */
	@Override
	public void setInventorySlotContents(final int par1,
			final ItemStack par2ItemStack) {
		if (par1 == 0) {
			payment = par2ItemStack;
		}
	}

	/**
	 * Returns the name of the inventory.
	 */
	@Override
	public String getInvName() {
		return isInvNameLocalized() ? field_94048_i : "container.beacon";
	}

	/**
	 * If this returns false, the inventory name will be used as an unlocalized
	 * name, and translated into the player's language. Otherwise it will be
	 * used directly.
	 */
	@Override
	public boolean isInvNameLocalized() {
		return field_94048_i != null && field_94048_i.length() > 0;
	}

	public void func_94047_a(final String par1Str) {
		field_94048_i = par1Str;
	}

	/**
	 * Returns the maximum stack size for a inventory slot. Seems to always be
	 * 64, possibly will be extended. *Isn't this more of a set than a get?*
	 */
	@Override
	public int getInventoryStackLimit() {
		return 1;
	}

	/**
	 * Do not make give this method the name canInteractWith because it clashes
	 * with Container
	 */
	@Override
	public boolean isUseableByPlayer(final EntityPlayer par1EntityPlayer) {
		return worldObj.getBlockTileEntity(xCoord, yCoord, zCoord) != this ? false
				: par1EntityPlayer.getDistanceSq(xCoord + 0.5D, yCoord + 0.5D,
						zCoord + 0.5D) <= 64.0D;
	}

	@Override
	public void openChest() {
	}

	@Override
	public void closeChest() {
	}

	/**
	 * Returns true if automation is allowed to insert the given stack (ignoring
	 * stack size) into the given slot.
	 */
	@Override
	public boolean isStackValidForSlot(final int par1,
			final ItemStack par2ItemStack) {
		return par2ItemStack.itemID == Item.emerald.itemID
				|| par2ItemStack.itemID == Item.diamond.itemID
				|| par2ItemStack.itemID == Item.ingotGold.itemID
				|| par2ItemStack.itemID == Item.ingotIron.itemID;
	}
}
