package net.minecraft.src;

public class EnchantmentUntouching extends Enchantment {
	protected EnchantmentUntouching(final int par1, final int par2) {
		super(par1, par2, EnumEnchantmentType.digger);
		setName("untouching");
	}

	/**
	 * Returns the minimal value of enchantability needed on the enchantment
	 * level passed.
	 */
	@Override
	public int getMinEnchantability(final int par1) {
		return 15;
	}

	/**
	 * Returns the maximum value of enchantability nedded on the enchantment
	 * level passed.
	 */
	@Override
	public int getMaxEnchantability(final int par1) {
		return super.getMinEnchantability(par1) + 50;
	}

	/**
	 * Returns the maximum level that the enchantment can have.
	 */
	@Override
	public int getMaxLevel() {
		return 1;
	}

	/**
	 * Determines if the enchantment passed can be applyied together with this
	 * enchantment.
	 */
	@Override
	public boolean canApplyTogether(final Enchantment par1Enchantment) {
		return super.canApplyTogether(par1Enchantment)
				&& par1Enchantment.effectId != Enchantment.fortune.effectId;
	}

	@Override
	public boolean canApply(final ItemStack par1ItemStack) {
		return par1ItemStack.getItem().itemID == Item.shears.itemID ? true
				: super.canApply(par1ItemStack);
	}
}
