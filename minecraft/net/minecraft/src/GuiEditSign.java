package net.minecraft.src;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

public class GuiEditSign extends GuiScreen {
	/**
	 * This String is just a local copy of the characters allowed in text
	 * rendering of minecraft.
	 */
	private static final String allowedCharacters = ChatAllowedCharacters.allowedCharacters;

	/** The title string that is displayed in the top-center of the screen. */
	protected String screenTitle = "Edit sign message:";

	/** Reference to the sign object. */
	private final TileEntitySign entitySign;

	/** Counts the number of screen updates. */
	private int updateCounter;

	/** The number of the line that is being edited. */
	private int editLine = 0;

	/** "Done" button for the GUI. */
	private GuiButton doneBtn;

	public GuiEditSign(final TileEntitySign par1TileEntitySign) {
		entitySign = par1TileEntitySign;
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui() {
		buttonList.clear();
		Keyboard.enableRepeatEvents(true);
		buttonList.add(doneBtn = new GuiButton(0, width / 2 - 100,
				height / 4 + 120, "Done"));
		entitySign.setEditable(false);
	}

	/**
	 * Called when the screen is unloaded. Used to disable keyboard repeat
	 * events
	 */
	@Override
	public void onGuiClosed() {
		Keyboard.enableRepeatEvents(false);
		final NetClientHandler var1 = mc.getNetHandler();

		if (var1 != null) {
			var1.addToSendQueue(new Packet130UpdateSign(entitySign.xCoord,
					entitySign.yCoord, entitySign.zCoord, entitySign.signText));
		}

		entitySign.setEditable(true);
	}

	/**
	 * Called from the main game loop to update the screen.
	 */
	@Override
	public void updateScreen() {
		++updateCounter;
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	@Override
	protected void actionPerformed(final GuiButton par1GuiButton) {
		if (par1GuiButton.enabled) {
			if (par1GuiButton.id == 0) {
				entitySign.onInventoryChanged();
				mc.displayGuiScreen((GuiScreen) null);
			}
		}
	}

	/**
	 * Fired when a key is typed. This is the equivalent of
	 * KeyListener.keyTyped(KeyEvent e).
	 */
	@Override
	protected void keyTyped(final char par1, final int par2) {
		if (par2 == 200) {
			editLine = editLine - 1 & 3;
		}

		if (par2 == 208 || par2 == 28) {
			editLine = editLine + 1 & 3;
		}

		if (par2 == 14 && entitySign.signText[editLine].length() > 0) {
			entitySign.signText[editLine] = entitySign.signText[editLine]
					.substring(0, entitySign.signText[editLine].length() - 1);
		}

		if (GuiEditSign.allowedCharacters.indexOf(par1) >= 0
				&& entitySign.signText[editLine].length() < 15) {
			entitySign.signText[editLine] = entitySign.signText[editLine]
					+ par1;
		}

		if (par2 == 1) {
			actionPerformed(doneBtn);
		}
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(final int par1, final int par2, final float par3) {
		drawDefaultBackground();
		drawCenteredString(fontRenderer, screenTitle, width / 2, 40, 16777215);
		GL11.glPushMatrix();
		GL11.glTranslatef(width / 2, 0.0F, 50.0F);
		final float var4 = 93.75F;
		GL11.glScalef(-var4, -var4, -var4);
		GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
		final Block var5 = entitySign.getBlockType();

		if (var5 == Block.signPost) {
			final float var6 = entitySign.getBlockMetadata() * 360 / 16.0F;
			GL11.glRotatef(var6, 0.0F, 1.0F, 0.0F);
			GL11.glTranslatef(0.0F, -1.0625F, 0.0F);
		} else {
			final int var8 = entitySign.getBlockMetadata();
			float var7 = 0.0F;

			if (var8 == 2) {
				var7 = 180.0F;
			}

			if (var8 == 4) {
				var7 = 90.0F;
			}

			if (var8 == 5) {
				var7 = -90.0F;
			}

			GL11.glRotatef(var7, 0.0F, 1.0F, 0.0F);
			GL11.glTranslatef(0.0F, -1.0625F, 0.0F);
		}

		if (updateCounter / 6 % 2 == 0) {
			entitySign.lineBeingEdited = editLine;
		}

		TileEntityRenderer.instance.renderTileEntityAt(entitySign, -0.5D,
				-0.75D, -0.5D, 0.0F);
		entitySign.lineBeingEdited = -1;
		GL11.glPopMatrix();
		super.drawScreen(par1, par2, par3);
	}
}
