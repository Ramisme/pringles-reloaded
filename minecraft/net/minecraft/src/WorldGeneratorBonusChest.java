package net.minecraft.src;

import java.util.Random;

public class WorldGeneratorBonusChest extends WorldGenerator {
	/**
	 * Instance of WeightedRandomChestContent what will randomly generate items
	 * into the Bonus Chest.
	 */
	private final WeightedRandomChestContent[] theBonusChestGenerator;

	/**
	 * Value of this int will determine how much items gonna generate in Bonus
	 * Chest.
	 */
	private final int itemsToGenerateInBonusChest;

	public WorldGeneratorBonusChest(
			final WeightedRandomChestContent[] par1ArrayOfWeightedRandomChestContent,
			final int par2) {
		theBonusChestGenerator = par1ArrayOfWeightedRandomChestContent;
		itemsToGenerateInBonusChest = par2;
	}

	@Override
	public boolean generate(final World par1World, final Random par2Random,
			final int par3, int par4, final int par5) {
		int var12;

		for (; ((var12 = par1World.getBlockId(par3, par4, par5)) == 0 || var12 == Block.leaves.blockID)
				&& par4 > 1; --par4) {
			;
		}

		if (par4 < 1) {
			return false;
		} else {
			++par4;

			for (int var7 = 0; var7 < 4; ++var7) {
				final int var8 = par3 + par2Random.nextInt(4)
						- par2Random.nextInt(4);
				final int var9 = par4 + par2Random.nextInt(3)
						- par2Random.nextInt(3);
				final int var10 = par5 + par2Random.nextInt(4)
						- par2Random.nextInt(4);

				if (par1World.isAirBlock(var8, var9, var10)
						&& par1World.doesBlockHaveSolidTopSurface(var8,
								var9 - 1, var10)) {
					par1World.setBlock(var8, var9, var10, Block.chest.blockID,
							0, 2);
					final TileEntityChest var11 = (TileEntityChest) par1World
							.getBlockTileEntity(var8, var9, var10);

					if (var11 != null && var11 != null) {
						WeightedRandomChestContent.generateChestContents(
								par2Random, theBonusChestGenerator, var11,
								itemsToGenerateInBonusChest);
					}

					if (par1World.isAirBlock(var8 - 1, var9, var10)
							&& par1World.doesBlockHaveSolidTopSurface(var8 - 1,
									var9 - 1, var10)) {
						par1World.setBlock(var8 - 1, var9, var10,
								Block.torchWood.blockID, 0, 2);
					}

					if (par1World.isAirBlock(var8 + 1, var9, var10)
							&& par1World.doesBlockHaveSolidTopSurface(var8 - 1,
									var9 - 1, var10)) {
						par1World.setBlock(var8 + 1, var9, var10,
								Block.torchWood.blockID, 0, 2);
					}

					if (par1World.isAirBlock(var8, var9, var10 - 1)
							&& par1World.doesBlockHaveSolidTopSurface(var8 - 1,
									var9 - 1, var10)) {
						par1World.setBlock(var8, var9, var10 - 1,
								Block.torchWood.blockID, 0, 2);
					}

					if (par1World.isAirBlock(var8, var9, var10 + 1)
							&& par1World.doesBlockHaveSolidTopSurface(var8 - 1,
									var9 - 1, var10)) {
						par1World.setBlock(var8, var9, var10 + 1,
								Block.torchWood.blockID, 0, 2);
					}

					return true;
				}
			}

			return false;
		}
	}
}
