package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import net.minecraft.server.MinecraftServer;

public class SaveHandler implements ISaveHandler, IPlayerFileData {
	/** The directory in which to save world data. */
	private final File worldDirectory;

	/** The directory in which to save player data. */
	private final File playersDirectory;
	private final File mapDataDir;

	/**
	 * The time in milliseconds when this field was initialized. Stored in the
	 * session lock file.
	 */
	private final long initializationTime = System.currentTimeMillis();

	/** The directory name of the world */
	private final String saveDirectoryName;

	public SaveHandler(final File par1File, final String par2Str,
			final boolean par3) {
		worldDirectory = new File(par1File, par2Str);
		worldDirectory.mkdirs();
		playersDirectory = new File(worldDirectory, "players");
		mapDataDir = new File(worldDirectory, "data");
		mapDataDir.mkdirs();
		saveDirectoryName = par2Str;

		if (par3) {
			playersDirectory.mkdirs();
		}

		setSessionLock();
	}

	/**
	 * Creates a session lock file for this process
	 */
	private void setSessionLock() {
		try {
			final File var1 = new File(worldDirectory, "session.lock");
			final DataOutputStream var2 = new DataOutputStream(
					new FileOutputStream(var1));

			try {
				var2.writeLong(initializationTime);
			} finally {
				var2.close();
			}
		} catch (final IOException var7) {
			var7.printStackTrace();
			throw new RuntimeException("Failed to check session lock, aborting");
		}
	}

	/**
	 * Gets the File object corresponding to the base directory of this world.
	 */
	protected File getWorldDirectory() {
		return worldDirectory;
	}

	/**
	 * Checks the session lock to prevent save collisions
	 */
	@Override
	public void checkSessionLock() throws MinecraftException {
		try {
			final File var1 = new File(worldDirectory, "session.lock");
			final DataInputStream var2 = new DataInputStream(
					new FileInputStream(var1));

			try {
				if (var2.readLong() != initializationTime) {
					throw new MinecraftException(
							"The save is being accessed from another location, aborting");
				}
			} finally {
				var2.close();
			}
		} catch (final IOException var7) {
			throw new MinecraftException(
					"Failed to check session lock, aborting");
		}
	}

	/**
	 * Returns the chunk loader with the provided world provider
	 */
	@Override
	public IChunkLoader getChunkLoader(final WorldProvider par1WorldProvider) {
		throw new RuntimeException("Old Chunk Storage is no longer supported.");
	}

	/**
	 * Loads and returns the world info
	 */
	@Override
	public WorldInfo loadWorldInfo() {
		File var1 = new File(worldDirectory, "level.dat");
		NBTTagCompound var2;
		NBTTagCompound var3;

		if (var1.exists()) {
			try {
				var2 = CompressedStreamTools
						.readCompressed(new FileInputStream(var1));
				var3 = var2.getCompoundTag("Data");
				return new WorldInfo(var3);
			} catch (final Exception var5) {
				var5.printStackTrace();
			}
		}

		var1 = new File(worldDirectory, "level.dat_old");

		if (var1.exists()) {
			try {
				var2 = CompressedStreamTools
						.readCompressed(new FileInputStream(var1));
				var3 = var2.getCompoundTag("Data");
				return new WorldInfo(var3);
			} catch (final Exception var4) {
				var4.printStackTrace();
			}
		}

		return null;
	}

	/**
	 * Saves the given World Info with the given NBTTagCompound as the Player.
	 */
	@Override
	public void saveWorldInfoWithPlayer(final WorldInfo par1WorldInfo,
			final NBTTagCompound par2NBTTagCompound) {
		final NBTTagCompound var3 = par1WorldInfo
				.cloneNBTCompound(par2NBTTagCompound);
		final NBTTagCompound var4 = new NBTTagCompound();
		var4.setTag("Data", var3);

		try {
			final File var5 = new File(worldDirectory, "level.dat_new");
			final File var6 = new File(worldDirectory, "level.dat_old");
			final File var7 = new File(worldDirectory, "level.dat");
			CompressedStreamTools.writeCompressed(var4, new FileOutputStream(
					var5));

			if (var6.exists()) {
				var6.delete();
			}

			var7.renameTo(var6);

			if (var7.exists()) {
				var7.delete();
			}

			var5.renameTo(var7);

			if (var5.exists()) {
				var5.delete();
			}
		} catch (final Exception var8) {
			var8.printStackTrace();
		}
	}

	/**
	 * Saves the passed in world info.
	 */
	@Override
	public void saveWorldInfo(final WorldInfo par1WorldInfo) {
		final NBTTagCompound var2 = par1WorldInfo.getNBTTagCompound();
		final NBTTagCompound var3 = new NBTTagCompound();
		var3.setTag("Data", var2);

		try {
			final File var4 = new File(worldDirectory, "level.dat_new");
			final File var5 = new File(worldDirectory, "level.dat_old");
			final File var6 = new File(worldDirectory, "level.dat");
			CompressedStreamTools.writeCompressed(var3, new FileOutputStream(
					var4));

			if (var5.exists()) {
				var5.delete();
			}

			var6.renameTo(var5);

			if (var6.exists()) {
				var6.delete();
			}

			var4.renameTo(var6);

			if (var4.exists()) {
				var4.delete();
			}
		} catch (final Exception var7) {
			var7.printStackTrace();
		}
	}

	/**
	 * Writes the player data to disk from the specified PlayerEntityMP.
	 */
	@Override
	public void writePlayerData(final EntityPlayer par1EntityPlayer) {
		try {
			final NBTTagCompound var2 = new NBTTagCompound();
			par1EntityPlayer.writeToNBT(var2);
			final File var3 = new File(playersDirectory,
					par1EntityPlayer.username + ".dat.tmp");
			final File var4 = new File(playersDirectory,
					par1EntityPlayer.username + ".dat");
			CompressedStreamTools.writeCompressed(var2, new FileOutputStream(
					var3));

			if (var4.exists()) {
				var4.delete();
			}

			var3.renameTo(var4);
		} catch (final Exception var5) {
			MinecraftServer
					.getServer()
					.getLogAgent()
					.logWarning(
							"Failed to save player data for "
									+ par1EntityPlayer.username);
		}
	}

	/**
	 * Reads the player data from disk into the specified PlayerEntityMP.
	 */
	@Override
	public NBTTagCompound readPlayerData(final EntityPlayer par1EntityPlayer) {
		final NBTTagCompound var2 = getPlayerData(par1EntityPlayer.username);

		if (var2 != null) {
			par1EntityPlayer.readFromNBT(var2);
		}

		return var2;
	}

	/**
	 * Gets the player data for the given playername as a NBTTagCompound.
	 */
	public NBTTagCompound getPlayerData(final String par1Str) {
		try {
			final File var2 = new File(playersDirectory, par1Str + ".dat");

			if (var2.exists()) {
				return CompressedStreamTools
						.readCompressed(new FileInputStream(var2));
			}
		} catch (final Exception var3) {
			MinecraftServer.getServer().getLogAgent()
					.logWarning("Failed to load player data for " + par1Str);
		}

		return null;
	}

	/**
	 * returns null if no saveHandler is relevent (eg. SMP)
	 */
	@Override
	public IPlayerFileData getSaveHandler() {
		return this;
	}

	/**
	 * Returns an array of usernames for which player.dat exists for.
	 */
	@Override
	public String[] getAvailablePlayerDat() {
		final String[] var1 = playersDirectory.list();

		for (int var2 = 0; var2 < var1.length; ++var2) {
			if (var1[var2].endsWith(".dat")) {
				var1[var2] = var1[var2].substring(0, var1[var2].length() - 4);
			}
		}

		return var1;
	}

	/**
	 * Called to flush all changes to disk, waiting for them to complete.
	 */
	@Override
	public void flush() {
	}

	/**
	 * Gets the file location of the given map
	 */
	@Override
	public File getMapFileFromName(final String par1Str) {
		return new File(mapDataDir, par1Str + ".dat");
	}

	/**
	 * Returns the name of the directory where world information is saved.
	 */
	@Override
	public String getWorldDirectoryName() {
		return saveDirectoryName;
	}
}
