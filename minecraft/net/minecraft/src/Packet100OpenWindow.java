package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet100OpenWindow extends Packet {
	public int windowId;
	public int inventoryType;
	public String windowTitle;
	public int slotsCount;

	/**
	 * If false, the client will look up a string like "window.minecart". If
	 * true, the client uses what the server provides.
	 */
	public boolean useProvidedWindowTitle;

	public Packet100OpenWindow() {
	}

	public Packet100OpenWindow(final int par1, final int par2,
			final String par3Str, final int par4, final boolean par5) {
		windowId = par1;
		inventoryType = par2;
		windowTitle = par3Str;
		slotsCount = par4;
		useProvidedWindowTitle = par5;
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleOpenWindow(this);
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		windowId = par1DataInputStream.readByte() & 255;
		inventoryType = par1DataInputStream.readByte() & 255;
		windowTitle = Packet.readString(par1DataInputStream, 32);
		slotsCount = par1DataInputStream.readByte() & 255;
		useProvidedWindowTitle = par1DataInputStream.readBoolean();
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		par1DataOutputStream.writeByte(windowId & 255);
		par1DataOutputStream.writeByte(inventoryType & 255);
		Packet.writeString(windowTitle, par1DataOutputStream);
		par1DataOutputStream.writeByte(slotsCount & 255);
		par1DataOutputStream.writeBoolean(useProvidedWindowTitle);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 4 + windowTitle.length();
	}
}
