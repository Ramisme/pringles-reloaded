package net.minecraft.src;

import net.minecraft.client.Minecraft;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

public class GuiInventory extends InventoryEffectRenderer {
	/**
	 * x size of the inventory window in pixels. Defined as float, passed as int
	 */
	private float xSize_lo;

	/**
	 * y size of the inventory window in pixels. Defined as float, passed as
	 * int.
	 */
	private float ySize_lo;

	public GuiInventory(final EntityPlayer par1EntityPlayer) {
		super(par1EntityPlayer.inventoryContainer);
		allowUserInput = true;
		par1EntityPlayer.addStat(AchievementList.openInventory, 1);
	}

	/**
	 * Called from the main game loop to update the screen.
	 */
	@Override
	public void updateScreen() {
		if (mc.playerController.isInCreativeMode()) {
			mc.displayGuiScreen(new GuiContainerCreative(mc.thePlayer));
		}
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui() {
		buttonList.clear();

		if (mc.playerController.isInCreativeMode()) {
			mc.displayGuiScreen(new GuiContainerCreative(mc.thePlayer));
		} else {
			super.initGui();
		}
	}

	/**
	 * Draw the foreground layer for the GuiContainer (everything in front of
	 * the items)
	 */
	@Override
	protected void drawGuiContainerForegroundLayer(final int par1,
			final int par2) {
		fontRenderer.drawString(
				StatCollector.translateToLocal("container.crafting"), 86, 16,
				4210752);
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(final int par1, final int par2, final float par3) {
		super.drawScreen(par1, par2, par3);
		xSize_lo = par1;
		ySize_lo = par2;
	}

	/**
	 * Draw the background layer for the GuiContainer (everything behind the
	 * items)
	 */
	@Override
	protected void drawGuiContainerBackgroundLayer(final float par1,
			final int par2, final int par3) {
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		mc.renderEngine.bindTexture("/gui/inventory.png");
		final int var4 = guiLeft;
		final int var5 = guiTop;
		drawTexturedModalRect(var4, var5, 0, 0, xSize, ySize);
		GuiInventory.drawPlayerOnGui(mc, var4 + 51, var5 + 75, 30, var4 + 51
				- xSize_lo, var5 + 75 - 50 - ySize_lo);
	}

	public static void drawPlayerOnGui(final Minecraft par0Minecraft,
			final int par1, final int par2, final int par3, final float par4,
			final float par5) {
		GL11.glEnable(GL11.GL_COLOR_MATERIAL);
		GL11.glPushMatrix();
		GL11.glTranslatef(par1, par2, 50.0F);
		GL11.glScalef(-par3, par3, par3);
		GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
		final float var6 = par0Minecraft.thePlayer.renderYawOffset;
		final float var7 = par0Minecraft.thePlayer.rotationYaw;
		final float var8 = par0Minecraft.thePlayer.rotationPitch;
		GL11.glRotatef(135.0F, 0.0F, 1.0F, 0.0F);
		RenderHelper.enableStandardItemLighting();
		GL11.glRotatef(-135.0F, 0.0F, 1.0F, 0.0F);
		GL11.glRotatef(-((float) Math.atan(par5 / 40.0F)) * 20.0F, 1.0F, 0.0F,
				0.0F);
		par0Minecraft.thePlayer.renderYawOffset = (float) Math
				.atan(par4 / 40.0F) * 20.0F;
		par0Minecraft.thePlayer.rotationYaw = (float) Math.atan(par4 / 40.0F) * 40.0F;
		par0Minecraft.thePlayer.rotationPitch = -((float) Math
				.atan(par5 / 40.0F)) * 20.0F;
		par0Minecraft.thePlayer.rotationYawHead = par0Minecraft.thePlayer.rotationYaw;
		GL11.glTranslatef(0.0F, par0Minecraft.thePlayer.yOffset, 0.0F);
		RenderManager.instance.playerViewY = 180.0F;
		RenderManager.instance.renderEntityWithPosYaw(par0Minecraft.thePlayer,
				0.0D, 0.0D, 0.0D, 0.0F, 1.0F);
		par0Minecraft.thePlayer.renderYawOffset = var6;
		par0Minecraft.thePlayer.rotationYaw = var7;
		par0Minecraft.thePlayer.rotationPitch = var8;
		GL11.glPopMatrix();
		RenderHelper.disableStandardItemLighting();
		GL11.glDisable(GL12.GL_RESCALE_NORMAL);
		OpenGlHelper.setActiveTexture(OpenGlHelper.lightmapTexUnit);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		OpenGlHelper.setActiveTexture(OpenGlHelper.defaultTexUnit);
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	@Override
	protected void actionPerformed(final GuiButton par1GuiButton) {
		if (par1GuiButton.id == 0) {
			mc.displayGuiScreen(new GuiAchievements(mc.statFileWriter));
		}

		if (par1GuiButton.id == 1) {
			mc.displayGuiScreen(new GuiStats(this, mc.statFileWriter));
		}
	}
}
