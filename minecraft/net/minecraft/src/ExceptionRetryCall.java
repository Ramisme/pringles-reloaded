package net.minecraft.src;

public class ExceptionRetryCall extends ExceptionMcoService {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4613409990154024479L;
	public final int field_96393_c;

	public ExceptionRetryCall(final int par1) {
		super(503, "Retry operation");
		field_96393_c = par1;
	}
}
