package net.minecraft.src;

public class RenderZombie extends RenderBiped {
	private final ModelBiped field_82434_o;
	private ModelZombieVillager field_82432_p;
	protected ModelBiped field_82437_k;
	protected ModelBiped field_82435_l;
	protected ModelBiped field_82436_m;
	protected ModelBiped field_82433_n;
	private int field_82431_q = 1;

	public RenderZombie() {
		super(new ModelZombie(), 0.5F, 1.0F);
		field_82434_o = modelBipedMain;
		field_82432_p = new ModelZombieVillager();
	}

	@Override
	protected void func_82421_b() {
		field_82423_g = new ModelZombie(1.0F, true);
		field_82425_h = new ModelZombie(0.5F, true);
		field_82437_k = field_82423_g;
		field_82435_l = field_82425_h;
		field_82436_m = new ModelZombieVillager(1.0F, 0.0F, true);
		field_82433_n = new ModelZombieVillager(0.5F, 0.0F, true);
	}

	protected int func_82429_a(final EntityZombie par1EntityZombie,
			final int par2, final float par3) {
		func_82427_a(par1EntityZombie);
		return super.shouldRenderPass(par1EntityZombie, par2, par3);
	}

	public void func_82426_a(final EntityZombie par1EntityZombie,
			final double par2, final double par4, final double par6,
			final float par8, final float par9) {
		func_82427_a(par1EntityZombie);
		super.doRenderLiving(par1EntityZombie, par2, par4, par6, par8, par9);
	}

	protected void func_82428_a(final EntityZombie par1EntityZombie,
			final float par2) {
		func_82427_a(par1EntityZombie);
		super.renderEquippedItems(par1EntityZombie, par2);
	}

	private void func_82427_a(final EntityZombie par1EntityZombie) {
		if (par1EntityZombie.isVillager()) {
			if (field_82431_q != field_82432_p.func_82897_a()) {
				field_82432_p = new ModelZombieVillager();
				field_82431_q = field_82432_p.func_82897_a();
				field_82436_m = new ModelZombieVillager(1.0F, 0.0F, true);
				field_82433_n = new ModelZombieVillager(0.5F, 0.0F, true);
			}

			mainModel = field_82432_p;
			field_82423_g = field_82436_m;
			field_82425_h = field_82433_n;
		} else {
			mainModel = field_82434_o;
			field_82423_g = field_82437_k;
			field_82425_h = field_82435_l;
		}

		modelBipedMain = (ModelBiped) mainModel;
	}

	protected void func_82430_a(final EntityZombie par1EntityZombie,
			final float par2, float par3, final float par4) {
		if (par1EntityZombie.isConverting()) {
			par3 += (float) (Math.cos(par1EntityZombie.ticksExisted * 3.25D)
					* Math.PI * 0.25D);
		}

		super.rotateCorpse(par1EntityZombie, par2, par3, par4);
	}

	@Override
	protected void renderEquippedItems(final EntityLiving par1EntityLiving,
			final float par2) {
		func_82428_a((EntityZombie) par1EntityLiving, par2);
	}

	@Override
	public void doRenderLiving(final EntityLiving par1EntityLiving,
			final double par2, final double par4, final double par6,
			final float par8, final float par9) {
		func_82426_a((EntityZombie) par1EntityLiving, par2, par4, par6, par8,
				par9);
	}

	/**
	 * Queries whether should render the specified pass or not.
	 */
	@Override
	protected int shouldRenderPass(final EntityLiving par1EntityLiving,
			final int par2, final float par3) {
		return func_82429_a((EntityZombie) par1EntityLiving, par2, par3);
	}

	@Override
	protected void rotateCorpse(final EntityLiving par1EntityLiving,
			final float par2, final float par3, final float par4) {
		func_82430_a((EntityZombie) par1EntityLiving, par2, par3, par4);
	}

	/**
	 * Actually renders the given argument. This is a synthetic bridge method,
	 * always casting down its argument and then handing it off to a worker
	 * function which does the actual work. In all probabilty, the class Render
	 * is generic (Render<T extends Entity) and this method has signature public
	 * void doRender(T entity, double d, double d1, double d2, float f, float
	 * f1). But JAD is pre 1.5 so doesn't do that.
	 */
	@Override
	public void doRender(final Entity par1Entity, final double par2,
			final double par4, final double par6, final float par8,
			final float par9) {
		func_82426_a((EntityZombie) par1Entity, par2, par4, par6, par8, par9);
	}
}
