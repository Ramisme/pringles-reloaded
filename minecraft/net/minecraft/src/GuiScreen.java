package net.minecraft.src;

import java.awt.Toolkit;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.Minecraft;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

public class GuiScreen extends Gui {
	public static final boolean isMacOs = Minecraft.getOs() == EnumOS.MACOS;

	/** Reference to the Minecraft object. */
	protected Minecraft mc;

	/** The width of the screen object. */
	public int width;

	/** The height of the screen object. */
	public int height;

	/** A list of all the buttons in this container. */
	protected List buttonList = new ArrayList();
	public boolean allowUserInput = false;

	/** The FontRenderer used by GuiScreen */
	protected FontRenderer fontRenderer;
	public GuiParticle guiParticles;

	/** The button that was just pressed. */
	private GuiButton selectedButton = null;
	private int eventButton = 0;
	private long field_85043_c = 0L;
	private int field_92018_d = 0;

	/**
	 * Draws the screen and all the components in it.
	 */
	public void drawScreen(final int par1, final int par2, final float par3) {
		for (int var4 = 0; var4 < buttonList.size(); ++var4) {
			final GuiButton var5 = (GuiButton) buttonList.get(var4);
			var5.drawButton(mc, par1, par2);
		}
	}

	/**
	 * Fired when a key is typed. This is the equivalent of
	 * KeyListener.keyTyped(KeyEvent e).
	 */
	protected void keyTyped(final char par1, final int par2) {
		if (par2 == 1) {
			mc.displayGuiScreen((GuiScreen) null);
			mc.setIngameFocus();
		}
	}

	/**
	 * Returns a string stored in the system clipboard.
	 */
	public static String getClipboardString() {
		try {
			final Transferable var0 = Toolkit.getDefaultToolkit()
					.getSystemClipboard().getContents((Object) null);

			if (var0 != null
					&& var0.isDataFlavorSupported(DataFlavor.stringFlavor)) {
				return (String) var0.getTransferData(DataFlavor.stringFlavor);
			}
		} catch (final Exception var1) {
			;
		}

		return "";
	}

	/**
	 * store a string in the system clipboard
	 */
	public static void setClipboardString(final String par0Str) {
		try {
			final StringSelection var1 = new StringSelection(par0Str);
			Toolkit.getDefaultToolkit().getSystemClipboard()
					.setContents(var1, (ClipboardOwner) null);
		} catch (final Exception var2) {
			;
		}
	}

	/**
	 * Called when the mouse is clicked.
	 */
	protected void mouseClicked(final int par1, final int par2, final int par3) {
		if (par3 == 0) {
			for (int var4 = 0; var4 < buttonList.size(); ++var4) {
				final GuiButton var5 = (GuiButton) buttonList.get(var4);

				if (var5.mousePressed(mc, par1, par2)) {
					selectedButton = var5;
					mc.sndManager.playSoundFX("random.click", 1.0F, 1.0F);
					actionPerformed(var5);
				}
			}
		}
	}

	/**
	 * Called when the mouse is moved or a mouse button is released. Signature:
	 * (mouseX, mouseY, which) which==-1 is mouseMove, which==0 or which==1 is
	 * mouseUp
	 */
	protected void mouseMovedOrUp(final int par1, final int par2, final int par3) {
		if (selectedButton != null && par3 == 0) {
			selectedButton.mouseReleased(par1, par2);
			selectedButton = null;
		}
	}

	protected void func_85041_a(final int par1, final int par2, final int par3,
			final long par4) {
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	protected void actionPerformed(final GuiButton par1GuiButton) {
	}

	/**
	 * Causes the screen to lay out its subcomponents again. This is the
	 * equivalent of the Java call Container.validate()
	 */
	public void setWorldAndResolution(final Minecraft par1Minecraft,
			final int par2, final int par3) {
		guiParticles = new GuiParticle(par1Minecraft);
		mc = par1Minecraft;
		fontRenderer = par1Minecraft.fontRenderer;
		width = par2;
		height = par3;
		buttonList.clear();
		initGui();
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	public void initGui() {
	}

	/**
	 * Delegates mouse and keyboard input.
	 */
	public void handleInput() {
		while (Mouse.next()) {
			handleMouseInput();
		}

		while (Keyboard.next()) {
			handleKeyboardInput();
		}
	}

	/**
	 * Handles mouse input.
	 */
	public void handleMouseInput() {
		final int var1 = Mouse.getEventX() * width / mc.displayWidth;
		final int var2 = height - Mouse.getEventY() * height / mc.displayHeight
				- 1;

		if (Mouse.getEventButtonState()) {
			if (mc.gameSettings.touchscreen && field_92018_d++ > 0) {
				return;
			}

			eventButton = Mouse.getEventButton();
			field_85043_c = Minecraft.getSystemTime();
			mouseClicked(var1, var2, eventButton);
		} else if (Mouse.getEventButton() != -1) {
			if (mc.gameSettings.touchscreen && --field_92018_d > 0) {
				return;
			}

			eventButton = -1;
			mouseMovedOrUp(var1, var2, Mouse.getEventButton());
		} else if (eventButton != -1 && field_85043_c > 0L) {
			final long var3 = Minecraft.getSystemTime() - field_85043_c;
			func_85041_a(var1, var2, eventButton, var3);
		}
	}

	/**
	 * Handles keyboard input.
	 */
	public void handleKeyboardInput() {
		if (Keyboard.getEventKeyState()) {
			int var1 = Keyboard.getEventKey();
			final char var2 = Keyboard.getEventCharacter();

			if (var1 == 87) {
				mc.toggleFullscreen();
				return;
			}

			if (GuiScreen.isMacOs && var1 == 28 && var2 == 0) {
				var1 = 29;
			}

			keyTyped(var2, var1);
		}
	}

	/**
	 * Called from the main game loop to update the screen.
	 */
	public void updateScreen() {
	}

	/**
	 * Called when the screen is unloaded. Used to disable keyboard repeat
	 * events
	 */
	public void onGuiClosed() {
	}

	/**
	 * Draws either a gradient over the background screen (when it exists) or a
	 * flat gradient over background.png
	 */
	public void drawDefaultBackground() {
		drawWorldBackground(0);
	}

	public void drawWorldBackground(final int par1) {
		if (mc.theWorld != null) {
			drawGradientRect(0, 0, width, height, -1072689136, -804253680);
		} else {
			drawBackground(par1);
		}
	}

	/**
	 * Draws the background (i is always 0 as of 1.2.2)
	 */
	public void drawBackground(final int par1) {
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glDisable(GL11.GL_FOG);
		final Tessellator var2 = Tessellator.instance;
		mc.renderEngine.bindTexture("/gui/background.png");
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		final float var3 = 32.0F;
		var2.startDrawingQuads();
		var2.setColorOpaque_I(4210752);
		var2.addVertexWithUV(0.0D, height, 0.0D, 0.0D, height / var3 + par1);
		var2.addVertexWithUV(width, height, 0.0D, width / var3, height / var3
				+ par1);
		var2.addVertexWithUV(width, 0.0D, 0.0D, width / var3, par1);
		var2.addVertexWithUV(0.0D, 0.0D, 0.0D, 0.0D, par1);
		var2.draw();
	}

	/**
	 * Returns true if this GUI should pause the game when it is displayed in
	 * single-player
	 */
	public boolean doesGuiPauseGame() {
		return true;
	}

	public void confirmClicked(final boolean par1, final int par2) {
	}

	public static boolean isCtrlKeyDown() {
		final boolean var0 = Keyboard.isKeyDown(28)
				&& Keyboard.getEventCharacter() == 0;
		return Keyboard.isKeyDown(29) || Keyboard.isKeyDown(157)
				|| GuiScreen.isMacOs
				&& (var0 || Keyboard.isKeyDown(219) || Keyboard.isKeyDown(220));
	}

	public static boolean isShiftKeyDown() {
		return Keyboard.isKeyDown(42) || Keyboard.isKeyDown(54);
	}
}
