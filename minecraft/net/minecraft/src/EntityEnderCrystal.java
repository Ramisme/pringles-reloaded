package net.minecraft.src;

public class EntityEnderCrystal extends Entity {
	/** Used to create the rotation animation when rendering the crystal. */
	public int innerRotation;
	public int health;

	public EntityEnderCrystal(final World par1World) {
		super(par1World);
		innerRotation = 0;
		preventEntitySpawning = true;
		setSize(2.0F, 2.0F);
		yOffset = height / 2.0F;
		health = 5;
		innerRotation = rand.nextInt(100000);
	}

	public EntityEnderCrystal(final World par1World, final double par2,
			final double par4, final double par6) {
		this(par1World);
		setPosition(par2, par4, par6);
	}

	/**
	 * returns if this entity triggers Block.onEntityWalking on the blocks they
	 * walk on. used for spiders and wolves to prevent them from trampling crops
	 */
	@Override
	protected boolean canTriggerWalking() {
		return false;
	}

	@Override
	protected void entityInit() {
		dataWatcher.addObject(8, Integer.valueOf(health));
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate() {
		prevPosX = posX;
		prevPosY = posY;
		prevPosZ = posZ;
		++innerRotation;
		dataWatcher.updateObject(8, Integer.valueOf(health));
		final int var1 = MathHelper.floor_double(posX);
		final int var2 = MathHelper.floor_double(posY);
		final int var3 = MathHelper.floor_double(posZ);

		if (worldObj.getBlockId(var1, var2, var3) != Block.fire.blockID) {
			worldObj.setBlock(var1, var2, var3, Block.fire.blockID);
		}
	}

	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	@Override
	protected void writeEntityToNBT(final NBTTagCompound par1NBTTagCompound) {
	}

	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	@Override
	protected void readEntityFromNBT(final NBTTagCompound par1NBTTagCompound) {
	}

	@Override
	public float getShadowSize() {
		return 0.0F;
	}

	/**
	 * Returns true if other Entities should be prevented from moving through
	 * this Entity.
	 */
	@Override
	public boolean canBeCollidedWith() {
		return true;
	}

	/**
	 * Called when the entity is attacked.
	 */
	@Override
	public boolean attackEntityFrom(final DamageSource par1DamageSource,
			final int par2) {
		if (isEntityInvulnerable()) {
			return false;
		} else {
			if (!isDead && !worldObj.isRemote) {
				health = 0;

				if (health <= 0) {
					setDead();

					if (!worldObj.isRemote) {
						worldObj.createExplosion((Entity) null, posX, posY,
								posZ, 6.0F, true);
					}
				}
			}

			return true;
		}
	}
}
