package net.minecraft.src;

import java.io.File;

public class AnvilSaveHandler extends SaveHandler {
	public AnvilSaveHandler(final File par1File, final String par2Str,
			final boolean par3) {
		super(par1File, par2Str, par3);
	}

	/**
	 * Returns the chunk loader with the provided world provider
	 */
	@Override
	public IChunkLoader getChunkLoader(final WorldProvider par1WorldProvider) {
		final File var2 = getWorldDirectory();
		File var3;

		if (par1WorldProvider instanceof WorldProviderHell) {
			var3 = new File(var2, "DIM-1");
			var3.mkdirs();
			return new AnvilChunkLoader(var3);
		} else if (par1WorldProvider instanceof WorldProviderEnd) {
			var3 = new File(var2, "DIM1");
			var3.mkdirs();
			return new AnvilChunkLoader(var3);
		} else {
			return new AnvilChunkLoader(var2);
		}
	}

	/**
	 * Saves the given World Info with the given NBTTagCompound as the Player.
	 */
	@Override
	public void saveWorldInfoWithPlayer(final WorldInfo par1WorldInfo,
			final NBTTagCompound par2NBTTagCompound) {
		par1WorldInfo.setSaveVersion(19133);
		super.saveWorldInfoWithPlayer(par1WorldInfo, par2NBTTagCompound);
	}

	/**
	 * Called to flush all changes to disk, waiting for them to complete.
	 */
	@Override
	public void flush() {
		try {
			ThreadedFileIOBase.threadedIOInstance.waitForFinish();
		} catch (final InterruptedException var2) {
			var2.printStackTrace();
		}

		RegionFileCache.clearRegionFileReferences();
	}
}
