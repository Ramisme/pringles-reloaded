package net.minecraft.src;

public class GuiSleepMP extends GuiChat {
	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui() {
		super.initGui();
		final StringTranslate var1 = StringTranslate.getInstance();
		buttonList.add(new GuiButton(1, width / 2 - 100, height - 40, var1
				.translateKey("multiplayer.stopSleeping")));
	}

	/**
	 * Fired when a key is typed. This is the equivalent of
	 * KeyListener.keyTyped(KeyEvent e).
	 */
	@Override
	protected void keyTyped(final char par1, final int par2) {
		if (par2 == 1) {
			wakeEntity();
		} else if (par2 == 28) {
			final String var3 = inputField.getText().trim();

			if (var3.length() > 0) {
				mc.thePlayer.sendChatMessage(var3);
			}

			inputField.setText("");
			mc.ingameGUI.getChatGUI().resetScroll();
		} else {
			super.keyTyped(par1, par2);
		}
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	@Override
	protected void actionPerformed(final GuiButton par1GuiButton) {
		if (par1GuiButton.id == 1) {
			wakeEntity();
		} else {
			super.actionPerformed(par1GuiButton);
		}
	}

	/**
	 * Wakes the entity from the bed
	 */
	private void wakeEntity() {
		final NetClientHandler var1 = mc.thePlayer.sendQueue;
		var1.addToSendQueue(new Packet19EntityAction(mc.thePlayer, 3));
	}
}
