package net.minecraft.src;

import java.io.File;
import java.io.IOException;

import net.minecraft.client.Minecraft;
import net.minecraft.server.MinecraftServer;

public class IntegratedServer extends MinecraftServer {
	/** The Minecraft instance. */
	private final Minecraft mc;
	private final WorldSettings theWorldSettings;
	private final ILogAgent serverLogAgent = new LogAgent("Minecraft-Server",
			" [SERVER]", new File(Minecraft.getMinecraftDir(),
					"output-server.log").getAbsolutePath());

	/** Instance of IntegratedServerListenThread. */
	private IntegratedServerListenThread theServerListeningThread;
	private boolean isGamePaused = false;
	private boolean isPublic;
	private ThreadLanServerPing lanServerPing;

	public IntegratedServer(final Minecraft par1Minecraft,
			final String par2Str, final String par3Str,
			final WorldSettings par4WorldSettings) {
		super(new File(Minecraft.getMinecraftDir(), "saves"));
		setServerOwner(par1Minecraft.session.username);
		setFolderName(par2Str);
		setWorldName(par3Str);
		setDemo(par1Minecraft.isDemo());
		canCreateBonusChest(par4WorldSettings.isBonusChestEnabled());
		setBuildLimit(256);
		setConfigurationManager(new IntegratedPlayerList(this));
		mc = par1Minecraft;
		theWorldSettings = par4WorldSettings;

		try {
			theServerListeningThread = new IntegratedServerListenThread(this);
		} catch (final IOException var6) {
			throw new Error();
		}
	}

	@Override
	protected void loadAllWorlds(final String par1Str, final String par2Str,
			final long par3, final WorldType par5WorldType, final String par6Str) {
		convertMapIfNeeded(par1Str);
		final ISaveHandler var7 = getActiveAnvilConverter().getSaveLoader(
				par1Str, true);

		if (Reflector.DimensionManager.exists()) {
			final Object var8 = isDemo() ? new DemoWorldServer(this, var7,
					par2Str, 0, theProfiler, getLogAgent())
					: new WorldServerOF(this, var7, par2Str, 0,
							theWorldSettings, theProfiler, getLogAgent());
			final Integer[] var9 = (Integer[]) Reflector.call(
					Reflector.DimensionManager_getStaticDimensionIDs,
					new Object[0]);
			final Integer[] var10 = var9;
			final int var11 = var9.length;

			for (int var12 = 0; var12 < var11; ++var12) {
				final int var13 = var10[var12].intValue();
				final Object var14 = var13 == 0 ? var8 : new WorldServerMulti(
						this, var7, par2Str, var13, theWorldSettings,
						(WorldServer) var8, theProfiler, getLogAgent());
				((WorldServer) var14).addWorldAccess(new WorldManager(this,
						(WorldServer) var14));

				if (!isSinglePlayer()) {
					((WorldServer) var14).getWorldInfo().setGameType(
							getGameType());
				}

				if (Reflector.EventBus.exists()) {
					Reflector.postForgeBusEvent(
							Reflector.WorldEvent_Load_Constructor,
							new Object[] { var14 });
				}
			}

			getConfigurationManager().setPlayerManager(
					new WorldServer[] { (WorldServer) var8 });
		} else {
			worldServers = new WorldServer[3];
			timeOfLastDimensionTick = new long[worldServers.length][100];

			for (int var15 = 0; var15 < worldServers.length; ++var15) {
				byte var16 = 0;

				if (var15 == 1) {
					var16 = -1;
				}

				if (var15 == 2) {
					var16 = 1;
				}

				if (var15 == 0) {
					if (isDemo()) {
						worldServers[var15] = new DemoWorldServer(this, var7,
								par2Str, var16, theProfiler, getLogAgent());
					} else {
						worldServers[var15] = new WorldServerOF(this, var7,
								par2Str, var16, theWorldSettings, theProfiler,
								getLogAgent());
					}
				} else {
					worldServers[var15] = new WorldServerMulti(this, var7,
							par2Str, var16, theWorldSettings, worldServers[0],
							theProfiler, getLogAgent());
				}

				worldServers[var15].addWorldAccess(new WorldManager(this,
						worldServers[var15]));
				getConfigurationManager().setPlayerManager(worldServers);
			}
		}

		setDifficultyForAllWorlds(getDifficulty());
		initialWorldChunkLoad();
	}

	/**
	 * Initialises the server and starts it.
	 */
	@Override
	protected boolean startServer() throws IOException {
		serverLogAgent
				.logInfo("Starting integrated minecraft server version 1.5.2");
		setOnlineMode(false);
		setCanSpawnAnimals(true);
		setCanSpawnNPCs(true);
		setAllowPvp(true);
		setAllowFlight(true);
		serverLogAgent.logInfo("Generating keypair");
		setKeyPair(CryptManager.createNewKeyPair());
		Object var1;

		if (Reflector.FMLCommonHandler_handleServerAboutToStart.exists()) {
			var1 = Reflector.call(Reflector.FMLCommonHandler_instance,
					new Object[0]);

			if (!Reflector.callBoolean(var1,
					Reflector.FMLCommonHandler_handleServerAboutToStart,
					new Object[] { this })) {
				return false;
			}
		}

		loadAllWorlds(getFolderName(), getWorldName(),
				theWorldSettings.getSeed(), theWorldSettings.getTerrainType(),
				theWorldSettings.func_82749_j());
		setMOTD(getServerOwner() + " - "
				+ worldServers[0].getWorldInfo().getWorldName());

		if (Reflector.FMLCommonHandler_handleServerStarting.exists()) {
			var1 = Reflector.call(Reflector.FMLCommonHandler_instance,
					new Object[0]);

			if (Reflector.FMLCommonHandler_handleServerStarting.getReturnType() == Boolean.TYPE) {
				return Reflector.callBoolean(var1,
						Reflector.FMLCommonHandler_handleServerStarting,
						new Object[] { this });
			}

			Reflector.callVoid(var1,
					Reflector.FMLCommonHandler_handleServerStarting,
					new Object[] { this });
		}

		return true;
	}

	/**
	 * Main function called by run() every loop.
	 */
	@Override
	public void tick() {
		final boolean var1 = isGamePaused;
		isGamePaused = theServerListeningThread.isGamePaused();

		if (!var1 && isGamePaused) {
			serverLogAgent.logInfo("Saving and pausing game...");
			getConfigurationManager().saveAllPlayerData();
			saveAllWorlds(false);
		}

		if (!isGamePaused) {
			super.tick();
		}
	}

	@Override
	public boolean canStructuresSpawn() {
		return false;
	}

	@Override
	public EnumGameType getGameType() {
		return theWorldSettings.getGameType();
	}

	/**
	 * Defaults to "1" (Easy) for the dedicated server, defaults to "2" (Normal)
	 * on the client.
	 */
	@Override
	public int getDifficulty() {
		return mc.gameSettings.difficulty;
	}

	/**
	 * Defaults to false.
	 */
	@Override
	public boolean isHardcore() {
		return theWorldSettings.getHardcoreEnabled();
	}

	@Override
	protected File getDataDirectory() {
		return mc.mcDataDir;
	}

	@Override
	public boolean isDedicatedServer() {
		return false;
	}

	/**
	 * Gets the IntergratedServerListenThread.
	 */
	public IntegratedServerListenThread getServerListeningThread() {
		return theServerListeningThread;
	}

	/**
	 * Called on exit from the main run() loop.
	 */
	@Override
	protected void finalTick(final CrashReport par1CrashReport) {
		mc.crashed(par1CrashReport);
	}

	/**
	 * Adds the server info, including from theWorldServer, to the crash report.
	 */
	@Override
	public CrashReport addServerInfoToCrashReport(CrashReport par1CrashReport) {
		par1CrashReport = super.addServerInfoToCrashReport(par1CrashReport);
		par1CrashReport.func_85056_g().addCrashSectionCallable("Type",
				new CallableType3(this));
		par1CrashReport.func_85056_g().addCrashSectionCallable("Is Modded",
				new CallableIsModded(this));
		return par1CrashReport;
	}

	@Override
	public void addServerStatsToSnooper(
			final PlayerUsageSnooper par1PlayerUsageSnooper) {
		super.addServerStatsToSnooper(par1PlayerUsageSnooper);
		par1PlayerUsageSnooper.addData("snooper_partner", mc
				.getPlayerUsageSnooper().getUniqueID());
	}

	/**
	 * Returns whether snooping is enabled or not.
	 */
	@Override
	public boolean isSnooperEnabled() {
		return Minecraft.getMinecraft().isSnooperEnabled();
	}

	/**
	 * On dedicated does nothing. On integrated, sets commandsAllowedForAll,
	 * gameType and allows external connections.
	 */
	@Override
	public String shareToLAN(final EnumGameType par1EnumGameType,
			final boolean par2) {
		try {
			final String var3 = theServerListeningThread.func_71755_c();
			getLogAgent().logInfo("Started on " + var3);
			isPublic = true;
			lanServerPing = new ThreadLanServerPing(getMOTD(), var3);
			lanServerPing.start();
			getConfigurationManager().setGameType(par1EnumGameType);
			getConfigurationManager().setCommandsAllowedForAll(par2);
			return var3;
		} catch (final IOException var4) {
			return null;
		}
	}

	@Override
	public ILogAgent getLogAgent() {
		return serverLogAgent;
	}

	/**
	 * Saves all necessary data as preparation for stopping the server.
	 */
	@Override
	public void stopServer() {
		super.stopServer();

		if (lanServerPing != null) {
			lanServerPing.interrupt();
			lanServerPing = null;
		}
	}

	/**
	 * Sets the serverRunning variable to false, in order to get the server to
	 * shut down.
	 */
	@Override
	public void initiateShutdown() {
		super.initiateShutdown();

		if (lanServerPing != null) {
			lanServerPing.interrupt();
			lanServerPing = null;
		}
	}

	/**
	 * Returns true if this integrated server is open to LAN
	 */
	public boolean getPublic() {
		return isPublic;
	}

	/**
	 * Sets the game type for all worlds.
	 */
	@Override
	public void setGameType(final EnumGameType par1EnumGameType) {
		getConfigurationManager().setGameType(par1EnumGameType);
	}

	/**
	 * Return whether command blocks are enabled.
	 */
	@Override
	public boolean isCommandBlockEnabled() {
		return true;
	}

	@Override
	public NetworkListenThread getNetworkThread() {
		return getServerListeningThread();
	}
}
