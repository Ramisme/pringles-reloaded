package net.minecraft.src;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

class GuiCreateFlatWorldListSlot extends GuiSlot {
	public int field_82454_a;

	final GuiCreateFlatWorld createFlatWorldGui;

	public GuiCreateFlatWorldListSlot(
			final GuiCreateFlatWorld par1GuiCreateFlatWorld) {
		super(par1GuiCreateFlatWorld.mc, par1GuiCreateFlatWorld.width,
				par1GuiCreateFlatWorld.height, 43,
				par1GuiCreateFlatWorld.height - 60, 24);
		createFlatWorldGui = par1GuiCreateFlatWorld;
		field_82454_a = -1;
	}

	private void func_82452_a(final int par1, final int par2,
			final ItemStack par3ItemStack) {
		func_82451_d(par1 + 1, par2 + 1);
		GL11.glEnable(GL12.GL_RESCALE_NORMAL);

		if (par3ItemStack != null) {
			RenderHelper.enableGUIStandardItemLighting();
			GuiCreateFlatWorld.getRenderItem().renderItemIntoGUI(
					createFlatWorldGui.fontRenderer,
					createFlatWorldGui.mc.renderEngine, par3ItemStack,
					par1 + 2, par2 + 2);
			RenderHelper.disableStandardItemLighting();
		}

		GL11.glDisable(GL12.GL_RESCALE_NORMAL);
	}

	private void func_82451_d(final int par1, final int par2) {
		func_82450_b(par1, par2, 0, 0);
	}

	private void func_82450_b(final int par1, final int par2, final int par3,
			final int par4) {
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		createFlatWorldGui.mc.renderEngine.bindTexture("/gui/slot.png");
		final Tessellator var9 = Tessellator.instance;
		var9.startDrawingQuads();
		var9.addVertexWithUV(par1 + 0, par2 + 18, createFlatWorldGui.zLevel,
				(par3 + 0) * 0.0078125F, (par4 + 18) * 0.0078125F);
		var9.addVertexWithUV(par1 + 18, par2 + 18, createFlatWorldGui.zLevel,
				(par3 + 18) * 0.0078125F, (par4 + 18) * 0.0078125F);
		var9.addVertexWithUV(par1 + 18, par2 + 0, createFlatWorldGui.zLevel,
				(par3 + 18) * 0.0078125F, (par4 + 0) * 0.0078125F);
		var9.addVertexWithUV(par1 + 0, par2 + 0, createFlatWorldGui.zLevel,
				(par3 + 0) * 0.0078125F, (par4 + 0) * 0.0078125F);
		var9.draw();
	}

	/**
	 * Gets the size of the current slot list.
	 */
	@Override
	protected int getSize() {
		return GuiCreateFlatWorld.func_82271_a(createFlatWorldGui)
				.getFlatLayers().size();
	}

	/**
	 * the element in the slot that was clicked, boolean for wether it was
	 * double clicked or not
	 */
	@Override
	protected void elementClicked(final int par1, final boolean par2) {
		field_82454_a = par1;
		createFlatWorldGui.func_82270_g();
	}

	/**
	 * returns true if the element passed in is currently selected
	 */
	@Override
	protected boolean isSelected(final int par1) {
		return par1 == field_82454_a;
	}

	@Override
	protected void drawBackground() {
	}

	@Override
	protected void drawSlot(final int par1, final int par2, final int par3,
			final int par4, final Tessellator par5Tessellator) {
		final FlatLayerInfo var6 = (FlatLayerInfo) GuiCreateFlatWorld
				.func_82271_a(createFlatWorldGui)
				.getFlatLayers()
				.get(GuiCreateFlatWorld.func_82271_a(createFlatWorldGui)
						.getFlatLayers().size()
						- par1 - 1);
		final ItemStack var7 = var6.getFillBlock() == 0 ? null : new ItemStack(
				var6.getFillBlock(), 1, var6.getFillBlockMeta());
		final String var8 = var7 == null ? "Air" : Item.itemsList[var6
				.getFillBlock()].func_77653_i(var7);
		func_82452_a(par2, par3, var7);
		createFlatWorldGui.fontRenderer.drawString(var8, par2 + 18 + 5,
				par3 + 3, 16777215);
		String var9;

		if (par1 == 0) {
			var9 = StatCollector.translateToLocalFormatted(
					"createWorld.customize.flat.layer.top",
					new Object[] { Integer.valueOf(var6.getLayerCount()) });
		} else if (par1 == GuiCreateFlatWorld.func_82271_a(createFlatWorldGui)
				.getFlatLayers().size() - 1) {
			var9 = StatCollector.translateToLocalFormatted(
					"createWorld.customize.flat.layer.bottom",
					new Object[] { Integer.valueOf(var6.getLayerCount()) });
		} else {
			var9 = StatCollector.translateToLocalFormatted(
					"createWorld.customize.flat.layer",
					new Object[] { Integer.valueOf(var6.getLayerCount()) });
		}

		createFlatWorldGui.fontRenderer.drawString(var9, par2 + 2 + 213
				- createFlatWorldGui.fontRenderer.getStringWidth(var9),
				par3 + 3, 16777215);
	}

	@Override
	protected int getScrollBarX() {
		return createFlatWorldGui.width - 70;
	}
}
