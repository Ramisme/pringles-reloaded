package net.minecraft.src;

public class RenderPig extends RenderLiving {
	public RenderPig(final ModelBase par1ModelBase,
			final ModelBase par2ModelBase, final float par3) {
		super(par1ModelBase, par3);
		setRenderPassModel(par2ModelBase);
	}

	protected int renderSaddledPig(final EntityPig par1EntityPig,
			final int par2, final float par3) {
		if (par2 == 0 && par1EntityPig.getSaddled()) {
			loadTexture("/mob/saddle.png");
			return 1;
		} else {
			return -1;
		}
	}

	public void renderLivingPig(final EntityPig par1EntityPig,
			final double par2, final double par4, final double par6,
			final float par8, final float par9) {
		super.doRenderLiving(par1EntityPig, par2, par4, par6, par8, par9);
	}

	/**
	 * Queries whether should render the specified pass or not.
	 */
	@Override
	protected int shouldRenderPass(final EntityLiving par1EntityLiving,
			final int par2, final float par3) {
		return renderSaddledPig((EntityPig) par1EntityLiving, par2, par3);
	}

	@Override
	public void doRenderLiving(final EntityLiving par1EntityLiving,
			final double par2, final double par4, final double par6,
			final float par8, final float par9) {
		renderLivingPig((EntityPig) par1EntityLiving, par2, par4, par6, par8,
				par9);
	}

	/**
	 * Actually renders the given argument. This is a synthetic bridge method,
	 * always casting down its argument and then handing it off to a worker
	 * function which does the actual work. In all probabilty, the class Render
	 * is generic (Render<T extends Entity) and this method has signature public
	 * void doRender(T entity, double d, double d1, double d2, float f, float
	 * f1). But JAD is pre 1.5 so doesn't do that.
	 */
	@Override
	public void doRender(final Entity par1Entity, final double par2,
			final double par4, final double par6, final float par8,
			final float par9) {
		renderLivingPig((EntityPig) par1Entity, par2, par4, par6, par8, par9);
	}
}
