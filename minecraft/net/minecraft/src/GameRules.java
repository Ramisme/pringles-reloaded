package net.minecraft.src;

import java.util.Collection;
import java.util.Iterator;
import java.util.TreeMap;

public class GameRules {
	private final TreeMap theGameRules = new TreeMap();

	public GameRules() {
		addGameRule("doFireTick", "true");
		addGameRule("mobGriefing", "true");
		addGameRule("keepInventory", "false");
		addGameRule("doMobSpawning", "true");
		addGameRule("doMobLoot", "true");
		addGameRule("doTileDrops", "true");
		addGameRule("commandBlockOutput", "true");
	}

	/**
	 * Define a game rule and its default value.
	 */
	public void addGameRule(final String par1Str, final String par2Str) {
		theGameRules.put(par1Str, new GameRuleValue(par2Str));
	}

	public void setOrCreateGameRule(final String par1Str, final String par2Str) {
		final GameRuleValue var3 = (GameRuleValue) theGameRules.get(par1Str);

		if (var3 != null) {
			var3.setValue(par2Str);
		} else {
			addGameRule(par1Str, par2Str);
		}
	}

	/**
	 * Gets the string Game Rule value.
	 */
	public String getGameRuleStringValue(final String par1Str) {
		final GameRuleValue var2 = (GameRuleValue) theGameRules.get(par1Str);
		return var2 != null ? var2.getGameRuleStringValue() : "";
	}

	/**
	 * Gets the boolean Game Rule value.
	 */
	public boolean getGameRuleBooleanValue(final String par1Str) {
		final GameRuleValue var2 = (GameRuleValue) theGameRules.get(par1Str);
		return var2 != null ? var2.getGameRuleBooleanValue() : false;
	}

	/**
	 * Return the defined game rules as NBT.
	 */
	public NBTTagCompound writeGameRulesToNBT() {
		final NBTTagCompound var1 = new NBTTagCompound("GameRules");
		final Iterator var2 = theGameRules.keySet().iterator();

		while (var2.hasNext()) {
			final String var3 = (String) var2.next();
			final GameRuleValue var4 = (GameRuleValue) theGameRules.get(var3);
			var1.setString(var3, var4.getGameRuleStringValue());
		}

		return var1;
	}

	/**
	 * Set defined game rules from NBT.
	 */
	public void readGameRulesFromNBT(final NBTTagCompound par1NBTTagCompound) {
		final Collection var2 = par1NBTTagCompound.getTags();
		final Iterator var3 = var2.iterator();

		while (var3.hasNext()) {
			final NBTBase var4 = (NBTBase) var3.next();
			final String var5 = var4.getName();
			final String var6 = par1NBTTagCompound.getString(var4.getName());
			setOrCreateGameRule(var5, var6);
		}
	}

	/**
	 * Return the defined game rules.
	 */
	public String[] getRules() {
		return (String[]) theGameRules.keySet().toArray(new String[0]);
	}

	/**
	 * Return whether the specified game rule is defined.
	 */
	public boolean hasRule(final String par1Str) {
		return theGameRules.containsKey(par1Str);
	}
}
