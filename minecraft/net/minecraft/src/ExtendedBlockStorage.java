package net.minecraft.src;

public class ExtendedBlockStorage {
	/**
	 * Contains the bottom-most Y block represented by this
	 * ExtendedBlockStorage. Typically a multiple of 16.
	 */
	private final int yBase;

	/**
	 * A total count of the number of non-air blocks in this block storage's
	 * Chunk.
	 */
	private int blockRefCount;

	/**
	 * Contains the number of blocks in this block storage's parent chunk that
	 * require random ticking. Used to cull the Chunk from random tick updates
	 * for performance reasons.
	 */
	private int tickRefCount;

	/**
	 * Contains the least significant 8 bits of each block ID belonging to this
	 * block storage's parent Chunk.
	 */
	private byte[] blockLSBArray;

	/**
	 * Contains the most significant 4 bits of each block ID belonging to this
	 * block storage's parent Chunk.
	 */
	private NibbleArray blockMSBArray;

	/**
	 * Stores the metadata associated with blocks in this ExtendedBlockStorage.
	 */
	private NibbleArray blockMetadataArray;

	/** The NibbleArray containing a block of Block-light data. */
	private NibbleArray blocklightArray;

	/** The NibbleArray containing a block of Sky-light data. */
	private NibbleArray skylightArray;

	public ExtendedBlockStorage(final int par1, final boolean par2) {
		yBase = par1;
		blockLSBArray = new byte[4096];
		blockMetadataArray = new NibbleArray(blockLSBArray.length, 4);
		blocklightArray = new NibbleArray(blockLSBArray.length, 4);

		if (par2) {
			skylightArray = new NibbleArray(blockLSBArray.length, 4);
		}
	}

	/**
	 * Returns the extended block ID for a location in a chunk, merged from a
	 * byte array and a NibbleArray to form a full 12-bit block ID.
	 */
	public int getExtBlockID(final int par1, final int par2, final int par3) {
		final int var4 = blockLSBArray[par2 << 8 | par3 << 4 | par1] & 255;
		return blockMSBArray != null ? blockMSBArray.get(par1, par2, par3) << 8
				| var4 : var4;
	}

	/**
	 * Sets the extended block ID for a location in a chunk, splitting bits
	 * 11..8 into a NibbleArray and bits 7..0 into a byte array. Also performs
	 * reference counting to determine whether or not to broadly cull this Chunk
	 * from the random-update tick list.
	 */
	public void setExtBlockID(final int par1, final int par2, final int par3,
			final int par4) {
		int var5 = blockLSBArray[par2 << 8 | par3 << 4 | par1] & 255;

		if (blockMSBArray != null) {
			var5 |= blockMSBArray.get(par1, par2, par3) << 8;
		}

		if (var5 == 0 && par4 != 0) {
			++blockRefCount;

			if (Block.blocksList[par4] != null
					&& Block.blocksList[par4].getTickRandomly()) {
				++tickRefCount;
			}
		} else if (var5 != 0 && par4 == 0) {
			--blockRefCount;

			if (Block.blocksList[var5] != null
					&& Block.blocksList[var5].getTickRandomly()) {
				--tickRefCount;
			}
		} else if (Block.blocksList[var5] != null
				&& Block.blocksList[var5].getTickRandomly()
				&& (Block.blocksList[par4] == null || !Block.blocksList[par4]
						.getTickRandomly())) {
			--tickRefCount;
		} else if ((Block.blocksList[var5] == null || !Block.blocksList[var5]
				.getTickRandomly())
				&& Block.blocksList[par4] != null
				&& Block.blocksList[par4].getTickRandomly()) {
			++tickRefCount;
		}

		blockLSBArray[par2 << 8 | par3 << 4 | par1] = (byte) (par4 & 255);

		if (par4 > 255) {
			if (blockMSBArray == null) {
				blockMSBArray = new NibbleArray(blockLSBArray.length, 4);
			}

			blockMSBArray.set(par1, par2, par3, (par4 & 3840) >> 8);
		} else if (blockMSBArray != null) {
			blockMSBArray.set(par1, par2, par3, 0);
		}
	}

	/**
	 * Returns the metadata associated with the block at the given coordinates
	 * in this ExtendedBlockStorage.
	 */
	public int getExtBlockMetadata(final int par1, final int par2,
			final int par3) {
		return blockMetadataArray.get(par1, par2, par3);
	}

	/**
	 * Sets the metadata of the Block at the given coordinates in this
	 * ExtendedBlockStorage to the given metadata.
	 */
	public void setExtBlockMetadata(final int par1, final int par2,
			final int par3, final int par4) {
		blockMetadataArray.set(par1, par2, par3, par4);
	}

	/**
	 * Returns whether or not this block storage's Chunk is fully empty, based
	 * on its internal reference count.
	 */
	public boolean isEmpty() {
		return blockRefCount == 0;
	}

	/**
	 * Returns whether or not this block storage's Chunk will require random
	 * ticking, used to avoid looping through random block ticks when there are
	 * no blocks that would randomly tick.
	 */
	public boolean getNeedsRandomTick() {
		return tickRefCount > 0;
	}

	/**
	 * Returns the Y location of this ExtendedBlockStorage.
	 */
	public int getYLocation() {
		return yBase;
	}

	/**
	 * Sets the saved Sky-light value in the extended block storage structure.
	 */
	public void setExtSkylightValue(final int par1, final int par2,
			final int par3, final int par4) {
		skylightArray.set(par1, par2, par3, par4);
	}

	/**
	 * Gets the saved Sky-light value in the extended block storage structure.
	 */
	public int getExtSkylightValue(final int par1, final int par2,
			final int par3) {
		return skylightArray.get(par1, par2, par3);
	}

	/**
	 * Sets the saved Block-light value in the extended block storage structure.
	 */
	public void setExtBlocklightValue(final int par1, final int par2,
			final int par3, final int par4) {
		blocklightArray.set(par1, par2, par3, par4);
	}

	/**
	 * Gets the saved Block-light value in the extended block storage structure.
	 */
	public int getExtBlocklightValue(final int par1, final int par2,
			final int par3) {
		return blocklightArray.get(par1, par2, par3);
	}

	public void removeInvalidBlocks() {
		blockRefCount = 0;
		tickRefCount = 0;

		for (int var1 = 0; var1 < 16; ++var1) {
			for (int var2 = 0; var2 < 16; ++var2) {
				for (int var3 = 0; var3 < 16; ++var3) {
					final int var4 = getExtBlockID(var1, var2, var3);

					if (var4 > 0) {
						if (Block.blocksList[var4] == null) {
							blockLSBArray[var2 << 8 | var3 << 4 | var1] = 0;

							if (blockMSBArray != null) {
								blockMSBArray.set(var1, var2, var3, 0);
							}
						} else {
							++blockRefCount;

							if (Block.blocksList[var4].getTickRandomly()) {
								++tickRefCount;
							}
						}
					}
				}
			}
		}
	}

	public byte[] getBlockLSBArray() {
		return blockLSBArray;
	}

	public void clearMSBArray() {
		blockMSBArray = null;
	}

	/**
	 * Returns the block ID MSB (bits 11..8) array for this storage array's
	 * Chunk.
	 */
	public NibbleArray getBlockMSBArray() {
		return blockMSBArray;
	}

	public NibbleArray getMetadataArray() {
		return blockMetadataArray;
	}

	/**
	 * Returns the NibbleArray instance containing Block-light data.
	 */
	public NibbleArray getBlocklightArray() {
		return blocklightArray;
	}

	/**
	 * Returns the NibbleArray instance containing Sky-light data.
	 */
	public NibbleArray getSkylightArray() {
		return skylightArray;
	}

	/**
	 * Sets the array of block ID least significant bits for this
	 * ExtendedBlockStorage.
	 */
	public void setBlockLSBArray(final byte[] par1ArrayOfByte) {
		blockLSBArray = par1ArrayOfByte;
	}

	/**
	 * Sets the array of blockID most significant bits (blockMSBArray) for this
	 * ExtendedBlockStorage.
	 */
	public void setBlockMSBArray(final NibbleArray par1NibbleArray) {
		blockMSBArray = par1NibbleArray;
	}

	/**
	 * Sets the NibbleArray of block metadata (blockMetadataArray) for this
	 * ExtendedBlockStorage.
	 */
	public void setBlockMetadataArray(final NibbleArray par1NibbleArray) {
		blockMetadataArray = par1NibbleArray;
	}

	/**
	 * Sets the NibbleArray instance used for Block-light values in this
	 * particular storage block.
	 */
	public void setBlocklightArray(final NibbleArray par1NibbleArray) {
		blocklightArray = par1NibbleArray;
	}

	/**
	 * Sets the NibbleArray instance used for Sky-light values in this
	 * particular storage block.
	 */
	public void setSkylightArray(final NibbleArray par1NibbleArray) {
		skylightArray = par1NibbleArray;
	}

	/**
	 * Called by a Chunk to initialize the MSB array if getBlockMSBArray returns
	 * null. Returns the newly-created NibbleArray instance.
	 */
	public NibbleArray createBlockMSBArray() {
		blockMSBArray = new NibbleArray(blockLSBArray.length, 4);
		return blockMSBArray;
	}
}
