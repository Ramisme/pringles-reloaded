package net.minecraft.src;

import org.lwjgl.opengl.GL11;

public class EntityLargeExplodeFX extends EntityFX {
	private int field_70581_a = 0;
	private int field_70584_aq = 0;

	/** The Rendering Engine. */
	private final RenderEngine theRenderEngine;
	private final float field_70582_as;

	public EntityLargeExplodeFX(final RenderEngine par1RenderEngine,
			final World par2World, final double par3, final double par5,
			final double par7, final double par9, final double par11,
			final double par13) {
		super(par2World, par3, par5, par7, 0.0D, 0.0D, 0.0D);
		theRenderEngine = par1RenderEngine;
		field_70584_aq = 6 + rand.nextInt(4);
		particleRed = particleGreen = particleBlue = rand.nextFloat() * 0.6F + 0.4F;
		field_70582_as = 1.0F - (float) par9 * 0.5F;
	}

	@Override
	public void renderParticle(final Tessellator par1Tessellator,
			final float par2, final float par3, final float par4,
			final float par5, final float par6, final float par7) {
		final int var8 = (int) ((field_70581_a + par2) * 15.0F / field_70584_aq);

		if (var8 <= 15) {
			theRenderEngine.bindTexture("/misc/explosion.png");
			final float var9 = var8 % 4 / 4.0F;
			final float var10 = var9 + 0.24975F;
			final float var11 = var8 / 4 / 4.0F;
			final float var12 = var11 + 0.24975F;
			final float var13 = 2.0F * field_70582_as;
			final float var14 = (float) (prevPosX + (posX - prevPosX) * par2 - EntityFX.interpPosX);
			final float var15 = (float) (prevPosY + (posY - prevPosY) * par2 - EntityFX.interpPosY);
			final float var16 = (float) (prevPosZ + (posZ - prevPosZ) * par2 - EntityFX.interpPosZ);
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			GL11.glDisable(GL11.GL_LIGHTING);
			RenderHelper.disableStandardItemLighting();
			par1Tessellator.startDrawingQuads();
			par1Tessellator.setColorRGBA_F(particleRed, particleGreen,
					particleBlue, 1.0F);
			par1Tessellator.setNormal(0.0F, 1.0F, 0.0F);
			par1Tessellator.setBrightness(240);
			par1Tessellator.addVertexWithUV(
					var14 - par3 * var13 - par6 * var13, var15 - par4 * var13,
					var16 - par5 * var13 - par7 * var13, var10, var12);
			par1Tessellator.addVertexWithUV(
					var14 - par3 * var13 + par6 * var13, var15 + par4 * var13,
					var16 - par5 * var13 + par7 * var13, var10, var11);
			par1Tessellator.addVertexWithUV(
					var14 + par3 * var13 + par6 * var13, var15 + par4 * var13,
					var16 + par5 * var13 + par7 * var13, var9, var11);
			par1Tessellator.addVertexWithUV(
					var14 + par3 * var13 - par6 * var13, var15 - par4 * var13,
					var16 + par5 * var13 - par7 * var13, var9, var12);
			par1Tessellator.draw();
			GL11.glPolygonOffset(0.0F, 0.0F);
			GL11.glEnable(GL11.GL_LIGHTING);
		}
	}

	@Override
	public int getBrightnessForRender(final float par1) {
		return 61680;
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate() {
		prevPosX = posX;
		prevPosY = posY;
		prevPosZ = posZ;
		++field_70581_a;

		if (field_70581_a == field_70584_aq) {
			setDead();
		}
	}

	@Override
	public int getFXLayer() {
		return 3;
	}
}
