package net.minecraft.src;

public class BlockTrapDoor extends Block {
	protected BlockTrapDoor(final int par1, final Material par2Material) {
		super(par1, par2Material);
		final float var3 = 0.5F;
		final float var4 = 1.0F;
		setBlockBounds(0.5F - var3, 0.0F, 0.5F - var3, 0.5F + var3, var4,
				0.5F + var3);
		setCreativeTab(CreativeTabs.tabRedstone);
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	@Override
	public boolean getBlocksMovement(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		return !BlockTrapDoor.isTrapdoorOpen(par1IBlockAccess.getBlockMetadata(
				par2, par3, par4));
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return 0;
	}

	/**
	 * Returns the bounding box of the wired rectangular prism to render.
	 */
	@Override
	public AxisAlignedBB getSelectedBoundingBoxFromPool(final World par1World,
			final int par2, final int par3, final int par4) {
		setBlockBoundsBasedOnState(par1World, par2, par3, par4);
		return super
				.getSelectedBoundingBoxFromPool(par1World, par2, par3, par4);
	}

	/**
	 * Returns a bounding box from the pool of bounding boxes (this means this
	 * box can change after the pool has been cleared to be reused)
	 */
	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(final World par1World,
			final int par2, final int par3, final int par4) {
		setBlockBoundsBasedOnState(par1World, par2, par3, par4);
		return super.getCollisionBoundingBoxFromPool(par1World, par2, par3,
				par4);
	}

	/**
	 * Updates the blocks bounds based on its current state. Args: world, x, y,
	 * z
	 */
	@Override
	public void setBlockBoundsBasedOnState(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		setBlockBoundsForBlockRender(par1IBlockAccess.getBlockMetadata(par2,
				par3, par4));
	}

	/**
	 * Sets the block's bounds for rendering it as an item
	 */
	@Override
	public void setBlockBoundsForItemRender() {
		final float var1 = 0.1875F;
		setBlockBounds(0.0F, 0.5F - var1 / 2.0F, 0.0F, 1.0F,
				0.5F + var1 / 2.0F, 1.0F);
	}

	public void setBlockBoundsForBlockRender(final int par1) {
		final float var2 = 0.1875F;

		if ((par1 & 8) != 0) {
			setBlockBounds(0.0F, 1.0F - var2, 0.0F, 1.0F, 1.0F, 1.0F);
		} else {
			setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, var2, 1.0F);
		}

		if (BlockTrapDoor.isTrapdoorOpen(par1)) {
			if ((par1 & 3) == 0) {
				setBlockBounds(0.0F, 0.0F, 1.0F - var2, 1.0F, 1.0F, 1.0F);
			}

			if ((par1 & 3) == 1) {
				setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, var2);
			}

			if ((par1 & 3) == 2) {
				setBlockBounds(1.0F - var2, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
			}

			if ((par1 & 3) == 3) {
				setBlockBounds(0.0F, 0.0F, 0.0F, var2, 1.0F, 1.0F);
			}
		}
	}

	/**
	 * Called when the block is clicked by a player. Args: x, y, z, entityPlayer
	 */
	@Override
	public void onBlockClicked(final World par1World, final int par2,
			final int par3, final int par4, final EntityPlayer par5EntityPlayer) {
	}

	/**
	 * Called upon block activation (right click on the block.)
	 */
	@Override
	public boolean onBlockActivated(final World par1World, final int par2,
			final int par3, final int par4,
			final EntityPlayer par5EntityPlayer, final int par6,
			final float par7, final float par8, final float par9) {
		if (blockMaterial == Material.iron) {
			return true;
		} else {
			final int var10 = par1World.getBlockMetadata(par2, par3, par4);
			par1World
					.setBlockMetadataWithNotify(par2, par3, par4, var10 ^ 4, 2);
			par1World.playAuxSFXAtEntity(par5EntityPlayer, 1003, par2, par3,
					par4, 0);
			return true;
		}
	}

	public void onPoweredBlockChange(final World par1World, final int par2,
			final int par3, final int par4, final boolean par5) {
		final int var6 = par1World.getBlockMetadata(par2, par3, par4);
		final boolean var7 = (var6 & 4) > 0;

		if (var7 != par5) {
			par1World.setBlockMetadataWithNotify(par2, par3, par4, var6 ^ 4, 2);
			par1World.playAuxSFXAtEntity((EntityPlayer) null, 1003, par2, par3,
					par4, 0);
		}
	}

	/**
	 * Lets the block know when one of its neighbor changes. Doesn't know which
	 * neighbor changed (coordinates passed are their own) Args: x, y, z,
	 * neighbor blockID
	 */
	@Override
	public void onNeighborBlockChange(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		if (!par1World.isRemote) {
			final int var6 = par1World.getBlockMetadata(par2, par3, par4);
			int var7 = par2;
			int var8 = par4;

			if ((var6 & 3) == 0) {
				var8 = par4 + 1;
			}

			if ((var6 & 3) == 1) {
				--var8;
			}

			if ((var6 & 3) == 2) {
				var7 = par2 + 1;
			}

			if ((var6 & 3) == 3) {
				--var7;
			}

			if (!BlockTrapDoor.isValidSupportBlock(par1World.getBlockId(var7,
					par3, var8))) {
				par1World.setBlockToAir(par2, par3, par4);
				dropBlockAsItem(par1World, par2, par3, par4, var6, 0);
			}

			final boolean var9 = par1World.isBlockIndirectlyGettingPowered(
					par2, par3, par4);

			if (var9 || par5 > 0 && Block.blocksList[par5].canProvidePower()) {
				onPoweredBlockChange(par1World, par2, par3, par4, var9);
			}
		}
	}

	/**
	 * Ray traces through the blocks collision from start vector to end vector
	 * returning a ray trace hit. Args: world, x, y, z, startVec, endVec
	 */
	@Override
	public MovingObjectPosition collisionRayTrace(final World par1World,
			final int par2, final int par3, final int par4,
			final Vec3 par5Vec3, final Vec3 par6Vec3) {
		setBlockBoundsBasedOnState(par1World, par2, par3, par4);
		return super.collisionRayTrace(par1World, par2, par3, par4, par5Vec3,
				par6Vec3);
	}

	/**
	 * Called when a block is placed using its ItemBlock. Args: World, X, Y, Z,
	 * side, hitX, hitY, hitZ, block metadata
	 */
	@Override
	public int onBlockPlaced(final World par1World, final int par2,
			final int par3, final int par4, final int par5, final float par6,
			final float par7, final float par8, final int par9) {
		int var10 = 0;

		if (par5 == 2) {
			var10 = 0;
		}

		if (par5 == 3) {
			var10 = 1;
		}

		if (par5 == 4) {
			var10 = 2;
		}

		if (par5 == 5) {
			var10 = 3;
		}

		if (par5 != 1 && par5 != 0 && par7 > 0.5F) {
			var10 |= 8;
		}

		return var10;
	}

	/**
	 * checks to see if you can place this block can be placed on that side of a
	 * block: BlockLever overrides
	 */
	@Override
	public boolean canPlaceBlockOnSide(final World par1World, int par2,
			final int par3, int par4, final int par5) {
		if (par5 == 0) {
			return false;
		} else if (par5 == 1) {
			return false;
		} else {
			if (par5 == 2) {
				++par4;
			}

			if (par5 == 3) {
				--par4;
			}

			if (par5 == 4) {
				++par2;
			}

			if (par5 == 5) {
				--par2;
			}

			return BlockTrapDoor.isValidSupportBlock(par1World.getBlockId(par2,
					par3, par4));
		}
	}

	public static boolean isTrapdoorOpen(final int par0) {
		return (par0 & 4) != 0;
	}

	/**
	 * Checks if the block ID is a valid support block for the trap door to
	 * connect with. If it is not the trapdoor is dropped into the world.
	 */
	private static boolean isValidSupportBlock(final int par0) {
		if (par0 <= 0) {
			return false;
		} else {
			final Block var1 = Block.blocksList[par0];
			return var1 != null && var1.blockMaterial.isOpaque()
					&& var1.renderAsNormalBlock() || var1 == Block.glowStone
					|| var1 instanceof BlockHalfSlab
					|| var1 instanceof BlockStairs;
		}
	}
}
