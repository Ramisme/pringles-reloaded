package net.minecraft.src;

public class ItemFishingRod extends Item {
	private Icon theIcon;

	public ItemFishingRod(final int par1) {
		super(par1);
		setMaxDamage(64);
		setMaxStackSize(1);
		setCreativeTab(CreativeTabs.tabTools);
	}

	/**
	 * Returns True is the item is renderer in full 3D when hold.
	 */
	@Override
	public boolean isFull3D() {
		return true;
	}

	/**
	 * Returns true if this item should be rotated by 180 degrees around the Y
	 * axis when being held in an entities hands.
	 */
	@Override
	public boolean shouldRotateAroundWhenRendering() {
		return true;
	}

	/**
	 * Called whenever this item is equipped and the right mouse button is
	 * pressed. Args: itemStack, world, entityPlayer
	 */
	@Override
	public ItemStack onItemRightClick(final ItemStack par1ItemStack,
			final World par2World, final EntityPlayer par3EntityPlayer) {
		if (par3EntityPlayer.fishEntity != null) {
			final int var4 = par3EntityPlayer.fishEntity.catchFish();
			par1ItemStack.damageItem(var4, par3EntityPlayer);
			par3EntityPlayer.swingItem();
		} else {
			par2World.playSoundAtEntity(par3EntityPlayer, "random.bow", 0.5F,
					0.4F / (Item.itemRand.nextFloat() * 0.4F + 0.8F));

			if (!par2World.isRemote) {
				par2World.spawnEntityInWorld(new EntityFishHook(par2World,
						par3EntityPlayer));
			}

			par3EntityPlayer.swingItem();
		}

		return par1ItemStack;
	}

	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		super.registerIcons(par1IconRegister);
		theIcon = par1IconRegister.registerIcon("fishingRod_empty");
	}

	public Icon func_94597_g() {
		return theIcon;
	}
}
