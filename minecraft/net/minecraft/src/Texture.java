package net.minecraft.src;

import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import net.minecraft.client.Minecraft;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GLContext;
import org.lwjgl.util.Dimension;

public class Texture {
	private int glTextureId;
	private final int textureId;
	private final int textureType;

	/** Width of this texture in pixels. */
	private int width;

	/** Height of this texture in pixels. */
	private int height;
	private final int textureDepth;
	private final int textureFormat;
	private final int textureTarget;
	private final int textureMinFilter;
	private final int textureMagFilter;
	private final int textureWrap;
	private final boolean mipmapActive;
	private final String textureName;
	private Rect2i textureRect;
	/**
	 * Uninitialized boolean. If true, the texture is re-uploaded every time
	 * it's modified. If false, every tick after it's been modified at least
	 * once in that tick.
	 */
	private boolean autoCreate;

	/**
	 * False if the texture has been modified since it was last uploaded to the
	 * GPU.
	 */
	private boolean textureNotModified;
	private ByteBuffer textureData;
	private boolean textureBound;
	public ByteBuffer[] mipmapDatas;
	public Dimension[] mipmapDimensions;

	private Texture(final String par1Str, final int par2, final int par3,
			final int par4, final int par5, final int par6, final int par7,
			int par8, final int par9) {
		textureName = par1Str;
		textureType = par2;
		width = par3;
		height = par4;
		textureDepth = par5;
		textureFormat = par7;

		if (Config.isUseMipmaps()
				&& Texture.isMipMapTexture(textureType, textureName)) {
			par8 = Config.getMipmapType();
		}

		textureMinFilter = par8;
		textureMagFilter = par9;
		final char par61 = 33071;
		textureWrap = par61;
		textureRect = new Rect2i(0, 0, par3, par4);

		if (par4 == 1 && par5 == 1) {
			textureTarget = 3552;
		} else if (par5 == 1) {
			textureTarget = 3553;
		} else {
			textureTarget = 32879;
		}

		mipmapActive = par8 != 9728 && par8 != 9729 || par9 != 9728
				&& par9 != 9729;

		if (par2 != 2) {
			glTextureId = GL11.glGenTextures();
			GL11.glBindTexture(textureTarget, glTextureId);
			GL11.glTexParameteri(textureTarget, GL11.GL_TEXTURE_MIN_FILTER,
					par8);
			GL11.glTexParameteri(textureTarget, GL11.GL_TEXTURE_MAG_FILTER,
					par9);

			if (mipmapActive) {
				updateMipmapLevel(-1);
			}

			GL11.glTexParameteri(textureTarget, GL11.GL_TEXTURE_WRAP_S, par61);
			GL11.glTexParameteri(textureTarget, GL11.GL_TEXTURE_WRAP_T, par61);
		} else {
			glTextureId = -1;
		}

		textureId = TextureManager.instance().getNextTextureId();
	}

	public Texture(final String par1Str, final int par2, final int par3,
			final int par4, final int par5, final int par6, final int par7,
			final int par8, final BufferedImage par9BufferedImage) {
		this(par1Str, par2, par3, par4, 1, par5, par6, par7, par8,
				par9BufferedImage);
	}

	public Texture(final String par1Str, final int par2, final int par3,
			final int par4, final int par5, final int par6, final int par7,
			final int par8, final int par9,
			final BufferedImage par10BufferedImage) {
		this(par1Str, par2, par3, par4, par5, par6, par7, par8, par9);

		if (par10BufferedImage == null) {
			if (par3 != -1 && par4 != -1) {
				final byte[] var11 = new byte[par3 * par4 * par5 * 4];

				for (int var12 = 0; var12 < var11.length; ++var12) {
					var11[var12] = 0;
				}

				textureData = GLAllocation.createDirectByteBuffer(var11.length);
				textureData.clear();
				textureData.put(var11);
				textureData.position(0).limit(var11.length);

				if (autoCreate) {
					uploadTexture();
				} else {
					textureNotModified = false;
				}
			} else {
			}
		} else {
			transferFromImage(par10BufferedImage);

			if (par2 != 2) {
				uploadTexture();
				autoCreate = false;
			}
		}
	}

	public final Rect2i getTextureRect() {
		return textureRect;
	}

	public void fillRect(final Rect2i par1Rect2i, final int par2) {
		if (textureTarget != 32879) {
			final Rect2i var3 = new Rect2i(0, 0, width, height);
			var3.intersection(par1Rect2i);
			textureData.position(0);

			for (int var4 = var3.getRectY(); var4 < var3.getRectY()
					+ var3.getRectHeight(); ++var4) {
				final int var5 = var4 * width * 4;

				for (int var6 = var3.getRectX(); var6 < var3.getRectX()
						+ var3.getRectWidth(); ++var6) {
					textureData.put(var5 + var6 * 4 + 0,
							(byte) (par2 >> 24 & 255));
					textureData.put(var5 + var6 * 4 + 1,
							(byte) (par2 >> 16 & 255));
					textureData.put(var5 + var6 * 4 + 2,
							(byte) (par2 >> 8 & 255));
					textureData.put(var5 + var6 * 4 + 3,
							(byte) (par2 >> 0 & 255));
				}
			}

			if (par1Rect2i.getRectX() == 0 && par1Rect2i.getRectY() == 0
					&& par1Rect2i.getRectWidth() == width
					&& par1Rect2i.getRectHeight() == height) {
				textureNotModified = false;
			}

			if (autoCreate) {
				uploadTexture();
			} else {
				textureNotModified = false;
			}
		}
	}

	public void writeImage(final String par1Str) {
		final BufferedImage var2 = new BufferedImage(width, height, 2);
		final ByteBuffer var3 = getTextureData();
		final byte[] var4 = new byte[width * height * 4];
		var3.position(0);
		var3.get(var4);

		for (int var5 = 0; var5 < width; ++var5) {
			for (int var6 = 0; var6 < height; ++var6) {
				final int var7 = var6 * width * 4 + var5 * 4;
				final byte var8 = 0;
				int var9 = var8 | (var4[var7 + 2] & 255) << 0;
				var9 |= (var4[var7 + 1] & 255) << 8;
				var9 |= (var4[var7 + 0] & 255) << 16;
				var9 |= (var4[var7 + 3] & 255) << 24;
				var2.setRGB(var5, var6, var9);
			}
		}

		textureData.position(width * height * 4);

		try {
			ImageIO.write(var2, "png", new File(Minecraft.getMinecraftDir(),
					par1Str));
		} catch (final Exception var10) {
			var10.printStackTrace();
		}
	}

	public void copyFrom(final int par1, final int par2,
			final Texture par3Texture, final boolean par4) {
		if (textureTarget != 32879) {
			ByteBuffer var5;

			if (textureNotModified) {
				if (!textureBound) {
					return;
				}

				var5 = par3Texture.getTextureData();
				var5.position(0);
				GL11.glTexSubImage2D(textureTarget, 0, par1, par2,
						par3Texture.getWidth(), par3Texture.getHeight(),
						textureFormat, GL11.GL_UNSIGNED_BYTE, var5);

				if (mipmapActive) {
					if (par3Texture.mipmapDatas == null) {
						par3Texture.generateMipMapData();
					}

					final ByteBuffer[] var13 = par3Texture.mipmapDatas;
					final Dimension[] var14 = par3Texture.mipmapDimensions;

					if (var13 != null && var14 != null) {
						registerMipMapsSub(par1, par2, var13, var14);
					}
				}

				return;
			}

			var5 = par3Texture.getTextureData();
			textureData.position(0);
			var5.position(0);

			for (int var6 = 0; var6 < par3Texture.getHeight(); ++var6) {
				int var7 = par2 + var6;
				final int var8 = var6 * par3Texture.getWidth() * 4;
				final int var9 = var7 * width * 4;

				if (par4) {
					var7 = par2 + par3Texture.getHeight() - var6;
				}

				for (int var10 = 0; var10 < par3Texture.getWidth(); ++var10) {
					int var11 = var9 + (var10 + par1) * 4;
					final int var12 = var8 + var10 * 4;

					if (par4) {
						var11 = par1 + var10 * width * 4 + var7 * 4;
					}

					textureData.put(var11 + 0, var5.get(var12 + 0));
					textureData.put(var11 + 1, var5.get(var12 + 1));
					textureData.put(var11 + 2, var5.get(var12 + 2));
					textureData.put(var11 + 3, var5.get(var12 + 3));
				}
			}

			textureData.position(width * height * 4);

			if (autoCreate) {
				uploadTexture();
			} else {
				textureNotModified = false;
			}
		}
	}

	public void func_104062_b(final int par1, final int par2,
			final Texture par3Texture) {
		if (!textureBound) {
			Config.getRenderEngine().bindTexture(glTextureId);
		}

		GL11.glTexSubImage2D(textureTarget, 0, par1, par2, par3Texture
				.getWidth(), par3Texture.getHeight(), textureFormat,
				GL11.GL_UNSIGNED_BYTE, (ByteBuffer) par3Texture
						.getTextureData().position(0));
		textureNotModified = true;

		if (mipmapActive) {
			if (par3Texture.mipmapDatas == null) {
				par3Texture.generateMipMapData();
			}

			final ByteBuffer[] var4 = par3Texture.mipmapDatas;
			final Dimension[] var5 = par3Texture.mipmapDimensions;

			if (var4 != null && var5 != null) {
				registerMipMapsSub(par1, par2, var4, var5);
			}
		}
	}

	public void transferFromImage(final BufferedImage par1BufferedImage) {
		if (textureTarget != 32879) {
			final int var2 = par1BufferedImage.getWidth();
			final int var3 = par1BufferedImage.getHeight();

			if (var2 <= width && var3 <= height) {
				final int[] var4 = new int[] { 3, 0, 1, 2 };
				final int[] var5 = new int[] { 3, 2, 1, 0 };
				final int[] var6 = textureFormat == 32993 ? var5 : var4;
				final int[] var7 = new int[width * height];
				par1BufferedImage.getTransparency();
				par1BufferedImage.getRGB(0, 0, width, height, var7, 0, var2);
				final byte[] var9 = new byte[width * height * 4];
				long var10 = 0L;
				long var12 = 0L;
				long var14 = 0L;
				long var16 = 0L;
				int var19;
				int var18;
				int var21;
				int var20;
				int var23;
				int var22;
				int var24;

				for (var18 = 0; var18 < height; ++var18) {
					for (var19 = 0; var19 < width; ++var19) {
						var20 = var18 * width + var19;
						var21 = var7[var20];
						var22 = var21 >> 24 & 255;

						if (var22 != 0) {
							var23 = var21 >> 16 & 255;
							var24 = var21 >> 8 & 255;
							final int var25 = var21 & 255;
							var10 += var23;
							var12 += var24;
							var14 += var25;
							++var16;
						}
					}
				}

				var18 = 0;
				var19 = 0;
				var20 = 0;

				if (var16 > 0L) {
					var18 = (int) (var10 / var16);
					var19 = (int) (var12 / var16);
					var20 = (int) (var14 / var16);
				}

				for (var21 = 0; var21 < height; ++var21) {
					for (var22 = 0; var22 < width; ++var22) {
						var23 = var21 * width + var22;
						var24 = var23 * 4;
						var9[var24 + var6[0]] = (byte) (var7[var23] >> 24 & 255);
						var9[var24 + var6[1]] = (byte) (var7[var23] >> 16 & 255);
						var9[var24 + var6[2]] = (byte) (var7[var23] >> 8 & 255);
						var9[var24 + var6[3]] = (byte) (var7[var23] >> 0 & 255);
						final byte var26 = (byte) (var7[var23] >> 24 & 255);

						if (var26 == 0) {
							var9[var24 + var6[1]] = (byte) var18;
							var9[var24 + var6[2]] = (byte) var19;
							var9[var24 + var6[3]] = (byte) var20;
						}
					}
				}

				textureData = GLAllocation.createDirectByteBuffer(var9.length);
				textureData.clear();
				textureData.put(var9);
				textureData.limit(var9.length);

				if (autoCreate) {
					uploadTexture();
				} else {
					textureNotModified = false;
				}
			} else {
				Minecraft
						.getMinecraft()
						.getLogAgent()
						.logWarning(
								"transferFromImage called with a BufferedImage with dimensions ("
										+ var2
										+ ", "
										+ var3
										+ ") larger than the Texture dimensions ("
										+ width + ", " + height
										+ "). Ignoring.");
			}
		}
	}

	public int getTextureId() {
		return textureId;
	}

	public int getGlTextureId() {
		return glTextureId;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public String getTextureName() {
		return textureName;
	}

	public void bindTexture(final int par1) {
		Config.getRenderEngine().bindTexture(glTextureId);
	}

	public void uploadTexture() {
		if (glTextureId <= 0) {
			glTextureId = GL11.glGenTextures();
			GL11.glBindTexture(textureTarget, glTextureId);
			GL11.glTexParameteri(textureTarget, GL11.GL_TEXTURE_MIN_FILTER,
					textureMinFilter);
			GL11.glTexParameteri(textureTarget, GL11.GL_TEXTURE_MAG_FILTER,
					textureMagFilter);

			if (mipmapActive) {
				updateMipmapLevel(16);
			}

			GL11.glTexParameteri(textureTarget, GL11.GL_TEXTURE_WRAP_S,
					GL11.GL_CLAMP);
			GL11.glTexParameteri(textureTarget, GL11.GL_TEXTURE_WRAP_T,
					GL11.GL_CLAMP);
		}

		textureData.clear();

		if (height != 1 && textureDepth != 1) {
			GL12.glTexImage3D(textureTarget, 0, textureFormat, width, height,
					textureDepth, 0, textureFormat, GL11.GL_UNSIGNED_BYTE,
					textureData);
		} else if (height != 1) {
			GL11.glTexImage2D(textureTarget, 0, textureFormat, width, height,
					0, textureFormat, GL11.GL_UNSIGNED_BYTE, textureData);

			if (mipmapActive) {
				generateMipMaps(true);
			}
		} else {
			GL11.glTexImage1D(textureTarget, 0, textureFormat, width, 0,
					textureFormat, GL11.GL_UNSIGNED_BYTE, textureData);
		}

		textureNotModified = true;
	}

	public ByteBuffer getTextureData() {
		return textureData;
	}

	public void generateMipMapData() {
		generateMipMaps(false);
	}

	private void generateMipMaps(final boolean var1) {
		if (mipmapDatas == null) {
			allocateMipmapDatas();
		}

		ByteBuffer var2 = textureData;
		int var3 = width;
		boolean var4 = true;

		for (int var5 = 0; var5 < mipmapDatas.length; ++var5) {
			final ByteBuffer var6 = mipmapDatas[var5];
			final int var7 = var5 + 1;
			final Dimension var8 = mipmapDimensions[var5];
			final int var9 = var8.getWidth();
			final int var10 = var8.getHeight();

			if (var4) {
				var6.clear();
				var2.clear();

				for (int var11 = 0; var11 < var9; ++var11) {
					for (int var12 = 0; var12 < var10; ++var12) {
						final int var13 = var2
								.getInt((var11 * 2 + 0 + (var12 * 2 + 0) * var3) * 4);
						final int var14 = var2
								.getInt((var11 * 2 + 1 + (var12 * 2 + 0) * var3) * 4);
						final int var15 = var2
								.getInt((var11 * 2 + 1 + (var12 * 2 + 1) * var3) * 4);
						final int var16 = var2
								.getInt((var11 * 2 + 0 + (var12 * 2 + 1) * var3) * 4);
						final int var17 = this.alphaBlend(var13, var14, var15,
								var16);
						var6.putInt((var11 + var12 * var9) * 4, var17);
					}
				}

				var6.clear();
				var2.clear();
			}

			if (var1) {
				GL11.glTexImage2D(textureTarget, var7, textureFormat, var9,
						var10, 0, textureFormat, GL11.GL_UNSIGNED_BYTE, var6);
			}

			var2 = var6;
			var3 = var9;

			if (var9 <= 1 || var10 <= 1) {
				var4 = false;
			}
		}
	}

	private void registerMipMapsSub(final int var1, final int var2,
			final ByteBuffer[] var3, final Dimension[] var4) {
		int var5 = var1 / 2;
		int var6 = var2 / 2;

		for (int var7 = 0; var7 < var3.length; ++var7) {
			final ByteBuffer var8 = var3[var7];
			final int var9 = var7 + 1;
			final Dimension var10 = var4[var7];
			final int var11 = var10.getWidth();
			final int var12 = var10.getHeight();
			var8.clear();
			GL11.glTexSubImage2D(textureTarget, var9, var5, var6, var11, var12,
					textureFormat, GL11.GL_UNSIGNED_BYTE, var8);
			var5 /= 2;
			var6 /= 2;
		}
	}

	private int alphaBlend(final int var1, final int var2, final int var3,
			final int var4) {
		final int var5 = this.alphaBlend(var1, var2);
		final int var6 = this.alphaBlend(var3, var4);
		final int var7 = this.alphaBlend(var5, var6);
		return var7;
	}

	private int alphaBlend(int var1, int var2) {
		int var3 = (var1 & -16777216) >> 24 & 255;
		int var4 = (var2 & -16777216) >> 24 & 255;
		int var5 = (var3 + var4) / 2;

		if (var3 == 0 && var4 == 0) {
			var3 = 1;
			var4 = 1;
		} else {
			if (var3 == 0) {
				var1 = var2;
				var5 /= 2;
			}

			if (var4 == 0) {
				var2 = var1;
				var5 /= 2;
			}
		}

		final int var6 = (var1 >> 16 & 255) * var3;
		final int var7 = (var1 >> 8 & 255) * var3;
		final int var8 = (var1 & 255) * var3;
		final int var9 = (var2 >> 16 & 255) * var4;
		final int var10 = (var2 >> 8 & 255) * var4;
		final int var11 = (var2 & 255) * var4;
		final int var12 = (var6 + var9) / (var3 + var4);
		final int var13 = (var7 + var10) / (var3 + var4);
		final int var14 = (var8 + var11) / (var3 + var4);
		return var5 << 24 | var12 << 16 | var13 << 8 | var14;
	}

	private void allocateMipmapDatas() {
		final int var1 = TextureUtils.ceilPowerOfTwo(width);
		final int var2 = TextureUtils.ceilPowerOfTwo(height);

		if (var1 == width && var2 == height) {
			final ArrayList var4 = new ArrayList();
			final ArrayList var5 = new ArrayList();
			int var6 = var1;
			int var7 = var2;

			while (true) {
				var6 /= 2;
				var7 /= 2;

				if (var6 <= 0 && var7 <= 0) {
					mipmapDatas = (ByteBuffer[]) var4
							.toArray(new ByteBuffer[var4.size()]);
					mipmapDimensions = (Dimension[]) var5
							.toArray(new Dimension[var5.size()]);
					return;
				}

				if (var6 <= 0) {
					var6 = 1;
				}

				if (var7 <= 0) {
					var7 = 1;
				}

				final int var8 = var6 * var7 * 4;
				final ByteBuffer var9 = GLAllocation
						.createDirectByteBuffer(var8);
				var4.add(var9);
				final Dimension var10 = new Dimension(var6, var7);
				var5.add(var10);
			}
		} else {
			Config.dbg("Mipmaps not possible (power of 2 dimensions needed), texture: "
					+ textureName + ", dim: " + width + "x" + height);
			mipmapDatas = new ByteBuffer[0];
			mipmapDimensions = new Dimension[0];
		}
	}

	private int getMaxMipmapLevel(int var1) {
		int var2;

		for (var2 = 0; var1 > 0; ++var2) {
			var1 /= 2;
		}

		return var2 - 1;
	}

	public static boolean isMipMapTexture(final int var0, final String var1) {
		return var0 == 3 ? true : var0 == 2 ? false : var1.equals("terrain");
	}

	public void scaleUp(final int var1) {
		if (textureTarget == 3553) {
			final int var2 = TextureUtils.ceilPowerOfTwo(var1);
			final int var3 = Math.max(width, height);

			for (int var4 = TextureUtils.ceilPowerOfTwo(var3); var4 < var2; var4 *= 2) {
				scale2x();
			}
		}
	}

	private void scale2x() {
		final int var1 = width;
		final byte[] var3 = new byte[width * height * 4];
		textureData.position(0);
		textureData.get(var3);
		width *= 2;
		height *= 2;
		textureRect = new Rect2i(0, 0, width, height);
		textureData = GLAllocation.createDirectByteBuffer(width * height * 4);
		copyScaled(var3, var1, textureData, width);
	}

	private void copyScaled(final byte[] var1, final int var2,
			final ByteBuffer var3, final int var4) {
		final int var5 = var4 / var2;
		final byte[] var6 = new byte[4];
		var3.clear();

		if (var5 > 1) {
			for (int var8 = 0; var8 < var2; ++var8) {
				final int var9 = var8 * var2;
				final int var10 = var8 * var5;
				final int var11 = var10 * var4;

				for (int var12 = 0; var12 < var2; ++var12) {
					final int var13 = (var12 + var9) * 4;
					var6[0] = var1[var13];
					var6[1] = var1[var13 + 1];
					var6[2] = var1[var13 + 2];
					var6[3] = var1[var13 + 3];
					final int var14 = var12 * var5;
					final int var15 = var14 + var11;

					for (int var16 = 0; var16 < var5; ++var16) {
						final int var17 = var15 + var16 * var4;
						var3.position(var17 * 4);

						for (int var18 = 0; var18 < var5; ++var18) {
							var3.put(var6);
						}
					}
				}
			}
		}

		var3.position(0).limit(var4 * var4 * 4);
	}

	public void updateMipmapLevel(final int var1) {
		if (mipmapActive) {
			if (GLContext.getCapabilities().OpenGL12) {
				GL11.glTexParameteri(GL11.GL_TEXTURE_2D,
						GL12.GL_TEXTURE_BASE_LEVEL, 0);
				int var2 = Config.getMipmapLevel();

				if (var2 >= 4) {
					final int var3 = Math.min(width, height);
					var2 = getMaxMipmapLevel(var3);

					if (var1 > 1) {
						final int var4 = TextureUtils.getPowerOfTwo(var1);
						var2 = var4;
					}

					if (var2 < 0) {
						var2 = 0;
					}
				}

				GL11.glTexParameteri(GL11.GL_TEXTURE_2D,
						GL12.GL_TEXTURE_MAX_LEVEL, var2);
			}

			if (Config.getAnisotropicFilterLevel() > 1
					&& GLContext.getCapabilities().GL_EXT_texture_filter_anisotropic) {
				final FloatBuffer var6 = BufferUtils.createFloatBuffer(16);
				var6.rewind();
				GL11.glGetFloat(34047, var6);
				final float var5 = var6.get(0);
				float var7 = Config.getAnisotropicFilterLevel();
				var7 = Math.min(var7, var5);
				GL11.glTexParameterf(GL11.GL_TEXTURE_2D, 34046, var7);
			}
		}
	}

	public void setTextureBound(final boolean var1) {
		textureBound = var1;
	}

	public boolean isTextureBound() {
		return textureBound;
	}

	public void deleteTexture() {
		if (glTextureId > 0) {
			GL11.glDeleteTextures(glTextureId);
			glTextureId = 0;
		}

		textureData = null;
		mipmapDatas = null;
		mipmapDimensions = null;
	}

	@Override
	public String toString() {
		return "Texture: " + textureName + ", dim: " + width + "x" + height
				+ ", gl: " + glTextureId + ", created: " + textureNotModified;
	}

	public Texture duplicate(final int var1) {
		final Texture var2 = new Texture(textureName, var1, width, height,
				textureDepth, textureWrap, textureFormat, textureMinFilter,
				textureMagFilter);
		textureData.clear();
		var2.textureData = GLAllocation.createDirectByteBuffer(textureData
				.capacity());
		var2.textureData.put(textureData);
		textureData.clear();
		var2.textureData.clear();
		return var2;
	}

	public void createAndUploadTexture() {
		Config.dbg("Forge method not implemented: TextureStitched.createAndUploadTexture()");
	}
}
