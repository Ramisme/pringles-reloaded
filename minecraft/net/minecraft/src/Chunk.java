package net.minecraft.src;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Chunk {
	/**
	 * Determines if the chunk is lit or not at a light value greater than 0.
	 */
	public static boolean isLit;

	/**
	 * Used to store block IDs, block MSBs, Sky-light maps, Block-light maps,
	 * and metadata. Each entry corresponds to a logical segment of 16x16x16
	 * blocks, stacked vertically.
	 */
	private ExtendedBlockStorage[] storageArrays;

	/**
	 * Contains a 16x16 mapping on the X/Z plane of the biome ID to which each
	 * colum belongs.
	 */
	private byte[] blockBiomeArray;

	/**
	 * A map, similar to heightMap, that tracks how far down precipitation can
	 * fall.
	 */
	public int[] precipitationHeightMap;

	/** Which columns need their skylightMaps updated. */
	public boolean[] updateSkylightColumns;

	/** Whether or not this Chunk is currently loaded into the World */
	public boolean isChunkLoaded;

	/** Reference to the World object. */
	public World worldObj;
	public int[] heightMap;

	/** The x coordinate of the chunk. */
	public final int xPosition;

	/** The z coordinate of the chunk. */
	public final int zPosition;
	private boolean isGapLightingUpdated;

	/** A Map of ChunkPositions to TileEntities in this chunk */
	public Map chunkTileEntityMap;

	/**
	 * Array of Lists containing the entities in this Chunk. Each List
	 * represents a 16 block subchunk.
	 */
	public List[] entityLists;

	/** Boolean value indicating if the terrain is populated. */
	public boolean isTerrainPopulated;

	/**
	 * Set to true if the chunk has been modified and needs to be updated
	 * internally.
	 */
	public boolean isModified;

	/**
	 * Whether this Chunk has any Entities and thus requires saving on every
	 * tick
	 */
	public boolean hasEntities;

	/** The time according to World.worldTime when this chunk was last saved */
	public long lastSaveTime;

	/**
	 * Updates to this chunk will not be sent to clients if this is false. This
	 * field is set to true the first time the chunk is sent to a client, and
	 * never set to false.
	 */
	public boolean sendUpdates;

	/** Lowest value in the heightmap. */
	public int heightMapMinimum;

	/**
	 * Contains the current round-robin relight check index, and is implied as
	 * the relight check location as well.
	 */
	private int queuedLightChecks;
	boolean field_76653_p;

	public Chunk(final World par1World, final int par2, final int par3) {
		storageArrays = new ExtendedBlockStorage[16];
		blockBiomeArray = new byte[256];
		precipitationHeightMap = new int[256];
		updateSkylightColumns = new boolean[256];
		isGapLightingUpdated = false;
		chunkTileEntityMap = new HashMap();
		isTerrainPopulated = false;
		isModified = false;
		hasEntities = false;
		lastSaveTime = 0L;
		sendUpdates = false;
		heightMapMinimum = 0;
		queuedLightChecks = 4096;
		field_76653_p = false;
		entityLists = new List[16];
		worldObj = par1World;
		xPosition = par2;
		zPosition = par3;
		heightMap = new int[256];

		for (int var4 = 0; var4 < entityLists.length; ++var4) {
			entityLists[var4] = new ArrayList();
		}

		Arrays.fill(precipitationHeightMap, -999);
		Arrays.fill(blockBiomeArray, (byte) -1);
	}

	public Chunk(final World par1World, final byte[] par2ArrayOfByte,
			final int par3, final int par4) {
		this(par1World, par3, par4);
		final int var5 = par2ArrayOfByte.length / 256;

		for (int var6 = 0; var6 < 16; ++var6) {
			for (int var7 = 0; var7 < 16; ++var7) {
				for (int var8 = 0; var8 < var5; ++var8) {
					final byte var9 = par2ArrayOfByte[var6 << 11 | var7 << 7
							| var8];

					if (var9 != 0) {
						final int var10 = var8 >> 4;

						if (storageArrays[var10] == null) {
							storageArrays[var10] = new ExtendedBlockStorage(
									var10 << 4, !par1World.provider.hasNoSky);
						}

						storageArrays[var10].setExtBlockID(var6, var8 & 15,
								var7, var9);
					}
				}
			}
		}
	}

	/**
	 * Checks whether the chunk is at the X/Z location specified
	 */
	public boolean isAtLocation(final int par1, final int par2) {
		return par1 == xPosition && par2 == zPosition;
	}

	/**
	 * Returns the value in the height map at this x, z coordinate in the chunk
	 */
	public int getHeightValue(final int par1, final int par2) {
		return heightMap[par2 << 4 | par1];
	}

	/**
	 * Returns the topmost ExtendedBlockStorage instance for this Chunk that
	 * actually contains a block.
	 */
	public int getTopFilledSegment() {
		for (int var1 = storageArrays.length - 1; var1 >= 0; --var1) {
			if (storageArrays[var1] != null) {
				return storageArrays[var1].getYLocation();
			}
		}

		return 0;
	}

	/**
	 * Returns the ExtendedBlockStorage array for this Chunk.
	 */
	public ExtendedBlockStorage[] getBlockStorageArray() {
		return storageArrays;
	}

	/**
	 * Generates the height map for a chunk from scratch
	 */
	public void generateHeightMap() {
		final int var1 = getTopFilledSegment();

		for (int var2 = 0; var2 < 16; ++var2) {
			int var3 = 0;

			while (var3 < 16) {
				precipitationHeightMap[var2 + (var3 << 4)] = -999;
				int var4 = var1 + 16 - 1;

				while (true) {
					if (var4 > 0) {
						final int var5 = getBlockID(var2, var4 - 1, var3);

						if (Block.lightOpacity[var5] == 0) {
							--var4;
							continue;
						}

						heightMap[var3 << 4 | var2] = var4;
					}

					++var3;
					break;
				}
			}
		}

		isModified = true;
	}

	/**
	 * Generates the initial skylight map for the chunk upon generation or load.
	 */
	public void generateSkylightMap() {
		final int var1 = getTopFilledSegment();
		heightMapMinimum = Integer.MAX_VALUE;
		int var2;
		int var3;

		for (var2 = 0; var2 < 16; ++var2) {
			var3 = 0;

			while (var3 < 16) {
				precipitationHeightMap[var2 + (var3 << 4)] = -999;
				int var4 = var1 + 16 - 1;

				while (true) {
					if (var4 > 0) {
						if (getBlockLightOpacity(var2, var4 - 1, var3) == 0) {
							--var4;
							continue;
						}

						heightMap[var3 << 4 | var2] = var4;

						if (var4 < heightMapMinimum) {
							heightMapMinimum = var4;
						}
					}

					if (!worldObj.provider.hasNoSky) {
						var4 = 15;
						int var5 = var1 + 16 - 1;

						do {
							var4 -= getBlockLightOpacity(var2, var5, var3);

							if (var4 > 0) {
								final ExtendedBlockStorage var6 = storageArrays[var5 >> 4];

								if (var6 != null) {
									var6.setExtSkylightValue(var2, var5 & 15,
											var3, var4);
									worldObj.markBlockForRenderUpdate(
											(xPosition << 4) + var2, var5,
											(zPosition << 4) + var3);
								}
							}

							--var5;
						} while (var5 > 0 && var4 > 0);
					}

					++var3;
					break;
				}
			}
		}

		isModified = true;

		for (var2 = 0; var2 < 16; ++var2) {
			for (var3 = 0; var3 < 16; ++var3) {
				propagateSkylightOcclusion(var2, var3);
			}
		}
	}

	/**
	 * Propagates a given sky-visible block's light value downward and upward to
	 * neighboring blocks as necessary.
	 */
	private void propagateSkylightOcclusion(final int par1, final int par2) {
		updateSkylightColumns[par1 + par2 * 16] = true;
		isGapLightingUpdated = true;
	}

	/**
	 * Runs delayed skylight updates.
	 */
	private void updateSkylight_do() {
		worldObj.theProfiler.startSection("recheckGaps");

		if (worldObj.doChunksNearChunkExist(xPosition * 16 + 8, 0,
				zPosition * 16 + 8, 16)) {
			for (int var1 = 0; var1 < 16; ++var1) {
				for (int var2 = 0; var2 < 16; ++var2) {
					if (updateSkylightColumns[var1 + var2 * 16]) {
						updateSkylightColumns[var1 + var2 * 16] = false;
						final int var3 = getHeightValue(var1, var2);
						final int var4 = xPosition * 16 + var1;
						final int var5 = zPosition * 16 + var2;
						int var6 = worldObj.getChunkHeightMapMinimum(var4 - 1,
								var5);
						final int var7 = worldObj.getChunkHeightMapMinimum(
								var4 + 1, var5);
						final int var8 = worldObj.getChunkHeightMapMinimum(
								var4, var5 - 1);
						final int var9 = worldObj.getChunkHeightMapMinimum(
								var4, var5 + 1);

						if (var7 < var6) {
							var6 = var7;
						}

						if (var8 < var6) {
							var6 = var8;
						}

						if (var9 < var6) {
							var6 = var9;
						}

						checkSkylightNeighborHeight(var4, var5, var6);
						checkSkylightNeighborHeight(var4 - 1, var5, var3);
						checkSkylightNeighborHeight(var4 + 1, var5, var3);
						checkSkylightNeighborHeight(var4, var5 - 1, var3);
						checkSkylightNeighborHeight(var4, var5 + 1, var3);
					}
				}
			}

			isGapLightingUpdated = false;
		}

		worldObj.theProfiler.endSection();
	}

	/**
	 * Checks the height of a block next to a sky-visible block and schedules a
	 * lighting update as necessary.
	 */
	private void checkSkylightNeighborHeight(final int par1, final int par2,
			final int par3) {
		final int var4 = worldObj.getHeightValue(par1, par2);

		if (var4 > par3) {
			updateSkylightNeighborHeight(par1, par2, par3, var4 + 1);
		} else if (var4 < par3) {
			updateSkylightNeighborHeight(par1, par2, var4, par3 + 1);
		}
	}

	private void updateSkylightNeighborHeight(final int par1, final int par2,
			final int par3, final int par4) {
		if (par4 > par3 && worldObj.doChunksNearChunkExist(par1, 0, par2, 16)) {
			for (int var5 = par3; var5 < par4; ++var5) {
				worldObj.updateLightByType(EnumSkyBlock.Sky, par1, var5, par2);
			}

			isModified = true;
		}
	}

	/**
	 * Initiates the recalculation of both the block-light and sky-light for a
	 * given block inside a chunk.
	 */
	private void relightBlock(final int par1, final int par2, final int par3) {
		final int var4 = heightMap[par3 << 4 | par1] & 255;
		int var5 = var4;

		if (par2 > var4) {
			var5 = par2;
		}

		while (var5 > 0 && getBlockLightOpacity(par1, var5 - 1, par3) == 0) {
			--var5;
		}

		if (var5 != var4) {
			worldObj.markBlocksDirtyVertical(par1 + xPosition * 16, par3
					+ zPosition * 16, var5, var4);
			heightMap[par3 << 4 | par1] = var5;
			final int var6 = xPosition * 16 + par1;
			final int var7 = zPosition * 16 + par3;
			int var8;
			int var12;

			if (!worldObj.provider.hasNoSky) {
				ExtendedBlockStorage var9;

				if (var5 < var4) {
					for (var8 = var5; var8 < var4; ++var8) {
						var9 = storageArrays[var8 >> 4];

						if (var9 != null) {
							var9.setExtSkylightValue(par1, var8 & 15, par3, 15);
							worldObj.markBlockForRenderUpdate((xPosition << 4)
									+ par1, var8, (zPosition << 4) + par3);
						}
					}
				} else {
					for (var8 = var4; var8 < var5; ++var8) {
						var9 = storageArrays[var8 >> 4];

						if (var9 != null) {
							var9.setExtSkylightValue(par1, var8 & 15, par3, 0);
							worldObj.markBlockForRenderUpdate((xPosition << 4)
									+ par1, var8, (zPosition << 4) + par3);
						}
					}
				}

				var8 = 15;

				while (var5 > 0 && var8 > 0) {
					--var5;
					var12 = getBlockLightOpacity(par1, var5, par3);

					if (var12 == 0) {
						var12 = 1;
					}

					var8 -= var12;

					if (var8 < 0) {
						var8 = 0;
					}

					final ExtendedBlockStorage var10 = storageArrays[var5 >> 4];

					if (var10 != null) {
						var10.setExtSkylightValue(par1, var5 & 15, par3, var8);
					}
				}
			}

			var8 = heightMap[par3 << 4 | par1];
			var12 = var4;
			int var13 = var8;

			if (var8 < var4) {
				var12 = var8;
				var13 = var4;
			}

			if (var8 < heightMapMinimum) {
				heightMapMinimum = var8;
			}

			if (!worldObj.provider.hasNoSky) {
				updateSkylightNeighborHeight(var6 - 1, var7, var12, var13);
				updateSkylightNeighborHeight(var6 + 1, var7, var12, var13);
				updateSkylightNeighborHeight(var6, var7 - 1, var12, var13);
				updateSkylightNeighborHeight(var6, var7 + 1, var12, var13);
				updateSkylightNeighborHeight(var6, var7, var12, var13);
			}

			isModified = true;
		}
	}

	public int getBlockLightOpacity(final int par1, final int par2,
			final int par3) {
		return Block.lightOpacity[getBlockID(par1, par2, par3)];
	}

	/**
	 * Return the ID of a block in the chunk.
	 */
	public int getBlockID(final int par1, final int par2, final int par3) {
		if (par2 >> 4 >= storageArrays.length) {
			return 0;
		} else {
			final ExtendedBlockStorage var4 = storageArrays[par2 >> 4];
			return var4 != null ? var4.getExtBlockID(par1, par2 & 15, par3) : 0;
		}
	}

	/**
	 * Return the metadata corresponding to the given coordinates inside a
	 * chunk.
	 */
	public int getBlockMetadata(final int par1, final int par2, final int par3) {
		if (par2 >> 4 >= storageArrays.length) {
			return 0;
		} else {
			final ExtendedBlockStorage var4 = storageArrays[par2 >> 4];
			return var4 != null ? var4.getExtBlockMetadata(par1, par2 & 15,
					par3) : 0;
		}
	}

	/**
	 * Sets a blockID of a position within a chunk with metadata. Args: x, y, z,
	 * blockID, metadata
	 */
	public boolean setBlockIDWithMetadata(final int par1, final int par2,
			final int par3, final int par4, final int par5) {
		final int var6 = par3 << 4 | par1;

		if (par2 >= precipitationHeightMap[var6] - 1) {
			precipitationHeightMap[var6] = -999;
		}

		final int var7 = heightMap[var6];
		final int var8 = getBlockID(par1, par2, par3);
		final int var9 = getBlockMetadata(par1, par2, par3);

		if (var8 == par4 && var9 == par5) {
			return false;
		} else {
			ExtendedBlockStorage var10 = storageArrays[par2 >> 4];
			boolean var11 = false;

			if (var10 == null) {
				if (par4 == 0) {
					return false;
				}

				var10 = storageArrays[par2 >> 4] = new ExtendedBlockStorage(
						par2 >> 4 << 4, !worldObj.provider.hasNoSky);
				var11 = par2 >= var7;
			}

			final int var12 = xPosition * 16 + par1;
			final int var13 = zPosition * 16 + par3;

			if (var8 != 0 && !worldObj.isRemote) {
				Block.blocksList[var8].onSetBlockIDWithMetaData(worldObj,
						var12, par2, var13, var9);
			}

			var10.setExtBlockID(par1, par2 & 15, par3, par4);

			if (var8 != 0) {
				if (!worldObj.isRemote) {
					Block.blocksList[var8].breakBlock(worldObj, var12, par2,
							var13, var8, var9);
				} else if (Block.blocksList[var8] instanceof ITileEntityProvider
						&& var8 != par4) {
					worldObj.removeBlockTileEntity(var12, par2, var13);
				}
			}

			if (var10.getExtBlockID(par1, par2 & 15, par3) != par4) {
				return false;
			} else {
				var10.setExtBlockMetadata(par1, par2 & 15, par3, par5);

				if (var11) {
					generateSkylightMap();
				} else {
					if (Block.lightOpacity[par4 & 4095] > 0) {
						if (par2 >= var7) {
							relightBlock(par1, par2 + 1, par3);
						}
					} else if (par2 == var7 - 1) {
						relightBlock(par1, par2, par3);
					}

					propagateSkylightOcclusion(par1, par3);
				}

				TileEntity var14;

				if (par4 != 0) {
					if (!worldObj.isRemote) {
						Block.blocksList[par4].onBlockAdded(worldObj, var12,
								par2, var13);
					}

					if (Block.blocksList[par4] instanceof ITileEntityProvider) {
						var14 = getChunkBlockTileEntity(par1, par2, par3);

						if (var14 == null) {
							var14 = ((ITileEntityProvider) Block.blocksList[par4])
									.createNewTileEntity(worldObj);
							worldObj.setBlockTileEntity(var12, par2, var13,
									var14);
						}

						if (var14 != null) {
							var14.updateContainingBlockInfo();
						}
					}
				} else if (var8 > 0
						&& Block.blocksList[var8] instanceof ITileEntityProvider) {
					var14 = getChunkBlockTileEntity(par1, par2, par3);

					if (var14 != null) {
						var14.updateContainingBlockInfo();
					}
				}

				isModified = true;
				return true;
			}
		}
	}

	/**
	 * Set the metadata of a block in the chunk
	 */
	public boolean setBlockMetadata(final int par1, final int par2,
			final int par3, final int par4) {
		final ExtendedBlockStorage var5 = storageArrays[par2 >> 4];

		if (var5 == null) {
			return false;
		} else {
			final int var6 = var5.getExtBlockMetadata(par1, par2 & 15, par3);

			if (var6 == par4) {
				return false;
			} else {
				isModified = true;
				var5.setExtBlockMetadata(par1, par2 & 15, par3, par4);
				final int var7 = var5.getExtBlockID(par1, par2 & 15, par3);

				if (var7 > 0
						&& Block.blocksList[var7] instanceof ITileEntityProvider) {
					final TileEntity var8 = getChunkBlockTileEntity(par1, par2,
							par3);

					if (var8 != null) {
						var8.updateContainingBlockInfo();
						var8.blockMetadata = par4;
					}
				}

				return true;
			}
		}
	}

	/**
	 * Gets the amount of light saved in this block (doesn't adjust for
	 * daylight)
	 */
	public int getSavedLightValue(final EnumSkyBlock par1EnumSkyBlock,
			final int par2, final int par3, final int par4) {
		final ExtendedBlockStorage var5 = storageArrays[par3 >> 4];
		return var5 == null ? canBlockSeeTheSky(par2, par3, par4) ? par1EnumSkyBlock.defaultLightValue
				: 0
				: par1EnumSkyBlock == EnumSkyBlock.Sky ? worldObj.provider.hasNoSky ? 0
						: var5.getExtSkylightValue(par2, par3 & 15, par4)
						: par1EnumSkyBlock == EnumSkyBlock.Block ? var5
								.getExtBlocklightValue(par2, par3 & 15, par4)
								: par1EnumSkyBlock.defaultLightValue;
	}

	/**
	 * Sets the light value at the coordinate. If enumskyblock is set to sky it
	 * sets it in the skylightmap and if its a block then into the
	 * blocklightmap. Args enumSkyBlock, x, y, z, lightValue
	 */
	public void setLightValue(final EnumSkyBlock par1EnumSkyBlock,
			final int par2, final int par3, final int par4, final int par5) {
		ExtendedBlockStorage var6 = storageArrays[par3 >> 4];

		if (var6 == null) {
			var6 = storageArrays[par3 >> 4] = new ExtendedBlockStorage(
					par3 >> 4 << 4, !worldObj.provider.hasNoSky);
			generateSkylightMap();
		}

		isModified = true;

		if (par1EnumSkyBlock == EnumSkyBlock.Sky) {
			if (!worldObj.provider.hasNoSky) {
				var6.setExtSkylightValue(par2, par3 & 15, par4, par5);
			}
		} else if (par1EnumSkyBlock == EnumSkyBlock.Block) {
			var6.setExtBlocklightValue(par2, par3 & 15, par4, par5);
		}
	}

	/**
	 * Gets the amount of light on a block taking into account sunlight
	 */
	public int getBlockLightValue(final int par1, final int par2,
			final int par3, final int par4) {
		final ExtendedBlockStorage var5 = storageArrays[par2 >> 4];

		if (var5 == null) {
			return !worldObj.provider.hasNoSky
					&& par4 < EnumSkyBlock.Sky.defaultLightValue ? EnumSkyBlock.Sky.defaultLightValue
					- par4
					: 0;
		} else {
			int var6 = worldObj.provider.hasNoSky ? 0 : var5
					.getExtSkylightValue(par1, par2 & 15, par3);

			if (var6 > 0) {
				Chunk.isLit = true;
			}

			var6 -= par4;
			final int var7 = var5.getExtBlocklightValue(par1, par2 & 15, par3);

			if (var7 > var6) {
				var6 = var7;
			}

			return var6;
		}
	}

	/**
	 * Adds an entity to the chunk. Args: entity
	 */
	public void addEntity(final Entity par1Entity) {
		hasEntities = true;
		final int var2 = MathHelper.floor_double(par1Entity.posX / 16.0D);
		final int var3 = MathHelper.floor_double(par1Entity.posZ / 16.0D);

		if (var2 != xPosition || var3 != zPosition) {
			worldObj.getWorldLogAgent().logSevere(
					"Wrong location! " + par1Entity);
			Thread.dumpStack();
		}

		int var4 = MathHelper.floor_double(par1Entity.posY / 16.0D);

		if (var4 < 0) {
			var4 = 0;
		}

		if (var4 >= entityLists.length) {
			var4 = entityLists.length - 1;
		}

		par1Entity.addedToChunk = true;
		par1Entity.chunkCoordX = xPosition;
		par1Entity.chunkCoordY = var4;
		par1Entity.chunkCoordZ = zPosition;
		entityLists[var4].add(par1Entity);
	}

	/**
	 * removes entity using its y chunk coordinate as its index
	 */
	public void removeEntity(final Entity par1Entity) {
		removeEntityAtIndex(par1Entity, par1Entity.chunkCoordY);
	}

	/**
	 * Removes entity at the specified index from the entity array.
	 */
	public void removeEntityAtIndex(final Entity par1Entity, int par2) {
		if (par2 < 0) {
			par2 = 0;
		}

		if (par2 >= entityLists.length) {
			par2 = entityLists.length - 1;
		}

		entityLists[par2].remove(par1Entity);
	}

	/**
	 * Returns whether is not a block above this one blocking sight to the sky
	 * (done via checking against the heightmap)
	 */
	public boolean canBlockSeeTheSky(final int par1, final int par2,
			final int par3) {
		return par2 >= heightMap[par3 << 4 | par1];
	}

	/**
	 * Gets the TileEntity for a given block in this chunk
	 */
	public TileEntity getChunkBlockTileEntity(final int par1, final int par2,
			final int par3) {
		final ChunkPosition var4 = new ChunkPosition(par1, par2, par3);
		TileEntity var5 = (TileEntity) chunkTileEntityMap.get(var4);

		if (var5 == null) {
			final int var6 = getBlockID(par1, par2, par3);

			if (var6 <= 0 || !Block.blocksList[var6].hasTileEntity()) {
				return null;
			}

			if (var5 == null) {
				var5 = ((ITileEntityProvider) Block.blocksList[var6])
						.createNewTileEntity(worldObj);
				worldObj.setBlockTileEntity(xPosition * 16 + par1, par2,
						zPosition * 16 + par3, var5);
			}

			var5 = (TileEntity) chunkTileEntityMap.get(var4);
		}

		if (var5 != null && var5.isInvalid()) {
			chunkTileEntityMap.remove(var4);
			return null;
		} else {
			return var5;
		}
	}

	/**
	 * Adds a TileEntity to a chunk
	 */
	public void addTileEntity(final TileEntity par1TileEntity) {
		final int var2 = par1TileEntity.xCoord - xPosition * 16;
		final int var3 = par1TileEntity.yCoord;
		final int var4 = par1TileEntity.zCoord - zPosition * 16;
		setChunkBlockTileEntity(var2, var3, var4, par1TileEntity);

		if (isChunkLoaded) {
			worldObj.loadedTileEntityList.add(par1TileEntity);
		}
	}

	/**
	 * Sets the TileEntity for a given block in this chunk
	 */
	public void setChunkBlockTileEntity(final int par1, final int par2,
			final int par3, final TileEntity par4TileEntity) {
		final ChunkPosition var5 = new ChunkPosition(par1, par2, par3);
		par4TileEntity.setWorldObj(worldObj);
		par4TileEntity.xCoord = xPosition * 16 + par1;
		par4TileEntity.yCoord = par2;
		par4TileEntity.zCoord = zPosition * 16 + par3;

		if (getBlockID(par1, par2, par3) != 0
				&& Block.blocksList[getBlockID(par1, par2, par3)] instanceof ITileEntityProvider) {
			if (chunkTileEntityMap.containsKey(var5)) {
				((TileEntity) chunkTileEntityMap.get(var5)).invalidate();
			}

			par4TileEntity.validate();
			chunkTileEntityMap.put(var5, par4TileEntity);
		}
	}

	/**
	 * Removes the TileEntity for a given block in this chunk
	 */
	public void removeChunkBlockTileEntity(final int par1, final int par2,
			final int par3) {
		final ChunkPosition var4 = new ChunkPosition(par1, par2, par3);

		if (isChunkLoaded) {
			final TileEntity var5 = (TileEntity) chunkTileEntityMap
					.remove(var4);

			if (var5 != null) {
				var5.invalidate();
			}
		}
	}

	/**
	 * Called when this Chunk is loaded by the ChunkProvider
	 */
	public void onChunkLoad() {
		isChunkLoaded = true;
		worldObj.addTileEntity(chunkTileEntityMap.values());

		for (final List entityList : entityLists) {
			worldObj.addLoadedEntities(entityList);
		}
	}

	/**
	 * Called when this Chunk is unloaded by the ChunkProvider
	 */
	public void onChunkUnload() {
		isChunkLoaded = false;
		final Iterator var1 = chunkTileEntityMap.values().iterator();

		while (var1.hasNext()) {
			final TileEntity var2 = (TileEntity) var1.next();
			worldObj.markTileEntityForDespawn(var2);
		}

		for (final List entityList : entityLists) {
			worldObj.unloadEntities(entityList);
		}
	}

	/**
	 * Sets the isModified flag for this Chunk
	 */
	public void setChunkModified() {
		isModified = true;
	}

	/**
	 * Fills the given list of all entities that intersect within the given
	 * bounding box that aren't the passed entity Args: entity, aabb, listToFill
	 */
	public void getEntitiesWithinAABBForEntity(final Entity par1Entity,
			final AxisAlignedBB par2AxisAlignedBB, final List par3List,
			final IEntitySelector par4IEntitySelector) {
		int var5 = MathHelper
				.floor_double((par2AxisAlignedBB.minY - 2.0D) / 16.0D);
		int var6 = MathHelper
				.floor_double((par2AxisAlignedBB.maxY + 2.0D) / 16.0D);

		if (var5 < 0) {
			var5 = 0;
			var6 = Math.max(var5, var6);
		}

		if (var6 >= entityLists.length) {
			var6 = entityLists.length - 1;
			var5 = Math.min(var5, var6);
		}

		for (int var7 = var5; var7 <= var6; ++var7) {
			final List var8 = entityLists[var7];

			for (int var9 = 0; var9 < var8.size(); ++var9) {
				Entity var10 = (Entity) var8.get(var9);

				if (var10 != par1Entity
						&& var10.boundingBox.intersectsWith(par2AxisAlignedBB)
						&& (par4IEntitySelector == null || par4IEntitySelector
								.isEntityApplicable(var10))) {
					par3List.add(var10);
					final Entity[] var11 = var10.getParts();

					if (var11 != null) {
						for (final Entity element : var11) {
							var10 = element;

							if (var10 != par1Entity
									&& var10.boundingBox
											.intersectsWith(par2AxisAlignedBB)
									&& (par4IEntitySelector == null || par4IEntitySelector
											.isEntityApplicable(var10))) {
								par3List.add(var10);
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Gets all entities that can be assigned to the specified class. Args:
	 * entityClass, aabb, listToFill
	 */
	public void getEntitiesOfTypeWithinAAAB(final Class par1Class,
			final AxisAlignedBB par2AxisAlignedBB, final List par3List,
			final IEntitySelector par4IEntitySelector) {
		int var5 = MathHelper
				.floor_double((par2AxisAlignedBB.minY - 2.0D) / 16.0D);
		int var6 = MathHelper
				.floor_double((par2AxisAlignedBB.maxY + 2.0D) / 16.0D);

		if (var5 < 0) {
			var5 = 0;
		} else if (var5 >= entityLists.length) {
			var5 = entityLists.length - 1;
		}

		if (var6 >= entityLists.length) {
			var6 = entityLists.length - 1;
		} else if (var6 < 0) {
			var6 = 0;
		}

		for (int var7 = var5; var7 <= var6; ++var7) {
			final List var8 = entityLists[var7];

			for (int var9 = 0; var9 < var8.size(); ++var9) {
				final Entity var10 = (Entity) var8.get(var9);

				if (par1Class.isAssignableFrom(var10.getClass())
						&& var10.boundingBox.intersectsWith(par2AxisAlignedBB)
						&& (par4IEntitySelector == null || par4IEntitySelector
								.isEntityApplicable(var10))) {
					par3List.add(var10);
				}
			}
		}
	}

	/**
	 * Returns true if this Chunk needs to be saved
	 */
	public boolean needsSaving(final boolean par1) {
		if (par1) {
			if (hasEntities && worldObj.getTotalWorldTime() != lastSaveTime
					|| isModified) {
				return true;
			}
		} else if (hasEntities
				&& worldObj.getTotalWorldTime() >= lastSaveTime + 600L) {
			return true;
		}

		return isModified;
	}

	public Random getRandomWithSeed(final long par1) {
		return new Random(worldObj.getSeed() + xPosition * xPosition * 4987142
				+ xPosition * 5947611 + zPosition * zPosition * 4392871L
				+ zPosition * 389711 ^ par1);
	}

	public boolean isEmpty() {
		return false;
	}

	public void populateChunk(final IChunkProvider par1IChunkProvider,
			final IChunkProvider par2IChunkProvider, final int par3,
			final int par4) {
		if (!isTerrainPopulated
				&& par1IChunkProvider.chunkExists(par3 + 1, par4 + 1)
				&& par1IChunkProvider.chunkExists(par3, par4 + 1)
				&& par1IChunkProvider.chunkExists(par3 + 1, par4)) {
			par1IChunkProvider.populate(par2IChunkProvider, par3, par4);
		}

		if (par1IChunkProvider.chunkExists(par3 - 1, par4)
				&& !par1IChunkProvider.provideChunk(par3 - 1, par4).isTerrainPopulated
				&& par1IChunkProvider.chunkExists(par3 - 1, par4 + 1)
				&& par1IChunkProvider.chunkExists(par3, par4 + 1)
				&& par1IChunkProvider.chunkExists(par3 - 1, par4 + 1)) {
			par1IChunkProvider.populate(par2IChunkProvider, par3 - 1, par4);
		}

		if (par1IChunkProvider.chunkExists(par3, par4 - 1)
				&& !par1IChunkProvider.provideChunk(par3, par4 - 1).isTerrainPopulated
				&& par1IChunkProvider.chunkExists(par3 + 1, par4 - 1)
				&& par1IChunkProvider.chunkExists(par3 + 1, par4 - 1)
				&& par1IChunkProvider.chunkExists(par3 + 1, par4)) {
			par1IChunkProvider.populate(par2IChunkProvider, par3, par4 - 1);
		}

		if (par1IChunkProvider.chunkExists(par3 - 1, par4 - 1)
				&& !par1IChunkProvider.provideChunk(par3 - 1, par4 - 1).isTerrainPopulated
				&& par1IChunkProvider.chunkExists(par3, par4 - 1)
				&& par1IChunkProvider.chunkExists(par3 - 1, par4)) {
			par1IChunkProvider.populate(par2IChunkProvider, par3 - 1, par4 - 1);
		}
	}

	/**
	 * Gets the height to which rain/snow will fall. Calculates it if not
	 * already stored.
	 */
	public int getPrecipitationHeight(final int par1, final int par2) {
		final int var3 = par1 | par2 << 4;
		int var4 = precipitationHeightMap[var3];

		if (var4 == -999) {
			int var5 = getTopFilledSegment() + 15;
			var4 = -1;

			while (var5 > 0 && var4 == -1) {
				final int var6 = getBlockID(par1, var5, par2);
				final Material var7 = var6 == 0 ? Material.air
						: Block.blocksList[var6].blockMaterial;

				if (!var7.blocksMovement() && !var7.isLiquid()) {
					--var5;
				} else {
					var4 = var5 + 1;
				}
			}

			precipitationHeightMap[var3] = var4;
		}

		return var4;
	}

	/**
	 * Checks whether skylight needs updated; if it does, calls
	 * updateSkylight_do
	 */
	public void updateSkylight() {
		if (isGapLightingUpdated && !worldObj.provider.hasNoSky) {
			updateSkylight_do();
		}
	}

	/**
	 * Gets a ChunkCoordIntPair representing the Chunk's position.
	 */
	public ChunkCoordIntPair getChunkCoordIntPair() {
		return new ChunkCoordIntPair(xPosition, zPosition);
	}

	/**
	 * Returns whether the ExtendedBlockStorages containing levels (in blocks)
	 * from arg 1 to arg 2 are fully empty (true) or not (false).
	 */
	public boolean getAreLevelsEmpty(int par1, int par2) {
		if (par1 < 0) {
			par1 = 0;
		}

		if (par2 >= 256) {
			par2 = 255;
		}

		for (int var3 = par1; var3 <= par2; var3 += 16) {
			final ExtendedBlockStorage var4 = storageArrays[var3 >> 4];

			if (var4 != null && !var4.isEmpty()) {
				return false;
			}
		}

		return true;
	}

	public void setStorageArrays(
			final ExtendedBlockStorage[] par1ArrayOfExtendedBlockStorage) {
		storageArrays = par1ArrayOfExtendedBlockStorage;
	}

	/**
	 * Initialise this chunk with new binary data
	 */
	public void fillChunk(final byte[] par1ArrayOfByte, final int par2,
			final int par3, final boolean par4) {
		int var5 = 0;
		final boolean var6 = !worldObj.provider.hasNoSky;
		int var7;

		for (var7 = 0; var7 < storageArrays.length; ++var7) {
			if ((par2 & 1 << var7) != 0) {
				if (storageArrays[var7] == null) {
					storageArrays[var7] = new ExtendedBlockStorage(var7 << 4,
							var6);
				}

				final byte[] var8 = storageArrays[var7].getBlockLSBArray();
				System.arraycopy(par1ArrayOfByte, var5, var8, 0, var8.length);
				var5 += var8.length;
			} else if (par4 && storageArrays[var7] != null) {
				storageArrays[var7] = null;
			}
		}

		NibbleArray var9;

		for (var7 = 0; var7 < storageArrays.length; ++var7) {
			if ((par2 & 1 << var7) != 0 && storageArrays[var7] != null) {
				var9 = storageArrays[var7].getMetadataArray();
				System.arraycopy(par1ArrayOfByte, var5, var9.data, 0,
						var9.data.length);
				var5 += var9.data.length;
			}
		}

		for (var7 = 0; var7 < storageArrays.length; ++var7) {
			if ((par2 & 1 << var7) != 0 && storageArrays[var7] != null) {
				var9 = storageArrays[var7].getBlocklightArray();
				System.arraycopy(par1ArrayOfByte, var5, var9.data, 0,
						var9.data.length);
				var5 += var9.data.length;
			}
		}

		if (var6) {
			for (var7 = 0; var7 < storageArrays.length; ++var7) {
				if ((par2 & 1 << var7) != 0 && storageArrays[var7] != null) {
					var9 = storageArrays[var7].getSkylightArray();
					System.arraycopy(par1ArrayOfByte, var5, var9.data, 0,
							var9.data.length);
					var5 += var9.data.length;
				}
			}
		}

		for (var7 = 0; var7 < storageArrays.length; ++var7) {
			if ((par3 & 1 << var7) != 0) {
				if (storageArrays[var7] == null) {
					var5 += 2048;
				} else {
					var9 = storageArrays[var7].getBlockMSBArray();

					if (var9 == null) {
						var9 = storageArrays[var7].createBlockMSBArray();
					}

					System.arraycopy(par1ArrayOfByte, var5, var9.data, 0,
							var9.data.length);
					var5 += var9.data.length;
				}
			} else if (par4 && storageArrays[var7] != null
					&& storageArrays[var7].getBlockMSBArray() != null) {
				storageArrays[var7].clearMSBArray();
			}
		}

		if (par4) {
			System.arraycopy(par1ArrayOfByte, var5, blockBiomeArray, 0,
					blockBiomeArray.length);
		}

		for (var7 = 0; var7 < storageArrays.length; ++var7) {
			if (storageArrays[var7] != null && (par2 & 1 << var7) != 0) {
				storageArrays[var7].removeInvalidBlocks();
			}
		}

		generateHeightMap();
		final Iterator var10 = chunkTileEntityMap.values().iterator();

		while (var10.hasNext()) {
			final TileEntity var11 = (TileEntity) var10.next();
			var11.updateContainingBlockInfo();
		}
	}

	/**
	 * This method retrieves the biome at a set of coordinates
	 */
	public BiomeGenBase getBiomeGenForWorldCoords(final int par1,
			final int par2, final WorldChunkManager par3WorldChunkManager) {
		int var4 = blockBiomeArray[par2 << 4 | par1] & 255;

		if (var4 == 255) {
			final BiomeGenBase var5 = par3WorldChunkManager.getBiomeGenAt(
					(xPosition << 4) + par1, (zPosition << 4) + par2);
			var4 = var5.biomeID;
			blockBiomeArray[par2 << 4 | par1] = (byte) (var4 & 255);
		}

		return BiomeGenBase.biomeList[var4] == null ? BiomeGenBase.plains
				: BiomeGenBase.biomeList[var4];
	}

	/**
	 * Returns an array containing a 16x16 mapping on the X/Z of block positions
	 * in this Chunk to biome IDs.
	 */
	public byte[] getBiomeArray() {
		return blockBiomeArray;
	}

	/**
	 * Accepts a 256-entry array that contains a 16x16 mapping on the X/Z plane
	 * of block positions in this Chunk to biome IDs.
	 */
	public void setBiomeArray(final byte[] par1ArrayOfByte) {
		blockBiomeArray = par1ArrayOfByte;
	}

	/**
	 * Resets the relight check index to 0 for this Chunk.
	 */
	public void resetRelightChecks() {
		queuedLightChecks = 0;
	}

	/**
	 * Called once-per-chunk-per-tick, and advances the round-robin relight
	 * check index per-storage-block by up to 8 blocks at a time. In a
	 * worst-case scenario, can potentially take up to 1.6 seconds, calculated
	 * via (4096/(8*16))/20, to re-check all blocks in a chunk, which could
	 * explain both lagging light updates in certain cases as well as Nether
	 * relight
	 */
	public void enqueueRelightChecks() {
		for (int var1 = 0; var1 < 8; ++var1) {
			if (queuedLightChecks >= 4096) {
				return;
			}

			final int var2 = queuedLightChecks % 16;
			final int var3 = queuedLightChecks / 16 % 16;
			final int var4 = queuedLightChecks / 256;
			++queuedLightChecks;
			final int var5 = (xPosition << 4) + var3;
			final int var6 = (zPosition << 4) + var4;

			for (int var7 = 0; var7 < 16; ++var7) {
				final int var8 = (var2 << 4) + var7;

				if (storageArrays[var2] == null
						&& (var7 == 0 || var7 == 15 || var3 == 0 || var3 == 15
								|| var4 == 0 || var4 == 15)
						|| storageArrays[var2] != null
						&& storageArrays[var2].getExtBlockID(var3, var7, var4) == 0) {
					if (Block.lightValue[worldObj.getBlockId(var5, var8 - 1,
							var6)] > 0) {
						worldObj.updateAllLightTypes(var5, var8 - 1, var6);
					}

					if (Block.lightValue[worldObj.getBlockId(var5, var8 + 1,
							var6)] > 0) {
						worldObj.updateAllLightTypes(var5, var8 + 1, var6);
					}

					if (Block.lightValue[worldObj.getBlockId(var5 - 1, var8,
							var6)] > 0) {
						worldObj.updateAllLightTypes(var5 - 1, var8, var6);
					}

					if (Block.lightValue[worldObj.getBlockId(var5 + 1, var8,
							var6)] > 0) {
						worldObj.updateAllLightTypes(var5 + 1, var8, var6);
					}

					if (Block.lightValue[worldObj.getBlockId(var5, var8,
							var6 - 1)] > 0) {
						worldObj.updateAllLightTypes(var5, var8, var6 - 1);
					}

					if (Block.lightValue[worldObj.getBlockId(var5, var8,
							var6 + 1)] > 0) {
						worldObj.updateAllLightTypes(var5, var8, var6 + 1);
					}

					worldObj.updateAllLightTypes(var5, var8, var6);
				}
			}
		}
	}
}
