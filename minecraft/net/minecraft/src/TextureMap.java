package net.minecraft.src;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.minecraft.client.Minecraft;

public class TextureMap implements IconRegister {
	/** 0 = terrain.png, 1 = items.png */
	public final int textureType;
	public final String textureName;
	public final String basePath;
	public final String textureExt;
	private final HashMap mapTexturesStiched = new HashMap();
	private BufferedImage missingImage = new BufferedImage(64, 64, 2);
	private TextureStitched missingTextureStiched;
	private Texture atlasTexture;
	private final List listTextureStiched = new ArrayList();
	private final Map textureStichedMap = new HashMap();
	private int iconGridSize = -1;
	private int iconGridCountX = -1;
	private int iconGridCountY = -1;
	private double iconGridSizeU = -1.0D;
	private double iconGridSizeV = -1.0D;
	private TextureStitched[] iconGrid = null;

	public TextureMap(final int par1, final String par2, final String par3Str,
			final BufferedImage par4BufferedImage) {
		textureType = par1;
		textureName = par2;
		basePath = par3Str;
		textureExt = ".png";
		missingImage = par4BufferedImage;
	}

	public void refreshTextures() {
		Config.dbg("Creating texture map: " + textureName);

		if (atlasTexture != null) {
			atlasTexture.deleteTexture();
		}

		final Iterator var1 = textureStichedMap.values().iterator();

		while (var1.hasNext()) {
			final TextureStitched var2 = (TextureStitched) var1.next();
			var2.deleteTextures();
		}

		textureStichedMap.clear();
		Reflector.callVoid(Reflector.ForgeHooksClient_onTextureStitchedPre,
				new Object[] { this });
		int var23;
		int var24;

		if (textureType == 0) {
			final Block[] var3 = Block.blocksList;
			var23 = var3.length;

			for (var24 = 0; var24 < var23; ++var24) {
				final Block var4 = var3[var24];

				if (var4 != null) {
					var4.registerIcons(this);
				}
			}

			Minecraft.getMinecraft().renderGlobal
					.registerDestroyBlockIcons(this);
			RenderManager.instance.updateIcons(this);
			ConnectedTextures.updateIcons(this);
			NaturalTextures.updateIcons(this);
		}

		final Item[] var25 = Item.itemsList;
		var23 = var25.length;

		for (var24 = 0; var24 < var23; ++var24) {
			final Item var26 = var25[var24];

			if (var26 != null && var26.getSpriteNumber() == textureType) {
				var26.registerIcons(this);
			}
		}

		final HashMap var27 = new HashMap();
		final Stitcher var5 = TextureManager.instance().createStitcher(
				textureName);
		mapTexturesStiched.clear();
		listTextureStiched.clear();
		final Texture var6 = TextureManager.instance().makeTexture("missingno",
				2, missingImage.getWidth(), missingImage.getHeight(), 10496,
				6408, 9728, 9728, false, missingImage);
		final StitchHolder var7 = new StitchHolder(var6);
		var5.addStitchHolder(var7);
		var27.put(var7, Arrays.asList(new Texture[] { var6 }));
		Iterator var8 = textureStichedMap.keySet().iterator();
		final ArrayList var9 = new ArrayList();

		while (var8.hasNext()) {
			final String var10 = (String) var8.next();
			final String var11 = makeFullTextureName(var10) + textureExt;
			final List var12 = TextureManager.instance().createNewTexture(
					var10, var11,
					(TextureStitched) textureStichedMap.get(var10));
			var9.add(var12);
		}

		iconGridSize = getStandardTileSize(var9);
		Config.dbg("Icon grid size: " + textureName + ", " + iconGridSize);
		Iterator var31 = var9.iterator();
		List var29;

		while (var31.hasNext()) {
			var29 = (List) var31.next();

			if (!var29.isEmpty()) {
				scaleTextures(var29, iconGridSize);
			}
		}

		var31 = var9.iterator();

		while (var31.hasNext()) {
			var29 = (List) var31.next();

			if (!var29.isEmpty()) {
				final StitchHolder var32 = new StitchHolder(
						(Texture) var29.get(0));
				var5.addStitchHolder(var32);
				var27.put(var32, var29);
			}
		}

		try {
			var5.doStitch();
		} catch (final StitcherException var22) {
			throw var22;
		}

		atlasTexture = var5.getTexture();
		Config.dbg("Texture size: " + textureName + ", "
				+ atlasTexture.getWidth() + "x" + atlasTexture.getHeight());
		atlasTexture.updateMipmapLevel(iconGridSize);
		var8 = var5.getStichSlots().iterator();

		while (var8.hasNext()) {
			final StitchSlot var28 = (StitchSlot) var8.next();
			final StitchHolder var35 = var28.getStitchHolder();
			final Texture var34 = var35.func_98150_a();
			final String var13 = var34.getTextureName();
			final List var14 = (List) var27.get(var35);
			TextureStitched var15 = (TextureStitched) textureStichedMap
					.get(var13);
			boolean var16 = false;

			if (var15 == null) {
				var16 = true;
				var15 = TextureStitched.makeTextureStitched(var13);

				if (!var13.equals("missingno")) {
					Minecraft
							.getMinecraft()
							.getLogAgent()
							.logWarning(
									"Couldn\'t find premade icon for " + var13
											+ " doing " + textureName);
				}
			}

			var15.init(atlasTexture, var14, var28.getOriginX(), var28
					.getOriginY(), var35.func_98150_a().getWidth(), var35
					.func_98150_a().getHeight(), var35.isRotated());
			mapTexturesStiched.put(var13, var15);

			if (!var16) {
				textureStichedMap.remove(var13);
			}

			if (var14.size() > 1) {
				listTextureStiched.add(var15);
				final String var17 = makeFullTextureName(var13) + ".txt";
				final ITexturePack var18 = Minecraft.getMinecraft().texturePackList
						.getSelectedTexturePack();
				final boolean var19 = !var18.func_98138_b("/" + basePath
						+ var13 + ".png", false);

				try {
					final InputStream var20 = var18.func_98137_a("/" + var17,
							var19);
					Minecraft.getMinecraft().getLogAgent()
							.logInfo("Found animation info for: " + var17);
					var15.readAnimationInfo(new BufferedReader(
							new InputStreamReader(var20)));
				} catch (final IOException var21) {
					;
				}
			}
		}

		missingTextureStiched = (TextureStitched) mapTexturesStiched
				.get("missingno");
		var8 = textureStichedMap.values().iterator();

		while (var8.hasNext()) {
			final TextureStitched var30 = (TextureStitched) var8.next();
			var30.copyFrom(missingTextureStiched);
		}

		textureStichedMap.putAll(mapTexturesStiched);
		mapTexturesStiched.clear();
		updateIconGrid();
		atlasTexture.writeImage("debug.stitched_" + textureName + ".png");
		Reflector.callVoid(Reflector.ForgeHooksClient_onTextureStitchedPost,
				new Object[] { this });
		atlasTexture.uploadTexture();

		if (Config.isMultiTexture()) {
			var31 = textureStichedMap.values().iterator();

			while (var31.hasNext()) {
				final TextureStitched var33 = (TextureStitched) var31.next();
				var33.createTileTexture();
			}
		}
	}

	public void updateAnimations() {
		if (listTextureStiched.size() > 0) {
			getTexture().bindTexture(0);
			atlasTexture.setTextureBound(true);
			final Iterator var1 = listTextureStiched.iterator();

			while (var1.hasNext()) {
				final TextureStitched var2 = (TextureStitched) var1.next();

				if (textureType == 0) {
					if (!isTerrainAnimationActive(var2)) {
						continue;
					}
				} else if (textureType == 1 && !Config.isAnimatedItems()) {
					continue;
				}

				var2.updateAnimation();
			}

			atlasTexture.setTextureBound(false);

			if (Config.isMultiTexture()) {
				for (int var4 = 0; var4 < listTextureStiched.size(); ++var4) {
					final TextureStitched var3 = (TextureStitched) listTextureStiched
							.get(var4);

					if (isTerrainAnimationActive(var3)) {
						var3.updateTileAnimation();
					}
				}
			}
		}
	}

	public Texture getTexture() {
		return atlasTexture;
	}

	@Override
	public Icon registerIcon(String par1Str) {
		if (par1Str == null) {
			new RuntimeException("Don\'t register null!").printStackTrace();
			par1Str = "null";
		}

		TextureStitched var2 = (TextureStitched) textureStichedMap.get(par1Str);

		if (var2 == null) {
			var2 = TextureStitched.makeTextureStitched(par1Str);
			var2.setIndexInMap(textureStichedMap.size());
			textureStichedMap.put(par1Str, var2);
		}

		return var2;
	}

	public Icon getMissingIcon() {
		return missingTextureStiched;
	}

	private String makeFullTextureName(final String var1) {
		final int var2 = var1.indexOf(":");

		if (var2 > 0) {
			final String var3 = var1.substring(0, var2);
			final String var4 = var1.substring(var2 + 1);
			return "mods/" + var3 + "/" + basePath + var4;
		} else {
			return var1.startsWith("ctm/") ? var1 : basePath + var1;
		}
	}

	public TextureStitched getIconSafe(final String var1) {
		return (TextureStitched) textureStichedMap.get(var1);
	}

	private int getStandardTileSize(final List var1) {
		final int[] var2 = new int[16];
		final Iterator var3 = var1.iterator();
		int var6;

		while (var3.hasNext()) {
			final List var4 = (List) var3.next();

			if (!var4.isEmpty()) {
				final Texture var5 = (Texture) var4.get(0);

				if (var5 != null) {
					var6 = TextureUtils.getPowerOfTwo(var5.getWidth());
					final int var7 = TextureUtils.getPowerOfTwo(var5
							.getHeight());
					final int var8 = Math.max(var6, var7);

					if (var8 < var2.length) {
						++var2[var8];
					}
				}
			}
		}

		int var9 = 4;
		int var10 = 0;
		int var11;

		for (var11 = 0; var11 < var2.length; ++var11) {
			var6 = var2[var11];

			if (var6 > var10) {
				var9 = var11;
				var10 = var6;
			}
		}

		if (var9 < 4) {
			var9 = 4;
		}

		var11 = TextureUtils.twoToPower(var9);
		return var11;
	}

	private void scaleTextures(final List var1, final int var2) {
		if (!var1.isEmpty()) {
			final Texture var3 = (Texture) var1.get(0);
			final int var4 = Math.max(var3.getWidth(), var3.getHeight());

			if (var4 < var2) {
				for (int var5 = 0; var5 < var1.size(); ++var5) {
					final Texture var6 = (Texture) var1.get(var5);
					var6.scaleUp(var2);
				}
			}
		}
	}

	public TextureStitched getTextureExtry(final String var1) {
		return (TextureStitched) textureStichedMap.get(var1);
	}

	public boolean setTextureEntry(final String var1, final TextureStitched var2) {
		if (!textureStichedMap.containsKey(var1)) {
			var2.setIndexInMap(textureStichedMap.size());
			textureStichedMap.put(var1, var2);
			return true;
		} else {
			return false;
		}
	}

	private void updateIconGrid() {
		iconGridCountX = -1;
		iconGridCountY = -1;
		iconGrid = null;

		if (iconGridSize > 0) {
			iconGridCountX = atlasTexture.getWidth() / iconGridSize;
			iconGridCountY = atlasTexture.getHeight() / iconGridSize;
			iconGrid = new TextureStitched[iconGridCountX * iconGridCountY];
			iconGridSizeU = 1.0D / iconGridCountX;
			iconGridSizeV = 1.0D / iconGridCountY;
			final Iterator var1 = textureStichedMap.values().iterator();

			while (var1.hasNext()) {
				final TextureStitched var2 = (TextureStitched) var1.next();
				final double var3 = Math.min(var2.getMinU(), var2.getMaxU());
				final double var5 = Math.min(var2.getMinV(), var2.getMaxV());
				final double var7 = Math.max(var2.getMinU(), var2.getMaxU());
				final double var9 = Math.max(var2.getMinV(), var2.getMaxV());
				final int var11 = (int) (var3 / iconGridSizeU);
				final int var12 = (int) (var5 / iconGridSizeV);
				final int var13 = (int) (var7 / iconGridSizeU);
				final int var14 = (int) (var9 / iconGridSizeV);

				for (int var15 = var11; var15 <= var13; ++var15) {
					if (var15 >= 0 && var15 < iconGridCountX) {
						for (int var16 = var12; var16 <= var14; ++var16) {
							if (var16 >= 0 && var16 < iconGridCountX) {
								final int var17 = var16 * iconGridCountX
										+ var15;
								iconGrid[var17] = var2;
							} else {
								Config.dbg("Invalid grid V: " + var16
										+ ", icon: " + var2.getIconName());
							}
						}
					} else {
						Config.dbg("Invalid grid U: " + var15 + ", icon: "
								+ var2.getIconName());
					}
				}
			}
		}
	}

	public TextureStitched getIconByUV(final double var1, final double var3) {
		if (iconGrid == null) {
			return null;
		} else {
			final int var5 = (int) (var1 / iconGridSizeU);
			final int var6 = (int) (var3 / iconGridSizeV);
			final int var7 = var6 * iconGridCountX + var5;
			return var7 >= 0 && var7 <= iconGrid.length ? iconGrid[var7] : null;
		}
	}

	public TextureStitched getMissingTextureStiched() {
		return missingTextureStiched;
	}

	public int getMaxTextureIndex() {
		return textureStichedMap.size();
	}

	private boolean isTerrainAnimationActive(final TextureStitched var1) {
		return var1 != TextureUtils.iconWater
				&& var1 != TextureUtils.iconWaterFlow ? var1 != TextureUtils.iconLava
				&& var1 != TextureUtils.iconLavaFlow ? var1 != TextureUtils.iconFire0
				&& var1 != TextureUtils.iconFire1 ? var1 == TextureUtils.iconPortal ? Config
				.isAnimatedPortal() : Config.isAnimatedTerrain()
				: Config.isAnimatedFire()
				: Config.isAnimatedLava()
				: Config.isAnimatedWater();
	}
}
