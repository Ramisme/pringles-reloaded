package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet11PlayerPosition extends Packet10Flying {
	public Packet11PlayerPosition() {
		moving = true;
	}

	public Packet11PlayerPosition(final double par1, final double par3,
			final double par5, final double par7, final boolean par9) {
		xPosition = par1;
		yPosition = par3;
		stance = par5;
		zPosition = par7;
		onGround = par9;
		moving = true;
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		xPosition = par1DataInputStream.readDouble();
		yPosition = par1DataInputStream.readDouble();
		stance = par1DataInputStream.readDouble();
		zPosition = par1DataInputStream.readDouble();
		super.readPacketData(par1DataInputStream);
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		par1DataOutputStream.writeDouble(xPosition);
		par1DataOutputStream.writeDouble(yPosition);
		par1DataOutputStream.writeDouble(stance);
		par1DataOutputStream.writeDouble(zPosition);
		super.writePacketData(par1DataOutputStream);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 33;
	}
}
