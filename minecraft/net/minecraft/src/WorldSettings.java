package net.minecraft.src;

public final class WorldSettings {
	/** The seed for the map. */
	private final long seed;

	/** The EnumGameType. */
	private final EnumGameType theGameType;

	/**
	 * Switch for the map features. 'true' for enabled, 'false' for disabled.
	 */
	private final boolean mapFeaturesEnabled;

	/** True if hardcore mode is enabled */
	private final boolean hardcoreEnabled;
	private final WorldType terrainType;

	/** True if Commands (cheats) are allowed. */
	private boolean commandsAllowed;

	/** True if the Bonus Chest is enabled. */
	private boolean bonusChestEnabled;
	private String field_82751_h;

	public WorldSettings(final long par1, final EnumGameType par3EnumGameType,
			final boolean par4, final boolean par5,
			final WorldType par6WorldType) {
		field_82751_h = "";
		seed = par1;
		theGameType = par3EnumGameType;
		mapFeaturesEnabled = par4;
		hardcoreEnabled = par5;
		terrainType = par6WorldType;
	}

	public WorldSettings(final WorldInfo par1WorldInfo) {
		this(par1WorldInfo.getSeed(), par1WorldInfo.getGameType(),
				par1WorldInfo.isMapFeaturesEnabled(), par1WorldInfo
						.isHardcoreModeEnabled(), par1WorldInfo
						.getTerrainType());
	}

	/**
	 * Enables the bonus chest.
	 */
	public WorldSettings enableBonusChest() {
		bonusChestEnabled = true;
		return this;
	}

	/**
	 * Enables Commands (cheats).
	 */
	public WorldSettings enableCommands() {
		commandsAllowed = true;
		return this;
	}

	public WorldSettings func_82750_a(final String par1Str) {
		field_82751_h = par1Str;
		return this;
	}

	/**
	 * Returns true if the Bonus Chest is enabled.
	 */
	public boolean isBonusChestEnabled() {
		return bonusChestEnabled;
	}

	/**
	 * Returns the seed for the world.
	 */
	public long getSeed() {
		return seed;
	}

	/**
	 * Gets the game type.
	 */
	public EnumGameType getGameType() {
		return theGameType;
	}

	/**
	 * Returns true if hardcore mode is enabled, otherwise false
	 */
	public boolean getHardcoreEnabled() {
		return hardcoreEnabled;
	}

	/**
	 * Get whether the map features (e.g. strongholds) generation is enabled or
	 * disabled.
	 */
	public boolean isMapFeaturesEnabled() {
		return mapFeaturesEnabled;
	}

	public WorldType getTerrainType() {
		return terrainType;
	}

	/**
	 * Returns true if Commands (cheats) are allowed.
	 */
	public boolean areCommandsAllowed() {
		return commandsAllowed;
	}

	/**
	 * Gets the GameType by ID
	 */
	public static EnumGameType getGameTypeById(final int par0) {
		return EnumGameType.getByID(par0);
	}

	public String func_82749_j() {
		return field_82751_h;
	}
}
