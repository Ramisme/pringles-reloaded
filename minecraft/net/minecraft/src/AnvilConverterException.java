package net.minecraft.src;

public class AnvilConverterException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6454503215083091355L;

	public AnvilConverterException(final String par1Str) {
		super(par1Str);
	}
}
