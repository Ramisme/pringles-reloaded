package net.minecraft.src;

import java.net.HttpURLConnection;
import java.net.URL;

import javax.imageio.ImageIO;

class ThreadDownloadImage extends Thread {
	/** The URL of the image to download. */
	final String location;

	/** The image buffer to use. */
	final IImageBuffer buffer;

	/** The image data. */
	final ThreadDownloadImageData imageData;

	ThreadDownloadImage(final ThreadDownloadImageData par1,
			final String par2Str, final IImageBuffer par3IImageBuffer) {
		imageData = par1;
		location = par2Str;
		buffer = par3IImageBuffer;
	}

	@Override
	public void run() {
		HttpURLConnection var1 = null;

		try {
			final URL var2 = new URL(location);
			var1 = (HttpURLConnection) var2.openConnection();
			var1.setDoInput(true);
			var1.setDoOutput(false);
			var1.connect();

			if (var1.getResponseCode() / 100 == 4) {
				return;
			}

			if (buffer == null) {
				imageData.image = ImageIO.read(var1.getInputStream());
			} else {
				imageData.image = buffer.parseUserSkin(ImageIO.read(var1
						.getInputStream()));
			}
		} catch (final Exception var6) {
			var6.printStackTrace();
		} finally {
			var1.disconnect();
		}
	}
}
