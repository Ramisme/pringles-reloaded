package net.minecraft.src;

import java.util.concurrent.Callable;

class CallablePacketID implements Callable {
	final Packet thePacket;

	final NetServerHandler theNetServerHandler;

	CallablePacketID(final NetServerHandler par1NetServerHandler,
			final Packet par2Packet) {
		theNetServerHandler = par1NetServerHandler;
		thePacket = par2Packet;
	}

	public String callPacketID() {
		return String.valueOf(thePacket.getPacketId());
	}

	@Override
	public Object call() {
		return callPacketID();
	}
}
