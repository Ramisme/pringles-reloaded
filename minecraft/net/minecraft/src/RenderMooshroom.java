package net.minecraft.src;

import org.lwjgl.opengl.GL11;

public class RenderMooshroom extends RenderLiving {
	public RenderMooshroom(final ModelBase par1ModelBase, final float par2) {
		super(par1ModelBase, par2);
	}

	public void renderLivingMooshroom(
			final EntityMooshroom par1EntityMooshroom, final double par2,
			final double par4, final double par6, final float par8,
			final float par9) {
		super.doRenderLiving(par1EntityMooshroom, par2, par4, par6, par8, par9);
	}

	protected void renderMooshroomEquippedItems(
			final EntityMooshroom par1EntityMooshroom, final float par2) {
		super.renderEquippedItems(par1EntityMooshroom, par2);

		if (!par1EntityMooshroom.isChild()) {
			loadTexture("/terrain.png");
			GL11.glEnable(GL11.GL_CULL_FACE);
			GL11.glPushMatrix();
			GL11.glScalef(1.0F, -1.0F, 1.0F);
			GL11.glTranslatef(0.2F, 0.4F, 0.5F);
			GL11.glRotatef(42.0F, 0.0F, 1.0F, 0.0F);
			renderBlocks.renderBlockAsItem(Block.mushroomRed, 0, 1.0F);
			GL11.glTranslatef(0.1F, 0.0F, -0.6F);
			GL11.glRotatef(42.0F, 0.0F, 1.0F, 0.0F);
			renderBlocks.renderBlockAsItem(Block.mushroomRed, 0, 1.0F);
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			((ModelQuadruped) mainModel).head.postRender(0.0625F);
			GL11.glScalef(1.0F, -1.0F, 1.0F);
			GL11.glTranslatef(0.0F, 0.75F, -0.2F);
			GL11.glRotatef(12.0F, 0.0F, 1.0F, 0.0F);
			renderBlocks.renderBlockAsItem(Block.mushroomRed, 0, 1.0F);
			GL11.glPopMatrix();
			GL11.glDisable(GL11.GL_CULL_FACE);
		}
	}

	@Override
	protected void renderEquippedItems(final EntityLiving par1EntityLiving,
			final float par2) {
		renderMooshroomEquippedItems((EntityMooshroom) par1EntityLiving, par2);
	}

	@Override
	public void doRenderLiving(final EntityLiving par1EntityLiving,
			final double par2, final double par4, final double par6,
			final float par8, final float par9) {
		renderLivingMooshroom((EntityMooshroom) par1EntityLiving, par2, par4,
				par6, par8, par9);
	}

	/**
	 * Actually renders the given argument. This is a synthetic bridge method,
	 * always casting down its argument and then handing it off to a worker
	 * function which does the actual work. In all probabilty, the class Render
	 * is generic (Render<T extends Entity) and this method has signature public
	 * void doRender(T entity, double d, double d1, double d2, float f, float
	 * f1). But JAD is pre 1.5 so doesn't do that.
	 */
	@Override
	public void doRender(final Entity par1Entity, final double par2,
			final double par4, final double par6, final float par8,
			final float par9) {
		renderLivingMooshroom((EntityMooshroom) par1Entity, par2, par4, par6,
				par8, par9);
	}
}
