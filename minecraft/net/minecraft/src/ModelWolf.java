package net.minecraft.src;

import org.lwjgl.opengl.GL11;

public class ModelWolf extends ModelBase {
	/** main box for the wolf head */
	public ModelRenderer wolfHeadMain;

	/** The wolf's body */
	public ModelRenderer wolfBody;

	/** Wolf'se first leg */
	public ModelRenderer wolfLeg1;

	/** Wolf's second leg */
	public ModelRenderer wolfLeg2;

	/** Wolf's third leg */
	public ModelRenderer wolfLeg3;

	/** Wolf's fourth leg */
	public ModelRenderer wolfLeg4;

	/** The wolf's tail */
	ModelRenderer wolfTail;

	/** The wolf's mane */
	ModelRenderer wolfMane;

	public ModelWolf() {
		final float var1 = 0.0F;
		final float var2 = 13.5F;
		wolfHeadMain = new ModelRenderer(this, 0, 0);
		wolfHeadMain.addBox(-3.0F, -3.0F, -2.0F, 6, 6, 4, var1);
		wolfHeadMain.setRotationPoint(-1.0F, var2, -7.0F);
		wolfBody = new ModelRenderer(this, 18, 14);
		wolfBody.addBox(-4.0F, -2.0F, -3.0F, 6, 9, 6, var1);
		wolfBody.setRotationPoint(0.0F, 14.0F, 2.0F);
		wolfMane = new ModelRenderer(this, 21, 0);
		wolfMane.addBox(-4.0F, -3.0F, -3.0F, 8, 6, 7, var1);
		wolfMane.setRotationPoint(-1.0F, 14.0F, 2.0F);
		wolfLeg1 = new ModelRenderer(this, 0, 18);
		wolfLeg1.addBox(-1.0F, 0.0F, -1.0F, 2, 8, 2, var1);
		wolfLeg1.setRotationPoint(-2.5F, 16.0F, 7.0F);
		wolfLeg2 = new ModelRenderer(this, 0, 18);
		wolfLeg2.addBox(-1.0F, 0.0F, -1.0F, 2, 8, 2, var1);
		wolfLeg2.setRotationPoint(0.5F, 16.0F, 7.0F);
		wolfLeg3 = new ModelRenderer(this, 0, 18);
		wolfLeg3.addBox(-1.0F, 0.0F, -1.0F, 2, 8, 2, var1);
		wolfLeg3.setRotationPoint(-2.5F, 16.0F, -4.0F);
		wolfLeg4 = new ModelRenderer(this, 0, 18);
		wolfLeg4.addBox(-1.0F, 0.0F, -1.0F, 2, 8, 2, var1);
		wolfLeg4.setRotationPoint(0.5F, 16.0F, -4.0F);
		wolfTail = new ModelRenderer(this, 9, 18);
		wolfTail.addBox(-1.0F, 0.0F, -1.0F, 2, 8, 2, var1);
		wolfTail.setRotationPoint(-1.0F, 12.0F, 8.0F);
		wolfHeadMain.setTextureOffset(16, 14).addBox(-3.0F, -5.0F, 0.0F, 2, 2,
				1, var1);
		wolfHeadMain.setTextureOffset(16, 14).addBox(1.0F, -5.0F, 0.0F, 2, 2,
				1, var1);
		wolfHeadMain.setTextureOffset(0, 10).addBox(-1.5F, 0.0F, -5.0F, 3, 3,
				4, var1);
	}

	/**
	 * Sets the models various rotation angles then renders the model.
	 */
	@Override
	public void render(final Entity par1Entity, final float par2,
			final float par3, final float par4, final float par5,
			final float par6, final float par7) {
		super.render(par1Entity, par2, par3, par4, par5, par6, par7);
		setRotationAngles(par2, par3, par4, par5, par6, par7, par1Entity);

		if (isChild) {
			final float var8 = 2.0F;
			GL11.glPushMatrix();
			GL11.glTranslatef(0.0F, 5.0F * par7, 2.0F * par7);
			wolfHeadMain.renderWithRotation(par7);
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			GL11.glScalef(1.0F / var8, 1.0F / var8, 1.0F / var8);
			GL11.glTranslatef(0.0F, 24.0F * par7, 0.0F);
			wolfBody.render(par7);
			wolfLeg1.render(par7);
			wolfLeg2.render(par7);
			wolfLeg3.render(par7);
			wolfLeg4.render(par7);
			wolfTail.renderWithRotation(par7);
			wolfMane.render(par7);
			GL11.glPopMatrix();
		} else {
			wolfHeadMain.renderWithRotation(par7);
			wolfBody.render(par7);
			wolfLeg1.render(par7);
			wolfLeg2.render(par7);
			wolfLeg3.render(par7);
			wolfLeg4.render(par7);
			wolfTail.renderWithRotation(par7);
			wolfMane.render(par7);
		}
	}

	/**
	 * Used for easily adding entity-dependent animations. The second and third
	 * float params here are the same second and third as in the
	 * setRotationAngles method.
	 */
	@Override
	public void setLivingAnimations(final EntityLiving par1EntityLiving,
			final float par2, final float par3, final float par4) {
		final EntityWolf var5 = (EntityWolf) par1EntityLiving;

		if (var5.isAngry()) {
			wolfTail.rotateAngleY = 0.0F;
		} else {
			wolfTail.rotateAngleY = MathHelper.cos(par2 * 0.6662F) * 1.4F
					* par3;
		}

		if (var5.isSitting()) {
			wolfMane.setRotationPoint(-1.0F, 16.0F, -3.0F);
			wolfMane.rotateAngleX = (float) Math.PI * 2F / 5F;
			wolfMane.rotateAngleY = 0.0F;
			wolfBody.setRotationPoint(0.0F, 18.0F, 0.0F);
			wolfBody.rotateAngleX = (float) Math.PI / 4F;
			wolfTail.setRotationPoint(-1.0F, 21.0F, 6.0F);
			wolfLeg1.setRotationPoint(-2.5F, 22.0F, 2.0F);
			wolfLeg1.rotateAngleX = (float) Math.PI * 3F / 2F;
			wolfLeg2.setRotationPoint(0.5F, 22.0F, 2.0F);
			wolfLeg2.rotateAngleX = (float) Math.PI * 3F / 2F;
			wolfLeg3.rotateAngleX = 5.811947F;
			wolfLeg3.setRotationPoint(-2.49F, 17.0F, -4.0F);
			wolfLeg4.rotateAngleX = 5.811947F;
			wolfLeg4.setRotationPoint(0.51F, 17.0F, -4.0F);
		} else {
			wolfBody.setRotationPoint(0.0F, 14.0F, 2.0F);
			wolfBody.rotateAngleX = (float) Math.PI / 2F;
			wolfMane.setRotationPoint(-1.0F, 14.0F, -3.0F);
			wolfMane.rotateAngleX = wolfBody.rotateAngleX;
			wolfTail.setRotationPoint(-1.0F, 12.0F, 8.0F);
			wolfLeg1.setRotationPoint(-2.5F, 16.0F, 7.0F);
			wolfLeg2.setRotationPoint(0.5F, 16.0F, 7.0F);
			wolfLeg3.setRotationPoint(-2.5F, 16.0F, -4.0F);
			wolfLeg4.setRotationPoint(0.5F, 16.0F, -4.0F);
			wolfLeg1.rotateAngleX = MathHelper.cos(par2 * 0.6662F) * 1.4F
					* par3;
			wolfLeg2.rotateAngleX = MathHelper.cos(par2 * 0.6662F
					+ (float) Math.PI)
					* 1.4F * par3;
			wolfLeg3.rotateAngleX = MathHelper.cos(par2 * 0.6662F
					+ (float) Math.PI)
					* 1.4F * par3;
			wolfLeg4.rotateAngleX = MathHelper.cos(par2 * 0.6662F) * 1.4F
					* par3;
		}

		wolfHeadMain.rotateAngleZ = var5.getInterestedAngle(par4)
				+ var5.getShakeAngle(par4, 0.0F);
		wolfMane.rotateAngleZ = var5.getShakeAngle(par4, -0.08F);
		wolfBody.rotateAngleZ = var5.getShakeAngle(par4, -0.16F);
		wolfTail.rotateAngleZ = var5.getShakeAngle(par4, -0.2F);
	}

	/**
	 * Sets the model's various rotation angles. For bipeds, par1 and par2 are
	 * used for animating the movement of arms and legs, where par1 represents
	 * the time(so that arms and legs swing back and forth) and par2 represents
	 * how "far" arms and legs can swing at most.
	 */
	@Override
	public void setRotationAngles(final float par1, final float par2,
			final float par3, final float par4, final float par5,
			final float par6, final Entity par7Entity) {
		super.setRotationAngles(par1, par2, par3, par4, par5, par6, par7Entity);
		wolfHeadMain.rotateAngleX = par5 / (180F / (float) Math.PI);
		wolfHeadMain.rotateAngleY = par4 / (180F / (float) Math.PI);
		wolfTail.rotateAngleX = par3;
	}
}
