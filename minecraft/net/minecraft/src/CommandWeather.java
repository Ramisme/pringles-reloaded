package net.minecraft.src;

import java.util.List;
import java.util.Random;

import net.minecraft.server.MinecraftServer;

public class CommandWeather extends CommandBase {
	@Override
	public String getCommandName() {
		return "weather";
	}

	/**
	 * Return the required permission level for this command.
	 */
	@Override
	public int getRequiredPermissionLevel() {
		return 2;
	}

	@Override
	public void processCommand(final ICommandSender par1ICommandSender,
			final String[] par2ArrayOfStr) {
		if (par2ArrayOfStr.length < 1) {
			throw new WrongUsageException("commands.weather.usage",
					new Object[0]);
		} else {
			int var3 = (300 + new Random().nextInt(600)) * 20;

			if (par2ArrayOfStr.length >= 2) {
				var3 = CommandBase.parseIntBounded(par1ICommandSender,
						par2ArrayOfStr[1], 1, 1000000) * 20;
			}

			final WorldServer var4 = MinecraftServer.getServer().worldServers[0];
			final WorldInfo var5 = var4.getWorldInfo();
			var5.setRainTime(var3);
			var5.setThunderTime(var3);

			if ("clear".equalsIgnoreCase(par2ArrayOfStr[0])) {
				var5.setRaining(false);
				var5.setThundering(false);
				CommandBase.notifyAdmins(par1ICommandSender,
						"commands.weather.clear", new Object[0]);
			} else if ("rain".equalsIgnoreCase(par2ArrayOfStr[0])) {
				var5.setRaining(true);
				var5.setThundering(false);
				CommandBase.notifyAdmins(par1ICommandSender,
						"commands.weather.rain", new Object[0]);
			} else if ("thunder".equalsIgnoreCase(par2ArrayOfStr[0])) {
				var5.setRaining(true);
				var5.setThundering(true);
				CommandBase.notifyAdmins(par1ICommandSender,
						"commands.weather.thunder", new Object[0]);
			}
		}
	}

	/**
	 * Adds the strings available in this command to the given list of tab
	 * completion options.
	 */
	@Override
	public List addTabCompletionOptions(
			final ICommandSender par1ICommandSender,
			final String[] par2ArrayOfStr) {
		return par2ArrayOfStr.length == 1 ? CommandBase
				.getListOfStringsMatchingLastWord(par2ArrayOfStr, new String[] {
						"clear", "rain", "thunder" }) : null;
	}
}
