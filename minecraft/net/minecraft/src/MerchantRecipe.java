package net.minecraft.src;

public class MerchantRecipe {
	/** Item the Villager buys. */
	private ItemStack itemToBuy;

	/** Second Item the Villager buys. */
	private ItemStack secondItemToBuy;

	/** Item the Villager sells. */
	private ItemStack itemToSell;

	/**
	 * Saves how much has been tool used when put into to slot to be enchanted.
	 */
	private int toolUses;

	/** Maximum times this trade can be used. */
	private int maxTradeUses;

	public MerchantRecipe(final NBTTagCompound par1NBTTagCompound) {
		readFromTags(par1NBTTagCompound);
	}

	public MerchantRecipe(final ItemStack par1ItemStack,
			final ItemStack par2ItemStack, final ItemStack par3ItemStack) {
		itemToBuy = par1ItemStack;
		secondItemToBuy = par2ItemStack;
		itemToSell = par3ItemStack;
		maxTradeUses = 7;
	}

	public MerchantRecipe(final ItemStack par1ItemStack,
			final ItemStack par2ItemStack) {
		this(par1ItemStack, (ItemStack) null, par2ItemStack);
	}

	public MerchantRecipe(final ItemStack par1ItemStack, final Item par2Item) {
		this(par1ItemStack, new ItemStack(par2Item));
	}

	/**
	 * Gets the itemToBuy.
	 */
	public ItemStack getItemToBuy() {
		return itemToBuy;
	}

	/**
	 * Gets secondItemToBuy.
	 */
	public ItemStack getSecondItemToBuy() {
		return secondItemToBuy;
	}

	/**
	 * Gets if Villager has secondItemToBuy.
	 */
	public boolean hasSecondItemToBuy() {
		return secondItemToBuy != null;
	}

	/**
	 * Gets itemToSell.
	 */
	public ItemStack getItemToSell() {
		return itemToSell;
	}

	/**
	 * checks if both the first and second ItemToBuy IDs are the same
	 */
	public boolean hasSameIDsAs(final MerchantRecipe par1MerchantRecipe) {
		return itemToBuy.itemID == par1MerchantRecipe.itemToBuy.itemID
				&& itemToSell.itemID == par1MerchantRecipe.itemToSell.itemID ? secondItemToBuy == null
				&& par1MerchantRecipe.secondItemToBuy == null
				|| secondItemToBuy != null
				&& par1MerchantRecipe.secondItemToBuy != null
				&& secondItemToBuy.itemID == par1MerchantRecipe.secondItemToBuy.itemID
				: false;
	}

	/**
	 * checks first and second ItemToBuy ID's and count. Calls hasSameIDs
	 */
	public boolean hasSameItemsAs(final MerchantRecipe par1MerchantRecipe) {
		return hasSameIDsAs(par1MerchantRecipe)
				&& (itemToBuy.stackSize < par1MerchantRecipe.itemToBuy.stackSize || secondItemToBuy != null
						&& secondItemToBuy.stackSize < par1MerchantRecipe.secondItemToBuy.stackSize);
	}

	public void incrementToolUses() {
		++toolUses;
	}

	public void func_82783_a(final int par1) {
		maxTradeUses += par1;
	}

	public boolean func_82784_g() {
		return toolUses >= maxTradeUses;
	}

	public void func_82785_h() {
		toolUses = maxTradeUses;
	}

	public void readFromTags(final NBTTagCompound par1NBTTagCompound) {
		final NBTTagCompound var2 = par1NBTTagCompound.getCompoundTag("buy");
		itemToBuy = ItemStack.loadItemStackFromNBT(var2);
		final NBTTagCompound var3 = par1NBTTagCompound.getCompoundTag("sell");
		itemToSell = ItemStack.loadItemStackFromNBT(var3);

		if (par1NBTTagCompound.hasKey("buyB")) {
			secondItemToBuy = ItemStack.loadItemStackFromNBT(par1NBTTagCompound
					.getCompoundTag("buyB"));
		}

		if (par1NBTTagCompound.hasKey("uses")) {
			toolUses = par1NBTTagCompound.getInteger("uses");
		}

		if (par1NBTTagCompound.hasKey("maxUses")) {
			maxTradeUses = par1NBTTagCompound.getInteger("maxUses");
		} else {
			maxTradeUses = 7;
		}
	}

	public NBTTagCompound writeToTags() {
		final NBTTagCompound var1 = new NBTTagCompound();
		var1.setCompoundTag("buy",
				itemToBuy.writeToNBT(new NBTTagCompound("buy")));
		var1.setCompoundTag("sell",
				itemToSell.writeToNBT(new NBTTagCompound("sell")));

		if (secondItemToBuy != null) {
			var1.setCompoundTag("buyB",
					secondItemToBuy.writeToNBT(new NBTTagCompound("buyB")));
		}

		var1.setInteger("uses", toolUses);
		var1.setInteger("maxUses", maxTradeUses);
		return var1;
	}
}
