package net.minecraft.src;

import java.util.List;
import java.util.Random;

public class BlockTallGrass extends BlockFlower {
	private static final String[] grassTypes = new String[] { "deadbush",
			"tallgrass", "fern" };
	private Icon[] iconArray;

	protected BlockTallGrass(final int par1) {
		super(par1, Material.vine);
		final float var2 = 0.4F;
		setBlockBounds(0.5F - var2, 0.0F, 0.5F - var2, 0.5F + var2, 0.8F,
				0.5F + var2);
	}

	/**
	 * From the specified side and block metadata retrieves the blocks texture.
	 * Args: side, metadata
	 */
	@Override
	public Icon getIcon(final int par1, int par2) {
		if (par2 >= iconArray.length) {
			par2 = 0;
		}

		return iconArray[par2];
	}

	@Override
	public int getBlockColor() {
		final double var1 = 0.5D;
		final double var3 = 1.0D;
		return ColorizerGrass.getGrassColor(var1, var3);
	}

	/**
	 * Returns the color this block should be rendered. Used by leaves.
	 */
	@Override
	public int getRenderColor(final int par1) {
		return par1 == 0 ? 16777215 : ColorizerFoliage.getFoliageColorBasic();
	}

	/**
	 * Returns a integer with hex for 0xrrggbb with this color multiplied
	 * against the blocks color. Note only called when first determining what to
	 * render.
	 */
	@Override
	public int colorMultiplier(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		final int var5 = par1IBlockAccess.getBlockMetadata(par2, par3, par4);
		return var5 == 0 ? 16777215 : par1IBlockAccess.getBiomeGenForCoords(
				par2, par4).getBiomeGrassColor();
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return par2Random.nextInt(8) == 0 ? Item.seeds.itemID : -1;
	}

	/**
	 * Returns the usual quantity dropped by the block plus a bonus of 1 to 'i'
	 * (inclusive).
	 */
	@Override
	public int quantityDroppedWithBonus(final int par1, final Random par2Random) {
		return 1 + par2Random.nextInt(par1 * 2 + 1);
	}

	/**
	 * Called when the player destroys a block with an item that can harvest it.
	 * (i, j, k) are the coordinates of the block and l is the block's
	 * subtype/damage.
	 */
	@Override
	public void harvestBlock(final World par1World,
			final EntityPlayer par2EntityPlayer, final int par3,
			final int par4, final int par5, final int par6) {
		if (!par1World.isRemote
				&& par2EntityPlayer.getCurrentEquippedItem() != null
				&& par2EntityPlayer.getCurrentEquippedItem().itemID == Item.shears.itemID) {
			par2EntityPlayer.addStat(StatList.mineBlockStatArray[blockID], 1);
			dropBlockAsItem_do(par1World, par3, par4, par5, new ItemStack(
					Block.tallGrass, 1, par6));
		} else {
			super.harvestBlock(par1World, par2EntityPlayer, par3, par4, par5,
					par6);
		}
	}

	/**
	 * Get the block's damage value (for use with pick block).
	 */
	@Override
	public int getDamageValue(final World par1World, final int par2,
			final int par3, final int par4) {
		return par1World.getBlockMetadata(par2, par3, par4);
	}

	/**
	 * returns a list of blocks with the same ID, but different meta (eg: wood
	 * returns 4 blocks)
	 */
	@Override
	public void getSubBlocks(final int par1,
			final CreativeTabs par2CreativeTabs, final List par3List) {
		for (int var4 = 1; var4 < 3; ++var4) {
			par3List.add(new ItemStack(par1, 1, var4));
		}
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		iconArray = new Icon[BlockTallGrass.grassTypes.length];

		for (int var2 = 0; var2 < iconArray.length; ++var2) {
			iconArray[var2] = par1IconRegister
					.registerIcon(BlockTallGrass.grassTypes[var2]);
		}
	}
}
