package net.minecraft.src;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import net.minecraft.server.MinecraftServer;

public class EntityPlayerMP extends EntityPlayer implements ICrafting {
	private final StringTranslate translator = new StringTranslate("en_US");

	/**
	 * The NetServerHandler assigned to this player by the
	 * ServerConfigurationManager.
	 */
	public NetServerHandler playerNetServerHandler;

	/** Reference to the MinecraftServer object. */
	public MinecraftServer mcServer;

	/** The ItemInWorldManager belonging to this player */
	public ItemInWorldManager theItemInWorldManager;

	/** player X position as seen by PlayerManager */
	public double managedPosX;

	/** player Z position as seen by PlayerManager */
	public double managedPosZ;

	/** LinkedList that holds the loaded chunks. */
	public final List loadedChunks = new LinkedList();

	/** entities added to this list will be packet29'd to the player */
	public final List destroyedItemsNetCache = new LinkedList();

	/** set to getHealth */
	private int lastHealth = -99999999;

	/** set to foodStats.GetFoodLevel */
	private int lastFoodLevel = -99999999;

	/** set to foodStats.getSaturationLevel() == 0.0F each tick */
	private boolean wasHungry = true;

	/** Amount of experience the client was last set to */
	private int lastExperience = -99999999;

	/** de-increments onUpdate, attackEntityFrom is ignored if this >0 */
	private int initialInvulnerability = 60;

	/** must be between 3>x>15 (strictly between) */
	private int renderDistance = 0;
	private int chatVisibility = 0;
	private boolean chatColours = true;

	/**
	 * The currently in use window ID. Incremented every time a window is
	 * opened.
	 */
	private int currentWindowId = 0;

	/**
	 * poor mans concurency flag, lets hope the jvm doesn't re-order the setting
	 * of this flag wrt the inventory change on the next line
	 */
	public boolean playerInventoryBeingManipulated;
	public int ping;

	/**
	 * Set when a player beats the ender dragon, used to respawn the player at
	 * the spawn point while retaining inventory and XP
	 */
	public boolean playerConqueredTheEnd = false;

	public EntityPlayerMP(final MinecraftServer par1MinecraftServer,
			final World par2World, final String par3Str,
			final ItemInWorldManager par4ItemInWorldManager) {
		super(par2World);
		par4ItemInWorldManager.thisPlayerMP = this;
		theItemInWorldManager = par4ItemInWorldManager;
		renderDistance = par1MinecraftServer.getConfigurationManager()
				.getViewDistance();
		final ChunkCoordinates var5 = par2World.getSpawnPoint();
		int var6 = var5.posX;
		int var7 = var5.posZ;
		int var8 = var5.posY;

		if (!par2World.provider.hasNoSky
				&& par2World.getWorldInfo().getGameType() != EnumGameType.ADVENTURE) {
			final int var9 = Math.max(5,
					par1MinecraftServer.getSpawnProtectionSize() - 6);
			var6 += rand.nextInt(var9 * 2) - var9;
			var7 += rand.nextInt(var9 * 2) - var9;
			var8 = par2World.getTopSolidOrLiquidBlock(var6, var7);
		}

		mcServer = par1MinecraftServer;
		stepHeight = 0.0F;
		username = par3Str;
		yOffset = 0.0F;
		setLocationAndAngles(var6 + 0.5D, var8, var7 + 0.5D, 0.0F, 0.0F);

		while (!par2World.getCollidingBoundingBoxes(this, boundingBox)
				.isEmpty()) {
			setPosition(posX, posY + 1.0D, posZ);
		}
	}

	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	@Override
	public void readEntityFromNBT(final NBTTagCompound par1NBTTagCompound) {
		super.readEntityFromNBT(par1NBTTagCompound);

		if (par1NBTTagCompound.hasKey("playerGameType")) {
			if (MinecraftServer.getServer().func_104056_am()) {
				theItemInWorldManager.setGameType(MinecraftServer.getServer()
						.getGameType());
			} else {
				theItemInWorldManager.setGameType(EnumGameType
						.getByID(par1NBTTagCompound
								.getInteger("playerGameType")));
			}
		}
	}

	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	@Override
	public void writeEntityToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeEntityToNBT(par1NBTTagCompound);
		par1NBTTagCompound.setInteger("playerGameType", theItemInWorldManager
				.getGameType().getID());
	}

	/**
	 * Add experience levels to this player.
	 */
	@Override
	public void addExperienceLevel(final int par1) {
		super.addExperienceLevel(par1);
		lastExperience = -1;
	}

	public void addSelfToInternalCraftingInventory() {
		openContainer.addCraftingToCrafters(this);
	}

	/**
	 * sets the players height back to normal after doing things like sleeping
	 * and dieing
	 */
	@Override
	protected void resetHeight() {
		yOffset = 0.0F;
	}

	@Override
	public float getEyeHeight() {
		return 1.62F;
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate() {
		theItemInWorldManager.updateBlockRemoving();
		--initialInvulnerability;
		openContainer.detectAndSendChanges();

		while (!destroyedItemsNetCache.isEmpty()) {
			final int var1 = Math.min(destroyedItemsNetCache.size(), 127);
			final int[] var2 = new int[var1];
			final Iterator var3 = destroyedItemsNetCache.iterator();
			int var4 = 0;

			while (var3.hasNext() && var4 < var1) {
				var2[var4++] = ((Integer) var3.next()).intValue();
				var3.remove();
			}

			playerNetServerHandler
					.sendPacketToPlayer(new Packet29DestroyEntity(var2));
		}

		if (!loadedChunks.isEmpty()) {
			final ArrayList var6 = new ArrayList();
			final Iterator var7 = loadedChunks.iterator();
			final ArrayList var8 = new ArrayList();

			while (var7.hasNext() && var6.size() < 5) {
				final ChunkCoordIntPair var9 = (ChunkCoordIntPair) var7.next();
				var7.remove();

				if (var9 != null
						&& worldObj.blockExists(var9.chunkXPos << 4, 0,
								var9.chunkZPos << 4)) {
					var6.add(worldObj.getChunkFromChunkCoords(var9.chunkXPos,
							var9.chunkZPos));
					var8.addAll(((WorldServer) worldObj).getAllTileEntityInBox(
							var9.chunkXPos * 16, 0, var9.chunkZPos * 16,
							var9.chunkXPos * 16 + 16, 256,
							var9.chunkZPos * 16 + 16));
				}
			}

			if (!var6.isEmpty()) {
				playerNetServerHandler
						.sendPacketToPlayer(new Packet56MapChunks(var6));
				Iterator var11 = var8.iterator();

				while (var11.hasNext()) {
					final TileEntity var5 = (TileEntity) var11.next();
					sendTileEntityToPlayer(var5);
				}

				var11 = var6.iterator();

				while (var11.hasNext()) {
					final Chunk var10 = (Chunk) var11.next();
					getServerForPlayer().getEntityTracker().func_85172_a(this,
							var10);
				}
			}
		}
	}

	@Override
	public void setEntityHealth(final int par1) {
		super.setEntityHealth(par1);
		final Collection var2 = getWorldScoreboard().func_96520_a(
				ScoreObjectiveCriteria.field_96638_f);
		final Iterator var3 = var2.iterator();

		while (var3.hasNext()) {
			final ScoreObjective var4 = (ScoreObjective) var3.next();
			getWorldScoreboard().func_96529_a(getEntityName(), var4)
					.func_96651_a(Arrays.asList(new EntityPlayer[] { this }));
		}
	}

	public void onUpdateEntity() {
		try {
			super.onUpdate();

			for (int var1 = 0; var1 < inventory.getSizeInventory(); ++var1) {
				final ItemStack var5 = inventory.getStackInSlot(var1);

				if (var5 != null && Item.itemsList[var5.itemID].isMap()
						&& playerNetServerHandler.packetSize() <= 5) {
					final Packet var6 = ((ItemMapBase) Item.itemsList[var5.itemID])
							.createMapDataPacket(var5, worldObj, this);

					if (var6 != null) {
						playerNetServerHandler.sendPacketToPlayer(var6);
					}
				}
			}

			if (getHealth() != lastHealth
					|| lastFoodLevel != foodStats.getFoodLevel()
					|| foodStats.getSaturationLevel() == 0.0F != wasHungry) {
				playerNetServerHandler
						.sendPacketToPlayer(new Packet8UpdateHealth(
								getHealth(), foodStats.getFoodLevel(),
								foodStats.getSaturationLevel()));
				lastHealth = getHealth();
				lastFoodLevel = foodStats.getFoodLevel();
				wasHungry = foodStats.getSaturationLevel() == 0.0F;
			}

			if (experienceTotal != lastExperience) {
				lastExperience = experienceTotal;
				playerNetServerHandler
						.sendPacketToPlayer(new Packet43Experience(experience,
								experienceTotal, experienceLevel));
			}
		} catch (final Throwable var4) {
			final CrashReport var2 = CrashReport.makeCrashReport(var4,
					"Ticking player");
			final CrashReportCategory var3 = var2
					.makeCategory("Player being ticked");
			func_85029_a(var3);
			throw new ReportedException(var2);
		}
	}

	/**
	 * Called when the mob's health reaches 0.
	 */
	@Override
	public void onDeath(final DamageSource par1DamageSource) {
		mcServer.getConfigurationManager().sendChatMsg(
				field_94063_bt.func_94546_b());

		if (!worldObj.getGameRules().getGameRuleBooleanValue("keepInventory")) {
			inventory.dropAllItems();
		}

		final Collection var2 = worldObj.getScoreboard().func_96520_a(
				ScoreObjectiveCriteria.field_96642_c);
		final Iterator var3 = var2.iterator();

		while (var3.hasNext()) {
			final ScoreObjective var4 = (ScoreObjective) var3.next();
			final Score var5 = getWorldScoreboard().func_96529_a(
					getEntityName(), var4);
			var5.func_96648_a();
		}

		final EntityLiving var6 = func_94060_bK();

		if (var6 != null) {
			var6.addToPlayerScore(this, scoreValue);
		}
	}

	/**
	 * Called when the entity is attacked.
	 */
	@Override
	public boolean attackEntityFrom(final DamageSource par1DamageSource,
			final int par2) {
		if (isEntityInvulnerable()) {
			return false;
		} else {
			final boolean var3 = mcServer.isDedicatedServer()
					&& mcServer.isPVPEnabled()
					&& "fall".equals(par1DamageSource.damageType);

			if (!var3 && initialInvulnerability > 0
					&& par1DamageSource != DamageSource.outOfWorld) {
				return false;
			} else {
				if (par1DamageSource instanceof EntityDamageSource) {
					final Entity var4 = par1DamageSource.getEntity();

					if (var4 instanceof EntityPlayer
							&& !func_96122_a((EntityPlayer) var4)) {
						return false;
					}

					if (var4 instanceof EntityArrow) {
						final EntityArrow var5 = (EntityArrow) var4;

						if (var5.shootingEntity instanceof EntityPlayer
								&& !func_96122_a((EntityPlayer) var5.shootingEntity)) {
							return false;
						}
					}
				}

				return super.attackEntityFrom(par1DamageSource, par2);
			}
		}
	}

	@Override
	public boolean func_96122_a(final EntityPlayer par1EntityPlayer) {
		return !mcServer.isPVPEnabled() ? false : super
				.func_96122_a(par1EntityPlayer);
	}

	/**
	 * Teleports the entity to another dimension. Params: Dimension number to
	 * teleport to
	 */
	@Override
	public void travelToDimension(int par1) {
		if (dimension == 1 && par1 == 1) {
			triggerAchievement(AchievementList.theEnd2);
			worldObj.removeEntity(this);
			playerConqueredTheEnd = true;
			playerNetServerHandler.sendPacketToPlayer(new Packet70GameEvent(4,
					0));
		} else {
			if (dimension == 1 && par1 == 0) {
				triggerAchievement(AchievementList.theEnd);
				final ChunkCoordinates var2 = mcServer.worldServerForDimension(
						par1).getEntrancePortalLocation();

				if (var2 != null) {
					playerNetServerHandler.setPlayerLocation(var2.posX,
							var2.posY, var2.posZ, 0.0F, 0.0F);
				}

				par1 = 1;
			} else {
				triggerAchievement(AchievementList.portal);
			}

			mcServer.getConfigurationManager().transferPlayerToDimension(this,
					par1);
			lastExperience = -1;
			lastHealth = -1;
			lastFoodLevel = -1;
		}
	}

	/**
	 * called from onUpdate for all tileEntity in specific chunks
	 */
	private void sendTileEntityToPlayer(final TileEntity par1TileEntity) {
		if (par1TileEntity != null) {
			final Packet var2 = par1TileEntity.getDescriptionPacket();

			if (var2 != null) {
				playerNetServerHandler.sendPacketToPlayer(var2);
			}
		}
	}

	/**
	 * Called whenever an item is picked up from walking over it. Args:
	 * pickedUpEntity, stackSize
	 */
	@Override
	public void onItemPickup(final Entity par1Entity, final int par2) {
		super.onItemPickup(par1Entity, par2);
		openContainer.detectAndSendChanges();
	}

	/**
	 * Attempts to have the player sleep in a bed at the specified location.
	 */
	@Override
	public EnumStatus sleepInBedAt(final int par1, final int par2,
			final int par3) {
		final EnumStatus var4 = super.sleepInBedAt(par1, par2, par3);

		if (var4 == EnumStatus.OK) {
			final Packet17Sleep var5 = new Packet17Sleep(this, 0, par1, par2,
					par3);
			getServerForPlayer().getEntityTracker()
					.sendPacketToAllPlayersTrackingEntity(this, var5);
			playerNetServerHandler.setPlayerLocation(posX, posY, posZ,
					rotationYaw, rotationPitch);
			playerNetServerHandler.sendPacketToPlayer(var5);
		}

		return var4;
	}

	/**
	 * Wake up the player if they're sleeping.
	 */
	@Override
	public void wakeUpPlayer(final boolean par1, final boolean par2,
			final boolean par3) {
		if (isPlayerSleeping()) {
			getServerForPlayer().getEntityTracker()
					.sendPacketToAllAssociatedPlayers(this,
							new Packet18Animation(this, 3));
		}

		super.wakeUpPlayer(par1, par2, par3);

		if (playerNetServerHandler != null) {
			playerNetServerHandler.setPlayerLocation(posX, posY, posZ,
					rotationYaw, rotationPitch);
		}
	}

	/**
	 * Called when a player mounts an entity. e.g. mounts a pig, mounts a boat.
	 */
	@Override
	public void mountEntity(final Entity par1Entity) {
		super.mountEntity(par1Entity);
		playerNetServerHandler.sendPacketToPlayer(new Packet39AttachEntity(
				this, ridingEntity));
		playerNetServerHandler.setPlayerLocation(posX, posY, posZ, rotationYaw,
				rotationPitch);
	}

	/**
	 * Takes in the distance the entity has fallen this tick and whether its on
	 * the ground to update the fall distance and deal fall damage if landing on
	 * the ground. Args: distanceFallenThisTick, onGround
	 */
	@Override
	protected void updateFallState(final double par1, final boolean par3) {
	}

	/**
	 * likeUpdateFallState, but called from updateFlyingState, rather than
	 * moveEntity
	 */
	public void updateFlyingState(final double par1, final boolean par3) {
		super.updateFallState(par1, par3);
	}

	private void incrementWindowID() {
		currentWindowId = currentWindowId % 100 + 1;
	}

	/**
	 * Displays the crafting GUI for a workbench.
	 */
	@Override
	public void displayGUIWorkbench(final int par1, final int par2,
			final int par3) {
		incrementWindowID();
		playerNetServerHandler.sendPacketToPlayer(new Packet100OpenWindow(
				currentWindowId, 1, "Crafting", 9, true));
		openContainer = new ContainerWorkbench(inventory, worldObj, par1, par2,
				par3);
		openContainer.windowId = currentWindowId;
		openContainer.addCraftingToCrafters(this);
	}

	@Override
	public void displayGUIEnchantment(final int par1, final int par2,
			final int par3, final String par4Str) {
		incrementWindowID();
		playerNetServerHandler.sendPacketToPlayer(new Packet100OpenWindow(
				currentWindowId, 4, par4Str == null ? "" : par4Str, 9,
				par4Str != null));
		openContainer = new ContainerEnchantment(inventory, worldObj, par1,
				par2, par3);
		openContainer.windowId = currentWindowId;
		openContainer.addCraftingToCrafters(this);
	}

	/**
	 * Displays the GUI for interacting with an anvil.
	 */
	@Override
	public void displayGUIAnvil(final int par1, final int par2, final int par3) {
		incrementWindowID();
		playerNetServerHandler.sendPacketToPlayer(new Packet100OpenWindow(
				currentWindowId, 8, "Repairing", 9, true));
		openContainer = new ContainerRepair(inventory, worldObj, par1, par2,
				par3, this);
		openContainer.windowId = currentWindowId;
		openContainer.addCraftingToCrafters(this);
	}

	/**
	 * Displays the GUI for interacting with a chest inventory. Args:
	 * chestInventory
	 */
	@Override
	public void displayGUIChest(final IInventory par1IInventory) {
		if (openContainer != inventoryContainer) {
			closeScreen();
		}

		incrementWindowID();
		playerNetServerHandler.sendPacketToPlayer(new Packet100OpenWindow(
				currentWindowId, 0, par1IInventory.getInvName(), par1IInventory
						.getSizeInventory(), par1IInventory
						.isInvNameLocalized()));
		openContainer = new ContainerChest(inventory, par1IInventory);
		openContainer.windowId = currentWindowId;
		openContainer.addCraftingToCrafters(this);
	}

	@Override
	public void displayGUIHopper(final TileEntityHopper par1TileEntityHopper) {
		incrementWindowID();
		playerNetServerHandler.sendPacketToPlayer(new Packet100OpenWindow(
				currentWindowId, 9, par1TileEntityHopper.getInvName(),
				par1TileEntityHopper.getSizeInventory(), par1TileEntityHopper
						.isInvNameLocalized()));
		openContainer = new ContainerHopper(inventory, par1TileEntityHopper);
		openContainer.windowId = currentWindowId;
		openContainer.addCraftingToCrafters(this);
	}

	@Override
	public void displayGUIHopperMinecart(
			final EntityMinecartHopper par1EntityMinecartHopper) {
		incrementWindowID();
		playerNetServerHandler.sendPacketToPlayer(new Packet100OpenWindow(
				currentWindowId, 9, par1EntityMinecartHopper.getInvName(),
				par1EntityMinecartHopper.getSizeInventory(),
				par1EntityMinecartHopper.isInvNameLocalized()));
		openContainer = new ContainerHopper(inventory, par1EntityMinecartHopper);
		openContainer.windowId = currentWindowId;
		openContainer.addCraftingToCrafters(this);
	}

	/**
	 * Displays the furnace GUI for the passed in furnace entity. Args:
	 * tileEntityFurnace
	 */
	@Override
	public void displayGUIFurnace(final TileEntityFurnace par1TileEntityFurnace) {
		incrementWindowID();
		playerNetServerHandler.sendPacketToPlayer(new Packet100OpenWindow(
				currentWindowId, 2, par1TileEntityFurnace.getInvName(),
				par1TileEntityFurnace.getSizeInventory(), par1TileEntityFurnace
						.isInvNameLocalized()));
		openContainer = new ContainerFurnace(inventory, par1TileEntityFurnace);
		openContainer.windowId = currentWindowId;
		openContainer.addCraftingToCrafters(this);
	}

	/**
	 * Displays the dipsenser GUI for the passed in dispenser entity. Args:
	 * TileEntityDispenser
	 */
	@Override
	public void displayGUIDispenser(
			final TileEntityDispenser par1TileEntityDispenser) {
		incrementWindowID();
		playerNetServerHandler.sendPacketToPlayer(new Packet100OpenWindow(
				currentWindowId,
				par1TileEntityDispenser instanceof TileEntityDropper ? 10 : 3,
				par1TileEntityDispenser.getInvName(), par1TileEntityDispenser
						.getSizeInventory(), par1TileEntityDispenser
						.isInvNameLocalized()));
		openContainer = new ContainerDispenser(inventory,
				par1TileEntityDispenser);
		openContainer.windowId = currentWindowId;
		openContainer.addCraftingToCrafters(this);
	}

	/**
	 * Displays the GUI for interacting with a brewing stand.
	 */
	@Override
	public void displayGUIBrewingStand(
			final TileEntityBrewingStand par1TileEntityBrewingStand) {
		incrementWindowID();
		playerNetServerHandler.sendPacketToPlayer(new Packet100OpenWindow(
				currentWindowId, 5, par1TileEntityBrewingStand.getInvName(),
				par1TileEntityBrewingStand.getSizeInventory(),
				par1TileEntityBrewingStand.isInvNameLocalized()));
		openContainer = new ContainerBrewingStand(inventory,
				par1TileEntityBrewingStand);
		openContainer.windowId = currentWindowId;
		openContainer.addCraftingToCrafters(this);
	}

	/**
	 * Displays the GUI for interacting with a beacon.
	 */
	@Override
	public void displayGUIBeacon(final TileEntityBeacon par1TileEntityBeacon) {
		incrementWindowID();
		playerNetServerHandler.sendPacketToPlayer(new Packet100OpenWindow(
				currentWindowId, 7, par1TileEntityBeacon.getInvName(),
				par1TileEntityBeacon.getSizeInventory(), par1TileEntityBeacon
						.isInvNameLocalized()));
		openContainer = new ContainerBeacon(inventory, par1TileEntityBeacon);
		openContainer.windowId = currentWindowId;
		openContainer.addCraftingToCrafters(this);
	}

	@Override
	public void displayGUIMerchant(final IMerchant par1IMerchant,
			final String par2Str) {
		incrementWindowID();
		openContainer = new ContainerMerchant(inventory, par1IMerchant,
				worldObj);
		openContainer.windowId = currentWindowId;
		openContainer.addCraftingToCrafters(this);
		final InventoryMerchant var3 = ((ContainerMerchant) openContainer)
				.getMerchantInventory();
		playerNetServerHandler.sendPacketToPlayer(new Packet100OpenWindow(
				currentWindowId, 6, par2Str == null ? "" : par2Str, var3
						.getSizeInventory(), par2Str != null));
		final MerchantRecipeList var4 = par1IMerchant.getRecipes(this);

		if (var4 != null) {
			try {
				final ByteArrayOutputStream var5 = new ByteArrayOutputStream();
				final DataOutputStream var6 = new DataOutputStream(var5);
				var6.writeInt(currentWindowId);
				var4.writeRecipiesToStream(var6);
				playerNetServerHandler
						.sendPacketToPlayer(new Packet250CustomPayload(
								"MC|TrList", var5.toByteArray()));
			} catch (final IOException var7) {
				var7.printStackTrace();
			}
		}
	}

	/**
	 * Sends the contents of an inventory slot to the client-side Container.
	 * This doesn't have to match the actual contents of that slot. Args:
	 * Container, slot number, slot contents
	 */
	@Override
	public void sendSlotContents(final Container par1Container, final int par2,
			final ItemStack par3ItemStack) {
		if (!(par1Container.getSlot(par2) instanceof SlotCrafting)) {
			if (!playerInventoryBeingManipulated) {
				playerNetServerHandler.sendPacketToPlayer(new Packet103SetSlot(
						par1Container.windowId, par2, par3ItemStack));
			}
		}
	}

	public void sendContainerToPlayer(final Container par1Container) {
		sendContainerAndContentsToPlayer(par1Container,
				par1Container.getInventory());
	}

	@Override
	public void sendContainerAndContentsToPlayer(final Container par1Container,
			final List par2List) {
		playerNetServerHandler.sendPacketToPlayer(new Packet104WindowItems(
				par1Container.windowId, par2List));
		playerNetServerHandler.sendPacketToPlayer(new Packet103SetSlot(-1, -1,
				inventory.getItemStack()));
	}

	/**
	 * Sends two ints to the client-side Container. Used for furnace burning
	 * time, smelting progress, brewing progress, and enchanting level. Normally
	 * the first int identifies which variable to update, and the second
	 * contains the new value. Both are truncated to shorts in non-local SMP.
	 */
	@Override
	public void sendProgressBarUpdate(final Container par1Container,
			final int par2, final int par3) {
		playerNetServerHandler
				.sendPacketToPlayer(new Packet105UpdateProgressbar(
						par1Container.windowId, par2, par3));
	}

	/**
	 * sets current screen to null (used on escape buttons of GUIs)
	 */
	@Override
	public void closeScreen() {
		playerNetServerHandler.sendPacketToPlayer(new Packet101CloseWindow(
				openContainer.windowId));
		closeInventory();
	}

	/**
	 * updates item held by mouse
	 */
	public void updateHeldItem() {
		if (!playerInventoryBeingManipulated) {
			playerNetServerHandler.sendPacketToPlayer(new Packet103SetSlot(-1,
					-1, inventory.getItemStack()));
		}
	}

	public void closeInventory() {
		openContainer.onCraftGuiClosed(this);
		openContainer = inventoryContainer;
	}

	/**
	 * Adds a value to a statistic field.
	 */
	@Override
	public void addStat(final StatBase par1StatBase, int par2) {
		if (par1StatBase != null) {
			if (!par1StatBase.isIndependent) {
				while (par2 > 100) {
					playerNetServerHandler
							.sendPacketToPlayer(new Packet200Statistic(
									par1StatBase.statId, 100));
					par2 -= 100;
				}

				playerNetServerHandler
						.sendPacketToPlayer(new Packet200Statistic(
								par1StatBase.statId, par2));
			}
		}
	}

	public void mountEntityAndWakeUp() {
		if (riddenByEntity != null) {
			riddenByEntity.mountEntity(this);
		}

		if (sleeping) {
			wakeUpPlayer(true, false, false);
		}
	}

	/**
	 * this function is called when a players inventory is sent to him,
	 * lastHealth is updated on any dimension transitions, then reset.
	 */
	public void setPlayerHealthUpdated() {
		lastHealth = -99999999;
	}

	/**
	 * Add a chat message to the player
	 */
	@Override
	public void addChatMessage(final String par1Str) {
		final StringTranslate var2 = StringTranslate.getInstance();
		final String var3 = var2.translateKey(par1Str);
		playerNetServerHandler.sendPacketToPlayer(new Packet3Chat(var3));
	}

	/**
	 * Used for when item use count runs out, ie: eating completed
	 */
	@Override
	protected void onItemUseFinish() {
		playerNetServerHandler.sendPacketToPlayer(new Packet38EntityStatus(
				entityId, (byte) 9));
		super.onItemUseFinish();
	}

	/**
	 * sets the itemInUse when the use item button is clicked. Args: itemstack,
	 * int maxItemUseDuration
	 */
	@Override
	public void setItemInUse(final ItemStack par1ItemStack, final int par2) {
		super.setItemInUse(par1ItemStack, par2);

		if (par1ItemStack != null
				&& par1ItemStack.getItem() != null
				&& par1ItemStack.getItem().getItemUseAction(par1ItemStack) == EnumAction.eat) {
			getServerForPlayer().getEntityTracker()
					.sendPacketToAllAssociatedPlayers(this,
							new Packet18Animation(this, 5));
		}
	}

	/**
	 * Copies the values from the given player into this player if boolean par2
	 * is true. Always clones Ender Chest Inventory.
	 */
	@Override
	public void clonePlayer(final EntityPlayer par1EntityPlayer,
			final boolean par2) {
		super.clonePlayer(par1EntityPlayer, par2);
		lastExperience = -1;
		lastHealth = -1;
		lastFoodLevel = -1;
		destroyedItemsNetCache
				.addAll(((EntityPlayerMP) par1EntityPlayer).destroyedItemsNetCache);
	}

	@Override
	protected void onNewPotionEffect(final PotionEffect par1PotionEffect) {
		super.onNewPotionEffect(par1PotionEffect);
		playerNetServerHandler.sendPacketToPlayer(new Packet41EntityEffect(
				entityId, par1PotionEffect));
	}

	@Override
	protected void onChangedPotionEffect(final PotionEffect par1PotionEffect) {
		super.onChangedPotionEffect(par1PotionEffect);
		playerNetServerHandler.sendPacketToPlayer(new Packet41EntityEffect(
				entityId, par1PotionEffect));
	}

	@Override
	protected void onFinishedPotionEffect(final PotionEffect par1PotionEffect) {
		super.onFinishedPotionEffect(par1PotionEffect);
		playerNetServerHandler
				.sendPacketToPlayer(new Packet42RemoveEntityEffect(entityId,
						par1PotionEffect));
	}

	/**
	 * Move the entity to the coordinates informed, but keep yaw/pitch values.
	 */
	@Override
	public void setPositionAndUpdate(final double par1, final double par3,
			final double par5) {
		playerNetServerHandler.setPlayerLocation(par1, par3, par5, rotationYaw,
				rotationPitch);
	}

	/**
	 * Called when the player performs a critical hit on the Entity. Args:
	 * entity that was hit critically
	 */
	@Override
	public void onCriticalHit(final Entity par1Entity) {
		getServerForPlayer().getEntityTracker()
				.sendPacketToAllAssociatedPlayers(this,
						new Packet18Animation(par1Entity, 6));
	}

	@Override
	public void onEnchantmentCritical(final Entity par1Entity) {
		getServerForPlayer().getEntityTracker()
				.sendPacketToAllAssociatedPlayers(this,
						new Packet18Animation(par1Entity, 7));
	}

	/**
	 * Sends the player's abilities to the server (if there is one).
	 */
	@Override
	public void sendPlayerAbilities() {
		if (playerNetServerHandler != null) {
			playerNetServerHandler
					.sendPacketToPlayer(new Packet202PlayerAbilities(
							capabilities));
		}
	}

	public WorldServer getServerForPlayer() {
		return (WorldServer) worldObj;
	}

	/**
	 * Sets the player's game mode and sends it to them.
	 */
	@Override
	public void setGameType(final EnumGameType par1EnumGameType) {
		theItemInWorldManager.setGameType(par1EnumGameType);
		playerNetServerHandler.sendPacketToPlayer(new Packet70GameEvent(3,
				par1EnumGameType.getID()));
	}

	@Override
	public void sendChatToPlayer(final String par1Str) {
		playerNetServerHandler.sendPacketToPlayer(new Packet3Chat(par1Str));
	}

	/**
	 * Returns true if the command sender is allowed to use the given command.
	 */
	@Override
	public boolean canCommandSenderUseCommand(final int par1,
			final String par2Str) {
		return "seed".equals(par2Str) && !mcServer.isDedicatedServer() ? true
				: !"tell".equals(par2Str) && !"help".equals(par2Str)
						&& !"me".equals(par2Str) ? mcServer
						.getConfigurationManager().areCommandsAllowed(username)
						: true;
	}

	/**
	 * Gets the player's IP address. Used in /banip.
	 */
	public String getPlayerIP() {
		String var1 = playerNetServerHandler.netManager.getSocketAddress()
				.toString();
		var1 = var1.substring(var1.indexOf("/") + 1);
		var1 = var1.substring(0, var1.indexOf(":"));
		return var1;
	}

	public void updateClientInfo(
			final Packet204ClientInfo par1Packet204ClientInfo) {
		if (translator.getLanguageList().containsKey(
				par1Packet204ClientInfo.getLanguage())) {
			translator
					.setLanguage(par1Packet204ClientInfo.getLanguage(), false);
		}

		final int var2 = 256 >> par1Packet204ClientInfo.getRenderDistance();

		if (var2 > 3 && var2 < 15) {
			renderDistance = var2;
		}

		chatVisibility = par1Packet204ClientInfo.getChatVisibility();
		chatColours = par1Packet204ClientInfo.getChatColours();

		if (mcServer.isSinglePlayer()
				&& mcServer.getServerOwner().equals(username)) {
			mcServer.setDifficultyForAllWorlds(par1Packet204ClientInfo
					.getDifficulty());
		}

		setHideCape(1, !par1Packet204ClientInfo.getShowCape());
	}

	@Override
	public StringTranslate getTranslator() {
		return translator;
	}

	public int getChatVisibility() {
		return chatVisibility;
	}

	/**
	 * on recieving this message the client (if permission is given) will
	 * download the requested textures
	 */
	public void requestTexturePackLoad(final String par1Str, final int par2) {
		final String var3 = par1Str + "\u0000" + par2;
		playerNetServerHandler.sendPacketToPlayer(new Packet250CustomPayload(
				"MC|TPack", var3.getBytes()));
	}

	/**
	 * Return the position for this command sender.
	 */
	@Override
	public ChunkCoordinates getPlayerCoordinates() {
		return new ChunkCoordinates(MathHelper.floor_double(posX),
				MathHelper.floor_double(posY + 0.5D),
				MathHelper.floor_double(posZ));
	}
}
