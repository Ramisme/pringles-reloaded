package net.minecraft.src;

public class EntityDamageSource extends DamageSource {
	protected Entity damageSourceEntity;

	public EntityDamageSource(final String par1Str, final Entity par2Entity) {
		super(par1Str);
		damageSourceEntity = par2Entity;
	}

	@Override
	public Entity getEntity() {
		return damageSourceEntity;
	}

	/**
	 * Returns the message to be displayed on player death.
	 */
	@Override
	public String getDeathMessage(final EntityLiving par1EntityLiving) {
		final ItemStack var2 = damageSourceEntity instanceof EntityLiving ? ((EntityLiving) damageSourceEntity)
				.getHeldItem() : null;
		final String var3 = "death.attack." + damageType;
		final String var4 = var3 + ".item";
		return var2 != null && var2.hasDisplayName()
				&& StatCollector.func_94522_b(var4) ? StatCollector
				.translateToLocalFormatted(
						var4,
						new Object[] {
								par1EntityLiving.getTranslatedEntityName(),
								damageSourceEntity.getTranslatedEntityName(),
								var2.getDisplayName() }) : StatCollector
				.translateToLocalFormatted(var3, new Object[] {
						par1EntityLiving.getTranslatedEntityName(),
						damageSourceEntity.getTranslatedEntityName() });
	}

	/**
	 * Return whether this damage source will have its damage amount scaled
	 * based on the current difficulty.
	 */
	@Override
	public boolean isDifficultyScaled() {
		return damageSourceEntity != null
				&& damageSourceEntity instanceof EntityLiving
				&& !(damageSourceEntity instanceof EntityPlayer);
	}
}
