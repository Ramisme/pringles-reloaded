package net.minecraft.src;

public class EntitySplashFX extends EntityRainFX {
	public EntitySplashFX(final World par1World, final double par2,
			final double par4, final double par6, final double par8,
			final double par10, final double par12) {
		super(par1World, par2, par4, par6);
		particleGravity = 0.04F;
		nextTextureIndexX();

		if (par10 == 0.0D && (par8 != 0.0D || par12 != 0.0D)) {
			motionX = par8;
			motionY = par10 + 0.1D;
			motionZ = par12;
		}
	}
}
