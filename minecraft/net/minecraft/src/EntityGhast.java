package net.minecraft.src;

public class EntityGhast extends EntityFlying implements IMob {
	public int courseChangeCooldown = 0;
	public double waypointX;
	public double waypointY;
	public double waypointZ;
	private Entity targetedEntity = null;

	/** Cooldown time between target loss and new target aquirement. */
	private int aggroCooldown = 0;
	public int prevAttackCounter = 0;
	public int attackCounter = 0;

	/** The explosion radius of spawned fireballs. */
	private int explosionStrength = 1;

	public EntityGhast(final World par1World) {
		super(par1World);
		texture = "/mob/ghast.png";
		setSize(4.0F, 4.0F);
		isImmuneToFire = true;
		experienceValue = 5;
	}

	/**
	 * Called when the entity is attacked.
	 */
	@Override
	public boolean attackEntityFrom(final DamageSource par1DamageSource,
			final int par2) {
		if (isEntityInvulnerable()) {
			return false;
		} else if ("fireball".equals(par1DamageSource.getDamageType())
				&& par1DamageSource.getEntity() instanceof EntityPlayer) {
			super.attackEntityFrom(par1DamageSource, 1000);
			((EntityPlayer) par1DamageSource.getEntity())
					.triggerAchievement(AchievementList.ghast);
			return true;
		} else {
			return super.attackEntityFrom(par1DamageSource, par2);
		}
	}

	@Override
	protected void entityInit() {
		super.entityInit();
		dataWatcher.addObject(16, Byte.valueOf((byte) 0));
	}

	@Override
	public int getMaxHealth() {
		return 10;
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate() {
		super.onUpdate();
		final byte var1 = dataWatcher.getWatchableObjectByte(16);
		texture = var1 == 1 ? "/mob/ghast_fire.png" : "/mob/ghast.png";
	}

	@Override
	protected void updateEntityActionState() {
		if (!worldObj.isRemote && worldObj.difficultySetting == 0) {
			setDead();
		}

		despawnEntity();
		prevAttackCounter = attackCounter;
		final double var1 = waypointX - posX;
		final double var3 = waypointY - posY;
		final double var5 = waypointZ - posZ;
		double var7 = var1 * var1 + var3 * var3 + var5 * var5;

		if (var7 < 1.0D || var7 > 3600.0D) {
			waypointX = posX + (rand.nextFloat() * 2.0F - 1.0F) * 16.0F;
			waypointY = posY + (rand.nextFloat() * 2.0F - 1.0F) * 16.0F;
			waypointZ = posZ + (rand.nextFloat() * 2.0F - 1.0F) * 16.0F;
		}

		if (courseChangeCooldown-- <= 0) {
			courseChangeCooldown += rand.nextInt(5) + 2;
			var7 = MathHelper.sqrt_double(var7);

			if (isCourseTraversable(waypointX, waypointY, waypointZ, var7)) {
				motionX += var1 / var7 * 0.1D;
				motionY += var3 / var7 * 0.1D;
				motionZ += var5 / var7 * 0.1D;
			} else {
				waypointX = posX;
				waypointY = posY;
				waypointZ = posZ;
			}
		}

		if (targetedEntity != null && targetedEntity.isDead) {
			targetedEntity = null;
		}

		if (targetedEntity == null || aggroCooldown-- <= 0) {
			targetedEntity = worldObj.getClosestVulnerablePlayerToEntity(this,
					100.0D);

			if (targetedEntity != null) {
				aggroCooldown = 20;
			}
		}

		final double var9 = 64.0D;

		if (targetedEntity != null
				&& targetedEntity.getDistanceSqToEntity(this) < var9 * var9) {
			final double var11 = targetedEntity.posX - posX;
			final double var13 = targetedEntity.boundingBox.minY
					+ targetedEntity.height / 2.0F - (posY + height / 2.0F);
			final double var15 = targetedEntity.posZ - posZ;
			renderYawOffset = rotationYaw = -((float) Math.atan2(var11, var15))
					* 180.0F / (float) Math.PI;

			if (canEntityBeSeen(targetedEntity)) {
				if (attackCounter == 10) {
					worldObj.playAuxSFXAtEntity((EntityPlayer) null, 1007,
							(int) posX, (int) posY, (int) posZ, 0);
				}

				++attackCounter;

				if (attackCounter == 20) {
					worldObj.playAuxSFXAtEntity((EntityPlayer) null, 1008,
							(int) posX, (int) posY, (int) posZ, 0);
					final EntityLargeFireball var17 = new EntityLargeFireball(
							worldObj, this, var11, var13, var15);
					var17.field_92057_e = explosionStrength;
					final double var18 = 4.0D;
					final Vec3 var20 = getLook(1.0F);
					var17.posX = posX + var20.xCoord * var18;
					var17.posY = posY + height / 2.0F + 0.5D;
					var17.posZ = posZ + var20.zCoord * var18;
					worldObj.spawnEntityInWorld(var17);
					attackCounter = -40;
				}
			} else if (attackCounter > 0) {
				--attackCounter;
			}
		} else {
			renderYawOffset = rotationYaw = -((float) Math.atan2(motionX,
					motionZ)) * 180.0F / (float) Math.PI;

			if (attackCounter > 0) {
				--attackCounter;
			}
		}

		if (!worldObj.isRemote) {
			final byte var21 = dataWatcher.getWatchableObjectByte(16);
			final byte var12 = (byte) (attackCounter > 10 ? 1 : 0);

			if (var21 != var12) {
				dataWatcher.updateObject(16, Byte.valueOf(var12));
			}
		}
	}

	/**
	 * True if the ghast has an unobstructed line of travel to the waypoint.
	 */
	private boolean isCourseTraversable(final double par1, final double par3,
			final double par5, final double par7) {
		final double var9 = (waypointX - posX) / par7;
		final double var11 = (waypointY - posY) / par7;
		final double var13 = (waypointZ - posZ) / par7;
		final AxisAlignedBB var15 = boundingBox.copy();

		for (int var16 = 1; var16 < par7; ++var16) {
			var15.offset(var9, var11, var13);

			if (!worldObj.getCollidingBoundingBoxes(this, var15).isEmpty()) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Returns the sound this mob makes while it's alive.
	 */
	@Override
	protected String getLivingSound() {
		return "mob.ghast.moan";
	}

	/**
	 * Returns the sound this mob makes when it is hurt.
	 */
	@Override
	protected String getHurtSound() {
		return "mob.ghast.scream";
	}

	/**
	 * Returns the sound this mob makes on death.
	 */
	@Override
	protected String getDeathSound() {
		return "mob.ghast.death";
	}

	/**
	 * Returns the item ID for the item the mob drops on death.
	 */
	@Override
	protected int getDropItemId() {
		return Item.gunpowder.itemID;
	}

	/**
	 * Drop 0-2 items of this living's type. @param par1 - Whether this entity
	 * has recently been hit by a player. @param par2 - Level of Looting used to
	 * kill this mob.
	 */
	@Override
	protected void dropFewItems(final boolean par1, final int par2) {
		int var3 = rand.nextInt(2) + rand.nextInt(1 + par2);
		int var4;

		for (var4 = 0; var4 < var3; ++var4) {
			dropItem(Item.ghastTear.itemID, 1);
		}

		var3 = rand.nextInt(3) + rand.nextInt(1 + par2);

		for (var4 = 0; var4 < var3; ++var4) {
			dropItem(Item.gunpowder.itemID, 1);
		}
	}

	/**
	 * Returns the volume for the sounds this mob makes.
	 */
	@Override
	protected float getSoundVolume() {
		return 10.0F;
	}

	/**
	 * Checks if the entity's current position is a valid location to spawn this
	 * entity.
	 */
	@Override
	public boolean getCanSpawnHere() {
		return rand.nextInt(20) == 0 && super.getCanSpawnHere()
				&& worldObj.difficultySetting > 0;
	}

	/**
	 * Will return how many at most can spawn in a chunk at once.
	 */
	@Override
	public int getMaxSpawnedInChunk() {
		return 1;
	}

	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	@Override
	public void writeEntityToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeEntityToNBT(par1NBTTagCompound);
		par1NBTTagCompound.setInteger("ExplosionPower", explosionStrength);
	}

	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	@Override
	public void readEntityFromNBT(final NBTTagCompound par1NBTTagCompound) {
		super.readEntityFromNBT(par1NBTTagCompound);

		if (par1NBTTagCompound.hasKey("ExplosionPower")) {
			explosionStrength = par1NBTTagCompound.getInteger("ExplosionPower");
		}
	}
}
