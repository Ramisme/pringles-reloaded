package net.minecraft.src;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertyManager {
	private final Properties properties = new Properties();

	/** Reference to the logger. */
	private final ILogAgent logger;
	private final File associatedFile;

	public PropertyManager(final File par1File, final ILogAgent par2ILogAgent) {
		associatedFile = par1File;
		logger = par2ILogAgent;

		if (par1File.exists()) {
			FileInputStream var3 = null;

			try {
				var3 = new FileInputStream(par1File);
				properties.load(var3);
			} catch (final Exception var13) {
				par2ILogAgent.logWarningException("Failed to load " + par1File,
						var13);
				logMessageAndSave();
			} finally {
				if (var3 != null) {
					try {
						var3.close();
					} catch (final IOException var12) {
						;
					}
				}
			}
		} else {
			par2ILogAgent.logWarning(par1File + " does not exist");
			logMessageAndSave();
		}
	}

	/**
	 * logs an info message then calls saveSettingsToFile Yes this appears to be
	 * a potential stack overflow - these 2 functions call each other repeatdly
	 * if an exception occurs.
	 */
	public void logMessageAndSave() {
		logger.logInfo("Generating new properties file");
		saveProperties();
	}

	/**
	 * Writes the properties to the properties file.
	 */
	public void saveProperties() {
		FileOutputStream var1 = null;

		try {
			var1 = new FileOutputStream(associatedFile);
			properties.store(var1, "Minecraft server properties");
		} catch (final Exception var11) {
			logger.logWarningException("Failed to save " + associatedFile,
					var11);
			logMessageAndSave();
		} finally {
			if (var1 != null) {
				try {
					var1.close();
				} catch (final IOException var10) {
					;
				}
			}
		}
	}

	/**
	 * Returns this PropertyManager's file object used for property saving.
	 */
	public File getPropertiesFile() {
		return associatedFile;
	}

	/**
	 * Gets a property. If it does not exist, set it to the specified value.
	 */
	public String getProperty(final String par1Str, final String par2Str) {
		if (!properties.containsKey(par1Str)) {
			properties.setProperty(par1Str, par2Str);
			saveProperties();
		}

		return properties.getProperty(par1Str, par2Str);
	}

	/**
	 * Gets an integer property. If it does not exist, set it to the specified
	 * value.
	 */
	public int getIntProperty(final String par1Str, final int par2) {
		try {
			return Integer.parseInt(getProperty(par1Str, "" + par2));
		} catch (final Exception var4) {
			properties.setProperty(par1Str, "" + par2);
			return par2;
		}
	}

	/**
	 * Gets a boolean property. If it does not exist, set it to the specified
	 * value.
	 */
	public boolean getBooleanProperty(final String par1Str, final boolean par2) {
		try {
			return Boolean.parseBoolean(getProperty(par1Str, "" + par2));
		} catch (final Exception var4) {
			properties.setProperty(par1Str, "" + par2);
			return par2;
		}
	}

	/**
	 * Saves an Object with the given property name.
	 */
	public void setProperty(final String par1Str, final Object par2Obj) {
		properties.setProperty(par1Str, "" + par2Obj);
	}
}
