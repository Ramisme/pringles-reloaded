package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet8UpdateHealth extends Packet {
	/** Variable used for incoming health packets */
	public int healthMP;
	public int food;

	/**
	 * Players logging on get a saturation of 5.0. Eating food increases the
	 * saturation as well as the food bar.
	 */
	public float foodSaturation;

	public Packet8UpdateHealth() {
	}

	public Packet8UpdateHealth(final int par1, final int par2, final float par3) {
		healthMP = par1;
		food = par2;
		foodSaturation = par3;
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		healthMP = par1DataInputStream.readShort();
		food = par1DataInputStream.readShort();
		foodSaturation = par1DataInputStream.readFloat();
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		par1DataOutputStream.writeShort(healthMP);
		par1DataOutputStream.writeShort(food);
		par1DataOutputStream.writeFloat(foodSaturation);
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleUpdateHealth(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 8;
	}

	/**
	 * only false for the abstract Packet class, all real packets return true
	 */
	@Override
	public boolean isRealPacket() {
		return true;
	}

	/**
	 * eg return packet30entity.entityId == entityId; WARNING : will throw if
	 * you compare a packet to a different packet class
	 */
	@Override
	public boolean containsSameEntityIDAs(final Packet par1Packet) {
		return true;
	}
}
