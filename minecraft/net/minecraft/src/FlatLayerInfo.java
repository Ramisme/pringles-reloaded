package net.minecraft.src;

public class FlatLayerInfo {
	/** Amount of layers for this set of layers. */
	private int layerCount;

	/** Block type used on this set of layers. */
	private int layerFillBlock;

	/** Block metadata used on this set of laeyrs. */
	private int layerFillBlockMeta;
	private int layerMinimumY;

	public FlatLayerInfo(final int par1, final int par2) {
		layerCount = 1;
		layerFillBlock = 0;
		layerFillBlockMeta = 0;
		layerMinimumY = 0;
		layerCount = par1;
		layerFillBlock = par2;
	}

	public FlatLayerInfo(final int par1, final int par2, final int par3) {
		this(par1, par2);
		layerFillBlockMeta = par3;
	}

	/**
	 * Return the amount of layers for this set of layers.
	 */
	public int getLayerCount() {
		return layerCount;
	}

	/**
	 * Return the block type used on this set of layers.
	 */
	public int getFillBlock() {
		return layerFillBlock;
	}

	/**
	 * Return the block metadata used on this set of layers.
	 */
	public int getFillBlockMeta() {
		return layerFillBlockMeta;
	}

	/**
	 * Return the minimum Y coordinate for this layer, set during generation.
	 */
	public int getMinY() {
		return layerMinimumY;
	}

	/**
	 * Set the minimum Y coordinate for this layer.
	 */
	public void setMinY(final int par1) {
		layerMinimumY = par1;
	}

	@Override
	public String toString() {
		String var1 = Integer.toString(layerFillBlock);

		if (layerCount > 1) {
			var1 = layerCount + "x" + var1;
		}

		if (layerFillBlockMeta > 0) {
			var1 = var1 + ":" + layerFillBlockMeta;
		}

		return var1;
	}
}
