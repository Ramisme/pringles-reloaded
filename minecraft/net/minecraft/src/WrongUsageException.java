package net.minecraft.src;

public class WrongUsageException extends SyntaxErrorException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3027980784379517044L;

	public WrongUsageException(final String par1Str,
			final Object... par2ArrayOfObj) {
		super(par1Str, par2ArrayOfObj);
	}
}
