package net.minecraft.src;

public class BlockFenceGate extends BlockDirectional {
	public BlockFenceGate(final int par1) {
		super(par1, Material.wood);
		setCreativeTab(CreativeTabs.tabRedstone);
	}

	/**
	 * From the specified side and block metadata retrieves the blocks texture.
	 * Args: side, metadata
	 */
	@Override
	public Icon getIcon(final int par1, final int par2) {
		return Block.planks.getBlockTextureFromSide(par1);
	}

	/**
	 * Checks to see if its valid to put this block at the specified
	 * coordinates. Args: world, x, y, z
	 */
	@Override
	public boolean canPlaceBlockAt(final World par1World, final int par2,
			final int par3, final int par4) {
		return !par1World.getBlockMaterial(par2, par3 - 1, par4).isSolid() ? false
				: super.canPlaceBlockAt(par1World, par2, par3, par4);
	}

	/**
	 * Returns a bounding box from the pool of bounding boxes (this means this
	 * box can change after the pool has been cleared to be reused)
	 */
	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(final World par1World,
			final int par2, final int par3, final int par4) {
		final int var5 = par1World.getBlockMetadata(par2, par3, par4);
		return BlockFenceGate.isFenceGateOpen(var5) ? null : var5 != 2
				&& var5 != 0 ? AxisAlignedBB.getAABBPool()
				.getAABB(par2 + 0.375F, par3, par4, par2 + 0.625F, par3 + 1.5F,
						par4 + 1) : AxisAlignedBB.getAABBPool().getAABB(par2,
				par3, par4 + 0.375F, par2 + 1, par3 + 1.5F, par4 + 0.625F);
	}

	/**
	 * Updates the blocks bounds based on its current state. Args: world, x, y,
	 * z
	 */
	@Override
	public void setBlockBoundsBasedOnState(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		final int var5 = BlockDirectional.getDirection(par1IBlockAccess
				.getBlockMetadata(par2, par3, par4));

		if (var5 != 2 && var5 != 0) {
			setBlockBounds(0.375F, 0.0F, 0.0F, 0.625F, 1.0F, 1.0F);
		} else {
			setBlockBounds(0.0F, 0.0F, 0.375F, 1.0F, 1.0F, 0.625F);
		}
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	@Override
	public boolean getBlocksMovement(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		return BlockFenceGate.isFenceGateOpen(par1IBlockAccess
				.getBlockMetadata(par2, par3, par4));
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return 21;
	}

	/**
	 * Called when the block is placed in the world.
	 */
	@Override
	public void onBlockPlacedBy(final World par1World, final int par2,
			final int par3, final int par4,
			final EntityLiving par5EntityLiving, final ItemStack par6ItemStack) {
		final int var7 = (MathHelper
				.floor_double(par5EntityLiving.rotationYaw * 4.0F / 360.0F + 0.5D) & 3) % 4;
		par1World.setBlockMetadataWithNotify(par2, par3, par4, var7, 2);
	}

	/**
	 * Called upon block activation (right click on the block.)
	 */
	@Override
	public boolean onBlockActivated(final World par1World, final int par2,
			final int par3, final int par4,
			final EntityPlayer par5EntityPlayer, final int par6,
			final float par7, final float par8, final float par9) {
		int var10 = par1World.getBlockMetadata(par2, par3, par4);

		if (BlockFenceGate.isFenceGateOpen(var10)) {
			par1World.setBlockMetadataWithNotify(par2, par3, par4, var10 & -5,
					2);
		} else {
			final int var11 = (MathHelper
					.floor_double(par5EntityPlayer.rotationYaw * 4.0F / 360.0F + 0.5D) & 3) % 4;
			final int var12 = BlockDirectional.getDirection(var10);

			if (var12 == (var11 + 2) % 4) {
				var10 = var11;
			}

			par1World
					.setBlockMetadataWithNotify(par2, par3, par4, var10 | 4, 2);
		}

		par1World.playAuxSFXAtEntity(par5EntityPlayer, 1003, par2, par3, par4,
				0);
		return true;
	}

	/**
	 * Lets the block know when one of its neighbor changes. Doesn't know which
	 * neighbor changed (coordinates passed are their own) Args: x, y, z,
	 * neighbor blockID
	 */
	@Override
	public void onNeighborBlockChange(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		if (!par1World.isRemote) {
			final int var6 = par1World.getBlockMetadata(par2, par3, par4);
			final boolean var7 = par1World.isBlockIndirectlyGettingPowered(
					par2, par3, par4);

			if (var7 || par5 > 0 && Block.blocksList[par5].canProvidePower()) {
				if (var7 && !BlockFenceGate.isFenceGateOpen(var6)) {
					par1World.setBlockMetadataWithNotify(par2, par3, par4,
							var6 | 4, 2);
					par1World.playAuxSFXAtEntity((EntityPlayer) null, 1003,
							par2, par3, par4, 0);
				} else if (!var7 && BlockFenceGate.isFenceGateOpen(var6)) {
					par1World.setBlockMetadataWithNotify(par2, par3, par4, var6
							& -5, 2);
					par1World.playAuxSFXAtEntity((EntityPlayer) null, 1003,
							par2, par3, par4, 0);
				}
			}
		}
	}

	/**
	 * Returns if the fence gate is open according to its metadata.
	 */
	public static boolean isFenceGateOpen(final int par0) {
		return (par0 & 4) != 0;
	}

	/**
	 * Returns true if the given side of this block type should be rendered, if
	 * the adjacent block is at the given coordinates. Args: blockAccess, x, y,
	 * z, side
	 */
	@Override
	public boolean shouldSideBeRendered(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		return true;
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
	}
}
