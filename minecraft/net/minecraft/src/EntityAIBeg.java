package net.minecraft.src;

public class EntityAIBeg extends EntityAIBase {
	private final EntityWolf theWolf;
	private EntityPlayer thePlayer;
	private final World worldObject;
	private final float minPlayerDistance;
	private int field_75384_e;

	public EntityAIBeg(final EntityWolf par1EntityWolf, final float par2) {
		theWolf = par1EntityWolf;
		worldObject = par1EntityWolf.worldObj;
		minPlayerDistance = par2;
		setMutexBits(2);
	}

	/**
	 * Returns whether the EntityAIBase should begin execution.
	 */
	@Override
	public boolean shouldExecute() {
		thePlayer = worldObject.getClosestPlayerToEntity(theWolf,
				minPlayerDistance);
		return thePlayer == null ? false : hasPlayerGotBoneInHand(thePlayer);
	}

	/**
	 * Returns whether an in-progress EntityAIBase should continue executing
	 */
	@Override
	public boolean continueExecuting() {
		return !thePlayer.isEntityAlive() ? false : theWolf
				.getDistanceSqToEntity(thePlayer) > minPlayerDistance
				* minPlayerDistance ? false : field_75384_e > 0
				&& hasPlayerGotBoneInHand(thePlayer);
	}

	/**
	 * Execute a one shot task or start executing a continuous task
	 */
	@Override
	public void startExecuting() {
		theWolf.func_70918_i(true);
		field_75384_e = 40 + theWolf.getRNG().nextInt(40);
	}

	/**
	 * Resets the task
	 */
	@Override
	public void resetTask() {
		theWolf.func_70918_i(false);
		thePlayer = null;
	}

	/**
	 * Updates the task
	 */
	@Override
	public void updateTask() {
		theWolf.getLookHelper().setLookPosition(thePlayer.posX,
				thePlayer.posY + thePlayer.getEyeHeight(), thePlayer.posZ,
				10.0F, theWolf.getVerticalFaceSpeed());
		--field_75384_e;
	}

	/**
	 * Gets if the Player has the Bone in the hand.
	 */
	private boolean hasPlayerGotBoneInHand(final EntityPlayer par1EntityPlayer) {
		final ItemStack var2 = par1EntityPlayer.inventory.getCurrentItem();
		return var2 == null ? false : !theWolf.isTamed()
				&& var2.itemID == Item.bone.itemID ? true : theWolf
				.isBreedingItem(var2);
	}
}
