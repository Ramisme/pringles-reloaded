package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.List;

public class Packet20NamedEntitySpawn extends Packet {
	/** The entity ID, in this case it's the player ID. */
	public int entityId;

	/** The player's name. */
	public String name;

	/** The player's X position. */
	public int xPosition;

	/** The player's Y position. */
	public int yPosition;

	/** The player's Z position. */
	public int zPosition;

	/** The player's rotation. */
	public byte rotation;

	/** The player's pitch. */
	public byte pitch;

	/** The current item the player is holding. */
	public int currentItem;
	private DataWatcher metadata;
	private List metadataWatchableObjects;

	public Packet20NamedEntitySpawn() {
	}

	public Packet20NamedEntitySpawn(final EntityPlayer par1EntityPlayer) {
		entityId = par1EntityPlayer.entityId;
		name = par1EntityPlayer.username;
		xPosition = MathHelper.floor_double(par1EntityPlayer.posX * 32.0D);
		yPosition = MathHelper.floor_double(par1EntityPlayer.posY * 32.0D);
		zPosition = MathHelper.floor_double(par1EntityPlayer.posZ * 32.0D);
		rotation = (byte) (int) (par1EntityPlayer.rotationYaw * 256.0F / 360.0F);
		pitch = (byte) (int) (par1EntityPlayer.rotationPitch * 256.0F / 360.0F);
		final ItemStack var2 = par1EntityPlayer.inventory.getCurrentItem();
		currentItem = var2 == null ? 0 : var2.itemID;
		metadata = par1EntityPlayer.getDataWatcher();
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		entityId = par1DataInputStream.readInt();
		name = Packet.readString(par1DataInputStream, 16);
		xPosition = par1DataInputStream.readInt();
		yPosition = par1DataInputStream.readInt();
		zPosition = par1DataInputStream.readInt();
		rotation = par1DataInputStream.readByte();
		pitch = par1DataInputStream.readByte();
		currentItem = par1DataInputStream.readShort();
		metadataWatchableObjects = DataWatcher
				.readWatchableObjects(par1DataInputStream);
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		par1DataOutputStream.writeInt(entityId);
		Packet.writeString(name, par1DataOutputStream);
		par1DataOutputStream.writeInt(xPosition);
		par1DataOutputStream.writeInt(yPosition);
		par1DataOutputStream.writeInt(zPosition);
		par1DataOutputStream.writeByte(rotation);
		par1DataOutputStream.writeByte(pitch);
		par1DataOutputStream.writeShort(currentItem);
		metadata.writeWatchableObjects(par1DataOutputStream);
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleNamedEntitySpawn(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 28;
	}

	public List getWatchedMetadata() {
		if (metadataWatchableObjects == null) {
			metadataWatchableObjects = metadata.getAllWatched();
		}

		return metadataWatchableObjects;
	}
}
