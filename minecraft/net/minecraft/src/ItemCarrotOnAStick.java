package net.minecraft.src;

public class ItemCarrotOnAStick extends Item {
	public ItemCarrotOnAStick(final int par1) {
		super(par1);
		setCreativeTab(CreativeTabs.tabTransport);
		setMaxStackSize(1);
		setMaxDamage(25);
	}

	/**
	 * Returns True is the item is renderer in full 3D when hold.
	 */
	@Override
	public boolean isFull3D() {
		return true;
	}

	/**
	 * Returns true if this item should be rotated by 180 degrees around the Y
	 * axis when being held in an entities hands.
	 */
	@Override
	public boolean shouldRotateAroundWhenRendering() {
		return true;
	}

	/**
	 * Called whenever this item is equipped and the right mouse button is
	 * pressed. Args: itemStack, world, entityPlayer
	 */
	@Override
	public ItemStack onItemRightClick(final ItemStack par1ItemStack,
			final World par2World, final EntityPlayer par3EntityPlayer) {
		if (par3EntityPlayer.isRiding()
				&& par3EntityPlayer.ridingEntity instanceof EntityPig) {
			final EntityPig var4 = (EntityPig) par3EntityPlayer.ridingEntity;

			if (var4.getAIControlledByPlayer().isControlledByPlayer()
					&& par1ItemStack.getMaxDamage()
							- par1ItemStack.getItemDamage() >= 7) {
				var4.getAIControlledByPlayer().boostSpeed();
				par1ItemStack.damageItem(7, par3EntityPlayer);

				if (par1ItemStack.stackSize == 0) {
					final ItemStack var5 = new ItemStack(Item.fishingRod);
					var5.setTagCompound(par1ItemStack.stackTagCompound);
					return var5;
				}
			}
		}

		return par1ItemStack;
	}
}
