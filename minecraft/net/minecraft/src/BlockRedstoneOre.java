package net.minecraft.src;

import java.util.Random;

public class BlockRedstoneOre extends Block {
	private final boolean glowing;

	public BlockRedstoneOre(final int par1, final boolean par2) {
		super(par1, Material.rock);

		if (par2) {
			setTickRandomly(true);
		}

		glowing = par2;
	}

	/**
	 * How many world ticks before ticking
	 */
	@Override
	public int tickRate(final World par1World) {
		return 30;
	}

	/**
	 * Called when the block is clicked by a player. Args: x, y, z, entityPlayer
	 */
	@Override
	public void onBlockClicked(final World par1World, final int par2,
			final int par3, final int par4, final EntityPlayer par5EntityPlayer) {
		glow(par1World, par2, par3, par4);
		super.onBlockClicked(par1World, par2, par3, par4, par5EntityPlayer);
	}

	/**
	 * Called whenever an entity is walking on top of this block. Args: world,
	 * x, y, z, entity
	 */
	@Override
	public void onEntityWalking(final World par1World, final int par2,
			final int par3, final int par4, final Entity par5Entity) {
		glow(par1World, par2, par3, par4);
		super.onEntityWalking(par1World, par2, par3, par4, par5Entity);
	}

	/**
	 * Called upon block activation (right click on the block.)
	 */
	@Override
	public boolean onBlockActivated(final World par1World, final int par2,
			final int par3, final int par4,
			final EntityPlayer par5EntityPlayer, final int par6,
			final float par7, final float par8, final float par9) {
		glow(par1World, par2, par3, par4);
		return super.onBlockActivated(par1World, par2, par3, par4,
				par5EntityPlayer, par6, par7, par8, par9);
	}

	/**
	 * The redstone ore glows.
	 */
	private void glow(final World par1World, final int par2, final int par3,
			final int par4) {
		sparkle(par1World, par2, par3, par4);

		if (blockID == Block.oreRedstone.blockID) {
			par1World.setBlock(par2, par3, par4,
					Block.oreRedstoneGlowing.blockID);
		}
	}

	/**
	 * Ticks the block if it's been scheduled
	 */
	@Override
	public void updateTick(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		if (blockID == Block.oreRedstoneGlowing.blockID) {
			par1World.setBlock(par2, par3, par4, Block.oreRedstone.blockID);
		}
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return Item.redstone.itemID;
	}

	/**
	 * Returns the usual quantity dropped by the block plus a bonus of 1 to 'i'
	 * (inclusive).
	 */
	@Override
	public int quantityDroppedWithBonus(final int par1, final Random par2Random) {
		return quantityDropped(par2Random) + par2Random.nextInt(par1 + 1);
	}

	/**
	 * Returns the quantity of items to drop on block destruction.
	 */
	@Override
	public int quantityDropped(final Random par1Random) {
		return 4 + par1Random.nextInt(2);
	}

	/**
	 * Drops the block items with a specified chance of dropping the specified
	 * items
	 */
	@Override
	public void dropBlockAsItemWithChance(final World par1World,
			final int par2, final int par3, final int par4, final int par5,
			final float par6, final int par7) {
		super.dropBlockAsItemWithChance(par1World, par2, par3, par4, par5,
				par6, par7);

		if (idDropped(par5, par1World.rand, par7) != blockID) {
			final int var8 = 1 + par1World.rand.nextInt(5);
			dropXpOnBlockBreak(par1World, par2, par3, par4, var8);
		}
	}

	/**
	 * A randomly called display update to be able to add particles or other
	 * items for display
	 */
	@Override
	public void randomDisplayTick(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		if (glowing) {
			sparkle(par1World, par2, par3, par4);
		}
	}

	/**
	 * The redstone ore sparkles.
	 */
	private void sparkle(final World par1World, final int par2, final int par3,
			final int par4) {
		final Random var5 = par1World.rand;
		final double var6 = 0.0625D;

		for (int var8 = 0; var8 < 6; ++var8) {
			double var9 = par2 + var5.nextFloat();
			double var11 = par3 + var5.nextFloat();
			double var13 = par4 + var5.nextFloat();

			if (var8 == 0 && !par1World.isBlockOpaqueCube(par2, par3 + 1, par4)) {
				var11 = par3 + 1 + var6;
			}

			if (var8 == 1 && !par1World.isBlockOpaqueCube(par2, par3 - 1, par4)) {
				var11 = par3 + 0 - var6;
			}

			if (var8 == 2 && !par1World.isBlockOpaqueCube(par2, par3, par4 + 1)) {
				var13 = par4 + 1 + var6;
			}

			if (var8 == 3 && !par1World.isBlockOpaqueCube(par2, par3, par4 - 1)) {
				var13 = par4 + 0 - var6;
			}

			if (var8 == 4 && !par1World.isBlockOpaqueCube(par2 + 1, par3, par4)) {
				var9 = par2 + 1 + var6;
			}

			if (var8 == 5 && !par1World.isBlockOpaqueCube(par2 - 1, par3, par4)) {
				var9 = par2 + 0 - var6;
			}

			if (var9 < par2 || var9 > par2 + 1 || var11 < 0.0D
					|| var11 > par3 + 1 || var13 < par4 || var13 > par4 + 1) {
				par1World.spawnParticle("reddust", var9, var11, var13, 0.0D,
						0.0D, 0.0D);
			}
		}
	}

	/**
	 * Returns an item stack containing a single instance of the current block
	 * type. 'i' is the block's subtype/damage and is ignored for blocks which
	 * do not support subtypes. Blocks which cannot be harvested should return
	 * null.
	 */
	@Override
	protected ItemStack createStackedBlock(final int par1) {
		return new ItemStack(Block.oreRedstone);
	}
}
