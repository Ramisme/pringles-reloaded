package net.minecraft.src;

public class BlockSoulSand extends Block {
	public BlockSoulSand(final int par1) {
		super(par1, Material.sand);
		setCreativeTab(CreativeTabs.tabBlock);
	}

	/**
	 * Returns a bounding box from the pool of bounding boxes (this means this
	 * box can change after the pool has been cleared to be reused)
	 */
	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(final World par1World,
			final int par2, final int par3, final int par4) {
		final float var5 = 0.125F;
		return AxisAlignedBB.getAABBPool().getAABB(par2, par3, par4, par2 + 1,
				par3 + 1 - var5, par4 + 1);
	}

	/**
	 * Triggered whenever an entity collides with this block (enters into the
	 * block). Args: world, x, y, z, entity
	 */
	@Override
	public void onEntityCollidedWithBlock(final World par1World,
			final int par2, final int par3, final int par4,
			final Entity par5Entity) {
		par5Entity.motionX *= 0.4D;
		par5Entity.motionZ *= 0.4D;
	}
}
