package net.minecraft.src;

public class ItemBlockWithMetadata extends ItemBlock {
	private final Block theBlock;

	public ItemBlockWithMetadata(final int par1, final Block par2Block) {
		super(par1);
		theBlock = par2Block;
		setMaxDamage(0);
		setHasSubtypes(true);
	}

	/**
	 * Gets an icon index based on an item's damage value
	 */
	@Override
	public Icon getIconFromDamage(final int par1) {
		return theBlock.getIcon(2, par1);
	}

	/**
	 * Returns the metadata of the block which this Item (ItemBlock) can place
	 */
	@Override
	public int getMetadata(final int par1) {
		return par1;
	}
}
