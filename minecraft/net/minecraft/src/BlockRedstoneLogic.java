package net.minecraft.src;

import java.util.Random;

public abstract class BlockRedstoneLogic extends BlockDirectional {
	/** Tells whether the repeater is powered or not */
	protected final boolean isRepeaterPowered;

	protected BlockRedstoneLogic(final int par1, final boolean par2) {
		super(par1, Material.circuits);
		isRepeaterPowered = par2;
		setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.125F, 1.0F);
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	/**
	 * Checks to see if its valid to put this block at the specified
	 * coordinates. Args: world, x, y, z
	 */
	@Override
	public boolean canPlaceBlockAt(final World par1World, final int par2,
			final int par3, final int par4) {
		return !par1World.doesBlockHaveSolidTopSurface(par2, par3 - 1, par4) ? false
				: super.canPlaceBlockAt(par1World, par2, par3, par4);
	}

	/**
	 * Can this block stay at this position. Similar to canPlaceBlockAt except
	 * gets checked often with plants.
	 */
	@Override
	public boolean canBlockStay(final World par1World, final int par2,
			final int par3, final int par4) {
		return !par1World.doesBlockHaveSolidTopSurface(par2, par3 - 1, par4) ? false
				: super.canBlockStay(par1World, par2, par3, par4);
	}

	/**
	 * Ticks the block if it's been scheduled
	 */
	@Override
	public void updateTick(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		final int var6 = par1World.getBlockMetadata(par2, par3, par4);

		if (!func_94476_e(par1World, par2, par3, par4, var6)) {
			final boolean var7 = func_94478_d(par1World, par2, par3, par4, var6);

			if (isRepeaterPowered && !var7) {
				par1World.setBlock(par2, par3, par4, func_94484_i().blockID,
						var6, 2);
			} else if (!isRepeaterPowered) {
				par1World.setBlock(par2, par3, par4, func_94485_e().blockID,
						var6, 2);

				if (!var7) {
					par1World.func_82740_a(par2, par3, par4,
							func_94485_e().blockID, func_94486_g(var6), -1);
				}
			}
		}
	}

	/**
	 * From the specified side and block metadata retrieves the blocks texture.
	 * Args: side, metadata
	 */
	@Override
	public Icon getIcon(final int par1, final int par2) {
		return par1 == 0 ? isRepeaterPowered ? Block.torchRedstoneActive
				.getBlockTextureFromSide(par1) : Block.torchRedstoneIdle
				.getBlockTextureFromSide(par1) : par1 == 1 ? blockIcon
				: Block.stoneDoubleSlab.getBlockTextureFromSide(1);
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		blockIcon = par1IconRegister
				.registerIcon(isRepeaterPowered ? "repeater_lit" : "repeater");
	}

	/**
	 * Returns true if the given side of this block type should be rendered, if
	 * the adjacent block is at the given coordinates. Args: blockAccess, x, y,
	 * z, side
	 */
	@Override
	public boolean shouldSideBeRendered(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		return par5 != 0 && par5 != 1;
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return 36;
	}

	protected boolean func_96470_c(final int par1) {
		return isRepeaterPowered;
	}

	/**
	 * Returns true if the block is emitting direct/strong redstone power on the
	 * specified side. Args: World, X, Y, Z, side. Note that the side is
	 * reversed - eg it is 1 (up) when checking the bottom of the block.
	 */
	@Override
	public int isProvidingStrongPower(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		return isProvidingWeakPower(par1IBlockAccess, par2, par3, par4, par5);
	}

	/**
	 * Returns true if the block is emitting indirect/weak redstone power on the
	 * specified side. If isBlockNormalCube returns true, standard redstone
	 * propagation rules will apply instead and this will not be called. Args:
	 * World, X, Y, Z, side. Note that the side is reversed - eg it is 1 (up)
	 * when checking the bottom of the block.
	 */
	@Override
	public int isProvidingWeakPower(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		final int var6 = par1IBlockAccess.getBlockMetadata(par2, par3, par4);

		if (!func_96470_c(var6)) {
			return 0;
		} else {
			final int var7 = BlockDirectional.getDirection(var6);
			return var7 == 0 && par5 == 3 ? func_94480_d(par1IBlockAccess,
					par2, par3, par4, var6)
					: var7 == 1 && par5 == 4 ? func_94480_d(par1IBlockAccess,
							par2, par3, par4, var6)
							: var7 == 2 && par5 == 2 ? func_94480_d(
									par1IBlockAccess, par2, par3, par4, var6)
									: var7 == 3 && par5 == 5 ? func_94480_d(
											par1IBlockAccess, par2, par3, par4,
											var6) : 0;
		}
	}

	/**
	 * Lets the block know when one of its neighbor changes. Doesn't know which
	 * neighbor changed (coordinates passed are their own) Args: x, y, z,
	 * neighbor blockID
	 */
	@Override
	public void onNeighborBlockChange(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		if (!canBlockStay(par1World, par2, par3, par4)) {
			dropBlockAsItem(par1World, par2, par3, par4,
					par1World.getBlockMetadata(par2, par3, par4), 0);
			par1World.setBlockToAir(par2, par3, par4);
			par1World.notifyBlocksOfNeighborChange(par2 + 1, par3, par4,
					blockID);
			par1World.notifyBlocksOfNeighborChange(par2 - 1, par3, par4,
					blockID);
			par1World.notifyBlocksOfNeighborChange(par2, par3, par4 + 1,
					blockID);
			par1World.notifyBlocksOfNeighborChange(par2, par3, par4 - 1,
					blockID);
			par1World.notifyBlocksOfNeighborChange(par2, par3 - 1, par4,
					blockID);
			par1World.notifyBlocksOfNeighborChange(par2, par3 + 1, par4,
					blockID);
		} else {
			func_94479_f(par1World, par2, par3, par4, par5);
		}
	}

	protected void func_94479_f(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		final int var6 = par1World.getBlockMetadata(par2, par3, par4);

		if (!func_94476_e(par1World, par2, par3, par4, var6)) {
			final boolean var7 = func_94478_d(par1World, par2, par3, par4, var6);

			if ((isRepeaterPowered && !var7 || !isRepeaterPowered && var7)
					&& !par1World.isBlockTickScheduled(par2, par3, par4,
							blockID)) {
				byte var8 = -1;

				if (func_83011_d(par1World, par2, par3, par4, var6)) {
					var8 = -3;
				} else if (isRepeaterPowered) {
					var8 = -2;
				}

				par1World.func_82740_a(par2, par3, par4, blockID,
						func_94481_j_(var6), var8);
			}
		}
	}

	public boolean func_94476_e(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		return false;
	}

	protected boolean func_94478_d(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		return getInputStrength(par1World, par2, par3, par4, par5) > 0;
	}

	/**
	 * Returns the signal strength at one input of the block. Args: world, X, Y,
	 * Z, side
	 */
	protected int getInputStrength(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		final int var6 = BlockDirectional.getDirection(par5);
		final int var7 = par2 + Direction.offsetX[var6];
		final int var8 = par4 + Direction.offsetZ[var6];
		final int var9 = par1World.getIndirectPowerLevelTo(var7, par3, var8,
				Direction.directionToFacing[var6]);
		return var9 >= 15 ? var9
				: Math.max(
						var9,
						par1World.getBlockId(var7, par3, var8) == Block.redstoneWire.blockID ? par1World
								.getBlockMetadata(var7, par3, var8) : 0);
	}

	protected int func_94482_f(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		final int var6 = BlockDirectional.getDirection(par5);

		switch (var6) {
		case 0:
		case 2:
			return Math.max(
					func_94488_g(par1IBlockAccess, par2 - 1, par3, par4, 4),
					func_94488_g(par1IBlockAccess, par2 + 1, par3, par4, 5));

		case 1:
		case 3:
			return Math.max(
					func_94488_g(par1IBlockAccess, par2, par3, par4 + 1, 3),
					func_94488_g(par1IBlockAccess, par2, par3, par4 - 1, 2));

		default:
			return 0;
		}
	}

	protected int func_94488_g(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		final int var6 = par1IBlockAccess.getBlockId(par2, par3, par4);
		return func_94477_d(var6) ? var6 == Block.redstoneWire.blockID ? par1IBlockAccess
				.getBlockMetadata(par2, par3, par4) : par1IBlockAccess
				.isBlockProvidingPowerTo(par2, par3, par4, par5)
				: 0;
	}

	/**
	 * Can this block provide power. Only wire currently seems to have this
	 * change based on its state.
	 */
	@Override
	public boolean canProvidePower() {
		return true;
	}

	/**
	 * Called when the block is placed in the world.
	 */
	@Override
	public void onBlockPlacedBy(final World par1World, final int par2,
			final int par3, final int par4,
			final EntityLiving par5EntityLiving, final ItemStack par6ItemStack) {
		final int var7 = ((MathHelper
				.floor_double(par5EntityLiving.rotationYaw * 4.0F / 360.0F + 0.5D) & 3) + 2) % 4;
		par1World.setBlockMetadataWithNotify(par2, par3, par4, var7, 3);
		final boolean var8 = func_94478_d(par1World, par2, par3, par4, var7);

		if (var8) {
			par1World.scheduleBlockUpdate(par2, par3, par4, blockID, 1);
		}
	}

	/**
	 * Called whenever the block is added into the world. Args: world, x, y, z
	 */
	@Override
	public void onBlockAdded(final World par1World, final int par2,
			final int par3, final int par4) {
		func_94483_i_(par1World, par2, par3, par4);
	}

	protected void func_94483_i_(final World par1World, final int par2,
			final int par3, final int par4) {
		final int var5 = BlockDirectional.getDirection(par1World
				.getBlockMetadata(par2, par3, par4));

		if (var5 == 1) {
			par1World
					.notifyBlockOfNeighborChange(par2 + 1, par3, par4, blockID);
			par1World.notifyBlocksOfNeighborChange(par2 + 1, par3, par4,
					blockID, 4);
		}

		if (var5 == 3) {
			par1World
					.notifyBlockOfNeighborChange(par2 - 1, par3, par4, blockID);
			par1World.notifyBlocksOfNeighborChange(par2 - 1, par3, par4,
					blockID, 5);
		}

		if (var5 == 2) {
			par1World
					.notifyBlockOfNeighborChange(par2, par3, par4 + 1, blockID);
			par1World.notifyBlocksOfNeighborChange(par2, par3, par4 + 1,
					blockID, 2);
		}

		if (var5 == 0) {
			par1World
					.notifyBlockOfNeighborChange(par2, par3, par4 - 1, blockID);
			par1World.notifyBlocksOfNeighborChange(par2, par3, par4 - 1,
					blockID, 3);
		}
	}

	/**
	 * Called right before the block is destroyed by a player. Args: world, x,
	 * y, z, metaData
	 */
	@Override
	public void onBlockDestroyedByPlayer(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		if (isRepeaterPowered) {
			par1World.notifyBlocksOfNeighborChange(par2 + 1, par3, par4,
					blockID);
			par1World.notifyBlocksOfNeighborChange(par2 - 1, par3, par4,
					blockID);
			par1World.notifyBlocksOfNeighborChange(par2, par3, par4 + 1,
					blockID);
			par1World.notifyBlocksOfNeighborChange(par2, par3, par4 - 1,
					blockID);
			par1World.notifyBlocksOfNeighborChange(par2, par3 - 1, par4,
					blockID);
			par1World.notifyBlocksOfNeighborChange(par2, par3 + 1, par4,
					blockID);
		}

		super.onBlockDestroyedByPlayer(par1World, par2, par3, par4, par5);
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	protected boolean func_94477_d(final int par1) {
		final Block var2 = Block.blocksList[par1];
		return var2 != null && var2.canProvidePower();
	}

	protected int func_94480_d(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		return 15;
	}

	public static boolean isRedstoneRepeaterBlockID(final int par0) {
		return Block.redstoneRepeaterIdle.func_94487_f(par0)
				|| Block.redstoneComparatorIdle.func_94487_f(par0);
	}

	public boolean func_94487_f(final int par1) {
		return par1 == func_94485_e().blockID || par1 == func_94484_i().blockID;
	}

	public boolean func_83011_d(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		final int var6 = BlockDirectional.getDirection(par5);

		if (BlockRedstoneLogic.isRedstoneRepeaterBlockID(par1World.getBlockId(
				par2 - Direction.offsetX[var6], par3, par4
						- Direction.offsetZ[var6]))) {
			final int var7 = par1World.getBlockMetadata(par2
					- Direction.offsetX[var6], par3, par4
					- Direction.offsetZ[var6]);
			final int var8 = BlockDirectional.getDirection(var7);
			return var8 != var6;
		} else {
			return false;
		}
	}

	protected int func_94486_g(final int par1) {
		return func_94481_j_(par1);
	}

	protected abstract int func_94481_j_(int var1);

	protected abstract BlockRedstoneLogic func_94485_e();

	protected abstract BlockRedstoneLogic func_94484_i();

	/**
	 * Returns true if the given block ID is equivalent to this one. Example:
	 * redstoneTorchOn matches itself and redstoneTorchOff, and vice versa. Most
	 * blocks only match themselves.
	 */
	@Override
	public boolean isAssociatedBlockID(final int par1) {
		return func_94487_f(par1);
	}
}
