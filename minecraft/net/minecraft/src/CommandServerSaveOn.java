package net.minecraft.src;

import net.minecraft.server.MinecraftServer;

public class CommandServerSaveOn extends CommandBase {
	@Override
	public String getCommandName() {
		return "save-on";
	}

	/**
	 * Return the required permission level for this command.
	 */
	@Override
	public int getRequiredPermissionLevel() {
		return 4;
	}

	@Override
	public void processCommand(final ICommandSender par1ICommandSender,
			final String[] par2ArrayOfStr) {
		final MinecraftServer var3 = MinecraftServer.getServer();

		for (final WorldServer worldServer : var3.worldServers) {
			if (worldServer != null) {
				final WorldServer var5 = worldServer;
				var5.canNotSave = false;
			}
		}

		CommandBase.notifyAdmins(par1ICommandSender, "commands.save.enabled",
				new Object[0]);
	}
}
