package net.minecraft.src;

public class EntityPig extends EntityAnimal {
	/** AI task for player control. */
	private final EntityAIControlledByPlayer aiControlledByPlayer;

	public EntityPig(final World par1World) {
		super(par1World);
		texture = "/mob/pig.png";
		setSize(0.9F, 0.9F);
		getNavigator().setAvoidsWater(true);
		final float var2 = 0.25F;
		tasks.addTask(0, new EntityAISwimming(this));
		tasks.addTask(1, new EntityAIPanic(this, 0.38F));
		tasks.addTask(2, aiControlledByPlayer = new EntityAIControlledByPlayer(
				this, 0.34F));
		tasks.addTask(3, new EntityAIMate(this, var2));
		tasks.addTask(4, new EntityAITempt(this, 0.3F,
				Item.carrotOnAStick.itemID, false));
		tasks.addTask(4, new EntityAITempt(this, 0.3F, Item.carrot.itemID,
				false));
		tasks.addTask(5, new EntityAIFollowParent(this, 0.28F));
		tasks.addTask(6, new EntityAIWander(this, var2));
		tasks.addTask(7, new EntityAIWatchClosest(this, EntityPlayer.class,
				6.0F));
		tasks.addTask(8, new EntityAILookIdle(this));
	}

	/**
	 * Returns true if the newer Entity AI code should be run
	 */
	@Override
	public boolean isAIEnabled() {
		return true;
	}

	@Override
	public int getMaxHealth() {
		return 10;
	}

	@Override
	protected void updateAITasks() {
		super.updateAITasks();
	}

	/**
	 * returns true if all the conditions for steering the entity are met. For
	 * pigs, this is true if it is being ridden by a player and the player is
	 * holding a carrot-on-a-stick
	 */
	@Override
	public boolean canBeSteered() {
		final ItemStack var1 = ((EntityPlayer) riddenByEntity).getHeldItem();
		return var1 != null && var1.itemID == Item.carrotOnAStick.itemID;
	}

	@Override
	protected void entityInit() {
		super.entityInit();
		dataWatcher.addObject(16, Byte.valueOf((byte) 0));
	}

	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	@Override
	public void writeEntityToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeEntityToNBT(par1NBTTagCompound);
		par1NBTTagCompound.setBoolean("Saddle", getSaddled());
	}

	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	@Override
	public void readEntityFromNBT(final NBTTagCompound par1NBTTagCompound) {
		super.readEntityFromNBT(par1NBTTagCompound);
		setSaddled(par1NBTTagCompound.getBoolean("Saddle"));
	}

	/**
	 * Returns the sound this mob makes while it's alive.
	 */
	@Override
	protected String getLivingSound() {
		return "mob.pig.say";
	}

	/**
	 * Returns the sound this mob makes when it is hurt.
	 */
	@Override
	protected String getHurtSound() {
		return "mob.pig.say";
	}

	/**
	 * Returns the sound this mob makes on death.
	 */
	@Override
	protected String getDeathSound() {
		return "mob.pig.death";
	}

	/**
	 * Plays step sound at given x, y, z for the entity
	 */
	@Override
	protected void playStepSound(final int par1, final int par2,
			final int par3, final int par4) {
		playSound("mob.pig.step", 0.15F, 1.0F);
	}

	/**
	 * Called when a player interacts with a mob. e.g. gets milk from a cow,
	 * gets into the saddle on a pig.
	 */
	@Override
	public boolean interact(final EntityPlayer par1EntityPlayer) {
		if (super.interact(par1EntityPlayer)) {
			return true;
		} else if (getSaddled()
				&& !worldObj.isRemote
				&& (riddenByEntity == null || riddenByEntity == par1EntityPlayer)) {
			par1EntityPlayer.mountEntity(this);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns the item ID for the item the mob drops on death.
	 */
	@Override
	protected int getDropItemId() {
		return isBurning() ? Item.porkCooked.itemID : Item.porkRaw.itemID;
	}

	/**
	 * Drop 0-2 items of this living's type. @param par1 - Whether this entity
	 * has recently been hit by a player. @param par2 - Level of Looting used to
	 * kill this mob.
	 */
	@Override
	protected void dropFewItems(final boolean par1, final int par2) {
		final int var3 = rand.nextInt(3) + 1 + rand.nextInt(1 + par2);

		for (int var4 = 0; var4 < var3; ++var4) {
			if (isBurning()) {
				dropItem(Item.porkCooked.itemID, 1);
			} else {
				dropItem(Item.porkRaw.itemID, 1);
			}
		}

		if (getSaddled()) {
			dropItem(Item.saddle.itemID, 1);
		}
	}

	/**
	 * Returns true if the pig is saddled.
	 */
	public boolean getSaddled() {
		return (dataWatcher.getWatchableObjectByte(16) & 1) != 0;
	}

	/**
	 * Set or remove the saddle of the pig.
	 */
	public void setSaddled(final boolean par1) {
		if (par1) {
			dataWatcher.updateObject(16, Byte.valueOf((byte) 1));
		} else {
			dataWatcher.updateObject(16, Byte.valueOf((byte) 0));
		}
	}

	/**
	 * Called when a lightning bolt hits the entity.
	 */
	@Override
	public void onStruckByLightning(
			final EntityLightningBolt par1EntityLightningBolt) {
		if (!worldObj.isRemote) {
			final EntityPigZombie var2 = new EntityPigZombie(worldObj);
			var2.setLocationAndAngles(posX, posY, posZ, rotationYaw,
					rotationPitch);
			worldObj.spawnEntityInWorld(var2);
			setDead();
		}
	}

	/**
	 * Called when the mob is falling. Calculates and applies fall damage.
	 */
	@Override
	protected void fall(final float par1) {
		super.fall(par1);

		if (par1 > 5.0F && riddenByEntity instanceof EntityPlayer) {
			((EntityPlayer) riddenByEntity)
					.triggerAchievement(AchievementList.flyPig);
		}
	}

	/**
	 * This function is used when two same-species animals in 'love mode' breed
	 * to generate the new baby animal.
	 */
	public EntityPig spawnBabyAnimal(final EntityAgeable par1EntityAgeable) {
		return new EntityPig(worldObj);
	}

	/**
	 * Checks if the parameter is an item which this animal can be fed to breed
	 * it (wheat, carrots or seeds depending on the animal type)
	 */
	@Override
	public boolean isBreedingItem(final ItemStack par1ItemStack) {
		return par1ItemStack != null
				&& par1ItemStack.itemID == Item.carrot.itemID;
	}

	/**
	 * Return the AI task for player control.
	 */
	public EntityAIControlledByPlayer getAIControlledByPlayer() {
		return aiControlledByPlayer;
	}

	@Override
	public EntityAgeable createChild(final EntityAgeable par1EntityAgeable) {
		return spawnBabyAnimal(par1EntityAgeable);
	}
}
