package net.minecraft.src;

final class StatTypeSimple implements IStatType {
	/**
	 * Formats a given stat for human consumption.
	 */
	@Override
	public String format(final int par1) {
		return StatBase.getNumberFormat().format(par1);
	}
}
