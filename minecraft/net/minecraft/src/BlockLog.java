package net.minecraft.src;

import java.util.List;
import java.util.Random;

public class BlockLog extends Block {
	/** The type of tree this log came from. */
	public static final String[] woodType = new String[] { "oak", "spruce",
			"birch", "jungle" };
	public static final String[] treeTextureTypes = new String[] { "tree_side",
			"tree_spruce", "tree_birch", "tree_jungle" };
	private Icon[] iconArray;
	private Icon tree_top;

	protected BlockLog(final int par1) {
		super(par1, Material.wood);
		setCreativeTab(CreativeTabs.tabBlock);
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return 31;
	}

	/**
	 * Returns the quantity of items to drop on block destruction.
	 */
	@Override
	public int quantityDropped(final Random par1Random) {
		return 1;
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return Block.wood.blockID;
	}

	/**
	 * ejects contained items into the world, and notifies neighbours of an
	 * update, as appropriate
	 */
	@Override
	public void breakBlock(final World par1World, final int par2,
			final int par3, final int par4, final int par5, final int par6) {
		final byte var7 = 4;
		final int var8 = var7 + 1;

		if (par1World.checkChunksExist(par2 - var8, par3 - var8, par4 - var8,
				par2 + var8, par3 + var8, par4 + var8)) {
			for (int var9 = -var7; var9 <= var7; ++var9) {
				for (int var10 = -var7; var10 <= var7; ++var10) {
					for (int var11 = -var7; var11 <= var7; ++var11) {
						final int var12 = par1World.getBlockId(par2 + var9,
								par3 + var10, par4 + var11);

						if (var12 == Block.leaves.blockID) {
							final int var13 = par1World.getBlockMetadata(par2
									+ var9, par3 + var10, par4 + var11);

							if ((var13 & 8) == 0) {
								par1World.setBlockMetadataWithNotify(par2
										+ var9, par3 + var10, par4 + var11,
										var13 | 8, 4);
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Called when a block is placed using its ItemBlock. Args: World, X, Y, Z,
	 * side, hitX, hitY, hitZ, block metadata
	 */
	@Override
	public int onBlockPlaced(final World par1World, final int par2,
			final int par3, final int par4, final int par5, final float par6,
			final float par7, final float par8, final int par9) {
		final int var10 = par9 & 3;
		byte var11 = 0;

		switch (par5) {
		case 0:
		case 1:
			var11 = 0;
			break;

		case 2:
		case 3:
			var11 = 8;
			break;

		case 4:
		case 5:
			var11 = 4;
		}

		return var10 | var11;
	}

	/**
	 * From the specified side and block metadata retrieves the blocks texture.
	 * Args: side, metadata
	 */
	@Override
	public Icon getIcon(final int par1, final int par2) {
		final int var3 = par2 & 12;
		final int var4 = par2 & 3;
		return var3 == 0 && (par1 == 1 || par1 == 0) ? tree_top : var3 == 4
				&& (par1 == 5 || par1 == 4) ? tree_top : var3 == 8
				&& (par1 == 2 || par1 == 3) ? tree_top : iconArray[var4];
	}

	/**
	 * Determines the damage on the item the block drops. Used in cloth and
	 * wood.
	 */
	@Override
	public int damageDropped(final int par1) {
		return par1 & 3;
	}

	/**
	 * returns a number between 0 and 3
	 */
	public static int limitToValidMetadata(final int par0) {
		return par0 & 3;
	}

	/**
	 * returns a list of blocks with the same ID, but different meta (eg: wood
	 * returns 4 blocks)
	 */
	@Override
	public void getSubBlocks(final int par1,
			final CreativeTabs par2CreativeTabs, final List par3List) {
		par3List.add(new ItemStack(par1, 1, 0));
		par3List.add(new ItemStack(par1, 1, 1));
		par3List.add(new ItemStack(par1, 1, 2));
		par3List.add(new ItemStack(par1, 1, 3));
	}

	/**
	 * Returns an item stack containing a single instance of the current block
	 * type. 'i' is the block's subtype/damage and is ignored for blocks which
	 * do not support subtypes. Blocks which cannot be harvested should return
	 * null.
	 */
	@Override
	protected ItemStack createStackedBlock(final int par1) {
		return new ItemStack(blockID, 1, BlockLog.limitToValidMetadata(par1));
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		tree_top = par1IconRegister.registerIcon("tree_top");
		iconArray = new Icon[BlockLog.treeTextureTypes.length];

		for (int var2 = 0; var2 < iconArray.length; ++var2) {
			iconArray[var2] = par1IconRegister
					.registerIcon(BlockLog.treeTextureTypes[var2]);
		}
	}
}
