package net.minecraft.src;

public class LongHashMap {
	/** the array of all elements in the hash */
	private transient LongHashMapEntry[] hashArray = new LongHashMapEntry[1024];

	/** the number of elements in the hash array */
	private transient int numHashElements;

	/**
	 * the maximum amount of elements in the hash (probably 3/4 the size due to
	 * meh hashing function)
	 */
	private int capacity;

	public LongHashMap() {
		capacity = (int) (0.75F * hashArray.length);
	}

	/**
	 * returns the hashed key given the original key
	 */
	private static int getHashedKey(final long par0) {
		return (int) (par0 ^ par0 >>> 27);
	}

	/**
	 * gets the index in the hash given the array length and the hashed key
	 */
	private static int getHashIndex(final int par0, final int par1) {
		return par0 & par1 - 1;
	}

	public int getNumHashElements() {
		return numHashElements;
	}

	/**
	 * get the value from the map given the key
	 */
	public Object getValueByKey(final long par1) {
		final int var3 = LongHashMap.getHashedKey(par1);

		for (LongHashMapEntry var4 = hashArray[LongHashMap.getHashIndex(var3,
				hashArray.length)]; var4 != null; var4 = var4.nextEntry) {
			if (var4.key == par1) {
				return var4.value;
			}
		}

		return null;
	}

	public boolean containsItem(final long par1) {
		return getEntry(par1) != null;
	}

	final LongHashMapEntry getEntry(final long par1) {
		final int var3 = LongHashMap.getHashedKey(par1);

		for (LongHashMapEntry var4 = hashArray[LongHashMap.getHashIndex(var3,
				hashArray.length)]; var4 != null; var4 = var4.nextEntry) {
			if (var4.key == par1) {
				return var4;
			}
		}

		return null;
	}

	/**
	 * Add a key-value pair.
	 */
	public void add(final long par1, final Object par3Obj) {
		final int var4 = LongHashMap.getHashedKey(par1);
		final int var5 = LongHashMap.getHashIndex(var4, hashArray.length);

		for (LongHashMapEntry var6 = hashArray[var5]; var6 != null; var6 = var6.nextEntry) {
			if (var6.key == par1) {
				var6.value = par3Obj;
				return;
			}
		}

		createKey(var4, par1, par3Obj, var5);
	}

	/**
	 * resizes the table
	 */
	private void resizeTable(final int par1) {
		final LongHashMapEntry[] var2 = hashArray;
		final int var3 = var2.length;

		if (var3 == 1073741824) {
			capacity = Integer.MAX_VALUE;
		} else {
			final LongHashMapEntry[] var4 = new LongHashMapEntry[par1];
			copyHashTableTo(var4);
			hashArray = var4;
			final float var10001 = par1;
			this.getClass();
			capacity = (int) (var10001 * 0.75F);
		}
	}

	/**
	 * copies the hash table to the specified array
	 */
	private void copyHashTableTo(
			final LongHashMapEntry[] par1ArrayOfLongHashMapEntry) {
		final LongHashMapEntry[] var2 = hashArray;
		final int var3 = par1ArrayOfLongHashMapEntry.length;

		for (int var4 = 0; var4 < var2.length; ++var4) {
			LongHashMapEntry var5 = var2[var4];

			if (var5 != null) {
				var2[var4] = null;
				LongHashMapEntry var6;

				do {
					var6 = var5.nextEntry;
					final int var7 = LongHashMap.getHashIndex(var5.hash, var3);
					var5.nextEntry = par1ArrayOfLongHashMapEntry[var7];
					par1ArrayOfLongHashMapEntry[var7] = var5;
					var5 = var6;
				} while (var6 != null);
			}
		}
	}

	/**
	 * calls the removeKey method and returns removed object
	 */
	public Object remove(final long par1) {
		final LongHashMapEntry var3 = removeKey(par1);
		return var3 == null ? null : var3.value;
	}

	/**
	 * removes the key from the hash linked list
	 */
	final LongHashMapEntry removeKey(final long par1) {
		final int var3 = LongHashMap.getHashedKey(par1);
		final int var4 = LongHashMap.getHashIndex(var3, hashArray.length);
		LongHashMapEntry var5 = hashArray[var4];
		LongHashMapEntry var6;
		LongHashMapEntry var7;

		for (var6 = var5; var6 != null; var6 = var7) {
			var7 = var6.nextEntry;

			if (var6.key == par1) {
				--numHashElements;

				if (var5 == var6) {
					hashArray[var4] = var7;
				} else {
					var5.nextEntry = var7;
				}

				return var6;
			}

			var5 = var6;
		}

		return var6;
	}

	/**
	 * creates the key in the hash table
	 */
	private void createKey(final int par1, final long par2,
			final Object par4Obj, final int par5) {
		final LongHashMapEntry var6 = hashArray[par5];
		hashArray[par5] = new LongHashMapEntry(par1, par2, par4Obj, var6);

		if (numHashElements++ >= capacity) {
			resizeTable(2 * hashArray.length);
		}
	}

	/**
	 * public method to get the hashed key(hashCode)
	 */
	static int getHashCode(final long par0) {
		return LongHashMap.getHashedKey(par0);
	}

	public double getKeyDistribution() {
		int var1 = 0;

		for (final LongHashMapEntry element : hashArray) {
			if (element != null) {
				++var1;
			}
		}

		return 1.0D * var1 / numHashElements;
	}
}
