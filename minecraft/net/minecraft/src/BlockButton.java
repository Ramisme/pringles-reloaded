package net.minecraft.src;

import java.util.List;
import java.util.Random;

public abstract class BlockButton extends Block {
	/** Whether this button is sensible to arrows, used by wooden buttons. */
	private final boolean sensible;

	protected BlockButton(final int par1, final boolean par2) {
		super(par1, Material.circuits);
		setTickRandomly(true);
		setCreativeTab(CreativeTabs.tabRedstone);
		sensible = par2;
	}

	/**
	 * Returns a bounding box from the pool of bounding boxes (this means this
	 * box can change after the pool has been cleared to be reused)
	 */
	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(final World par1World,
			final int par2, final int par3, final int par4) {
		return null;
	}

	/**
	 * How many world ticks before ticking
	 */
	@Override
	public int tickRate(final World par1World) {
		return sensible ? 30 : 20;
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	/**
	 * checks to see if you can place this block can be placed on that side of a
	 * block: BlockLever overrides
	 */
	@Override
	public boolean canPlaceBlockOnSide(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		return par5 == 2 && par1World.isBlockNormalCube(par2, par3, par4 + 1) ? true
				: par5 == 3
						&& par1World.isBlockNormalCube(par2, par3, par4 - 1) ? true
						: par5 == 4
								&& par1World.isBlockNormalCube(par2 + 1, par3,
										par4) ? true : par5 == 5
								&& par1World.isBlockNormalCube(par2 - 1, par3,
										par4);
	}

	/**
	 * Checks to see if its valid to put this block at the specified
	 * coordinates. Args: world, x, y, z
	 */
	@Override
	public boolean canPlaceBlockAt(final World par1World, final int par2,
			final int par3, final int par4) {
		return par1World.isBlockNormalCube(par2 - 1, par3, par4) ? true
				: par1World.isBlockNormalCube(par2 + 1, par3, par4) ? true
						: par1World.isBlockNormalCube(par2, par3, par4 - 1) ? true
								: par1World.isBlockNormalCube(par2, par3,
										par4 + 1);
	}

	/**
	 * Called when a block is placed using its ItemBlock. Args: World, X, Y, Z,
	 * side, hitX, hitY, hitZ, block metadata
	 */
	@Override
	public int onBlockPlaced(final World par1World, final int par2,
			final int par3, final int par4, final int par5, final float par6,
			final float par7, final float par8, final int par9) {
		int var10 = par1World.getBlockMetadata(par2, par3, par4);
		final int var11 = var10 & 8;
		var10 &= 7;

		if (par5 == 2 && par1World.isBlockNormalCube(par2, par3, par4 + 1)) {
			var10 = 4;
		} else if (par5 == 3
				&& par1World.isBlockNormalCube(par2, par3, par4 - 1)) {
			var10 = 3;
		} else if (par5 == 4
				&& par1World.isBlockNormalCube(par2 + 1, par3, par4)) {
			var10 = 2;
		} else if (par5 == 5
				&& par1World.isBlockNormalCube(par2 - 1, par3, par4)) {
			var10 = 1;
		} else {
			var10 = getOrientation(par1World, par2, par3, par4);
		}

		return var10 + var11;
	}

	/**
	 * Get side which this button is facing.
	 */
	private int getOrientation(final World par1World, final int par2,
			final int par3, final int par4) {
		return par1World.isBlockNormalCube(par2 - 1, par3, par4) ? 1
				: par1World.isBlockNormalCube(par2 + 1, par3, par4) ? 2
						: par1World.isBlockNormalCube(par2, par3, par4 - 1) ? 3
								: par1World.isBlockNormalCube(par2, par3,
										par4 + 1) ? 4 : 1;
	}

	/**
	 * Lets the block know when one of its neighbor changes. Doesn't know which
	 * neighbor changed (coordinates passed are their own) Args: x, y, z,
	 * neighbor blockID
	 */
	@Override
	public void onNeighborBlockChange(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		if (redundantCanPlaceBlockAt(par1World, par2, par3, par4)) {
			final int var6 = par1World.getBlockMetadata(par2, par3, par4) & 7;
			boolean var7 = false;

			if (!par1World.isBlockNormalCube(par2 - 1, par3, par4) && var6 == 1) {
				var7 = true;
			}

			if (!par1World.isBlockNormalCube(par2 + 1, par3, par4) && var6 == 2) {
				var7 = true;
			}

			if (!par1World.isBlockNormalCube(par2, par3, par4 - 1) && var6 == 3) {
				var7 = true;
			}

			if (!par1World.isBlockNormalCube(par2, par3, par4 + 1) && var6 == 4) {
				var7 = true;
			}

			if (var7) {
				dropBlockAsItem(par1World, par2, par3, par4,
						par1World.getBlockMetadata(par2, par3, par4), 0);
				par1World.setBlockToAir(par2, par3, par4);
			}
		}
	}

	/**
	 * This method is redundant, check it out...
	 */
	private boolean redundantCanPlaceBlockAt(final World par1World,
			final int par2, final int par3, final int par4) {
		if (!canPlaceBlockAt(par1World, par2, par3, par4)) {
			dropBlockAsItem(par1World, par2, par3, par4,
					par1World.getBlockMetadata(par2, par3, par4), 0);
			par1World.setBlockToAir(par2, par3, par4);
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Updates the blocks bounds based on its current state. Args: world, x, y,
	 * z
	 */
	@Override
	public void setBlockBoundsBasedOnState(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		final int var5 = par1IBlockAccess.getBlockMetadata(par2, par3, par4);
		func_82534_e(var5);
	}

	private void func_82534_e(final int par1) {
		final int var2 = par1 & 7;
		final boolean var3 = (par1 & 8) > 0;
		final float var4 = 0.375F;
		final float var5 = 0.625F;
		final float var6 = 0.1875F;
		float var7 = 0.125F;

		if (var3) {
			var7 = 0.0625F;
		}

		if (var2 == 1) {
			setBlockBounds(0.0F, var4, 0.5F - var6, var7, var5, 0.5F + var6);
		} else if (var2 == 2) {
			setBlockBounds(1.0F - var7, var4, 0.5F - var6, 1.0F, var5,
					0.5F + var6);
		} else if (var2 == 3) {
			setBlockBounds(0.5F - var6, var4, 0.0F, 0.5F + var6, var5, var7);
		} else if (var2 == 4) {
			setBlockBounds(0.5F - var6, var4, 1.0F - var7, 0.5F + var6, var5,
					1.0F);
		}
	}

	/**
	 * Called when the block is clicked by a player. Args: x, y, z, entityPlayer
	 */
	@Override
	public void onBlockClicked(final World par1World, final int par2,
			final int par3, final int par4, final EntityPlayer par5EntityPlayer) {
	}

	/**
	 * Called upon block activation (right click on the block.)
	 */
	@Override
	public boolean onBlockActivated(final World par1World, final int par2,
			final int par3, final int par4,
			final EntityPlayer par5EntityPlayer, final int par6,
			final float par7, final float par8, final float par9) {
		final int var10 = par1World.getBlockMetadata(par2, par3, par4);
		final int var11 = var10 & 7;
		final int var12 = 8 - (var10 & 8);

		if (var12 == 0) {
			return true;
		} else {
			par1World.setBlockMetadataWithNotify(par2, par3, par4, var11
					+ var12, 3);
			par1World.markBlockRangeForRenderUpdate(par2, par3, par4, par2,
					par3, par4);
			par1World.playSoundEffect(par2 + 0.5D, par3 + 0.5D, par4 + 0.5D,
					"random.click", 0.3F, 0.6F);
			func_82536_d(par1World, par2, par3, par4, var11);
			par1World.scheduleBlockUpdate(par2, par3, par4, blockID,
					tickRate(par1World));
			return true;
		}
	}

	/**
	 * ejects contained items into the world, and notifies neighbours of an
	 * update, as appropriate
	 */
	@Override
	public void breakBlock(final World par1World, final int par2,
			final int par3, final int par4, final int par5, final int par6) {
		if ((par6 & 8) > 0) {
			final int var7 = par6 & 7;
			func_82536_d(par1World, par2, par3, par4, var7);
		}

		super.breakBlock(par1World, par2, par3, par4, par5, par6);
	}

	/**
	 * Returns true if the block is emitting indirect/weak redstone power on the
	 * specified side. If isBlockNormalCube returns true, standard redstone
	 * propagation rules will apply instead and this will not be called. Args:
	 * World, X, Y, Z, side. Note that the side is reversed - eg it is 1 (up)
	 * when checking the bottom of the block.
	 */
	@Override
	public int isProvidingWeakPower(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		return (par1IBlockAccess.getBlockMetadata(par2, par3, par4) & 8) > 0 ? 15
				: 0;
	}

	/**
	 * Returns true if the block is emitting direct/strong redstone power on the
	 * specified side. Args: World, X, Y, Z, side. Note that the side is
	 * reversed - eg it is 1 (up) when checking the bottom of the block.
	 */
	@Override
	public int isProvidingStrongPower(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		final int var6 = par1IBlockAccess.getBlockMetadata(par2, par3, par4);

		if ((var6 & 8) == 0) {
			return 0;
		} else {
			final int var7 = var6 & 7;
			return var7 == 5 && par5 == 1 ? 15 : var7 == 4 && par5 == 2 ? 15
					: var7 == 3 && par5 == 3 ? 15 : var7 == 2 && par5 == 4 ? 15
							: var7 == 1 && par5 == 5 ? 15 : 0;
		}
	}

	/**
	 * Can this block provide power. Only wire currently seems to have this
	 * change based on its state.
	 */
	@Override
	public boolean canProvidePower() {
		return true;
	}

	/**
	 * Ticks the block if it's been scheduled
	 */
	@Override
	public void updateTick(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		if (!par1World.isRemote) {
			final int var6 = par1World.getBlockMetadata(par2, par3, par4);

			if ((var6 & 8) != 0) {
				if (sensible) {
					func_82535_o(par1World, par2, par3, par4);
				} else {
					par1World.setBlockMetadataWithNotify(par2, par3, par4,
							var6 & 7, 3);
					final int var7 = var6 & 7;
					func_82536_d(par1World, par2, par3, par4, var7);
					par1World.playSoundEffect(par2 + 0.5D, par3 + 0.5D,
							par4 + 0.5D, "random.click", 0.3F, 0.5F);
					par1World.markBlockRangeForRenderUpdate(par2, par3, par4,
							par2, par3, par4);
				}
			}
		}
	}

	/**
	 * Sets the block's bounds for rendering it as an item
	 */
	@Override
	public void setBlockBoundsForItemRender() {
		final float var1 = 0.1875F;
		final float var2 = 0.125F;
		final float var3 = 0.125F;
		setBlockBounds(0.5F - var1, 0.5F - var2, 0.5F - var3, 0.5F + var1,
				0.5F + var2, 0.5F + var3);
	}

	/**
	 * Triggered whenever an entity collides with this block (enters into the
	 * block). Args: world, x, y, z, entity
	 */
	@Override
	public void onEntityCollidedWithBlock(final World par1World,
			final int par2, final int par3, final int par4,
			final Entity par5Entity) {
		if (!par1World.isRemote) {
			if (sensible) {
				if ((par1World.getBlockMetadata(par2, par3, par4) & 8) == 0) {
					func_82535_o(par1World, par2, par3, par4);
				}
			}
		}
	}

	private void func_82535_o(final World par1World, final int par2,
			final int par3, final int par4) {
		final int var5 = par1World.getBlockMetadata(par2, par3, par4);
		final int var6 = var5 & 7;
		final boolean var7 = (var5 & 8) != 0;
		func_82534_e(var5);
		final List var9 = par1World.getEntitiesWithinAABB(
				EntityArrow.class,
				AxisAlignedBB.getAABBPool().getAABB(par2 + minX, par3 + minY,
						par4 + minZ, par2 + maxX, par3 + maxY, par4 + maxZ));
		final boolean var8 = !var9.isEmpty();

		if (var8 && !var7) {
			par1World.setBlockMetadataWithNotify(par2, par3, par4, var6 | 8, 3);
			func_82536_d(par1World, par2, par3, par4, var6);
			par1World.markBlockRangeForRenderUpdate(par2, par3, par4, par2,
					par3, par4);
			par1World.playSoundEffect(par2 + 0.5D, par3 + 0.5D, par4 + 0.5D,
					"random.click", 0.3F, 0.6F);
		}

		if (!var8 && var7) {
			par1World.setBlockMetadataWithNotify(par2, par3, par4, var6, 3);
			func_82536_d(par1World, par2, par3, par4, var6);
			par1World.markBlockRangeForRenderUpdate(par2, par3, par4, par2,
					par3, par4);
			par1World.playSoundEffect(par2 + 0.5D, par3 + 0.5D, par4 + 0.5D,
					"random.click", 0.3F, 0.5F);
		}

		if (var8) {
			par1World.scheduleBlockUpdate(par2, par3, par4, blockID,
					tickRate(par1World));
		}
	}

	private void func_82536_d(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		par1World.notifyBlocksOfNeighborChange(par2, par3, par4, blockID);

		if (par5 == 1) {
			par1World.notifyBlocksOfNeighborChange(par2 - 1, par3, par4,
					blockID);
		} else if (par5 == 2) {
			par1World.notifyBlocksOfNeighborChange(par2 + 1, par3, par4,
					blockID);
		} else if (par5 == 3) {
			par1World.notifyBlocksOfNeighborChange(par2, par3, par4 - 1,
					blockID);
		} else if (par5 == 4) {
			par1World.notifyBlocksOfNeighborChange(par2, par3, par4 + 1,
					blockID);
		} else {
			par1World.notifyBlocksOfNeighborChange(par2, par3 - 1, par4,
					blockID);
		}
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
	}
}
