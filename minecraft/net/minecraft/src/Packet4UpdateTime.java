package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet4UpdateTime extends Packet {
	/** World age in ticks. */
	public long worldAge;

	/** The world time in minutes. */
	public long time;

	public Packet4UpdateTime() {
	}

	public Packet4UpdateTime(final long par1, final long par3) {
		worldAge = par1;
		time = par3;
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		worldAge = par1DataInputStream.readLong();
		time = par1DataInputStream.readLong();
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		par1DataOutputStream.writeLong(worldAge);
		par1DataOutputStream.writeLong(time);
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleUpdateTime(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 16;
	}

	/**
	 * only false for the abstract Packet class, all real packets return true
	 */
	@Override
	public boolean isRealPacket() {
		return true;
	}

	/**
	 * eg return packet30entity.entityId == entityId; WARNING : will throw if
	 * you compare a packet to a different packet class
	 */
	@Override
	public boolean containsSameEntityIDAs(final Packet par1Packet) {
		return true;
	}

	/**
	 * If this returns true, the packet may be processed on any thread;
	 * otherwise it is queued for the main thread to handle.
	 */
	@Override
	public boolean canProcessAsync() {
		return true;
	}
}
