package net.minecraft.src;

public class ItemCloth extends ItemBlock {
	public ItemCloth(final int par1) {
		super(par1);
		setMaxDamage(0);
		setHasSubtypes(true);
	}

	/**
	 * Gets an icon index based on an item's damage value
	 */
	@Override
	public Icon getIconFromDamage(final int par1) {
		return Block.cloth.getIcon(2, BlockCloth.getBlockFromDye(par1));
	}

	/**
	 * Returns the metadata of the block which this Item (ItemBlock) can place
	 */
	@Override
	public int getMetadata(final int par1) {
		return par1;
	}

	/**
	 * Returns the unlocalized name of this item. This version accepts an
	 * ItemStack so different stacks can have different names based on their
	 * damage or NBT.
	 */
	@Override
	public String getUnlocalizedName(final ItemStack par1ItemStack) {
		return super.getUnlocalizedName()
				+ "."
				+ ItemDye.dyeColorNames[BlockCloth
						.getBlockFromDye(par1ItemStack.getItemDamage())];
	}
}
