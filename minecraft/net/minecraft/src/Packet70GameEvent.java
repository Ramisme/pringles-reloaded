package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet70GameEvent extends Packet {
	/**
	 * The client prints clientMessage[eventType] to chat when this packet is
	 * received.
	 */
	public static final String[] clientMessage = new String[] {
			"tile.bed.notValid", null, null, "gameMode.changed" };

	/** 0: Invalid bed, 1: Rain starts, 2: Rain stops, 3: Game mode changed. */
	public int eventType;

	/**
	 * When reason==3, the game mode to set. See EnumGameType for a list of
	 * values.
	 */
	public int gameMode;

	public Packet70GameEvent() {
	}

	public Packet70GameEvent(final int par1, final int par2) {
		eventType = par1;
		gameMode = par2;
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		eventType = par1DataInputStream.readByte();
		gameMode = par1DataInputStream.readByte();
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		par1DataOutputStream.writeByte(eventType);
		par1DataOutputStream.writeByte(gameMode);
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleGameEvent(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 2;
	}
}
