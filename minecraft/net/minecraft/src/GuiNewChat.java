package net.minecraft.src;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.minecraft.client.Minecraft;

import org.lwjgl.opengl.GL11;
import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.events.chat.ChatRenderEvent;

public class GuiNewChat extends Gui {
	/** The Minecraft instance. */
	protected final Minecraft mc;

	/** A list of messages previously sent through the chat GUI */
	protected final List sentMessages = new ArrayList();

	/** Chat lines to be displayed in the chat box */
	protected final List chatLines = new ArrayList();
	protected final List field_96134_d = new ArrayList();
	protected int field_73768_d = 0;
	protected boolean field_73769_e = false;

	public GuiNewChat(final Minecraft par1Minecraft) {
		mc = par1Minecraft;
	}

	public void drawChat(final int par1) {
		if (mc.gameSettings.chatVisibility != 2) {
			final int var2 = func_96127_i();
			boolean var3 = false;
			int var4 = 0;
			final int var5 = field_96134_d.size();
			final float var6 = mc.gameSettings.chatOpacity * 0.9F + 0.1F;

			if (var5 > 0) {
				if (getChatOpen()) {
					var3 = true;
				}

				final float var7 = func_96131_h();
				final int var8 = MathHelper.ceiling_float_int(func_96126_f()
						/ var7);
				GL11.glPushMatrix();
				GL11.glTranslatef(2.0F, -2.0F, 0.0F);
				GL11.glScalef(var7, var7, 1.0F);
				int var9;
				int var11;
				int var14;

				for (var9 = 0; var9 + field_73768_d < field_96134_d.size()
						&& var9 < var2; ++var9) {
					final ChatLine var10 = (ChatLine) field_96134_d.get(var9
							+ field_73768_d);

					if (var10 != null) {
						var11 = par1 - var10.getUpdatedCounter();

						if (var11 < 200 || var3) {
							double var12 = var11 / 200.0D;
							var12 = 1.0D - var12;
							var12 *= 10.0D;

							if (var12 < 0.0D) {
								var12 = 0.0D;
							}

							if (var12 > 1.0D) {
								var12 = 1.0D;
							}

							var12 *= var12;
							var14 = (int) (255.0D * var12);

							if (var3) {
								var14 = 255;
							}

							var14 = (int) (var14 * var6);
							++var4;

							if (var14 > 3) {
								final byte var15 = 0;
								final int var16 = -var9 * 9;
								Gui.drawRect(var15, var16 - 9,
										var15 + var8 + 4, var16,
										var14 / 2 << 24);
								GL11.glEnable(GL11.GL_BLEND);

								// TODO: GuiNewChat#drawChat
								final ChatRenderEvent event = new ChatRenderEvent(
										var10.getChatLineString());
								Pringles.getInstance().getFactory()
										.getEventManager().sendEvent(event);
								if (event.isCancelled()) {
									continue;
								}

								String var17 = event.getMessage();

								if (!mc.gameSettings.chatColours) {
									var17 = StringUtils
											.stripControlCodes(var17);
								}

								mc.fontRenderer.drawStringWithShadow(var17,
										var15, var16 - 8,
										16777215 + (var14 << 24));
							}
						}
					}
				}

				if (var3) {
					var9 = mc.fontRenderer.FONT_HEIGHT;
					GL11.glTranslatef(-3.0F, 0.0F, 0.0F);
					final int var18 = var5 * var9 + var5;
					var11 = var4 * var9 + var4;
					final int var20 = field_73768_d * var11 / var5;
					final int var13 = var11 * var11 / var18;

					if (var18 != var11) {
						var14 = var20 > 0 ? 170 : 96;
						final int var19 = field_73769_e ? 13382451 : 3355562;
						Gui.drawRect(0, -var20, 2, -var20 - var13, var19
								+ (var14 << 24));
						Gui.drawRect(2, -var20, 1, -var20 - var13,
								13421772 + (var14 << 24));
					}
				}

				GL11.glPopMatrix();
			}
		}
	}

	/**
	 * Clears the chat.
	 */
	public void clearChatMessages() {
		field_96134_d.clear();
		chatLines.clear();
		sentMessages.clear();
	}

	/**
	 * takes a String and prints it to chat
	 */
	public void printChatMessage(final String par1Str) {
		printChatMessageWithOptionalDeletion(par1Str, 0);
	}

	/**
	 * prints the String to Chat. If the ID is not 0, deletes an existing Chat
	 * Line of that ID from the GUI
	 */
	public void printChatMessageWithOptionalDeletion(final String par1Str,
			final int par2) {
		func_96129_a(par1Str, par2, mc.ingameGUI.getUpdateCounter(), false);
		mc.getLogAgent().logInfo("[CHAT] " + par1Str);
	}

	protected void func_96129_a(final String par1Str, final int par2,
			final int par3, final boolean par4) {
		final boolean var5 = getChatOpen();
		boolean var6 = true;

		if (par2 != 0) {
			deleteChatLine(par2);
		}

		final Iterator var7 = mc.fontRenderer.listFormattedStringToWidth(
				par1Str,
				MathHelper.floor_float(func_96126_f() / func_96131_h()))
				.iterator();

		while (var7.hasNext()) {
			String var8 = (String) var7.next();

			if (var5 && field_73768_d > 0) {
				field_73769_e = true;
				scroll(1);
			}

			if (!var6) {
				var8 = " " + var8;
			}

			var6 = false;
			field_96134_d.add(0, new ChatLine(par3, var8, par2));
		}

		while (field_96134_d.size() > 100) {
			field_96134_d.remove(field_96134_d.size() - 1);
		}

		if (!par4) {
			chatLines.add(0, new ChatLine(par3, par1Str.trim(), par2));

			while (chatLines.size() > 100) {
				chatLines.remove(chatLines.size() - 1);
			}
		}
	}

	public void func_96132_b() {
		field_96134_d.clear();
		resetScroll();

		for (int var1 = chatLines.size() - 1; var1 >= 0; --var1) {
			final ChatLine var2 = (ChatLine) chatLines.get(var1);
			func_96129_a(var2.getChatLineString(), var2.getChatLineID(),
					var2.getUpdatedCounter(), true);
		}
	}

	/**
	 * Gets the list of messages previously sent through the chat GUI
	 */
	public List getSentMessages() {
		return sentMessages;
	}

	/**
	 * Adds this string to the list of sent messages, for recall using the
	 * up/down arrow keys
	 */
	public void addToSentMessages(final String par1Str) {
		if (sentMessages.isEmpty()
				|| !((String) sentMessages.get(sentMessages.size() - 1))
						.equals(par1Str)) {
			sentMessages.add(par1Str);
		}
	}

	/**
	 * Resets the chat scroll (executed when the GUI is closed)
	 */
	public void resetScroll() {
		field_73768_d = 0;
		field_73769_e = false;
	}

	/**
	 * Scrolls the chat by the given number of lines.
	 */
	public void scroll(final int par1) {
		field_73768_d += par1;
		final int var2 = field_96134_d.size();

		if (field_73768_d > var2 - func_96127_i()) {
			field_73768_d = var2 - func_96127_i();
		}

		if (field_73768_d <= 0) {
			field_73768_d = 0;
			field_73769_e = false;
		}
	}

	public ChatClickData func_73766_a(final int par1, final int par2) {
		if (!getChatOpen()) {
			return null;
		} else {
			final ScaledResolution var3 = new ScaledResolution(mc.gameSettings,
					mc.displayWidth, mc.displayHeight);
			final int var4 = var3.getScaleFactor();
			final float var5 = func_96131_h();
			int var6 = par1 / var4 - 3;
			int var7 = par2 / var4 - 25;
			var6 = MathHelper.floor_float(var6 / var5);
			var7 = MathHelper.floor_float(var7 / var5);

			if (var6 >= 0 && var7 >= 0) {
				final int var8 = Math.min(func_96127_i(), field_96134_d.size());

				if (var6 <= MathHelper.floor_float(func_96126_f()
						/ func_96131_h())
						&& var7 < mc.fontRenderer.FONT_HEIGHT * var8 + var8) {
					final int var9 = var7 / (mc.fontRenderer.FONT_HEIGHT + 1)
							+ field_73768_d;
					return new ChatClickData(mc.fontRenderer,
							(ChatLine) field_96134_d.get(var9), var6, var7
									- (var9 - field_73768_d)
									* mc.fontRenderer.FONT_HEIGHT + var9);
				} else {
					return null;
				}
			} else {
				return null;
			}
		}
	}

	/**
	 * Adds a message to the chat after translating to the client's locale.
	 */
	public void addTranslatedMessage(final String par1Str,
			final Object... par2ArrayOfObj) {
		printChatMessage(StringTranslate.getInstance().translateKeyFormat(
				par1Str, par2ArrayOfObj));
	}

	/**
	 * @return {@code true} if the chat GUI is open
	 */
	public boolean getChatOpen() {
		return mc.currentScreen instanceof GuiChat;
	}

	/**
	 * finds and deletes a Chat line by ID
	 */
	public void deleteChatLine(final int par1) {
		Iterator var2 = field_96134_d.iterator();
		ChatLine var3;

		do {
			if (!var2.hasNext()) {
				var2 = chatLines.iterator();

				do {
					if (!var2.hasNext()) {
						return;
					}

					var3 = (ChatLine) var2.next();
				} while (var3.getChatLineID() != par1);

				var2.remove();
				return;
			}

			var3 = (ChatLine) var2.next();
		} while (var3.getChatLineID() != par1);

		var2.remove();
	}

	public int func_96126_f() {
		return GuiNewChat.func_96128_a(mc.gameSettings.chatWidth);
	}

	public int func_96133_g() {
		return GuiNewChat
				.func_96130_b(getChatOpen() ? mc.gameSettings.chatHeightFocused
						: mc.gameSettings.chatHeightUnfocused);
	}

	public float func_96131_h() {
		return mc.gameSettings.chatScale;
	}

	public static final int func_96128_a(final float par0) {
		final short var1 = 320;
		final byte var2 = 40;
		return MathHelper.floor_float(par0 * (var1 - var2) + var2);
	}

	public static final int func_96130_b(final float par0) {
		final short var1 = 180;
		final byte var2 = 20;
		return MathHelper.floor_float(par0 * (var1 - var2) + var2);
	}

	public int func_96127_i() {
		return func_96133_g() / 9;
	}
}
