package net.minecraft.src;

import java.io.IOException;

import org.lwjgl.input.Keyboard;

public class GuiScreenInvite extends GuiScreen {
	private GuiTextField field_96227_a;
	private final McoServer field_96223_b;
	private final GuiScreen field_96224_c;
	private final GuiScreenConfigureWorld field_96222_d;
	private final String field_101016_p = "Could not invite the provided name";
	private String field_96226_p;
	private boolean field_96225_q = false;

	public GuiScreenInvite(final GuiScreen par1GuiScreen,
			final GuiScreenConfigureWorld par2GuiScreenConfigureWorld,
			final McoServer par3McoServer) {
		field_96224_c = par1GuiScreen;
		field_96222_d = par2GuiScreenConfigureWorld;
		field_96223_b = par3McoServer;
	}

	/**
	 * Called from the main game loop to update the screen.
	 */
	@Override
	public void updateScreen() {
		field_96227_a.updateCursorCounter();
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui() {
		final StringTranslate var1 = StringTranslate.getInstance();
		Keyboard.enableRepeatEvents(true);
		buttonList.clear();
		buttonList.add(new GuiButton(0, width / 2 - 100, height / 4 + 96 + 12,
				var1.translateKey("mco.configure.world.buttons.invite")));
		buttonList.add(new GuiButton(1, width / 2 - 100, height / 4 + 120 + 12,
				var1.translateKey("gui.cancel")));
		field_96227_a = new GuiTextField(fontRenderer, width / 2 - 100, 66,
				200, 20);
		field_96227_a.setFocused(true);
	}

	/**
	 * Called when the screen is unloaded. Used to disable keyboard repeat
	 * events
	 */
	@Override
	public void onGuiClosed() {
		Keyboard.enableRepeatEvents(false);
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	@Override
	protected void actionPerformed(final GuiButton par1GuiButton) {
		if (par1GuiButton.enabled) {
			if (par1GuiButton.id == 1) {
				mc.displayGuiScreen(field_96222_d);
			} else if (par1GuiButton.id == 0) {
				final McoClient var2 = new McoClient(mc.session);

				try {
					final McoServer var3 = var2.func_96387_b(
							field_96223_b.field_96408_a,
							field_96227_a.getText());

					if (var3 != null) {
						field_96223_b.field_96402_f = var3.field_96402_f;
						mc.displayGuiScreen(new GuiScreenConfigureWorld(
								field_96224_c, field_96223_b));
					} else {
						func_101015_a(field_101016_p);
					}
				} catch (final ExceptionMcoService var4) {
					func_101015_a(var4.field_96391_b);
				} catch (final IOException var5) {
					func_101015_a(field_101016_p);
				}
			}
		}
	}

	private void func_101015_a(final String par1Str) {
		field_96225_q = true;
		field_96226_p = par1Str;
	}

	/**
	 * Fired when a key is typed. This is the equivalent of
	 * KeyListener.keyTyped(KeyEvent e).
	 */
	@Override
	protected void keyTyped(final char par1, final int par2) {
		field_96227_a.textboxKeyTyped(par1, par2);

		if (par1 == 9) {
			if (field_96227_a.isFocused()) {
				field_96227_a.setFocused(false);
			} else {
				field_96227_a.setFocused(true);
			}
		}

		if (par1 == 13) {
			actionPerformed((GuiButton) buttonList.get(0));
		}
	}

	/**
	 * Called when the mouse is clicked.
	 */
	@Override
	protected void mouseClicked(final int par1, final int par2, final int par3) {
		super.mouseClicked(par1, par2, par3);
		field_96227_a.mouseClicked(par1, par2, par3);
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(final int par1, final int par2, final float par3) {
		final StringTranslate var4 = StringTranslate.getInstance();
		drawDefaultBackground();
		drawCenteredString(fontRenderer, var4.translateKey(""), width / 2, 17,
				16777215);
		drawString(fontRenderer,
				var4.translateKey("mco.configure.world.invite.profile.name"),
				width / 2 - 100, 53, 10526880);

		if (field_96225_q) {
			drawCenteredString(fontRenderer, field_96226_p, width / 2, 100,
					16711680);
		}

		field_96227_a.drawTextBox();
		super.drawScreen(par1, par2, par3);
	}
}
