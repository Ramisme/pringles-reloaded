package net.minecraft.src;

public class EntityCrit2FX extends EntityFX {
	/** Entity that had been hit and done the Critical hit on. */
	private final Entity theEntity;
	private int currentLife;
	private int maximumLife;
	private final String particleName;

	public EntityCrit2FX(final World par1World, final Entity par2Entity) {
		this(par1World, par2Entity, "crit");
	}

	public EntityCrit2FX(final World par1World, final Entity par2Entity,
			final String par3Str) {
		super(par1World, par2Entity.posX, par2Entity.boundingBox.minY
				+ par2Entity.height / 2.0F, par2Entity.posZ,
				par2Entity.motionX, par2Entity.motionY, par2Entity.motionZ);
		currentLife = 0;
		maximumLife = 0;
		theEntity = par2Entity;
		maximumLife = 3;
		particleName = par3Str;
		onUpdate();
	}

	@Override
	public void renderParticle(final Tessellator par1Tessellator,
			final float par2, final float par3, final float par4,
			final float par5, final float par6, final float par7) {
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate() {
		for (int var1 = 0; var1 < 16; ++var1) {
			final double var2 = rand.nextFloat() * 2.0F - 1.0F;
			final double var4 = rand.nextFloat() * 2.0F - 1.0F;
			final double var6 = rand.nextFloat() * 2.0F - 1.0F;

			if (var2 * var2 + var4 * var4 + var6 * var6 <= 1.0D) {
				final double var8 = theEntity.posX + var2 * theEntity.width
						/ 4.0D;
				final double var10 = theEntity.boundingBox.minY
						+ theEntity.height / 2.0F + var4 * theEntity.height
						/ 4.0D;
				final double var12 = theEntity.posZ + var6 * theEntity.width
						/ 4.0D;
				worldObj.spawnParticle(particleName, var8, var10, var12, var2,
						var4 + 0.2D, var6);
			}
		}

		++currentLife;

		if (currentLife >= maximumLife) {
			setDead();
		}
	}

	@Override
	public int getFXLayer() {
		return 3;
	}
}
