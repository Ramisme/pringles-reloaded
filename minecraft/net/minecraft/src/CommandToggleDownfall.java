package net.minecraft.src;

import net.minecraft.server.MinecraftServer;

public class CommandToggleDownfall extends CommandBase {
	@Override
	public String getCommandName() {
		return "toggledownfall";
	}

	/**
	 * Return the required permission level for this command.
	 */
	@Override
	public int getRequiredPermissionLevel() {
		return 2;
	}

	@Override
	public void processCommand(final ICommandSender par1ICommandSender,
			final String[] par2ArrayOfStr) {
		toggleDownfall();
		CommandBase.notifyAdmins(par1ICommandSender,
				"commands.downfall.success", new Object[0]);
	}

	/**
	 * Toggle rain and enable thundering.
	 */
	protected void toggleDownfall() {
		MinecraftServer.getServer().worldServers[0].toggleRain();
		MinecraftServer.getServer().worldServers[0].getWorldInfo()
				.setThundering(true);
	}
}
