package net.minecraft.src;

public class InventoryPlayer implements IInventory {
	/**
	 * An array of 36 item stacks indicating the main player inventory
	 * (including the visible bar).
	 */
	public ItemStack[] mainInventory = new ItemStack[36];

	/** An array of 4 item stacks containing the currently worn armor pieces. */
	public ItemStack[] armorInventory = new ItemStack[4];

	/** The index of the currently held item (0-8). */
	public int currentItem = 0;

	/** The current ItemStack. */
	private ItemStack currentItemStack;

	/** The player whose inventory this is. */
	public EntityPlayer player;
	private ItemStack itemStack;

	/**
	 * Set true whenever the inventory changes. Nothing sets it false so you
	 * will have to write your own code to check it and reset the value.
	 */
	public boolean inventoryChanged = false;

	public InventoryPlayer(final EntityPlayer par1EntityPlayer) {
		player = par1EntityPlayer;
	}

	/**
	 * Returns the item stack currently held by the player.
	 */
	public ItemStack getCurrentItem() {
		return currentItem < 9 && currentItem >= 0 ? mainInventory[currentItem]
				: null;
	}

	/**
	 * Get the size of the player hotbar inventory
	 */
	public static int getHotbarSize() {
		return 9;
	}

	/**
	 * Returns a slot index in main inventory containing a specific itemID
	 */
	private int getInventorySlotContainItem(final int par1) {
		for (int var2 = 0; var2 < mainInventory.length; ++var2) {
			if (mainInventory[var2] != null
					&& mainInventory[var2].itemID == par1) {
				return var2;
			}
		}

		return -1;
	}

	private int getInventorySlotContainItemAndDamage(final int par1,
			final int par2) {
		for (int var3 = 0; var3 < mainInventory.length; ++var3) {
			if (mainInventory[var3] != null
					&& mainInventory[var3].itemID == par1
					&& mainInventory[var3].getItemDamage() == par2) {
				return var3;
			}
		}

		return -1;
	}

	/**
	 * stores an itemstack in the users inventory
	 */
	private int storeItemStack(final ItemStack par1ItemStack) {
		for (int var2 = 0; var2 < mainInventory.length; ++var2) {
			if (mainInventory[var2] != null
					&& mainInventory[var2].itemID == par1ItemStack.itemID
					&& mainInventory[var2].isStackable()
					&& mainInventory[var2].stackSize < mainInventory[var2]
							.getMaxStackSize()
					&& mainInventory[var2].stackSize < getInventoryStackLimit()
					&& (!mainInventory[var2].getHasSubtypes() || mainInventory[var2]
							.getItemDamage() == par1ItemStack.getItemDamage())
					&& ItemStack.areItemStackTagsEqual(mainInventory[var2],
							par1ItemStack)) {
				return var2;
			}
		}

		return -1;
	}

	/**
	 * Returns the first item stack that is empty.
	 */
	public int getFirstEmptyStack() {
		for (int var1 = 0; var1 < mainInventory.length; ++var1) {
			if (mainInventory[var1] == null) {
				return var1;
			}
		}

		return -1;
	}

	/**
	 * Sets a specific itemID as the current item being held (only if it exists
	 * on the hotbar)
	 */
	public void setCurrentItem(final int par1, final int par2,
			final boolean par3, final boolean par4) {
		currentItemStack = getCurrentItem();
		int var7;

		if (par3) {
			var7 = getInventorySlotContainItemAndDamage(par1, par2);
		} else {
			var7 = getInventorySlotContainItem(par1);
		}

		if (var7 >= 0 && var7 < 9) {
			currentItem = var7;
		} else {
			if (par4 && par1 > 0) {
				final int var6 = getFirstEmptyStack();

				if (var6 >= 0 && var6 < 9) {
					currentItem = var6;
				}

				func_70439_a(Item.itemsList[par1], par2);
			}
		}
	}

	/**
	 * Switch the current item to the next one or the previous one
	 */
	public void changeCurrentItem(int par1) {
		if (par1 > 0) {
			par1 = 1;
		}

		if (par1 < 0) {
			par1 = -1;
		}

		for (currentItem -= par1; currentItem < 0; currentItem += 9) {
			;
		}

		while (currentItem >= 9) {
			currentItem -= 9;
		}
	}

	/**
	 * Clear this player's inventory, using the specified ID and metadata as
	 * filters or -1 for no filter.
	 */
	public int clearInventory(final int par1, final int par2) {
		int var3 = 0;
		int var4;
		ItemStack var5;

		for (var4 = 0; var4 < mainInventory.length; ++var4) {
			var5 = mainInventory[var4];

			if (var5 != null && (par1 <= -1 || var5.itemID == par1)
					&& (par2 <= -1 || var5.getItemDamage() == par2)) {
				var3 += var5.stackSize;
				mainInventory[var4] = null;
			}
		}

		for (var4 = 0; var4 < armorInventory.length; ++var4) {
			var5 = armorInventory[var4];

			if (var5 != null && (par1 <= -1 || var5.itemID == par1)
					&& (par2 <= -1 || var5.getItemDamage() == par2)) {
				var3 += var5.stackSize;
				armorInventory[var4] = null;
			}
		}

		return var3;
	}

	public void func_70439_a(final Item par1Item, final int par2) {
		if (par1Item != null) {
			final int var3 = getInventorySlotContainItemAndDamage(
					par1Item.itemID, par2);

			if (var3 >= 0) {
				mainInventory[var3] = mainInventory[currentItem];
			}

			if (currentItemStack != null
					&& currentItemStack.isItemEnchantable()
					&& getInventorySlotContainItemAndDamage(
							currentItemStack.itemID,
							currentItemStack.getItemDamageForDisplay()) == currentItem) {
				return;
			}

			mainInventory[currentItem] = new ItemStack(
					Item.itemsList[par1Item.itemID], 1, par2);
		}
	}

	/**
	 * This function stores as many items of an ItemStack as possible in a
	 * matching slot and returns the quantity of left over items.
	 */
	private int storePartialItemStack(final ItemStack par1ItemStack) {
		final int var2 = par1ItemStack.itemID;
		int var3 = par1ItemStack.stackSize;
		int var4;

		if (par1ItemStack.getMaxStackSize() == 1) {
			var4 = getFirstEmptyStack();

			if (var4 < 0) {
				return var3;
			} else {
				if (mainInventory[var4] == null) {
					mainInventory[var4] = ItemStack
							.copyItemStack(par1ItemStack);
				}

				return 0;
			}
		} else {
			var4 = storeItemStack(par1ItemStack);

			if (var4 < 0) {
				var4 = getFirstEmptyStack();
			}

			if (var4 < 0) {
				return var3;
			} else {
				if (mainInventory[var4] == null) {
					mainInventory[var4] = new ItemStack(var2, 0,
							par1ItemStack.getItemDamage());

					if (par1ItemStack.hasTagCompound()) {
						mainInventory[var4]
								.setTagCompound((NBTTagCompound) par1ItemStack
										.getTagCompound().copy());
					}
				}

				int var5 = var3;

				if (var3 > mainInventory[var4].getMaxStackSize()
						- mainInventory[var4].stackSize) {
					var5 = mainInventory[var4].getMaxStackSize()
							- mainInventory[var4].stackSize;
				}

				if (var5 > getInventoryStackLimit()
						- mainInventory[var4].stackSize) {
					var5 = getInventoryStackLimit()
							- mainInventory[var4].stackSize;
				}

				if (var5 == 0) {
					return var3;
				} else {
					var3 -= var5;
					mainInventory[var4].stackSize += var5;
					mainInventory[var4].animationsToGo = 5;
					return var3;
				}
			}
		}
	}

	/**
	 * Decrement the number of animations remaining. Only called on client side.
	 * This is used to handle the animation of receiving a block.
	 */
	public void decrementAnimations() {
		for (int var1 = 0; var1 < mainInventory.length; ++var1) {
			if (mainInventory[var1] != null) {
				mainInventory[var1].updateAnimation(player.worldObj, player,
						var1, currentItem == var1);
			}
		}
	}

	/**
	 * removed one item of specified itemID from inventory (if it is in a stack,
	 * the stack size will reduce with 1)
	 */
	public boolean consumeInventoryItem(final int par1) {
		final int var2 = getInventorySlotContainItem(par1);

		if (var2 < 0) {
			return false;
		} else {
			if (--mainInventory[var2].stackSize <= 0) {
				mainInventory[var2] = null;
			}

			return true;
		}
	}

	/**
	 * Get if a specifiied item id is inside the inventory.
	 */
	public boolean hasItem(final int par1) {
		final int var2 = getInventorySlotContainItem(par1);
		return var2 >= 0;
	}

	/**
	 * Adds the item stack to the inventory, returns false if it is impossible.
	 */
	public boolean addItemStackToInventory(final ItemStack par1ItemStack) {
		if (par1ItemStack == null) {
			return false;
		} else {
			try {
				int var2;

				if (par1ItemStack.isItemDamaged()) {
					var2 = getFirstEmptyStack();

					if (var2 >= 0) {
						mainInventory[var2] = ItemStack
								.copyItemStack(par1ItemStack);
						mainInventory[var2].animationsToGo = 5;
						par1ItemStack.stackSize = 0;
						return true;
					} else if (player.capabilities.isCreativeMode) {
						par1ItemStack.stackSize = 0;
						return true;
					} else {
						return false;
					}
				} else {
					do {
						var2 = par1ItemStack.stackSize;
						par1ItemStack.stackSize = storePartialItemStack(par1ItemStack);
					} while (par1ItemStack.stackSize > 0
							&& par1ItemStack.stackSize < var2);

					if (par1ItemStack.stackSize == var2
							&& player.capabilities.isCreativeMode) {
						par1ItemStack.stackSize = 0;
						return true;
					} else {
						return par1ItemStack.stackSize < var2;
					}
				}
			} catch (final Throwable var5) {
				final CrashReport var3 = CrashReport.makeCrashReport(var5,
						"Adding item to inventory");
				final CrashReportCategory var4 = var3
						.makeCategory("Item being added");
				var4.addCrashSection("Item ID",
						Integer.valueOf(par1ItemStack.itemID));
				var4.addCrashSection("Item data",
						Integer.valueOf(par1ItemStack.getItemDamage()));
				var4.addCrashSectionCallable("Item name", new CallableItemName(
						this, par1ItemStack));
				throw new ReportedException(var3);
			}
		}
	}

	/**
	 * Removes from an inventory slot (first arg) up to a specified number
	 * (second arg) of items and returns them in a new stack.
	 */
	@Override
	public ItemStack decrStackSize(int par1, final int par2) {
		ItemStack[] var3 = mainInventory;

		if (par1 >= mainInventory.length) {
			var3 = armorInventory;
			par1 -= mainInventory.length;
		}

		if (var3[par1] != null) {
			ItemStack var4;

			if (var3[par1].stackSize <= par2) {
				var4 = var3[par1];
				var3[par1] = null;
				return var4;
			} else {
				var4 = var3[par1].splitStack(par2);

				if (var3[par1].stackSize == 0) {
					var3[par1] = null;
				}

				return var4;
			}
		} else {
			return null;
		}
	}

	/**
	 * When some containers are closed they call this on each slot, then drop
	 * whatever it returns as an EntityItem - like when you close a workbench
	 * GUI.
	 */
	@Override
	public ItemStack getStackInSlotOnClosing(int par1) {
		ItemStack[] var2 = mainInventory;

		if (par1 >= mainInventory.length) {
			var2 = armorInventory;
			par1 -= mainInventory.length;
		}

		if (var2[par1] != null) {
			final ItemStack var3 = var2[par1];
			var2[par1] = null;
			return var3;
		} else {
			return null;
		}
	}

	/**
	 * Sets the given item stack to the specified slot in the inventory (can be
	 * crafting or armor sections).
	 */
	@Override
	public void setInventorySlotContents(int par1, final ItemStack par2ItemStack) {
		ItemStack[] var3 = mainInventory;

		if (par1 >= var3.length) {
			par1 -= var3.length;
			var3 = armorInventory;
		}

		var3[par1] = par2ItemStack;
	}

	/**
	 * Gets the strength of the current item (tool) against the specified block,
	 * 1.0f if not holding anything.
	 */
	public float getStrVsBlock(final Block par1Block) {
		float var2 = 1.0F;

		if (mainInventory[currentItem] != null) {
			var2 *= mainInventory[currentItem].getStrVsBlock(par1Block);
		}

		return var2;
	}

	/**
	 * Writes the inventory out as a list of compound tags. This is where the
	 * slot indices are used (+100 for armor, +80 for crafting).
	 */
	public NBTTagList writeToNBT(final NBTTagList par1NBTTagList) {
		int var2;
		NBTTagCompound var3;

		for (var2 = 0; var2 < mainInventory.length; ++var2) {
			if (mainInventory[var2] != null) {
				var3 = new NBTTagCompound();
				var3.setByte("Slot", (byte) var2);
				mainInventory[var2].writeToNBT(var3);
				par1NBTTagList.appendTag(var3);
			}
		}

		for (var2 = 0; var2 < armorInventory.length; ++var2) {
			if (armorInventory[var2] != null) {
				var3 = new NBTTagCompound();
				var3.setByte("Slot", (byte) (var2 + 100));
				armorInventory[var2].writeToNBT(var3);
				par1NBTTagList.appendTag(var3);
			}
		}

		return par1NBTTagList;
	}

	/**
	 * Reads from the given tag list and fills the slots in the inventory with
	 * the correct items.
	 */
	public void readFromNBT(final NBTTagList par1NBTTagList) {
		mainInventory = new ItemStack[36];
		armorInventory = new ItemStack[4];

		for (int var2 = 0; var2 < par1NBTTagList.tagCount(); ++var2) {
			final NBTTagCompound var3 = (NBTTagCompound) par1NBTTagList
					.tagAt(var2);
			final int var4 = var3.getByte("Slot") & 255;
			final ItemStack var5 = ItemStack.loadItemStackFromNBT(var3);

			if (var5 != null) {
				if (var4 >= 0 && var4 < mainInventory.length) {
					mainInventory[var4] = var5;
				}

				if (var4 >= 100 && var4 < armorInventory.length + 100) {
					armorInventory[var4 - 100] = var5;
				}
			}
		}
	}

	/**
	 * Returns the number of slots in the inventory.
	 */
	@Override
	public int getSizeInventory() {
		return mainInventory.length + 4;
	}

	/**
	 * Returns the stack in slot i
	 */
	@Override
	public ItemStack getStackInSlot(int par1) {
		ItemStack[] var2 = mainInventory;

		if (par1 >= var2.length) {
			par1 -= var2.length;
			var2 = armorInventory;
		}

		return var2[par1];
	}

	/**
	 * Returns the name of the inventory.
	 */
	@Override
	public String getInvName() {
		return "container.inventory";
	}

	/**
	 * If this returns false, the inventory name will be used as an unlocalized
	 * name, and translated into the player's language. Otherwise it will be
	 * used directly.
	 */
	@Override
	public boolean isInvNameLocalized() {
		return false;
	}

	/**
	 * Returns the maximum stack size for a inventory slot. Seems to always be
	 * 64, possibly will be extended. *Isn't this more of a set than a get?*
	 */
	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	/**
	 * Return damage vs an entity done by the current held weapon, or 1 if
	 * nothing is held
	 */
	public int getDamageVsEntity(final Entity par1Entity) {
		final ItemStack var2 = getStackInSlot(currentItem);
		return var2 != null ? var2.getDamageVsEntity(par1Entity) : 1;
	}

	/**
	 * Returns whether the current item (tool) can harvest from the specified
	 * block (actually get a result).
	 */
	public boolean canHarvestBlock(final Block par1Block) {
		if (par1Block.blockMaterial.isToolNotRequired()) {
			return true;
		} else {
			final ItemStack var2 = getStackInSlot(currentItem);
			return var2 != null ? var2.canHarvestBlock(par1Block) : false;
		}
	}

	/**
	 * returns a player armor item (as itemstack) contained in specified armor
	 * slot.
	 */
	public ItemStack armorItemInSlot(final int par1) {
		return armorInventory[par1];
	}

	/**
	 * Based on the damage values and maximum damage values of each armor item,
	 * returns the current armor value.
	 */
	public int getTotalArmorValue() {
		int var1 = 0;

		for (final ItemStack element : armorInventory) {
			if (element != null && element.getItem() instanceof ItemArmor) {
				final int var3 = ((ItemArmor) element.getItem()).damageReduceAmount;
				var1 += var3;
			}
		}

		return var1;
	}

	/**
	 * Damages armor in each slot by the specified amount.
	 */
	public void damageArmor(int par1) {
		par1 /= 4;

		if (par1 < 1) {
			par1 = 1;
		}

		for (int var2 = 0; var2 < armorInventory.length; ++var2) {
			if (armorInventory[var2] != null
					&& armorInventory[var2].getItem() instanceof ItemArmor) {
				armorInventory[var2].damageItem(par1, player);

				if (armorInventory[var2].stackSize == 0) {
					armorInventory[var2] = null;
				}
			}
		}
	}

	/**
	 * Drop all armor and main inventory items.
	 */
	public void dropAllItems() {
		int var1;

		for (var1 = 0; var1 < mainInventory.length; ++var1) {
			if (mainInventory[var1] != null) {
				player.dropPlayerItemWithRandomChoice(mainInventory[var1], true);
				mainInventory[var1] = null;
			}
		}

		for (var1 = 0; var1 < armorInventory.length; ++var1) {
			if (armorInventory[var1] != null) {
				player.dropPlayerItemWithRandomChoice(armorInventory[var1],
						true);
				armorInventory[var1] = null;
			}
		}
	}

	/**
	 * Called when an the contents of an Inventory change, usually
	 */
	@Override
	public void onInventoryChanged() {
		inventoryChanged = true;
	}

	public void setItemStack(final ItemStack par1ItemStack) {
		itemStack = par1ItemStack;
	}

	public ItemStack getItemStack() {
		return itemStack;
	}

	/**
	 * Do not make give this method the name canInteractWith because it clashes
	 * with Container
	 */
	@Override
	public boolean isUseableByPlayer(final EntityPlayer par1EntityPlayer) {
		return player.isDead ? false : par1EntityPlayer
				.getDistanceSqToEntity(player) <= 64.0D;
	}

	/**
	 * Returns true if the specified ItemStack exists in the inventory.
	 */
	public boolean hasItemStack(final ItemStack par1ItemStack) {
		int var2;

		for (var2 = 0; var2 < armorInventory.length; ++var2) {
			if (armorInventory[var2] != null
					&& armorInventory[var2].isItemEqual(par1ItemStack)) {
				return true;
			}
		}

		for (var2 = 0; var2 < mainInventory.length; ++var2) {
			if (mainInventory[var2] != null
					&& mainInventory[var2].isItemEqual(par1ItemStack)) {
				return true;
			}
		}

		return false;
	}

	@Override
	public void openChest() {
	}

	@Override
	public void closeChest() {
	}

	/**
	 * Returns true if automation is allowed to insert the given stack (ignoring
	 * stack size) into the given slot.
	 */
	@Override
	public boolean isStackValidForSlot(final int par1,
			final ItemStack par2ItemStack) {
		return true;
	}

	/**
	 * Copy the ItemStack contents from another InventoryPlayer instance
	 */
	public void copyInventory(final InventoryPlayer par1InventoryPlayer) {
		int var2;

		for (var2 = 0; var2 < mainInventory.length; ++var2) {
			mainInventory[var2] = ItemStack
					.copyItemStack(par1InventoryPlayer.mainInventory[var2]);
		}

		for (var2 = 0; var2 < armorInventory.length; ++var2) {
			armorInventory[var2] = ItemStack
					.copyItemStack(par1InventoryPlayer.armorInventory[var2]);
		}

		currentItem = par1InventoryPlayer.currentItem;
	}
}
