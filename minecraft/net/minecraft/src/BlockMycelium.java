package net.minecraft.src;

import java.util.Random;

public class BlockMycelium extends Block {
	private Icon field_94422_a;
	private Icon field_94421_b;

	protected BlockMycelium(final int par1) {
		super(par1, Material.grass);
		setTickRandomly(true);
		setCreativeTab(CreativeTabs.tabBlock);
	}

	/**
	 * From the specified side and block metadata retrieves the blocks texture.
	 * Args: side, metadata
	 */
	@Override
	public Icon getIcon(final int par1, final int par2) {
		return par1 == 1 ? field_94422_a : par1 == 0 ? Block.dirt
				.getBlockTextureFromSide(par1) : blockIcon;
	}

	/**
	 * Retrieves the block texture to use based on the display side. Args:
	 * iBlockAccess, x, y, z, side
	 */
	@Override
	public Icon getBlockTexture(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		if (par5 == 1) {
			return field_94422_a;
		} else if (par5 == 0) {
			return Block.dirt.getBlockTextureFromSide(par5);
		} else {
			final Material var6 = par1IBlockAccess.getBlockMaterial(par2,
					par3 + 1, par4);
			return var6 != Material.snow && var6 != Material.craftedSnow ? blockIcon
					: field_94421_b;
		}
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		blockIcon = par1IconRegister.registerIcon("mycel_side");
		field_94422_a = par1IconRegister.registerIcon("mycel_top");
		field_94421_b = par1IconRegister.registerIcon("snow_side");
	}

	/**
	 * Ticks the block if it's been scheduled
	 */
	@Override
	public void updateTick(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		if (!par1World.isRemote) {
			if (par1World.getBlockLightValue(par2, par3 + 1, par4) < 4
					&& Block.lightOpacity[par1World.getBlockId(par2, par3 + 1,
							par4)] > 2) {
				par1World.setBlock(par2, par3, par4, Block.dirt.blockID);
			} else if (par1World.getBlockLightValue(par2, par3 + 1, par4) >= 9) {
				for (int var6 = 0; var6 < 4; ++var6) {
					final int var7 = par2 + par5Random.nextInt(3) - 1;
					final int var8 = par3 + par5Random.nextInt(5) - 3;
					final int var9 = par4 + par5Random.nextInt(3) - 1;
					final int var10 = par1World
							.getBlockId(var7, var8 + 1, var9);

					if (par1World.getBlockId(var7, var8, var9) == Block.dirt.blockID
							&& par1World.getBlockLightValue(var7, var8 + 1,
									var9) >= 4
							&& Block.lightOpacity[var10] <= 2) {
						par1World.setBlock(var7, var8, var9, blockID);
					}
				}
			}
		}
	}

	/**
	 * A randomly called display update to be able to add particles or other
	 * items for display
	 */
	@Override
	public void randomDisplayTick(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		super.randomDisplayTick(par1World, par2, par3, par4, par5Random);

		if (par5Random.nextInt(10) == 0) {
			par1World.spawnParticle("townaura", par2 + par5Random.nextFloat(),
					par3 + 1.1F, par4 + par5Random.nextFloat(), 0.0D, 0.0D,
					0.0D);
		}
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return Block.dirt.idDropped(0, par2Random, par3);
	}
}
