package net.minecraft.src;

import java.util.Iterator;

import org.lwjgl.opengl.GL11;

public class GuiGameOver extends GuiScreen {
	/**
	 * The cooldown timer for the buttons, increases every tick and enables all
	 * buttons when reaching 20.
	 */
	private int cooldownTimer;

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui() {
		buttonList.clear();

		if (mc.theWorld.getWorldInfo().isHardcoreModeEnabled()) {
			if (mc.isIntegratedServerRunning()) {
				buttonList.add(new GuiButton(1, width / 2 - 100,
						height / 4 + 96, StatCollector
								.translateToLocal("deathScreen.deleteWorld")));
			} else {
				buttonList.add(new GuiButton(1, width / 2 - 100,
						height / 4 + 96, StatCollector
								.translateToLocal("deathScreen.leaveServer")));
			}
		} else {
			buttonList.add(new GuiButton(1, width / 2 - 100, height / 4 + 72,
					StatCollector.translateToLocal("deathScreen.respawn")));
			buttonList.add(new GuiButton(2, width / 2 - 100, height / 4 + 96,
					StatCollector.translateToLocal("deathScreen.titleScreen")));

			if (mc.session == null) {
				((GuiButton) buttonList.get(1)).enabled = false;
			}
		}

		GuiButton var2;

		for (final Iterator var1 = buttonList.iterator(); var1.hasNext(); var2.enabled = false) {
			var2 = (GuiButton) var1.next();
		}
	}

	/**
	 * Fired when a key is typed. This is the equivalent of
	 * KeyListener.keyTyped(KeyEvent e).
	 */
	@Override
	protected void keyTyped(final char par1, final int par2) {
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	@Override
	protected void actionPerformed(final GuiButton par1GuiButton) {
		switch (par1GuiButton.id) {
		case 1:
			mc.thePlayer.respawnPlayer();
			mc.displayGuiScreen((GuiScreen) null);
			break;

		case 2:
			mc.theWorld.sendQuittingDisconnectingPacket();
			mc.loadWorld((WorldClient) null);
			mc.displayGuiScreen(new GuiMainMenu());
		}
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(final int par1, final int par2, final float par3) {
		drawGradientRect(0, 0, width, height, 1615855616, -1602211792);
		GL11.glPushMatrix();
		GL11.glScalef(2.0F, 2.0F, 2.0F);
		final boolean var4 = mc.theWorld.getWorldInfo().isHardcoreModeEnabled();
		final String var5 = var4 ? StatCollector
				.translateToLocal("deathScreen.title.hardcore") : StatCollector
				.translateToLocal("deathScreen.title");
		drawCenteredString(fontRenderer, var5, width / 2 / 2, 30, 16777215);
		GL11.glPopMatrix();

		if (var4) {
			drawCenteredString(fontRenderer,
					StatCollector.translateToLocal("deathScreen.hardcoreInfo"),
					width / 2, 144, 16777215);
		}

		drawCenteredString(fontRenderer,
				StatCollector.translateToLocal("deathScreen.score") + ": "
						+ EnumChatFormatting.YELLOW + mc.thePlayer.getScore(),
				width / 2, 100, 16777215);
		super.drawScreen(par1, par2, par3);
	}

	/**
	 * Returns true if this GUI should pause the game when it is displayed in
	 * single-player
	 */
	@Override
	public boolean doesGuiPauseGame() {
		return false;
	}

	/**
	 * Called from the main game loop to update the screen.
	 */
	@Override
	public void updateScreen() {
		super.updateScreen();
		++cooldownTimer;
		GuiButton var2;

		if (cooldownTimer == 20) {
			for (final Iterator var1 = buttonList.iterator(); var1.hasNext(); var2.enabled = true) {
				var2 = (GuiButton) var1.next();
			}
		}
	}
}
