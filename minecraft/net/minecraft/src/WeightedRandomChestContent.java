package net.minecraft.src;

import java.util.Random;

public class WeightedRandomChestContent extends WeightedRandomItem {
	/** The Item/Block ID to generate in the Chest. */
	private ItemStack theItemId = null;

	/** The minimum chance of item generating. */
	private final int theMinimumChanceToGenerateItem;

	/** The maximum chance of item generating. */
	private final int theMaximumChanceToGenerateItem;

	public WeightedRandomChestContent(final int par1, final int par2,
			final int par3, final int par4, final int par5) {
		super(par5);
		theItemId = new ItemStack(par1, 1, par2);
		theMinimumChanceToGenerateItem = par3;
		theMaximumChanceToGenerateItem = par4;
	}

	public WeightedRandomChestContent(final ItemStack par1ItemStack,
			final int par2, final int par3, final int par4) {
		super(par4);
		theItemId = par1ItemStack;
		theMinimumChanceToGenerateItem = par2;
		theMaximumChanceToGenerateItem = par3;
	}

	/**
	 * Generates the Chest contents.
	 */
	public static void generateChestContents(
			final Random par0Random,
			final WeightedRandomChestContent[] par1ArrayOfWeightedRandomChestContent,
			final IInventory par2IInventory, final int par3) {
		for (int var4 = 0; var4 < par3; ++var4) {
			final WeightedRandomChestContent var5 = (WeightedRandomChestContent) WeightedRandom
					.getRandomItem(par0Random,
							par1ArrayOfWeightedRandomChestContent);
			final int var6 = var5.theMinimumChanceToGenerateItem
					+ par0Random.nextInt(var5.theMaximumChanceToGenerateItem
							- var5.theMinimumChanceToGenerateItem + 1);

			if (var5.theItemId.getMaxStackSize() >= var6) {
				final ItemStack var7 = var5.theItemId.copy();
				var7.stackSize = var6;
				par2IInventory.setInventorySlotContents(
						par0Random.nextInt(par2IInventory.getSizeInventory()),
						var7);
			} else {
				for (int var9 = 0; var9 < var6; ++var9) {
					final ItemStack var8 = var5.theItemId.copy();
					var8.stackSize = 1;
					par2IInventory.setInventorySlotContents(par0Random
							.nextInt(par2IInventory.getSizeInventory()), var8);
				}
			}
		}
	}

	/**
	 * Generates the Dispenser contents.
	 */
	public static void generateDispenserContents(
			final Random par0Random,
			final WeightedRandomChestContent[] par1ArrayOfWeightedRandomChestContent,
			final TileEntityDispenser par2TileEntityDispenser, final int par3) {
		for (int var4 = 0; var4 < par3; ++var4) {
			final WeightedRandomChestContent var5 = (WeightedRandomChestContent) WeightedRandom
					.getRandomItem(par0Random,
							par1ArrayOfWeightedRandomChestContent);
			final int var6 = var5.theMinimumChanceToGenerateItem
					+ par0Random.nextInt(var5.theMaximumChanceToGenerateItem
							- var5.theMinimumChanceToGenerateItem + 1);

			if (var5.theItemId.getMaxStackSize() >= var6) {
				final ItemStack var7 = var5.theItemId.copy();
				var7.stackSize = var6;
				par2TileEntityDispenser.setInventorySlotContents(par0Random
						.nextInt(par2TileEntityDispenser.getSizeInventory()),
						var7);
			} else {
				for (int var9 = 0; var9 < var6; ++var9) {
					final ItemStack var8 = var5.theItemId.copy();
					var8.stackSize = 1;
					par2TileEntityDispenser.setInventorySlotContents(
							par0Random.nextInt(par2TileEntityDispenser
									.getSizeInventory()), var8);
				}
			}
		}
	}

	public static WeightedRandomChestContent[] func_92080_a(
			final WeightedRandomChestContent[] par0ArrayOfWeightedRandomChestContent,
			final WeightedRandomChestContent... par1ArrayOfWeightedRandomChestContent) {
		final WeightedRandomChestContent[] var2 = new WeightedRandomChestContent[par0ArrayOfWeightedRandomChestContent.length
				+ par1ArrayOfWeightedRandomChestContent.length];
		int var3 = 0;

		for (final WeightedRandomChestContent element : par0ArrayOfWeightedRandomChestContent) {
			var2[var3++] = element;
		}

		final WeightedRandomChestContent[] var8 = par1ArrayOfWeightedRandomChestContent;
		final int var5 = par1ArrayOfWeightedRandomChestContent.length;

		for (int var6 = 0; var6 < var5; ++var6) {
			final WeightedRandomChestContent var7 = var8[var6];
			var2[var3++] = var7;
		}

		return var2;
	}
}
