package net.minecraft.src;

public class ModelVillager extends ModelBase {
	/** The head box of the VillagerModel */
	public ModelRenderer villagerHead;

	/** The body of the VillagerModel */
	public ModelRenderer villagerBody;

	/** The arms of the VillagerModel */
	public ModelRenderer villagerArms;

	/** The right leg of the VillagerModel */
	public ModelRenderer rightVillagerLeg;

	/** The left leg of the VillagerModel */
	public ModelRenderer leftVillagerLeg;
	public ModelRenderer field_82898_f;

	public ModelVillager(final float par1) {
		this(par1, 0.0F, 64, 64);
	}

	public ModelVillager(final float par1, final float par2, final int par3,
			final int par4) {
		villagerHead = new ModelRenderer(this).setTextureSize(par3, par4);
		villagerHead.setRotationPoint(0.0F, 0.0F + par2, 0.0F);
		villagerHead.setTextureOffset(0, 0).addBox(-4.0F, -10.0F, -4.0F, 8, 10,
				8, par1);
		field_82898_f = new ModelRenderer(this).setTextureSize(par3, par4);
		field_82898_f.setRotationPoint(0.0F, par2 - 2.0F, 0.0F);
		field_82898_f.setTextureOffset(24, 0).addBox(-1.0F, -1.0F, -6.0F, 2, 4,
				2, par1);
		villagerHead.addChild(field_82898_f);
		villagerBody = new ModelRenderer(this).setTextureSize(par3, par4);
		villagerBody.setRotationPoint(0.0F, 0.0F + par2, 0.0F);
		villagerBody.setTextureOffset(16, 20).addBox(-4.0F, 0.0F, -3.0F, 8, 12,
				6, par1);
		villagerBody.setTextureOffset(0, 38).addBox(-4.0F, 0.0F, -3.0F, 8, 18,
				6, par1 + 0.5F);
		villagerArms = new ModelRenderer(this).setTextureSize(par3, par4);
		villagerArms.setRotationPoint(0.0F, 0.0F + par2 + 2.0F, 0.0F);
		villagerArms.setTextureOffset(44, 22).addBox(-8.0F, -2.0F, -2.0F, 4, 8,
				4, par1);
		villagerArms.setTextureOffset(44, 22).addBox(4.0F, -2.0F, -2.0F, 4, 8,
				4, par1);
		villagerArms.setTextureOffset(40, 38).addBox(-4.0F, 2.0F, -2.0F, 8, 4,
				4, par1);
		rightVillagerLeg = new ModelRenderer(this, 0, 22).setTextureSize(par3,
				par4);
		rightVillagerLeg.setRotationPoint(-2.0F, 12.0F + par2, 0.0F);
		rightVillagerLeg.addBox(-2.0F, 0.0F, -2.0F, 4, 12, 4, par1);
		leftVillagerLeg = new ModelRenderer(this, 0, 22).setTextureSize(par3,
				par4);
		leftVillagerLeg.mirror = true;
		leftVillagerLeg.setRotationPoint(2.0F, 12.0F + par2, 0.0F);
		leftVillagerLeg.addBox(-2.0F, 0.0F, -2.0F, 4, 12, 4, par1);
	}

	/**
	 * Sets the models various rotation angles then renders the model.
	 */
	@Override
	public void render(final Entity par1Entity, final float par2,
			final float par3, final float par4, final float par5,
			final float par6, final float par7) {
		setRotationAngles(par2, par3, par4, par5, par6, par7, par1Entity);
		villagerHead.render(par7);
		villagerBody.render(par7);
		rightVillagerLeg.render(par7);
		leftVillagerLeg.render(par7);
		villagerArms.render(par7);
	}

	/**
	 * Sets the model's various rotation angles. For bipeds, par1 and par2 are
	 * used for animating the movement of arms and legs, where par1 represents
	 * the time(so that arms and legs swing back and forth) and par2 represents
	 * how "far" arms and legs can swing at most.
	 */
	@Override
	public void setRotationAngles(final float par1, final float par2,
			final float par3, final float par4, final float par5,
			final float par6, final Entity par7Entity) {
		villagerHead.rotateAngleY = par4 / (180F / (float) Math.PI);
		villagerHead.rotateAngleX = par5 / (180F / (float) Math.PI);
		villagerArms.rotationPointY = 3.0F;
		villagerArms.rotationPointZ = -1.0F;
		villagerArms.rotateAngleX = -0.75F;
		rightVillagerLeg.rotateAngleX = MathHelper.cos(par1 * 0.6662F) * 1.4F
				* par2 * 0.5F;
		leftVillagerLeg.rotateAngleX = MathHelper.cos(par1 * 0.6662F
				+ (float) Math.PI)
				* 1.4F * par2 * 0.5F;
		rightVillagerLeg.rotateAngleY = 0.0F;
		leftVillagerLeg.rotateAngleY = 0.0F;
	}
}
