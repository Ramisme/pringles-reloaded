package net.minecraft.src;

import net.minecraft.client.Minecraft;

import org.lwjgl.opengl.GL11;

public class GuiButton extends Gui {
	/** Button width in pixels */
	protected int width;

	/** Button height in pixels */
	protected int height;

	/** The x position of this control. */
	public int xPosition;

	/** The y position of this control. */
	public int yPosition;

	/** The string displayed on this control. */
	public String displayString;

	/** ID for this control. */
	public int id;

	/** True if this control is enabled, false to disable. */
	public boolean enabled;

	/** Hides the button completely if false. */
	public boolean drawButton;
	protected boolean field_82253_i;

	public GuiButton(final int par1, final int par2, final int par3,
			final String par4Str) {
		this(par1, par2, par3, 200, 20, par4Str);
	}

	public GuiButton(final int par1, final int par2, final int par3,
			final int par4, final int par5, final String par6Str) {
		width = 200;
		height = 20;
		enabled = true;
		drawButton = true;
		id = par1;
		xPosition = par2;
		yPosition = par3;
		width = par4;
		height = par5;
		displayString = par6Str;
	}

	/**
	 * Returns 0 if the button is disabled, 1 if the mouse is NOT hovering over
	 * this button and 2 if it IS hovering over this button.
	 */
	protected int getHoverState(final boolean par1) {
		byte var2 = 1;

		if (!enabled) {
			var2 = 0;
		} else if (par1) {
			var2 = 2;
		}

		return var2;
	}

	/**
	 * Draws this button to the screen.
	 */
	public void drawButton(final Minecraft par1Minecraft, final int par2,
			final int par3) {
		if (drawButton) {
			final FontRenderer var4 = par1Minecraft.fontRenderer;
			par1Minecraft.renderEngine.bindTexture("/gui/gui.png");
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			field_82253_i = par2 >= xPosition && par3 >= yPosition
					&& par2 < xPosition + width && par3 < yPosition + height;
			final int var5 = getHoverState(field_82253_i);
			drawTexturedModalRect(xPosition, yPosition, 0, 46 + var5 * 20,
					width / 2, height);
			drawTexturedModalRect(xPosition + width / 2, yPosition,
					200 - width / 2, 46 + var5 * 20, width / 2, height);
			mouseDragged(par1Minecraft, par2, par3);
			int var6 = 14737632;

			if (!enabled) {
				var6 = -6250336;
			} else if (field_82253_i) {
				var6 = 16777120;
			}

			drawCenteredString(var4, displayString, xPosition + width / 2,
					yPosition + (height - 8) / 2, var6);
		}
	}

	/**
	 * Fired when the mouse button is dragged. Equivalent of
	 * MouseListener.mouseDragged(MouseEvent e).
	 */
	protected void mouseDragged(final Minecraft par1Minecraft, final int par2,
			final int par3) {
	}

	/**
	 * Fired when the mouse button is released. Equivalent of
	 * MouseListener.mouseReleased(MouseEvent e).
	 */
	public void mouseReleased(final int par1, final int par2) {
	}

	/**
	 * Returns true if the mouse has been pressed on this control. Equivalent of
	 * MouseListener.mousePressed(MouseEvent e).
	 */
	public boolean mousePressed(final Minecraft par1Minecraft, final int par2,
			final int par3) {
		return enabled && drawButton && par2 >= xPosition && par3 >= yPosition
				&& par2 < xPosition + width && par3 < yPosition + height;
	}

	public boolean func_82252_a() {
		return field_82253_i;
	}

	public void func_82251_b(final int par1, final int par2) {
	}
}
