package net.minecraft.src;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public final class ItemStack {
	/** Size of the stack. */
	public int stackSize;

	/**
	 * Number of animation frames to go when receiving an item (by walking into
	 * it, for example).
	 */
	public int animationsToGo;

	/** ID of the item. */
	public int itemID;

	/**
	 * A NBTTagMap containing data about an ItemStack. Can only be used for non
	 * stackable items
	 */
	public NBTTagCompound stackTagCompound;

	/** Damage dealt to the item or number of use. Raise when using items. */
	private int itemDamage;

	/** Item frame this stack is on, or null if not on an item frame. */
	private EntityItemFrame itemFrame;

	public ItemStack(final Block par1Block) {
		this(par1Block, 1);
	}

	public ItemStack(final Block par1Block, final int par2) {
		this(par1Block.blockID, par2, 0);
	}

	public ItemStack(final Block par1Block, final int par2, final int par3) {
		this(par1Block.blockID, par2, par3);
	}

	public ItemStack(final Item par1Item) {
		this(par1Item.itemID, 1, 0);
	}

	public ItemStack(final Item par1Item, final int par2) {
		this(par1Item.itemID, par2, 0);
	}

	public ItemStack(final Item par1Item, final int par2, final int par3) {
		this(par1Item.itemID, par2, par3);
	}

	public ItemStack(final int par1, final int par2, final int par3) {
		stackSize = 0;
		itemFrame = null;
		itemID = par1;
		stackSize = par2;
		itemDamage = par3;

		if (itemDamage < 0) {
			itemDamage = 0;
		}
	}

	public static ItemStack loadItemStackFromNBT(
			final NBTTagCompound par0NBTTagCompound) {
		final ItemStack var1 = new ItemStack();
		var1.readFromNBT(par0NBTTagCompound);
		return var1.getItem() != null ? var1 : null;
	}

	private ItemStack() {
		stackSize = 0;
		itemFrame = null;
	}

	/**
	 * Remove the argument from the stack size. Return a new stack object with
	 * argument size.
	 */
	public ItemStack splitStack(final int par1) {
		final ItemStack var2 = new ItemStack(itemID, par1, itemDamage);

		if (stackTagCompound != null) {
			var2.stackTagCompound = (NBTTagCompound) stackTagCompound.copy();
		}

		stackSize -= par1;
		return var2;
	}

	/**
	 * Returns the object corresponding to the stack.
	 */
	public Item getItem() {
		return Item.itemsList[itemID];
	}

	/**
	 * Returns the icon index of the current stack.
	 */
	public Icon getIconIndex() {
		return getItem().getIconIndex(this);
	}

	public int getItemSpriteNumber() {
		return getItem().getSpriteNumber();
	}

	public boolean tryPlaceItemIntoWorld(final EntityPlayer par1EntityPlayer,
			final World par2World, final int par3, final int par4,
			final int par5, final int par6, final float par7, final float par8,
			final float par9) {
		final boolean var10 = getItem().onItemUse(this, par1EntityPlayer,
				par2World, par3, par4, par5, par6, par7, par8, par9);

		if (var10) {
			par1EntityPlayer.addStat(StatList.objectUseStats[itemID], 1);
		}

		return var10;
	}

	/**
	 * Returns the strength of the stack against a given block.
	 */
	public float getStrVsBlock(final Block par1Block) {
		return getItem().getStrVsBlock(this, par1Block);
	}

	/**
	 * Called whenever this item stack is equipped and right clicked. Returns
	 * the new item stack to put in the position where this item is. Args:
	 * world, player
	 */
	public ItemStack useItemRightClick(final World par1World,
			final EntityPlayer par2EntityPlayer) {
		return getItem().onItemRightClick(this, par1World, par2EntityPlayer);
	}

	public ItemStack onFoodEaten(final World par1World,
			final EntityPlayer par2EntityPlayer) {
		return getItem().onEaten(this, par1World, par2EntityPlayer);
	}

	/**
	 * Write the stack fields to a NBT object. Return the new NBT object.
	 */
	public NBTTagCompound writeToNBT(final NBTTagCompound par1NBTTagCompound) {
		par1NBTTagCompound.setShort("id", (short) itemID);
		par1NBTTagCompound.setByte("Count", (byte) stackSize);
		par1NBTTagCompound.setShort("Damage", (short) itemDamage);

		if (stackTagCompound != null) {
			par1NBTTagCompound.setTag("tag", stackTagCompound);
		}

		return par1NBTTagCompound;
	}

	/**
	 * Read the stack fields from a NBT object.
	 */
	public void readFromNBT(final NBTTagCompound par1NBTTagCompound) {
		itemID = par1NBTTagCompound.getShort("id");
		stackSize = par1NBTTagCompound.getByte("Count");
		itemDamage = par1NBTTagCompound.getShort("Damage");

		if (itemDamage < 0) {
			itemDamage = 0;
		}

		if (par1NBTTagCompound.hasKey("tag")) {
			stackTagCompound = par1NBTTagCompound.getCompoundTag("tag");
		}
	}

	/**
	 * Returns maximum size of the stack.
	 */
	public int getMaxStackSize() {
		return getItem().getItemStackLimit();
	}

	/**
	 * Returns true if the ItemStack can hold 2 or more units of the item.
	 */
	public boolean isStackable() {
		return getMaxStackSize() > 1
				&& (!isItemStackDamageable() || !isItemDamaged());
	}

	/**
	 * true if this itemStack is damageable
	 */
	public boolean isItemStackDamageable() {
		return Item.itemsList[itemID].getMaxDamage() > 0;
	}

	public boolean getHasSubtypes() {
		return Item.itemsList[itemID].getHasSubtypes();
	}

	/**
	 * returns true when a damageable item is damaged
	 */
	public boolean isItemDamaged() {
		return isItemStackDamageable() && itemDamage > 0;
	}

	/**
	 * gets the damage of an itemstack, for displaying purposes
	 */
	public int getItemDamageForDisplay() {
		return itemDamage;
	}

	/**
	 * gets the damage of an itemstack
	 */
	public int getItemDamage() {
		return itemDamage;
	}

	/**
	 * Sets the item damage of the ItemStack.
	 */
	public void setItemDamage(final int par1) {
		itemDamage = par1;

		if (itemDamage < 0) {
			itemDamage = 0;
		}
	}

	/**
	 * Returns the max damage an item in the stack can take.
	 */
	public int getMaxDamage() {
		return Item.itemsList[itemID].getMaxDamage();
	}

	/**
	 * Attempts to damage the ItemStack with par1 amount of damage, If the
	 * ItemStack has the Unbreaking enchantment there is a chance for each point
	 * of damage to be negated. Returns true if it takes more damage than
	 * getMaxDamage(). Returns false otherwise or if the ItemStack can't be
	 * damaged or if all points of damage are negated.
	 */
	public boolean attemptDamageItem(int par1, final Random par2Random) {
		if (!isItemStackDamageable()) {
			return false;
		} else {
			if (par1 > 0) {
				final int var3 = EnchantmentHelper.getEnchantmentLevel(
						Enchantment.unbreaking.effectId, this);
				int var4 = 0;

				for (int var5 = 0; var3 > 0 && var5 < par1; ++var5) {
					if (EnchantmentDurability.negateDamage(this, var3,
							par2Random)) {
						++var4;
					}
				}

				par1 -= var4;

				if (par1 <= 0) {
					return false;
				}
			}

			itemDamage += par1;
			return itemDamage > getMaxDamage();
		}
	}

	/**
	 * Damages the item in the ItemStack
	 */
	public void damageItem(final int par1, final EntityLiving par2EntityLiving) {
		if (!(par2EntityLiving instanceof EntityPlayer)
				|| !((EntityPlayer) par2EntityLiving).capabilities.isCreativeMode) {
			if (isItemStackDamageable()) {
				if (attemptDamageItem(par1, par2EntityLiving.getRNG())) {
					par2EntityLiving.renderBrokenItemStack(this);

					if (par2EntityLiving instanceof EntityPlayer) {
						((EntityPlayer) par2EntityLiving).addStat(
								StatList.objectBreakStats[itemID], 1);
					}

					--stackSize;

					if (stackSize < 0) {
						stackSize = 0;
					}

					itemDamage = 0;
				}
			}
		}
	}

	/**
	 * Calls the corresponding fct in di
	 */
	public void hitEntity(final EntityLiving par1EntityLiving,
			final EntityPlayer par2EntityPlayer) {
		final boolean var3 = Item.itemsList[itemID].hitEntity(this,
				par1EntityLiving, par2EntityPlayer);

		if (var3) {
			par2EntityPlayer.addStat(StatList.objectUseStats[itemID], 1);
		}
	}

	public void onBlockDestroyed(final World par1World, final int par2,
			final int par3, final int par4, final int par5,
			final EntityPlayer par6EntityPlayer) {
		final boolean var7 = Item.itemsList[itemID].onBlockDestroyed(this,
				par1World, par2, par3, par4, par5, par6EntityPlayer);

		if (var7) {
			par6EntityPlayer.addStat(StatList.objectUseStats[itemID], 1);
		}
	}

	/**
	 * Returns the damage against a given entity.
	 */
	public int getDamageVsEntity(final Entity par1Entity) {
		return Item.itemsList[itemID].getDamageVsEntity(par1Entity);
	}

	/**
	 * Checks if the itemStack object can harvest a specified block
	 */
	public boolean canHarvestBlock(final Block par1Block) {
		return Item.itemsList[itemID].canHarvestBlock(par1Block);
	}

	public boolean interactWith(final EntityLiving par1EntityLiving) {
		return Item.itemsList[itemID].itemInteractionForEntity(this,
				par1EntityLiving);
	}

	/**
	 * Returns a new stack with the same properties.
	 */
	public ItemStack copy() {
		final ItemStack var1 = new ItemStack(itemID, stackSize, itemDamage);

		if (stackTagCompound != null) {
			var1.stackTagCompound = (NBTTagCompound) stackTagCompound.copy();
		}

		return var1;
	}

	public static boolean areItemStackTagsEqual(final ItemStack par0ItemStack,
			final ItemStack par1ItemStack) {
		return par0ItemStack == null && par1ItemStack == null ? true
				: par0ItemStack != null && par1ItemStack != null ? par0ItemStack.stackTagCompound == null
						&& par1ItemStack.stackTagCompound != null ? false
						: par0ItemStack.stackTagCompound == null
								|| par0ItemStack.stackTagCompound
										.equals(par1ItemStack.stackTagCompound)
						: false;
	}

	/**
	 * compares ItemStack argument1 with ItemStack argument2; returns true if
	 * both ItemStacks are equal
	 */
	public static boolean areItemStacksEqual(final ItemStack par0ItemStack,
			final ItemStack par1ItemStack) {
		return par0ItemStack == null && par1ItemStack == null ? true
				: par0ItemStack != null && par1ItemStack != null ? par0ItemStack
						.isItemStackEqual(par1ItemStack) : false;
	}

	/**
	 * compares ItemStack argument to the instance ItemStack; returns true if
	 * both ItemStacks are equal
	 */
	private boolean isItemStackEqual(final ItemStack par1ItemStack) {
		return stackSize != par1ItemStack.stackSize ? false
				: itemID != par1ItemStack.itemID ? false
						: itemDamage != par1ItemStack.itemDamage ? false
								: stackTagCompound == null
										&& par1ItemStack.stackTagCompound != null ? false
										: stackTagCompound == null
												|| stackTagCompound
														.equals(par1ItemStack.stackTagCompound);
	}

	/**
	 * compares ItemStack argument to the instance ItemStack; returns true if
	 * the Items contained in both ItemStacks are equal
	 */
	public boolean isItemEqual(final ItemStack par1ItemStack) {
		return itemID == par1ItemStack.itemID
				&& itemDamage == par1ItemStack.itemDamage;
	}

	public String getItemName() {
		return Item.itemsList[itemID].getUnlocalizedName(this);
	}

	/**
	 * Creates a copy of a ItemStack, a null parameters will return a null.
	 */
	public static ItemStack copyItemStack(final ItemStack par0ItemStack) {
		return par0ItemStack == null ? null : par0ItemStack.copy();
	}

	@Override
	public String toString() {
		return stackSize + "x" + Item.itemsList[itemID].getUnlocalizedName()
				+ "@" + itemDamage;
	}

	/**
	 * Called each tick as long the ItemStack in on player inventory. Used to
	 * progress the pickup animation and update maps.
	 */
	public void updateAnimation(final World par1World, final Entity par2Entity,
			final int par3, final boolean par4) {
		if (animationsToGo > 0) {
			--animationsToGo;
		}

		Item.itemsList[itemID]
				.onUpdate(this, par1World, par2Entity, par3, par4);
	}

	public void onCrafting(final World par1World,
			final EntityPlayer par2EntityPlayer, final int par3) {
		par2EntityPlayer.addStat(StatList.objectCraftStats[itemID], par3);
		Item.itemsList[itemID].onCreated(this, par1World, par2EntityPlayer);
	}

	public int getMaxItemUseDuration() {
		return getItem().getMaxItemUseDuration(this);
	}

	public EnumAction getItemUseAction() {
		return getItem().getItemUseAction(this);
	}

	/**
	 * Called when the player releases the use item button. Args: world,
	 * entityplayer, itemInUseCount
	 */
	public void onPlayerStoppedUsing(final World par1World,
			final EntityPlayer par2EntityPlayer, final int par3) {
		getItem().onPlayerStoppedUsing(this, par1World, par2EntityPlayer, par3);
	}

	/**
	 * Returns true if the ItemStack has an NBTTagCompound. Currently used to
	 * store enchantments.
	 */
	public boolean hasTagCompound() {
		return stackTagCompound != null;
	}

	/**
	 * Returns the NBTTagCompound of the ItemStack.
	 */
	public NBTTagCompound getTagCompound() {
		return stackTagCompound;
	}

	public NBTTagList getEnchantmentTagList() {
		return stackTagCompound == null ? null : (NBTTagList) stackTagCompound
				.getTag("ench");
	}

	/**
	 * Assigns a NBTTagCompound to the ItemStack, minecraft validates that only
	 * non-stackable items can have it.
	 */
	public void setTagCompound(final NBTTagCompound par1NBTTagCompound) {
		stackTagCompound = par1NBTTagCompound;
	}

	/**
	 * returns the display name of the itemstack
	 */
	public String getDisplayName() {
		String var1 = getItem().getItemDisplayName(this);

		if (stackTagCompound != null && stackTagCompound.hasKey("display")) {
			final NBTTagCompound var2 = stackTagCompound
					.getCompoundTag("display");

			if (var2.hasKey("Name")) {
				var1 = var2.getString("Name");
			}
		}

		return var1;
	}

	/**
	 * Sets the item's name (used by anvil to rename the items).
	 */
	public void setItemName(final String par1Str) {
		if (stackTagCompound == null) {
			stackTagCompound = new NBTTagCompound("tag");
		}

		if (!stackTagCompound.hasKey("display")) {
			stackTagCompound.setCompoundTag("display", new NBTTagCompound());
		}

		stackTagCompound.getCompoundTag("display").setString("Name", par1Str);
	}

	/**
	 * Returns true if the itemstack has a display name
	 */
	public boolean hasDisplayName() {
		return stackTagCompound == null ? false : !stackTagCompound
				.hasKey("display") ? false : stackTagCompound.getCompoundTag(
				"display").hasKey("Name");
	}

	/**
	 * Return a list of strings containing information about the item
	 */
	public List getTooltip(final EntityPlayer par1EntityPlayer,
			final boolean par2) {
		final ArrayList var3 = new ArrayList();
		final Item var4 = Item.itemsList[itemID];
		String var5 = getDisplayName();

		if (hasDisplayName()) {
			var5 = EnumChatFormatting.ITALIC + var5 + EnumChatFormatting.RESET;
		}

		if (par2) {
			String var6 = "";

			if (var5.length() > 0) {
				var5 = var5 + " (";
				var6 = ")";
			}

			if (getHasSubtypes()) {
				var5 = var5
						+ String.format(
								"#%04d/%d%s",
								new Object[] { Integer.valueOf(itemID),
										Integer.valueOf(itemDamage), var6 });
			} else {
				var5 = var5
						+ String.format("#%04d%s",
								new Object[] { Integer.valueOf(itemID), var6 });
			}
		} else if (!hasDisplayName() && itemID == Item.map.itemID) {
			var5 = var5 + " #" + itemDamage;
		}

		var3.add(var5);
		var4.addInformation(this, par1EntityPlayer, var3, par2);

		if (hasTagCompound()) {
			final NBTTagList var10 = getEnchantmentTagList();

			if (var10 != null) {
				for (int var7 = 0; var7 < var10.tagCount(); ++var7) {
					final short var8 = ((NBTTagCompound) var10.tagAt(var7))
							.getShort("id");
					final short var9 = ((NBTTagCompound) var10.tagAt(var7))
							.getShort("lvl");

					if (Enchantment.enchantmentsList[var8] != null) {
						var3.add(Enchantment.enchantmentsList[var8]
								.getTranslatedName(var9));
					}
				}
			}

			if (stackTagCompound.hasKey("display")) {
				final NBTTagCompound var11 = stackTagCompound
						.getCompoundTag("display");

				if (var11.hasKey("color")) {
					if (par2) {
						var3.add("Color: #"
								+ Integer
										.toHexString(var11.getInteger("color"))
										.toUpperCase());
					} else {
						var3.add(EnumChatFormatting.ITALIC
								+ StatCollector.translateToLocal("item.dyed"));
					}
				}

				if (var11.hasKey("Lore")) {
					final NBTTagList var12 = var11.getTagList("Lore");

					if (var12.tagCount() > 0) {
						for (int var13 = 0; var13 < var12.tagCount(); ++var13) {
							var3.add(EnumChatFormatting.DARK_PURPLE + ""
									+ EnumChatFormatting.ITALIC
									+ ((NBTTagString) var12.tagAt(var13)).data);
						}
					}
				}
			}
		}

		if (par2 && isItemDamaged()) {
			var3.add("Durability: "
					+ (getMaxDamage() - getItemDamageForDisplay()) + " / "
					+ getMaxDamage());
		}

		return var3;
	}

	public boolean hasEffect() {
		return getItem().hasEffect(this);
	}

	public EnumRarity getRarity() {
		return getItem().getRarity(this);
	}

	/**
	 * True if it is a tool and has no enchantments to begin with
	 */
	public boolean isItemEnchantable() {
		return !getItem().isItemTool(this) ? false : !isItemEnchanted();
	}

	/**
	 * Adds an enchantment with a desired level on the ItemStack.
	 */
	public void addEnchantment(final Enchantment par1Enchantment, final int par2) {
		if (stackTagCompound == null) {
			setTagCompound(new NBTTagCompound());
		}

		if (!stackTagCompound.hasKey("ench")) {
			stackTagCompound.setTag("ench", new NBTTagList("ench"));
		}

		final NBTTagList var3 = (NBTTagList) stackTagCompound.getTag("ench");
		final NBTTagCompound var4 = new NBTTagCompound();
		var4.setShort("id", (short) par1Enchantment.effectId);
		var4.setShort("lvl", (byte) par2);
		var3.appendTag(var4);
	}

	/**
	 * True if the item has enchantment data
	 */
	public boolean isItemEnchanted() {
		return stackTagCompound != null && stackTagCompound.hasKey("ench");
	}

	public void setTagInfo(final String par1Str, final NBTBase par2NBTBase) {
		if (stackTagCompound == null) {
			setTagCompound(new NBTTagCompound());
		}

		stackTagCompound.setTag(par1Str, par2NBTBase);
	}

	public boolean func_82835_x() {
		return getItem().func_82788_x();
	}

	/**
	 * Return whether this stack is on an item frame.
	 */
	public boolean isOnItemFrame() {
		return itemFrame != null;
	}

	/**
	 * Set the item frame this stack is on.
	 */
	public void setItemFrame(final EntityItemFrame par1EntityItemFrame) {
		itemFrame = par1EntityItemFrame;
	}

	/**
	 * Return the item frame this stack is on. Returns null if not on an item
	 * frame.
	 */
	public EntityItemFrame getItemFrame() {
		return itemFrame;
	}

	/**
	 * Get this stack's repair cost, or 0 if no repair cost is defined.
	 */
	public int getRepairCost() {
		return hasTagCompound() && stackTagCompound.hasKey("RepairCost") ? stackTagCompound
				.getInteger("RepairCost") : 0;
	}

	/**
	 * Set this stack's repair cost.
	 */
	public void setRepairCost(final int par1) {
		if (!hasTagCompound()) {
			stackTagCompound = new NBTTagCompound("tag");
		}

		stackTagCompound.setInteger("RepairCost", par1);
	}
}
