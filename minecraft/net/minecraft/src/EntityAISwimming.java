package net.minecraft.src;

public class EntityAISwimming extends EntityAIBase {
	private final EntityLiving theEntity;

	public EntityAISwimming(final EntityLiving par1EntityLiving) {
		theEntity = par1EntityLiving;
		setMutexBits(4);
		par1EntityLiving.getNavigator().setCanSwim(true);
	}

	/**
	 * Returns whether the EntityAIBase should begin execution.
	 */
	@Override
	public boolean shouldExecute() {
		return theEntity.isInWater() || theEntity.handleLavaMovement();
	}

	/**
	 * Updates the task
	 */
	@Override
	public void updateTask() {
		if (theEntity.getRNG().nextFloat() < 0.8F) {
			theEntity.getJumpHelper().setJumping();
		}
	}
}
