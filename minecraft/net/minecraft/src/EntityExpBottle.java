package net.minecraft.src;

public class EntityExpBottle extends EntityThrowable {
	public EntityExpBottle(final World par1World) {
		super(par1World);
	}

	public EntityExpBottle(final World par1World,
			final EntityLiving par2EntityLiving) {
		super(par1World, par2EntityLiving);
	}

	public EntityExpBottle(final World par1World, final double par2,
			final double par4, final double par6) {
		super(par1World, par2, par4, par6);
	}

	/**
	 * Gets the amount of gravity to apply to the thrown entity with each tick.
	 */
	@Override
	protected float getGravityVelocity() {
		return 0.07F;
	}

	@Override
	protected float func_70182_d() {
		return 0.7F;
	}

	@Override
	protected float func_70183_g() {
		return -20.0F;
	}

	/**
	 * Called when this EntityThrowable hits a block or entity.
	 */
	@Override
	protected void onImpact(final MovingObjectPosition par1MovingObjectPosition) {
		if (!worldObj.isRemote) {
			worldObj.playAuxSFX(2002, (int) Math.round(posX),
					(int) Math.round(posY), (int) Math.round(posZ), 0);
			int var2 = 3 + worldObj.rand.nextInt(5) + worldObj.rand.nextInt(5);

			while (var2 > 0) {
				final int var3 = EntityXPOrb.getXPSplit(var2);
				var2 -= var3;
				worldObj.spawnEntityInWorld(new EntityXPOrb(worldObj, posX,
						posY, posZ, var3));
			}

			setDead();
		}
	}
}
