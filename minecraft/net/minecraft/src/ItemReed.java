package net.minecraft.src;

public class ItemReed extends Item {
	/** The ID of the block the reed will spawn when used from inventory bar. */
	private final int spawnID;

	public ItemReed(final int par1, final Block par2Block) {
		super(par1);
		spawnID = par2Block.blockID;
	}

	/**
	 * Callback for item usage. If the item does something special on right
	 * clicking, he will have one of those. Return True if something happen and
	 * false if it don't. This is for ITEMS, not BLOCKS
	 */
	@Override
	public boolean onItemUse(final ItemStack par1ItemStack,
			final EntityPlayer par2EntityPlayer, final World par3World,
			int par4, int par5, int par6, int par7, final float par8,
			final float par9, final float par10) {
		final int var11 = par3World.getBlockId(par4, par5, par6);

		if (var11 == Block.snow.blockID
				&& (par3World.getBlockMetadata(par4, par5, par6) & 7) < 1) {
			par7 = 1;
		} else if (var11 != Block.vine.blockID
				&& var11 != Block.tallGrass.blockID
				&& var11 != Block.deadBush.blockID) {
			if (par7 == 0) {
				--par5;
			}

			if (par7 == 1) {
				++par5;
			}

			if (par7 == 2) {
				--par6;
			}

			if (par7 == 3) {
				++par6;
			}

			if (par7 == 4) {
				--par4;
			}

			if (par7 == 5) {
				++par4;
			}
		}

		if (!par2EntityPlayer.canPlayerEdit(par4, par5, par6, par7,
				par1ItemStack)) {
			return false;
		} else if (par1ItemStack.stackSize == 0) {
			return false;
		} else {
			if (par3World.canPlaceEntityOnSide(spawnID, par4, par5, par6,
					false, par7, (Entity) null, par1ItemStack)) {
				final Block var12 = Block.blocksList[spawnID];
				final int var13 = var12.onBlockPlaced(par3World, par4, par5,
						par6, par7, par8, par9, par10, 0);

				if (par3World.setBlock(par4, par5, par6, spawnID, var13, 3)) {
					if (par3World.getBlockId(par4, par5, par6) == spawnID) {
						Block.blocksList[spawnID].onBlockPlacedBy(par3World,
								par4, par5, par6, par2EntityPlayer,
								par1ItemStack);
						Block.blocksList[spawnID].onPostBlockPlaced(par3World,
								par4, par5, par6, var13);
					}

					par3World.playSoundEffect(par4 + 0.5F, par5 + 0.5F,
							par6 + 0.5F, var12.stepSound.getPlaceSound(),
							(var12.stepSound.getVolume() + 1.0F) / 2.0F,
							var12.stepSound.getPitch() * 0.8F);
					--par1ItemStack.stackSize;
				}
			}

			return true;
		}
	}
}
