package net.minecraft.src;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

public abstract class World implements IBlockAccess {
	/**
	 * boolean; if true updates scheduled by scheduleBlockUpdate happen
	 * immediately
	 */
	public boolean scheduledUpdatesAreImmediate = false;

	/** A list of all Entities in all currently-loaded chunks */
	public List loadedEntityList = new ArrayList();
	protected List unloadedEntityList = new ArrayList();

	/** A list of all TileEntities in all currently-loaded chunks */
	public List loadedTileEntityList = new ArrayList();
	private final List addedTileEntityList = new ArrayList();

	/** Entities marked for removal. */
	private final List entityRemoval = new ArrayList();

	/** Array list of players in the world. */
	public List playerEntities = new ArrayList();

	/** a list of all the lightning entities */
	public List weatherEffects = new ArrayList();
	private final long cloudColour = 16777215L;

	/** How much light is subtracted from full daylight */
	public int skylightSubtracted = 0;

	/**
	 * Contains the current Linear Congruential Generator seed for block
	 * updates. Used with an A value of 3 and a C value of 0x3c6ef35f, producing
	 * a highly planar series of values ill-suited for choosing random blocks in
	 * a 16x128x16 field.
	 */
	protected int updateLCG = new Random().nextInt();

	/**
	 * magic number used to generate fast random numbers for 3d distribution
	 * within a chunk
	 */
	protected final int DIST_HASH_MAGIC = 1013904223;
	protected float prevRainingStrength;
	protected float rainingStrength;
	protected float prevThunderingStrength;
	protected float thunderingStrength;

	/**
	 * Set to 2 whenever a lightning bolt is generated in SSP. Decrements if > 0
	 * in updateWeather(). Value appears to be unused.
	 */
	public int lastLightningBolt = 0;

	/** Option > Difficulty setting (0 - 3) */
	public int difficultySetting;

	/** RNG for World. */
	public Random rand = new Random();

	/** The WorldProvider instance that World uses. */
	public final WorldProvider provider;
	protected List worldAccesses = new ArrayList();

	/** Handles chunk operations and caching */
	protected IChunkProvider chunkProvider;
	protected final ISaveHandler saveHandler;

	/**
	 * holds information about a world (size on disk, time, spawn point, seed,
	 * ...)
	 */
	protected WorldInfo worldInfo;

	/** Boolean that is set to true when trying to find a spawn point */
	public boolean findingSpawnPoint;
	public MapStorage mapStorage;
	public final VillageCollection villageCollectionObj;
	protected final VillageSiege villageSiegeObj = new VillageSiege(this);
	public final Profiler theProfiler;

	/** The world-local pool of vectors */
	private final Vec3Pool vecPool = new Vec3Pool(300, 2000);
	private final Calendar theCalendar = Calendar.getInstance();
	protected Scoreboard worldScoreboard = new Scoreboard();

	/** The log agent for this world. */
	private final ILogAgent worldLogAgent;
	private final ArrayList collidingBoundingBoxes = new ArrayList();
	private boolean scanningTileEntities;

	/** indicates if enemies are spawned or not */
	protected boolean spawnHostileMobs = true;

	/** A flag indicating whether we should spawn peaceful mobs. */
	protected boolean spawnPeacefulMobs = true;

	/** Positions to update */
	protected Set activeChunkSet = new HashSet();

	/** number of ticks until the next random ambients play */
	private int ambientTickCountdown;

	/**
	 * is a temporary list of blocks and light values used when updating light
	 * levels. Holds up to 32x32x32 blocks (the maximum influence of a light
	 * source.) Every element is a packed bit value:
	 * 0000000000LLLLzzzzzzyyyyyyxxxxxx. The 4-bit L is a light level used when
	 * darkening blocks. 6-bit numbers x, y and z represent the block's offset
	 * from the original block, plus 32 (i.e. value of 31 would mean a -1 offset
	 */
	int[] lightUpdateBlockList;

	/** This is set to true for client worlds, and false for server worlds. */
	public boolean isRemote;

	/**
	 * Gets the biome for a given set of x/z coordinates
	 */
	@Override
	public BiomeGenBase getBiomeGenForCoords(final int par1, final int par2) {
		if (blockExists(par1, 0, par2)) {
			final Chunk var3 = getChunkFromBlockCoords(par1, par2);

			if (var3 != null) {
				return var3.getBiomeGenForWorldCoords(par1 & 15, par2 & 15,
						provider.worldChunkMgr);
			}
		}

		return provider.worldChunkMgr.getBiomeGenAt(par1, par2);
	}

	public WorldChunkManager getWorldChunkManager() {
		return provider.worldChunkMgr;
	}

	public World(final ISaveHandler par1ISaveHandler, final String par2Str,
			final WorldProvider par3WorldProvider,
			final WorldSettings par4WorldSettings, final Profiler par5Profiler,
			final ILogAgent par6ILogAgent) {
		ambientTickCountdown = rand.nextInt(12000);
		lightUpdateBlockList = new int[32768];
		isRemote = false;
		saveHandler = par1ISaveHandler;
		theProfiler = par5Profiler;
		worldInfo = new WorldInfo(par4WorldSettings, par2Str);
		provider = par3WorldProvider;
		mapStorage = new MapStorage(par1ISaveHandler);
		worldLogAgent = par6ILogAgent;
		final VillageCollection var7 = (VillageCollection) mapStorage.loadData(
				VillageCollection.class, "villages");

		if (var7 == null) {
			villageCollectionObj = new VillageCollection(this);
			mapStorage.setData("villages", villageCollectionObj);
		} else {
			villageCollectionObj = var7;
			villageCollectionObj.func_82566_a(this);
		}

		par3WorldProvider.registerWorld(this);
		chunkProvider = createChunkProvider();
		calculateInitialSkylight();
		calculateInitialWeather();
	}

	public World(final ISaveHandler par1ISaveHandler, final String par2Str,
			final WorldSettings par3WorldSettings,
			final WorldProvider par4WorldProvider, final Profiler par5Profiler,
			final ILogAgent par6ILogAgent) {
		ambientTickCountdown = rand.nextInt(12000);
		lightUpdateBlockList = new int[32768];
		isRemote = false;
		saveHandler = par1ISaveHandler;
		theProfiler = par5Profiler;
		mapStorage = new MapStorage(par1ISaveHandler);
		worldLogAgent = par6ILogAgent;
		worldInfo = par1ISaveHandler.loadWorldInfo();

		if (par4WorldProvider != null) {
			provider = par4WorldProvider;
		} else if (worldInfo != null && worldInfo.getDimension() != 0) {
			provider = WorldProvider.getProviderForDimension(worldInfo
					.getDimension());
		} else {
			provider = WorldProvider.getProviderForDimension(0);
		}

		if (worldInfo == null) {
			worldInfo = new WorldInfo(par3WorldSettings, par2Str);
		} else {
			worldInfo.setWorldName(par2Str);
		}

		provider.registerWorld(this);
		chunkProvider = createChunkProvider();

		if (!worldInfo.isInitialized()) {
			try {
				initialize(par3WorldSettings);
			} catch (final Throwable var11) {
				final CrashReport var8 = CrashReport.makeCrashReport(var11,
						"Exception initializing level");

				try {
					addWorldInfoToCrashReport(var8);
				} catch (final Throwable var10) {
					;
				}

				throw new ReportedException(var8);
			}

			worldInfo.setServerInitialized(true);
		}

		final VillageCollection var7 = (VillageCollection) mapStorage.loadData(
				VillageCollection.class, "villages");

		if (var7 == null) {
			villageCollectionObj = new VillageCollection(this);
			mapStorage.setData("villages", villageCollectionObj);
		} else {
			villageCollectionObj = var7;
			villageCollectionObj.func_82566_a(this);
		}

		calculateInitialSkylight();
		calculateInitialWeather();
	}

	/**
	 * Creates the chunk provider for this world. Called in the constructor.
	 * Retrieves provider from worldProvider?
	 */
	protected abstract IChunkProvider createChunkProvider();

	protected void initialize(final WorldSettings par1WorldSettings) {
		worldInfo.setServerInitialized(true);
	}

	/**
	 * Sets a new spawn location by finding an uncovered block at a random (x,z)
	 * location in the chunk.
	 */
	public void setSpawnLocation() {
		this.setSpawnLocation(8, 64, 8);
	}

	/**
	 * Returns the block ID of the first block at this (x,z) location with air
	 * above it, searching from sea level upwards.
	 */
	public int getFirstUncoveredBlock(final int par1, final int par2) {
		int var3;

		for (var3 = 63; !isAirBlock(par1, var3 + 1, par2); ++var3) {
			;
		}

		return getBlockId(par1, var3, par2);
	}

	/**
	 * Returns the block ID at coords x,y,z
	 */
	@Override
	public int getBlockId(final int par1, final int par2, final int par3) {
		if (par1 >= -30000000 && par3 >= -30000000 && par1 < 30000000
				&& par3 < 30000000) {
			if (par2 < 0) {
				return 0;
			} else if (par2 >= 256) {
				return 0;
			} else {
				Chunk var4 = null;

				try {
					var4 = getChunkFromChunkCoords(par1 >> 4, par3 >> 4);
					return var4.getBlockID(par1 & 15, par2, par3 & 15);
				} catch (final Throwable var8) {
					final CrashReport var6 = CrashReport.makeCrashReport(var8,
							"Exception getting block type in world");
					final CrashReportCategory var7 = var6
							.makeCategory("Requested block coordinates");
					var7.addCrashSection("Found chunk",
							Boolean.valueOf(var4 == null));
					var7.addCrashSection("Location", CrashReportCategory
							.getLocationInfo(par1, par2, par3));
					throw new ReportedException(var6);
				}
			}
		} else {
			return 0;
		}
	}

	/**
	 * Returns true if the block at the specified coordinates is empty
	 */
	@Override
	public boolean isAirBlock(final int par1, final int par2, final int par3) {
		return getBlockId(par1, par2, par3) == 0;
	}

	/**
	 * Checks if a block at a given position should have a tile entity.
	 */
	public boolean blockHasTileEntity(final int par1, final int par2,
			final int par3) {
		final int var4 = getBlockId(par1, par2, par3);
		return Block.blocksList[var4] != null
				&& Block.blocksList[var4].hasTileEntity();
	}

	/**
	 * Returns the render type of the block at the given coordinate.
	 */
	public int blockGetRenderType(final int par1, final int par2, final int par3) {
		final int var4 = getBlockId(par1, par2, par3);
		return Block.blocksList[var4] != null ? Block.blocksList[var4]
				.getRenderType() : -1;
	}

	/**
	 * Returns whether a block exists at world coordinates x, y, z
	 */
	public boolean blockExists(final int par1, final int par2, final int par3) {
		return par2 >= 0 && par2 < 256 ? chunkExists(par1 >> 4, par3 >> 4)
				: false;
	}

	/**
	 * Checks if any of the chunks within distance (argument 4) blocks of the
	 * given block exist
	 */
	public boolean doChunksNearChunkExist(final int par1, final int par2,
			final int par3, final int par4) {
		return checkChunksExist(par1 - par4, par2 - par4, par3 - par4, par1
				+ par4, par2 + par4, par3 + par4);
	}

	/**
	 * Checks between a min and max all the chunks inbetween actually exist.
	 * Args: minX, minY, minZ, maxX, maxY, maxZ
	 */
	public boolean checkChunksExist(int par1, final int par2, int par3,
			int par4, final int par5, int par6) {
		if (par5 >= 0 && par2 < 256) {
			par1 >>= 4;
			par3 >>= 4;
			par4 >>= 4;
			par6 >>= 4;

			for (int var7 = par1; var7 <= par4; ++var7) {
				for (int var8 = par3; var8 <= par6; ++var8) {
					if (!chunkExists(var7, var8)) {
						return false;
					}
				}
			}

			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns whether a chunk exists at chunk coordinates x, y
	 */
	protected boolean chunkExists(final int par1, final int par2) {
		return chunkProvider.chunkExists(par1, par2);
	}

	/**
	 * Returns a chunk looked up by block coordinates. Args: x, z
	 */
	public Chunk getChunkFromBlockCoords(final int par1, final int par2) {
		return getChunkFromChunkCoords(par1 >> 4, par2 >> 4);
	}

	/**
	 * Returns back a chunk looked up by chunk coordinates Args: x, y
	 */
	public Chunk getChunkFromChunkCoords(final int par1, final int par2) {
		return chunkProvider.provideChunk(par1, par2);
	}

	/**
	 * Sets the block ID and metadata at a given location. Args: X, Y, Z, new
	 * block ID, new metadata, flags. Flag 1 will cause a block update. Flag 2
	 * will send the change to clients (you almost always want this). Flag 4
	 * prevents the block from being re-rendered, if this is a client world.
	 * Flags can be added together.
	 */
	public boolean setBlock(final int par1, final int par2, final int par3,
			final int par4, final int par5, final int par6) {
		if (par1 >= -30000000 && par3 >= -30000000 && par1 < 30000000
				&& par3 < 30000000) {
			if (par2 < 0) {
				return false;
			} else if (par2 >= 256) {
				return false;
			} else {
				final Chunk var7 = getChunkFromChunkCoords(par1 >> 4, par3 >> 4);
				int var8 = 0;

				if ((par6 & 1) != 0) {
					var8 = var7.getBlockID(par1 & 15, par2, par3 & 15);
				}

				final boolean var9 = var7.setBlockIDWithMetadata(par1 & 15,
						par2, par3 & 15, par4, par5);
				theProfiler.startSection("checkLight");
				updateAllLightTypes(par1, par2, par3);
				theProfiler.endSection();

				if (var9) {
					if ((par6 & 2) != 0 && (!isRemote || (par6 & 4) == 0)) {
						markBlockForUpdate(par1, par2, par3);
					}

					if (!isRemote && (par6 & 1) != 0) {
						notifyBlockChange(par1, par2, par3, var8);
						final Block var10 = Block.blocksList[par4];

						if (var10 != null && var10.hasComparatorInputOverride()) {
							func_96440_m(par1, par2, par3, par4);
						}
					}
				}

				return var9;
			}
		} else {
			return false;
		}
	}

	/**
	 * Returns the block's material.
	 */
	@Override
	public Material getBlockMaterial(final int par1, final int par2,
			final int par3) {
		final int var4 = getBlockId(par1, par2, par3);
		return var4 == 0 ? Material.air : Block.blocksList[var4].blockMaterial;
	}

	/**
	 * Returns the block metadata at coords x,y,z
	 */
	@Override
	public int getBlockMetadata(int par1, final int par2, int par3) {
		if (par1 >= -30000000 && par3 >= -30000000 && par1 < 30000000
				&& par3 < 30000000) {
			if (par2 < 0) {
				return 0;
			} else if (par2 >= 256) {
				return 0;
			} else {
				final Chunk var4 = getChunkFromChunkCoords(par1 >> 4, par3 >> 4);
				par1 &= 15;
				par3 &= 15;
				return var4.getBlockMetadata(par1, par2, par3);
			}
		} else {
			return 0;
		}
	}

	/**
	 * Sets the blocks metadata and if set will then notify blocks that this
	 * block changed, depending on the flag. Args: x, y, z, metadata, flag. See
	 * setBlock for flag description
	 */
	public boolean setBlockMetadataWithNotify(final int par1, final int par2,
			final int par3, final int par4, final int par5) {
		if (par1 >= -30000000 && par3 >= -30000000 && par1 < 30000000
				&& par3 < 30000000) {
			if (par2 < 0) {
				return false;
			} else if (par2 >= 256) {
				return false;
			} else {
				final Chunk var6 = getChunkFromChunkCoords(par1 >> 4, par3 >> 4);
				final int var7 = par1 & 15;
				final int var8 = par3 & 15;
				final boolean var9 = var6.setBlockMetadata(var7, par2, var8,
						par4);

				if (var9) {
					final int var10 = var6.getBlockID(var7, par2, var8);

					if ((par5 & 2) != 0 && (!isRemote || (par5 & 4) == 0)) {
						markBlockForUpdate(par1, par2, par3);
					}

					if (!isRemote && (par5 & 1) != 0) {
						notifyBlockChange(par1, par2, par3, var10);
						final Block var11 = Block.blocksList[var10];

						if (var11 != null && var11.hasComparatorInputOverride()) {
							func_96440_m(par1, par2, par3, var10);
						}
					}
				}

				return var9;
			}
		} else {
			return false;
		}
	}

	/**
	 * Sets a block to 0 and notifies relevant systems with the block change
	 * Args: x, y, z
	 */
	public boolean setBlockToAir(final int par1, final int par2, final int par3) {
		return this.setBlock(par1, par2, par3, 0, 0, 3);
	}

	/**
	 * Destroys a block and optionally drops items. Args: X, Y, Z, dropItems
	 */
	public boolean destroyBlock(final int par1, final int par2, final int par3,
			final boolean par4) {
		final int var5 = getBlockId(par1, par2, par3);

		if (var5 > 0) {
			final int var6 = getBlockMetadata(par1, par2, par3);
			playAuxSFX(2001, par1, par2, par3, var5 + (var6 << 12));

			if (par4) {
				Block.blocksList[var5].dropBlockAsItem(this, par1, par2, par3,
						var6, 0);
			}

			return this.setBlock(par1, par2, par3, 0, 0, 3);
		} else {
			return false;
		}
	}

	/**
	 * Sets a block and notifies relevant systems with the block change Args: x,
	 * y, z, blockID
	 */
	public boolean setBlock(final int par1, final int par2, final int par3,
			final int par4) {
		return this.setBlock(par1, par2, par3, par4, 0, 3);
	}

	/**
	 * On the client, re-renders the block. On the server, sends the block to
	 * the client (which will re-render it), including the tile entity
	 * description packet if applicable. Args: x, y, z
	 */
	public void markBlockForUpdate(final int par1, final int par2,
			final int par3) {
		for (int var4 = 0; var4 < worldAccesses.size(); ++var4) {
			((IWorldAccess) worldAccesses.get(var4)).markBlockForUpdate(par1,
					par2, par3);
		}
	}

	/**
	 * The block type change and need to notify other systems Args: x, y, z,
	 * blockID
	 */
	public void notifyBlockChange(final int par1, final int par2,
			final int par3, final int par4) {
		this.notifyBlocksOfNeighborChange(par1, par2, par3, par4);
	}

	/**
	 * marks a vertical line of blocks as dirty
	 */
	public void markBlocksDirtyVertical(final int par1, final int par2,
			int par3, int par4) {
		int var5;

		if (par3 > par4) {
			var5 = par4;
			par4 = par3;
			par3 = var5;
		}

		if (!provider.hasNoSky) {
			for (var5 = par3; var5 <= par4; ++var5) {
				updateLightByType(EnumSkyBlock.Sky, par1, var5, par2);
			}
		}

		markBlockRangeForRenderUpdate(par1, par3, par2, par1, par4, par2);
	}

	/**
	 * On the client, re-renders all blocks in this range, inclusive. On the
	 * server, does nothing. Args: min x, min y, min z, max x, max y, max z
	 */
	public void markBlockRangeForRenderUpdate(final int par1, final int par2,
			final int par3, final int par4, final int par5, final int par6) {
		for (int var7 = 0; var7 < worldAccesses.size(); ++var7) {
			((IWorldAccess) worldAccesses.get(var7))
					.markBlockRangeForRenderUpdate(par1, par2, par3, par4,
							par5, par6);
		}
	}

	/**
	 * Notifies neighboring blocks that this specified block changed Args: x, y,
	 * z, blockID
	 */
	public void notifyBlocksOfNeighborChange(final int par1, final int par2,
			final int par3, final int par4) {
		notifyBlockOfNeighborChange(par1 - 1, par2, par3, par4);
		notifyBlockOfNeighborChange(par1 + 1, par2, par3, par4);
		notifyBlockOfNeighborChange(par1, par2 - 1, par3, par4);
		notifyBlockOfNeighborChange(par1, par2 + 1, par3, par4);
		notifyBlockOfNeighborChange(par1, par2, par3 - 1, par4);
		notifyBlockOfNeighborChange(par1, par2, par3 + 1, par4);
	}

	/**
	 * Calls notifyBlockOfNeighborChange on adjacent blocks, except the one on
	 * the given side. Args: X, Y, Z, changingBlockID, side
	 */
	public void notifyBlocksOfNeighborChange(final int par1, final int par2,
			final int par3, final int par4, final int par5) {
		if (par5 != 4) {
			notifyBlockOfNeighborChange(par1 - 1, par2, par3, par4);
		}

		if (par5 != 5) {
			notifyBlockOfNeighborChange(par1 + 1, par2, par3, par4);
		}

		if (par5 != 0) {
			notifyBlockOfNeighborChange(par1, par2 - 1, par3, par4);
		}

		if (par5 != 1) {
			notifyBlockOfNeighborChange(par1, par2 + 1, par3, par4);
		}

		if (par5 != 2) {
			notifyBlockOfNeighborChange(par1, par2, par3 - 1, par4);
		}

		if (par5 != 3) {
			notifyBlockOfNeighborChange(par1, par2, par3 + 1, par4);
		}
	}

	/**
	 * Notifies a block that one of its neighbor change to the specified type
	 * Args: x, y, z, blockID
	 */
	public void notifyBlockOfNeighborChange(final int par1, final int par2,
			final int par3, final int par4) {
		if (!isRemote) {
			final int var5 = getBlockId(par1, par2, par3);
			final Block var6 = Block.blocksList[var5];

			if (var6 != null) {
				try {
					var6.onNeighborBlockChange(this, par1, par2, par3, par4);
				} catch (final Throwable var13) {
					final CrashReport var8 = CrashReport.makeCrashReport(var13,
							"Exception while updating neighbours");
					final CrashReportCategory var9 = var8
							.makeCategory("Block being updated");
					int var10;

					try {
						var10 = getBlockMetadata(par1, par2, par3);
					} catch (final Throwable var12) {
						var10 = -1;
					}

					var9.addCrashSectionCallable("Source block type",
							new CallableLvl1(this, par4));
					CrashReportCategory.func_85068_a(var9, par1, par2, par3,
							var5, var10);
					throw new ReportedException(var8);
				}
			}
		}
	}

	/**
	 * Returns true if the given block will receive a scheduled tick in the
	 * future. Args: X, Y, Z, blockID
	 */
	public boolean isBlockTickScheduled(final int par1, final int par2,
			final int par3, final int par4) {
		return false;
	}

	/**
	 * Checks if the specified block is able to see the sky
	 */
	public boolean canBlockSeeTheSky(final int par1, final int par2,
			final int par3) {
		return getChunkFromChunkCoords(par1 >> 4, par3 >> 4).canBlockSeeTheSky(
				par1 & 15, par2, par3 & 15);
	}

	/**
	 * Does the same as getBlockLightValue_do but without checking if its not a
	 * normal block
	 */
	public int getFullBlockLightValue(final int par1, int par2, final int par3) {
		if (par2 < 0) {
			return 0;
		} else {
			if (par2 >= 256) {
				par2 = 255;
			}

			return getChunkFromChunkCoords(par1 >> 4, par3 >> 4)
					.getBlockLightValue(par1 & 15, par2, par3 & 15, 0);
		}
	}

	/**
	 * Gets the light value of a block location
	 */
	public int getBlockLightValue(final int par1, final int par2, final int par3) {
		return getBlockLightValue_do(par1, par2, par3, true);
	}

	/**
	 * Gets the light value of a block location. This is the actual function
	 * that gets the value and has a bool flag that indicates if its a half step
	 * block to get the maximum light value of a direct neighboring block (left,
	 * right, forward, back, and up)
	 */
	public int getBlockLightValue_do(int par1, int par2, int par3,
			final boolean par4) {
		if (par1 >= -30000000 && par3 >= -30000000 && par1 < 30000000
				&& par3 < 30000000) {
			if (par4) {
				final int var5 = getBlockId(par1, par2, par3);

				if (Block.useNeighborBrightness[var5]) {
					int var6 = getBlockLightValue_do(par1, par2 + 1, par3,
							false);
					final int var7 = getBlockLightValue_do(par1 + 1, par2,
							par3, false);
					final int var8 = getBlockLightValue_do(par1 - 1, par2,
							par3, false);
					final int var9 = getBlockLightValue_do(par1, par2,
							par3 + 1, false);
					final int var10 = getBlockLightValue_do(par1, par2,
							par3 - 1, false);

					if (var7 > var6) {
						var6 = var7;
					}

					if (var8 > var6) {
						var6 = var8;
					}

					if (var9 > var6) {
						var6 = var9;
					}

					if (var10 > var6) {
						var6 = var10;
					}

					return var6;
				}
			}

			if (par2 < 0) {
				return 0;
			} else {
				if (par2 >= 256) {
					par2 = 255;
				}

				final Chunk var11 = getChunkFromChunkCoords(par1 >> 4,
						par3 >> 4);
				par1 &= 15;
				par3 &= 15;
				return var11.getBlockLightValue(par1, par2, par3,
						skylightSubtracted);
			}
		} else {
			return 15;
		}
	}

	/**
	 * Returns the y coordinate with a block in it at this x, z coordinate
	 */
	public int getHeightValue(final int par1, final int par2) {
		if (par1 >= -30000000 && par2 >= -30000000 && par1 < 30000000
				&& par2 < 30000000) {
			if (!chunkExists(par1 >> 4, par2 >> 4)) {
				return 0;
			} else {
				final Chunk var3 = getChunkFromChunkCoords(par1 >> 4, par2 >> 4);
				return var3.getHeightValue(par1 & 15, par2 & 15);
			}
		} else {
			return 0;
		}
	}

	/**
	 * Gets the heightMapMinimum field of the given chunk, or 0 if the chunk is
	 * not loaded. Coords are in blocks. Args: X, Z
	 */
	public int getChunkHeightMapMinimum(final int par1, final int par2) {
		if (par1 >= -30000000 && par2 >= -30000000 && par1 < 30000000
				&& par2 < 30000000) {
			if (!chunkExists(par1 >> 4, par2 >> 4)) {
				return 0;
			} else {
				final Chunk var3 = getChunkFromChunkCoords(par1 >> 4, par2 >> 4);
				return var3.heightMapMinimum;
			}
		} else {
			return 0;
		}
	}

	/**
	 * Brightness for SkyBlock.Sky is clear white and (through color computing
	 * it is assumed) DEPENDENT ON DAYTIME. Brightness for SkyBlock.Block is
	 * yellowish and independent.
	 */
	public int getSkyBlockTypeBrightness(final EnumSkyBlock par1EnumSkyBlock,
			final int par2, int par3, final int par4) {
		if (provider.hasNoSky && par1EnumSkyBlock == EnumSkyBlock.Sky) {
			return 0;
		} else {
			if (par3 < 0) {
				par3 = 0;
			}

			if (par3 >= 256) {
				return par1EnumSkyBlock.defaultLightValue;
			} else if (par2 >= -30000000 && par4 >= -30000000
					&& par2 < 30000000 && par4 < 30000000) {
				final int var5 = par2 >> 4;
				final int var6 = par4 >> 4;

				if (!chunkExists(var5, var6)) {
					return par1EnumSkyBlock.defaultLightValue;
				} else if (Block.useNeighborBrightness[getBlockId(par2, par3,
						par4)]) {
					int var12 = getSavedLightValue(par1EnumSkyBlock, par2,
							par3 + 1, par4);
					final int var8 = getSavedLightValue(par1EnumSkyBlock,
							par2 + 1, par3, par4);
					final int var9 = getSavedLightValue(par1EnumSkyBlock,
							par2 - 1, par3, par4);
					final int var10 = getSavedLightValue(par1EnumSkyBlock,
							par2, par3, par4 + 1);
					final int var11 = getSavedLightValue(par1EnumSkyBlock,
							par2, par3, par4 - 1);

					if (var8 > var12) {
						var12 = var8;
					}

					if (var9 > var12) {
						var12 = var9;
					}

					if (var10 > var12) {
						var12 = var10;
					}

					if (var11 > var12) {
						var12 = var11;
					}

					return var12;
				} else {
					final Chunk var7 = getChunkFromChunkCoords(var5, var6);
					return var7.getSavedLightValue(par1EnumSkyBlock, par2 & 15,
							par3, par4 & 15);
				}
			} else {
				return par1EnumSkyBlock.defaultLightValue;
			}
		}
	}

	/**
	 * Returns saved light value without taking into account the time of day.
	 * Either looks in the sky light map or block light map based on the
	 * enumSkyBlock arg.
	 */
	public int getSavedLightValue(final EnumSkyBlock par1EnumSkyBlock,
			final int par2, int par3, final int par4) {
		if (par3 < 0) {
			par3 = 0;
		}

		if (par3 >= 256) {
			par3 = 255;
		}

		if (par2 >= -30000000 && par4 >= -30000000 && par2 < 30000000
				&& par4 < 30000000) {
			final int var5 = par2 >> 4;
			final int var6 = par4 >> 4;

			if (!chunkExists(var5, var6)) {
				return par1EnumSkyBlock.defaultLightValue;
			} else {
				final Chunk var7 = getChunkFromChunkCoords(var5, var6);
				return var7.getSavedLightValue(par1EnumSkyBlock, par2 & 15,
						par3, par4 & 15);
			}
		} else {
			return par1EnumSkyBlock.defaultLightValue;
		}
	}

	/**
	 * Sets the light value either into the sky map or block map depending on if
	 * enumSkyBlock is set to sky or block. Args: enumSkyBlock, x, y, z,
	 * lightValue
	 */
	public void setLightValue(final EnumSkyBlock par1EnumSkyBlock,
			final int par2, final int par3, final int par4, final int par5) {
		if (par2 >= -30000000 && par4 >= -30000000 && par2 < 30000000
				&& par4 < 30000000) {
			if (par3 >= 0) {
				if (par3 < 256) {
					if (chunkExists(par2 >> 4, par4 >> 4)) {
						final Chunk var6 = getChunkFromChunkCoords(par2 >> 4,
								par4 >> 4);
						var6.setLightValue(par1EnumSkyBlock, par2 & 15, par3,
								par4 & 15, par5);

						for (int var7 = 0; var7 < worldAccesses.size(); ++var7) {
							((IWorldAccess) worldAccesses.get(var7))
									.markBlockForRenderUpdate(par2, par3, par4);
						}
					}
				}
			}
		}
	}

	/**
	 * On the client, re-renders this block. On the server, does nothing. Used
	 * for lighting updates.
	 */
	public void markBlockForRenderUpdate(final int par1, final int par2,
			final int par3) {
		for (int var4 = 0; var4 < worldAccesses.size(); ++var4) {
			((IWorldAccess) worldAccesses.get(var4)).markBlockForRenderUpdate(
					par1, par2, par3);
		}
	}

	/**
	 * Any Light rendered on a 1.8 Block goes through here
	 */
	@Override
	public int getLightBrightnessForSkyBlocks(final int par1, final int par2,
			final int par3, final int par4) {
		final int var5 = getSkyBlockTypeBrightness(EnumSkyBlock.Sky, par1,
				par2, par3);
		int var6 = getSkyBlockTypeBrightness(EnumSkyBlock.Block, par1, par2,
				par3);

		if (var6 < par4) {
			var6 = par4;
		}

		return var5 << 20 | var6 << 4;
	}

	@Override
	public float getBrightness(final int par1, final int par2, final int par3,
			final int par4) {
		int var5 = getBlockLightValue(par1, par2, par3);

		if (var5 < par4) {
			var5 = par4;
		}

		return provider.lightBrightnessTable[var5];
	}

	/**
	 * Returns how bright the block is shown as which is the block's light value
	 * looked up in a lookup table (light values aren't linear for brightness).
	 * Args: x, y, z
	 */
	@Override
	public float getLightBrightness(final int par1, final int par2,
			final int par3) {
		return provider.lightBrightnessTable[getBlockLightValue(par1, par2,
				par3)];
	}

	/**
	 * Checks whether its daytime by seeing if the light subtracted from the
	 * skylight is less than 4
	 */
	public boolean isDaytime() {
		return skylightSubtracted < 4;
	}

	/**
	 * ray traces all blocks, including non-collideable ones
	 */
	public MovingObjectPosition rayTraceBlocks(final Vec3 par1Vec3,
			final Vec3 par2Vec3) {
		return rayTraceBlocks_do_do(par1Vec3, par2Vec3, false, false);
	}

	public MovingObjectPosition rayTraceBlocks_do(final Vec3 par1Vec3,
			final Vec3 par2Vec3, final boolean par3) {
		return rayTraceBlocks_do_do(par1Vec3, par2Vec3, par3, false);
	}

	public MovingObjectPosition rayTraceBlocks_do_do(final Vec3 par1Vec3,
			final Vec3 par2Vec3, final boolean par3, final boolean par4) {
		if (!Double.isNaN(par1Vec3.xCoord) && !Double.isNaN(par1Vec3.yCoord)
				&& !Double.isNaN(par1Vec3.zCoord)) {
			if (!Double.isNaN(par2Vec3.xCoord)
					&& !Double.isNaN(par2Vec3.yCoord)
					&& !Double.isNaN(par2Vec3.zCoord)) {
				final int var5 = MathHelper.floor_double(par2Vec3.xCoord);
				final int var6 = MathHelper.floor_double(par2Vec3.yCoord);
				final int var7 = MathHelper.floor_double(par2Vec3.zCoord);
				int var8 = MathHelper.floor_double(par1Vec3.xCoord);
				int var9 = MathHelper.floor_double(par1Vec3.yCoord);
				int var10 = MathHelper.floor_double(par1Vec3.zCoord);
				int var11 = getBlockId(var8, var9, var10);
				final int var12 = getBlockMetadata(var8, var9, var10);
				final Block var13 = Block.blocksList[var11];

				if ((!par4 || var13 == null || var13
						.getCollisionBoundingBoxFromPool(this, var8, var9,
								var10) != null)
						&& var11 > 0 && var13.canCollideCheck(var12, par3)) {
					final MovingObjectPosition var14 = var13.collisionRayTrace(
							this, var8, var9, var10, par1Vec3, par2Vec3);

					if (var14 != null) {
						return var14;
					}
				}

				var11 = 200;

				while (var11-- >= 0) {
					if (Double.isNaN(par1Vec3.xCoord)
							|| Double.isNaN(par1Vec3.yCoord)
							|| Double.isNaN(par1Vec3.zCoord)) {
						return null;
					}

					if (var8 == var5 && var9 == var6 && var10 == var7) {
						return null;
					}

					boolean var39 = true;
					boolean var40 = true;
					boolean var41 = true;
					double var15 = 999.0D;
					double var17 = 999.0D;
					double var19 = 999.0D;

					if (var5 > var8) {
						var15 = var8 + 1.0D;
					} else if (var5 < var8) {
						var15 = var8 + 0.0D;
					} else {
						var39 = false;
					}

					if (var6 > var9) {
						var17 = var9 + 1.0D;
					} else if (var6 < var9) {
						var17 = var9 + 0.0D;
					} else {
						var40 = false;
					}

					if (var7 > var10) {
						var19 = var10 + 1.0D;
					} else if (var7 < var10) {
						var19 = var10 + 0.0D;
					} else {
						var41 = false;
					}

					double var21 = 999.0D;
					double var23 = 999.0D;
					double var25 = 999.0D;
					final double var27 = par2Vec3.xCoord - par1Vec3.xCoord;
					final double var29 = par2Vec3.yCoord - par1Vec3.yCoord;
					final double var31 = par2Vec3.zCoord - par1Vec3.zCoord;

					if (var39) {
						var21 = (var15 - par1Vec3.xCoord) / var27;
					}

					if (var40) {
						var23 = (var17 - par1Vec3.yCoord) / var29;
					}

					if (var41) {
						var25 = (var19 - par1Vec3.zCoord) / var31;
					}

					byte var42;

					if (var21 < var23 && var21 < var25) {
						if (var5 > var8) {
							var42 = 4;
						} else {
							var42 = 5;
						}

						par1Vec3.xCoord = var15;
						par1Vec3.yCoord += var29 * var21;
						par1Vec3.zCoord += var31 * var21;
					} else if (var23 < var25) {
						if (var6 > var9) {
							var42 = 0;
						} else {
							var42 = 1;
						}

						par1Vec3.xCoord += var27 * var23;
						par1Vec3.yCoord = var17;
						par1Vec3.zCoord += var31 * var23;
					} else {
						if (var7 > var10) {
							var42 = 2;
						} else {
							var42 = 3;
						}

						par1Vec3.xCoord += var27 * var25;
						par1Vec3.yCoord += var29 * var25;
						par1Vec3.zCoord = var19;
					}

					final Vec3 var34 = getWorldVec3Pool().getVecFromPool(
							par1Vec3.xCoord, par1Vec3.yCoord, par1Vec3.zCoord);
					var8 = (int) (var34.xCoord = MathHelper
							.floor_double(par1Vec3.xCoord));

					if (var42 == 5) {
						--var8;
						++var34.xCoord;
					}

					var9 = (int) (var34.yCoord = MathHelper
							.floor_double(par1Vec3.yCoord));

					if (var42 == 1) {
						--var9;
						++var34.yCoord;
					}

					var10 = (int) (var34.zCoord = MathHelper
							.floor_double(par1Vec3.zCoord));

					if (var42 == 3) {
						--var10;
						++var34.zCoord;
					}

					final int var35 = getBlockId(var8, var9, var10);
					final int var36 = getBlockMetadata(var8, var9, var10);
					final Block var37 = Block.blocksList[var35];

					if ((!par4 || var37 == null || var37
							.getCollisionBoundingBoxFromPool(this, var8, var9,
									var10) != null)
							&& var35 > 0 && var37.canCollideCheck(var36, par3)) {
						final MovingObjectPosition var38 = var37
								.collisionRayTrace(this, var8, var9, var10,
										par1Vec3, par2Vec3);

						if (var38 != null) {
							return var38;
						}
					}
				}

				return null;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	/**
	 * Plays a sound at the entity's position. Args: entity, sound, volume
	 * (relative to 1.0), and frequency (or pitch, also relative to 1.0).
	 */
	public void playSoundAtEntity(final Entity par1Entity,
			final String par2Str, final float par3, final float par4) {
		if (par1Entity != null && par2Str != null) {
			for (int var5 = 0; var5 < worldAccesses.size(); ++var5) {
				((IWorldAccess) worldAccesses.get(var5)).playSound(par2Str,
						par1Entity.posX, par1Entity.posY - par1Entity.yOffset,
						par1Entity.posZ, par3, par4);
			}
		}
	}

	/**
	 * Plays sound to all near players except the player reference given
	 */
	public void playSoundToNearExcept(final EntityPlayer par1EntityPlayer,
			final String par2Str, final float par3, final float par4) {
		if (par1EntityPlayer != null && par2Str != null) {
			for (int var5 = 0; var5 < worldAccesses.size(); ++var5) {
				((IWorldAccess) worldAccesses.get(var5)).playSoundToNearExcept(
						par1EntityPlayer, par2Str, par1EntityPlayer.posX,
						par1EntityPlayer.posY - par1EntityPlayer.yOffset,
						par1EntityPlayer.posZ, par3, par4);
			}
		}
	}

	/**
	 * Play a sound effect. Many many parameters for this function. Not sure
	 * what they do, but a classic call is : (double)i + 0.5D, (double)j + 0.5D,
	 * (double)k + 0.5D, 'random.door_open', 1.0F, world.rand.nextFloat() * 0.1F
	 * + 0.9F with i,j,k position of the block.
	 */
	public void playSoundEffect(final double par1, final double par3,
			final double par5, final String par7Str, final float par8,
			final float par9) {
		if (par7Str != null) {
			for (int var10 = 0; var10 < worldAccesses.size(); ++var10) {
				((IWorldAccess) worldAccesses.get(var10)).playSound(par7Str,
						par1, par3, par5, par8, par9);
			}
		}
	}

	/**
	 * par8 is loudness, all pars passed to
	 * minecraftInstance.sndManager.playSound
	 */
	public void playSound(final double par1, final double par3,
			final double par5, final String par7Str, final float par8,
			final float par9, final boolean par10) {
	}

	/**
	 * Plays a record at the specified coordinates of the specified name. Args:
	 * recordName, x, y, z
	 */
	public void playRecord(final String par1Str, final int par2,
			final int par3, final int par4) {
		for (int var5 = 0; var5 < worldAccesses.size(); ++var5) {
			((IWorldAccess) worldAccesses.get(var5)).playRecord(par1Str, par2,
					par3, par4);
		}
	}

	/**
	 * Spawns a particle. Args particleName, x, y, z, velX, velY, velZ
	 */
	public void spawnParticle(final String par1Str, final double par2,
			final double par4, final double par6, final double par8,
			final double par10, final double par12) {
		for (int var14 = 0; var14 < worldAccesses.size(); ++var14) {
			((IWorldAccess) worldAccesses.get(var14)).spawnParticle(par1Str,
					par2, par4, par6, par8, par10, par12);
		}
	}

	/**
	 * adds a lightning bolt to the list of lightning bolts in this world.
	 */
	public boolean addWeatherEffect(final Entity par1Entity) {
		weatherEffects.add(par1Entity);
		return true;
	}

	/**
	 * Called to place all entities as part of a world
	 */
	public boolean spawnEntityInWorld(final Entity par1Entity) {
		final int var2 = MathHelper.floor_double(par1Entity.posX / 16.0D);
		final int var3 = MathHelper.floor_double(par1Entity.posZ / 16.0D);
		boolean var4 = par1Entity.field_98038_p;

		if (par1Entity instanceof EntityPlayer) {
			var4 = true;
		}

		if (!var4 && !chunkExists(var2, var3)) {
			return false;
		} else {
			if (par1Entity instanceof EntityPlayer) {
				final EntityPlayer var5 = (EntityPlayer) par1Entity;
				playerEntities.add(var5);
				updateAllPlayersSleepingFlag();
			}

			getChunkFromChunkCoords(var2, var3).addEntity(par1Entity);
			loadedEntityList.add(par1Entity);
			obtainEntitySkin(par1Entity);
			return true;
		}
	}

	/**
	 * Start the skin for this entity downloading, if necessary, and increment
	 * its reference counter
	 */
	protected void obtainEntitySkin(final Entity par1Entity) {
		for (int var2 = 0; var2 < worldAccesses.size(); ++var2) {
			((IWorldAccess) worldAccesses.get(var2)).onEntityCreate(par1Entity);
		}
	}

	/**
	 * Decrement the reference counter for this entity's skin image data
	 */
	protected void releaseEntitySkin(final Entity par1Entity) {
		for (int var2 = 0; var2 < worldAccesses.size(); ++var2) {
			((IWorldAccess) worldAccesses.get(var2))
					.onEntityDestroy(par1Entity);
		}
	}

	/**
	 * Schedule the entity for removal during the next tick. Marks the entity
	 * dead in anticipation.
	 */
	public void removeEntity(final Entity par1Entity) {
		if (par1Entity.riddenByEntity != null) {
			par1Entity.riddenByEntity.mountEntity((Entity) null);
		}

		if (par1Entity.ridingEntity != null) {
			par1Entity.mountEntity((Entity) null);
		}

		par1Entity.setDead();

		if (par1Entity instanceof EntityPlayer) {
			playerEntities.remove(par1Entity);
			updateAllPlayersSleepingFlag();
		}
	}

	/**
	 * Do NOT use this method to remove normal entities- use normal removeEntity
	 */
	public void removePlayerEntityDangerously(final Entity par1Entity) {
		par1Entity.setDead();

		if (par1Entity instanceof EntityPlayer) {
			playerEntities.remove(par1Entity);
			updateAllPlayersSleepingFlag();
		}

		final int var2 = par1Entity.chunkCoordX;
		final int var3 = par1Entity.chunkCoordZ;

		if (par1Entity.addedToChunk && chunkExists(var2, var3)) {
			getChunkFromChunkCoords(var2, var3).removeEntity(par1Entity);
		}

		loadedEntityList.remove(par1Entity);
		releaseEntitySkin(par1Entity);
	}

	/**
	 * Adds a IWorldAccess to the list of worldAccesses
	 */
	public void addWorldAccess(final IWorldAccess par1IWorldAccess) {
		worldAccesses.add(par1IWorldAccess);
	}

	/**
	 * Removes a worldAccess from the worldAccesses object
	 */
	public void removeWorldAccess(final IWorldAccess par1IWorldAccess) {
		worldAccesses.remove(par1IWorldAccess);
	}

	/**
	 * Returns a list of bounding boxes that collide with aabb excluding the
	 * passed in entity's collision. Args: entity, aabb
	 */
	public List getCollidingBoundingBoxes(final Entity par1Entity,
			final AxisAlignedBB par2AxisAlignedBB) {
		collidingBoundingBoxes.clear();
		final int var3 = MathHelper.floor_double(par2AxisAlignedBB.minX);
		final int var4 = MathHelper.floor_double(par2AxisAlignedBB.maxX + 1.0D);
		final int var5 = MathHelper.floor_double(par2AxisAlignedBB.minY);
		final int var6 = MathHelper.floor_double(par2AxisAlignedBB.maxY + 1.0D);
		final int var7 = MathHelper.floor_double(par2AxisAlignedBB.minZ);
		final int var8 = MathHelper.floor_double(par2AxisAlignedBB.maxZ + 1.0D);

		for (int var9 = var3; var9 < var4; ++var9) {
			for (int var10 = var7; var10 < var8; ++var10) {
				if (blockExists(var9, 64, var10)) {
					for (int var11 = var5 - 1; var11 < var6; ++var11) {
						final Block var12 = Block.blocksList[getBlockId(var9,
								var11, var10)];

						if (var12 != null) {
							var12.addCollisionBoxesToList(this, var9, var11,
									var10, par2AxisAlignedBB,
									collidingBoundingBoxes, par1Entity);
						}
					}
				}
			}
		}

		final double var14 = 0.25D;
		final List var16 = this.getEntitiesWithinAABBExcludingEntity(
				par1Entity, par2AxisAlignedBB.expand(var14, var14, var14));

		for (int var15 = 0; var15 < var16.size(); ++var15) {
			AxisAlignedBB var13 = ((Entity) var16.get(var15)).getBoundingBox();

			if (var13 != null && var13.intersectsWith(par2AxisAlignedBB)) {
				collidingBoundingBoxes.add(var13);
			}

			var13 = par1Entity.getCollisionBox((Entity) var16.get(var15));

			if (var13 != null && var13.intersectsWith(par2AxisAlignedBB)) {
				collidingBoundingBoxes.add(var13);
			}
		}

		return collidingBoundingBoxes;
	}

	/**
	 * calculates and returns a list of colliding bounding boxes within a given
	 * AABB
	 */
	public List getCollidingBlockBounds(final AxisAlignedBB par1AxisAlignedBB) {
		collidingBoundingBoxes.clear();
		final int var2 = MathHelper.floor_double(par1AxisAlignedBB.minX);
		final int var3 = MathHelper.floor_double(par1AxisAlignedBB.maxX + 1.0D);
		final int var4 = MathHelper.floor_double(par1AxisAlignedBB.minY);
		final int var5 = MathHelper.floor_double(par1AxisAlignedBB.maxY + 1.0D);
		final int var6 = MathHelper.floor_double(par1AxisAlignedBB.minZ);
		final int var7 = MathHelper.floor_double(par1AxisAlignedBB.maxZ + 1.0D);

		for (int var8 = var2; var8 < var3; ++var8) {
			for (int var9 = var6; var9 < var7; ++var9) {
				if (blockExists(var8, 64, var9)) {
					for (int var10 = var4 - 1; var10 < var5; ++var10) {
						final Block var11 = Block.blocksList[getBlockId(var8,
								var10, var9)];

						if (var11 != null) {
							var11.addCollisionBoxesToList(this, var8, var10,
									var9, par1AxisAlignedBB,
									collidingBoundingBoxes, (Entity) null);
						}
					}
				}
			}
		}

		return collidingBoundingBoxes;
	}

	/**
	 * Returns the amount of skylight subtracted for the current time
	 */
	public int calculateSkylightSubtracted(final float par1) {
		final float var2 = getCelestialAngle(par1);
		float var3 = 1.0F - (MathHelper.cos(var2 * (float) Math.PI * 2.0F) * 2.0F + 0.5F);

		if (var3 < 0.0F) {
			var3 = 0.0F;
		}

		if (var3 > 1.0F) {
			var3 = 1.0F;
		}

		var3 = 1.0F - var3;
		var3 = (float) (var3 * (1.0D - getRainStrength(par1) * 5.0F / 16.0D));
		var3 = (float) (var3 * (1.0D - getWeightedThunderStrength(par1) * 5.0F / 16.0D));
		var3 = 1.0F - var3;
		return (int) (var3 * 11.0F);
	}

	/**
	 * Returns the sun brightness - checks time of day, rain and thunder
	 */
	public float getSunBrightness(final float par1) {
		final float var2 = getCelestialAngle(par1);
		float var3 = 1.0F - (MathHelper.cos(var2 * (float) Math.PI * 2.0F) * 2.0F + 0.2F);

		if (var3 < 0.0F) {
			var3 = 0.0F;
		}

		if (var3 > 1.0F) {
			var3 = 1.0F;
		}

		var3 = 1.0F - var3;
		var3 = (float) (var3 * (1.0D - getRainStrength(par1) * 5.0F / 16.0D));
		var3 = (float) (var3 * (1.0D - getWeightedThunderStrength(par1) * 5.0F / 16.0D));
		return var3 * 0.8F + 0.2F;
	}

	/**
	 * Calculates the color for the skybox
	 */
	public Vec3 getSkyColor(final Entity par1Entity, final float par2) {
		final float var3 = getCelestialAngle(par2);
		float var4 = MathHelper.cos(var3 * (float) Math.PI * 2.0F) * 2.0F + 0.5F;

		if (var4 < 0.0F) {
			var4 = 0.0F;
		}

		if (var4 > 1.0F) {
			var4 = 1.0F;
		}

		final int var5 = MathHelper.floor_double(par1Entity.posX);
		final int var6 = MathHelper.floor_double(par1Entity.posZ);
		final BiomeGenBase var7 = getBiomeGenForCoords(var5, var6);
		final float var8 = var7.getFloatTemperature();
		final int var9 = var7.getSkyColorByTemp(var8);
		float var10 = (var9 >> 16 & 255) / 255.0F;
		float var11 = (var9 >> 8 & 255) / 255.0F;
		float var12 = (var9 & 255) / 255.0F;
		var10 *= var4;
		var11 *= var4;
		var12 *= var4;
		final float var13 = getRainStrength(par2);
		float var14;
		float var15;

		if (var13 > 0.0F) {
			var14 = (var10 * 0.3F + var11 * 0.59F + var12 * 0.11F) * 0.6F;
			var15 = 1.0F - var13 * 0.75F;
			var10 = var10 * var15 + var14 * (1.0F - var15);
			var11 = var11 * var15 + var14 * (1.0F - var15);
			var12 = var12 * var15 + var14 * (1.0F - var15);
		}

		var14 = getWeightedThunderStrength(par2);

		if (var14 > 0.0F) {
			var15 = (var10 * 0.3F + var11 * 0.59F + var12 * 0.11F) * 0.2F;
			final float var16 = 1.0F - var14 * 0.75F;
			var10 = var10 * var16 + var15 * (1.0F - var16);
			var11 = var11 * var16 + var15 * (1.0F - var16);
			var12 = var12 * var16 + var15 * (1.0F - var16);
		}

		if (lastLightningBolt > 0) {
			var15 = lastLightningBolt - par2;

			if (var15 > 1.0F) {
				var15 = 1.0F;
			}

			var15 *= 0.45F;
			var10 = var10 * (1.0F - var15) + 0.8F * var15;
			var11 = var11 * (1.0F - var15) + 0.8F * var15;
			var12 = var12 * (1.0F - var15) + 1.0F * var15;
		}

		return getWorldVec3Pool().getVecFromPool(var10, var11, var12);
	}

	/**
	 * calls calculateCelestialAngle
	 */
	public float getCelestialAngle(final float par1) {
		return provider.calculateCelestialAngle(worldInfo.getWorldTime(), par1);
	}

	public int getMoonPhase() {
		return provider.getMoonPhase(worldInfo.getWorldTime());
	}

	/**
	 * Return getCelestialAngle()*2*PI
	 */
	public float getCelestialAngleRadians(final float par1) {
		final float var2 = getCelestialAngle(par1);
		return var2 * (float) Math.PI * 2.0F;
	}

	public Vec3 getCloudColour(final float par1) {
		final float var2 = getCelestialAngle(par1);
		float var3 = MathHelper.cos(var2 * (float) Math.PI * 2.0F) * 2.0F + 0.5F;

		if (var3 < 0.0F) {
			var3 = 0.0F;
		}

		if (var3 > 1.0F) {
			var3 = 1.0F;
		}

		float var4 = (cloudColour >> 16 & 255L) / 255.0F;
		float var5 = (cloudColour >> 8 & 255L) / 255.0F;
		float var6 = (cloudColour & 255L) / 255.0F;
		final float var7 = getRainStrength(par1);
		float var8;
		float var9;

		if (var7 > 0.0F) {
			var8 = (var4 * 0.3F + var5 * 0.59F + var6 * 0.11F) * 0.6F;
			var9 = 1.0F - var7 * 0.95F;
			var4 = var4 * var9 + var8 * (1.0F - var9);
			var5 = var5 * var9 + var8 * (1.0F - var9);
			var6 = var6 * var9 + var8 * (1.0F - var9);
		}

		var4 *= var3 * 0.9F + 0.1F;
		var5 *= var3 * 0.9F + 0.1F;
		var6 *= var3 * 0.85F + 0.15F;
		var8 = getWeightedThunderStrength(par1);

		if (var8 > 0.0F) {
			var9 = (var4 * 0.3F + var5 * 0.59F + var6 * 0.11F) * 0.2F;
			final float var10 = 1.0F - var8 * 0.95F;
			var4 = var4 * var10 + var9 * (1.0F - var10);
			var5 = var5 * var10 + var9 * (1.0F - var10);
			var6 = var6 * var10 + var9 * (1.0F - var10);
		}

		return getWorldVec3Pool().getVecFromPool(var4, var5, var6);
	}

	/**
	 * Returns vector(ish) with R/G/B for fog
	 */
	public Vec3 getFogColor(final float par1) {
		final float var2 = getCelestialAngle(par1);
		return provider.getFogColor(var2, par1);
	}

	/**
	 * Gets the height to which rain/snow will fall. Calculates it if not
	 * already stored.
	 */
	public int getPrecipitationHeight(final int par1, final int par2) {
		return getChunkFromBlockCoords(par1, par2).getPrecipitationHeight(
				par1 & 15, par2 & 15);
	}

	/**
	 * Finds the highest block on the x, z coordinate that is solid and returns
	 * its y coord. Args x, z
	 */
	public int getTopSolidOrLiquidBlock(int par1, int par2) {
		final Chunk var3 = getChunkFromBlockCoords(par1, par2);
		int var4 = var3.getTopFilledSegment() + 15;
		par1 &= 15;

		for (par2 &= 15; var4 > 0; --var4) {
			final int var5 = var3.getBlockID(par1, var4, par2);

			if (var5 != 0
					&& Block.blocksList[var5].blockMaterial.blocksMovement()
					&& Block.blocksList[var5].blockMaterial != Material.leaves) {
				return var4 + 1;
			}
		}

		return -1;
	}

	/**
	 * How bright are stars in the sky
	 */
	public float getStarBrightness(final float par1) {
		final float var2 = getCelestialAngle(par1);
		float var3 = 1.0F - (MathHelper.cos(var2 * (float) Math.PI * 2.0F) * 2.0F + 0.25F);

		if (var3 < 0.0F) {
			var3 = 0.0F;
		}

		if (var3 > 1.0F) {
			var3 = 1.0F;
		}

		return var3 * var3 * 0.5F;
	}

	/**
	 * Schedules a tick to a block with a delay (Most commonly the tick rate)
	 */
	public void scheduleBlockUpdate(final int par1, final int par2,
			final int par3, final int par4, final int par5) {
	}

	public void func_82740_a(final int par1, final int par2, final int par3,
			final int par4, final int par5, final int par6) {
	}

	/**
	 * Schedules a block update from the saved information in a chunk. Called
	 * when the chunk is loaded.
	 */
	public void scheduleBlockUpdateFromLoad(final int par1, final int par2,
			final int par3, final int par4, final int par5, final int par6) {
	}

	/**
	 * Updates (and cleans up) entities and tile entities
	 */
	public void updateEntities() {
		theProfiler.startSection("entities");
		theProfiler.startSection("global");
		int var1;
		Entity var2;
		CrashReport var4;
		CrashReportCategory var5;

		for (var1 = 0; var1 < weatherEffects.size(); ++var1) {
			var2 = (Entity) weatherEffects.get(var1);

			try {
				++var2.ticksExisted;
				var2.onUpdate();
			} catch (final Throwable var8) {
				var4 = CrashReport.makeCrashReport(var8, "Ticking entity");
				var5 = var4.makeCategory("Entity being ticked");

				if (var2 == null) {
					var5.addCrashSection("Entity", "~~NULL~~");
				} else {
					var2.func_85029_a(var5);
				}

				throw new ReportedException(var4);
			}

			if (var2.isDead) {
				weatherEffects.remove(var1--);
			}
		}

		theProfiler.endStartSection("remove");
		loadedEntityList.removeAll(unloadedEntityList);
		int var3;
		int var13;

		for (var1 = 0; var1 < unloadedEntityList.size(); ++var1) {
			var2 = (Entity) unloadedEntityList.get(var1);
			var3 = var2.chunkCoordX;
			var13 = var2.chunkCoordZ;

			if (var2.addedToChunk && chunkExists(var3, var13)) {
				getChunkFromChunkCoords(var3, var13).removeEntity(var2);
			}
		}

		for (var1 = 0; var1 < unloadedEntityList.size(); ++var1) {
			releaseEntitySkin((Entity) unloadedEntityList.get(var1));
		}

		unloadedEntityList.clear();
		theProfiler.endStartSection("regular");

		for (var1 = 0; var1 < loadedEntityList.size(); ++var1) {
			var2 = (Entity) loadedEntityList.get(var1);

			if (var2.ridingEntity != null) {
				if (!var2.ridingEntity.isDead
						&& var2.ridingEntity.riddenByEntity == var2) {
					continue;
				}

				var2.ridingEntity.riddenByEntity = null;
				var2.ridingEntity = null;
			}

			theProfiler.startSection("tick");

			if (!var2.isDead) {
				try {
					updateEntity(var2);
				} catch (final Throwable var7) {
					var4 = CrashReport.makeCrashReport(var7, "Ticking entity");
					var5 = var4.makeCategory("Entity being ticked");
					var2.func_85029_a(var5);
					throw new ReportedException(var4);
				}
			}

			theProfiler.endSection();
			theProfiler.startSection("remove");

			if (var2.isDead) {
				var3 = var2.chunkCoordX;
				var13 = var2.chunkCoordZ;

				if (var2.addedToChunk && chunkExists(var3, var13)) {
					getChunkFromChunkCoords(var3, var13).removeEntity(var2);
				}

				loadedEntityList.remove(var1--);
				releaseEntitySkin(var2);
			}

			theProfiler.endSection();
		}

		theProfiler.endStartSection("tileEntities");
		scanningTileEntities = true;
		final Iterator var14 = loadedTileEntityList.iterator();

		while (var14.hasNext()) {
			final TileEntity var9 = (TileEntity) var14.next();

			if (!var9.isInvalid() && var9.func_70309_m()
					&& blockExists(var9.xCoord, var9.yCoord, var9.zCoord)) {
				try {
					var9.updateEntity();
				} catch (final Throwable var6) {
					var4 = CrashReport.makeCrashReport(var6,
							"Ticking tile entity");
					var5 = var4.makeCategory("Tile entity being ticked");
					var9.func_85027_a(var5);
					throw new ReportedException(var4);
				}
			}

			if (var9.isInvalid()) {
				var14.remove();

				if (chunkExists(var9.xCoord >> 4, var9.zCoord >> 4)) {
					final Chunk var11 = getChunkFromChunkCoords(
							var9.xCoord >> 4, var9.zCoord >> 4);

					if (var11 != null) {
						var11.removeChunkBlockTileEntity(var9.xCoord & 15,
								var9.yCoord, var9.zCoord & 15);
					}
				}
			}
		}

		scanningTileEntities = false;

		if (!entityRemoval.isEmpty()) {
			loadedTileEntityList.removeAll(entityRemoval);
			entityRemoval.clear();
		}

		theProfiler.endStartSection("pendingTileEntities");

		if (!addedTileEntityList.isEmpty()) {
			for (int var10 = 0; var10 < addedTileEntityList.size(); ++var10) {
				final TileEntity var12 = (TileEntity) addedTileEntityList
						.get(var10);

				if (!var12.isInvalid()) {
					if (!loadedTileEntityList.contains(var12)) {
						loadedTileEntityList.add(var12);
					}

					if (chunkExists(var12.xCoord >> 4, var12.zCoord >> 4)) {
						final Chunk var15 = getChunkFromChunkCoords(
								var12.xCoord >> 4, var12.zCoord >> 4);

						if (var15 != null) {
							var15.setChunkBlockTileEntity(var12.xCoord & 15,
									var12.yCoord, var12.zCoord & 15, var12);
						}
					}

					markBlockForUpdate(var12.xCoord, var12.yCoord, var12.zCoord);
				}
			}

			addedTileEntityList.clear();
		}

		theProfiler.endSection();
		theProfiler.endSection();
	}

	public void addTileEntity(final Collection par1Collection) {
		if (scanningTileEntities) {
			addedTileEntityList.addAll(par1Collection);
		} else {
			loadedTileEntityList.addAll(par1Collection);
		}
	}

	/**
	 * Will update the entity in the world if the chunk the entity is in is
	 * currently loaded. Args: entity
	 */
	public void updateEntity(final Entity par1Entity) {
		updateEntityWithOptionalForce(par1Entity, true);
	}

	/**
	 * Will update the entity in the world if the chunk the entity is in is
	 * currently loaded or its forced to update. Args: entity, forceUpdate
	 */
	public void updateEntityWithOptionalForce(final Entity par1Entity,
			final boolean par2) {
		final int var3 = MathHelper.floor_double(par1Entity.posX);
		final int var4 = MathHelper.floor_double(par1Entity.posZ);
		final byte var5 = 32;

		if (!par2
				|| checkChunksExist(var3 - var5, 0, var4 - var5, var3 + var5,
						0, var4 + var5)) {
			par1Entity.lastTickPosX = par1Entity.posX;
			par1Entity.lastTickPosY = par1Entity.posY;
			par1Entity.lastTickPosZ = par1Entity.posZ;
			par1Entity.prevRotationYaw = par1Entity.rotationYaw;
			par1Entity.prevRotationPitch = par1Entity.rotationPitch;

			if (par2 && par1Entity.addedToChunk) {
				if (par1Entity.ridingEntity != null) {
					par1Entity.updateRidden();
				} else {
					++par1Entity.ticksExisted;
					par1Entity.onUpdate();
				}
			}

			theProfiler.startSection("chunkCheck");

			if (Double.isNaN(par1Entity.posX)
					|| Double.isInfinite(par1Entity.posX)) {
				par1Entity.posX = par1Entity.lastTickPosX;
			}

			if (Double.isNaN(par1Entity.posY)
					|| Double.isInfinite(par1Entity.posY)) {
				par1Entity.posY = par1Entity.lastTickPosY;
			}

			if (Double.isNaN(par1Entity.posZ)
					|| Double.isInfinite(par1Entity.posZ)) {
				par1Entity.posZ = par1Entity.lastTickPosZ;
			}

			if (Double.isNaN(par1Entity.rotationPitch)
					|| Double.isInfinite(par1Entity.rotationPitch)) {
				par1Entity.rotationPitch = par1Entity.prevRotationPitch;
			}

			if (Double.isNaN(par1Entity.rotationYaw)
					|| Double.isInfinite(par1Entity.rotationYaw)) {
				par1Entity.rotationYaw = par1Entity.prevRotationYaw;
			}

			final int var6 = MathHelper.floor_double(par1Entity.posX / 16.0D);
			final int var7 = MathHelper.floor_double(par1Entity.posY / 16.0D);
			final int var8 = MathHelper.floor_double(par1Entity.posZ / 16.0D);

			if (!par1Entity.addedToChunk || par1Entity.chunkCoordX != var6
					|| par1Entity.chunkCoordY != var7
					|| par1Entity.chunkCoordZ != var8) {
				if (par1Entity.addedToChunk
						&& chunkExists(par1Entity.chunkCoordX,
								par1Entity.chunkCoordZ)) {
					getChunkFromChunkCoords(par1Entity.chunkCoordX,
							par1Entity.chunkCoordZ).removeEntityAtIndex(
							par1Entity, par1Entity.chunkCoordY);
				}

				if (chunkExists(var6, var8)) {
					par1Entity.addedToChunk = true;
					getChunkFromChunkCoords(var6, var8).addEntity(par1Entity);
				} else {
					par1Entity.addedToChunk = false;
				}
			}

			theProfiler.endSection();

			if (par2 && par1Entity.addedToChunk
					&& par1Entity.riddenByEntity != null) {
				if (!par1Entity.riddenByEntity.isDead
						&& par1Entity.riddenByEntity.ridingEntity == par1Entity) {
					updateEntity(par1Entity.riddenByEntity);
				} else {
					par1Entity.riddenByEntity.ridingEntity = null;
					par1Entity.riddenByEntity = null;
				}
			}
		}
	}

	/**
	 * Returns true if there are no solid, live entities in the specified
	 * AxisAlignedBB
	 */
	public boolean checkNoEntityCollision(final AxisAlignedBB par1AxisAlignedBB) {
		return this.checkNoEntityCollision(par1AxisAlignedBB, (Entity) null);
	}

	/**
	 * Returns true if there are no solid, live entities in the specified
	 * AxisAlignedBB, excluding the given entity
	 */
	public boolean checkNoEntityCollision(
			final AxisAlignedBB par1AxisAlignedBB, final Entity par2Entity) {
		final List var3 = this.getEntitiesWithinAABBExcludingEntity(
				(Entity) null, par1AxisAlignedBB);

		for (int var4 = 0; var4 < var3.size(); ++var4) {
			final Entity var5 = (Entity) var3.get(var4);

			if (!var5.isDead && var5.preventEntitySpawning
					&& var5 != par2Entity) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Returns true if there are any blocks in the region constrained by an
	 * AxisAlignedBB
	 */
	public boolean checkBlockCollision(final AxisAlignedBB par1AxisAlignedBB) {
		int var2 = MathHelper.floor_double(par1AxisAlignedBB.minX);
		final int var3 = MathHelper.floor_double(par1AxisAlignedBB.maxX + 1.0D);
		int var4 = MathHelper.floor_double(par1AxisAlignedBB.minY);
		final int var5 = MathHelper.floor_double(par1AxisAlignedBB.maxY + 1.0D);
		int var6 = MathHelper.floor_double(par1AxisAlignedBB.minZ);
		final int var7 = MathHelper.floor_double(par1AxisAlignedBB.maxZ + 1.0D);

		if (par1AxisAlignedBB.minX < 0.0D) {
			--var2;
		}

		if (par1AxisAlignedBB.minY < 0.0D) {
			--var4;
		}

		if (par1AxisAlignedBB.minZ < 0.0D) {
			--var6;
		}

		for (int var8 = var2; var8 < var3; ++var8) {
			for (int var9 = var4; var9 < var5; ++var9) {
				for (int var10 = var6; var10 < var7; ++var10) {
					final Block var11 = Block.blocksList[getBlockId(var8, var9,
							var10)];

					if (var11 != null) {
						return true;
					}
				}
			}
		}

		return false;
	}

	/**
	 * Returns if any of the blocks within the aabb are liquids. Args: aabb
	 */
	public boolean isAnyLiquid(final AxisAlignedBB par1AxisAlignedBB) {
		int var2 = MathHelper.floor_double(par1AxisAlignedBB.minX);
		final int var3 = MathHelper.floor_double(par1AxisAlignedBB.maxX + 1.0D);
		int var4 = MathHelper.floor_double(par1AxisAlignedBB.minY);
		final int var5 = MathHelper.floor_double(par1AxisAlignedBB.maxY + 1.0D);
		int var6 = MathHelper.floor_double(par1AxisAlignedBB.minZ);
		final int var7 = MathHelper.floor_double(par1AxisAlignedBB.maxZ + 1.0D);

		if (par1AxisAlignedBB.minX < 0.0D) {
			--var2;
		}

		if (par1AxisAlignedBB.minY < 0.0D) {
			--var4;
		}

		if (par1AxisAlignedBB.minZ < 0.0D) {
			--var6;
		}

		for (int var8 = var2; var8 < var3; ++var8) {
			for (int var9 = var4; var9 < var5; ++var9) {
				for (int var10 = var6; var10 < var7; ++var10) {
					final Block var11 = Block.blocksList[getBlockId(var8, var9,
							var10)];

					if (var11 != null && var11.blockMaterial.isLiquid()) {
						return true;
					}
				}
			}
		}

		return false;
	}

	/**
	 * Returns whether or not the given bounding box is on fire or not
	 */
	public boolean isBoundingBoxBurning(final AxisAlignedBB par1AxisAlignedBB) {
		final int var2 = MathHelper.floor_double(par1AxisAlignedBB.minX);
		final int var3 = MathHelper.floor_double(par1AxisAlignedBB.maxX + 1.0D);
		final int var4 = MathHelper.floor_double(par1AxisAlignedBB.minY);
		final int var5 = MathHelper.floor_double(par1AxisAlignedBB.maxY + 1.0D);
		final int var6 = MathHelper.floor_double(par1AxisAlignedBB.minZ);
		final int var7 = MathHelper.floor_double(par1AxisAlignedBB.maxZ + 1.0D);

		if (checkChunksExist(var2, var4, var6, var3, var5, var7)) {
			for (int var8 = var2; var8 < var3; ++var8) {
				for (int var9 = var4; var9 < var5; ++var9) {
					for (int var10 = var6; var10 < var7; ++var10) {
						final int var11 = getBlockId(var8, var9, var10);

						if (var11 == Block.fire.blockID
								|| var11 == Block.lavaMoving.blockID
								|| var11 == Block.lavaStill.blockID) {
							return true;
						}
					}
				}
			}
		}

		return false;
	}

	/**
	 * handles the acceleration of an object whilst in water. Not sure if it is
	 * used elsewhere.
	 */
	public boolean handleMaterialAcceleration(
			final AxisAlignedBB par1AxisAlignedBB, final Material par2Material,
			final Entity par3Entity) {
		final int var4 = MathHelper.floor_double(par1AxisAlignedBB.minX);
		final int var5 = MathHelper.floor_double(par1AxisAlignedBB.maxX + 1.0D);
		final int var6 = MathHelper.floor_double(par1AxisAlignedBB.minY);
		final int var7 = MathHelper.floor_double(par1AxisAlignedBB.maxY + 1.0D);
		final int var8 = MathHelper.floor_double(par1AxisAlignedBB.minZ);
		final int var9 = MathHelper.floor_double(par1AxisAlignedBB.maxZ + 1.0D);

		if (!checkChunksExist(var4, var6, var8, var5, var7, var9)) {
			return false;
		} else {
			boolean var10 = false;
			Vec3 var11 = getWorldVec3Pool().getVecFromPool(0.0D, 0.0D, 0.0D);

			for (int var12 = var4; var12 < var5; ++var12) {
				for (int var13 = var6; var13 < var7; ++var13) {
					for (int var14 = var8; var14 < var9; ++var14) {
						final Block var15 = Block.blocksList[getBlockId(var12,
								var13, var14)];

						if (var15 != null
								&& var15.blockMaterial == par2Material) {
							final double var16 = var13
									+ 1
									- BlockFluid
											.getFluidHeightPercent(getBlockMetadata(
													var12, var13, var14));

							if (var7 >= var16) {
								var10 = true;
								var15.velocityToAddToEntity(this, var12, var13,
										var14, par3Entity, var11);
							}
						}
					}
				}
			}

			if (var11.lengthVector() > 0.0D && par3Entity.func_96092_aw()) {
				var11 = var11.normalize();
				final double var18 = 0.014D;
				par3Entity.motionX += var11.xCoord * var18;
				par3Entity.motionY += var11.yCoord * var18;
				par3Entity.motionZ += var11.zCoord * var18;
			}

			return var10;
		}
	}

	/**
	 * Returns true if the given bounding box contains the given material
	 */
	public boolean isMaterialInBB(final AxisAlignedBB par1AxisAlignedBB,
			final Material par2Material) {
		final int var3 = MathHelper.floor_double(par1AxisAlignedBB.minX);
		final int var4 = MathHelper.floor_double(par1AxisAlignedBB.maxX + 1.0D);
		final int var5 = MathHelper.floor_double(par1AxisAlignedBB.minY);
		final int var6 = MathHelper.floor_double(par1AxisAlignedBB.maxY + 1.0D);
		final int var7 = MathHelper.floor_double(par1AxisAlignedBB.minZ);
		final int var8 = MathHelper.floor_double(par1AxisAlignedBB.maxZ + 1.0D);

		for (int var9 = var3; var9 < var4; ++var9) {
			for (int var10 = var5; var10 < var6; ++var10) {
				for (int var11 = var7; var11 < var8; ++var11) {
					final Block var12 = Block.blocksList[getBlockId(var9,
							var10, var11)];

					if (var12 != null && var12.blockMaterial == par2Material) {
						return true;
					}
				}
			}
		}

		return false;
	}

	/**
	 * checks if the given AABB is in the material given. Used while swimming.
	 */
	public boolean isAABBInMaterial(final AxisAlignedBB par1AxisAlignedBB,
			final Material par2Material) {
		final int var3 = MathHelper.floor_double(par1AxisAlignedBB.minX);
		final int var4 = MathHelper.floor_double(par1AxisAlignedBB.maxX + 1.0D);
		final int var5 = MathHelper.floor_double(par1AxisAlignedBB.minY);
		final int var6 = MathHelper.floor_double(par1AxisAlignedBB.maxY + 1.0D);
		final int var7 = MathHelper.floor_double(par1AxisAlignedBB.minZ);
		final int var8 = MathHelper.floor_double(par1AxisAlignedBB.maxZ + 1.0D);

		for (int var9 = var3; var9 < var4; ++var9) {
			for (int var10 = var5; var10 < var6; ++var10) {
				for (int var11 = var7; var11 < var8; ++var11) {
					final Block var12 = Block.blocksList[getBlockId(var9,
							var10, var11)];

					if (var12 != null && var12.blockMaterial == par2Material) {
						final int var13 = getBlockMetadata(var9, var10, var11);
						double var14 = var10 + 1;

						if (var13 < 8) {
							var14 = var10 + 1 - var13 / 8.0D;
						}

						if (var14 >= par1AxisAlignedBB.minY) {
							return true;
						}
					}
				}
			}
		}

		return false;
	}

	/**
	 * Creates an explosion. Args: entity, x, y, z, strength
	 */
	public Explosion createExplosion(final Entity par1Entity,
			final double par2, final double par4, final double par6,
			final float par8, final boolean par9) {
		return newExplosion(par1Entity, par2, par4, par6, par8, false, par9);
	}

	/**
	 * returns a new explosion. Does initiation (at time of writing Explosion is
	 * not finished)
	 */
	public Explosion newExplosion(final Entity par1Entity, final double par2,
			final double par4, final double par6, final float par8,
			final boolean par9, final boolean par10) {
		final Explosion var11 = new Explosion(this, par1Entity, par2, par4,
				par6, par8);
		var11.isFlaming = par9;
		var11.isSmoking = par10;
		var11.doExplosionA();
		var11.doExplosionB(true);
		return var11;
	}

	/**
	 * Gets the percentage of real blocks within within a bounding box, along a
	 * specified vector.
	 */
	public float getBlockDensity(final Vec3 par1Vec3,
			final AxisAlignedBB par2AxisAlignedBB) {
		final double var3 = 1.0D / ((par2AxisAlignedBB.maxX - par2AxisAlignedBB.minX) * 2.0D + 1.0D);
		final double var5 = 1.0D / ((par2AxisAlignedBB.maxY - par2AxisAlignedBB.minY) * 2.0D + 1.0D);
		final double var7 = 1.0D / ((par2AxisAlignedBB.maxZ - par2AxisAlignedBB.minZ) * 2.0D + 1.0D);
		int var9 = 0;
		int var10 = 0;

		for (float var11 = 0.0F; var11 <= 1.0F; var11 = (float) (var11 + var3)) {
			for (float var12 = 0.0F; var12 <= 1.0F; var12 = (float) (var12 + var5)) {
				for (float var13 = 0.0F; var13 <= 1.0F; var13 = (float) (var13 + var7)) {
					final double var14 = par2AxisAlignedBB.minX
							+ (par2AxisAlignedBB.maxX - par2AxisAlignedBB.minX)
							* var11;
					final double var16 = par2AxisAlignedBB.minY
							+ (par2AxisAlignedBB.maxY - par2AxisAlignedBB.minY)
							* var12;
					final double var18 = par2AxisAlignedBB.minZ
							+ (par2AxisAlignedBB.maxZ - par2AxisAlignedBB.minZ)
							* var13;

					if (rayTraceBlocks(
							getWorldVec3Pool().getVecFromPool(var14, var16,
									var18), par1Vec3) == null) {
						++var9;
					}

					++var10;
				}
			}
		}

		return (float) var9 / (float) var10;
	}

	/**
	 * If the block in the given direction of the given coordinate is fire,
	 * extinguish it. Args: Player, X,Y,Z, blockDirection
	 */
	public boolean extinguishFire(final EntityPlayer par1EntityPlayer,
			int par2, int par3, int par4, final int par5) {
		if (par5 == 0) {
			--par3;
		}

		if (par5 == 1) {
			++par3;
		}

		if (par5 == 2) {
			--par4;
		}

		if (par5 == 3) {
			++par4;
		}

		if (par5 == 4) {
			--par2;
		}

		if (par5 == 5) {
			++par2;
		}

		if (getBlockId(par2, par3, par4) == Block.fire.blockID) {
			playAuxSFXAtEntity(par1EntityPlayer, 1004, par2, par3, par4, 0);
			setBlockToAir(par2, par3, par4);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * This string is 'All: (number of loaded entities)' Viewable by press ing
	 * F3
	 */
	public String getDebugLoadedEntities() {
		return "All: " + loadedEntityList.size();
	}

	/**
	 * Returns the name of the current chunk provider, by calling
	 * chunkprovider.makeString()
	 */
	public String getProviderName() {
		return chunkProvider.makeString();
	}

	/**
	 * Returns the TileEntity associated with a given block in X,Y,Z
	 * coordinates, or null if no TileEntity exists
	 */
	@Override
	public TileEntity getBlockTileEntity(final int par1, final int par2,
			final int par3) {
		if (par2 >= 0 && par2 < 256) {
			TileEntity var4 = null;
			int var5;
			TileEntity var6;

			if (scanningTileEntities) {
				for (var5 = 0; var5 < addedTileEntityList.size(); ++var5) {
					var6 = (TileEntity) addedTileEntityList.get(var5);

					if (!var6.isInvalid() && var6.xCoord == par1
							&& var6.yCoord == par2 && var6.zCoord == par3) {
						var4 = var6;
						break;
					}
				}
			}

			if (var4 == null) {
				final Chunk var7 = getChunkFromChunkCoords(par1 >> 4, par3 >> 4);

				if (var7 != null) {
					var4 = var7.getChunkBlockTileEntity(par1 & 15, par2,
							par3 & 15);
				}
			}

			if (var4 == null) {
				for (var5 = 0; var5 < addedTileEntityList.size(); ++var5) {
					var6 = (TileEntity) addedTileEntityList.get(var5);

					if (!var6.isInvalid() && var6.xCoord == par1
							&& var6.yCoord == par2 && var6.zCoord == par3) {
						var4 = var6;
						break;
					}
				}
			}

			return var4;
		} else {
			return null;
		}
	}

	/**
	 * Sets the TileEntity for a given block in X, Y, Z coordinates
	 */
	public void setBlockTileEntity(final int par1, final int par2,
			final int par3, final TileEntity par4TileEntity) {
		if (par4TileEntity != null && !par4TileEntity.isInvalid()) {
			if (scanningTileEntities) {
				par4TileEntity.xCoord = par1;
				par4TileEntity.yCoord = par2;
				par4TileEntity.zCoord = par3;
				final Iterator var5 = addedTileEntityList.iterator();

				while (var5.hasNext()) {
					final TileEntity var6 = (TileEntity) var5.next();

					if (var6.xCoord == par1 && var6.yCoord == par2
							&& var6.zCoord == par3) {
						var6.invalidate();
						var5.remove();
					}
				}

				addedTileEntityList.add(par4TileEntity);
			} else {
				loadedTileEntityList.add(par4TileEntity);
				final Chunk var7 = getChunkFromChunkCoords(par1 >> 4, par3 >> 4);

				if (var7 != null) {
					var7.setChunkBlockTileEntity(par1 & 15, par2, par3 & 15,
							par4TileEntity);
				}
			}
		}
	}

	/**
	 * Removes the TileEntity for a given block in X,Y,Z coordinates
	 */
	public void removeBlockTileEntity(final int par1, final int par2,
			final int par3) {
		final TileEntity var4 = getBlockTileEntity(par1, par2, par3);

		if (var4 != null && scanningTileEntities) {
			var4.invalidate();
			addedTileEntityList.remove(var4);
		} else {
			if (var4 != null) {
				addedTileEntityList.remove(var4);
				loadedTileEntityList.remove(var4);
			}

			final Chunk var5 = getChunkFromChunkCoords(par1 >> 4, par3 >> 4);

			if (var5 != null) {
				var5.removeChunkBlockTileEntity(par1 & 15, par2, par3 & 15);
			}
		}
	}

	/**
	 * adds tile entity to despawn list (renamed from markEntityForDespawn)
	 */
	public void markTileEntityForDespawn(final TileEntity par1TileEntity) {
		entityRemoval.add(par1TileEntity);
	}

	/**
	 * Returns true if the block at the specified coordinates is an opaque cube.
	 * Args: x, y, z
	 */
	@Override
	public boolean isBlockOpaqueCube(final int par1, final int par2,
			final int par3) {
		final Block var4 = Block.blocksList[getBlockId(par1, par2, par3)];
		return var4 == null ? false : var4.isOpaqueCube();
	}

	/**
	 * Indicate if a material is a normal solid opaque cube.
	 */
	@Override
	public boolean isBlockNormalCube(final int par1, final int par2,
			final int par3) {
		return Block.isNormalCube(getBlockId(par1, par2, par3));
	}

	public boolean func_85174_u(final int par1, final int par2, final int par3) {
		final int var4 = getBlockId(par1, par2, par3);

		if (var4 != 0 && Block.blocksList[var4] != null) {
			final AxisAlignedBB var5 = Block.blocksList[var4]
					.getCollisionBoundingBoxFromPool(this, par1, par2, par3);
			return var5 != null && var5.getAverageEdgeLength() >= 1.0D;
		} else {
			return false;
		}
	}

	/**
	 * Returns true if the block at the given coordinate has a solid (buildable)
	 * top surface.
	 */
	@Override
	public boolean doesBlockHaveSolidTopSurface(final int par1, final int par2,
			final int par3) {
		final Block var4 = Block.blocksList[getBlockId(par1, par2, par3)];
		return isBlockTopFacingSurfaceSolid(var4,
				getBlockMetadata(par1, par2, par3));
	}

	/**
	 * Performs check to see if the block is a normal, solid block, or if the
	 * metadata of the block indicates that its facing puts its solid side
	 * upwards. (inverted stairs, for example)
	 */
	public boolean isBlockTopFacingSurfaceSolid(final Block par1Block,
			final int par2) {
		return par1Block == null ? false
				: par1Block.blockMaterial.isOpaque()
						&& par1Block.renderAsNormalBlock() ? true
						: par1Block instanceof BlockStairs ? (par2 & 4) == 4
								: par1Block instanceof BlockHalfSlab ? (par2 & 8) == 8
										: par1Block instanceof BlockHopper ? true
												: par1Block instanceof BlockSnow ? (par2 & 7) == 7
														: false;
	}

	/**
	 * Checks if the block is a solid, normal cube. If the chunk does not exist,
	 * or is not loaded, it returns the boolean parameter.
	 */
	public boolean isBlockNormalCubeDefault(final int par1, final int par2,
			final int par3, final boolean par4) {
		if (par1 >= -30000000 && par3 >= -30000000 && par1 < 30000000
				&& par3 < 30000000) {
			final Chunk var5 = chunkProvider.provideChunk(par1 >> 4, par3 >> 4);

			if (var5 != null && !var5.isEmpty()) {
				final Block var6 = Block.blocksList[getBlockId(par1, par2, par3)];
				return var6 == null ? false : var6.blockMaterial.isOpaque()
						&& var6.renderAsNormalBlock();
			} else {
				return par4;
			}
		} else {
			return par4;
		}
	}

	/**
	 * Called on construction of the World class to setup the initial skylight
	 * values
	 */
	public void calculateInitialSkylight() {
		final int var1 = calculateSkylightSubtracted(1.0F);

		if (var1 != skylightSubtracted) {
			skylightSubtracted = var1;
		}
	}

	/**
	 * Set which types of mobs are allowed to spawn (peaceful vs hostile).
	 */
	public void setAllowedSpawnTypes(final boolean par1, final boolean par2) {
		spawnHostileMobs = par1;
		spawnPeacefulMobs = par2;
	}

	/**
	 * Runs a single tick for the world
	 */
	public void tick() {
		updateWeather();
	}

	/**
	 * Called from World constructor to set rainingStrength and
	 * thunderingStrength
	 */
	private void calculateInitialWeather() {
		if (worldInfo.isRaining()) {
			rainingStrength = 1.0F;

			if (worldInfo.isThundering()) {
				thunderingStrength = 1.0F;
			}
		}
	}

	/**
	 * Updates all weather states.
	 */
	protected void updateWeather() {
		if (!provider.hasNoSky) {
			int var1 = worldInfo.getThunderTime();

			if (var1 <= 0) {
				if (worldInfo.isThundering()) {
					worldInfo.setThunderTime(rand.nextInt(12000) + 3600);
				} else {
					worldInfo.setThunderTime(rand.nextInt(168000) + 12000);
				}
			} else {
				--var1;
				worldInfo.setThunderTime(var1);

				if (var1 <= 0) {
					worldInfo.setThundering(!worldInfo.isThundering());
				}
			}

			int var2 = worldInfo.getRainTime();

			if (var2 <= 0) {
				if (worldInfo.isRaining()) {
					worldInfo.setRainTime(rand.nextInt(12000) + 12000);
				} else {
					worldInfo.setRainTime(rand.nextInt(168000) + 12000);
				}
			} else {
				--var2;
				worldInfo.setRainTime(var2);

				if (var2 <= 0) {
					worldInfo.setRaining(!worldInfo.isRaining());
				}
			}

			prevRainingStrength = rainingStrength;

			if (worldInfo.isRaining()) {
				rainingStrength = (float) (rainingStrength + 0.01D);
			} else {
				rainingStrength = (float) (rainingStrength - 0.01D);
			}

			if (rainingStrength < 0.0F) {
				rainingStrength = 0.0F;
			}

			if (rainingStrength > 1.0F) {
				rainingStrength = 1.0F;
			}

			prevThunderingStrength = thunderingStrength;

			if (worldInfo.isThundering()) {
				thunderingStrength = (float) (thunderingStrength + 0.01D);
			} else {
				thunderingStrength = (float) (thunderingStrength - 0.01D);
			}

			if (thunderingStrength < 0.0F) {
				thunderingStrength = 0.0F;
			}

			if (thunderingStrength > 1.0F) {
				thunderingStrength = 1.0F;
			}
		}
	}

	public void toggleRain() {
		worldInfo.setRainTime(1);
	}

	protected void setActivePlayerChunksAndCheckLight() {
		activeChunkSet.clear();
		theProfiler.startSection("buildList");
		int var1;
		EntityPlayer var2;
		int var3;
		int var4;

		for (var1 = 0; var1 < playerEntities.size(); ++var1) {
			var2 = (EntityPlayer) playerEntities.get(var1);
			var3 = MathHelper.floor_double(var2.posX / 16.0D);
			var4 = MathHelper.floor_double(var2.posZ / 16.0D);
			final byte var5 = 7;

			for (int var6 = -var5; var6 <= var5; ++var6) {
				for (int var7 = -var5; var7 <= var5; ++var7) {
					activeChunkSet.add(new ChunkCoordIntPair(var6 + var3, var7
							+ var4));
				}
			}
		}

		theProfiler.endSection();

		if (ambientTickCountdown > 0) {
			--ambientTickCountdown;
		}

		theProfiler.startSection("playerCheckLight");

		if (!playerEntities.isEmpty()) {
			var1 = rand.nextInt(playerEntities.size());
			var2 = (EntityPlayer) playerEntities.get(var1);
			var3 = MathHelper.floor_double(var2.posX) + rand.nextInt(11) - 5;
			var4 = MathHelper.floor_double(var2.posY) + rand.nextInt(11) - 5;
			final int var8 = MathHelper.floor_double(var2.posZ)
					+ rand.nextInt(11) - 5;
			updateAllLightTypes(var3, var4, var8);
		}

		theProfiler.endSection();
	}

	protected void moodSoundAndLightCheck(final int par1, final int par2,
			final Chunk par3Chunk) {
		theProfiler.endStartSection("moodSound");

		if (ambientTickCountdown == 0 && !isRemote) {
			updateLCG = updateLCG * 3 + 1013904223;
			final int var4 = updateLCG >> 2;
			int var5 = var4 & 15;
			int var6 = var4 >> 8 & 15;
			final int var7 = var4 >> 16 & 127;
			final int var8 = par3Chunk.getBlockID(var5, var7, var6);
			var5 += par1;
			var6 += par2;

			if (var8 == 0
					&& getFullBlockLightValue(var5, var7, var6) <= rand
							.nextInt(8)
					&& getSavedLightValue(EnumSkyBlock.Sky, var5, var7, var6) <= 0) {
				final EntityPlayer var9 = getClosestPlayer(var5 + 0.5D,
						var7 + 0.5D, var6 + 0.5D, 8.0D);

				if (var9 != null
						&& var9.getDistanceSq(var5 + 0.5D, var7 + 0.5D,
								var6 + 0.5D) > 4.0D) {
					playSoundEffect(var5 + 0.5D, var7 + 0.5D, var6 + 0.5D,
							"ambient.cave.cave", 0.7F,
							0.8F + rand.nextFloat() * 0.2F);
					ambientTickCountdown = rand.nextInt(12000) + 6000;
				}
			}
		}

		theProfiler.endStartSection("checkLight");
		par3Chunk.enqueueRelightChecks();
	}

	/**
	 * plays random cave ambient sounds and runs updateTick on random blocks
	 * within each chunk in the vacinity of a player
	 */
	protected void tickBlocksAndAmbiance() {
		setActivePlayerChunksAndCheckLight();
	}

	/**
	 * checks to see if a given block is both water and is cold enough to freeze
	 */
	public boolean isBlockFreezable(final int par1, final int par2,
			final int par3) {
		return canBlockFreeze(par1, par2, par3, false);
	}

	/**
	 * checks to see if a given block is both water and has at least one
	 * immediately adjacent non-water block
	 */
	public boolean isBlockFreezableNaturally(final int par1, final int par2,
			final int par3) {
		return canBlockFreeze(par1, par2, par3, true);
	}

	/**
	 * checks to see if a given block is both water, and cold enough to freeze -
	 * if the par4 boolean is set, this will only return true if there is a
	 * non-water block immediately adjacent to the specified block
	 */
	public boolean canBlockFreeze(final int par1, final int par2,
			final int par3, final boolean par4) {
		final BiomeGenBase var5 = getBiomeGenForCoords(par1, par3);
		final float var6 = var5.getFloatTemperature();

		if (var6 > 0.15F) {
			return false;
		} else {
			if (par2 >= 0
					&& par2 < 256
					&& getSavedLightValue(EnumSkyBlock.Block, par1, par2, par3) < 10) {
				final int var7 = getBlockId(par1, par2, par3);

				if ((var7 == Block.waterStill.blockID || var7 == Block.waterMoving.blockID)
						&& getBlockMetadata(par1, par2, par3) == 0) {
					if (!par4) {
						return true;
					}

					boolean var8 = true;

					if (var8
							&& getBlockMaterial(par1 - 1, par2, par3) != Material.water) {
						var8 = false;
					}

					if (var8
							&& getBlockMaterial(par1 + 1, par2, par3) != Material.water) {
						var8 = false;
					}

					if (var8
							&& getBlockMaterial(par1, par2, par3 - 1) != Material.water) {
						var8 = false;
					}

					if (var8
							&& getBlockMaterial(par1, par2, par3 + 1) != Material.water) {
						var8 = false;
					}

					if (!var8) {
						return true;
					}
				}
			}

			return false;
		}
	}

	/**
	 * Tests whether or not snow can be placed at a given location
	 */
	public boolean canSnowAt(final int par1, final int par2, final int par3) {
		final BiomeGenBase var4 = getBiomeGenForCoords(par1, par3);
		final float var5 = var4.getFloatTemperature();

		if (var5 > 0.15F) {
			return false;
		} else {
			if (par2 >= 0
					&& par2 < 256
					&& getSavedLightValue(EnumSkyBlock.Block, par1, par2, par3) < 10) {
				final int var6 = getBlockId(par1, par2 - 1, par3);
				final int var7 = getBlockId(par1, par2, par3);

				if (var7 == 0
						&& Block.snow.canPlaceBlockAt(this, par1, par2, par3)
						&& var6 != 0
						&& var6 != Block.ice.blockID
						&& Block.blocksList[var6].blockMaterial
								.blocksMovement()) {
					return true;
				}
			}

			return false;
		}
	}

	public void updateAllLightTypes(final int par1, final int par2,
			final int par3) {
		if (!provider.hasNoSky) {
			updateLightByType(EnumSkyBlock.Sky, par1, par2, par3);
		}

		updateLightByType(EnumSkyBlock.Block, par1, par2, par3);
	}

	private int computeLightValue(final int par1, final int par2,
			final int par3, final EnumSkyBlock par4EnumSkyBlock) {
		if (par4EnumSkyBlock == EnumSkyBlock.Sky
				&& canBlockSeeTheSky(par1, par2, par3)) {
			return 15;
		} else {
			final int var5 = getBlockId(par1, par2, par3);
			int var6 = par4EnumSkyBlock == EnumSkyBlock.Sky ? 0
					: Block.lightValue[var5];
			int var7 = Block.lightOpacity[var5];

			if (var7 >= 15 && Block.lightValue[var5] > 0) {
				var7 = 1;
			}

			if (var7 < 1) {
				var7 = 1;
			}

			if (var7 >= 15) {
				return 0;
			} else if (var6 >= 14) {
				return var6;
			} else {
				for (int var8 = 0; var8 < 6; ++var8) {
					final int var9 = par1 + Facing.offsetsXForSide[var8];
					final int var10 = par2 + Facing.offsetsYForSide[var8];
					final int var11 = par3 + Facing.offsetsZForSide[var8];
					final int var12 = getSavedLightValue(par4EnumSkyBlock,
							var9, var10, var11) - var7;

					if (var12 > var6) {
						var6 = var12;
					}

					if (var6 >= 14) {
						return var6;
					}
				}

				return var6;
			}
		}
	}

	public void updateLightByType(final EnumSkyBlock par1EnumSkyBlock,
			final int par2, final int par3, final int par4) {
		if (doChunksNearChunkExist(par2, par3, par4, 17)) {
			int var5 = 0;
			int var6 = 0;
			theProfiler.startSection("getBrightness");
			final int var7 = getSavedLightValue(par1EnumSkyBlock, par2, par3,
					par4);
			final int var8 = computeLightValue(par2, par3, par4,
					par1EnumSkyBlock);
			int var9;
			int var10;
			int var11;
			int var12;
			int var13;
			int var14;
			int var15;
			int var17;
			int var16;

			if (var8 > var7) {
				lightUpdateBlockList[var6++] = 133152;
			} else if (var8 < var7) {
				lightUpdateBlockList[var6++] = 133152 | var7 << 18;

				while (var5 < var6) {
					var9 = lightUpdateBlockList[var5++];
					var10 = (var9 & 63) - 32 + par2;
					var11 = (var9 >> 6 & 63) - 32 + par3;
					var12 = (var9 >> 12 & 63) - 32 + par4;
					var13 = var9 >> 18 & 15;
					var14 = getSavedLightValue(par1EnumSkyBlock, var10, var11,
							var12);

					if (var14 == var13) {
						setLightValue(par1EnumSkyBlock, var10, var11, var12, 0);

						if (var13 > 0) {
							var15 = MathHelper.abs_int(var10 - par2);
							var16 = MathHelper.abs_int(var11 - par3);
							var17 = MathHelper.abs_int(var12 - par4);

							if (var15 + var16 + var17 < 17) {
								for (int var18 = 0; var18 < 6; ++var18) {
									final int var19 = var10
											+ Facing.offsetsXForSide[var18];
									final int var20 = var11
											+ Facing.offsetsYForSide[var18];
									final int var21 = var12
											+ Facing.offsetsZForSide[var18];
									final int var22 = Math.max(
											1,
											Block.lightOpacity[getBlockId(
													var19, var20, var21)]);
									var14 = getSavedLightValue(
											par1EnumSkyBlock, var19, var20,
											var21);

									if (var14 == var13 - var22
											&& var6 < lightUpdateBlockList.length) {
										lightUpdateBlockList[var6++] = var19
												- par2 + 32
												| var20 - par3 + 32 << 6
												| var21 - par4 + 32 << 12
												| var13 - var22 << 18;
									}
								}
							}
						}
					}
				}

				var5 = 0;
			}

			theProfiler.endSection();
			theProfiler.startSection("checkedPosition < toCheckCount");

			while (var5 < var6) {
				var9 = lightUpdateBlockList[var5++];
				var10 = (var9 & 63) - 32 + par2;
				var11 = (var9 >> 6 & 63) - 32 + par3;
				var12 = (var9 >> 12 & 63) - 32 + par4;
				var13 = getSavedLightValue(par1EnumSkyBlock, var10, var11,
						var12);
				var14 = computeLightValue(var10, var11, var12, par1EnumSkyBlock);

				if (var14 != var13) {
					setLightValue(par1EnumSkyBlock, var10, var11, var12, var14);

					if (var14 > var13) {
						var15 = Math.abs(var10 - par2);
						var16 = Math.abs(var11 - par3);
						var17 = Math.abs(var12 - par4);
						final boolean var23 = var6 < lightUpdateBlockList.length - 6;

						if (var15 + var16 + var17 < 17 && var23) {
							if (getSavedLightValue(par1EnumSkyBlock, var10 - 1,
									var11, var12) < var14) {
								lightUpdateBlockList[var6++] = var10 - 1 - par2
										+ 32 + (var11 - par3 + 32 << 6)
										+ (var12 - par4 + 32 << 12);
							}

							if (getSavedLightValue(par1EnumSkyBlock, var10 + 1,
									var11, var12) < var14) {
								lightUpdateBlockList[var6++] = var10 + 1 - par2
										+ 32 + (var11 - par3 + 32 << 6)
										+ (var12 - par4 + 32 << 12);
							}

							if (getSavedLightValue(par1EnumSkyBlock, var10,
									var11 - 1, var12) < var14) {
								lightUpdateBlockList[var6++] = var10 - par2
										+ 32 + (var11 - 1 - par3 + 32 << 6)
										+ (var12 - par4 + 32 << 12);
							}

							if (getSavedLightValue(par1EnumSkyBlock, var10,
									var11 + 1, var12) < var14) {
								lightUpdateBlockList[var6++] = var10 - par2
										+ 32 + (var11 + 1 - par3 + 32 << 6)
										+ (var12 - par4 + 32 << 12);
							}

							if (getSavedLightValue(par1EnumSkyBlock, var10,
									var11, var12 - 1) < var14) {
								lightUpdateBlockList[var6++] = var10 - par2
										+ 32 + (var11 - par3 + 32 << 6)
										+ (var12 - 1 - par4 + 32 << 12);
							}

							if (getSavedLightValue(par1EnumSkyBlock, var10,
									var11, var12 + 1) < var14) {
								lightUpdateBlockList[var6++] = var10 - par2
										+ 32 + (var11 - par3 + 32 << 6)
										+ (var12 + 1 - par4 + 32 << 12);
							}
						}
					}
				}
			}

			theProfiler.endSection();
		}
	}

	/**
	 * Runs through the list of updates to run and ticks them
	 */
	public boolean tickUpdates(final boolean par1) {
		return false;
	}

	public List getPendingBlockUpdates(final Chunk par1Chunk, final boolean par2) {
		return null;
	}

	/**
	 * Will get all entities within the specified AABB excluding the one passed
	 * into it. Args: entityToExclude, aabb
	 */
	public List getEntitiesWithinAABBExcludingEntity(final Entity par1Entity,
			final AxisAlignedBB par2AxisAlignedBB) {
		return this.getEntitiesWithinAABBExcludingEntity(par1Entity,
				par2AxisAlignedBB, (IEntitySelector) null);
	}

	public List getEntitiesWithinAABBExcludingEntity(final Entity par1Entity,
			final AxisAlignedBB par2AxisAlignedBB,
			final IEntitySelector par3IEntitySelector) {
		final ArrayList var4 = new ArrayList();
		final int var5 = MathHelper
				.floor_double((par2AxisAlignedBB.minX - 2.0D) / 16.0D);
		final int var6 = MathHelper
				.floor_double((par2AxisAlignedBB.maxX + 2.0D) / 16.0D);
		final int var7 = MathHelper
				.floor_double((par2AxisAlignedBB.minZ - 2.0D) / 16.0D);
		final int var8 = MathHelper
				.floor_double((par2AxisAlignedBB.maxZ + 2.0D) / 16.0D);

		for (int var9 = var5; var9 <= var6; ++var9) {
			for (int var10 = var7; var10 <= var8; ++var10) {
				if (chunkExists(var9, var10)) {
					getChunkFromChunkCoords(var9, var10)
							.getEntitiesWithinAABBForEntity(par1Entity,
									par2AxisAlignedBB, var4,
									par3IEntitySelector);
				}
			}
		}

		return var4;
	}

	/**
	 * Returns all entities of the specified class type which intersect with the
	 * AABB. Args: entityClass, aabb
	 */
	public List getEntitiesWithinAABB(final Class par1Class,
			final AxisAlignedBB par2AxisAlignedBB) {
		return selectEntitiesWithinAABB(par1Class, par2AxisAlignedBB,
				(IEntitySelector) null);
	}

	public List selectEntitiesWithinAABB(final Class par1Class,
			final AxisAlignedBB par2AxisAlignedBB,
			final IEntitySelector par3IEntitySelector) {
		final int var4 = MathHelper
				.floor_double((par2AxisAlignedBB.minX - 2.0D) / 16.0D);
		final int var5 = MathHelper
				.floor_double((par2AxisAlignedBB.maxX + 2.0D) / 16.0D);
		final int var6 = MathHelper
				.floor_double((par2AxisAlignedBB.minZ - 2.0D) / 16.0D);
		final int var7 = MathHelper
				.floor_double((par2AxisAlignedBB.maxZ + 2.0D) / 16.0D);
		final ArrayList var8 = new ArrayList();

		for (int var9 = var4; var9 <= var5; ++var9) {
			for (int var10 = var6; var10 <= var7; ++var10) {
				if (chunkExists(var9, var10)) {
					getChunkFromChunkCoords(var9, var10)
							.getEntitiesOfTypeWithinAAAB(par1Class,
									par2AxisAlignedBB, var8,
									par3IEntitySelector);
				}
			}
		}

		return var8;
	}

	public Entity findNearestEntityWithinAABB(final Class par1Class,
			final AxisAlignedBB par2AxisAlignedBB, final Entity par3Entity) {
		final List var4 = getEntitiesWithinAABB(par1Class, par2AxisAlignedBB);
		Entity var5 = null;
		double var6 = Double.MAX_VALUE;

		for (int var8 = 0; var8 < var4.size(); ++var8) {
			final Entity var9 = (Entity) var4.get(var8);

			if (var9 != par3Entity) {
				final double var10 = par3Entity.getDistanceSqToEntity(var9);

				if (var10 <= var6) {
					var5 = var9;
					var6 = var10;
				}
			}
		}

		return var5;
	}

	/**
	 * Returns the Entity with the given ID, or null if it doesn't exist in this
	 * World.
	 */
	public abstract Entity getEntityByID(int var1);

	/**
	 * Accessor for world Loaded Entity List
	 */
	public List getLoadedEntityList() {
		return loadedEntityList;
	}

	/**
	 * marks the chunk that contains this tilentity as modified and then calls
	 * worldAccesses.doNothingWithTileEntity
	 */
	public void updateTileEntityChunkAndDoNothing(final int par1,
			final int par2, final int par3, final TileEntity par4TileEntity) {
		if (blockExists(par1, par2, par3)) {
			getChunkFromBlockCoords(par1, par3).setChunkModified();
		}
	}

	/**
	 * Counts how many entities of an entity class exist in the world. Args:
	 * entityClass
	 */
	public int countEntities(final Class par1Class) {
		int var2 = 0;

		for (int var3 = 0; var3 < loadedEntityList.size(); ++var3) {
			final Entity var4 = (Entity) loadedEntityList.get(var3);

			if ((!(var4 instanceof EntityLiving) || !((EntityLiving) var4)
					.func_104002_bU())
					&& par1Class.isAssignableFrom(var4.getClass())) {
				++var2;
			}
		}

		return var2;
	}

	/**
	 * adds entities to the loaded entities list, and loads thier skins.
	 */
	public void addLoadedEntities(final List par1List) {
		loadedEntityList.addAll(par1List);

		for (int var2 = 0; var2 < par1List.size(); ++var2) {
			obtainEntitySkin((Entity) par1List.get(var2));
		}
	}

	/**
	 * Adds a list of entities to be unloaded on the next pass of
	 * World.updateEntities()
	 */
	public void unloadEntities(final List par1List) {
		unloadedEntityList.addAll(par1List);
	}

	/**
	 * Returns true if the given Entity can be placed on the given side of the
	 * given block position.
	 */
	public boolean canPlaceEntityOnSide(final int par1, final int par2,
			final int par3, final int par4, final boolean par5, final int par6,
			final Entity par7Entity, final ItemStack par8ItemStack) {
		final int var9 = getBlockId(par2, par3, par4);
		Block var10 = Block.blocksList[var9];
		final Block var11 = Block.blocksList[par1];
		AxisAlignedBB var12 = var11.getCollisionBoundingBoxFromPool(this, par2,
				par3, par4);

		if (par5) {
			var12 = null;
		}

		if (var12 != null && !this.checkNoEntityCollision(var12, par7Entity)) {
			return false;
		} else {
			if (var10 != null
					&& (var10 == Block.waterMoving || var10 == Block.waterStill
							|| var10 == Block.lavaMoving
							|| var10 == Block.lavaStill || var10 == Block.fire || var10.blockMaterial
								.isReplaceable())) {
				var10 = null;
			}

			return var10 != null && var10.blockMaterial == Material.circuits
					&& var11 == Block.anvil ? true : par1 > 0
					&& var10 == null
					&& var11.canPlaceBlockOnSide(this, par2, par3, par4, par6,
							par8ItemStack);
		}
	}

	public PathEntity getPathEntityToEntity(final Entity par1Entity,
			final Entity par2Entity, final float par3, final boolean par4,
			final boolean par5, final boolean par6, final boolean par7) {
		theProfiler.startSection("pathfind");
		final int var8 = MathHelper.floor_double(par1Entity.posX);
		final int var9 = MathHelper.floor_double(par1Entity.posY + 1.0D);
		final int var10 = MathHelper.floor_double(par1Entity.posZ);
		final int var11 = (int) (par3 + 16.0F);
		final int var12 = var8 - var11;
		final int var13 = var9 - var11;
		final int var14 = var10 - var11;
		final int var15 = var8 + var11;
		final int var16 = var9 + var11;
		final int var17 = var10 + var11;
		final ChunkCache var18 = new ChunkCache(this, var12, var13, var14,
				var15, var16, var17, 0);
		final PathEntity var19 = new PathFinder(var18, par4, par5, par6, par7)
				.createEntityPathTo(par1Entity, par2Entity, par3);
		theProfiler.endSection();
		return var19;
	}

	public PathEntity getEntityPathToXYZ(final Entity par1Entity,
			final int par2, final int par3, final int par4, final float par5,
			final boolean par6, final boolean par7, final boolean par8,
			final boolean par9) {
		theProfiler.startSection("pathfind");
		final int var10 = MathHelper.floor_double(par1Entity.posX);
		final int var11 = MathHelper.floor_double(par1Entity.posY);
		final int var12 = MathHelper.floor_double(par1Entity.posZ);
		final int var13 = (int) (par5 + 8.0F);
		final int var14 = var10 - var13;
		final int var15 = var11 - var13;
		final int var16 = var12 - var13;
		final int var17 = var10 + var13;
		final int var18 = var11 + var13;
		final int var19 = var12 + var13;
		final ChunkCache var20 = new ChunkCache(this, var14, var15, var16,
				var17, var18, var19, 0);
		final PathEntity var21 = new PathFinder(var20, par6, par7, par8, par9)
				.createEntityPathTo(par1Entity, par2, par3, par4, par5);
		theProfiler.endSection();
		return var21;
	}

	/**
	 * Is this block powering in the specified direction Args: x, y, z,
	 * direction
	 */
	@Override
	public int isBlockProvidingPowerTo(final int par1, final int par2,
			final int par3, final int par4) {
		final int var5 = getBlockId(par1, par2, par3);
		return var5 == 0 ? 0 : Block.blocksList[var5].isProvidingStrongPower(
				this, par1, par2, par3, par4);
	}

	/**
	 * Returns the highest redstone signal strength powering the given block.
	 * Args: X, Y, Z.
	 */
	public int getBlockPowerInput(final int par1, final int par2, final int par3) {
		final byte var4 = 0;
		int var5 = Math.max(var4,
				isBlockProvidingPowerTo(par1, par2 - 1, par3, 0));

		if (var5 >= 15) {
			return var5;
		} else {
			var5 = Math.max(var5,
					isBlockProvidingPowerTo(par1, par2 + 1, par3, 1));

			if (var5 >= 15) {
				return var5;
			} else {
				var5 = Math.max(var5,
						isBlockProvidingPowerTo(par1, par2, par3 - 1, 2));

				if (var5 >= 15) {
					return var5;
				} else {
					var5 = Math.max(var5,
							isBlockProvidingPowerTo(par1, par2, par3 + 1, 3));

					if (var5 >= 15) {
						return var5;
					} else {
						var5 = Math
								.max(var5,
										isBlockProvidingPowerTo(par1 - 1, par2,
												par3, 4));

						if (var5 >= 15) {
							return var5;
						} else {
							var5 = Math.max(
									var5,
									isBlockProvidingPowerTo(par1 + 1, par2,
											par3, 5));
							return var5 >= 15 ? var5 : var5;
						}
					}
				}
			}
		}
	}

	/**
	 * Returns the indirect signal strength being outputted by the given block
	 * in the *opposite* of the given direction. Args: X, Y, Z, direction
	 */
	public boolean getIndirectPowerOutput(final int par1, final int par2,
			final int par3, final int par4) {
		return getIndirectPowerLevelTo(par1, par2, par3, par4) > 0;
	}

	/**
	 * Gets the power level from a certain block face. Args: x, y, z, direction
	 */
	public int getIndirectPowerLevelTo(final int par1, final int par2,
			final int par3, final int par4) {
		if (isBlockNormalCube(par1, par2, par3)) {
			return getBlockPowerInput(par1, par2, par3);
		} else {
			final int var5 = getBlockId(par1, par2, par3);
			return var5 == 0 ? 0 : Block.blocksList[var5].isProvidingWeakPower(
					this, par1, par2, par3, par4);
		}
	}

	/**
	 * Used to see if one of the blocks next to you or your block is getting
	 * power from a neighboring block. Used by items like TNT or Doors so they
	 * don't have redstone going straight into them. Args: x, y, z
	 */
	public boolean isBlockIndirectlyGettingPowered(final int par1,
			final int par2, final int par3) {
		return getIndirectPowerLevelTo(par1, par2 - 1, par3, 0) > 0 ? true
				: getIndirectPowerLevelTo(par1, par2 + 1, par3, 1) > 0 ? true
						: getIndirectPowerLevelTo(par1, par2, par3 - 1, 2) > 0 ? true
								: getIndirectPowerLevelTo(par1, par2, par3 + 1,
										3) > 0 ? true
										: getIndirectPowerLevelTo(par1 - 1,
												par2, par3, 4) > 0 ? true
												: getIndirectPowerLevelTo(
														par1 + 1, par2, par3, 5) > 0;
	}

	public int getStrongestIndirectPower(final int par1, final int par2,
			final int par3) {
		int var4 = 0;

		for (int var5 = 0; var5 < 6; ++var5) {
			final int var6 = getIndirectPowerLevelTo(par1
					+ Facing.offsetsXForSide[var5], par2
					+ Facing.offsetsYForSide[var5], par3
					+ Facing.offsetsZForSide[var5], var5);

			if (var6 >= 15) {
				return 15;
			}

			if (var6 > var4) {
				var4 = var6;
			}
		}

		return var4;
	}

	/**
	 * Gets the closest player to the entity within the specified distance (if
	 * distance is less than 0 then ignored). Args: entity, dist
	 */
	public EntityPlayer getClosestPlayerToEntity(final Entity par1Entity,
			final double par2) {
		return getClosestPlayer(par1Entity.posX, par1Entity.posY,
				par1Entity.posZ, par2);
	}

	/**
	 * Gets the closest player to the point within the specified distance
	 * (distance can be set to less than 0 to not limit the distance). Args: x,
	 * y, z, dist
	 */
	public EntityPlayer getClosestPlayer(final double par1, final double par3,
			final double par5, final double par7) {
		double var9 = -1.0D;
		EntityPlayer var11 = null;

		for (int var12 = 0; var12 < playerEntities.size(); ++var12) {
			final EntityPlayer var13 = (EntityPlayer) playerEntities.get(var12);
			final double var14 = var13.getDistanceSq(par1, par3, par5);

			if ((par7 < 0.0D || var14 < par7 * par7)
					&& (var9 == -1.0D || var14 < var9)) {
				var9 = var14;
				var11 = var13;
			}
		}

		return var11;
	}

	/**
	 * Returns the closest vulnerable player to this entity within the given
	 * radius, or null if none is found
	 */
	public EntityPlayer getClosestVulnerablePlayerToEntity(
			final Entity par1Entity, final double par2) {
		return getClosestVulnerablePlayer(par1Entity.posX, par1Entity.posY,
				par1Entity.posZ, par2);
	}

	/**
	 * Returns the closest vulnerable player within the given radius, or null if
	 * none is found.
	 */
	public EntityPlayer getClosestVulnerablePlayer(final double par1,
			final double par3, final double par5, final double par7) {
		double var9 = -1.0D;
		EntityPlayer var11 = null;

		for (int var12 = 0; var12 < playerEntities.size(); ++var12) {
			final EntityPlayer var13 = (EntityPlayer) playerEntities.get(var12);

			if (!var13.capabilities.disableDamage && var13.isEntityAlive()) {
				final double var14 = var13.getDistanceSq(par1, par3, par5);
				double var16 = par7;

				if (var13.isSneaking()) {
					var16 = par7 * 0.800000011920929D;
				}

				if (var13.isInvisible()) {
					float var18 = var13.func_82243_bO();

					if (var18 < 0.1F) {
						var18 = 0.1F;
					}

					var16 *= 0.7F * var18;
				}

				if ((par7 < 0.0D || var14 < var16 * var16)
						&& (var9 == -1.0D || var14 < var9)) {
					var9 = var14;
					var11 = var13;
				}
			}
		}

		return var11;
	}

	/**
	 * Find a player by name in this world.
	 */
	public EntityPlayer getPlayerEntityByName(final String par1Str) {
		for (int var2 = 0; var2 < playerEntities.size(); ++var2) {
			if (par1Str
					.equals(((EntityPlayer) playerEntities.get(var2)).username)) {
				return (EntityPlayer) playerEntities.get(var2);
			}
		}

		return null;
	}

	/**
	 * If on MP, sends a quitting packet.
	 */
	public void sendQuittingDisconnectingPacket() {
	}

	/**
	 * Checks whether the session lock file was modified by another process
	 */
	public void checkSessionLock() throws MinecraftException {
		saveHandler.checkSessionLock();
	}

	public void func_82738_a(final long par1) {
		worldInfo.incrementTotalWorldTime(par1);
	}

	/**
	 * Retrieve the world seed from level.dat
	 */
	public long getSeed() {
		return worldInfo.getSeed();
	}

	public long getTotalWorldTime() {
		return worldInfo.getWorldTotalTime();
	}

	public long getWorldTime() {
		return worldInfo.getWorldTime();
	}

	/**
	 * Sets the world time.
	 */
	public void setWorldTime(final long par1) {
		worldInfo.setWorldTime(par1);
	}

	/**
	 * Returns the coordinates of the spawn point
	 */
	public ChunkCoordinates getSpawnPoint() {
		return new ChunkCoordinates(worldInfo.getSpawnX(),
				worldInfo.getSpawnY(), worldInfo.getSpawnZ());
	}

	public void setSpawnLocation(final int par1, final int par2, final int par3) {
		worldInfo.setSpawnPosition(par1, par2, par3);
	}

	/**
	 * spwans an entity and loads surrounding chunks
	 */
	public void joinEntityInSurroundings(final Entity par1Entity) {
		final int var2 = MathHelper.floor_double(par1Entity.posX / 16.0D);
		final int var3 = MathHelper.floor_double(par1Entity.posZ / 16.0D);
		final byte var4 = 2;

		for (int var5 = var2 - var4; var5 <= var2 + var4; ++var5) {
			for (int var6 = var3 - var4; var6 <= var3 + var4; ++var6) {
				getChunkFromChunkCoords(var5, var6);
			}
		}

		if (!loadedEntityList.contains(par1Entity)) {
			loadedEntityList.add(par1Entity);
		}
	}

	/**
	 * Called when checking if a certain block can be mined or not. The 'spawn
	 * safe zone' check is located here.
	 */
	public boolean canMineBlock(final EntityPlayer par1EntityPlayer,
			final int par2, final int par3, final int par4) {
		return true;
	}

	/**
	 * sends a Packet 38 (Entity Status) to all tracked players of that entity
	 */
	public void setEntityState(final Entity par1Entity, final byte par2) {
	}

	/**
	 * gets the IChunkProvider this world uses.
	 */
	public IChunkProvider getChunkProvider() {
		return chunkProvider;
	}

	/**
	 * Adds a block event with the given Args to the blockEventCache. During the
	 * next tick(), the block specified will have its onBlockEvent handler
	 * called with the given parameters. Args: X,Y,Z, BlockID, EventID,
	 * EventParameter
	 */
	public void addBlockEvent(final int par1, final int par2, final int par3,
			final int par4, final int par5, final int par6) {
		if (par4 > 0) {
			Block.blocksList[par4].onBlockEventReceived(this, par1, par2, par3,
					par5, par6);
		}
	}

	/**
	 * Returns this world's current save handler
	 */
	public ISaveHandler getSaveHandler() {
		return saveHandler;
	}

	/**
	 * Gets the World's WorldInfo instance
	 */
	public WorldInfo getWorldInfo() {
		return worldInfo;
	}

	/**
	 * Gets the GameRules instance.
	 */
	public GameRules getGameRules() {
		return worldInfo.getGameRulesInstance();
	}

	/**
	 * Updates the flag that indicates whether or not all players in the world
	 * are sleeping.
	 */
	public void updateAllPlayersSleepingFlag() {
	}

	public float getWeightedThunderStrength(final float par1) {
		return (prevThunderingStrength + (thunderingStrength - prevThunderingStrength)
				* par1)
				* getRainStrength(par1);
	}

	/**
	 * Not sure about this actually. Reverting this one myself.
	 */
	public float getRainStrength(final float par1) {
		return prevRainingStrength + (rainingStrength - prevRainingStrength)
				* par1;
	}

	public void setRainStrength(final float par1) {
		prevRainingStrength = par1;
		rainingStrength = par1;
	}

	/**
	 * Returns true if the current thunder strength (weighted with the rain
	 * strength) is greater than 0.9
	 */
	public boolean isThundering() {
		return getWeightedThunderStrength(1.0F) > 0.9D;
	}

	/**
	 * Returns true if the current rain strength is greater than 0.2
	 */
	public boolean isRaining() {
		return getRainStrength(1.0F) > 0.2D;
	}

	public boolean canLightningStrikeAt(final int par1, final int par2,
			final int par3) {
		if (!isRaining()) {
			return false;
		} else if (!canBlockSeeTheSky(par1, par2, par3)) {
			return false;
		} else if (getPrecipitationHeight(par1, par3) > par2) {
			return false;
		} else {
			final BiomeGenBase var4 = getBiomeGenForCoords(par1, par3);
			return var4.getEnableSnow() ? false : var4.canSpawnLightningBolt();
		}
	}

	/**
	 * Checks to see if the biome rainfall values for a given x,y,z coordinate
	 * set are extremely high
	 */
	public boolean isBlockHighHumidity(final int par1, final int par2,
			final int par3) {
		final BiomeGenBase var4 = getBiomeGenForCoords(par1, par3);
		return var4.isHighHumidity();
	}

	/**
	 * Assigns the given String id to the given MapDataBase using the
	 * MapStorage, removing any existing ones of the same id.
	 */
	public void setItemData(final String par1Str,
			final WorldSavedData par2WorldSavedData) {
		mapStorage.setData(par1Str, par2WorldSavedData);
	}

	/**
	 * Loads an existing MapDataBase corresponding to the given String id from
	 * disk using the MapStorage, instantiating the given Class, or returns null
	 * if none such file exists. args: Class to instantiate, String dataid
	 */
	public WorldSavedData loadItemData(final Class par1Class,
			final String par2Str) {
		return mapStorage.loadData(par1Class, par2Str);
	}

	/**
	 * Returns an unique new data id from the MapStorage for the given prefix
	 * and saves the idCounts map to the 'idcounts' file.
	 */
	public int getUniqueDataId(final String par1Str) {
		return mapStorage.getUniqueDataId(par1Str);
	}

	public void func_82739_e(final int par1, final int par2, final int par3,
			final int par4, final int par5) {
		for (int var6 = 0; var6 < worldAccesses.size(); ++var6) {
			((IWorldAccess) worldAccesses.get(var6)).broadcastSound(par1, par2,
					par3, par4, par5);
		}
	}

	/**
	 * See description for playAuxSFX.
	 */
	public void playAuxSFX(final int par1, final int par2, final int par3,
			final int par4, final int par5) {
		playAuxSFXAtEntity((EntityPlayer) null, par1, par2, par3, par4, par5);
	}

	/**
	 * See description for playAuxSFX.
	 */
	public void playAuxSFXAtEntity(final EntityPlayer par1EntityPlayer,
			final int par2, final int par3, final int par4, final int par5,
			final int par6) {
		try {
			for (int var7 = 0; var7 < worldAccesses.size(); ++var7) {
				((IWorldAccess) worldAccesses.get(var7)).playAuxSFX(
						par1EntityPlayer, par2, par3, par4, par5, par6);
			}
		} catch (final Throwable var10) {
			final CrashReport var8 = CrashReport.makeCrashReport(var10,
					"Playing level event");
			final CrashReportCategory var9 = var8
					.makeCategory("Level event being played");
			var9.addCrashSection("Block coordinates",
					CrashReportCategory.getLocationInfo(par3, par4, par5));
			var9.addCrashSection("Event source", par1EntityPlayer);
			var9.addCrashSection("Event type", Integer.valueOf(par2));
			var9.addCrashSection("Event data", Integer.valueOf(par6));
			throw new ReportedException(var8);
		}
	}

	/**
	 * Returns current world height.
	 */
	@Override
	public int getHeight() {
		return 256;
	}

	/**
	 * Returns current world height.
	 */
	public int getActualHeight() {
		return provider.hasNoSky ? 128 : 256;
	}

	public IUpdatePlayerListBox func_82735_a(
			final EntityMinecart par1EntityMinecart) {
		return null;
	}

	/**
	 * puts the World Random seed to a specific state dependant on the inputs
	 */
	public Random setRandomSeed(final int par1, final int par2, final int par3) {
		final long var4 = par1 * 341873128712L + par2 * 132897987541L
				+ getWorldInfo().getSeed() + par3;
		rand.setSeed(var4);
		return rand;
	}

	/**
	 * Returns the location of the closest structure of the specified type. If
	 * not found returns null.
	 */
	public ChunkPosition findClosestStructure(final String par1Str,
			final int par2, final int par3, final int par4) {
		return getChunkProvider().findClosestStructure(this, par1Str, par2,
				par3, par4);
	}

	/**
	 * set by !chunk.getAreLevelsEmpty
	 */
	@Override
	public boolean extendedLevelsInChunkCache() {
		return false;
	}

	/**
	 * Returns horizon height for use in rendering the sky.
	 */
	public double getHorizon() {
		return worldInfo.getTerrainType() == WorldType.FLAT ? 0.0D : 63.0D;
	}

	/**
	 * Adds some basic stats of the world to the given crash report.
	 */
	public CrashReportCategory addWorldInfoToCrashReport(
			final CrashReport par1CrashReport) {
		final CrashReportCategory var2 = par1CrashReport.makeCategoryDepth(
				"Affected level", 1);
		var2.addCrashSection("Level name", worldInfo == null ? "????"
				: worldInfo.getWorldName());
		var2.addCrashSectionCallable("All players", new CallableLvl2(this));
		var2.addCrashSectionCallable("Chunk stats", new CallableLvl3(this));

		try {
			worldInfo.addToCrashReport(var2);
		} catch (final Throwable var4) {
			var2.addCrashSectionThrowable("Level Data Unobtainable", var4);
		}

		return var2;
	}

	/**
	 * Starts (or continues) destroying a block with given ID at the given
	 * coordinates for the given partially destroyed value
	 */
	public void destroyBlockInWorldPartially(final int par1, final int par2,
			final int par3, final int par4, final int par5) {
		for (int var6 = 0; var6 < worldAccesses.size(); ++var6) {
			final IWorldAccess var7 = (IWorldAccess) worldAccesses.get(var6);
			var7.destroyBlockPartially(par1, par2, par3, par4, par5);
		}
	}

	/**
	 * Return the Vec3Pool object for this world.
	 */
	@Override
	public Vec3Pool getWorldVec3Pool() {
		return vecPool;
	}

	/**
	 * returns a calendar object containing the current date
	 */
	public Calendar getCurrentDate() {
		if (getTotalWorldTime() % 600L == 0L) {
			theCalendar.setTimeInMillis(System.currentTimeMillis());
		}

		return theCalendar;
	}

	public void func_92088_a(final double par1, final double par3,
			final double par5, final double par7, final double par9,
			final double par11, final NBTTagCompound par13NBTTagCompound) {
	}

	public Scoreboard getScoreboard() {
		return worldScoreboard;
	}

	public void func_96440_m(final int par1, final int par2, final int par3,
			final int par4) {
		for (int var5 = 0; var5 < 4; ++var5) {
			int var6 = par1 + Direction.offsetX[var5];
			int var7 = par3 + Direction.offsetZ[var5];
			int var8 = getBlockId(var6, par2, var7);

			if (var8 != 0) {
				Block var9 = Block.blocksList[var8];

				if (Block.redstoneComparatorIdle.func_94487_f(var8)) {
					var9.onNeighborBlockChange(this, var6, par2, var7, par4);
				} else if (Block.isNormalCube(var8)) {
					var6 += Direction.offsetX[var5];
					var7 += Direction.offsetZ[var5];
					var8 = getBlockId(var6, par2, var7);
					var9 = Block.blocksList[var8];

					if (Block.redstoneComparatorIdle.func_94487_f(var8)) {
						var9.onNeighborBlockChange(this, var6, par2, var7, par4);
					}
				}
			}
		}
	}

	public ILogAgent getWorldLogAgent() {
		return worldLogAgent;
	}
}
