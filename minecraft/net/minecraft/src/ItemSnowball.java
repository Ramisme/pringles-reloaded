package net.minecraft.src;

public class ItemSnowball extends Item {
	public ItemSnowball(final int par1) {
		super(par1);
		maxStackSize = 16;
		setCreativeTab(CreativeTabs.tabMisc);
	}

	/**
	 * Called whenever this item is equipped and the right mouse button is
	 * pressed. Args: itemStack, world, entityPlayer
	 */
	@Override
	public ItemStack onItemRightClick(final ItemStack par1ItemStack,
			final World par2World, final EntityPlayer par3EntityPlayer) {
		if (!par3EntityPlayer.capabilities.isCreativeMode) {
			--par1ItemStack.stackSize;
		}

		par2World.playSoundAtEntity(par3EntityPlayer, "random.bow", 0.5F,
				0.4F / (Item.itemRand.nextFloat() * 0.4F + 0.8F));

		if (!par2World.isRemote) {
			par2World.spawnEntityInWorld(new EntitySnowball(par2World,
					par3EntityPlayer));
		}

		return par1ItemStack;
	}
}
