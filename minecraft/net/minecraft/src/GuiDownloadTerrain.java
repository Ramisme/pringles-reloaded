package net.minecraft.src;

public class GuiDownloadTerrain extends GuiScreen {
	/** Network object that downloads the terrain data. */
	private final NetClientHandler netHandler;

	/** Counts the number of screen updates. */
	private int updateCounter = 0;

	public GuiDownloadTerrain(final NetClientHandler par1NetClientHandler) {
		netHandler = par1NetClientHandler;
	}

	/**
	 * Fired when a key is typed. This is the equivalent of
	 * KeyListener.keyTyped(KeyEvent e).
	 */
	@Override
	protected void keyTyped(final char par1, final int par2) {
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui() {
		buttonList.clear();
	}

	/**
	 * Called from the main game loop to update the screen.
	 */
	@Override
	public void updateScreen() {
		++updateCounter;

		if (updateCounter % 20 == 0) {
			netHandler.addToSendQueue(new Packet0KeepAlive());
		}

		if (netHandler != null) {
			netHandler.processReadPackets();
		}
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(final int par1, final int par2, final float par3) {
		drawBackground(0);
		final StringTranslate var4 = StringTranslate.getInstance();
		drawCenteredString(fontRenderer,
				var4.translateKey("multiplayer.downloadingTerrain"), width / 2,
				height / 2 - 50, 16777215);
		super.drawScreen(par1, par2, par3);
	}
}
