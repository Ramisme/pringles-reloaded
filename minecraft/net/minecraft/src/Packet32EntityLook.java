package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet32EntityLook extends Packet30Entity {
	public Packet32EntityLook() {
		rotating = true;
	}

	public Packet32EntityLook(final int par1, final byte par2, final byte par3) {
		super(par1);
		yaw = par2;
		pitch = par3;
		rotating = true;
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		super.readPacketData(par1DataInputStream);
		yaw = par1DataInputStream.readByte();
		pitch = par1DataInputStream.readByte();
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		super.writePacketData(par1DataOutputStream);
		par1DataOutputStream.writeByte(yaw);
		par1DataOutputStream.writeByte(pitch);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 6;
	}
}
