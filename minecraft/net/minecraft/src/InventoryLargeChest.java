package net.minecraft.src;

public class InventoryLargeChest implements IInventory {
	/** Name of the chest. */
	private final String name;

	/** Inventory object corresponding to double chest upper part */
	private final IInventory upperChest;

	/** Inventory object corresponding to double chest lower part */
	private final IInventory lowerChest;

	public InventoryLargeChest(final String par1Str, IInventory par2IInventory,
			IInventory par3IInventory) {
		name = par1Str;

		if (par2IInventory == null) {
			par2IInventory = par3IInventory;
		}

		if (par3IInventory == null) {
			par3IInventory = par2IInventory;
		}

		upperChest = par2IInventory;
		lowerChest = par3IInventory;
	}

	/**
	 * Returns the number of slots in the inventory.
	 */
	@Override
	public int getSizeInventory() {
		return upperChest.getSizeInventory() + lowerChest.getSizeInventory();
	}

	/**
	 * Return whether the given inventory is part of this large chest.
	 */
	public boolean isPartOfLargeChest(final IInventory par1IInventory) {
		return upperChest == par1IInventory || lowerChest == par1IInventory;
	}

	/**
	 * Returns the name of the inventory.
	 */
	@Override
	public String getInvName() {
		return upperChest.isInvNameLocalized() ? upperChest.getInvName()
				: lowerChest.isInvNameLocalized() ? lowerChest.getInvName()
						: name;
	}

	/**
	 * If this returns false, the inventory name will be used as an unlocalized
	 * name, and translated into the player's language. Otherwise it will be
	 * used directly.
	 */
	@Override
	public boolean isInvNameLocalized() {
		return upperChest.isInvNameLocalized()
				|| lowerChest.isInvNameLocalized();
	}

	/**
	 * Returns the stack in slot i
	 */
	@Override
	public ItemStack getStackInSlot(final int par1) {
		return par1 >= upperChest.getSizeInventory() ? lowerChest
				.getStackInSlot(par1 - upperChest.getSizeInventory())
				: upperChest.getStackInSlot(par1);
	}

	/**
	 * Removes from an inventory slot (first arg) up to a specified number
	 * (second arg) of items and returns them in a new stack.
	 */
	@Override
	public ItemStack decrStackSize(final int par1, final int par2) {
		return par1 >= upperChest.getSizeInventory() ? lowerChest
				.decrStackSize(par1 - upperChest.getSizeInventory(), par2)
				: upperChest.decrStackSize(par1, par2);
	}

	/**
	 * When some containers are closed they call this on each slot, then drop
	 * whatever it returns as an EntityItem - like when you close a workbench
	 * GUI.
	 */
	@Override
	public ItemStack getStackInSlotOnClosing(final int par1) {
		return par1 >= upperChest.getSizeInventory() ? lowerChest
				.getStackInSlotOnClosing(par1 - upperChest.getSizeInventory())
				: upperChest.getStackInSlotOnClosing(par1);
	}

	/**
	 * Sets the given item stack to the specified slot in the inventory (can be
	 * crafting or armor sections).
	 */
	@Override
	public void setInventorySlotContents(final int par1,
			final ItemStack par2ItemStack) {
		if (par1 >= upperChest.getSizeInventory()) {
			lowerChest.setInventorySlotContents(
					par1 - upperChest.getSizeInventory(), par2ItemStack);
		} else {
			upperChest.setInventorySlotContents(par1, par2ItemStack);
		}
	}

	/**
	 * Returns the maximum stack size for a inventory slot. Seems to always be
	 * 64, possibly will be extended. *Isn't this more of a set than a get?*
	 */
	@Override
	public int getInventoryStackLimit() {
		return upperChest.getInventoryStackLimit();
	}

	/**
	 * Called when an the contents of an Inventory change, usually
	 */
	@Override
	public void onInventoryChanged() {
		upperChest.onInventoryChanged();
		lowerChest.onInventoryChanged();
	}

	/**
	 * Do not make give this method the name canInteractWith because it clashes
	 * with Container
	 */
	@Override
	public boolean isUseableByPlayer(final EntityPlayer par1EntityPlayer) {
		return upperChest.isUseableByPlayer(par1EntityPlayer)
				&& lowerChest.isUseableByPlayer(par1EntityPlayer);
	}

	@Override
	public void openChest() {
		upperChest.openChest();
		lowerChest.openChest();
	}

	@Override
	public void closeChest() {
		upperChest.closeChest();
		lowerChest.closeChest();
	}

	/**
	 * Returns true if automation is allowed to insert the given stack (ignoring
	 * stack size) into the given slot.
	 */
	@Override
	public boolean isStackValidForSlot(final int par1,
			final ItemStack par2ItemStack) {
		return true;
	}
}
