package net.minecraft.src;

import java.util.Iterator;
import java.util.Random;

public class BlockChest extends BlockContainer {
	private final Random random = new Random();

	/** Determines whether of not the chest is trapped. */
	public final int isTrapped;

	protected BlockChest(final int par1, final int par2) {
		super(par1, Material.wood);
		isTrapped = par2;
		setCreativeTab(CreativeTabs.tabDecorations);
		setBlockBounds(0.0625F, 0.0F, 0.0625F, 0.9375F, 0.875F, 0.9375F);
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return 22;
	}

	/**
	 * Updates the blocks bounds based on its current state. Args: world, x, y,
	 * z
	 */
	@Override
	public void setBlockBoundsBasedOnState(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		if (par1IBlockAccess.getBlockId(par2, par3, par4 - 1) == blockID) {
			setBlockBounds(0.0625F, 0.0F, 0.0F, 0.9375F, 0.875F, 0.9375F);
		} else if (par1IBlockAccess.getBlockId(par2, par3, par4 + 1) == blockID) {
			setBlockBounds(0.0625F, 0.0F, 0.0625F, 0.9375F, 0.875F, 1.0F);
		} else if (par1IBlockAccess.getBlockId(par2 - 1, par3, par4) == blockID) {
			setBlockBounds(0.0F, 0.0F, 0.0625F, 0.9375F, 0.875F, 0.9375F);
		} else if (par1IBlockAccess.getBlockId(par2 + 1, par3, par4) == blockID) {
			setBlockBounds(0.0625F, 0.0F, 0.0625F, 1.0F, 0.875F, 0.9375F);
		} else {
			setBlockBounds(0.0625F, 0.0F, 0.0625F, 0.9375F, 0.875F, 0.9375F);
		}
	}

	/**
	 * Called whenever the block is added into the world. Args: world, x, y, z
	 */
	@Override
	public void onBlockAdded(final World par1World, final int par2,
			final int par3, final int par4) {
		super.onBlockAdded(par1World, par2, par3, par4);
		unifyAdjacentChests(par1World, par2, par3, par4);
		final int var5 = par1World.getBlockId(par2, par3, par4 - 1);
		final int var6 = par1World.getBlockId(par2, par3, par4 + 1);
		final int var7 = par1World.getBlockId(par2 - 1, par3, par4);
		final int var8 = par1World.getBlockId(par2 + 1, par3, par4);

		if (var5 == blockID) {
			unifyAdjacentChests(par1World, par2, par3, par4 - 1);
		}

		if (var6 == blockID) {
			unifyAdjacentChests(par1World, par2, par3, par4 + 1);
		}

		if (var7 == blockID) {
			unifyAdjacentChests(par1World, par2 - 1, par3, par4);
		}

		if (var8 == blockID) {
			unifyAdjacentChests(par1World, par2 + 1, par3, par4);
		}
	}

	/**
	 * Called when the block is placed in the world.
	 */
	@Override
	public void onBlockPlacedBy(final World par1World, final int par2,
			final int par3, final int par4,
			final EntityLiving par5EntityLiving, final ItemStack par6ItemStack) {
		final int var7 = par1World.getBlockId(par2, par3, par4 - 1);
		final int var8 = par1World.getBlockId(par2, par3, par4 + 1);
		final int var9 = par1World.getBlockId(par2 - 1, par3, par4);
		final int var10 = par1World.getBlockId(par2 + 1, par3, par4);
		byte var11 = 0;
		final int var12 = MathHelper
				.floor_double(par5EntityLiving.rotationYaw * 4.0F / 360.0F + 0.5D) & 3;

		if (var12 == 0) {
			var11 = 2;
		}

		if (var12 == 1) {
			var11 = 5;
		}

		if (var12 == 2) {
			var11 = 3;
		}

		if (var12 == 3) {
			var11 = 4;
		}

		if (var7 != blockID && var8 != blockID && var9 != blockID
				&& var10 != blockID) {
			par1World.setBlockMetadataWithNotify(par2, par3, par4, var11, 3);
		} else {
			if ((var7 == blockID || var8 == blockID)
					&& (var11 == 4 || var11 == 5)) {
				if (var7 == blockID) {
					par1World.setBlockMetadataWithNotify(par2, par3, par4 - 1,
							var11, 3);
				} else {
					par1World.setBlockMetadataWithNotify(par2, par3, par4 + 1,
							var11, 3);
				}

				par1World
						.setBlockMetadataWithNotify(par2, par3, par4, var11, 3);
			}

			if ((var9 == blockID || var10 == blockID)
					&& (var11 == 2 || var11 == 3)) {
				if (var9 == blockID) {
					par1World.setBlockMetadataWithNotify(par2 - 1, par3, par4,
							var11, 3);
				} else {
					par1World.setBlockMetadataWithNotify(par2 + 1, par3, par4,
							var11, 3);
				}

				par1World
						.setBlockMetadataWithNotify(par2, par3, par4, var11, 3);
			}
		}

		if (par6ItemStack.hasDisplayName()) {
			((TileEntityChest) par1World.getBlockTileEntity(par2, par3, par4))
					.func_94043_a(par6ItemStack.getDisplayName());
		}
	}

	/**
	 * Turns the adjacent chests to a double chest.
	 */
	public void unifyAdjacentChests(final World par1World, final int par2,
			final int par3, final int par4) {
		if (!par1World.isRemote) {
			final int var5 = par1World.getBlockId(par2, par3, par4 - 1);
			final int var6 = par1World.getBlockId(par2, par3, par4 + 1);
			final int var7 = par1World.getBlockId(par2 - 1, par3, par4);
			final int var8 = par1World.getBlockId(par2 + 1, par3, par4);
			int var10;
			int var11;
			byte var13;
			int var14;

			if (var5 != blockID && var6 != blockID) {
				if (var7 != blockID && var8 != blockID) {
					var13 = 3;

					if (Block.opaqueCubeLookup[var5]
							&& !Block.opaqueCubeLookup[var6]) {
						var13 = 3;
					}

					if (Block.opaqueCubeLookup[var6]
							&& !Block.opaqueCubeLookup[var5]) {
						var13 = 2;
					}

					if (Block.opaqueCubeLookup[var7]
							&& !Block.opaqueCubeLookup[var8]) {
						var13 = 5;
					}

					if (Block.opaqueCubeLookup[var8]
							&& !Block.opaqueCubeLookup[var7]) {
						var13 = 4;
					}
				} else {
					var10 = par1World.getBlockId(var7 == blockID ? par2 - 1
							: par2 + 1, par3, par4 - 1);
					var11 = par1World.getBlockId(var7 == blockID ? par2 - 1
							: par2 + 1, par3, par4 + 1);
					var13 = 3;
					if (var7 == blockID) {
						var14 = par1World
								.getBlockMetadata(par2 - 1, par3, par4);
					} else {
						var14 = par1World
								.getBlockMetadata(par2 + 1, par3, par4);
					}

					if (var14 == 2) {
						var13 = 2;
					}

					if ((Block.opaqueCubeLookup[var5] || Block.opaqueCubeLookup[var10])
							&& !Block.opaqueCubeLookup[var6]
							&& !Block.opaqueCubeLookup[var11]) {
						var13 = 3;
					}

					if ((Block.opaqueCubeLookup[var6] || Block.opaqueCubeLookup[var11])
							&& !Block.opaqueCubeLookup[var5]
							&& !Block.opaqueCubeLookup[var10]) {
						var13 = 2;
					}
				}
			} else {
				var10 = par1World.getBlockId(par2 - 1, par3,
						var5 == blockID ? par4 - 1 : par4 + 1);
				var11 = par1World.getBlockId(par2 + 1, par3,
						var5 == blockID ? par4 - 1 : par4 + 1);
				var13 = 5;
				if (var5 == blockID) {
					var14 = par1World.getBlockMetadata(par2, par3, par4 - 1);
				} else {
					var14 = par1World.getBlockMetadata(par2, par3, par4 + 1);
				}

				if (var14 == 4) {
					var13 = 4;
				}

				if ((Block.opaqueCubeLookup[var7] || Block.opaqueCubeLookup[var10])
						&& !Block.opaqueCubeLookup[var8]
						&& !Block.opaqueCubeLookup[var11]) {
					var13 = 5;
				}

				if ((Block.opaqueCubeLookup[var8] || Block.opaqueCubeLookup[var11])
						&& !Block.opaqueCubeLookup[var7]
						&& !Block.opaqueCubeLookup[var10]) {
					var13 = 4;
				}
			}

			par1World.setBlockMetadataWithNotify(par2, par3, par4, var13, 3);
		}
	}

	/**
	 * Checks to see if its valid to put this block at the specified
	 * coordinates. Args: world, x, y, z
	 */
	@Override
	public boolean canPlaceBlockAt(final World par1World, final int par2,
			final int par3, final int par4) {
		int var5 = 0;

		if (par1World.getBlockId(par2 - 1, par3, par4) == blockID) {
			++var5;
		}

		if (par1World.getBlockId(par2 + 1, par3, par4) == blockID) {
			++var5;
		}

		if (par1World.getBlockId(par2, par3, par4 - 1) == blockID) {
			++var5;
		}

		if (par1World.getBlockId(par2, par3, par4 + 1) == blockID) {
			++var5;
		}

		return var5 > 1 ? false : isThereANeighborChest(par1World, par2 - 1,
				par3, par4) ? false : isThereANeighborChest(par1World,
				par2 + 1, par3, par4) ? false : isThereANeighborChest(
				par1World, par2, par3, par4 - 1) ? false
				: !isThereANeighborChest(par1World, par2, par3, par4 + 1);
	}

	/**
	 * Checks the neighbor blocks to see if there is a chest there. Args: world,
	 * x, y, z
	 */
	private boolean isThereANeighborChest(final World par1World,
			final int par2, final int par3, final int par4) {
		return par1World.getBlockId(par2, par3, par4) != blockID ? false
				: par1World.getBlockId(par2 - 1, par3, par4) == blockID ? true
						: par1World.getBlockId(par2 + 1, par3, par4) == blockID ? true
								: par1World.getBlockId(par2, par3, par4 - 1) == blockID ? true
										: par1World.getBlockId(par2, par3,
												par4 + 1) == blockID;
	}

	/**
	 * Lets the block know when one of its neighbor changes. Doesn't know which
	 * neighbor changed (coordinates passed are their own) Args: x, y, z,
	 * neighbor blockID
	 */
	@Override
	public void onNeighborBlockChange(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		super.onNeighborBlockChange(par1World, par2, par3, par4, par5);
		final TileEntityChest var6 = (TileEntityChest) par1World
				.getBlockTileEntity(par2, par3, par4);

		if (var6 != null) {
			var6.updateContainingBlockInfo();
		}
	}

	/**
	 * ejects contained items into the world, and notifies neighbours of an
	 * update, as appropriate
	 */
	@Override
	public void breakBlock(final World par1World, final int par2,
			final int par3, final int par4, final int par5, final int par6) {
		final TileEntityChest var7 = (TileEntityChest) par1World
				.getBlockTileEntity(par2, par3, par4);

		if (var7 != null) {
			for (int var8 = 0; var8 < var7.getSizeInventory(); ++var8) {
				final ItemStack var9 = var7.getStackInSlot(var8);

				if (var9 != null) {
					final float var10 = random.nextFloat() * 0.8F + 0.1F;
					final float var11 = random.nextFloat() * 0.8F + 0.1F;
					EntityItem var14;

					for (final float var12 = random.nextFloat() * 0.8F + 0.1F; var9.stackSize > 0; par1World
							.spawnEntityInWorld(var14)) {
						int var13 = random.nextInt(21) + 10;

						if (var13 > var9.stackSize) {
							var13 = var9.stackSize;
						}

						var9.stackSize -= var13;
						var14 = new EntityItem(par1World, par2 + var10, par3
								+ var11, par4 + var12, new ItemStack(
								var9.itemID, var13, var9.getItemDamage()));
						final float var15 = 0.05F;
						var14.motionX = (float) random.nextGaussian() * var15;
						var14.motionY = (float) random.nextGaussian() * var15
								+ 0.2F;
						var14.motionZ = (float) random.nextGaussian() * var15;

						if (var9.hasTagCompound()) {
							var14.getEntityItem().setTagCompound(
									(NBTTagCompound) var9.getTagCompound()
											.copy());
						}
					}
				}
			}

			par1World.func_96440_m(par2, par3, par4, par5);
		}

		super.breakBlock(par1World, par2, par3, par4, par5, par6);
	}

	/**
	 * Called upon block activation (right click on the block.)
	 */
	@Override
	public boolean onBlockActivated(final World par1World, final int par2,
			final int par3, final int par4,
			final EntityPlayer par5EntityPlayer, final int par6,
			final float par7, final float par8, final float par9) {
		if (par1World.isRemote) {
			return true;
		} else {
			final IInventory var10 = getInventory(par1World, par2, par3, par4);

			if (var10 != null) {
				par5EntityPlayer.displayGUIChest(var10);
			}

			return true;
		}
	}

	/**
	 * Gets the inventory of the chest at the specified coords, accounting for
	 * blocks or ocelots on top of the chest, and double chests.
	 */
	public IInventory getInventory(final World par1World, final int par2,
			final int par3, final int par4) {
		Object var5 = par1World.getBlockTileEntity(par2, par3, par4);

		if (var5 == null) {
			return null;
		} else if (par1World.isBlockNormalCube(par2, par3 + 1, par4)) {
			return null;
		} else if (BlockChest
				.isOcelotBlockingChest(par1World, par2, par3, par4)) {
			return null;
		} else if (par1World.getBlockId(par2 - 1, par3, par4) == blockID
				&& (par1World.isBlockNormalCube(par2 - 1, par3 + 1, par4) || BlockChest
						.isOcelotBlockingChest(par1World, par2 - 1, par3, par4))) {
			return null;
		} else if (par1World.getBlockId(par2 + 1, par3, par4) == blockID
				&& (par1World.isBlockNormalCube(par2 + 1, par3 + 1, par4) || BlockChest
						.isOcelotBlockingChest(par1World, par2 + 1, par3, par4))) {
			return null;
		} else if (par1World.getBlockId(par2, par3, par4 - 1) == blockID
				&& (par1World.isBlockNormalCube(par2, par3 + 1, par4 - 1) || BlockChest
						.isOcelotBlockingChest(par1World, par2, par3, par4 - 1))) {
			return null;
		} else if (par1World.getBlockId(par2, par3, par4 + 1) == blockID
				&& (par1World.isBlockNormalCube(par2, par3 + 1, par4 + 1) || BlockChest
						.isOcelotBlockingChest(par1World, par2, par3, par4 + 1))) {
			return null;
		} else {
			if (par1World.getBlockId(par2 - 1, par3, par4) == blockID) {
				var5 = new InventoryLargeChest("container.chestDouble",
						(TileEntityChest) par1World.getBlockTileEntity(
								par2 - 1, par3, par4), (IInventory) var5);
			}

			if (par1World.getBlockId(par2 + 1, par3, par4) == blockID) {
				var5 = new InventoryLargeChest("container.chestDouble",
						(IInventory) var5,
						(TileEntityChest) par1World.getBlockTileEntity(
								par2 + 1, par3, par4));
			}

			if (par1World.getBlockId(par2, par3, par4 - 1) == blockID) {
				var5 = new InventoryLargeChest("container.chestDouble",
						(TileEntityChest) par1World.getBlockTileEntity(par2,
								par3, par4 - 1), (IInventory) var5);
			}

			if (par1World.getBlockId(par2, par3, par4 + 1) == blockID) {
				var5 = new InventoryLargeChest("container.chestDouble",
						(IInventory) var5,
						(TileEntityChest) par1World.getBlockTileEntity(par2,
								par3, par4 + 1));
			}

			return (IInventory) var5;
		}
	}

	/**
	 * Returns a new instance of a block's tile entity class. Called on placing
	 * the block.
	 */
	@Override
	public TileEntity createNewTileEntity(final World par1World) {
		final TileEntityChest var2 = new TileEntityChest();
		return var2;
	}

	/**
	 * Can this block provide power. Only wire currently seems to have this
	 * change based on its state.
	 */
	@Override
	public boolean canProvidePower() {
		return isTrapped == 1;
	}

	/**
	 * Returns true if the block is emitting indirect/weak redstone power on the
	 * specified side. If isBlockNormalCube returns true, standard redstone
	 * propagation rules will apply instead and this will not be called. Args:
	 * World, X, Y, Z, side. Note that the side is reversed - eg it is 1 (up)
	 * when checking the bottom of the block.
	 */
	@Override
	public int isProvidingWeakPower(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		if (!canProvidePower()) {
			return 0;
		} else {
			final int var6 = ((TileEntityChest) par1IBlockAccess
					.getBlockTileEntity(par2, par3, par4)).numUsingPlayers;
			return MathHelper.clamp_int(var6, 0, 15);
		}
	}

	/**
	 * Returns true if the block is emitting direct/strong redstone power on the
	 * specified side. Args: World, X, Y, Z, side. Note that the side is
	 * reversed - eg it is 1 (up) when checking the bottom of the block.
	 */
	@Override
	public int isProvidingStrongPower(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		return par5 == 1 ? isProvidingWeakPower(par1IBlockAccess, par2, par3,
				par4, par5) : 0;
	}

	/**
	 * Looks for a sitting ocelot within certain bounds. Such an ocelot is
	 * considered to be blocking access to the chest.
	 */
	private static boolean isOcelotBlockingChest(final World par0World,
			final int par1, final int par2, final int par3) {
		final Iterator var4 = par0World.getEntitiesWithinAABB(
				EntityOcelot.class,
				AxisAlignedBB.getAABBPool().getAABB(par1, par2 + 1, par3,
						par1 + 1, par2 + 2, par3 + 1)).iterator();
		EntityOcelot var6;

		do {
			if (!var4.hasNext()) {
				return false;
			}

			final EntityOcelot var5 = (EntityOcelot) var4.next();
			var6 = var5;
		} while (!var6.isSitting());

		return true;
	}

	/**
	 * If this returns true, then comparators facing away from this block will
	 * use the value from getComparatorInputOverride instead of the actual
	 * redstone signal strength.
	 */
	@Override
	public boolean hasComparatorInputOverride() {
		return true;
	}

	/**
	 * If hasComparatorInputOverride returns true, the return value from this is
	 * used instead of the redstone signal strength when this block inputs to a
	 * comparator.
	 */
	@Override
	public int getComparatorInputOverride(final World par1World,
			final int par2, final int par3, final int par4, final int par5) {
		return Container.calcRedstoneFromInventory(getInventory(par1World,
				par2, par3, par4));
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		blockIcon = par1IconRegister.registerIcon("wood");
	}
}
