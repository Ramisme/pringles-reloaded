package net.minecraft.src;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Reflector {
	public static ReflectorClass ModLoader = new ReflectorClass("ModLoader");
	public static ReflectorMethod ModLoader_renderWorldBlock = new ReflectorMethod(
			Reflector.ModLoader, "renderWorldBlock");
	public static ReflectorMethod ModLoader_renderInvBlock = new ReflectorMethod(
			Reflector.ModLoader, "renderInvBlock");
	public static ReflectorMethod ModLoader_renderBlockIsItemFull3D = new ReflectorMethod(
			Reflector.ModLoader, "renderBlockIsItemFull3D");
	public static ReflectorClass FMLRenderAccessLibrary = new ReflectorClass(
			"FMLRenderAccessLibrary");
	public static ReflectorMethod FMLRenderAccessLibrary_renderWorldBlock = new ReflectorMethod(
			Reflector.FMLRenderAccessLibrary, "renderWorldBlock");
	public static ReflectorMethod FMLRenderAccessLibrary_renderInventoryBlock = new ReflectorMethod(
			Reflector.FMLRenderAccessLibrary, "renderInventoryBlock");
	public static ReflectorMethod FMLRenderAccessLibrary_renderItemAsFull3DBlock = new ReflectorMethod(
			Reflector.FMLRenderAccessLibrary, "renderItemAsFull3DBlock");
	public static ReflectorClass LightCache = new ReflectorClass("LightCache");
	public static ReflectorField LightCache_cache = new ReflectorField(
			Reflector.LightCache, "cache");
	public static ReflectorMethod LightCache_clear = new ReflectorMethod(
			Reflector.LightCache, "clear");
	public static ReflectorClass BlockCoord = new ReflectorClass("BlockCoord");
	public static ReflectorMethod BlockCoord_resetPool = new ReflectorMethod(
			Reflector.BlockCoord, "resetPool");
	public static ReflectorClass MinecraftForge = new ReflectorClass(
			"net.minecraftforge.common.MinecraftForge");
	public static ReflectorField MinecraftForge_EVENT_BUS = new ReflectorField(
			Reflector.MinecraftForge, "EVENT_BUS");
	public static ReflectorClass ForgeHooks = new ReflectorClass(
			"net.minecraftforge.common.ForgeHooks");
	public static ReflectorMethod ForgeHooks_onLivingSetAttackTarget = new ReflectorMethod(
			Reflector.ForgeHooks, "onLivingSetAttackTarget");
	public static ReflectorMethod ForgeHooks_onLivingUpdate = new ReflectorMethod(
			Reflector.ForgeHooks, "onLivingUpdate");
	public static ReflectorMethod ForgeHooks_onLivingAttack = new ReflectorMethod(
			Reflector.ForgeHooks, "onLivingAttack");
	public static ReflectorMethod ForgeHooks_onLivingHurt = new ReflectorMethod(
			Reflector.ForgeHooks, "onLivingHurt");
	public static ReflectorMethod ForgeHooks_onLivingDeath = new ReflectorMethod(
			Reflector.ForgeHooks, "onLivingDeath");
	public static ReflectorMethod ForgeHooks_onLivingDrops = new ReflectorMethod(
			Reflector.ForgeHooks, "onLivingDrops");
	public static ReflectorMethod ForgeHooks_onLivingFall = new ReflectorMethod(
			Reflector.ForgeHooks, "onLivingFall");
	public static ReflectorMethod ForgeHooks_onLivingJump = new ReflectorMethod(
			Reflector.ForgeHooks, "onLivingJump");
	public static ReflectorMethod ForgeHooks_isLivingOnLadder = new ReflectorMethod(
			Reflector.ForgeHooks, "isLivingOnLadder");
	public static ReflectorClass ForgeHooksClient = new ReflectorClass(
			"net.minecraftforge.client.ForgeHooksClient");
	public static ReflectorMethod ForgeHooksClient_onDrawBlockHighlight = new ReflectorMethod(
			Reflector.ForgeHooksClient, "onDrawBlockHighlight");
	public static ReflectorMethod ForgeHooksClient_orientBedCamera = new ReflectorMethod(
			Reflector.ForgeHooksClient, "orientBedCamera");
	public static ReflectorMethod ForgeHooksClient_dispatchRenderLast = new ReflectorMethod(
			Reflector.ForgeHooksClient, "dispatchRenderLast");
	public static ReflectorMethod ForgeHooksClient_onTextureLoadPre = new ReflectorMethod(
			Reflector.ForgeHooksClient, "onTextureLoadPre");
	public static ReflectorMethod ForgeHooksClient_onTextureLoad = new ReflectorMethod(
			Reflector.ForgeHooksClient, "onTextureLoad");
	public static ReflectorMethod ForgeHooksClient_setRenderPass = new ReflectorMethod(
			Reflector.ForgeHooksClient, "setRenderPass");
	public static ReflectorMethod ForgeHooksClient_onTextureStitchedPre = new ReflectorMethod(
			Reflector.ForgeHooksClient, "onTextureStitchedPre");
	public static ReflectorMethod ForgeHooksClient_onTextureStitchedPost = new ReflectorMethod(
			Reflector.ForgeHooksClient, "onTextureStitchedPost");
	public static ReflectorClass FMLCommonHandler = new ReflectorClass(
			"cpw.mods.fml.common.FMLCommonHandler");
	public static ReflectorMethod FMLCommonHandler_instance = new ReflectorMethod(
			Reflector.FMLCommonHandler, "instance");
	public static ReflectorMethod FMLCommonHandler_handleServerStarting = new ReflectorMethod(
			Reflector.FMLCommonHandler, "handleServerStarting");
	public static ReflectorMethod FMLCommonHandler_handleServerAboutToStart = new ReflectorMethod(
			Reflector.FMLCommonHandler, "handleServerAboutToStart");
	public static ReflectorClass FMLClientHandler = new ReflectorClass(
			"cpw.mods.fml.client.FMLClientHandler");
	public static ReflectorMethod FMLClientHandler_instance = new ReflectorMethod(
			Reflector.FMLClientHandler, "instance");
	public static ReflectorMethod FMLClientHandler_isLoading = new ReflectorMethod(
			Reflector.FMLClientHandler, "isLoading");
	public static ReflectorClass ForgeWorldProvider = new ReflectorClass(
			WorldProvider.class);
	public static ReflectorMethod ForgeWorldProvider_getSkyRenderer = new ReflectorMethod(
			Reflector.ForgeWorldProvider, "getSkyRenderer");
	public static ReflectorMethod ForgeWorldProvider_getCloudRenderer = new ReflectorMethod(
			Reflector.ForgeWorldProvider, "getCloudRenderer");
	public static ReflectorClass IRenderHandler = new ReflectorClass(
			"net.minecraftforge.client.IRenderHandler");
	public static ReflectorMethod IRenderHandler_render = new ReflectorMethod(
			Reflector.IRenderHandler, "render");
	public static ReflectorClass DimensionManager = new ReflectorClass(
			"net.minecraftforge.common.DimensionManager");
	public static ReflectorMethod DimensionManager_getStaticDimensionIDs = new ReflectorMethod(
			Reflector.DimensionManager, "getStaticDimensionIDs");
	public static ReflectorClass WorldEvent_Load = new ReflectorClass(
			"net.minecraftforge.event.world.WorldEvent$Load");
	public static ReflectorConstructor WorldEvent_Load_Constructor = new ReflectorConstructor(
			Reflector.WorldEvent_Load, new Class[] { World.class });
	public static ReflectorClass EventBus = new ReflectorClass(
			"net.minecraftforge.event.EventBus");
	public static ReflectorMethod EventBus_post = new ReflectorMethod(
			Reflector.EventBus, "post");
	public static ReflectorClass ChunkWatchEvent_UnWatch = new ReflectorClass(
			"net.minecraftforge.event.world.ChunkWatchEvent$UnWatch");
	public static ReflectorConstructor ChunkWatchEvent_UnWatch_Constructor = new ReflectorConstructor(
			Reflector.ChunkWatchEvent_UnWatch, new Class[] {
					ChunkCoordIntPair.class, EntityPlayerMP.class });
	public static ReflectorClass ForgeBlock = new ReflectorClass(Block.class);
	public static ReflectorMethod ForgeBlock_getBedDirection = new ReflectorMethod(
			Reflector.ForgeBlock, "getBedDirection");
	public static ReflectorMethod ForgeBlock_isBedFoot = new ReflectorMethod(
			Reflector.ForgeBlock, "isBedFoot");
	public static ReflectorMethod ForgeBlock_canRenderInPass = new ReflectorMethod(
			Reflector.ForgeBlock, "canRenderInPass");
	public static ReflectorClass ForgeEntity = new ReflectorClass(Entity.class);
	public static ReflectorField ForgeEntity_captureDrops = new ReflectorField(
			Reflector.ForgeEntity, "captureDrops");
	public static ReflectorField ForgeEntity_capturedDrops = new ReflectorField(
			Reflector.ForgeEntity, "capturedDrops");
	public static ReflectorClass ForgeItem = new ReflectorClass(Item.class);
	public static ReflectorMethod ForgeItem_onEntitySwing = new ReflectorMethod(
			Reflector.ForgeItem, "onEntitySwing");
	public static ReflectorClass ForgePotionEffect = new ReflectorClass(
			PotionEffect.class);
	public static ReflectorMethod ForgePotionEffect_isCurativeItem = new ReflectorMethod(
			Reflector.ForgePotionEffect, "isCurativeItem");

	public static void callVoid(final ReflectorMethod var0,
			final Object... var1) {
		try {
			final Method var2 = var0.getTargetMethod();

			if (var2 == null) {
				return;
			}

			var2.invoke((Object) null, var1);
		} catch (final Throwable var3) {
			Reflector.handleException(var3, (Object) null, var0, var1);
		}
	}

	public static boolean callBoolean(final ReflectorMethod var0,
			final Object... var1) {
		try {
			final Method var2 = var0.getTargetMethod();

			if (var2 == null) {
				return false;
			} else {
				final Boolean var3 = (Boolean) var2.invoke((Object) null, var1);
				return var3.booleanValue();
			}
		} catch (final Throwable var4) {
			Reflector.handleException(var4, (Object) null, var0, var1);
			return false;
		}
	}

	public static int callInt(final ReflectorMethod var0, final Object... var1) {
		try {
			final Method var2 = var0.getTargetMethod();

			if (var2 == null) {
				return 0;
			} else {
				final Integer var3 = (Integer) var2.invoke((Object) null, var1);
				return var3.intValue();
			}
		} catch (final Throwable var4) {
			Reflector.handleException(var4, (Object) null, var0, var1);
			return 0;
		}
	}

	public static float callFloat(final ReflectorMethod var0,
			final Object... var1) {
		try {
			final Method var2 = var0.getTargetMethod();

			if (var2 == null) {
				return 0.0F;
			} else {
				final Float var3 = (Float) var2.invoke((Object) null, var1);
				return var3.floatValue();
			}
		} catch (final Throwable var4) {
			Reflector.handleException(var4, (Object) null, var0, var1);
			return 0.0F;
		}
	}

	public static String callString(final ReflectorMethod var0,
			final Object... var1) {
		try {
			final Method var2 = var0.getTargetMethod();

			if (var2 == null) {
				return null;
			} else {
				final String var3 = (String) var2.invoke((Object) null, var1);
				return var3;
			}
		} catch (final Throwable var4) {
			Reflector.handleException(var4, (Object) null, var0, var1);
			return null;
		}
	}

	public static Object call(final ReflectorMethod var0, final Object... var1) {
		try {
			final Method var2 = var0.getTargetMethod();

			if (var2 == null) {
				return null;
			} else {
				final Object var3 = var2.invoke((Object) null, var1);
				return var3;
			}
		} catch (final Throwable var4) {
			Reflector.handleException(var4, (Object) null, var0, var1);
			return null;
		}
	}

	public static void callVoid(final Object var0, final ReflectorMethod var1,
			final Object... var2) {
		try {
			if (var0 == null) {
				return;
			}

			final Method var3 = var1.getTargetMethod();

			if (var3 == null) {
				return;
			}

			var3.invoke(var0, var2);
		} catch (final Throwable var4) {
			Reflector.handleException(var4, var0, var1, var2);
		}
	}

	public static boolean callBoolean(final Object var0,
			final ReflectorMethod var1, final Object... var2) {
		try {
			final Method var3 = var1.getTargetMethod();

			if (var3 == null) {
				return false;
			} else {
				final Boolean var4 = (Boolean) var3.invoke(var0, var2);
				return var4.booleanValue();
			}
		} catch (final Throwable var5) {
			Reflector.handleException(var5, var0, var1, var2);
			return false;
		}
	}

	public static int callInt(final Object var0, final ReflectorMethod var1,
			final Object... var2) {
		try {
			final Method var3 = var1.getTargetMethod();

			if (var3 == null) {
				return 0;
			} else {
				final Integer var4 = (Integer) var3.invoke(var0, var2);
				return var4.intValue();
			}
		} catch (final Throwable var5) {
			Reflector.handleException(var5, var0, var1, var2);
			return 0;
		}
	}

	public static float callFloat(final Object var0,
			final ReflectorMethod var1, final Object... var2) {
		try {
			final Method var3 = var1.getTargetMethod();

			if (var3 == null) {
				return 0.0F;
			} else {
				final Float var4 = (Float) var3.invoke(var0, var2);
				return var4.floatValue();
			}
		} catch (final Throwable var5) {
			Reflector.handleException(var5, var0, var1, var2);
			return 0.0F;
		}
	}

	public static String callString(final Object var0,
			final ReflectorMethod var1, final Object... var2) {
		try {
			final Method var3 = var1.getTargetMethod();

			if (var3 == null) {
				return null;
			} else {
				final String var4 = (String) var3.invoke(var0, var2);
				return var4;
			}
		} catch (final Throwable var5) {
			Reflector.handleException(var5, var0, var1, var2);
			return null;
		}
	}

	public static Object call(final Object var0, final ReflectorMethod var1,
			final Object... var2) {
		try {
			final Method var3 = var1.getTargetMethod();

			if (var3 == null) {
				return null;
			} else {
				final Object var4 = var3.invoke(var0, var2);
				return var4;
			}
		} catch (final Throwable var5) {
			Reflector.handleException(var5, var0, var1, var2);
			return null;
		}
	}

	public static Object getFieldValue(final ReflectorField var0) {
		return Reflector.getFieldValue((Object) null, var0);
	}

	public static Object getFieldValue(final Object var0,
			final ReflectorField var1) {
		try {
			final Field var2 = var1.getTargetField();

			if (var2 == null) {
				return null;
			} else {
				final Object var3 = var2.get(var0);
				return var3;
			}
		} catch (final Throwable var4) {
			var4.printStackTrace();
			return null;
		}
	}

	public static void setFieldValue(final ReflectorField var0,
			final Object var1) {
		Reflector.setFieldValue((Object) null, var0, var1);
	}

	public static void setFieldValue(final Object var0,
			final ReflectorField var1, final Object var2) {
		try {
			final Field var3 = var1.getTargetField();

			if (var3 == null) {
				return;
			}

			var3.set(var0, var2);
		} catch (final Throwable var4) {
			var4.printStackTrace();
		}
	}

	public static void postForgeBusEvent(final ReflectorConstructor var0,
			final Object... var1) {
		try {
			final Object var2 = Reflector
					.getFieldValue(Reflector.MinecraftForge_EVENT_BUS);

			if (var2 == null) {
				return;
			}

			final Constructor var3 = var0.getTargetConstructor();

			if (var3 == null) {
				return;
			}

			final Object var4 = var3.newInstance(var1);
			Reflector.callVoid(var2, Reflector.EventBus_post,
					new Object[] { var4 });
		} catch (final Throwable var5) {
			var5.printStackTrace();
		}
	}

	public static boolean matchesTypes(final Class[] var0, final Class[] var1) {
		if (var0.length != var1.length) {
			return false;
		} else {
			for (int var2 = 0; var2 < var1.length; ++var2) {
				final Class var3 = var0[var2];
				final Class var4 = var1[var2];

				if (var3 != var4) {
					return false;
				}
			}

			return true;
		}
	}

	private static void handleException(final Throwable var0,
			final Object var1, final ReflectorMethod var2, final Object[] var3) {
		if (var0 instanceof InvocationTargetException) {
			var0.printStackTrace();
		} else {
			if (var0 instanceof IllegalArgumentException) {
				Config.dbg("*** IllegalArgumentException ***");
				Config.dbg("Method: " + var2.getTargetMethod());
				Config.dbg("Object: " + var1);
				Config.dbg("Parameter classes: "
						+ Config.arrayToString(Reflector.getClasses(var3)));
				Config.dbg("Parameters: " + Config.arrayToString(var3));
			}

			Config.dbg("*** Exception outside of method ***");
			Config.dbg("Method deactivated: " + var2.getTargetMethod());
			var2.deactivate();
			var0.printStackTrace();
		}
	}

	private static Object[] getClasses(final Object[] var0) {
		if (var0 == null) {
			return new Class[0];
		} else {
			final Class[] var1 = new Class[var0.length];

			for (int var2 = 0; var2 < var1.length; ++var2) {
				final Object var3 = var0[var2];

				if (var3 != null) {
					var1[var2] = var3.getClass();
				}
			}

			return var1;
		}
	}
}
