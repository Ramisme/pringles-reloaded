package net.minecraft.src;

public class ScoreObjective {
	private final Scoreboard theScoreboard;
	private final String name;

	/** The ScoreObjectiveCriteria for this objetive */
	private final ScoreObjectiveCriteria objectiveCriteria;
	private String displayName;

	public ScoreObjective(final Scoreboard par1Scoreboard,
			final String par2Str,
			final ScoreObjectiveCriteria par3ScoreObjectiveCriteria) {
		theScoreboard = par1Scoreboard;
		name = par2Str;
		objectiveCriteria = par3ScoreObjectiveCriteria;
		displayName = par2Str;
	}

	public Scoreboard getScoreboard() {
		return theScoreboard;
	}

	public String getName() {
		return name;
	}

	public ScoreObjectiveCriteria getCriteria() {
		return objectiveCriteria;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(final String par1Str) {
		displayName = par1Str;
		theScoreboard.func_96532_b(this);
	}
}
