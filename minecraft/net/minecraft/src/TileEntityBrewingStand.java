package net.minecraft.src;

import java.util.List;

public class TileEntityBrewingStand extends TileEntity implements
		ISidedInventory {
	private static final int[] field_102017_a = new int[] { 3 };
	private static final int[] field_102016_b = new int[] { 0, 1, 2 };

	/** The itemstacks currently placed in the slots of the brewing stand */
	private ItemStack[] brewingItemStacks = new ItemStack[4];
	private int brewTime;

	/**
	 * an integer with each bit specifying whether that slot of the stand
	 * contains a potion
	 */
	private int filledSlots;
	private int ingredientID;
	private String field_94132_e;

	/**
	 * Returns the name of the inventory.
	 */
	@Override
	public String getInvName() {
		return isInvNameLocalized() ? field_94132_e : "container.brewing";
	}

	/**
	 * If this returns false, the inventory name will be used as an unlocalized
	 * name, and translated into the player's language. Otherwise it will be
	 * used directly.
	 */
	@Override
	public boolean isInvNameLocalized() {
		return field_94132_e != null && field_94132_e.length() > 0;
	}

	public void func_94131_a(final String par1Str) {
		field_94132_e = par1Str;
	}

	/**
	 * Returns the number of slots in the inventory.
	 */
	@Override
	public int getSizeInventory() {
		return brewingItemStacks.length;
	}

	/**
	 * Allows the entity to update its state. Overridden in most subclasses,
	 * e.g. the mob spawner uses this to count ticks and creates a new spawn
	 * inside its implementation.
	 */
	@Override
	public void updateEntity() {
		if (brewTime > 0) {
			--brewTime;

			if (brewTime == 0) {
				brewPotions();
				onInventoryChanged();
			} else if (!canBrew()) {
				brewTime = 0;
				onInventoryChanged();
			} else if (ingredientID != brewingItemStacks[3].itemID) {
				brewTime = 0;
				onInventoryChanged();
			}
		} else if (canBrew()) {
			brewTime = 400;
			ingredientID = brewingItemStacks[3].itemID;
		}

		final int var1 = getFilledSlots();

		if (var1 != filledSlots) {
			filledSlots = var1;
			worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, var1, 2);
		}

		super.updateEntity();
	}

	public int getBrewTime() {
		return brewTime;
	}

	private boolean canBrew() {
		if (brewingItemStacks[3] != null && brewingItemStacks[3].stackSize > 0) {
			final ItemStack var1 = brewingItemStacks[3];

			if (!Item.itemsList[var1.itemID].isPotionIngredient()) {
				return false;
			} else {
				boolean var2 = false;

				for (int var3 = 0; var3 < 3; ++var3) {
					if (brewingItemStacks[var3] != null
							&& brewingItemStacks[var3].itemID == Item.potion.itemID) {
						final int var4 = brewingItemStacks[var3]
								.getItemDamage();
						final int var5 = getPotionResult(var4, var1);

						if (!ItemPotion.isSplash(var4)
								&& ItemPotion.isSplash(var5)) {
							var2 = true;
							break;
						}

						final List var6 = Item.potion.getEffects(var4);
						final List var7 = Item.potion.getEffects(var5);

						if ((var4 <= 0 || var6 != var7)
								&& (var6 == null || !var6.equals(var7)
										&& var7 != null) && var4 != var5) {
							var2 = true;
							break;
						}
					}
				}

				return var2;
			}
		} else {
			return false;
		}
	}

	private void brewPotions() {
		if (canBrew()) {
			final ItemStack var1 = brewingItemStacks[3];

			for (int var2 = 0; var2 < 3; ++var2) {
				if (brewingItemStacks[var2] != null
						&& brewingItemStacks[var2].itemID == Item.potion.itemID) {
					final int var3 = brewingItemStacks[var2].getItemDamage();
					final int var4 = getPotionResult(var3, var1);
					final List var5 = Item.potion.getEffects(var3);
					final List var6 = Item.potion.getEffects(var4);

					if ((var3 <= 0 || var5 != var6)
							&& (var5 == null || !var5.equals(var6)
									&& var6 != null)) {
						if (var3 != var4) {
							brewingItemStacks[var2].setItemDamage(var4);
						}
					} else if (!ItemPotion.isSplash(var3)
							&& ItemPotion.isSplash(var4)) {
						brewingItemStacks[var2].setItemDamage(var4);
					}
				}
			}

			if (Item.itemsList[var1.itemID].hasContainerItem()) {
				brewingItemStacks[3] = new ItemStack(
						Item.itemsList[var1.itemID].getContainerItem());
			} else {
				--brewingItemStacks[3].stackSize;

				if (brewingItemStacks[3].stackSize <= 0) {
					brewingItemStacks[3] = null;
				}
			}
		}
	}

	/**
	 * The result of brewing a potion of the specified damage value with an
	 * ingredient itemstack.
	 */
	private int getPotionResult(final int par1, final ItemStack par2ItemStack) {
		return par2ItemStack == null ? par1
				: Item.itemsList[par2ItemStack.itemID].isPotionIngredient() ? PotionHelper
						.applyIngredient(par1,
								Item.itemsList[par2ItemStack.itemID]
										.getPotionEffect()) : par1;
	}

	/**
	 * Reads a tile entity from NBT.
	 */
	@Override
	public void readFromNBT(final NBTTagCompound par1NBTTagCompound) {
		super.readFromNBT(par1NBTTagCompound);
		final NBTTagList var2 = par1NBTTagCompound.getTagList("Items");
		brewingItemStacks = new ItemStack[getSizeInventory()];

		for (int var3 = 0; var3 < var2.tagCount(); ++var3) {
			final NBTTagCompound var4 = (NBTTagCompound) var2.tagAt(var3);
			final byte var5 = var4.getByte("Slot");

			if (var5 >= 0 && var5 < brewingItemStacks.length) {
				brewingItemStacks[var5] = ItemStack.loadItemStackFromNBT(var4);
			}
		}

		brewTime = par1NBTTagCompound.getShort("BrewTime");

		if (par1NBTTagCompound.hasKey("CustomName")) {
			field_94132_e = par1NBTTagCompound.getString("CustomName");
		}
	}

	/**
	 * Writes a tile entity to NBT.
	 */
	@Override
	public void writeToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeToNBT(par1NBTTagCompound);
		par1NBTTagCompound.setShort("BrewTime", (short) brewTime);
		final NBTTagList var2 = new NBTTagList();

		for (int var3 = 0; var3 < brewingItemStacks.length; ++var3) {
			if (brewingItemStacks[var3] != null) {
				final NBTTagCompound var4 = new NBTTagCompound();
				var4.setByte("Slot", (byte) var3);
				brewingItemStacks[var3].writeToNBT(var4);
				var2.appendTag(var4);
			}
		}

		par1NBTTagCompound.setTag("Items", var2);

		if (isInvNameLocalized()) {
			par1NBTTagCompound.setString("CustomName", field_94132_e);
		}
	}

	/**
	 * Returns the stack in slot i
	 */
	@Override
	public ItemStack getStackInSlot(final int par1) {
		return par1 >= 0 && par1 < brewingItemStacks.length ? brewingItemStacks[par1]
				: null;
	}

	/**
	 * Removes from an inventory slot (first arg) up to a specified number
	 * (second arg) of items and returns them in a new stack.
	 */
	@Override
	public ItemStack decrStackSize(final int par1, final int par2) {
		if (par1 >= 0 && par1 < brewingItemStacks.length) {
			final ItemStack var3 = brewingItemStacks[par1];
			brewingItemStacks[par1] = null;
			return var3;
		} else {
			return null;
		}
	}

	/**
	 * When some containers are closed they call this on each slot, then drop
	 * whatever it returns as an EntityItem - like when you close a workbench
	 * GUI.
	 */
	@Override
	public ItemStack getStackInSlotOnClosing(final int par1) {
		if (par1 >= 0 && par1 < brewingItemStacks.length) {
			final ItemStack var2 = brewingItemStacks[par1];
			brewingItemStacks[par1] = null;
			return var2;
		} else {
			return null;
		}
	}

	/**
	 * Sets the given item stack to the specified slot in the inventory (can be
	 * crafting or armor sections).
	 */
	@Override
	public void setInventorySlotContents(final int par1,
			final ItemStack par2ItemStack) {
		if (par1 >= 0 && par1 < brewingItemStacks.length) {
			brewingItemStacks[par1] = par2ItemStack;
		}
	}

	/**
	 * Returns the maximum stack size for a inventory slot. Seems to always be
	 * 64, possibly will be extended. *Isn't this more of a set than a get?*
	 */
	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	/**
	 * Do not make give this method the name canInteractWith because it clashes
	 * with Container
	 */
	@Override
	public boolean isUseableByPlayer(final EntityPlayer par1EntityPlayer) {
		return worldObj.getBlockTileEntity(xCoord, yCoord, zCoord) != this ? false
				: par1EntityPlayer.getDistanceSq(xCoord + 0.5D, yCoord + 0.5D,
						zCoord + 0.5D) <= 64.0D;
	}

	@Override
	public void openChest() {
	}

	@Override
	public void closeChest() {
	}

	/**
	 * Returns true if automation is allowed to insert the given stack (ignoring
	 * stack size) into the given slot.
	 */
	@Override
	public boolean isStackValidForSlot(final int par1,
			final ItemStack par2ItemStack) {
		return par1 == 3 ? Item.itemsList[par2ItemStack.itemID]
				.isPotionIngredient()
				: par2ItemStack.itemID == Item.potion.itemID
						|| par2ItemStack.itemID == Item.glassBottle.itemID;
	}

	public void setBrewTime(final int par1) {
		brewTime = par1;
	}

	/**
	 * returns an integer with each bit specifying wether that slot of the stand
	 * contains a potion
	 */
	public int getFilledSlots() {
		int var1 = 0;

		for (int var2 = 0; var2 < 3; ++var2) {
			if (brewingItemStacks[var2] != null) {
				var1 |= 1 << var2;
			}
		}

		return var1;
	}

	/**
	 * Returns an array containing the indices of the slots that can be accessed
	 * by automation on the given side of this block.
	 */
	@Override
	public int[] getAccessibleSlotsFromSide(final int par1) {
		return par1 == 1 ? TileEntityBrewingStand.field_102017_a
				: TileEntityBrewingStand.field_102016_b;
	}

	/**
	 * Returns true if automation can insert the given item in the given slot
	 * from the given side. Args: Slot, item, side
	 */
	@Override
	public boolean canInsertItem(final int par1, final ItemStack par2ItemStack,
			final int par3) {
		return isStackValidForSlot(par1, par2ItemStack);
	}

	/**
	 * Returns true if automation can extract the given item in the given slot
	 * from the given side. Args: Slot, item, side
	 */
	@Override
	public boolean canExtractItem(final int par1,
			final ItemStack par2ItemStack, final int par3) {
		return true;
	}
}
