package net.minecraft.src;

import net.minecraft.client.Minecraft;

public class GuiConnecting extends GuiScreen {
	/** A reference to the NetClientHandler. */
	private NetClientHandler clientHandler;

	/** True if the connection attempt has been cancelled. */
	private boolean cancelled = false;
	private final GuiScreen field_98098_c;

	public GuiConnecting(final GuiScreen par1GuiScreen,
			final Minecraft par2Minecraft, final ServerData par3ServerData) {
		mc = par2Minecraft;
		field_98098_c = par1GuiScreen;
		final ServerAddress var4 = ServerAddress
				.func_78860_a(par3ServerData.serverIP);
		par2Minecraft.loadWorld((WorldClient) null);
		par2Minecraft.setServerData(par3ServerData);
		spawnNewServerThread(var4.getIP(), var4.getPort());
	}

	public GuiConnecting(final GuiScreen par1GuiScreen,
			final Minecraft par2Minecraft, final String par3Str, final int par4) {
		mc = par2Minecraft;
		field_98098_c = par1GuiScreen;
		par2Minecraft.loadWorld((WorldClient) null);
		spawnNewServerThread(par3Str, par4);
	}

	private void spawnNewServerThread(final String par1Str, final int par2) {
		mc.getLogAgent().logInfo("Connecting to " + par1Str + ", " + par2);
		new ThreadConnectToServer(this, par1Str, par2).start();
	}

	/**
	 * Called from the main game loop to update the screen.
	 */
	@Override
	public void updateScreen() {
		if (clientHandler != null) {
			clientHandler.processReadPackets();
		}
	}

	/**
	 * Fired when a key is typed. This is the equivalent of
	 * KeyListener.keyTyped(KeyEvent e).
	 */
	@Override
	protected void keyTyped(final char par1, final int par2) {
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui() {
		final StringTranslate var1 = StringTranslate.getInstance();
		buttonList.clear();
		buttonList.add(new GuiButton(0, width / 2 - 100, height / 4 + 120 + 12,
				var1.translateKey("gui.cancel")));
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	@Override
	protected void actionPerformed(final GuiButton par1GuiButton) {
		if (par1GuiButton.id == 0) {
			cancelled = true;

			if (clientHandler != null) {
				clientHandler.disconnect();
			}

			mc.displayGuiScreen(field_98098_c);
		}
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(final int par1, final int par2, final float par3) {
		drawDefaultBackground();
		final StringTranslate var4 = StringTranslate.getInstance();

		if (clientHandler == null) {
			drawCenteredString(fontRenderer,
					var4.translateKey("connect.connecting"), width / 2,
					height / 2 - 50, 16777215);
			drawCenteredString(fontRenderer, "", width / 2, height / 2 - 10,
					16777215);
		} else {
			drawCenteredString(fontRenderer,
					var4.translateKey("connect.authorizing"), width / 2,
					height / 2 - 50, 16777215);
			drawCenteredString(fontRenderer, clientHandler.field_72560_a,
					width / 2, height / 2 - 10, 16777215);
		}

		super.drawScreen(par1, par2, par3);
	}

	/**
	 * Sets the NetClientHandler.
	 */
	static NetClientHandler setNetClientHandler(
			final GuiConnecting par0GuiConnecting,
			final NetClientHandler par1NetClientHandler) {
		return par0GuiConnecting.clientHandler = par1NetClientHandler;
	}

	static Minecraft func_74256_a(final GuiConnecting par0GuiConnecting) {
		return par0GuiConnecting.mc;
	}

	static boolean isCancelled(final GuiConnecting par0GuiConnecting) {
		return par0GuiConnecting.cancelled;
	}

	static Minecraft func_74254_c(final GuiConnecting par0GuiConnecting) {
		return par0GuiConnecting.mc;
	}

	/**
	 * Gets the NetClientHandler.
	 */
	static NetClientHandler getNetClientHandler(
			final GuiConnecting par0GuiConnecting) {
		return par0GuiConnecting.clientHandler;
	}

	static GuiScreen func_98097_e(final GuiConnecting par0GuiConnecting) {
		return par0GuiConnecting.field_98098_c;
	}

	static Minecraft func_74250_f(final GuiConnecting par0GuiConnecting) {
		return par0GuiConnecting.mc;
	}

	static Minecraft func_74251_g(final GuiConnecting par0GuiConnecting) {
		return par0GuiConnecting.mc;
	}

	static Minecraft func_98096_h(final GuiConnecting par0GuiConnecting) {
		return par0GuiConnecting.mc;
	}
}
