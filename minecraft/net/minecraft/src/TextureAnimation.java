package net.minecraft.src;

import java.nio.ByteBuffer;
import java.util.Properties;

import org.lwjgl.opengl.GL11;

public class TextureAnimation {
	private int dstTextId = -1;
	private int dstX = 0;
	private int dstY = 0;
	private int frameWidth = 0;
	private int frameHeight = 0;
	private CustomAnimationFrame[] frames = null;
	private int activeFrame = 0;
	private ByteBuffer imageData = null;

	public TextureAnimation(final String var1, final byte[] var2,
			final String var3, final int var4, final int var5, final int var6,
			final int var7, final int var8, final Properties var9,
			final int var10) {
		dstTextId = var4;
		dstX = var5;
		dstY = var6;
		frameWidth = var7;
		frameHeight = var8;
		final int var11 = var7 * var8 * 4;

		if (var2.length % var11 != 0) {
			Config.dbg("Invalid animated texture length: " + var2.length
					+ ", frameWidth: " + var8 + ", frameHeight: " + var8);
		}

		imageData = GLAllocation.createDirectByteBuffer(var2.length);
		imageData.put(var2);
		int var12 = var2.length / var11;

		if (var9.get("tile.0") != null) {
			for (int var13 = 0; var9.get("tile." + var13) != null; ++var13) {
				var12 = var13 + 1;
			}
		}

		final String var21 = (String) var9.get("duration");
		final int var14 = Config.parseInt(var21, var10);
		frames = new CustomAnimationFrame[var12];

		for (int var15 = 0; var15 < frames.length; ++var15) {
			final String var16 = (String) var9.get("tile." + var15);
			final int var17 = Config.parseInt(var16, var15);
			final String var18 = (String) var9.get("duration." + var15);
			final int var19 = Config.parseInt(var18, var14);
			final CustomAnimationFrame var20 = new CustomAnimationFrame(var17,
					var19);
			frames[var15] = var20;
		}
	}

	public boolean nextFrame() {
		if (frames.length <= 0) {
			return false;
		} else {
			if (activeFrame >= frames.length) {
				activeFrame = 0;
			}

			final CustomAnimationFrame var1 = frames[activeFrame];
			++var1.counter;

			if (var1.counter < var1.duration) {
				return false;
			} else {
				var1.counter = 0;
				++activeFrame;

				if (activeFrame >= frames.length) {
					activeFrame = 0;
				}

				return true;
			}
		}
	}

	public int getActiveFrameIndex() {
		if (frames.length <= 0) {
			return 0;
		} else {
			if (activeFrame >= frames.length) {
				activeFrame = 0;
			}

			final CustomAnimationFrame var1 = frames[activeFrame];
			return var1.index;
		}
	}

	public int getFrameCount() {
		return frames.length;
	}

	public boolean updateTexture() {
		if (!nextFrame()) {
			return false;
		} else {
			final int var1 = frameWidth * frameHeight * 4;
			final int var2 = getActiveFrameIndex();
			final int var3 = var1 * var2;

			if (var3 + var1 > imageData.capacity()) {
				return false;
			} else {
				imageData.position(var3);
				Config.getRenderEngine().bindTexture(dstTextId);
				GL11.glTexSubImage2D(GL11.GL_TEXTURE_2D, 0, dstX, dstY,
						frameWidth, frameHeight, GL11.GL_RGBA,
						GL11.GL_UNSIGNED_BYTE, imageData);
				return true;
			}
		}
	}
}
