package net.minecraft.src;

public class EntitySpider extends EntityMob {
	public EntitySpider(final World par1World) {
		super(par1World);
		texture = "/mob/spider.png";
		setSize(1.4F, 0.9F);
		moveSpeed = 0.8F;
	}

	@Override
	protected void entityInit() {
		super.entityInit();
		dataWatcher.addObject(16, new Byte((byte) 0));
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate() {
		super.onUpdate();

		if (!worldObj.isRemote) {
			setBesideClimbableBlock(isCollidedHorizontally);
		}
	}

	@Override
	public int getMaxHealth() {
		return 16;
	}

	/**
	 * Returns the Y offset from the entity's position for any entity riding
	 * this one.
	 */
	@Override
	public double getMountedYOffset() {
		return height * 0.75D - 0.5D;
	}

	/**
	 * Finds the closest player within 16 blocks to attack, or null if this
	 * Entity isn't interested in attacking (Animals, Spiders at day, peaceful
	 * PigZombies).
	 */
	@Override
	protected Entity findPlayerToAttack() {
		final float var1 = getBrightness(1.0F);

		if (var1 < 0.5F) {
			final double var2 = 16.0D;
			return worldObj.getClosestVulnerablePlayerToEntity(this, var2);
		} else {
			return null;
		}
	}

	/**
	 * Returns the sound this mob makes while it's alive.
	 */
	@Override
	protected String getLivingSound() {
		return "mob.spider.say";
	}

	/**
	 * Returns the sound this mob makes when it is hurt.
	 */
	@Override
	protected String getHurtSound() {
		return "mob.spider.say";
	}

	/**
	 * Returns the sound this mob makes on death.
	 */
	@Override
	protected String getDeathSound() {
		return "mob.spider.death";
	}

	/**
	 * Plays step sound at given x, y, z for the entity
	 */
	@Override
	protected void playStepSound(final int par1, final int par2,
			final int par3, final int par4) {
		playSound("mob.spider.step", 0.15F, 1.0F);
	}

	/**
	 * Basic mob attack. Default to touch of death in EntityCreature. Overridden
	 * by each mob to define their attack.
	 */
	@Override
	protected void attackEntity(final Entity par1Entity, final float par2) {
		final float var3 = getBrightness(1.0F);

		if (var3 > 0.5F && rand.nextInt(100) == 0) {
			entityToAttack = null;
		} else {
			if (par2 > 2.0F && par2 < 6.0F && rand.nextInt(10) == 0) {
				if (onGround) {
					final double var4 = par1Entity.posX - posX;
					final double var6 = par1Entity.posZ - posZ;
					final float var8 = MathHelper.sqrt_double(var4 * var4
							+ var6 * var6);
					motionX = var4 / var8 * 0.5D * 0.800000011920929D + motionX
							* 0.20000000298023224D;
					motionZ = var6 / var8 * 0.5D * 0.800000011920929D + motionZ
							* 0.20000000298023224D;
					motionY = 0.4000000059604645D;
				}
			} else {
				super.attackEntity(par1Entity, par2);
			}
		}
	}

	/**
	 * Returns the item ID for the item the mob drops on death.
	 */
	@Override
	protected int getDropItemId() {
		return Item.silk.itemID;
	}

	/**
	 * Drop 0-2 items of this living's type. @param par1 - Whether this entity
	 * has recently been hit by a player. @param par2 - Level of Looting used to
	 * kill this mob.
	 */
	@Override
	protected void dropFewItems(final boolean par1, final int par2) {
		super.dropFewItems(par1, par2);

		if (par1 && (rand.nextInt(3) == 0 || rand.nextInt(1 + par2) > 0)) {
			dropItem(Item.spiderEye.itemID, 1);
		}
	}

	/**
	 * returns true if this entity is by a ladder, false otherwise
	 */
	@Override
	public boolean isOnLadder() {
		return isBesideClimbableBlock();
	}

	/**
	 * Sets the Entity inside a web block.
	 */
	@Override
	public void setInWeb() {
	}

	/**
	 * How large the spider should be scaled.
	 */
	public float spiderScaleAmount() {
		return 1.0F;
	}

	/**
	 * Get this Entity's EnumCreatureAttribute
	 */
	@Override
	public EnumCreatureAttribute getCreatureAttribute() {
		return EnumCreatureAttribute.ARTHROPOD;
	}

	@Override
	public boolean isPotionApplicable(final PotionEffect par1PotionEffect) {
		return par1PotionEffect.getPotionID() == Potion.poison.id ? false
				: super.isPotionApplicable(par1PotionEffect);
	}

	/**
	 * Returns true if the WatchableObject (Byte) is 0x01 otherwise returns
	 * false. The WatchableObject is updated using setBesideClimableBlock.
	 */
	public boolean isBesideClimbableBlock() {
		return (dataWatcher.getWatchableObjectByte(16) & 1) != 0;
	}

	/**
	 * Updates the WatchableObject (Byte) created in entityInit(), setting it to
	 * 0x01 if par1 is true or 0x00 if it is false.
	 */
	public void setBesideClimbableBlock(final boolean par1) {
		byte var2 = dataWatcher.getWatchableObjectByte(16);

		if (par1) {
			var2 = (byte) (var2 | 1);
		} else {
			var2 &= -2;
		}

		dataWatcher.updateObject(16, Byte.valueOf(var2));
	}

	/**
	 * Initialize this creature.
	 */
	@Override
	public void initCreature() {
		if (worldObj.rand.nextInt(100) == 0) {
			final EntitySkeleton var1 = new EntitySkeleton(worldObj);
			var1.setLocationAndAngles(posX, posY, posZ, rotationYaw, 0.0F);
			var1.initCreature();
			worldObj.spawnEntityInWorld(var1);
			var1.mountEntity(this);
		}
	}
}
