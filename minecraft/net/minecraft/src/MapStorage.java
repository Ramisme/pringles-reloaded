package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MapStorage {
	private final ISaveHandler saveHandler;

	/** Map of item data String id to loaded MapDataBases */
	private final Map loadedDataMap = new HashMap();

	/** List of loaded MapDataBases. */
	private final List loadedDataList = new ArrayList();

	/**
	 * Map of MapDataBase id String prefixes ('map' etc) to max known unique
	 * Short id (the 0 part etc) for that prefix
	 */
	private final Map idCounts = new HashMap();

	public MapStorage(final ISaveHandler par1ISaveHandler) {
		saveHandler = par1ISaveHandler;
		loadIdCounts();
	}

	/**
	 * Loads an existing MapDataBase corresponding to the given String id from
	 * disk, instantiating the given Class, or returns null if none such file
	 * exists. args: Class to instantiate, String dataid
	 */
	public WorldSavedData loadData(final Class par1Class, final String par2Str) {
		WorldSavedData var3 = (WorldSavedData) loadedDataMap.get(par2Str);

		if (var3 != null) {
			return var3;
		} else {
			if (saveHandler != null) {
				try {
					final File var4 = saveHandler.getMapFileFromName(par2Str);

					if (var4 != null && var4.exists()) {
						try {
							var3 = (WorldSavedData) par1Class.getConstructor(
									new Class[] { String.class }).newInstance(
									new Object[] { par2Str });
						} catch (final Exception var7) {
							throw new RuntimeException("Failed to instantiate "
									+ par1Class.toString(), var7);
						}

						final FileInputStream var5 = new FileInputStream(var4);
						final NBTTagCompound var6 = CompressedStreamTools
								.readCompressed(var5);
						var5.close();
						var3.readFromNBT(var6.getCompoundTag("data"));
					}
				} catch (final Exception var8) {
					var8.printStackTrace();
				}
			}

			if (var3 != null) {
				loadedDataMap.put(par2Str, var3);
				loadedDataList.add(var3);
			}

			return var3;
		}
	}

	/**
	 * Assigns the given String id to the given MapDataBase, removing any
	 * existing ones of the same id.
	 */
	public void setData(final String par1Str,
			final WorldSavedData par2WorldSavedData) {
		if (par2WorldSavedData == null) {
			throw new RuntimeException("Can\'t set null data");
		} else {
			if (loadedDataMap.containsKey(par1Str)) {
				loadedDataList.remove(loadedDataMap.remove(par1Str));
			}

			loadedDataMap.put(par1Str, par2WorldSavedData);
			loadedDataList.add(par2WorldSavedData);
		}
	}

	/**
	 * Saves all dirty loaded MapDataBases to disk.
	 */
	public void saveAllData() {
		for (int var1 = 0; var1 < loadedDataList.size(); ++var1) {
			final WorldSavedData var2 = (WorldSavedData) loadedDataList
					.get(var1);

			if (var2.isDirty()) {
				saveData(var2);
				var2.setDirty(false);
			}
		}
	}

	/**
	 * Saves the given MapDataBase to disk.
	 */
	private void saveData(final WorldSavedData par1WorldSavedData) {
		if (saveHandler != null) {
			try {
				final File var2 = saveHandler
						.getMapFileFromName(par1WorldSavedData.mapName);

				if (var2 != null) {
					final NBTTagCompound var3 = new NBTTagCompound();
					par1WorldSavedData.writeToNBT(var3);
					final NBTTagCompound var4 = new NBTTagCompound();
					var4.setCompoundTag("data", var3);
					final FileOutputStream var5 = new FileOutputStream(var2);
					CompressedStreamTools.writeCompressed(var4, var5);
					var5.close();
				}
			} catch (final Exception var6) {
				var6.printStackTrace();
			}
		}
	}

	/**
	 * Loads the idCounts Map from the 'idcounts' file.
	 */
	private void loadIdCounts() {
		try {
			idCounts.clear();

			if (saveHandler == null) {
				return;
			}

			final File var1 = saveHandler.getMapFileFromName("idcounts");

			if (var1 != null && var1.exists()) {
				final DataInputStream var2 = new DataInputStream(
						new FileInputStream(var1));
				final NBTTagCompound var3 = CompressedStreamTools.read(var2);
				var2.close();
				final Iterator var4 = var3.getTags().iterator();

				while (var4.hasNext()) {
					final NBTBase var5 = (NBTBase) var4.next();

					if (var5 instanceof NBTTagShort) {
						final NBTTagShort var6 = (NBTTagShort) var5;
						final String var7 = var6.getName();
						final short var8 = var6.data;
						idCounts.put(var7, Short.valueOf(var8));
					}
				}
			}
		} catch (final Exception var9) {
			var9.printStackTrace();
		}
	}

	/**
	 * Returns an unique new data id for the given prefix and saves the idCounts
	 * map to the 'idcounts' file.
	 */
	public int getUniqueDataId(final String par1Str) {
		Short var2 = (Short) idCounts.get(par1Str);

		if (var2 == null) {
			var2 = Short.valueOf((short) 0);
		} else {
			var2 = Short.valueOf((short) (var2.shortValue() + 1));
		}

		idCounts.put(par1Str, var2);

		if (saveHandler == null) {
			return var2.shortValue();
		} else {
			try {
				final File var3 = saveHandler.getMapFileFromName("idcounts");

				if (var3 != null) {
					final NBTTagCompound var4 = new NBTTagCompound();
					final Iterator var5 = idCounts.keySet().iterator();

					while (var5.hasNext()) {
						final String var6 = (String) var5.next();
						final short var7 = ((Short) idCounts.get(var6))
								.shortValue();
						var4.setShort(var6, var7);
					}

					final DataOutputStream var9 = new DataOutputStream(
							new FileOutputStream(var3));
					CompressedStreamTools.write(var4, var9);
					var9.close();
				}
			} catch (final Exception var8) {
				var8.printStackTrace();
			}

			return var2.shortValue();
		}
	}
}
