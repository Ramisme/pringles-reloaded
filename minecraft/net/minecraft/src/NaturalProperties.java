package net.minecraft.src;

public class NaturalProperties {
	public int rotation = 1;
	public boolean flip = false;

	public NaturalProperties(final String var1) {
		if (var1.equals("4")) {
			rotation = 4;
		} else if (var1.equals("2")) {
			rotation = 2;
		} else if (var1.equals("F")) {
			flip = true;
		} else if (var1.equals("4F")) {
			rotation = 4;
			flip = true;
		} else if (var1.equals("2F")) {
			rotation = 2;
			flip = true;
		} else {
			Config.dbg("NaturalTextures: Unknown type: " + var1);
		}
	}

	public boolean isValid() {
		return rotation != 2 && rotation != 4 ? flip : true;
	}
}
