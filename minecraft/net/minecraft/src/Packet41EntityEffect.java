package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet41EntityEffect extends Packet {
	public int entityId;
	public byte effectId;

	/** The effect's amplifier. */
	public byte effectAmplifier;
	public short duration;

	public Packet41EntityEffect() {
	}

	public Packet41EntityEffect(final int par1,
			final PotionEffect par2PotionEffect) {
		entityId = par1;
		effectId = (byte) (par2PotionEffect.getPotionID() & 255);
		effectAmplifier = (byte) (par2PotionEffect.getAmplifier() & 255);

		if (par2PotionEffect.getDuration() > 32767) {
			duration = 32767;
		} else {
			duration = (short) par2PotionEffect.getDuration();
		}
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		entityId = par1DataInputStream.readInt();
		effectId = par1DataInputStream.readByte();
		effectAmplifier = par1DataInputStream.readByte();
		duration = par1DataInputStream.readShort();
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		par1DataOutputStream.writeInt(entityId);
		par1DataOutputStream.writeByte(effectId);
		par1DataOutputStream.writeByte(effectAmplifier);
		par1DataOutputStream.writeShort(duration);
	}

	/**
	 * Returns true if duration is at maximum, false otherwise.
	 */
	public boolean isDurationMax() {
		return duration == 32767;
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleEntityEffect(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 8;
	}

	/**
	 * only false for the abstract Packet class, all real packets return true
	 */
	@Override
	public boolean isRealPacket() {
		return true;
	}

	/**
	 * eg return packet30entity.entityId == entityId; WARNING : will throw if
	 * you compare a packet to a different packet class
	 */
	@Override
	public boolean containsSameEntityIDAs(final Packet par1Packet) {
		final Packet41EntityEffect var2 = (Packet41EntityEffect) par1Packet;
		return var2.entityId == entityId && var2.effectId == effectId;
	}
}
