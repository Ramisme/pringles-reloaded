package net.minecraft.src;

import org.lwjgl.opengl.GL11;

public class RenderOcelot extends RenderLiving {
	public RenderOcelot(final ModelBase par1ModelBase, final float par2) {
		super(par1ModelBase, par2);
	}

	public void renderLivingOcelot(final EntityOcelot par1EntityOcelot,
			final double par2, final double par4, final double par6,
			final float par8, final float par9) {
		super.doRenderLiving(par1EntityOcelot, par2, par4, par6, par8, par9);
	}

	/**
	 * Pre-Renders the Ocelot.
	 */
	protected void preRenderOcelot(final EntityOcelot par1EntityOcelot,
			final float par2) {
		super.preRenderCallback(par1EntityOcelot, par2);

		if (par1EntityOcelot.isTamed()) {
			GL11.glScalef(0.8F, 0.8F, 0.8F);
		}
	}

	/**
	 * Allows the render to do any OpenGL state modifications necessary before
	 * the model is rendered. Args: entityLiving, partialTickTime
	 */
	@Override
	protected void preRenderCallback(final EntityLiving par1EntityLiving,
			final float par2) {
		preRenderOcelot((EntityOcelot) par1EntityLiving, par2);
	}

	@Override
	public void doRenderLiving(final EntityLiving par1EntityLiving,
			final double par2, final double par4, final double par6,
			final float par8, final float par9) {
		renderLivingOcelot((EntityOcelot) par1EntityLiving, par2, par4, par6,
				par8, par9);
	}

	/**
	 * Actually renders the given argument. This is a synthetic bridge method,
	 * always casting down its argument and then handing it off to a worker
	 * function which does the actual work. In all probabilty, the class Render
	 * is generic (Render<T extends Entity) and this method has signature public
	 * void doRender(T entity, double d, double d1, double d2, float f, float
	 * f1). But JAD is pre 1.5 so doesn't do that.
	 */
	@Override
	public void doRender(final Entity par1Entity, final double par2,
			final double par4, final double par6, final float par8,
			final float par9) {
		renderLivingOcelot((EntityOcelot) par1Entity, par2, par4, par6, par8,
				par9);
	}
}
