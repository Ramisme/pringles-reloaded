package net.minecraft.src;

import java.util.Iterator;
import java.util.List;

public class TileEntityChest extends TileEntity implements IInventory {
	private ItemStack[] chestContents = new ItemStack[36];

	/** Determines if the check for adjacent chests has taken place. */
	public boolean adjacentChestChecked = false;

	/** Contains the chest tile located adjacent to this one (if any) */
	public TileEntityChest adjacentChestZNeg;

	/** Contains the chest tile located adjacent to this one (if any) */
	public TileEntityChest adjacentChestXPos;

	/** Contains the chest tile located adjacent to this one (if any) */
	public TileEntityChest adjacentChestXNeg;

	/** Contains the chest tile located adjacent to this one (if any) */
	public TileEntityChest adjacentChestZPosition;

	/** The current angle of the lid (between 0 and 1) */
	public float lidAngle;

	/** The angle of the lid last tick */
	public float prevLidAngle;

	/** The number of players currently using this chest */
	public int numUsingPlayers;

	/** Server sync counter (once per 20 ticks) */
	private int ticksSinceSync;
	private int field_94046_i = -1;
	private String field_94045_s;

	/**
	 * Returns the number of slots in the inventory.
	 */
	@Override
	public int getSizeInventory() {
		return 27;
	}

	/**
	 * Returns the stack in slot i
	 */
	@Override
	public ItemStack getStackInSlot(final int par1) {
		return chestContents[par1];
	}

	/**
	 * Removes from an inventory slot (first arg) up to a specified number
	 * (second arg) of items and returns them in a new stack.
	 */
	@Override
	public ItemStack decrStackSize(final int par1, final int par2) {
		if (chestContents[par1] != null) {
			ItemStack var3;

			if (chestContents[par1].stackSize <= par2) {
				var3 = chestContents[par1];
				chestContents[par1] = null;
				onInventoryChanged();
				return var3;
			} else {
				var3 = chestContents[par1].splitStack(par2);

				if (chestContents[par1].stackSize == 0) {
					chestContents[par1] = null;
				}

				onInventoryChanged();
				return var3;
			}
		} else {
			return null;
		}
	}

	/**
	 * When some containers are closed they call this on each slot, then drop
	 * whatever it returns as an EntityItem - like when you close a workbench
	 * GUI.
	 */
	@Override
	public ItemStack getStackInSlotOnClosing(final int par1) {
		if (chestContents[par1] != null) {
			final ItemStack var2 = chestContents[par1];
			chestContents[par1] = null;
			return var2;
		} else {
			return null;
		}
	}

	/**
	 * Sets the given item stack to the specified slot in the inventory (can be
	 * crafting or armor sections).
	 */
	@Override
	public void setInventorySlotContents(final int par1,
			final ItemStack par2ItemStack) {
		chestContents[par1] = par2ItemStack;

		if (par2ItemStack != null
				&& par2ItemStack.stackSize > getInventoryStackLimit()) {
			par2ItemStack.stackSize = getInventoryStackLimit();
		}

		onInventoryChanged();
	}

	/**
	 * Returns the name of the inventory.
	 */
	@Override
	public String getInvName() {
		return isInvNameLocalized() ? field_94045_s : "container.chest";
	}

	/**
	 * If this returns false, the inventory name will be used as an unlocalized
	 * name, and translated into the player's language. Otherwise it will be
	 * used directly.
	 */
	@Override
	public boolean isInvNameLocalized() {
		return field_94045_s != null && field_94045_s.length() > 0;
	}

	public void func_94043_a(final String par1Str) {
		field_94045_s = par1Str;
	}

	/**
	 * Reads a tile entity from NBT.
	 */
	@Override
	public void readFromNBT(final NBTTagCompound par1NBTTagCompound) {
		super.readFromNBT(par1NBTTagCompound);
		final NBTTagList var2 = par1NBTTagCompound.getTagList("Items");
		chestContents = new ItemStack[getSizeInventory()];

		if (par1NBTTagCompound.hasKey("CustomName")) {
			field_94045_s = par1NBTTagCompound.getString("CustomName");
		}

		for (int var3 = 0; var3 < var2.tagCount(); ++var3) {
			final NBTTagCompound var4 = (NBTTagCompound) var2.tagAt(var3);
			final int var5 = var4.getByte("Slot") & 255;

			if (var5 >= 0 && var5 < chestContents.length) {
				chestContents[var5] = ItemStack.loadItemStackFromNBT(var4);
			}
		}
	}

	/**
	 * Writes a tile entity to NBT.
	 */
	@Override
	public void writeToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeToNBT(par1NBTTagCompound);
		final NBTTagList var2 = new NBTTagList();

		for (int var3 = 0; var3 < chestContents.length; ++var3) {
			if (chestContents[var3] != null) {
				final NBTTagCompound var4 = new NBTTagCompound();
				var4.setByte("Slot", (byte) var3);
				chestContents[var3].writeToNBT(var4);
				var2.appendTag(var4);
			}
		}

		par1NBTTagCompound.setTag("Items", var2);

		if (isInvNameLocalized()) {
			par1NBTTagCompound.setString("CustomName", field_94045_s);
		}
	}

	/**
	 * Returns the maximum stack size for a inventory slot. Seems to always be
	 * 64, possibly will be extended. *Isn't this more of a set than a get?*
	 */
	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	/**
	 * Do not make give this method the name canInteractWith because it clashes
	 * with Container
	 */
	@Override
	public boolean isUseableByPlayer(final EntityPlayer par1EntityPlayer) {
		return worldObj.getBlockTileEntity(xCoord, yCoord, zCoord) != this ? false
				: par1EntityPlayer.getDistanceSq(xCoord + 0.5D, yCoord + 0.5D,
						zCoord + 0.5D) <= 64.0D;
	}

	/**
	 * Causes the TileEntity to reset all it's cached values for it's container
	 * block, blockID, metaData and in the case of chests, the adjcacent chest
	 * check
	 */
	@Override
	public void updateContainingBlockInfo() {
		super.updateContainingBlockInfo();
		adjacentChestChecked = false;
	}

	private void func_90009_a(final TileEntityChest par1TileEntityChest,
			final int par2) {
		if (par1TileEntityChest.isInvalid()) {
			adjacentChestChecked = false;
		} else if (adjacentChestChecked) {
			switch (par2) {
			case 0:
				if (adjacentChestZPosition != par1TileEntityChest) {
					adjacentChestChecked = false;
				}

				break;

			case 1:
				if (adjacentChestXNeg != par1TileEntityChest) {
					adjacentChestChecked = false;
				}

				break;

			case 2:
				if (adjacentChestZNeg != par1TileEntityChest) {
					adjacentChestChecked = false;
				}

				break;

			case 3:
				if (adjacentChestXPos != par1TileEntityChest) {
					adjacentChestChecked = false;
				}
			}
		}
	}

	/**
	 * Performs the check for adjacent chests to determine if this chest is
	 * double or not.
	 */
	public void checkForAdjacentChests() {
		if (!adjacentChestChecked) {
			adjacentChestChecked = true;
			adjacentChestZNeg = null;
			adjacentChestXPos = null;
			adjacentChestXNeg = null;
			adjacentChestZPosition = null;

			if (func_94044_a(xCoord - 1, yCoord, zCoord)) {
				adjacentChestXNeg = (TileEntityChest) worldObj
						.getBlockTileEntity(xCoord - 1, yCoord, zCoord);
			}

			if (func_94044_a(xCoord + 1, yCoord, zCoord)) {
				adjacentChestXPos = (TileEntityChest) worldObj
						.getBlockTileEntity(xCoord + 1, yCoord, zCoord);
			}

			if (func_94044_a(xCoord, yCoord, zCoord - 1)) {
				adjacentChestZNeg = (TileEntityChest) worldObj
						.getBlockTileEntity(xCoord, yCoord, zCoord - 1);
			}

			if (func_94044_a(xCoord, yCoord, zCoord + 1)) {
				adjacentChestZPosition = (TileEntityChest) worldObj
						.getBlockTileEntity(xCoord, yCoord, zCoord + 1);
			}

			if (adjacentChestZNeg != null) {
				adjacentChestZNeg.func_90009_a(this, 0);
			}

			if (adjacentChestZPosition != null) {
				adjacentChestZPosition.func_90009_a(this, 2);
			}

			if (adjacentChestXPos != null) {
				adjacentChestXPos.func_90009_a(this, 1);
			}

			if (adjacentChestXNeg != null) {
				adjacentChestXNeg.func_90009_a(this, 3);
			}
		}
	}

	private boolean func_94044_a(final int par1, final int par2, final int par3) {
		final Block var4 = Block.blocksList[worldObj.getBlockId(par1, par2,
				par3)];
		return var4 != null && var4 instanceof BlockChest ? ((BlockChest) var4).isTrapped == func_98041_l()
				: false;
	}

	/**
	 * Allows the entity to update its state. Overridden in most subclasses,
	 * e.g. the mob spawner uses this to count ticks and creates a new spawn
	 * inside its implementation.
	 */
	@Override
	public void updateEntity() {
		super.updateEntity();
		checkForAdjacentChests();
		++ticksSinceSync;
		float var1;

		if (!worldObj.isRemote && numUsingPlayers != 0
				&& (ticksSinceSync + xCoord + yCoord + zCoord) % 200 == 0) {
			numUsingPlayers = 0;
			var1 = 5.0F;
			final List var2 = worldObj.getEntitiesWithinAABB(
					EntityPlayer.class,
					AxisAlignedBB.getAABBPool().getAABB(xCoord - var1,
							yCoord - var1, zCoord - var1, xCoord + 1 + var1,
							yCoord + 1 + var1, zCoord + 1 + var1));
			final Iterator var3 = var2.iterator();

			while (var3.hasNext()) {
				final EntityPlayer var4 = (EntityPlayer) var3.next();

				if (var4.openContainer instanceof ContainerChest) {
					final IInventory var5 = ((ContainerChest) var4.openContainer)
							.getLowerChestInventory();

					if (var5 == this
							|| var5 instanceof InventoryLargeChest
							&& ((InventoryLargeChest) var5)
									.isPartOfLargeChest(this)) {
						++numUsingPlayers;
					}
				}
			}
		}

		prevLidAngle = lidAngle;
		var1 = 0.1F;
		double var11;

		if (numUsingPlayers > 0 && lidAngle == 0.0F
				&& adjacentChestZNeg == null && adjacentChestXNeg == null) {
			double var8 = xCoord + 0.5D;
			var11 = zCoord + 0.5D;

			if (adjacentChestZPosition != null) {
				var11 += 0.5D;
			}

			if (adjacentChestXPos != null) {
				var8 += 0.5D;
			}

			worldObj.playSoundEffect(var8, yCoord + 0.5D, var11,
					"random.chestopen", 0.5F,
					worldObj.rand.nextFloat() * 0.1F + 0.9F);
		}

		if (numUsingPlayers == 0 && lidAngle > 0.0F || numUsingPlayers > 0
				&& lidAngle < 1.0F) {
			final float var9 = lidAngle;

			if (numUsingPlayers > 0) {
				lidAngle += var1;
			} else {
				lidAngle -= var1;
			}

			if (lidAngle > 1.0F) {
				lidAngle = 1.0F;
			}

			final float var10 = 0.5F;

			if (lidAngle < var10 && var9 >= var10 && adjacentChestZNeg == null
					&& adjacentChestXNeg == null) {
				var11 = xCoord + 0.5D;
				double var6 = zCoord + 0.5D;

				if (adjacentChestZPosition != null) {
					var6 += 0.5D;
				}

				if (adjacentChestXPos != null) {
					var11 += 0.5D;
				}

				worldObj.playSoundEffect(var11, yCoord + 0.5D, var6,
						"random.chestclosed", 0.5F,
						worldObj.rand.nextFloat() * 0.1F + 0.9F);
			}

			if (lidAngle < 0.0F) {
				lidAngle = 0.0F;
			}
		}
	}

	/**
	 * Called when a client event is received with the event number and
	 * argument, see World.sendClientEvent
	 */
	@Override
	public boolean receiveClientEvent(final int par1, final int par2) {
		if (par1 == 1) {
			numUsingPlayers = par2;
			return true;
		} else {
			return super.receiveClientEvent(par1, par2);
		}
	}

	@Override
	public void openChest() {
		if (numUsingPlayers < 0) {
			numUsingPlayers = 0;
		}

		++numUsingPlayers;
		worldObj.addBlockEvent(xCoord, yCoord, zCoord, getBlockType().blockID,
				1, numUsingPlayers);
		worldObj.notifyBlocksOfNeighborChange(xCoord, yCoord, zCoord,
				getBlockType().blockID);
		worldObj.notifyBlocksOfNeighborChange(xCoord, yCoord - 1, zCoord,
				getBlockType().blockID);
	}

	@Override
	public void closeChest() {
		if (getBlockType() != null && getBlockType() instanceof BlockChest) {
			--numUsingPlayers;
			worldObj.addBlockEvent(xCoord, yCoord, zCoord,
					getBlockType().blockID, 1, numUsingPlayers);
			worldObj.notifyBlocksOfNeighborChange(xCoord, yCoord, zCoord,
					getBlockType().blockID);
			worldObj.notifyBlocksOfNeighborChange(xCoord, yCoord - 1, zCoord,
					getBlockType().blockID);
		}
	}

	/**
	 * Returns true if automation is allowed to insert the given stack (ignoring
	 * stack size) into the given slot.
	 */
	@Override
	public boolean isStackValidForSlot(final int par1,
			final ItemStack par2ItemStack) {
		return true;
	}

	/**
	 * invalidates a tile entity
	 */
	@Override
	public void invalidate() {
		super.invalidate();
		updateContainingBlockInfo();
		checkForAdjacentChests();
	}

	public int func_98041_l() {
		if (field_94046_i == -1) {
			if (worldObj == null || !(getBlockType() instanceof BlockChest)) {
				return 0;
			}

			field_94046_i = ((BlockChest) getBlockType()).isTrapped;
		}

		return field_94046_i;
	}
}
