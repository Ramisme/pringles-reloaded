package net.minecraft.src;

public class EntityEnderPearl extends EntityThrowable {
	public EntityEnderPearl(final World par1World) {
		super(par1World);
	}

	public EntityEnderPearl(final World par1World,
			final EntityLiving par2EntityLiving) {
		super(par1World, par2EntityLiving);
	}

	public EntityEnderPearl(final World par1World, final double par2,
			final double par4, final double par6) {
		super(par1World, par2, par4, par6);
	}

	/**
	 * Called when this EntityThrowable hits a block or entity.
	 */
	@Override
	protected void onImpact(final MovingObjectPosition par1MovingObjectPosition) {
		if (par1MovingObjectPosition.entityHit != null) {
			par1MovingObjectPosition.entityHit.attackEntityFrom(
					DamageSource.causeThrownDamage(this, getThrower()), 0);
		}

		for (int var2 = 0; var2 < 32; ++var2) {
			worldObj.spawnParticle("portal", posX, posY + rand.nextDouble()
					* 2.0D, posZ, rand.nextGaussian(), 0.0D,
					rand.nextGaussian());
		}

		if (!worldObj.isRemote) {
			if (getThrower() != null && getThrower() instanceof EntityPlayerMP) {
				final EntityPlayerMP var3 = (EntityPlayerMP) getThrower();

				if (!var3.playerNetServerHandler.connectionClosed
						&& var3.worldObj == worldObj) {
					getThrower().setPositionAndUpdate(posX, posY, posZ);
					getThrower().fallDistance = 0.0F;
					getThrower().attackEntityFrom(DamageSource.fall, 5);
				}
			}

			setDead();
		}
	}
}
