package net.minecraft.src;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class EntityAITasks {
	/** A list of EntityAITaskEntrys in EntityAITasks. */
	private final List taskEntries = new ArrayList();

	/** A list of EntityAITaskEntrys that are currently being executed. */
	private final List executingTaskEntries = new ArrayList();

	/** Instance of Profiler. */
	private final Profiler theProfiler;
	private int field_75778_d = 0;
	private final int field_75779_e = 3;

	public EntityAITasks(final Profiler par1Profiler) {
		theProfiler = par1Profiler;
	}

	public void addTask(final int par1, final EntityAIBase par2EntityAIBase) {
		taskEntries.add(new EntityAITaskEntry(this, par1, par2EntityAIBase));
	}

	/**
	 * removes the indicated task from the entity's AI tasks.
	 */
	public void removeTask(final EntityAIBase par1EntityAIBase) {
		final Iterator var2 = taskEntries.iterator();

		while (var2.hasNext()) {
			final EntityAITaskEntry var3 = (EntityAITaskEntry) var2.next();
			final EntityAIBase var4 = var3.action;

			if (var4 == par1EntityAIBase) {
				if (executingTaskEntries.contains(var3)) {
					var4.resetTask();
					executingTaskEntries.remove(var3);
				}

				var2.remove();
			}
		}
	}

	public void onUpdateTasks() {
		final ArrayList var1 = new ArrayList();
		Iterator var2;
		EntityAITaskEntry var3;

		if (field_75778_d++ % field_75779_e == 0) {
			var2 = taskEntries.iterator();

			while (var2.hasNext()) {
				var3 = (EntityAITaskEntry) var2.next();
				final boolean var4 = executingTaskEntries.contains(var3);

				if (var4) {
					if (canUse(var3) && canContinue(var3)) {
						continue;
					}

					var3.action.resetTask();
					executingTaskEntries.remove(var3);
				}

				if (canUse(var3) && var3.action.shouldExecute()) {
					var1.add(var3);
					executingTaskEntries.add(var3);
				}
			}
		} else {
			var2 = executingTaskEntries.iterator();

			while (var2.hasNext()) {
				var3 = (EntityAITaskEntry) var2.next();

				if (!var3.action.continueExecuting()) {
					var3.action.resetTask();
					var2.remove();
				}
			}
		}

		theProfiler.startSection("goalStart");
		var2 = var1.iterator();

		while (var2.hasNext()) {
			var3 = (EntityAITaskEntry) var2.next();
			theProfiler.startSection(var3.action.getClass().getSimpleName());
			var3.action.startExecuting();
			theProfiler.endSection();
		}

		theProfiler.endSection();
		theProfiler.startSection("goalTick");
		var2 = executingTaskEntries.iterator();

		while (var2.hasNext()) {
			var3 = (EntityAITaskEntry) var2.next();
			var3.action.updateTask();
		}

		theProfiler.endSection();
	}

	/**
	 * Determine if a specific AI Task should continue being executed.
	 */
	private boolean canContinue(final EntityAITaskEntry par1EntityAITaskEntry) {
		theProfiler.startSection("canContinue");
		final boolean var2 = par1EntityAITaskEntry.action.continueExecuting();
		theProfiler.endSection();
		return var2;
	}

	/**
	 * Determine if a specific AI Task can be executed, which means that all
	 * running higher (= lower int value) priority tasks are compatible with it
	 * or all lower priority tasks can be interrupted.
	 */
	private boolean canUse(final EntityAITaskEntry par1EntityAITaskEntry) {
		theProfiler.startSection("canUse");
		final Iterator var2 = taskEntries.iterator();

		while (var2.hasNext()) {
			final EntityAITaskEntry var3 = (EntityAITaskEntry) var2.next();

			if (var3 != par1EntityAITaskEntry) {
				if (par1EntityAITaskEntry.priority >= var3.priority) {
					if (executingTaskEntries.contains(var3)
							&& !areTasksCompatible(par1EntityAITaskEntry, var3)) {
						theProfiler.endSection();
						return false;
					}
				} else if (executingTaskEntries.contains(var3)
						&& !var3.action.isInterruptible()) {
					theProfiler.endSection();
					return false;
				}
			}
		}

		theProfiler.endSection();
		return true;
	}

	/**
	 * Returns whether two EntityAITaskEntries can be executed concurrently
	 */
	private boolean areTasksCompatible(
			final EntityAITaskEntry par1EntityAITaskEntry,
			final EntityAITaskEntry par2EntityAITaskEntry) {
		return (par1EntityAITaskEntry.action.getMutexBits() & par2EntityAITaskEntry.action
				.getMutexBits()) == 0;
	}
}
