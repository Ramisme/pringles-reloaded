package net.minecraft.src;

public class ExceptionMcoHttp extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2397868763906381799L;

	public ExceptionMcoHttp(final String par1Str, final Exception par2Exception) {
		super(par1Str, par2Exception);
	}
}
