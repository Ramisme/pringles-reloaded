package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet61DoorChange extends Packet {
	public int sfxID;
	public int auxData;
	public int posX;
	public int posY;
	public int posZ;
	private boolean disableRelativeVolume;

	public Packet61DoorChange() {
	}

	public Packet61DoorChange(final int par1, final int par2, final int par3,
			final int par4, final int par5, final boolean par6) {
		sfxID = par1;
		posX = par2;
		posY = par3;
		posZ = par4;
		auxData = par5;
		disableRelativeVolume = par6;
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		sfxID = par1DataInputStream.readInt();
		posX = par1DataInputStream.readInt();
		posY = par1DataInputStream.readByte() & 255;
		posZ = par1DataInputStream.readInt();
		auxData = par1DataInputStream.readInt();
		disableRelativeVolume = par1DataInputStream.readBoolean();
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		par1DataOutputStream.writeInt(sfxID);
		par1DataOutputStream.writeInt(posX);
		par1DataOutputStream.writeByte(posY & 255);
		par1DataOutputStream.writeInt(posZ);
		par1DataOutputStream.writeInt(auxData);
		par1DataOutputStream.writeBoolean(disableRelativeVolume);
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleDoorChange(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 21;
	}

	public boolean getRelativeVolumeDisabled() {
		return disableRelativeVolume;
	}
}
