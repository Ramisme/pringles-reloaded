package net.minecraft.src;

public class BehaviorDefaultDispenseItem implements IBehaviorDispenseItem {
	/**
	 * Dispenses the specified ItemStack from a dispenser.
	 */
	@Override
	public final ItemStack dispense(final IBlockSource par1IBlockSource,
			final ItemStack par2ItemStack) {
		final ItemStack var3 = dispenseStack(par1IBlockSource, par2ItemStack);
		playDispenseSound(par1IBlockSource);
		spawnDispenseParticles(par1IBlockSource,
				BlockDispenser.getFacing(par1IBlockSource.getBlockMetadata()));
		return var3;
	}

	/**
	 * Dispense the specified stack, play the dispense sound and spawn
	 * particles.
	 */
	protected ItemStack dispenseStack(final IBlockSource par1IBlockSource,
			final ItemStack par2ItemStack) {
		final EnumFacing var3 = BlockDispenser.getFacing(par1IBlockSource
				.getBlockMetadata());
		final IPosition var4 = BlockDispenser
				.getIPositionFromBlockSource(par1IBlockSource);
		final ItemStack var5 = par2ItemStack.splitStack(1);
		BehaviorDefaultDispenseItem.doDispense(par1IBlockSource.getWorld(),
				var5, 6, var3, var4);
		return par2ItemStack;
	}

	public static void doDispense(final World par0World,
			final ItemStack par1ItemStack, final int par2,
			final EnumFacing par3EnumFacing, final IPosition par4IPosition) {
		final double var5 = par4IPosition.getX();
		final double var7 = par4IPosition.getY();
		final double var9 = par4IPosition.getZ();
		final EntityItem var11 = new EntityItem(par0World, var5, var7 - 0.3D,
				var9, par1ItemStack);
		final double var12 = par0World.rand.nextDouble() * 0.1D + 0.2D;
		var11.motionX = par3EnumFacing.getFrontOffsetX() * var12;
		var11.motionY = 0.20000000298023224D;
		var11.motionZ = par3EnumFacing.getFrontOffsetZ() * var12;
		var11.motionX += par0World.rand.nextGaussian() * 0.007499999832361937D
				* par2;
		var11.motionY += par0World.rand.nextGaussian() * 0.007499999832361937D
				* par2;
		var11.motionZ += par0World.rand.nextGaussian() * 0.007499999832361937D
				* par2;
		par0World.spawnEntityInWorld(var11);
	}

	/**
	 * Play the dispense sound from the specified block.
	 */
	protected void playDispenseSound(final IBlockSource par1IBlockSource) {
		par1IBlockSource.getWorld().playAuxSFX(1000,
				par1IBlockSource.getXInt(), par1IBlockSource.getYInt(),
				par1IBlockSource.getZInt(), 0);
	}

	/**
	 * Order clients to display dispense particles from the specified block and
	 * facing.
	 */
	protected void spawnDispenseParticles(final IBlockSource par1IBlockSource,
			final EnumFacing par2EnumFacing) {
		par1IBlockSource.getWorld().playAuxSFX(2000,
				par1IBlockSource.getXInt(), par1IBlockSource.getYInt(),
				par1IBlockSource.getZInt(), func_82488_a(par2EnumFacing));
	}

	private int func_82488_a(final EnumFacing par1EnumFacing) {
		return par1EnumFacing.getFrontOffsetX() + 1
				+ (par1EnumFacing.getFrontOffsetZ() + 1) * 3;
	}
}
