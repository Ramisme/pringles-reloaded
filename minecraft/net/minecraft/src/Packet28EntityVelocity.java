package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet28EntityVelocity extends Packet {
	public int entityId;
	public int motionX;
	public int motionY;
	public int motionZ;

	public Packet28EntityVelocity() {
	}

	public Packet28EntityVelocity(final Entity par1Entity) {
		this(par1Entity.entityId, par1Entity.motionX, par1Entity.motionY,
				par1Entity.motionZ);
	}

	public Packet28EntityVelocity(final int par1, double par2, double par4,
			double par6) {
		entityId = par1;
		final double var8 = 3.9D;

		if (par2 < -var8) {
			par2 = -var8;
		}

		if (par4 < -var8) {
			par4 = -var8;
		}

		if (par6 < -var8) {
			par6 = -var8;
		}

		if (par2 > var8) {
			par2 = var8;
		}

		if (par4 > var8) {
			par4 = var8;
		}

		if (par6 > var8) {
			par6 = var8;
		}

		motionX = (int) (par2 * 8000.0D);
		motionY = (int) (par4 * 8000.0D);
		motionZ = (int) (par6 * 8000.0D);
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		entityId = par1DataInputStream.readInt();
		motionX = par1DataInputStream.readShort();
		motionY = par1DataInputStream.readShort();
		motionZ = par1DataInputStream.readShort();
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		par1DataOutputStream.writeInt(entityId);
		par1DataOutputStream.writeShort(motionX);
		par1DataOutputStream.writeShort(motionY);
		par1DataOutputStream.writeShort(motionZ);
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleEntityVelocity(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 10;
	}

	/**
	 * only false for the abstract Packet class, all real packets return true
	 */
	@Override
	public boolean isRealPacket() {
		return true;
	}

	/**
	 * eg return packet30entity.entityId == entityId; WARNING : will throw if
	 * you compare a packet to a different packet class
	 */
	@Override
	public boolean containsSameEntityIDAs(final Packet par1Packet) {
		final Packet28EntityVelocity var2 = (Packet28EntityVelocity) par1Packet;
		return var2.entityId == entityId;
	}
}
