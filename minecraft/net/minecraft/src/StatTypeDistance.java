package net.minecraft.src;

final class StatTypeDistance implements IStatType {
	/**
	 * Formats a given stat for human consumption.
	 */
	@Override
	public String format(final int par1) {
		final double var2 = par1 / 100.0D;
		final double var4 = var2 / 1000.0D;
		return var4 > 0.5D ? StatBase.getDecimalFormat().format(var4) + " km"
				: var2 > 0.5D ? StatBase.getDecimalFormat().format(var2) + " m"
						: par1 + " cm";
	}
}
