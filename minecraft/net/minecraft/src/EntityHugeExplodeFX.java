package net.minecraft.src;

public class EntityHugeExplodeFX extends EntityFX {
	private int timeSinceStart = 0;

	/** the maximum time for the explosion */
	private int maximumTime = 0;

	public EntityHugeExplodeFX(final World par1World, final double par2,
			final double par4, final double par6, final double par8,
			final double par10, final double par12) {
		super(par1World, par2, par4, par6, 0.0D, 0.0D, 0.0D);
		maximumTime = 8;
	}

	@Override
	public void renderParticle(final Tessellator par1Tessellator,
			final float par2, final float par3, final float par4,
			final float par5, final float par6, final float par7) {
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate() {
		for (int var1 = 0; var1 < 6; ++var1) {
			final double var2 = posX + (rand.nextDouble() - rand.nextDouble())
					* 4.0D;
			final double var4 = posY + (rand.nextDouble() - rand.nextDouble())
					* 4.0D;
			final double var6 = posZ + (rand.nextDouble() - rand.nextDouble())
					* 4.0D;
			worldObj.spawnParticle("largeexplode", var2, var4, var6,
					(float) timeSinceStart / (float) maximumTime, 0.0D, 0.0D);
		}

		++timeSinceStart;

		if (timeSinceStart == maximumTime) {
			setDead();
		}
	}

	@Override
	public int getFXLayer() {
		return 1;
	}
}
