package net.minecraft.src;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class LanServerList {
	private final ArrayList listOfLanServers = new ArrayList();
	boolean wasUpdated;

	public synchronized boolean getWasUpdated() {
		return wasUpdated;
	}

	public synchronized void setWasNotUpdated() {
		wasUpdated = false;
	}

	public synchronized List getLanServers() {
		return Collections.unmodifiableList(listOfLanServers);
	}

	public synchronized void func_77551_a(final String par1Str,
			final InetAddress par2InetAddress) {
		final String var3 = ThreadLanServerPing
				.getMotdFromPingResponse(par1Str);
		String var4 = ThreadLanServerPing.getAdFromPingResponse(par1Str);

		if (var4 != null) {
			final int var5 = var4.indexOf(58);

			if (var5 > 0) {
				var4 = par2InetAddress.getHostAddress() + var4.substring(var5);
			}

			boolean var6 = false;
			final Iterator var7 = listOfLanServers.iterator();

			while (var7.hasNext()) {
				final LanServer var8 = (LanServer) var7.next();

				if (var8.getServerIpPort().equals(var4)) {
					var8.updateLastSeen();
					var6 = true;
					break;
				}
			}

			if (!var6) {
				listOfLanServers.add(new LanServer(var3, var4));
				wasUpdated = true;
			}
		}
	}
}
