package net.minecraft.src;

public class RenderSilverfish extends RenderLiving {
	public RenderSilverfish() {
		super(new ModelSilverfish(), 0.3F);
	}

	/**
	 * Return the silverfish's maximum death rotation.
	 */
	protected float getSilverfishDeathRotation(
			final EntitySilverfish par1EntitySilverfish) {
		return 180.0F;
	}

	/**
	 * Renders the silverfish.
	 */
	public void renderSilverfish(final EntitySilverfish par1EntitySilverfish,
			final double par2, final double par4, final double par6,
			final float par8, final float par9) {
		super.doRenderLiving(par1EntitySilverfish, par2, par4, par6, par8, par9);
	}

	/**
	 * Disallows the silverfish to render the renderPassModel.
	 */
	protected int shouldSilverfishRenderPass(
			final EntitySilverfish par1EntitySilverfish, final int par2,
			final float par3) {
		return -1;
	}

	@Override
	protected float getDeathMaxRotation(final EntityLiving par1EntityLiving) {
		return getSilverfishDeathRotation((EntitySilverfish) par1EntityLiving);
	}

	/**
	 * Queries whether should render the specified pass or not.
	 */
	@Override
	protected int shouldRenderPass(final EntityLiving par1EntityLiving,
			final int par2, final float par3) {
		return shouldSilverfishRenderPass((EntitySilverfish) par1EntityLiving,
				par2, par3);
	}

	@Override
	public void doRenderLiving(final EntityLiving par1EntityLiving,
			final double par2, final double par4, final double par6,
			final float par8, final float par9) {
		renderSilverfish((EntitySilverfish) par1EntityLiving, par2, par4, par6,
				par8, par9);
	}

	/**
	 * Actually renders the given argument. This is a synthetic bridge method,
	 * always casting down its argument and then handing it off to a worker
	 * function which does the actual work. In all probabilty, the class Render
	 * is generic (Render<T extends Entity) and this method has signature public
	 * void doRender(T entity, double d, double d1, double d2, float f, float
	 * f1). But JAD is pre 1.5 so doesn't do that.
	 */
	@Override
	public void doRender(final Entity par1Entity, final double par2,
			final double par4, final double par6, final float par8,
			final float par9) {
		renderSilverfish((EntitySilverfish) par1Entity, par2, par4, par6, par8,
				par9);
	}
}
