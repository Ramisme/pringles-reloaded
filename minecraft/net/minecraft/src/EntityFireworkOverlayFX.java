package net.minecraft.src;

public class EntityFireworkOverlayFX extends EntityFX {
	protected EntityFireworkOverlayFX(final World par1World, final double par2,
			final double par4, final double par6) {
		super(par1World, par2, par4, par6);
		particleMaxAge = 4;
	}

	@Override
	public void renderParticle(final Tessellator par1Tessellator,
			final float par2, final float par3, final float par4,
			final float par5, final float par6, final float par7) {
		final float var8 = 0.25F;
		final float var9 = var8 + 0.25F;
		final float var10 = 0.125F;
		final float var11 = var10 + 0.25F;
		final float var12 = 7.1F * MathHelper.sin((particleAge + par2 - 1.0F)
				* 0.25F * (float) Math.PI);
		particleAlpha = 0.6F - (particleAge + par2 - 1.0F) * 0.25F * 0.5F;
		final float var13 = (float) (prevPosX + (posX - prevPosX) * par2 - EntityFX.interpPosX);
		final float var14 = (float) (prevPosY + (posY - prevPosY) * par2 - EntityFX.interpPosY);
		final float var15 = (float) (prevPosZ + (posZ - prevPosZ) * par2 - EntityFX.interpPosZ);
		par1Tessellator.setColorRGBA_F(particleRed, particleGreen,
				particleBlue, particleAlpha);
		par1Tessellator.addVertexWithUV(var13 - par3 * var12 - par6 * var12,
				var14 - par4 * var12, var15 - par5 * var12 - par7 * var12,
				var9, var11);
		par1Tessellator.addVertexWithUV(var13 - par3 * var12 + par6 * var12,
				var14 + par4 * var12, var15 - par5 * var12 + par7 * var12,
				var9, var10);
		par1Tessellator.addVertexWithUV(var13 + par3 * var12 + par6 * var12,
				var14 + par4 * var12, var15 + par5 * var12 + par7 * var12,
				var8, var10);
		par1Tessellator.addVertexWithUV(var13 + par3 * var12 - par6 * var12,
				var14 - par4 * var12, var15 + par5 * var12 - par7 * var12,
				var8, var11);
	}
}
