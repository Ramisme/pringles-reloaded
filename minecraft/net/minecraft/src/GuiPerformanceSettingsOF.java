package net.minecraft.src;

public class GuiPerformanceSettingsOF extends GuiScreen {
	private final GuiScreen prevScreen;
	protected String title = "Performance Settings";
	private final GameSettings settings;
	private static EnumOptions[] enumOptions = new EnumOptions[] {
			EnumOptions.SMOOTH_FPS, EnumOptions.SMOOTH_WORLD,
			EnumOptions.LOAD_FAR, EnumOptions.PRELOADED_CHUNKS,
			EnumOptions.CHUNK_UPDATES, EnumOptions.CHUNK_UPDATES_DYNAMIC,
			EnumOptions.LAZY_CHUNK_LOADING };
	private int lastMouseX = 0;
	private int lastMouseY = 0;
	private long mouseStillTime = 0L;

	public GuiPerformanceSettingsOF(final GuiScreen var1,
			final GameSettings var2) {
		prevScreen = var1;
		settings = var2;
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui() {
		final StringTranslate var1 = StringTranslate.getInstance();
		int var2 = 0;
		final EnumOptions[] var3 = GuiPerformanceSettingsOF.enumOptions;
		final int var4 = var3.length;

		for (int var5 = 0; var5 < var4; ++var5) {
			final EnumOptions var6 = var3[var5];
			final int var7 = width / 2 - 155 + var2 % 2 * 160;
			final int var8 = height / 6 + 21 * (var2 / 2) - 10;

			if (!var6.getEnumFloat()) {
				buttonList.add(new GuiSmallButton(var6.returnEnumOrdinal(),
						var7, var8, var6, settings.getKeyBinding(var6)));
			} else {
				buttonList.add(new GuiSlider(var6.returnEnumOrdinal(), var7,
						var8, var6, settings.getKeyBinding(var6), settings
								.getOptionFloatValue(var6)));
			}

			++var2;
		}

		buttonList.add(new GuiButton(200, width / 2 - 100,
				height / 6 + 168 + 11, var1.translateKey("gui.done")));
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	@Override
	protected void actionPerformed(final GuiButton var1) {
		if (var1.enabled) {
			if (var1.id < 100 && var1 instanceof GuiSmallButton) {
				settings.setOptionValue(
						((GuiSmallButton) var1).returnEnumOptions(), 1);
				var1.displayString = settings.getKeyBinding(EnumOptions
						.getEnumOptions(var1.id));
			}

			if (var1.id == 200) {
				mc.gameSettings.saveOptions();
				mc.displayGuiScreen(prevScreen);
			}

			if (var1.id != EnumOptions.CLOUD_HEIGHT.ordinal()) {
				final ScaledResolution var2 = new ScaledResolution(
						mc.gameSettings, mc.displayWidth, mc.displayHeight);
				final int var3 = var2.getScaledWidth();
				final int var4 = var2.getScaledHeight();
				setWorldAndResolution(mc, var3, var4);
			}
		}
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(final int var1, final int var2, final float var3) {
		drawDefaultBackground();
		drawCenteredString(fontRenderer, title, width / 2, 20, 16777215);
		super.drawScreen(var1, var2, var3);

		if (Math.abs(var1 - lastMouseX) <= 5
				&& Math.abs(var2 - lastMouseY) <= 5) {
			final short var4 = 700;

			if (System.currentTimeMillis() >= mouseStillTime + var4) {
				final int var5 = width / 2 - 150;
				int var6 = height / 6 - 5;

				if (var2 <= var6 + 98) {
					var6 += 105;
				}

				final int var7 = var5 + 150 + 150;
				final int var8 = var6 + 84 + 10;
				final GuiButton var9 = getSelectedButton(var1, var2);

				if (var9 != null) {
					final String var10 = getButtonName(var9.displayString);
					final String[] var11 = getTooltipLines(var10);

					if (var11 == null) {
						return;
					}

					drawGradientRect(var5, var6, var7, var8, -536870912,
							-536870912);

					for (int var12 = 0; var12 < var11.length; ++var12) {
						final String var13 = var11[var12];
						fontRenderer.drawStringWithShadow(var13, var5 + 5, var6
								+ 5 + var12 * 11, 14540253);
					}
				}
			}
		} else {
			lastMouseX = var1;
			lastMouseY = var2;
			mouseStillTime = System.currentTimeMillis();
		}
	}

	private String[] getTooltipLines(final String var1) {
		return var1.equals("Smooth FPS") ? new String[] {
				"Stabilizes FPS by flushing the graphic driver buffers",
				"  OFF - no stabilization, FPS may fluctuate",
				"  ON - FPS stabilization",
				"This option is graphic driver dependant and its effect",
				"is not always visible" }
				: var1.equals("Smooth World") ? new String[] {
						"Removes lag spikes caused by the internal server.",
						"  OFF - no stabilization, FPS may fluctuate",
						"  ON - FPS stabilization",
						"Stabilizes FPS by distributing the internal server load.",
						"Effective only for local worlds and single-core CPU." }
						: var1.equals("Load Far") ? new String[] {
								"Loads the world chunks at distance Far.",
								"Switching the render distance does not cause all chunks ",
								"to be loaded again.",
								"  OFF - world chunks loaded up to render distance",
								"  ON - world chunks loaded at distance Far, allows",
								"       fast render distance switching" }
								: var1.equals("Preloaded Chunks") ? new String[] {
										"Defines an area in which no chunks will be loaded",
										"  OFF - after 5m new chunks will be loaded",
										"  2 - after 32m  new chunks will be loaded",
										"  8 - after 128m new chunks will be loaded",
										"Higher values need more time to load all the chunks" }
										: var1.equals("Chunk Updates") ? new String[] {
												"Chunk updates per frame",
												" 1 - (default) slower world loading, higher FPS",
												" 3 - faster world loading, lower FPS",
												" 5 - fastest world loading, lowest FPS" }
												: var1.equals("Dynamic Updates") ? new String[] {
														"Dynamic chunk updates",
														" OFF - (default) standard chunk updates per frame",
														" ON - more updates while the player is standing still",
														"Dynamic updates force more chunk updates while",
														"the player is standing still to load the world faster." }
														: var1.equals("Lazy Chunk Loading") ? new String[] {
																"Lazy Chunk Loading",
																" OFF - default server chunk loading",
																" ON - lazy server chunk loading (smoother)",
																"Smooths the integrated server chunk loading by",
																"distributing the chunks over several ticks.",
																"Turn it OFF if parts of the world do not load correctly.",
																"Effective only for local worlds and single-core CPU." }
																: null;
	}

	private String getButtonName(final String var1) {
		final int var2 = var1.indexOf(58);
		return var2 < 0 ? var1 : var1.substring(0, var2);
	}

	private GuiButton getSelectedButton(final int var1, final int var2) {
		for (int var3 = 0; var3 < buttonList.size(); ++var3) {
			final GuiButton var4 = (GuiButton) buttonList.get(var3);
			final boolean var5 = var1 >= var4.xPosition
					&& var2 >= var4.yPosition
					&& var1 < var4.xPosition + var4.width
					&& var2 < var4.yPosition + var4.height;

			if (var5) {
				return var4;
			}
		}

		return null;
	}
}
