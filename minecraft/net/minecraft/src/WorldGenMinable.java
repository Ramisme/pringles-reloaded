package net.minecraft.src;

import java.util.Random;

public class WorldGenMinable extends WorldGenerator {
	/** The block ID of the ore to be placed using this generator. */
	private final int minableBlockId;

	/** The number of blocks to generate. */
	private final int numberOfBlocks;
	private final int field_94523_c;

	public WorldGenMinable(final int par1, final int par2) {
		this(par1, par2, Block.stone.blockID);
	}

	public WorldGenMinable(final int par1, final int par2, final int par3) {
		minableBlockId = par1;
		numberOfBlocks = par2;
		field_94523_c = par3;
	}

	@Override
	public boolean generate(final World par1World, final Random par2Random,
			final int par3, final int par4, final int par5) {
		final float var6 = par2Random.nextFloat() * (float) Math.PI;
		final double var7 = par3 + 8 + MathHelper.sin(var6) * numberOfBlocks
				/ 8.0F;
		final double var9 = par3 + 8 - MathHelper.sin(var6) * numberOfBlocks
				/ 8.0F;
		final double var11 = par5 + 8 + MathHelper.cos(var6) * numberOfBlocks
				/ 8.0F;
		final double var13 = par5 + 8 - MathHelper.cos(var6) * numberOfBlocks
				/ 8.0F;
		final double var15 = par4 + par2Random.nextInt(3) - 2;
		final double var17 = par4 + par2Random.nextInt(3) - 2;

		for (int var19 = 0; var19 <= numberOfBlocks; ++var19) {
			final double var20 = var7 + (var9 - var7) * var19 / numberOfBlocks;
			final double var22 = var15 + (var17 - var15) * var19
					/ numberOfBlocks;
			final double var24 = var11 + (var13 - var11) * var19
					/ numberOfBlocks;
			final double var26 = par2Random.nextDouble() * numberOfBlocks
					/ 16.0D;
			final double var28 = (MathHelper.sin(var19 * (float) Math.PI
					/ numberOfBlocks) + 1.0F)
					* var26 + 1.0D;
			final double var30 = (MathHelper.sin(var19 * (float) Math.PI
					/ numberOfBlocks) + 1.0F)
					* var26 + 1.0D;
			final int var32 = MathHelper.floor_double(var20 - var28 / 2.0D);
			final int var33 = MathHelper.floor_double(var22 - var30 / 2.0D);
			final int var34 = MathHelper.floor_double(var24 - var28 / 2.0D);
			final int var35 = MathHelper.floor_double(var20 + var28 / 2.0D);
			final int var36 = MathHelper.floor_double(var22 + var30 / 2.0D);
			final int var37 = MathHelper.floor_double(var24 + var28 / 2.0D);

			for (int var38 = var32; var38 <= var35; ++var38) {
				final double var39 = (var38 + 0.5D - var20) / (var28 / 2.0D);

				if (var39 * var39 < 1.0D) {
					for (int var41 = var33; var41 <= var36; ++var41) {
						final double var42 = (var41 + 0.5D - var22)
								/ (var30 / 2.0D);

						if (var39 * var39 + var42 * var42 < 1.0D) {
							for (int var44 = var34; var44 <= var37; ++var44) {
								final double var45 = (var44 + 0.5D - var24)
										/ (var28 / 2.0D);

								if (var39 * var39 + var42 * var42 + var45
										* var45 < 1.0D
										&& par1World.getBlockId(var38, var41,
												var44) == field_94523_c) {
									par1World.setBlock(var38, var41, var44,
											minableBlockId, 0, 2);
								}
							}
						}
					}
				}
			}
		}

		return true;
	}
}
