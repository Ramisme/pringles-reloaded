package net.minecraft.src;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class NBTTagFloat extends NBTBase {
	/** The float value for the tag. */
	public float data;

	public NBTTagFloat(final String par1Str) {
		super(par1Str);
	}

	public NBTTagFloat(final String par1Str, final float par2) {
		super(par1Str);
		data = par2;
	}

	/**
	 * Write the actual data contents of the tag, implemented in NBT extension
	 * classes
	 */
	@Override
	void write(final DataOutput par1DataOutput) throws IOException {
		par1DataOutput.writeFloat(data);
	}

	/**
	 * Read the actual data contents of the tag, implemented in NBT extension
	 * classes
	 */
	@Override
	void load(final DataInput par1DataInput) throws IOException {
		data = par1DataInput.readFloat();
	}

	/**
	 * Gets the type byte for the tag.
	 */
	@Override
	public byte getId() {
		return (byte) 5;
	}

	@Override
	public String toString() {
		return "" + data;
	}

	/**
	 * Creates a clone of the tag.
	 */
	@Override
	public NBTBase copy() {
		return new NBTTagFloat(getName(), data);
	}

	@Override
	public boolean equals(final Object par1Obj) {
		if (super.equals(par1Obj)) {
			final NBTTagFloat var2 = (NBTTagFloat) par1Obj;
			return data == var2.data;
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return super.hashCode() ^ Float.floatToIntBits(data);
	}
}
