package net.minecraft.src;

import java.util.concurrent.Callable;

class CallableLevelSpawnLocation implements Callable {
	final WorldInfo worldInfoInstance;

	CallableLevelSpawnLocation(final WorldInfo par1WorldInfo) {
		worldInfoInstance = par1WorldInfo;
	}

	public String callLevelSpawnLocation() {
		return CrashReportCategory.getLocationInfo(
				WorldInfo.getSpawnXCoordinate(worldInfoInstance),
				WorldInfo.getSpawnYCoordinate(worldInfoInstance),
				WorldInfo.getSpawnZCoordinate(worldInfoInstance));
	}

	@Override
	public Object call() {
		return callLevelSpawnLocation();
	}
}
