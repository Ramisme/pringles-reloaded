package net.minecraft.src;

public class ModelSlime extends ModelBase {
	/** The slime's bodies, both the inside box and the outside box */
	ModelRenderer slimeBodies;

	/** The slime's right eye */
	ModelRenderer slimeRightEye;

	/** The slime's left eye */
	ModelRenderer slimeLeftEye;

	/** The slime's mouth */
	ModelRenderer slimeMouth;

	public ModelSlime(final int par1) {
		slimeBodies = new ModelRenderer(this, 0, par1);
		slimeBodies.addBox(-4.0F, 16.0F, -4.0F, 8, 8, 8);

		if (par1 > 0) {
			slimeBodies = new ModelRenderer(this, 0, par1);
			slimeBodies.addBox(-3.0F, 17.0F, -3.0F, 6, 6, 6);
			slimeRightEye = new ModelRenderer(this, 32, 0);
			slimeRightEye.addBox(-3.25F, 18.0F, -3.5F, 2, 2, 2);
			slimeLeftEye = new ModelRenderer(this, 32, 4);
			slimeLeftEye.addBox(1.25F, 18.0F, -3.5F, 2, 2, 2);
			slimeMouth = new ModelRenderer(this, 32, 8);
			slimeMouth.addBox(0.0F, 21.0F, -3.5F, 1, 1, 1);
		}
	}

	/**
	 * Sets the models various rotation angles then renders the model.
	 */
	@Override
	public void render(final Entity par1Entity, final float par2,
			final float par3, final float par4, final float par5,
			final float par6, final float par7) {
		setRotationAngles(par2, par3, par4, par5, par6, par7, par1Entity);
		slimeBodies.render(par7);

		if (slimeRightEye != null) {
			slimeRightEye.render(par7);
			slimeLeftEye.render(par7);
			slimeMouth.render(par7);
		}
	}
}
