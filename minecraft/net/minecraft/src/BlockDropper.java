package net.minecraft.src;

public class BlockDropper extends BlockDispenser {
	private final IBehaviorDispenseItem dropperDefaultBehaviour = new BehaviorDefaultDispenseItem();

	protected BlockDropper(final int par1) {
		super(par1);
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		blockIcon = par1IconRegister.registerIcon("furnace_side");
		furnaceTopIcon = par1IconRegister.registerIcon("furnace_top");
		furnaceFrontIcon = par1IconRegister.registerIcon("dropper_front");
		field_96473_e = par1IconRegister.registerIcon("dropper_front_vertical");
	}

	/**
	 * Returns the behavior for the given ItemStack.
	 */
	@Override
	protected IBehaviorDispenseItem getBehaviorForItemStack(
			final ItemStack par1ItemStack) {
		return dropperDefaultBehaviour;
	}

	/**
	 * Returns a new instance of a block's tile entity class. Called on placing
	 * the block.
	 */
	@Override
	public TileEntity createNewTileEntity(final World par1World) {
		return new TileEntityDropper();
	}

	@Override
	protected void dispense(final World par1World, final int par2,
			final int par3, final int par4) {
		final BlockSourceImpl var5 = new BlockSourceImpl(par1World, par2, par3,
				par4);
		final TileEntityDispenser var6 = (TileEntityDispenser) var5
				.getBlockTileEntity();

		if (var6 != null) {
			final int var7 = var6.getRandomStackFromInventory();

			if (var7 < 0) {
				par1World.playAuxSFX(1001, par2, par3, par4, 0);
			} else {
				final ItemStack var8 = var6.getStackInSlot(var7);
				final int var9 = par1World.getBlockMetadata(par2, par3, par4) & 7;
				final IInventory var10 = TileEntityHopper
						.getInventoryAtLocation(par1World, par2
								+ Facing.offsetsXForSide[var9], par3
								+ Facing.offsetsYForSide[var9], par4
								+ Facing.offsetsZForSide[var9]);
				ItemStack var11;

				if (var10 != null) {
					var11 = TileEntityHopper.insertStack(var10, var8.copy()
							.splitStack(1), Facing.oppositeSide[var9]);

					if (var11 == null) {
						var11 = var8.copy();

						if (--var11.stackSize == 0) {
							var11 = null;
						}
					} else {
						var11 = var8.copy();
					}
				} else {
					var11 = dropperDefaultBehaviour.dispense(var5, var8);

					if (var11 != null && var11.stackSize == 0) {
						var11 = null;
					}
				}

				var6.setInventorySlotContents(var7, var11);
			}
		}
	}
}
