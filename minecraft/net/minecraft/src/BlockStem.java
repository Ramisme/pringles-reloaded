package net.minecraft.src;

import java.util.Random;

public class BlockStem extends BlockFlower {
	/** Defines if it is a Melon or a Pumpkin that the stem is producing. */
	private final Block fruitType;
	private Icon theIcon;

	protected BlockStem(final int par1, final Block par2Block) {
		super(par1);
		fruitType = par2Block;
		setTickRandomly(true);
		final float var3 = 0.125F;
		setBlockBounds(0.5F - var3, 0.0F, 0.5F - var3, 0.5F + var3, 0.25F,
				0.5F + var3);
		setCreativeTab((CreativeTabs) null);
	}

	/**
	 * Gets passed in the blockID of the block below and supposed to return true
	 * if its allowed to grow on the type of blockID passed in. Args: blockID
	 */
	@Override
	protected boolean canThisPlantGrowOnThisBlockID(final int par1) {
		return par1 == Block.tilledField.blockID;
	}

	/**
	 * Ticks the block if it's been scheduled
	 */
	@Override
	public void updateTick(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		super.updateTick(par1World, par2, par3, par4, par5Random);

		if (par1World.getBlockLightValue(par2, par3 + 1, par4) >= 9) {
			final float var6 = getGrowthModifier(par1World, par2, par3, par4);

			if (par5Random.nextInt((int) (25.0F / var6) + 1) == 0) {
				int var7 = par1World.getBlockMetadata(par2, par3, par4);

				if (var7 < 7) {
					++var7;
					par1World.setBlockMetadataWithNotify(par2, par3, par4,
							var7, 2);
				} else {
					if (par1World.getBlockId(par2 - 1, par3, par4) == fruitType.blockID) {
						return;
					}

					if (par1World.getBlockId(par2 + 1, par3, par4) == fruitType.blockID) {
						return;
					}

					if (par1World.getBlockId(par2, par3, par4 - 1) == fruitType.blockID) {
						return;
					}

					if (par1World.getBlockId(par2, par3, par4 + 1) == fruitType.blockID) {
						return;
					}

					final int var8 = par5Random.nextInt(4);
					int var9 = par2;
					int var10 = par4;

					if (var8 == 0) {
						var9 = par2 - 1;
					}

					if (var8 == 1) {
						++var9;
					}

					if (var8 == 2) {
						var10 = par4 - 1;
					}

					if (var8 == 3) {
						++var10;
					}

					final int var11 = par1World.getBlockId(var9, par3 - 1,
							var10);

					if (par1World.getBlockId(var9, par3, var10) == 0
							&& (var11 == Block.tilledField.blockID
									|| var11 == Block.dirt.blockID || var11 == Block.grass.blockID)) {
						par1World
								.setBlock(var9, par3, var10, fruitType.blockID);
					}
				}
			}
		}
	}

	public void fertilizeStem(final World par1World, final int par2,
			final int par3, final int par4) {
		int var5 = par1World.getBlockMetadata(par2, par3, par4)
				+ MathHelper.getRandomIntegerInRange(par1World.rand, 2, 5);

		if (var5 > 7) {
			var5 = 7;
		}

		par1World.setBlockMetadataWithNotify(par2, par3, par4, var5, 2);
	}

	private float getGrowthModifier(final World par1World, final int par2,
			final int par3, final int par4) {
		float var5 = 1.0F;
		final int var6 = par1World.getBlockId(par2, par3, par4 - 1);
		final int var7 = par1World.getBlockId(par2, par3, par4 + 1);
		final int var8 = par1World.getBlockId(par2 - 1, par3, par4);
		final int var9 = par1World.getBlockId(par2 + 1, par3, par4);
		final int var10 = par1World.getBlockId(par2 - 1, par3, par4 - 1);
		final int var11 = par1World.getBlockId(par2 + 1, par3, par4 - 1);
		final int var12 = par1World.getBlockId(par2 + 1, par3, par4 + 1);
		final int var13 = par1World.getBlockId(par2 - 1, par3, par4 + 1);
		final boolean var14 = var8 == blockID || var9 == blockID;
		final boolean var15 = var6 == blockID || var7 == blockID;
		final boolean var16 = var10 == blockID || var11 == blockID
				|| var12 == blockID || var13 == blockID;

		for (int var17 = par2 - 1; var17 <= par2 + 1; ++var17) {
			for (int var18 = par4 - 1; var18 <= par4 + 1; ++var18) {
				final int var19 = par1World.getBlockId(var17, par3 - 1, var18);
				float var20 = 0.0F;

				if (var19 == Block.tilledField.blockID) {
					var20 = 1.0F;

					if (par1World.getBlockMetadata(var17, par3 - 1, var18) > 0) {
						var20 = 3.0F;
					}
				}

				if (var17 != par2 || var18 != par4) {
					var20 /= 4.0F;
				}

				var5 += var20;
			}
		}

		if (var16 || var14 && var15) {
			var5 /= 2.0F;
		}

		return var5;
	}

	/**
	 * Returns the color this block should be rendered. Used by leaves.
	 */
	@Override
	public int getRenderColor(final int par1) {
		final int var2 = par1 * 32;
		final int var3 = 255 - par1 * 8;
		final int var4 = par1 * 4;
		return var2 << 16 | var3 << 8 | var4;
	}

	/**
	 * Returns a integer with hex for 0xrrggbb with this color multiplied
	 * against the blocks color. Note only called when first determining what to
	 * render.
	 */
	@Override
	public int colorMultiplier(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		return getRenderColor(par1IBlockAccess.getBlockMetadata(par2, par3,
				par4));
	}

	/**
	 * Sets the block's bounds for rendering it as an item
	 */
	@Override
	public void setBlockBoundsForItemRender() {
		final float var1 = 0.125F;
		setBlockBounds(0.5F - var1, 0.0F, 0.5F - var1, 0.5F + var1, 0.25F,
				0.5F + var1);
	}

	/**
	 * Updates the blocks bounds based on its current state. Args: world, x, y,
	 * z
	 */
	@Override
	public void setBlockBoundsBasedOnState(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		maxY = (par1IBlockAccess.getBlockMetadata(par2, par3, par4) * 2 + 2) / 16.0F;
		final float var5 = 0.125F;
		setBlockBounds(0.5F - var5, 0.0F, 0.5F - var5, 0.5F + var5,
				(float) maxY, 0.5F + var5);
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return 19;
	}

	/**
	 * Returns the current state of the stem. Returns -1 if the stem is not
	 * fully grown, or a value between 0 and 3 based on the direction the stem
	 * is facing.
	 */
	public int getState(final IBlockAccess par1IBlockAccess, final int par2,
			final int par3, final int par4) {
		final int var5 = par1IBlockAccess.getBlockMetadata(par2, par3, par4);
		return var5 < 7 ? -1
				: par1IBlockAccess.getBlockId(par2 - 1, par3, par4) == fruitType.blockID ? 0
						: par1IBlockAccess.getBlockId(par2 + 1, par3, par4) == fruitType.blockID ? 1
								: par1IBlockAccess.getBlockId(par2, par3,
										par4 - 1) == fruitType.blockID ? 2
										: par1IBlockAccess.getBlockId(par2,
												par3, par4 + 1) == fruitType.blockID ? 3
												: -1;
	}

	/**
	 * Drops the block items with a specified chance of dropping the specified
	 * items
	 */
	@Override
	public void dropBlockAsItemWithChance(final World par1World,
			final int par2, final int par3, final int par4, final int par5,
			final float par6, final int par7) {
		super.dropBlockAsItemWithChance(par1World, par2, par3, par4, par5,
				par6, par7);

		if (!par1World.isRemote) {
			Item var8 = null;

			if (fruitType == Block.pumpkin) {
				var8 = Item.pumpkinSeeds;
			}

			if (fruitType == Block.melon) {
				var8 = Item.melonSeeds;
			}

			for (int var9 = 0; var9 < 3; ++var9) {
				if (par1World.rand.nextInt(15) <= par5) {
					dropBlockAsItem_do(par1World, par2, par3, par4,
							new ItemStack(var8));
				}
			}
		}
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return -1;
	}

	/**
	 * Returns the quantity of items to drop on block destruction.
	 */
	@Override
	public int quantityDropped(final Random par1Random) {
		return 1;
	}

	/**
	 * only called by clickMiddleMouseButton , and passed to
	 * inventory.setCurrentItem (along with isCreative)
	 */
	@Override
	public int idPicked(final World par1World, final int par2, final int par3,
			final int par4) {
		return fruitType == Block.pumpkin ? Item.pumpkinSeeds.itemID
				: fruitType == Block.melon ? Item.melonSeeds.itemID : 0;
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		blockIcon = par1IconRegister.registerIcon("stem_straight");
		theIcon = par1IconRegister.registerIcon("stem_bent");
	}

	public Icon func_94368_p() {
		return theIcon;
	}
}
