package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet2ClientProtocol extends Packet {
	private int protocolVersion;
	private String username;
	private String serverHost;
	private int serverPort;

	public Packet2ClientProtocol() {
	}

	public Packet2ClientProtocol(final int par1, final String par2Str,
			final String par3Str, final int par4) {
		protocolVersion = par1;
		username = par2Str;
		serverHost = par3Str;
		serverPort = par4;
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		protocolVersion = par1DataInputStream.readByte();
		username = Packet.readString(par1DataInputStream, 16);
		serverHost = Packet.readString(par1DataInputStream, 255);
		serverPort = par1DataInputStream.readInt();
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		par1DataOutputStream.writeByte(protocolVersion);
		Packet.writeString(username, par1DataOutputStream);
		Packet.writeString(serverHost, par1DataOutputStream);
		par1DataOutputStream.writeInt(serverPort);
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleClientProtocol(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 3 + 2 * username.length();
	}

	/**
	 * Returns the protocol version.
	 */
	public int getProtocolVersion() {
		return protocolVersion;
	}

	/**
	 * Returns the username.
	 */
	public String getUsername() {
		return username;
	}
}
