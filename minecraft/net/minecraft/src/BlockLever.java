package net.minecraft.src;

public class BlockLever extends Block {
	protected BlockLever(final int par1) {
		super(par1, Material.circuits);
		setCreativeTab(CreativeTabs.tabRedstone);
	}

	/**
	 * Returns a bounding box from the pool of bounding boxes (this means this
	 * box can change after the pool has been cleared to be reused)
	 */
	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(final World par1World,
			final int par2, final int par3, final int par4) {
		return null;
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return 12;
	}

	/**
	 * checks to see if you can place this block can be placed on that side of a
	 * block: BlockLever overrides
	 */
	@Override
	public boolean canPlaceBlockOnSide(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		return par5 == 0 && par1World.isBlockNormalCube(par2, par3 + 1, par4) ? true
				: par5 == 1
						&& par1World.doesBlockHaveSolidTopSurface(par2,
								par3 - 1, par4) ? true
						: par5 == 2
								&& par1World.isBlockNormalCube(par2, par3,
										par4 + 1) ? true : par5 == 3
								&& par1World.isBlockNormalCube(par2, par3,
										par4 - 1) ? true : par5 == 4
								&& par1World.isBlockNormalCube(par2 + 1, par3,
										par4) ? true : par5 == 5
								&& par1World.isBlockNormalCube(par2 - 1, par3,
										par4);
	}

	/**
	 * Checks to see if its valid to put this block at the specified
	 * coordinates. Args: world, x, y, z
	 */
	@Override
	public boolean canPlaceBlockAt(final World par1World, final int par2,
			final int par3, final int par4) {
		return par1World.isBlockNormalCube(par2 - 1, par3, par4) ? true
				: par1World.isBlockNormalCube(par2 + 1, par3, par4) ? true
						: par1World.isBlockNormalCube(par2, par3, par4 - 1) ? true
								: par1World.isBlockNormalCube(par2, par3,
										par4 + 1) ? true : par1World
										.doesBlockHaveSolidTopSurface(par2,
												par3 - 1, par4) ? true
										: par1World.isBlockNormalCube(par2,
												par3 + 1, par4);
	}

	/**
	 * Called when a block is placed using its ItemBlock. Args: World, X, Y, Z,
	 * side, hitX, hitY, hitZ, block metadata
	 */
	@Override
	public int onBlockPlaced(final World par1World, final int par2,
			final int par3, final int par4, final int par5, final float par6,
			final float par7, final float par8, final int par9) {
		final int var11 = par9 & 8;
		byte var12 = -1;

		if (par5 == 0 && par1World.isBlockNormalCube(par2, par3 + 1, par4)) {
			var12 = 0;
		}

		if (par5 == 1
				&& par1World.doesBlockHaveSolidTopSurface(par2, par3 - 1, par4)) {
			var12 = 5;
		}

		if (par5 == 2 && par1World.isBlockNormalCube(par2, par3, par4 + 1)) {
			var12 = 4;
		}

		if (par5 == 3 && par1World.isBlockNormalCube(par2, par3, par4 - 1)) {
			var12 = 3;
		}

		if (par5 == 4 && par1World.isBlockNormalCube(par2 + 1, par3, par4)) {
			var12 = 2;
		}

		if (par5 == 5 && par1World.isBlockNormalCube(par2 - 1, par3, par4)) {
			var12 = 1;
		}

		return var12 + var11;
	}

	/**
	 * Called when the block is placed in the world.
	 */
	@Override
	public void onBlockPlacedBy(final World par1World, final int par2,
			final int par3, final int par4,
			final EntityLiving par5EntityLiving, final ItemStack par6ItemStack) {
		final int var7 = par1World.getBlockMetadata(par2, par3, par4);
		final int var8 = var7 & 7;
		final int var9 = var7 & 8;

		if (var8 == BlockLever.invertMetadata(1)) {
			if ((MathHelper
					.floor_double(par5EntityLiving.rotationYaw * 4.0F / 360.0F + 0.5D) & 1) == 0) {
				par1World.setBlockMetadataWithNotify(par2, par3, par4,
						5 | var9, 2);
			} else {
				par1World.setBlockMetadataWithNotify(par2, par3, par4,
						6 | var9, 2);
			}
		} else if (var8 == BlockLever.invertMetadata(0)) {
			if ((MathHelper
					.floor_double(par5EntityLiving.rotationYaw * 4.0F / 360.0F + 0.5D) & 1) == 0) {
				par1World.setBlockMetadataWithNotify(par2, par3, par4,
						7 | var9, 2);
			} else {
				par1World.setBlockMetadataWithNotify(par2, par3, par4,
						0 | var9, 2);
			}
		}
	}

	/**
	 * only used in ComponentScatteredFeatureJunglePyramid.addComponentParts"
	 */
	public static int invertMetadata(final int par0) {
		switch (par0) {
		case 0:
			return 0;

		case 1:
			return 5;

		case 2:
			return 4;

		case 3:
			return 3;

		case 4:
			return 2;

		case 5:
			return 1;

		default:
			return -1;
		}
	}

	/**
	 * Lets the block know when one of its neighbor changes. Doesn't know which
	 * neighbor changed (coordinates passed are their own) Args: x, y, z,
	 * neighbor blockID
	 */
	@Override
	public void onNeighborBlockChange(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		if (checkIfAttachedToBlock(par1World, par2, par3, par4)) {
			final int var6 = par1World.getBlockMetadata(par2, par3, par4) & 7;
			boolean var7 = false;

			if (!par1World.isBlockNormalCube(par2 - 1, par3, par4) && var6 == 1) {
				var7 = true;
			}

			if (!par1World.isBlockNormalCube(par2 + 1, par3, par4) && var6 == 2) {
				var7 = true;
			}

			if (!par1World.isBlockNormalCube(par2, par3, par4 - 1) && var6 == 3) {
				var7 = true;
			}

			if (!par1World.isBlockNormalCube(par2, par3, par4 + 1) && var6 == 4) {
				var7 = true;
			}

			if (!par1World.doesBlockHaveSolidTopSurface(par2, par3 - 1, par4)
					&& var6 == 5) {
				var7 = true;
			}

			if (!par1World.doesBlockHaveSolidTopSurface(par2, par3 - 1, par4)
					&& var6 == 6) {
				var7 = true;
			}

			if (!par1World.isBlockNormalCube(par2, par3 + 1, par4) && var6 == 0) {
				var7 = true;
			}

			if (!par1World.isBlockNormalCube(par2, par3 + 1, par4) && var6 == 7) {
				var7 = true;
			}

			if (var7) {
				dropBlockAsItem(par1World, par2, par3, par4,
						par1World.getBlockMetadata(par2, par3, par4), 0);
				par1World.setBlockToAir(par2, par3, par4);
			}
		}
	}

	/**
	 * Checks if the block is attached to another block. If it is not, it
	 * returns false and drops the block as an item. If it is it returns true.
	 */
	private boolean checkIfAttachedToBlock(final World par1World,
			final int par2, final int par3, final int par4) {
		if (!canPlaceBlockAt(par1World, par2, par3, par4)) {
			dropBlockAsItem(par1World, par2, par3, par4,
					par1World.getBlockMetadata(par2, par3, par4), 0);
			par1World.setBlockToAir(par2, par3, par4);
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Updates the blocks bounds based on its current state. Args: world, x, y,
	 * z
	 */
	@Override
	public void setBlockBoundsBasedOnState(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		final int var5 = par1IBlockAccess.getBlockMetadata(par2, par3, par4) & 7;
		float var6 = 0.1875F;

		if (var5 == 1) {
			setBlockBounds(0.0F, 0.2F, 0.5F - var6, var6 * 2.0F, 0.8F,
					0.5F + var6);
		} else if (var5 == 2) {
			setBlockBounds(1.0F - var6 * 2.0F, 0.2F, 0.5F - var6, 1.0F, 0.8F,
					0.5F + var6);
		} else if (var5 == 3) {
			setBlockBounds(0.5F - var6, 0.2F, 0.0F, 0.5F + var6, 0.8F,
					var6 * 2.0F);
		} else if (var5 == 4) {
			setBlockBounds(0.5F - var6, 0.2F, 1.0F - var6 * 2.0F, 0.5F + var6,
					0.8F, 1.0F);
		} else if (var5 != 5 && var5 != 6) {
			if (var5 == 0 || var5 == 7) {
				var6 = 0.25F;
				setBlockBounds(0.5F - var6, 0.4F, 0.5F - var6, 0.5F + var6,
						1.0F, 0.5F + var6);
			}
		} else {
			var6 = 0.25F;
			setBlockBounds(0.5F - var6, 0.0F, 0.5F - var6, 0.5F + var6, 0.6F,
					0.5F + var6);
		}
	}

	/**
	 * Called upon block activation (right click on the block.)
	 */
	@Override
	public boolean onBlockActivated(final World par1World, final int par2,
			final int par3, final int par4,
			final EntityPlayer par5EntityPlayer, final int par6,
			final float par7, final float par8, final float par9) {
		if (par1World.isRemote) {
			return true;
		} else {
			final int var10 = par1World.getBlockMetadata(par2, par3, par4);
			final int var11 = var10 & 7;
			final int var12 = 8 - (var10 & 8);
			par1World.setBlockMetadataWithNotify(par2, par3, par4, var11
					+ var12, 3);
			par1World.playSoundEffect(par2 + 0.5D, par3 + 0.5D, par4 + 0.5D,
					"random.click", 0.3F, var12 > 0 ? 0.6F : 0.5F);
			par1World.notifyBlocksOfNeighborChange(par2, par3, par4, blockID);

			if (var11 == 1) {
				par1World.notifyBlocksOfNeighborChange(par2 - 1, par3, par4,
						blockID);
			} else if (var11 == 2) {
				par1World.notifyBlocksOfNeighborChange(par2 + 1, par3, par4,
						blockID);
			} else if (var11 == 3) {
				par1World.notifyBlocksOfNeighborChange(par2, par3, par4 - 1,
						blockID);
			} else if (var11 == 4) {
				par1World.notifyBlocksOfNeighborChange(par2, par3, par4 + 1,
						blockID);
			} else if (var11 != 5 && var11 != 6) {
				if (var11 == 0 || var11 == 7) {
					par1World.notifyBlocksOfNeighborChange(par2, par3 + 1,
							par4, blockID);
				}
			} else {
				par1World.notifyBlocksOfNeighborChange(par2, par3 - 1, par4,
						blockID);
			}

			return true;
		}
	}

	/**
	 * ejects contained items into the world, and notifies neighbours of an
	 * update, as appropriate
	 */
	@Override
	public void breakBlock(final World par1World, final int par2,
			final int par3, final int par4, final int par5, final int par6) {
		if ((par6 & 8) > 0) {
			par1World.notifyBlocksOfNeighborChange(par2, par3, par4, blockID);
			final int var7 = par6 & 7;

			if (var7 == 1) {
				par1World.notifyBlocksOfNeighborChange(par2 - 1, par3, par4,
						blockID);
			} else if (var7 == 2) {
				par1World.notifyBlocksOfNeighborChange(par2 + 1, par3, par4,
						blockID);
			} else if (var7 == 3) {
				par1World.notifyBlocksOfNeighborChange(par2, par3, par4 - 1,
						blockID);
			} else if (var7 == 4) {
				par1World.notifyBlocksOfNeighborChange(par2, par3, par4 + 1,
						blockID);
			} else if (var7 != 5 && var7 != 6) {
				if (var7 == 0 || var7 == 7) {
					par1World.notifyBlocksOfNeighborChange(par2, par3 + 1,
							par4, blockID);
				}
			} else {
				par1World.notifyBlocksOfNeighborChange(par2, par3 - 1, par4,
						blockID);
			}
		}

		super.breakBlock(par1World, par2, par3, par4, par5, par6);
	}

	/**
	 * Returns true if the block is emitting indirect/weak redstone power on the
	 * specified side. If isBlockNormalCube returns true, standard redstone
	 * propagation rules will apply instead and this will not be called. Args:
	 * World, X, Y, Z, side. Note that the side is reversed - eg it is 1 (up)
	 * when checking the bottom of the block.
	 */
	@Override
	public int isProvidingWeakPower(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		return (par1IBlockAccess.getBlockMetadata(par2, par3, par4) & 8) > 0 ? 15
				: 0;
	}

	/**
	 * Returns true if the block is emitting direct/strong redstone power on the
	 * specified side. Args: World, X, Y, Z, side. Note that the side is
	 * reversed - eg it is 1 (up) when checking the bottom of the block.
	 */
	@Override
	public int isProvidingStrongPower(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		final int var6 = par1IBlockAccess.getBlockMetadata(par2, par3, par4);

		if ((var6 & 8) == 0) {
			return 0;
		} else {
			final int var7 = var6 & 7;
			return var7 == 0 && par5 == 0 ? 15 : var7 == 7 && par5 == 0 ? 15
					: var7 == 6 && par5 == 1 ? 15 : var7 == 5 && par5 == 1 ? 15
							: var7 == 4 && par5 == 2 ? 15 : var7 == 3
									&& par5 == 3 ? 15
									: var7 == 2 && par5 == 4 ? 15 : var7 == 1
											&& par5 == 5 ? 15 : 0;
		}
	}

	/**
	 * Can this block provide power. Only wire currently seems to have this
	 * change based on its state.
	 */
	@Override
	public boolean canProvidePower() {
		return true;
	}
}
