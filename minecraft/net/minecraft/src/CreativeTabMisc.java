package net.minecraft.src;

import java.util.List;

final class CreativeTabMisc extends CreativeTabs {
	CreativeTabMisc(final int par1, final String par2Str) {
		super(par1, par2Str);
	}

	/**
	 * the itemID for the item to be displayed on the tab
	 */
	@Override
	public int getTabIconItemIndex() {
		return Item.bucketLava.itemID;
	}

	/**
	 * only shows items which have tabToDisplayOn == this
	 */
	@Override
	public void displayAllReleventItems(final List par1List) {
		super.displayAllReleventItems(par1List);
		func_92116_a(par1List,
				new EnumEnchantmentType[] { EnumEnchantmentType.all });
	}
}
