package net.minecraft.src;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.lwjgl.opengl.GL11;

public class TileEntityRenderer {
	/**
	 * A mapping of TileEntitySpecialRenderers used for each TileEntity that has
	 * one
	 */
	private final Map specialRendererMap = new HashMap();

	/** The static instance of TileEntityRenderer */
	public static TileEntityRenderer instance = new TileEntityRenderer();

	/** The FontRenderer instance used by the TileEntityRenderer */
	private FontRenderer fontRenderer;

	/** The player's current X position (same as playerX) */
	public static double staticPlayerX;

	/** The player's current Y position (same as playerY) */
	public static double staticPlayerY;

	/** The player's current Z position (same as playerZ) */
	public static double staticPlayerZ;

	/** The RenderEngine instance used by the TileEntityRenderer */
	public RenderEngine renderEngine;

	/** Reference to the World object. */
	public World worldObj;
	public EntityLiving entityLivingPlayer;
	public float playerYaw;
	public float playerPitch;

	/** The player's X position in this rendering context */
	public double playerX;

	/** The player's Y position in this rendering context */
	public double playerY;

	/** The player's Z position in this rendering context */
	public double playerZ;

	private TileEntityRenderer() {
		specialRendererMap.put(TileEntitySign.class,
				new TileEntitySignRenderer());
		specialRendererMap.put(TileEntityMobSpawner.class,
				new TileEntityMobSpawnerRenderer());
		specialRendererMap.put(TileEntityPiston.class,
				new TileEntityRendererPiston());
		specialRendererMap.put(TileEntityChest.class,
				new TileEntityChestRenderer());
		specialRendererMap.put(TileEntityEnderChest.class,
				new TileEntityEnderChestRenderer());
		specialRendererMap.put(TileEntityEnchantmentTable.class,
				new RenderEnchantmentTable());
		specialRendererMap
				.put(TileEntityEndPortal.class, new RenderEndPortal());
		specialRendererMap.put(TileEntityBeacon.class,
				new TileEntityBeaconRenderer());
		specialRendererMap.put(TileEntitySkull.class,
				new TileEntitySkullRenderer());
		final Iterator var1 = specialRendererMap.values().iterator();

		while (var1.hasNext()) {
			final TileEntitySpecialRenderer var2 = (TileEntitySpecialRenderer) var1
					.next();
			var2.setTileEntityRenderer(this);
		}
	}

	/**
	 * Returns the TileEntitySpecialRenderer used to render this TileEntity
	 * class, or null if it has no special renderer
	 */
	public TileEntitySpecialRenderer getSpecialRendererForClass(
			final Class par1Class) {
		TileEntitySpecialRenderer var2 = (TileEntitySpecialRenderer) specialRendererMap
				.get(par1Class);

		if (var2 == null && par1Class != TileEntity.class) {
			var2 = getSpecialRendererForClass(par1Class.getSuperclass());
			specialRendererMap.put(par1Class, var2);
		}

		return var2;
	}

	/**
	 * Returns true if this TileEntity instance has a TileEntitySpecialRenderer
	 * associated with it, false otherwise.
	 */
	public boolean hasSpecialRenderer(final TileEntity par1TileEntity) {
		return getSpecialRendererForEntity(par1TileEntity) != null;
	}

	/**
	 * Returns the TileEntitySpecialRenderer used to render this TileEntity
	 * instance, or null if it has no special renderer
	 */
	public TileEntitySpecialRenderer getSpecialRendererForEntity(
			final TileEntity par1TileEntity) {
		return par1TileEntity == null ? null
				: getSpecialRendererForClass(par1TileEntity.getClass());
	}

	/**
	 * Caches several render-related references, including the active World,
	 * RenderEngine, FontRenderer, and the camera- bound EntityLiving's
	 * interpolated pitch, yaw and position. Args: world, renderengine,
	 * fontrenderer, entityliving, partialTickTime
	 */
	public void cacheActiveRenderInfo(final World par1World,
			final RenderEngine par2RenderEngine,
			final FontRenderer par3FontRenderer,
			final EntityLiving par4EntityLiving, final float par5) {
		if (worldObj != par1World) {
			setWorld(par1World);
		}

		renderEngine = par2RenderEngine;
		entityLivingPlayer = par4EntityLiving;
		fontRenderer = par3FontRenderer;
		playerYaw = par4EntityLiving.prevRotationYaw
				+ (par4EntityLiving.rotationYaw - par4EntityLiving.prevRotationYaw)
				* par5;
		playerPitch = par4EntityLiving.prevRotationPitch
				+ (par4EntityLiving.rotationPitch - par4EntityLiving.prevRotationPitch)
				* par5;
		playerX = par4EntityLiving.lastTickPosX
				+ (par4EntityLiving.posX - par4EntityLiving.lastTickPosX)
				* par5;
		playerY = par4EntityLiving.lastTickPosY
				+ (par4EntityLiving.posY - par4EntityLiving.lastTickPosY)
				* par5;
		playerZ = par4EntityLiving.lastTickPosZ
				+ (par4EntityLiving.posZ - par4EntityLiving.lastTickPosZ)
				* par5;
	}

	/**
	 * Render this TileEntity at its current position from the player
	 */
	public void renderTileEntity(final TileEntity par1TileEntity,
			final float par2) {
		if (par1TileEntity.getDistanceFrom(playerX, playerY, playerZ) < par1TileEntity
				.getMaxRenderDistanceSquared()) {
			final int var3 = worldObj.getLightBrightnessForSkyBlocks(
					par1TileEntity.xCoord, par1TileEntity.yCoord,
					par1TileEntity.zCoord, 0);
			final int var4 = var3 % 65536;
			final int var5 = var3 / 65536;
			OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit,
					var4 / 1.0F, var5 / 1.0F);
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			renderTileEntityAt(par1TileEntity, par1TileEntity.xCoord
					- TileEntityRenderer.staticPlayerX, par1TileEntity.yCoord
					- TileEntityRenderer.staticPlayerY, par1TileEntity.zCoord
					- TileEntityRenderer.staticPlayerZ, par2);
		}
	}

	/**
	 * Render this TileEntity at a given set of coordinates
	 */
	public void renderTileEntityAt(final TileEntity par1TileEntity,
			final double par2, final double par4, final double par6,
			final float par8) {
		final TileEntitySpecialRenderer var9 = getSpecialRendererForEntity(par1TileEntity);

		if (var9 != null) {
			try {
				var9.renderTileEntityAt(par1TileEntity, par2, par4, par6, par8);
			} catch (final Throwable var13) {
				final CrashReport var11 = CrashReport.makeCrashReport(var13,
						"Rendering Tile Entity");
				final CrashReportCategory var12 = var11
						.makeCategory("Tile Entity Details");
				par1TileEntity.func_85027_a(var12);
				throw new ReportedException(var11);
			}
		}
	}

	/**
	 * Sets the world used by all TileEntitySpecialRender instances and notifies
	 * them of this change.
	 */
	public void setWorld(final World par1World) {
		worldObj = par1World;
		final Iterator var2 = specialRendererMap.values().iterator();

		while (var2.hasNext()) {
			final TileEntitySpecialRenderer var3 = (TileEntitySpecialRenderer) var2
					.next();

			if (var3 != null) {
				var3.onWorldChange(par1World);
			}
		}
	}

	public FontRenderer getFontRenderer() {
		return fontRenderer;
	}
}
