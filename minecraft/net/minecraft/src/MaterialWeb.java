package net.minecraft.src;

final class MaterialWeb extends Material {
	MaterialWeb(final MapColor par1MapColor) {
		super(par1MapColor);
	}

	/**
	 * Returns if this material is considered solid or not
	 */
	@Override
	public boolean blocksMovement() {
		return false;
	}
}
