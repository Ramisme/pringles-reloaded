package net.minecraft.src;

import java.util.Random;

public class BlockFlowerPot extends Block {
	public BlockFlowerPot(final int par1) {
		super(par1, Material.circuits);
		setBlockBoundsForItemRender();
	}

	/**
	 * Sets the block's bounds for rendering it as an item
	 */
	@Override
	public void setBlockBoundsForItemRender() {
		final float var1 = 0.375F;
		final float var2 = var1 / 2.0F;
		setBlockBounds(0.5F - var2, 0.0F, 0.5F - var2, 0.5F + var2, var1,
				0.5F + var2);
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return 33;
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	/**
	 * Called upon block activation (right click on the block.)
	 */
	@Override
	public boolean onBlockActivated(final World par1World, final int par2,
			final int par3, final int par4,
			final EntityPlayer par5EntityPlayer, final int par6,
			final float par7, final float par8, final float par9) {
		final ItemStack var10 = par5EntityPlayer.inventory.getCurrentItem();

		if (var10 == null) {
			return false;
		} else if (par1World.getBlockMetadata(par2, par3, par4) != 0) {
			return false;
		} else {
			final int var11 = BlockFlowerPot.getMetaForPlant(var10);

			if (var11 > 0) {
				par1World
						.setBlockMetadataWithNotify(par2, par3, par4, var11, 2);

				if (!par5EntityPlayer.capabilities.isCreativeMode
						&& --var10.stackSize <= 0) {
					par5EntityPlayer.inventory.setInventorySlotContents(
							par5EntityPlayer.inventory.currentItem,
							(ItemStack) null);
				}

				return true;
			} else {
				return false;
			}
		}
	}

	/**
	 * only called by clickMiddleMouseButton , and passed to
	 * inventory.setCurrentItem (along with isCreative)
	 */
	@Override
	public int idPicked(final World par1World, final int par2, final int par3,
			final int par4) {
		final ItemStack var5 = BlockFlowerPot.getPlantForMeta(par1World
				.getBlockMetadata(par2, par3, par4));
		return var5 == null ? Item.flowerPot.itemID : var5.itemID;
	}

	/**
	 * Get the block's damage value (for use with pick block).
	 */
	@Override
	public int getDamageValue(final World par1World, final int par2,
			final int par3, final int par4) {
		final ItemStack var5 = BlockFlowerPot.getPlantForMeta(par1World
				.getBlockMetadata(par2, par3, par4));
		return var5 == null ? Item.flowerPot.itemID : var5.getItemDamage();
	}

	/**
	 * Returns true only if block is flowerPot
	 */
	@Override
	public boolean isFlowerPot() {
		return true;
	}

	/**
	 * Checks to see if its valid to put this block at the specified
	 * coordinates. Args: world, x, y, z
	 */
	@Override
	public boolean canPlaceBlockAt(final World par1World, final int par2,
			final int par3, final int par4) {
		return super.canPlaceBlockAt(par1World, par2, par3, par4)
				&& par1World.doesBlockHaveSolidTopSurface(par2, par3 - 1, par4);
	}

	/**
	 * Lets the block know when one of its neighbor changes. Doesn't know which
	 * neighbor changed (coordinates passed are their own) Args: x, y, z,
	 * neighbor blockID
	 */
	@Override
	public void onNeighborBlockChange(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		if (!par1World.doesBlockHaveSolidTopSurface(par2, par3 - 1, par4)) {
			dropBlockAsItem(par1World, par2, par3, par4,
					par1World.getBlockMetadata(par2, par3, par4), 0);
			par1World.setBlockToAir(par2, par3, par4);
		}
	}

	/**
	 * Drops the block items with a specified chance of dropping the specified
	 * items
	 */
	@Override
	public void dropBlockAsItemWithChance(final World par1World,
			final int par2, final int par3, final int par4, final int par5,
			final float par6, final int par7) {
		super.dropBlockAsItemWithChance(par1World, par2, par3, par4, par5,
				par6, par7);

		if (par5 > 0) {
			final ItemStack var8 = BlockFlowerPot.getPlantForMeta(par5);

			if (var8 != null) {
				dropBlockAsItem_do(par1World, par2, par3, par4, var8);
			}
		}
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return Item.flowerPot.itemID;
	}

	/**
	 * Return the item associated with the specified flower pot metadata value.
	 */
	public static ItemStack getPlantForMeta(final int par0) {
		switch (par0) {
		case 1:
			return new ItemStack(Block.plantRed);

		case 2:
			return new ItemStack(Block.plantYellow);

		case 3:
			return new ItemStack(Block.sapling, 1, 0);

		case 4:
			return new ItemStack(Block.sapling, 1, 1);

		case 5:
			return new ItemStack(Block.sapling, 1, 2);

		case 6:
			return new ItemStack(Block.sapling, 1, 3);

		case 7:
			return new ItemStack(Block.mushroomRed);

		case 8:
			return new ItemStack(Block.mushroomBrown);

		case 9:
			return new ItemStack(Block.cactus);

		case 10:
			return new ItemStack(Block.deadBush);

		case 11:
			return new ItemStack(Block.tallGrass, 1, 2);

		default:
			return null;
		}
	}

	/**
	 * Return the flower pot metadata value associated with the specified item.
	 */
	public static int getMetaForPlant(final ItemStack par0ItemStack) {
		final int var1 = par0ItemStack.getItem().itemID;

		if (var1 == Block.plantRed.blockID) {
			return 1;
		} else if (var1 == Block.plantYellow.blockID) {
			return 2;
		} else if (var1 == Block.cactus.blockID) {
			return 9;
		} else if (var1 == Block.mushroomBrown.blockID) {
			return 8;
		} else if (var1 == Block.mushroomRed.blockID) {
			return 7;
		} else if (var1 == Block.deadBush.blockID) {
			return 10;
		} else {
			if (var1 == Block.sapling.blockID) {
				switch (par0ItemStack.getItemDamage()) {
				case 0:
					return 3;

				case 1:
					return 4;

				case 2:
					return 5;

				case 3:
					return 6;
				}
			}

			if (var1 == Block.tallGrass.blockID) {
				switch (par0ItemStack.getItemDamage()) {
				case 2:
					return 11;
				}
			}

			return 0;
		}
	}
}
