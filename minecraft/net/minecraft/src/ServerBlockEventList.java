package net.minecraft.src;

import java.util.ArrayList;

class ServerBlockEventList extends ArrayList {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2927722749340104196L;

	private ServerBlockEventList() {
	}

	ServerBlockEventList(final ServerBlockEvent par1ServerBlockEvent) {
		this();
	}
}
