package net.minecraft.src;

public class ReportedException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2325070351159358411L;
	/** Instance of CrashReport. */
	private final CrashReport theReportedExceptionCrashReport;

	public ReportedException(final CrashReport par1CrashReport) {
		theReportedExceptionCrashReport = par1CrashReport;
	}

	/**
	 * Gets the CrashReport wrapped by this exception.
	 */
	public CrashReport getCrashReport() {
		return theReportedExceptionCrashReport;
	}

	@Override
	public Throwable getCause() {
		return theReportedExceptionCrashReport.getCrashCause();
	}

	@Override
	public String getMessage() {
		return theReportedExceptionCrashReport.getDescription();
	}
}
