package net.minecraft.src;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

import net.minecraft.client.Minecraft;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

public class GameSettings {
	private static final String[] RENDER_DISTANCES = new String[] {
			"options.renderDistance.far", "options.renderDistance.normal",
			"options.renderDistance.short", "options.renderDistance.tiny" };
	private static final String[] DIFFICULTIES = new String[] {
			"options.difficulty.peaceful", "options.difficulty.easy",
			"options.difficulty.normal", "options.difficulty.hard" };

	/** GUI scale values */
	private static final String[] GUISCALES = new String[] {
			"options.guiScale.auto", "options.guiScale.small",
			"options.guiScale.normal", "options.guiScale.large" };
	private static final String[] CHAT_VISIBILITIES = new String[] {
			"options.chat.visibility.full", "options.chat.visibility.system",
			"options.chat.visibility.hidden" };
	private static final String[] PARTICLES = new String[] {
			"options.particles.all", "options.particles.decreased",
			"options.particles.minimal" };

	/** Limit framerate labels */
	private static final String[] LIMIT_FRAMERATES = new String[] {
			"performance.max", "performance.balanced", "performance.powersaver" };
	private static final String[] AMBIENT_OCCLUSIONS = new String[] {
			"options.ao.off", "options.ao.min", "options.ao.max" };
	public float musicVolume = 1.0F;
	public float soundVolume = 1.0F;
	public float mouseSensitivity = 0.5F;
	public boolean invertMouse = false;
	public int renderDistance = 0;
	public boolean viewBobbing = true;
	public boolean anaglyph = false;

	/** Advanced OpenGL */
	public boolean advancedOpengl = false;
	public int limitFramerate = 1;
	public boolean fancyGraphics = true;

	/** Smooth Lighting */
	public int ambientOcclusion = 2;

	/** Clouds flag */
	public boolean clouds = true;
	public int ofRenderDistanceFine = 128;
	public int ofLimitFramerateFine = 0;
	public int ofFogType = 1;
	public float ofFogStart = 0.8F;
	public int ofMipmapLevel = 0;
	public int ofMipmapType = 0;
	public boolean ofLoadFar = false;
	public int ofPreloadedChunks = 0;
	public boolean ofOcclusionFancy = false;
	public boolean ofSmoothFps = false;
	public boolean ofSmoothWorld = Config.isSingleProcessor();
	public boolean ofLazyChunkLoading = Config.isSingleProcessor();
	public float ofAoLevel = 1.0F;
	public int ofAaLevel = 0;
	public int ofAfLevel = 1;
	public int ofClouds = 0;
	public float ofCloudsHeight = 0.0F;
	public int ofTrees = 0;
	public int ofGrass = 0;
	public int ofRain = 0;
	public int ofWater = 0;
	public int ofDroppedItems = 0;
	public int ofBetterGrass = 3;
	public int ofAutoSaveTicks = 4000;
	public boolean ofLagometer = false;
	public boolean ofProfiler = false;
	public boolean ofWeather = true;
	public boolean ofSky = true;
	public boolean ofStars = true;
	public boolean ofSunMoon = true;
	public int ofChunkUpdates = 1;
	public int ofChunkLoading = 0;
	public boolean ofChunkUpdatesDynamic = false;
	public int ofTime = 0;
	public boolean ofClearWater = false;
	public boolean ofDepthFog = true;
	public boolean ofBetterSnow = false;
	public String ofFullscreenMode = "Default";
	public boolean ofSwampColors = true;
	public boolean ofRandomMobs = true;
	public boolean ofSmoothBiomes = true;
	public boolean ofCustomFonts = true;
	public boolean ofCustomColors = true;
	public boolean ofCustomSky = true;
	public boolean ofShowCapes = true;
	public int ofConnectedTextures = 2;
	public boolean ofNaturalTextures = false;
	public int ofAnimatedWater = 0;
	public int ofAnimatedLava = 0;
	public boolean ofAnimatedFire = true;
	public boolean ofAnimatedPortal = true;
	public boolean ofAnimatedRedstone = true;
	public boolean ofAnimatedExplosion = true;
	public boolean ofAnimatedFlame = true;
	public boolean ofAnimatedSmoke = true;
	public boolean ofVoidParticles = true;
	public boolean ofWaterParticles = true;
	public boolean ofRainSplash = true;
	public boolean ofPortalParticles = true;
	public boolean ofPotionParticles = true;
	public boolean ofDrippingWaterLava = true;
	public boolean ofAnimatedTerrain = true;
	public boolean ofAnimatedItems = true;
	public boolean ofAnimatedTextures = true;
	public static final int DEFAULT = 0;
	public static final int FAST = 1;
	public static final int FANCY = 2;
	public static final int OFF = 3;
	public static final int ANIM_ON = 0;
	public static final int ANIM_GENERATED = 1;
	public static final int ANIM_OFF = 2;
	public static final int CL_DEFAULT = 0;
	public static final int CL_SMOOTH = 1;
	public static final int CL_THREADED = 2;
	public static final String DEFAULT_STR = "Default";
	public KeyBinding ofKeyBindZoom;

	/** The name of the selected texture pack. */
	public String skin = "Default";
	public int chatVisibility = 0;
	public boolean chatColours = true;
	public boolean chatLinks = true;
	public boolean chatLinksPrompt = true;
	public float chatOpacity = 1.0F;
	public boolean serverTextures = true;
	public boolean snooperEnabled = true;
	public boolean fullScreen = false;
	public boolean enableVsync = true;
	public boolean hideServerAddress = false;

	/**
	 * Whether to show advanced information on item tooltips, toggled by F3+H
	 */
	public boolean advancedItemTooltips = false;

	/** Whether to pause when the game loses focus, toggled by F3+P */
	public boolean pauseOnLostFocus = true;

	/** Whether to show your cape */
	public boolean showCape = true;
	public boolean touchscreen = false;
	public int overrideWidth = 0;
	public int overrideHeight = 0;
	public boolean heldItemTooltips = true;
	public float chatScale = 1.0F;
	public float chatWidth = 1.0F;
	public float chatHeightUnfocused = 0.44366196F;
	public float chatHeightFocused = 1.0F;
	public KeyBinding keyBindForward = new KeyBinding("key.forward", 17);
	public KeyBinding keyBindLeft = new KeyBinding("key.left", 30);
	public KeyBinding keyBindBack = new KeyBinding("key.back", 31);
	public KeyBinding keyBindRight = new KeyBinding("key.right", 32);
	public KeyBinding keyBindJump = new KeyBinding("key.jump", 57);
	public KeyBinding keyBindInventory = new KeyBinding("key.inventory", 18);
	public KeyBinding keyBindDrop = new KeyBinding("key.drop", 16);
	public KeyBinding keyBindChat = new KeyBinding("key.chat", 20);
	public KeyBinding keyBindSneak = new KeyBinding("key.sneak", 42);
	public KeyBinding keyBindAttack = new KeyBinding("key.attack", -100);
	public KeyBinding keyBindUseItem = new KeyBinding("key.use", -99);
	public KeyBinding keyBindPlayerList = new KeyBinding("key.playerlist", 15);
	public KeyBinding keyBindPickBlock = new KeyBinding("key.pickItem", -98);
	public KeyBinding keyBindCommand = new KeyBinding("key.command", 53);
	public KeyBinding[] keyBindings;
	protected Minecraft mc;
	private File optionsFile;
	public int difficulty;
	public boolean hideGUI;
	public int thirdPersonView;

	/** true if debug info should be displayed instead of version */
	public boolean showDebugInfo;
	public boolean showDebugProfilerChart;

	/** The lastServer string. */
	public String lastServer;

	/** No clipping for singleplayer */
	public boolean noclip;

	/** Smooth Camera Toggle */
	public boolean smoothCamera;
	public boolean debugCamEnable;

	/** No clipping movement rate */
	public float noclipRate;

	/** Change rate for debug camera */
	public float debugCamRate;
	public float fovSetting;
	public float gammaSetting;

	/** GUI scale */
	public int guiScale;

	/** Determines amount of particles. 0 = All, 1 = Decreased, 2 = Minimal */
	public int particleSetting;

	/** Game settings language */
	public String language;
	private File optionsFileOF;

	public GameSettings(final Minecraft par1Minecraft, final File par2File) {
		renderDistance = 1;
		limitFramerate = 0;
		ofKeyBindZoom = new KeyBinding("Zoom", 29);
		keyBindings = new KeyBinding[] { keyBindAttack, keyBindUseItem,
				keyBindForward, keyBindLeft, keyBindBack, keyBindRight,
				keyBindJump, keyBindSneak, keyBindDrop, keyBindInventory,
				keyBindChat, keyBindPlayerList, keyBindPickBlock,
				ofKeyBindZoom, keyBindCommand };
		difficulty = 2;
		hideGUI = false;
		thirdPersonView = 0;
		showDebugInfo = false;
		showDebugProfilerChart = false;
		lastServer = "";
		noclip = false;
		smoothCamera = false;
		debugCamEnable = false;
		noclipRate = 1.0F;
		debugCamRate = 1.0F;
		fovSetting = 0.0F;
		gammaSetting = 0.0F;
		guiScale = 0;
		particleSetting = 0;
		language = "en_US";
		mc = par1Minecraft;
		optionsFile = new File(par2File, "options.txt");
		optionsFileOF = new File(par2File, "optionsof.txt");
		loadOptions();
		Config.setGameSettings(this);
	}

	public GameSettings() {
		renderDistance = 1;
		limitFramerate = 0;
		ofKeyBindZoom = new KeyBinding("Zoom", 29);
		keyBindings = new KeyBinding[] { keyBindAttack, keyBindUseItem,
				keyBindForward, keyBindLeft, keyBindBack, keyBindRight,
				keyBindJump, keyBindSneak, keyBindDrop, keyBindInventory,
				keyBindChat, keyBindPlayerList, keyBindPickBlock,
				ofKeyBindZoom, keyBindCommand };
		difficulty = 2;
		hideGUI = false;
		thirdPersonView = 0;
		showDebugInfo = false;
		showDebugProfilerChart = false;
		lastServer = "";
		noclip = false;
		smoothCamera = false;
		debugCamEnable = false;
		noclipRate = 1.0F;
		debugCamRate = 1.0F;
		fovSetting = 0.0F;
		gammaSetting = 0.0F;
		guiScale = 0;
		particleSetting = 0;
		language = "en_US";
	}

	public String getKeyBindingDescription(final int par1) {
		final StringTranslate var2 = StringTranslate.getInstance();
		return var2.translateKey(keyBindings[par1].keyDescription);
	}

	/**
	 * The string that appears inside the button/slider in the options menu.
	 */
	public String getOptionDisplayString(final int par1) {
		final int var2 = keyBindings[par1].keyCode;
		return GameSettings.getKeyDisplayString(var2);
	}

	/**
	 * Represents a key or mouse button as a string. Args: key
	 */
	public static String getKeyDisplayString(final int par0) {
		return par0 < 0 ? StatCollector
				.translateToLocalFormatted("key.mouseButton",
						new Object[] { Integer.valueOf(par0 + 101) })
				: Keyboard.getKeyName(par0);
	}

	/**
	 * Returns whether the specified key binding is currently being pressed.
	 */
	public static boolean isKeyDown(final KeyBinding par0KeyBinding) {
		return par0KeyBinding.keyCode < 0 ? Mouse
				.isButtonDown(par0KeyBinding.keyCode + 100) : Keyboard
				.isKeyDown(par0KeyBinding.keyCode);
	}

	/**
	 * Sets a key binding.
	 */
	public void setKeyBinding(final int par1, final int par2) {
		keyBindings[par1].keyCode = par2;
		saveOptions();
	}

	/**
	 * If the specified option is controlled by a slider (float value), this
	 * will set the float value.
	 */
	public void setOptionFloatValue(final EnumOptions par1EnumOptions,
			final float par2) {
		if (par1EnumOptions == EnumOptions.MUSIC) {
			musicVolume = par2;
			mc.sndManager.onSoundOptionsChanged();
		}

		if (par1EnumOptions == EnumOptions.SOUND) {
			soundVolume = par2;
			mc.sndManager.onSoundOptionsChanged();
		}

		if (par1EnumOptions == EnumOptions.SENSITIVITY) {
			mouseSensitivity = par2;
		}

		if (par1EnumOptions == EnumOptions.FOV) {
			fovSetting = par2;
		}

		if (par1EnumOptions == EnumOptions.GAMMA) {
			gammaSetting = par2;
		}

		if (par1EnumOptions == EnumOptions.CLOUD_HEIGHT) {
			ofCloudsHeight = par2;
		}

		if (par1EnumOptions == EnumOptions.AO_LEVEL) {
			ofAoLevel = par2;
			mc.renderGlobal.loadRenderers();
		}

		if (par1EnumOptions == EnumOptions.RENDER_DISTANCE_FINE) {
			final int var3 = ofRenderDistanceFine;
			ofRenderDistanceFine = 32 + (int) (par2 * 480.0F);
			ofRenderDistanceFine = ofRenderDistanceFine >> 4 << 4;
			ofRenderDistanceFine = Config.limit(ofRenderDistanceFine, 32, 512);
			renderDistance = GameSettings
					.fineToRenderDistance(ofRenderDistanceFine);

			if (ofRenderDistanceFine != var3) {
				mc.renderGlobal.loadRenderers();
			}
		}

		if (par1EnumOptions == EnumOptions.FRAMERATE_LIMIT_FINE) {
			ofLimitFramerateFine = (int) (par2 * 200.0F);
			enableVsync = false;

			if (ofLimitFramerateFine < 5) {
				enableVsync = true;
				ofLimitFramerateFine = 0;
			}

			if (ofLimitFramerateFine > 199) {
				enableVsync = false;
				ofLimitFramerateFine = 0;
			}

			if (ofLimitFramerateFine > 30) {
				ofLimitFramerateFine = ofLimitFramerateFine / 5 * 5;
			}

			if (ofLimitFramerateFine > 100) {
				ofLimitFramerateFine = ofLimitFramerateFine / 10 * 10;
			}

			limitFramerate = GameSettings
					.fineToLimitFramerate(ofLimitFramerateFine);
			updateVSync();
		}

		if (par1EnumOptions == EnumOptions.CHAT_OPACITY) {
			chatOpacity = par2;
			mc.ingameGUI.getChatGUI().func_96132_b();
		}

		if (par1EnumOptions == EnumOptions.CHAT_HEIGHT_FOCUSED) {
			chatHeightFocused = par2;
			mc.ingameGUI.getChatGUI().func_96132_b();
		}

		if (par1EnumOptions == EnumOptions.CHAT_HEIGHT_UNFOCUSED) {
			chatHeightUnfocused = par2;
			mc.ingameGUI.getChatGUI().func_96132_b();
		}

		if (par1EnumOptions == EnumOptions.CHAT_WIDTH) {
			chatWidth = par2;
			mc.ingameGUI.getChatGUI().func_96132_b();
		}

		if (par1EnumOptions == EnumOptions.CHAT_SCALE) {
			chatScale = par2;
			mc.ingameGUI.getChatGUI().func_96132_b();
		}
	}

	private void updateWaterOpacity() {
		byte var1 = 3;

		if (ofClearWater) {
			var1 = 1;
		}

		Block.waterStill.setLightOpacity(var1);
		Block.waterMoving.setLightOpacity(var1);

		if (mc.theWorld != null) {
			final IChunkProvider var2 = mc.theWorld.chunkProvider;

			if (var2 != null) {
				for (int var3 = -512; var3 < 512; ++var3) {
					for (int var4 = -512; var4 < 512; ++var4) {
						if (var2.chunkExists(var3, var4)) {
							final Chunk var5 = var2.provideChunk(var3, var4);

							if (var5 != null && !(var5 instanceof EmptyChunk)) {
								final ExtendedBlockStorage[] var6 = var5
										.getBlockStorageArray();

								for (final ExtendedBlockStorage var8 : var6) {
									if (var8 != null) {
										final NibbleArray var9 = var8
												.getSkylightArray();

										if (var9 != null) {
											final byte[] var10 = var9.data;

											for (int var11 = 0; var11 < var10.length; ++var11) {
												var10[var11] = 0;
											}
										}
									}
								}

								var5.generateSkylightMap();
							}
						}
					}
				}

				mc.renderGlobal.loadRenderers();
			}
		}
	}

	public void updateChunkLoading() {
		if (mc.renderGlobal != null) {
			mc.renderGlobal.loadRenderers();
		}
	}

	public void setAllAnimations(final boolean var1) {
		final int var2 = var1 ? 0 : 2;
		ofAnimatedWater = var2;
		ofAnimatedLava = var2;
		ofAnimatedFire = var1;
		ofAnimatedPortal = var1;
		ofAnimatedRedstone = var1;
		ofAnimatedExplosion = var1;
		ofAnimatedFlame = var1;
		ofAnimatedSmoke = var1;
		ofVoidParticles = var1;
		ofWaterParticles = var1;
		ofRainSplash = var1;
		ofPortalParticles = var1;
		ofPotionParticles = var1;
		particleSetting = var1 ? 0 : 2;
		ofDrippingWaterLava = var1;
		ofAnimatedTerrain = var1;
		ofAnimatedItems = var1;
		ofAnimatedTextures = var1;
	}

	/**
	 * For non-float options. Toggles the option on/off, or cycles through the
	 * list i.e. render distances.
	 */
	public void setOptionValue(final EnumOptions par1EnumOptions, final int par2) {
		if (par1EnumOptions == EnumOptions.INVERT_MOUSE) {
			invertMouse = !invertMouse;
		}

		if (par1EnumOptions == EnumOptions.RENDER_DISTANCE) {
			renderDistance = renderDistance + par2 & 3;
			ofRenderDistanceFine = GameSettings
					.renderDistanceToFine(renderDistance);
		}

		if (par1EnumOptions == EnumOptions.GUI_SCALE) {
			guiScale = guiScale + par2 & 3;
		}

		if (par1EnumOptions == EnumOptions.PARTICLES) {
			particleSetting = (particleSetting + par2) % 3;
		}

		if (par1EnumOptions == EnumOptions.VIEW_BOBBING) {
			viewBobbing = !viewBobbing;
		}

		if (par1EnumOptions == EnumOptions.RENDER_CLOUDS) {
			clouds = !clouds;
		}

		if (par1EnumOptions == EnumOptions.ADVANCED_OPENGL) {
			if (!Config.isOcclusionAvailable()) {
				ofOcclusionFancy = false;
				advancedOpengl = false;
			} else if (!advancedOpengl) {
				advancedOpengl = true;
				ofOcclusionFancy = false;
			} else if (!ofOcclusionFancy) {
				ofOcclusionFancy = true;
			} else {
				ofOcclusionFancy = false;
				advancedOpengl = false;
			}

			mc.renderGlobal.setAllRenderersVisible();
		}

		if (par1EnumOptions == EnumOptions.ANAGLYPH) {
			anaglyph = !anaglyph;
			mc.renderEngine.refreshTextures();
		}

		if (par1EnumOptions == EnumOptions.FRAMERATE_LIMIT) {
			limitFramerate = (limitFramerate + par2 + 3) % 3;
			ofLimitFramerateFine = GameSettings
					.limitFramerateToFine(limitFramerate);
		}

		if (par1EnumOptions == EnumOptions.DIFFICULTY) {
			difficulty = difficulty + par2 & 3;
		}

		if (par1EnumOptions == EnumOptions.GRAPHICS) {
			fancyGraphics = !fancyGraphics;
			mc.renderGlobal.loadRenderers();
		}

		if (par1EnumOptions == EnumOptions.AMBIENT_OCCLUSION) {
			ambientOcclusion = (ambientOcclusion + par2) % 3;
			mc.renderGlobal.loadRenderers();
		}

		if (par1EnumOptions == EnumOptions.FOG_FANCY) {
			switch (ofFogType) {
			case 1:
				ofFogType = 2;

				if (!Config.isFancyFogAvailable()) {
					ofFogType = 3;
				}

				break;

			case 2:
				ofFogType = 3;
				break;

			case 3:
				ofFogType = 1;
				break;

			default:
				ofFogType = 1;
			}
		}

		if (par1EnumOptions == EnumOptions.FOG_START) {
			ofFogStart += 0.2F;

			if (ofFogStart > 0.81F) {
				ofFogStart = 0.2F;
			}
		}

		if (par1EnumOptions == EnumOptions.MIPMAP_LEVEL) {
			++ofMipmapLevel;

			if (ofMipmapLevel > 4) {
				ofMipmapLevel = 0;
			}

			mc.renderEngine.refreshBlockTextures();
		}

		if (par1EnumOptions == EnumOptions.MIPMAP_TYPE) {
			++ofMipmapType;

			if (ofMipmapType > 3) {
				ofMipmapType = 0;
			}

			mc.renderEngine.refreshBlockTextures();
		}

		if (par1EnumOptions == EnumOptions.LOAD_FAR) {
			ofLoadFar = !ofLoadFar;
			mc.renderGlobal.loadRenderers();
		}

		if (par1EnumOptions == EnumOptions.PRELOADED_CHUNKS) {
			ofPreloadedChunks += 2;

			if (ofPreloadedChunks > 8) {
				ofPreloadedChunks = 0;
			}

			mc.renderGlobal.loadRenderers();
		}

		if (par1EnumOptions == EnumOptions.SMOOTH_FPS) {
			ofSmoothFps = !ofSmoothFps;
		}

		if (par1EnumOptions == EnumOptions.SMOOTH_WORLD) {
			ofSmoothWorld = !ofSmoothWorld;
			Config.updateThreadPriorities();
		}

		if (par1EnumOptions == EnumOptions.CLOUDS) {
			++ofClouds;

			if (ofClouds > 3) {
				ofClouds = 0;
			}
		}

		if (par1EnumOptions == EnumOptions.TREES) {
			++ofTrees;

			if (ofTrees > 2) {
				ofTrees = 0;
			}

			mc.renderGlobal.loadRenderers();
		}

		if (par1EnumOptions == EnumOptions.GRASS) {
			++ofGrass;

			if (ofGrass > 2) {
				ofGrass = 0;
			}

			RenderBlocks.fancyGrass = Config.isGrassFancy();
			mc.renderGlobal.loadRenderers();
		}

		if (par1EnumOptions == EnumOptions.DROPPED_ITEMS) {
			++ofDroppedItems;

			if (ofDroppedItems > 2) {
				ofDroppedItems = 0;
			}
		}

		if (par1EnumOptions == EnumOptions.RAIN) {
			++ofRain;

			if (ofRain > 3) {
				ofRain = 0;
			}
		}

		if (par1EnumOptions == EnumOptions.WATER) {
			++ofWater;

			if (ofWater > 2) {
				ofWater = 0;
			}
		}

		if (par1EnumOptions == EnumOptions.ANIMATED_WATER) {
			++ofAnimatedWater;

			if (ofAnimatedWater > 2) {
				ofAnimatedWater = 0;
			}
		}

		if (par1EnumOptions == EnumOptions.ANIMATED_LAVA) {
			++ofAnimatedLava;

			if (ofAnimatedLava > 2) {
				ofAnimatedLava = 0;
			}
		}

		if (par1EnumOptions == EnumOptions.ANIMATED_FIRE) {
			ofAnimatedFire = !ofAnimatedFire;
		}

		if (par1EnumOptions == EnumOptions.ANIMATED_PORTAL) {
			ofAnimatedPortal = !ofAnimatedPortal;
		}

		if (par1EnumOptions == EnumOptions.ANIMATED_REDSTONE) {
			ofAnimatedRedstone = !ofAnimatedRedstone;
		}

		if (par1EnumOptions == EnumOptions.ANIMATED_EXPLOSION) {
			ofAnimatedExplosion = !ofAnimatedExplosion;
		}

		if (par1EnumOptions == EnumOptions.ANIMATED_FLAME) {
			ofAnimatedFlame = !ofAnimatedFlame;
		}

		if (par1EnumOptions == EnumOptions.ANIMATED_SMOKE) {
			ofAnimatedSmoke = !ofAnimatedSmoke;
		}

		if (par1EnumOptions == EnumOptions.VOID_PARTICLES) {
			ofVoidParticles = !ofVoidParticles;
		}

		if (par1EnumOptions == EnumOptions.WATER_PARTICLES) {
			ofWaterParticles = !ofWaterParticles;
		}

		if (par1EnumOptions == EnumOptions.PORTAL_PARTICLES) {
			ofPortalParticles = !ofPortalParticles;
		}

		if (par1EnumOptions == EnumOptions.POTION_PARTICLES) {
			ofPotionParticles = !ofPotionParticles;
		}

		if (par1EnumOptions == EnumOptions.DRIPPING_WATER_LAVA) {
			ofDrippingWaterLava = !ofDrippingWaterLava;
		}

		if (par1EnumOptions == EnumOptions.ANIMATED_TERRAIN) {
			ofAnimatedTerrain = !ofAnimatedTerrain;
		}

		if (par1EnumOptions == EnumOptions.ANIMATED_TEXTURES) {
			ofAnimatedTextures = !ofAnimatedTextures;
		}

		if (par1EnumOptions == EnumOptions.ANIMATED_ITEMS) {
			ofAnimatedItems = !ofAnimatedItems;
		}

		if (par1EnumOptions == EnumOptions.RAIN_SPLASH) {
			ofRainSplash = !ofRainSplash;
		}

		if (par1EnumOptions == EnumOptions.LAGOMETER) {
			ofLagometer = !ofLagometer;
		}

		if (par1EnumOptions == EnumOptions.AUTOSAVE_TICKS) {
			ofAutoSaveTicks *= 10;

			if (ofAutoSaveTicks > 40000) {
				ofAutoSaveTicks = 40;
			}
		}

		if (par1EnumOptions == EnumOptions.BETTER_GRASS) {
			++ofBetterGrass;

			if (ofBetterGrass > 3) {
				ofBetterGrass = 1;
			}

			mc.renderGlobal.loadRenderers();
		}

		if (par1EnumOptions == EnumOptions.CONNECTED_TEXTURES) {
			++ofConnectedTextures;

			if (ofConnectedTextures > 3) {
				ofConnectedTextures = 1;
			}

			mc.renderGlobal.loadRenderers();
		}

		if (par1EnumOptions == EnumOptions.WEATHER) {
			ofWeather = !ofWeather;
		}

		if (par1EnumOptions == EnumOptions.SKY) {
			ofSky = !ofSky;
		}

		if (par1EnumOptions == EnumOptions.STARS) {
			ofStars = !ofStars;
		}

		if (par1EnumOptions == EnumOptions.SUN_MOON) {
			ofSunMoon = !ofSunMoon;
		}

		if (par1EnumOptions == EnumOptions.CHUNK_UPDATES) {
			++ofChunkUpdates;

			if (ofChunkUpdates > 5) {
				ofChunkUpdates = 1;
			}
		}

		if (par1EnumOptions == EnumOptions.CHUNK_LOADING) {
			++ofChunkLoading;

			if (ofChunkLoading > 2) {
				ofChunkLoading = 0;
			}

			updateChunkLoading();
		}

		if (par1EnumOptions == EnumOptions.CHUNK_UPDATES_DYNAMIC) {
			ofChunkUpdatesDynamic = !ofChunkUpdatesDynamic;
		}

		if (par1EnumOptions == EnumOptions.TIME) {
			++ofTime;

			if (ofTime > 3) {
				ofTime = 0;
			}
		}

		if (par1EnumOptions == EnumOptions.CLEAR_WATER) {
			ofClearWater = !ofClearWater;
			updateWaterOpacity();
		}

		if (par1EnumOptions == EnumOptions.DEPTH_FOG) {
			ofDepthFog = !ofDepthFog;
		}

		if (par1EnumOptions == EnumOptions.AA_LEVEL) {
			final int[] var3 = new int[] { 0, 2, 4, 6, 8, 12, 16 };
			boolean var4 = false;

			for (int var5 = 0; var5 < var3.length - 1; ++var5) {
				if (ofAaLevel == var3[var5]) {
					ofAaLevel = var3[var5 + 1];
					var4 = true;
					break;
				}
			}

			if (!var4) {
				ofAaLevel = 0;
			}
		}

		if (par1EnumOptions == EnumOptions.AF_LEVEL) {
			ofAfLevel *= 2;

			if (ofAfLevel > 16) {
				ofAfLevel = 1;
			}

			ofAfLevel = Config.limit(ofAfLevel, 1, 16);
			mc.renderEngine.refreshBlockTextures();
			mc.renderGlobal.loadRenderers();
		}

		if (par1EnumOptions == EnumOptions.PROFILER) {
			ofProfiler = !ofProfiler;
		}

		if (par1EnumOptions == EnumOptions.BETTER_SNOW) {
			ofBetterSnow = !ofBetterSnow;
			mc.renderGlobal.loadRenderers();
		}

		if (par1EnumOptions == EnumOptions.SWAMP_COLORS) {
			ofSwampColors = !ofSwampColors;
			CustomColorizer.updateUseDefaultColorMultiplier();
			mc.renderGlobal.loadRenderers();
		}

		if (par1EnumOptions == EnumOptions.RANDOM_MOBS) {
			ofRandomMobs = !ofRandomMobs;
			RandomMobs.resetTextures();
		}

		if (par1EnumOptions == EnumOptions.SMOOTH_BIOMES) {
			ofSmoothBiomes = !ofSmoothBiomes;
			CustomColorizer.updateUseDefaultColorMultiplier();
			mc.renderGlobal.loadRenderers();
		}

		if (par1EnumOptions == EnumOptions.CUSTOM_FONTS) {
			ofCustomFonts = !ofCustomFonts;
			mc.fontRenderer.readFontData();
			mc.standardGalacticFontRenderer.readFontData();
		}

		if (par1EnumOptions == EnumOptions.CUSTOM_COLORS) {
			ofCustomColors = !ofCustomColors;
			CustomColorizer.update(mc.renderEngine);
			mc.renderGlobal.loadRenderers();
		}

		if (par1EnumOptions == EnumOptions.CUSTOM_SKY) {
			ofCustomSky = !ofCustomSky;
			CustomSky.update(mc.renderEngine);
		}

		if (par1EnumOptions == EnumOptions.SHOW_CAPES) {
			ofShowCapes = !ofShowCapes;
			mc.renderGlobal.updateCapes();
		}

		if (par1EnumOptions == EnumOptions.NATURAL_TEXTURES) {
			ofNaturalTextures = !ofNaturalTextures;
			NaturalTextures.update(mc.renderEngine);
			mc.renderGlobal.loadRenderers();
		}

		if (par1EnumOptions == EnumOptions.LAZY_CHUNK_LOADING) {
			ofLazyChunkLoading = !ofLazyChunkLoading;
			mc.renderGlobal.loadRenderers();
		}

		if (par1EnumOptions == EnumOptions.FULLSCREEN_MODE) {
			final List var6 = Arrays.asList(Config.getFullscreenModes());

			if (ofFullscreenMode.equals("Default")) {
				ofFullscreenMode = (String) var6.get(0);
			} else {
				int var7 = var6.indexOf(ofFullscreenMode);

				if (var7 < 0) {
					ofFullscreenMode = "Default";
				} else {
					++var7;

					if (var7 >= var6.size()) {
						ofFullscreenMode = "Default";
					} else {
						ofFullscreenMode = (String) var6.get(var7);
					}
				}
			}
		}

		if (par1EnumOptions == EnumOptions.HELD_ITEM_TOOLTIPS) {
			heldItemTooltips = !heldItemTooltips;
		}

		if (par1EnumOptions == EnumOptions.CHAT_VISIBILITY) {
			chatVisibility = (chatVisibility + par2) % 3;
		}

		if (par1EnumOptions == EnumOptions.CHAT_COLOR) {
			chatColours = !chatColours;
		}

		if (par1EnumOptions == EnumOptions.CHAT_LINKS) {
			chatLinks = !chatLinks;
		}

		if (par1EnumOptions == EnumOptions.CHAT_LINKS_PROMPT) {
			chatLinksPrompt = !chatLinksPrompt;
		}

		if (par1EnumOptions == EnumOptions.USE_SERVER_TEXTURES) {
			serverTextures = !serverTextures;
		}

		if (par1EnumOptions == EnumOptions.SNOOPER_ENABLED) {
			snooperEnabled = !snooperEnabled;
		}

		if (par1EnumOptions == EnumOptions.SHOW_CAPE) {
			showCape = !showCape;
		}

		if (par1EnumOptions == EnumOptions.TOUCHSCREEN) {
			touchscreen = !touchscreen;
		}

		if (par1EnumOptions == EnumOptions.USE_FULLSCREEN) {
			fullScreen = !fullScreen;

			if (mc.isFullScreen() != fullScreen) {
				mc.toggleFullscreen();
			}
		}

		if (par1EnumOptions == EnumOptions.ENABLE_VSYNC) {
			enableVsync = !enableVsync;
			Display.setVSyncEnabled(enableVsync);
		}

		saveOptions();
	}

	public float getOptionFloatValue(final EnumOptions par1EnumOptions) {
		return par1EnumOptions == EnumOptions.CLOUD_HEIGHT ? ofCloudsHeight
				: par1EnumOptions == EnumOptions.AO_LEVEL ? ofAoLevel
						: par1EnumOptions == EnumOptions.RENDER_DISTANCE_FINE ? (ofRenderDistanceFine - 32) / 480.0F
								: par1EnumOptions == EnumOptions.FRAMERATE_LIMIT_FINE ? ofLimitFramerateFine > 0
										&& ofLimitFramerateFine < 200 ? ofLimitFramerateFine / 200.0F
										: enableVsync ? 0.0F : 1.0F
										: par1EnumOptions == EnumOptions.FOV ? fovSetting
												: par1EnumOptions == EnumOptions.GAMMA ? gammaSetting
														: par1EnumOptions == EnumOptions.MUSIC ? musicVolume
																: par1EnumOptions == EnumOptions.SOUND ? soundVolume
																		: par1EnumOptions == EnumOptions.SENSITIVITY ? mouseSensitivity
																				: par1EnumOptions == EnumOptions.CHAT_OPACITY ? chatOpacity
																						: par1EnumOptions == EnumOptions.CHAT_HEIGHT_FOCUSED ? chatHeightFocused
																								: par1EnumOptions == EnumOptions.CHAT_HEIGHT_UNFOCUSED ? chatHeightUnfocused
																										: par1EnumOptions == EnumOptions.CHAT_SCALE ? chatScale
																												: par1EnumOptions == EnumOptions.CHAT_WIDTH ? chatWidth
																														: 0.0F;
	}

	public boolean getOptionOrdinalValue(final EnumOptions par1EnumOptions) {
		switch (EnumOptionsHelper.enumOptionsMappingHelperArray[par1EnumOptions
				.ordinal()]) {
		case 1:
			return invertMouse;

		case 2:
			return viewBobbing;

		case 3:
			return anaglyph;

		case 4:
			return advancedOpengl;

		case 5:
			return clouds;

		case 6:
			return chatColours;

		case 7:
			return chatLinks;

		case 8:
			return chatLinksPrompt;

		case 9:
			return serverTextures;

		case 10:
			return snooperEnabled;

		case 11:
			return fullScreen;

		case 12:
			return enableVsync;

		case 13:
			return showCape;

		case 14:
			return touchscreen;

		default:
			return false;
		}
	}

	/**
	 * Returns the translation of the given index in the given String array. If
	 * the index is smaller than 0 or greater than/equal to the length of the
	 * String array, it is changed to 0.
	 */
	private static String getTranslation(final String[] par0ArrayOfStr, int par1) {
		if (par1 < 0 || par1 >= par0ArrayOfStr.length) {
			par1 = 0;
		}

		final StringTranslate var2 = StringTranslate.getInstance();
		return var2.translateKey(par0ArrayOfStr[par1]);
	}

	/**
	 * Gets a key binding.
	 */
	public String getKeyBinding(final EnumOptions par1EnumOptions) {
		final StringTranslate var2 = StringTranslate.getInstance();
		String var3 = var2.translateKey(par1EnumOptions.getEnumString());

		if (var3 == null) {
			var3 = par1EnumOptions.getEnumString();
		}

		final String var4 = var3 + ": ";

		if (par1EnumOptions == EnumOptions.RENDER_DISTANCE_FINE) {
			String var9 = "Tiny";
			short var7 = 32;

			if (ofRenderDistanceFine >= 64) {
				var9 = "Short";
				var7 = 64;
			}

			if (ofRenderDistanceFine >= 128) {
				var9 = "Normal";
				var7 = 128;
			}

			if (ofRenderDistanceFine >= 256) {
				var9 = "Far";
				var7 = 256;
			}

			if (ofRenderDistanceFine >= 512) {
				var9 = "Extreme";
				var7 = 512;
			}

			final int var8 = ofRenderDistanceFine - var7;
			return var8 == 0 ? var4 + var9 : var4 + var9 + " +" + var8;
		} else if (par1EnumOptions == EnumOptions.FRAMERATE_LIMIT_FINE) {
			return ofLimitFramerateFine > 0 && ofLimitFramerateFine < 200 ? var4
					+ " " + ofLimitFramerateFine + " FPS"
					: enableVsync ? var4 + " VSync" : var4 + " MaxFPS";
		} else if (par1EnumOptions == EnumOptions.ADVANCED_OPENGL) {
			return !advancedOpengl ? var4 + "OFF" : ofOcclusionFancy ? var4
					+ "Fancy" : var4 + "Fast";
		} else if (par1EnumOptions == EnumOptions.FOG_FANCY) {
			switch (ofFogType) {
			case 1:
				return var4 + "Fast";

			case 2:
				return var4 + "Fancy";

			case 3:
				return var4 + "OFF";

			default:
				return var4 + "OFF";
			}
		} else if (par1EnumOptions == EnumOptions.FOG_START) {
			return var4 + ofFogStart;
		} else if (par1EnumOptions == EnumOptions.MIPMAP_LEVEL) {
			return ofMipmapLevel == 0 ? var4 + "OFF"
					: ofMipmapLevel == 4 ? var4 + "Max" : var4 + ofMipmapLevel;
		} else if (par1EnumOptions == EnumOptions.MIPMAP_TYPE) {
			switch (ofMipmapType) {
			case 0:
				return var4 + "Nearest";

			case 1:
				return var4 + "Linear";

			case 2:
				return var4 + "Bilinear";

			case 3:
				return var4 + "Trilinear";

			default:
				return var4 + "Nearest";
			}
		} else if (par1EnumOptions == EnumOptions.LOAD_FAR) {
			return ofLoadFar ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.PRELOADED_CHUNKS) {
			return ofPreloadedChunks == 0 ? var4 + "OFF" : var4
					+ ofPreloadedChunks;
		} else if (par1EnumOptions == EnumOptions.SMOOTH_FPS) {
			return ofSmoothFps ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.SMOOTH_WORLD) {
			return ofSmoothWorld ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.CLOUDS) {
			switch (ofClouds) {
			case 1:
				return var4 + "Fast";

			case 2:
				return var4 + "Fancy";

			case 3:
				return var4 + "OFF";

			default:
				return var4 + "Default";
			}
		} else if (par1EnumOptions == EnumOptions.TREES) {
			switch (ofTrees) {
			case 1:
				return var4 + "Fast";

			case 2:
				return var4 + "Fancy";

			default:
				return var4 + "Default";
			}
		} else if (par1EnumOptions == EnumOptions.GRASS) {
			switch (ofGrass) {
			case 1:
				return var4 + "Fast";

			case 2:
				return var4 + "Fancy";

			default:
				return var4 + "Default";
			}
		} else if (par1EnumOptions == EnumOptions.DROPPED_ITEMS) {
			switch (ofDroppedItems) {
			case 1:
				return var4 + "Fast";

			case 2:
				return var4 + "Fancy";

			default:
				return var4 + "Default";
			}
		} else if (par1EnumOptions == EnumOptions.RAIN) {
			switch (ofRain) {
			case 1:
				return var4 + "Fast";

			case 2:
				return var4 + "Fancy";

			case 3:
				return var4 + "OFF";

			default:
				return var4 + "Default";
			}
		} else if (par1EnumOptions == EnumOptions.WATER) {
			switch (ofWater) {
			case 1:
				return var4 + "Fast";

			case 2:
				return var4 + "Fancy";

			case 3:
				return var4 + "OFF";

			default:
				return var4 + "Default";
			}
		} else if (par1EnumOptions == EnumOptions.ANIMATED_WATER) {
			switch (ofAnimatedWater) {
			case 1:
				return var4 + "Dynamic";

			case 2:
				return var4 + "OFF";

			default:
				return var4 + "ON";
			}
		} else if (par1EnumOptions == EnumOptions.ANIMATED_LAVA) {
			switch (ofAnimatedLava) {
			case 1:
				return var4 + "Dynamic";

			case 2:
				return var4 + "OFF";

			default:
				return var4 + "ON";
			}
		} else if (par1EnumOptions == EnumOptions.ANIMATED_FIRE) {
			return ofAnimatedFire ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.ANIMATED_PORTAL) {
			return ofAnimatedPortal ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.ANIMATED_REDSTONE) {
			return ofAnimatedRedstone ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.ANIMATED_EXPLOSION) {
			return ofAnimatedExplosion ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.ANIMATED_FLAME) {
			return ofAnimatedFlame ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.ANIMATED_SMOKE) {
			return ofAnimatedSmoke ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.VOID_PARTICLES) {
			return ofVoidParticles ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.WATER_PARTICLES) {
			return ofWaterParticles ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.PORTAL_PARTICLES) {
			return ofPortalParticles ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.POTION_PARTICLES) {
			return ofPotionParticles ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.DRIPPING_WATER_LAVA) {
			return ofDrippingWaterLava ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.ANIMATED_TERRAIN) {
			return ofAnimatedTerrain ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.ANIMATED_TEXTURES) {
			return ofAnimatedTextures ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.ANIMATED_ITEMS) {
			return ofAnimatedItems ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.RAIN_SPLASH) {
			return ofRainSplash ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.LAGOMETER) {
			return ofLagometer ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.AUTOSAVE_TICKS) {
			return ofAutoSaveTicks <= 40 ? var4 + "Default (2s)"
					: ofAutoSaveTicks <= 400 ? var4 + "20s"
							: ofAutoSaveTicks <= 4000 ? var4 + "3min" : var4
									+ "30min";
		} else if (par1EnumOptions == EnumOptions.BETTER_GRASS) {
			switch (ofBetterGrass) {
			case 1:
				return var4 + "Fast";

			case 2:
				return var4 + "Fancy";

			default:
				return var4 + "OFF";
			}
		} else if (par1EnumOptions == EnumOptions.CONNECTED_TEXTURES) {
			switch (ofConnectedTextures) {
			case 1:
				return var4 + "Fast";

			case 2:
				return var4 + "Fancy";

			default:
				return var4 + "OFF";
			}
		} else if (par1EnumOptions == EnumOptions.WEATHER) {
			return ofWeather ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.SKY) {
			return ofSky ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.STARS) {
			return ofStars ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.SUN_MOON) {
			return ofSunMoon ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.CHUNK_UPDATES) {
			return var4 + ofChunkUpdates;
		} else if (par1EnumOptions == EnumOptions.CHUNK_LOADING) {
			return ofChunkLoading == 1 ? var4 + "Smooth"
					: ofChunkLoading == 2 ? var4 + "Multi-Core" : var4
							+ "Default";
		} else if (par1EnumOptions == EnumOptions.CHUNK_UPDATES_DYNAMIC) {
			return ofChunkUpdatesDynamic ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.TIME) {
			return ofTime == 1 ? var4 + "Day Only" : ofTime == 3 ? var4
					+ "Night Only" : var4 + "Default";
		} else if (par1EnumOptions == EnumOptions.CLEAR_WATER) {
			return ofClearWater ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.DEPTH_FOG) {
			return ofDepthFog ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.AA_LEVEL) {
			return ofAaLevel == 0 ? var4 + "OFF" : var4 + ofAaLevel;
		} else if (par1EnumOptions == EnumOptions.AF_LEVEL) {
			return ofAfLevel == 1 ? var4 + "OFF" : var4 + ofAfLevel;
		} else if (par1EnumOptions == EnumOptions.PROFILER) {
			return ofProfiler ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.BETTER_SNOW) {
			return ofBetterSnow ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.SWAMP_COLORS) {
			return ofSwampColors ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.RANDOM_MOBS) {
			return ofRandomMobs ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.SMOOTH_BIOMES) {
			return ofSmoothBiomes ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.CUSTOM_FONTS) {
			return ofCustomFonts ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.CUSTOM_COLORS) {
			return ofCustomColors ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.CUSTOM_SKY) {
			return ofCustomSky ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.SHOW_CAPES) {
			return ofShowCapes ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.NATURAL_TEXTURES) {
			return ofNaturalTextures ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.LAZY_CHUNK_LOADING) {
			return ofLazyChunkLoading ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions == EnumOptions.FULLSCREEN_MODE) {
			return var4 + ofFullscreenMode;
		} else if (par1EnumOptions == EnumOptions.HELD_ITEM_TOOLTIPS) {
			return heldItemTooltips ? var4 + "ON" : var4 + "OFF";
		} else if (par1EnumOptions.getEnumFloat()) {
			final float var10 = getOptionFloatValue(par1EnumOptions);
			return par1EnumOptions == EnumOptions.SENSITIVITY ? var10 == 0.0F ? var4
					+ var2.translateKey("options.sensitivity.min")
					: var10 == 1.0F ? var4
							+ var2.translateKey("options.sensitivity.max")
							: var4 + (int) (var10 * 200.0F) + "%"
					: par1EnumOptions == EnumOptions.FOV ? var10 == 0.0F ? var4
							+ var2.translateKey("options.fov.min")
							: var10 == 1.0F ? var4
									+ var2.translateKey("options.fov.max")
									: var4 + (int) (70.0F + var10 * 40.0F)
							: par1EnumOptions == EnumOptions.GAMMA ? var10 == 0.0F ? var4
									+ var2.translateKey("options.gamma.min")
									: var10 == 1.0F ? var4
											+ var2.translateKey("options.gamma.max")
											: var4 + "+"
													+ (int) (var10 * 100.0F)
													+ "%"
									: par1EnumOptions == EnumOptions.CHAT_OPACITY ? var4
											+ (int) (var10 * 90.0F + 10.0F)
											+ "%"
											: par1EnumOptions == EnumOptions.CHAT_HEIGHT_UNFOCUSED ? var4
													+ GuiNewChat
															.func_96130_b(var10)
													+ "px"
													: par1EnumOptions == EnumOptions.CHAT_HEIGHT_FOCUSED ? var4
															+ GuiNewChat
																	.func_96130_b(var10)
															+ "px"
															: par1EnumOptions == EnumOptions.CHAT_WIDTH ? var4
																	+ GuiNewChat
																			.func_96128_a(var10)
																	+ "px"
																	: var10 == 0.0F ? var4
																			+ var2.translateKey("options.off")
																			: var4
																					+ (int) (var10 * 100.0F)
																					+ "%";
		} else if (par1EnumOptions.getEnumBoolean()) {
			final boolean var6 = getOptionOrdinalValue(par1EnumOptions);
			return var6 ? var4 + var2.translateKey("options.on") : var4
					+ var2.translateKey("options.off");
		} else {
			return par1EnumOptions == EnumOptions.RENDER_DISTANCE ? var4
					+ GameSettings.getTranslation(
							GameSettings.RENDER_DISTANCES, renderDistance)
					: par1EnumOptions == EnumOptions.DIFFICULTY ? var4
							+ GameSettings.getTranslation(
									GameSettings.DIFFICULTIES, difficulty)
							: par1EnumOptions == EnumOptions.GUI_SCALE ? var4
									+ GameSettings.getTranslation(
											GameSettings.GUISCALES, guiScale)
									: par1EnumOptions == EnumOptions.CHAT_VISIBILITY ? var4
											+ GameSettings
													.getTranslation(
															GameSettings.CHAT_VISIBILITIES,
															chatVisibility)
											: par1EnumOptions == EnumOptions.PARTICLES ? var4
													+ GameSettings
															.getTranslation(
																	GameSettings.PARTICLES,
																	particleSetting)
													: par1EnumOptions == EnumOptions.FRAMERATE_LIMIT ? var4
															+ GameSettings
																	.getTranslation(
																			GameSettings.LIMIT_FRAMERATES,
																			limitFramerate)
															: par1EnumOptions == EnumOptions.AMBIENT_OCCLUSION ? var4
																	+ GameSettings
																			.getTranslation(
																					GameSettings.AMBIENT_OCCLUSIONS,
																					ambientOcclusion)
																	: par1EnumOptions == EnumOptions.GRAPHICS ? fancyGraphics ? var4
																			+ var2.translateKey("options.graphics.fancy")
																			: var4
																					+ var2.translateKey("options.graphics.fast")
																			: var4;
		}
	}

	/**
	 * Loads the options from the options file. It appears that this has
	 * replaced the previous 'loadOptions'
	 */
	public void loadOptions() {
		try {
			if (!optionsFile.exists()) {
				return;
			}

			final BufferedReader var1 = new BufferedReader(new FileReader(
					optionsFile));
			String var2 = "";

			while ((var2 = var1.readLine()) != null) {
				try {
					final String[] var3 = var2.split(":");

					if (var3[0].equals("music")) {
						musicVolume = parseFloat(var3[1]);
					}

					if (var3[0].equals("sound")) {
						soundVolume = parseFloat(var3[1]);
					}

					if (var3[0].equals("mouseSensitivity")) {
						mouseSensitivity = parseFloat(var3[1]);
					}

					if (var3[0].equals("fov")) {
						fovSetting = parseFloat(var3[1]);
					}

					if (var3[0].equals("gamma")) {
						gammaSetting = parseFloat(var3[1]);
					}

					if (var3[0].equals("invertYMouse")) {
						invertMouse = var3[1].equals("true");
					}

					if (var3[0].equals("viewDistance")) {
						renderDistance = Integer.parseInt(var3[1]);
						ofRenderDistanceFine = GameSettings
								.renderDistanceToFine(renderDistance);
					}

					if (var3[0].equals("guiScale")) {
						guiScale = Integer.parseInt(var3[1]);
					}

					if (var3[0].equals("particles")) {
						particleSetting = Integer.parseInt(var3[1]);
					}

					if (var3[0].equals("bobView")) {
						viewBobbing = var3[1].equals("true");
					}

					if (var3[0].equals("anaglyph3d")) {
						anaglyph = var3[1].equals("true");
					}

					if (var3[0].equals("advancedOpengl")) {
						advancedOpengl = var3[1].equals("true");
					}

					if (var3[0].equals("fpsLimit")) {
						limitFramerate = Integer.parseInt(var3[1]);
						ofLimitFramerateFine = GameSettings
								.limitFramerateToFine(limitFramerate);
					}

					if (var3[0].equals("difficulty")) {
						difficulty = Integer.parseInt(var3[1]);
					}

					if (var3[0].equals("fancyGraphics")) {
						fancyGraphics = var3[1].equals("true");
					}

					if (var3[0].equals("ao")) {
						if (var3[1].equals("true")) {
							ambientOcclusion = 2;
						} else if (var3[1].equals("false")) {
							ambientOcclusion = 0;
						} else {
							ambientOcclusion = Integer.parseInt(var3[1]);
						}
					}

					if (var3[0].equals("clouds")) {
						clouds = var3[1].equals("true");
					}

					if (var3[0].equals("skin")) {
						skin = var3[1];
					}

					if (var3[0].equals("lastServer") && var3.length >= 2) {
						lastServer = var3[1];
					}

					if (var3[0].equals("lang") && var3.length >= 2) {
						language = var3[1];
					}

					if (var3[0].equals("chatVisibility")) {
						chatVisibility = Integer.parseInt(var3[1]);
					}

					if (var3[0].equals("chatColors")) {
						chatColours = var3[1].equals("true");
					}

					if (var3[0].equals("chatLinks")) {
						chatLinks = var3[1].equals("true");
					}

					if (var3[0].equals("chatLinksPrompt")) {
						chatLinksPrompt = var3[1].equals("true");
					}

					if (var3[0].equals("chatOpacity")) {
						chatOpacity = parseFloat(var3[1]);
					}

					if (var3[0].equals("serverTextures")) {
						serverTextures = var3[1].equals("true");
					}

					if (var3[0].equals("snooperEnabled")) {
						snooperEnabled = var3[1].equals("true");
					}

					if (var3[0].equals("fullscreen")) {
						fullScreen = var3[1].equals("true");
					}

					if (var3[0].equals("enableVsync")) {
						enableVsync = var3[1].equals("true");
						updateVSync();
					}

					if (var3[0].equals("hideServerAddress")) {
						hideServerAddress = var3[1].equals("true");
					}

					if (var3[0].equals("advancedItemTooltips")) {
						advancedItemTooltips = var3[1].equals("true");
					}

					if (var3[0].equals("pauseOnLostFocus")) {
						pauseOnLostFocus = var3[1].equals("true");
					}

					if (var3[0].equals("showCape")) {
						showCape = var3[1].equals("true");
					}

					if (var3[0].equals("touchscreen")) {
						touchscreen = var3[1].equals("true");
					}

					if (var3[0].equals("overrideHeight")) {
						overrideHeight = Integer.parseInt(var3[1]);
					}

					if (var3[0].equals("overrideWidth")) {
						overrideWidth = Integer.parseInt(var3[1]);
					}

					if (var3[0].equals("heldItemTooltips")) {
						heldItemTooltips = var3[1].equals("true");
					}

					if (var3[0].equals("chatHeightFocused")) {
						chatHeightFocused = parseFloat(var3[1]);
					}

					if (var3[0].equals("chatHeightUnfocused")) {
						chatHeightUnfocused = parseFloat(var3[1]);
					}

					if (var3[0].equals("chatScale")) {
						chatScale = parseFloat(var3[1]);
					}

					if (var3[0].equals("chatWidth")) {
						chatWidth = parseFloat(var3[1]);
					}

					for (int var4 = 0; var4 < keyBindings.length; ++var4) {
						if (var3[0].equals("key_"
								+ keyBindings[var4].keyDescription)) {
							keyBindings[var4].keyCode = Integer
									.parseInt(var3[1]);
						}
					}
				} catch (final Exception var7) {
					mc.getLogAgent().logWarning("Skipping bad option: " + var2);
					var7.printStackTrace();
				}
			}

			KeyBinding.resetKeyBindingArrayAndHash();
			var1.close();
		} catch (final Exception var8) {
			mc.getLogAgent().logWarning("Failed to load options");
			var8.printStackTrace();
		}

		try {
			File var9 = optionsFileOF;

			if (!var9.exists()) {
				var9 = optionsFile;
			}

			if (!var9.exists()) {
				return;
			}

			final BufferedReader var10 = new BufferedReader(
					new FileReader(var9));
			String var11 = "";

			while ((var11 = var10.readLine()) != null) {
				try {
					final String[] var12 = var11.split(":");

					if (var12[0].equals("ofRenderDistanceFine")
							&& var12.length >= 2) {
						ofRenderDistanceFine = Integer.valueOf(var12[1])
								.intValue();
						ofRenderDistanceFine = Config.limit(
								ofRenderDistanceFine, 32, 512);
						renderDistance = GameSettings
								.fineToRenderDistance(ofRenderDistanceFine);
					}

					if (var12[0].equals("ofLimitFramerateFine")
							&& var12.length >= 2) {
						ofLimitFramerateFine = Integer.valueOf(var12[1])
								.intValue();
						ofLimitFramerateFine = Config.limit(
								ofLimitFramerateFine, 0, 199);
						limitFramerate = GameSettings
								.fineToLimitFramerate(ofLimitFramerateFine);
					}

					if (var12[0].equals("ofFogType") && var12.length >= 2) {
						ofFogType = Integer.valueOf(var12[1]).intValue();
						ofFogType = Config.limit(ofFogType, 1, 3);
					}

					if (var12[0].equals("ofFogStart") && var12.length >= 2) {
						ofFogStart = Float.valueOf(var12[1]).floatValue();

						if (ofFogStart < 0.2F) {
							ofFogStart = 0.2F;
						}

						if (ofFogStart > 0.81F) {
							ofFogStart = 0.8F;
						}
					}

					if (var12[0].equals("ofMipmapLevel") && var12.length >= 2) {
						ofMipmapLevel = Integer.valueOf(var12[1]).intValue();

						if (ofMipmapLevel < 0) {
							ofMipmapLevel = 0;
						}

						if (ofMipmapLevel > 4) {
							ofMipmapLevel = 4;
						}
					}

					if (var12[0].equals("ofMipmapType") && var12.length >= 2) {
						ofMipmapType = Integer.valueOf(var12[1]).intValue();
						ofMipmapType = Config.limit(ofMipmapType, 0, 3);
					}

					if (var12[0].equals("ofLoadFar") && var12.length >= 2) {
						ofLoadFar = Boolean.valueOf(var12[1]).booleanValue();
					}

					if (var12[0].equals("ofPreloadedChunks")
							&& var12.length >= 2) {
						ofPreloadedChunks = Integer.valueOf(var12[1])
								.intValue();

						if (ofPreloadedChunks < 0) {
							ofPreloadedChunks = 0;
						}

						if (ofPreloadedChunks > 8) {
							ofPreloadedChunks = 8;
						}
					}

					if (var12[0].equals("ofOcclusionFancy")
							&& var12.length >= 2) {
						ofOcclusionFancy = Boolean.valueOf(var12[1])
								.booleanValue();
					}

					if (var12[0].equals("ofSmoothFps") && var12.length >= 2) {
						ofSmoothFps = Boolean.valueOf(var12[1]).booleanValue();
					}

					if (var12[0].equals("ofSmoothWorld") && var12.length >= 2) {
						ofSmoothWorld = Boolean.valueOf(var12[1])
								.booleanValue();
					}

					if (var12[0].equals("ofAoLevel") && var12.length >= 2) {
						ofAoLevel = Float.valueOf(var12[1]).floatValue();
						ofAoLevel = Config.limit(ofAoLevel, 0.0F, 1.0F);
					}

					if (var12[0].equals("ofClouds") && var12.length >= 2) {
						ofClouds = Integer.valueOf(var12[1]).intValue();
						ofClouds = Config.limit(ofClouds, 0, 3);
					}

					if (var12[0].equals("ofCloudsHeight") && var12.length >= 2) {
						ofCloudsHeight = Float.valueOf(var12[1]).floatValue();
						ofCloudsHeight = Config.limit(ofCloudsHeight, 0.0F,
								1.0F);
					}

					if (var12[0].equals("ofTrees") && var12.length >= 2) {
						ofTrees = Integer.valueOf(var12[1]).intValue();
						ofTrees = Config.limit(ofTrees, 0, 2);
					}

					if (var12[0].equals("ofGrass") && var12.length >= 2) {
						ofGrass = Integer.valueOf(var12[1]).intValue();
						ofGrass = Config.limit(ofGrass, 0, 2);
					}

					if (var12[0].equals("ofDroppedItems") && var12.length >= 2) {
						ofDroppedItems = Integer.valueOf(var12[1]).intValue();
						ofDroppedItems = Config.limit(ofDroppedItems, 0, 2);
					}

					if (var12[0].equals("ofRain") && var12.length >= 2) {
						ofRain = Integer.valueOf(var12[1]).intValue();
						ofRain = Config.limit(ofRain, 0, 3);
					}

					if (var12[0].equals("ofWater") && var12.length >= 2) {
						ofWater = Integer.valueOf(var12[1]).intValue();
						ofWater = Config.limit(ofWater, 0, 3);
					}

					if (var12[0].equals("ofAnimatedWater") && var12.length >= 2) {
						ofAnimatedWater = Integer.valueOf(var12[1]).intValue();
						ofAnimatedWater = Config.limit(ofAnimatedWater, 0, 2);
					}

					if (var12[0].equals("ofAnimatedLava") && var12.length >= 2) {
						ofAnimatedLava = Integer.valueOf(var12[1]).intValue();
						ofAnimatedLava = Config.limit(ofAnimatedLava, 0, 2);
					}

					if (var12[0].equals("ofAnimatedFire") && var12.length >= 2) {
						ofAnimatedFire = Boolean.valueOf(var12[1])
								.booleanValue();
					}

					if (var12[0].equals("ofAnimatedPortal")
							&& var12.length >= 2) {
						ofAnimatedPortal = Boolean.valueOf(var12[1])
								.booleanValue();
					}

					if (var12[0].equals("ofAnimatedRedstone")
							&& var12.length >= 2) {
						ofAnimatedRedstone = Boolean.valueOf(var12[1])
								.booleanValue();
					}

					if (var12[0].equals("ofAnimatedExplosion")
							&& var12.length >= 2) {
						ofAnimatedExplosion = Boolean.valueOf(var12[1])
								.booleanValue();
					}

					if (var12[0].equals("ofAnimatedFlame") && var12.length >= 2) {
						ofAnimatedFlame = Boolean.valueOf(var12[1])
								.booleanValue();
					}

					if (var12[0].equals("ofAnimatedSmoke") && var12.length >= 2) {
						ofAnimatedSmoke = Boolean.valueOf(var12[1])
								.booleanValue();
					}

					if (var12[0].equals("ofVoidParticles") && var12.length >= 2) {
						ofVoidParticles = Boolean.valueOf(var12[1])
								.booleanValue();
					}

					if (var12[0].equals("ofWaterParticles")
							&& var12.length >= 2) {
						ofWaterParticles = Boolean.valueOf(var12[1])
								.booleanValue();
					}

					if (var12[0].equals("ofPortalParticles")
							&& var12.length >= 2) {
						ofPortalParticles = Boolean.valueOf(var12[1])
								.booleanValue();
					}

					if (var12[0].equals("ofPotionParticles")
							&& var12.length >= 2) {
						ofPotionParticles = Boolean.valueOf(var12[1])
								.booleanValue();
					}

					if (var12[0].equals("ofDrippingWaterLava")
							&& var12.length >= 2) {
						ofDrippingWaterLava = Boolean.valueOf(var12[1])
								.booleanValue();
					}

					if (var12[0].equals("ofAnimatedTerrain")
							&& var12.length >= 2) {
						ofAnimatedTerrain = Boolean.valueOf(var12[1])
								.booleanValue();
					}

					if (var12[0].equals("ofAnimatedTextures")
							&& var12.length >= 2) {
						ofAnimatedTextures = Boolean.valueOf(var12[1])
								.booleanValue();
					}

					if (var12[0].equals("ofAnimatedItems") && var12.length >= 2) {
						ofAnimatedItems = Boolean.valueOf(var12[1])
								.booleanValue();
					}

					if (var12[0].equals("ofRainSplash") && var12.length >= 2) {
						ofRainSplash = Boolean.valueOf(var12[1]).booleanValue();
					}

					if (var12[0].equals("ofLagometer") && var12.length >= 2) {
						ofLagometer = Boolean.valueOf(var12[1]).booleanValue();
					}

					if (var12[0].equals("ofAutoSaveTicks") && var12.length >= 2) {
						ofAutoSaveTicks = Integer.valueOf(var12[1]).intValue();
						ofAutoSaveTicks = Config.limit(ofAutoSaveTicks, 40,
								40000);
					}

					if (var12[0].equals("ofBetterGrass") && var12.length >= 2) {
						ofBetterGrass = Integer.valueOf(var12[1]).intValue();
						ofBetterGrass = Config.limit(ofBetterGrass, 1, 3);
					}

					if (var12[0].equals("ofConnectedTextures")
							&& var12.length >= 2) {
						ofConnectedTextures = Integer.valueOf(var12[1])
								.intValue();
						ofConnectedTextures = Config.limit(ofConnectedTextures,
								1, 3);
					}

					if (var12[0].equals("ofWeather") && var12.length >= 2) {
						ofWeather = Boolean.valueOf(var12[1]).booleanValue();
					}

					if (var12[0].equals("ofSky") && var12.length >= 2) {
						ofSky = Boolean.valueOf(var12[1]).booleanValue();
					}

					if (var12[0].equals("ofStars") && var12.length >= 2) {
						ofStars = Boolean.valueOf(var12[1]).booleanValue();
					}

					if (var12[0].equals("ofSunMoon") && var12.length >= 2) {
						ofSunMoon = Boolean.valueOf(var12[1]).booleanValue();
					}

					if (var12[0].equals("ofChunkUpdates") && var12.length >= 2) {
						ofChunkUpdates = Integer.valueOf(var12[1]).intValue();
						ofChunkUpdates = Config.limit(ofChunkUpdates, 1, 5);
					}

					if (var12[0].equals("ofChunkLoading") && var12.length >= 2) {
						ofChunkLoading = Integer.valueOf(var12[1]).intValue();
						ofChunkLoading = Config.limit(ofChunkLoading, 0, 2);
						updateChunkLoading();
					}

					if (var12[0].equals("ofChunkUpdatesDynamic")
							&& var12.length >= 2) {
						ofChunkUpdatesDynamic = Boolean.valueOf(var12[1])
								.booleanValue();
					}

					if (var12[0].equals("ofTime") && var12.length >= 2) {
						ofTime = Integer.valueOf(var12[1]).intValue();
						ofTime = Config.limit(ofTime, 0, 3);
					}

					if (var12[0].equals("ofClearWater") && var12.length >= 2) {
						ofClearWater = Boolean.valueOf(var12[1]).booleanValue();
						updateWaterOpacity();
					}

					if (var12[0].equals("ofDepthFog") && var12.length >= 2) {
						ofDepthFog = Boolean.valueOf(var12[1]).booleanValue();
					}

					if (var12[0].equals("ofAaLevel") && var12.length >= 2) {
						ofAaLevel = Integer.valueOf(var12[1]).intValue();
						ofAaLevel = Config.limit(ofAaLevel, 0, 16);
					}

					if (var12[0].equals("ofAfLevel") && var12.length >= 2) {
						ofAfLevel = Integer.valueOf(var12[1]).intValue();
						ofAfLevel = Config.limit(ofAfLevel, 1, 16);
					}

					if (var12[0].equals("ofProfiler") && var12.length >= 2) {
						ofProfiler = Boolean.valueOf(var12[1]).booleanValue();
					}

					if (var12[0].equals("ofBetterSnow") && var12.length >= 2) {
						ofBetterSnow = Boolean.valueOf(var12[1]).booleanValue();
					}

					if (var12[0].equals("ofSwampColors") && var12.length >= 2) {
						ofSwampColors = Boolean.valueOf(var12[1])
								.booleanValue();
					}

					if (var12[0].equals("ofRandomMobs") && var12.length >= 2) {
						ofRandomMobs = Boolean.valueOf(var12[1]).booleanValue();
					}

					if (var12[0].equals("ofSmoothBiomes") && var12.length >= 2) {
						ofSmoothBiomes = Boolean.valueOf(var12[1])
								.booleanValue();
					}

					if (var12[0].equals("ofCustomFonts") && var12.length >= 2) {
						ofCustomFonts = Boolean.valueOf(var12[1])
								.booleanValue();
					}

					if (var12[0].equals("ofCustomColors") && var12.length >= 2) {
						ofCustomColors = Boolean.valueOf(var12[1])
								.booleanValue();
					}

					if (var12[0].equals("ofCustomSky") && var12.length >= 2) {
						ofCustomSky = Boolean.valueOf(var12[1]).booleanValue();
					}

					if (var12[0].equals("ofShowCapes") && var12.length >= 2) {
						ofShowCapes = Boolean.valueOf(var12[1]).booleanValue();
					}

					if (var12[0].equals("ofNaturalTextures")
							&& var12.length >= 2) {
						ofNaturalTextures = Boolean.valueOf(var12[1])
								.booleanValue();
					}

					if (var12[0].equals("ofLazyChunkLoading")
							&& var12.length >= 2) {
						ofLazyChunkLoading = Boolean.valueOf(var12[1])
								.booleanValue();
					}

					if (var12[0].equals("ofFullscreenMode")
							&& var12.length >= 2) {
						ofFullscreenMode = var12[1];
					}
				} catch (final Exception var5) {
					Config.dbg("Skipping bad option: " + var11);
					var5.printStackTrace();
				}
			}

			KeyBinding.resetKeyBindingArrayAndHash();
			var10.close();
		} catch (final Exception var6) {
			Config.dbg("Failed to load options");
			var6.printStackTrace();
		}
	}

	/**
	 * Parses a string into a float.
	 */
	private float parseFloat(final String par1Str) {
		return par1Str.equals("true") ? 1.0F : par1Str.equals("false") ? 0.0F
				: Float.parseFloat(par1Str);
	}

	/**
	 * Saves the options to the options file.
	 */
	public void saveOptions() {
		if (Reflector.FMLClientHandler.exists()) {
			final Object var1 = Reflector.call(
					Reflector.FMLClientHandler_instance, new Object[0]);

			if (var1 != null
					&& Reflector
							.callBoolean(var1,
									Reflector.FMLClientHandler_isLoading,
									new Object[0])) {
				return;
			}
		}

		PrintWriter var5;

		try {
			var5 = new PrintWriter(new FileWriter(optionsFile));
			var5.println("music:" + musicVolume);
			var5.println("sound:" + soundVolume);
			var5.println("invertYMouse:" + invertMouse);
			var5.println("mouseSensitivity:" + mouseSensitivity);
			var5.println("fov:" + fovSetting);
			var5.println("gamma:" + gammaSetting);
			var5.println("viewDistance:" + renderDistance);
			var5.println("guiScale:" + guiScale);
			var5.println("particles:" + particleSetting);
			var5.println("bobView:" + viewBobbing);
			var5.println("anaglyph3d:" + anaglyph);
			var5.println("advancedOpengl:" + advancedOpengl);
			var5.println("fpsLimit:" + limitFramerate);
			var5.println("difficulty:" + difficulty);
			var5.println("fancyGraphics:" + fancyGraphics);
			var5.println("ao:" + ambientOcclusion);
			var5.println("clouds:" + clouds);
			var5.println("skin:" + skin);
			var5.println("lastServer:" + lastServer);
			var5.println("lang:" + language);
			var5.println("chatVisibility:" + chatVisibility);
			var5.println("chatColors:" + chatColours);
			var5.println("chatLinks:" + chatLinks);
			var5.println("chatLinksPrompt:" + chatLinksPrompt);
			var5.println("chatOpacity:" + chatOpacity);
			var5.println("serverTextures:" + serverTextures);
			var5.println("snooperEnabled:" + snooperEnabled);
			var5.println("fullscreen:" + fullScreen);
			var5.println("enableVsync:" + enableVsync);
			var5.println("hideServerAddress:" + hideServerAddress);
			var5.println("advancedItemTooltips:" + advancedItemTooltips);
			var5.println("pauseOnLostFocus:" + pauseOnLostFocus);
			var5.println("showCape:" + showCape);
			var5.println("touchscreen:" + touchscreen);
			var5.println("overrideWidth:" + overrideWidth);
			var5.println("overrideHeight:" + overrideHeight);
			var5.println("heldItemTooltips:" + heldItemTooltips);
			var5.println("chatHeightFocused:" + chatHeightFocused);
			var5.println("chatHeightUnfocused:" + chatHeightUnfocused);
			var5.println("chatScale:" + chatScale);
			var5.println("chatWidth:" + chatWidth);

			for (final KeyBinding keyBinding : keyBindings) {
				var5.println("key_" + keyBinding.keyDescription + ":"
						+ keyBinding.keyCode);
			}

			var5.close();
		} catch (final Exception var4) {
			mc.getLogAgent().logWarning("Failed to save options");
			var4.printStackTrace();
		}

		try {
			var5 = new PrintWriter(new FileWriter(optionsFileOF));
			var5.println("ofRenderDistanceFine:" + ofRenderDistanceFine);
			var5.println("ofLimitFramerateFine:" + ofLimitFramerateFine);
			var5.println("ofFogType:" + ofFogType);
			var5.println("ofFogStart:" + ofFogStart);
			var5.println("ofMipmapLevel:" + ofMipmapLevel);
			var5.println("ofMipmapType:" + ofMipmapType);
			var5.println("ofLoadFar:" + ofLoadFar);
			var5.println("ofPreloadedChunks:" + ofPreloadedChunks);
			var5.println("ofOcclusionFancy:" + ofOcclusionFancy);
			var5.println("ofSmoothFps:" + ofSmoothFps);
			var5.println("ofSmoothWorld:" + ofSmoothWorld);
			var5.println("ofAoLevel:" + ofAoLevel);
			var5.println("ofClouds:" + ofClouds);
			var5.println("ofCloudsHeight:" + ofCloudsHeight);
			var5.println("ofTrees:" + ofTrees);
			var5.println("ofGrass:" + ofGrass);
			var5.println("ofDroppedItems:" + ofDroppedItems);
			var5.println("ofRain:" + ofRain);
			var5.println("ofWater:" + ofWater);
			var5.println("ofAnimatedWater:" + ofAnimatedWater);
			var5.println("ofAnimatedLava:" + ofAnimatedLava);
			var5.println("ofAnimatedFire:" + ofAnimatedFire);
			var5.println("ofAnimatedPortal:" + ofAnimatedPortal);
			var5.println("ofAnimatedRedstone:" + ofAnimatedRedstone);
			var5.println("ofAnimatedExplosion:" + ofAnimatedExplosion);
			var5.println("ofAnimatedFlame:" + ofAnimatedFlame);
			var5.println("ofAnimatedSmoke:" + ofAnimatedSmoke);
			var5.println("ofVoidParticles:" + ofVoidParticles);
			var5.println("ofWaterParticles:" + ofWaterParticles);
			var5.println("ofPortalParticles:" + ofPortalParticles);
			var5.println("ofPotionParticles:" + ofPotionParticles);
			var5.println("ofDrippingWaterLava:" + ofDrippingWaterLava);
			var5.println("ofAnimatedTerrain:" + ofAnimatedTerrain);
			var5.println("ofAnimatedTextures:" + ofAnimatedTextures);
			var5.println("ofAnimatedItems:" + ofAnimatedItems);
			var5.println("ofRainSplash:" + ofRainSplash);
			var5.println("ofLagometer:" + ofLagometer);
			var5.println("ofAutoSaveTicks:" + ofAutoSaveTicks);
			var5.println("ofBetterGrass:" + ofBetterGrass);
			var5.println("ofConnectedTextures:" + ofConnectedTextures);
			var5.println("ofWeather:" + ofWeather);
			var5.println("ofSky:" + ofSky);
			var5.println("ofStars:" + ofStars);
			var5.println("ofSunMoon:" + ofSunMoon);
			var5.println("ofChunkUpdates:" + ofChunkUpdates);
			var5.println("ofChunkLoading:" + ofChunkLoading);
			var5.println("ofChunkUpdatesDynamic:" + ofChunkUpdatesDynamic);
			var5.println("ofTime:" + ofTime);
			var5.println("ofClearWater:" + ofClearWater);
			var5.println("ofDepthFog:" + ofDepthFog);
			var5.println("ofAaLevel:" + ofAaLevel);
			var5.println("ofAfLevel:" + ofAfLevel);
			var5.println("ofProfiler:" + ofProfiler);
			var5.println("ofBetterSnow:" + ofBetterSnow);
			var5.println("ofSwampColors:" + ofSwampColors);
			var5.println("ofRandomMobs:" + ofRandomMobs);
			var5.println("ofSmoothBiomes:" + ofSmoothBiomes);
			var5.println("ofCustomFonts:" + ofCustomFonts);
			var5.println("ofCustomColors:" + ofCustomColors);
			var5.println("ofCustomSky:" + ofCustomSky);
			var5.println("ofShowCapes:" + ofShowCapes);
			var5.println("ofNaturalTextures:" + ofNaturalTextures);
			var5.println("ofLazyChunkLoading:" + ofLazyChunkLoading);
			var5.println("ofFullscreenMode:" + ofFullscreenMode);
			var5.close();
		} catch (final Exception var3) {
			Config.dbg("Failed to save options");
			var3.printStackTrace();
		}

		sendSettingsToServer();
	}

	/**
	 * Send a client info packet with settings information to the server
	 */
	public void sendSettingsToServer() {
		if (mc.thePlayer != null) {
			mc.thePlayer.sendQueue.addToSendQueue(new Packet204ClientInfo(
					language, renderDistance, chatVisibility, chatColours,
					difficulty, showCape));
		}
	}

	public void resetSettings() {
		renderDistance = 1;
		ofRenderDistanceFine = GameSettings
				.renderDistanceToFine(renderDistance);
		viewBobbing = true;
		anaglyph = false;
		advancedOpengl = false;
		limitFramerate = 0;
		enableVsync = false;
		updateVSync();
		ofLimitFramerateFine = 0;
		fancyGraphics = true;
		ambientOcclusion = 2;
		clouds = true;
		fovSetting = 0.0F;
		gammaSetting = 0.0F;
		guiScale = 0;
		particleSetting = 0;
		heldItemTooltips = true;
		ofFogType = 1;
		ofFogStart = 0.8F;
		ofMipmapLevel = 0;
		ofMipmapType = 0;
		ofLoadFar = false;
		ofPreloadedChunks = 0;
		ofOcclusionFancy = false;
		ofSmoothFps = false;
		ofSmoothWorld = Config.isSingleProcessor();
		ofLazyChunkLoading = Config.isSingleProcessor();
		ofAoLevel = 1.0F;
		ofAaLevel = 0;
		ofAfLevel = 1;
		ofClouds = 0;
		ofCloudsHeight = 0.0F;
		ofTrees = 0;
		ofGrass = 0;
		ofRain = 0;
		ofWater = 0;
		ofBetterGrass = 3;
		ofAutoSaveTicks = 4000;
		ofLagometer = false;
		ofProfiler = false;
		ofWeather = true;
		ofSky = true;
		ofStars = true;
		ofSunMoon = true;
		ofChunkUpdates = 1;
		ofChunkLoading = 0;
		ofChunkUpdatesDynamic = false;
		ofTime = 0;
		ofClearWater = false;
		ofDepthFog = true;
		ofBetterSnow = false;
		ofFullscreenMode = "Default";
		ofSwampColors = true;
		ofRandomMobs = true;
		ofSmoothBiomes = true;
		ofCustomFonts = true;
		ofCustomColors = true;
		ofCustomSky = true;
		ofShowCapes = true;
		ofConnectedTextures = 2;
		ofNaturalTextures = false;
		ofAnimatedWater = 0;
		ofAnimatedLava = 0;
		ofAnimatedFire = true;
		ofAnimatedPortal = true;
		ofAnimatedRedstone = true;
		ofAnimatedExplosion = true;
		ofAnimatedFlame = true;
		ofAnimatedSmoke = true;
		ofVoidParticles = true;
		ofWaterParticles = true;
		ofRainSplash = true;
		ofPortalParticles = true;
		ofPotionParticles = true;
		ofDrippingWaterLava = true;
		ofAnimatedTerrain = true;
		ofAnimatedItems = true;
		ofAnimatedTextures = true;
		mc.renderGlobal.updateCapes();
		updateWaterOpacity();
		mc.renderGlobal.setAllRenderersVisible();
		mc.renderEngine.refreshTextures();
		mc.renderGlobal.loadRenderers();
		saveOptions();
	}

	public void updateVSync() {
		Display.setVSyncEnabled(enableVsync);
	}

	private static int fineToRenderDistance(final int var0) {
		byte var1 = 3;

		if (var0 > 32) {
			var1 = 2;
		}

		if (var0 > 64) {
			var1 = 1;
		}

		if (var0 > 128) {
			var1 = 0;
		}

		return var1;
	}

	private static int renderDistanceToFine(final int var0) {
		return 32 << 3 - var0;
	}

	private static int fineToLimitFramerate(final int var0) {
		byte var1 = 2;

		if (var0 > 35) {
			var1 = 1;
		}

		if (var0 >= 200) {
			var1 = 0;
		}

		if (var0 <= 0) {
			var1 = 0;
		}

		return var1;
	}

	private static int limitFramerateToFine(final int var0) {
		switch (var0) {
		case 0:
			return 0;

		case 1:
			return 120;

		case 2:
			return 35;

		default:
			return 0;
		}
	}

	/**
	 * Should render clouds
	 */
	public boolean shouldRenderClouds() {
		return ofRenderDistanceFine > 64 && clouds;
	}
}
