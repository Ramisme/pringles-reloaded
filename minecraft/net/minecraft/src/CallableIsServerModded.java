package net.minecraft.src;

import java.util.concurrent.Callable;

import net.minecraft.server.MinecraftServer;

public class CallableIsServerModded implements Callable {
	/** Reference to the MinecraftServer object. */
	final MinecraftServer mcServer;

	public CallableIsServerModded(final MinecraftServer par1) {
		mcServer = par1;
	}

	public String func_96558_a() {
		return mcServer.theProfiler.profilingEnabled ? mcServer.theProfiler
				.getNameOfLastSection() : "N/A (disabled)";
	}

	@Override
	public Object call() {
		return func_96558_a();
	}
}
