package net.minecraft.src;

import java.util.List;

public class BlockLilyPad extends BlockFlower {
	protected BlockLilyPad(final int par1) {
		super(par1);
		final float var2 = 0.5F;
		final float var3 = 0.015625F;
		setBlockBounds(0.5F - var2, 0.0F, 0.5F - var2, 0.5F + var2, var3,
				0.5F + var2);
		setCreativeTab(CreativeTabs.tabDecorations);
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return 23;
	}

	/**
	 * Adds all intersecting collision boxes to a list. (Be sure to only add
	 * boxes to the list if they intersect the mask.) Parameters: World, X, Y,
	 * Z, mask, list, colliding entity
	 */
	@Override
	public void addCollisionBoxesToList(final World par1World, final int par2,
			final int par3, final int par4,
			final AxisAlignedBB par5AxisAlignedBB, final List par6List,
			final Entity par7Entity) {
		if (par7Entity == null || !(par7Entity instanceof EntityBoat)) {
			super.addCollisionBoxesToList(par1World, par2, par3, par4,
					par5AxisAlignedBB, par6List, par7Entity);
		}
	}

	/**
	 * Returns a bounding box from the pool of bounding boxes (this means this
	 * box can change after the pool has been cleared to be reused)
	 */
	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(final World par1World,
			final int par2, final int par3, final int par4) {
		return AxisAlignedBB.getAABBPool().getAABB(par2 + minX, par3 + minY,
				par4 + minZ, par2 + maxX, par3 + maxY, par4 + maxZ);
	}

	@Override
	public int getBlockColor() {
		return 2129968;
	}

	/**
	 * Returns the color this block should be rendered. Used by leaves.
	 */
	@Override
	public int getRenderColor(final int par1) {
		return 2129968;
	}

	/**
	 * Returns a integer with hex for 0xrrggbb with this color multiplied
	 * against the blocks color. Note only called when first determining what to
	 * render.
	 */
	@Override
	public int colorMultiplier(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		return 2129968;
	}

	/**
	 * Gets passed in the blockID of the block below and supposed to return true
	 * if its allowed to grow on the type of blockID passed in. Args: blockID
	 */
	@Override
	protected boolean canThisPlantGrowOnThisBlockID(final int par1) {
		return par1 == Block.waterStill.blockID;
	}

	/**
	 * Can this block stay at this position. Similar to canPlaceBlockAt except
	 * gets checked often with plants.
	 */
	@Override
	public boolean canBlockStay(final World par1World, final int par2,
			final int par3, final int par4) {
		return par3 >= 0 && par3 < 256 ? par1World.getBlockMaterial(par2,
				par3 - 1, par4) == Material.water
				&& par1World.getBlockMetadata(par2, par3 - 1, par4) == 0
				: false;
	}
}
