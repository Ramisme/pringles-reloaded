package net.minecraft.src;

public class SlotCrafting extends Slot {
	/** The craft matrix inventory linked to this result slot. */
	private final IInventory craftMatrix;

	/** The player that is using the GUI where this slot resides. */
	private final EntityPlayer thePlayer;

	/**
	 * The number of items that have been crafted so far. Gets passed to
	 * ItemStack.onCrafting before being reset.
	 */
	private int amountCrafted;

	public SlotCrafting(final EntityPlayer par1EntityPlayer,
			final IInventory par2IInventory, final IInventory par3IInventory,
			final int par4, final int par5, final int par6) {
		super(par3IInventory, par4, par5, par6);
		thePlayer = par1EntityPlayer;
		craftMatrix = par2IInventory;
	}

	/**
	 * Check if the stack is a valid item for this slot. Always true beside for
	 * the armor slots.
	 */
	@Override
	public boolean isItemValid(final ItemStack par1ItemStack) {
		return false;
	}

	/**
	 * Decrease the size of the stack in slot (first int arg) by the amount of
	 * the second int arg. Returns the new stack.
	 */
	@Override
	public ItemStack decrStackSize(final int par1) {
		if (getHasStack()) {
			amountCrafted += Math.min(par1, getStack().stackSize);
		}

		return super.decrStackSize(par1);
	}

	/**
	 * the itemStack passed in is the output - ie, iron ingots, and pickaxes,
	 * not ore and wood. Typically increases an internal count then calls
	 * onCrafting(item).
	 */
	@Override
	protected void onCrafting(final ItemStack par1ItemStack, final int par2) {
		amountCrafted += par2;
		this.onCrafting(par1ItemStack);
	}

	/**
	 * the itemStack passed in is the output - ie, iron ingots, and pickaxes,
	 * not ore and wood.
	 */
	@Override
	protected void onCrafting(final ItemStack par1ItemStack) {
		par1ItemStack.onCrafting(thePlayer.worldObj, thePlayer, amountCrafted);
		amountCrafted = 0;

		if (par1ItemStack.itemID == Block.workbench.blockID) {
			thePlayer.addStat(AchievementList.buildWorkBench, 1);
		} else if (par1ItemStack.itemID == Item.pickaxeWood.itemID) {
			thePlayer.addStat(AchievementList.buildPickaxe, 1);
		} else if (par1ItemStack.itemID == Block.furnaceIdle.blockID) {
			thePlayer.addStat(AchievementList.buildFurnace, 1);
		} else if (par1ItemStack.itemID == Item.hoeWood.itemID) {
			thePlayer.addStat(AchievementList.buildHoe, 1);
		} else if (par1ItemStack.itemID == Item.bread.itemID) {
			thePlayer.addStat(AchievementList.makeBread, 1);
		} else if (par1ItemStack.itemID == Item.cake.itemID) {
			thePlayer.addStat(AchievementList.bakeCake, 1);
		} else if (par1ItemStack.itemID == Item.pickaxeStone.itemID) {
			thePlayer.addStat(AchievementList.buildBetterPickaxe, 1);
		} else if (par1ItemStack.itemID == Item.swordWood.itemID) {
			thePlayer.addStat(AchievementList.buildSword, 1);
		} else if (par1ItemStack.itemID == Block.enchantmentTable.blockID) {
			thePlayer.addStat(AchievementList.enchantments, 1);
		} else if (par1ItemStack.itemID == Block.bookShelf.blockID) {
			thePlayer.addStat(AchievementList.bookcase, 1);
		}
	}

	@Override
	public void onPickupFromSlot(final EntityPlayer par1EntityPlayer,
			final ItemStack par2ItemStack) {
		this.onCrafting(par2ItemStack);

		for (int var3 = 0; var3 < craftMatrix.getSizeInventory(); ++var3) {
			final ItemStack var4 = craftMatrix.getStackInSlot(var3);

			if (var4 != null) {
				craftMatrix.decrStackSize(var3, 1);

				if (var4.getItem().hasContainerItem()) {
					final ItemStack var5 = new ItemStack(var4.getItem()
							.getContainerItem());

					if (!var4.getItem()
							.doesContainerItemLeaveCraftingGrid(var4)
							|| !thePlayer.inventory
									.addItemStackToInventory(var5)) {
						if (craftMatrix.getStackInSlot(var3) == null) {
							craftMatrix.setInventorySlotContents(var3, var5);
						} else {
							thePlayer.dropPlayerItem(var5);
						}
					}
				}
			}
		}
	}
}
