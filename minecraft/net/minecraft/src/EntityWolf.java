package net.minecraft.src;

public class EntityWolf extends EntityTameable {
	private float field_70926_e;
	private float field_70924_f;

	/** true is the wolf is wet else false */
	private boolean isShaking;
	private boolean field_70928_h;

	/**
	 * This time increases while wolf is shaking and emitting water particles.
	 */
	private float timeWolfIsShaking;
	private float prevTimeWolfIsShaking;

	public EntityWolf(final World par1World) {
		super(par1World);
		texture = "/mob/wolf.png";
		setSize(0.6F, 0.8F);
		moveSpeed = 0.3F;
		getNavigator().setAvoidsWater(true);
		tasks.addTask(1, new EntityAISwimming(this));
		tasks.addTask(2, aiSit);
		tasks.addTask(3, new EntityAILeapAtTarget(this, 0.4F));
		tasks.addTask(4, new EntityAIAttackOnCollide(this, moveSpeed, true));
		tasks.addTask(5, new EntityAIFollowOwner(this, moveSpeed, 10.0F, 2.0F));
		tasks.addTask(6, new EntityAIMate(this, moveSpeed));
		tasks.addTask(7, new EntityAIWander(this, moveSpeed));
		tasks.addTask(8, new EntityAIBeg(this, 8.0F));
		tasks.addTask(9, new EntityAIWatchClosest(this, EntityPlayer.class,
				8.0F));
		tasks.addTask(9, new EntityAILookIdle(this));
		targetTasks.addTask(1, new EntityAIOwnerHurtByTarget(this));
		targetTasks.addTask(2, new EntityAIOwnerHurtTarget(this));
		targetTasks.addTask(3, new EntityAIHurtByTarget(this, true));
		targetTasks.addTask(4, new EntityAITargetNonTamed(this,
				EntitySheep.class, 16.0F, 200, false));
	}

	/**
	 * Returns true if the newer Entity AI code should be run
	 */
	@Override
	public boolean isAIEnabled() {
		return true;
	}

	/**
	 * Sets the active target the Task system uses for tracking
	 */
	@Override
	public void setAttackTarget(final EntityLiving par1EntityLiving) {
		super.setAttackTarget(par1EntityLiving);

		if (par1EntityLiving instanceof EntityPlayer) {
			setAngry(true);
		}
	}

	/**
	 * main AI tick function, replaces updateEntityActionState
	 */
	@Override
	protected void updateAITick() {
		dataWatcher.updateObject(18, Integer.valueOf(getHealth()));
	}

	@Override
	public int getMaxHealth() {
		return isTamed() ? 20 : 8;
	}

	@Override
	protected void entityInit() {
		super.entityInit();
		dataWatcher.addObject(18, new Integer(getHealth()));
		dataWatcher.addObject(19, new Byte((byte) 0));
		dataWatcher.addObject(20,
				new Byte((byte) BlockCloth.getBlockFromDye(1)));
	}

	/**
	 * Plays step sound at given x, y, z for the entity
	 */
	@Override
	protected void playStepSound(final int par1, final int par2,
			final int par3, final int par4) {
		playSound("mob.wolf.step", 0.15F, 1.0F);
	}

	/**
	 * Returns the texture's file path as a String.
	 */
	@Override
	public String getTexture() {
		return isTamed() ? "/mob/wolf_tame.png"
				: isAngry() ? "/mob/wolf_angry.png" : super.getTexture();
	}

	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	@Override
	public void writeEntityToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeEntityToNBT(par1NBTTagCompound);
		par1NBTTagCompound.setBoolean("Angry", isAngry());
		par1NBTTagCompound.setByte("CollarColor", (byte) getCollarColor());
	}

	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	@Override
	public void readEntityFromNBT(final NBTTagCompound par1NBTTagCompound) {
		super.readEntityFromNBT(par1NBTTagCompound);
		setAngry(par1NBTTagCompound.getBoolean("Angry"));

		if (par1NBTTagCompound.hasKey("CollarColor")) {
			setCollarColor(par1NBTTagCompound.getByte("CollarColor"));
		}
	}

	/**
	 * Determines if an entity can be despawned, used on idle far away entities
	 */
	@Override
	protected boolean canDespawn() {
		return isAngry() && !isTamed();
	}

	/**
	 * Returns the sound this mob makes while it's alive.
	 */
	@Override
	protected String getLivingSound() {
		return isAngry() ? "mob.wolf.growl"
				: rand.nextInt(3) == 0 ? isTamed()
						&& dataWatcher.getWatchableObjectInt(18) < 10 ? "mob.wolf.whine"
						: "mob.wolf.panting"
						: "mob.wolf.bark";
	}

	/**
	 * Returns the sound this mob makes when it is hurt.
	 */
	@Override
	protected String getHurtSound() {
		return "mob.wolf.hurt";
	}

	/**
	 * Returns the sound this mob makes on death.
	 */
	@Override
	protected String getDeathSound() {
		return "mob.wolf.death";
	}

	/**
	 * Returns the volume for the sounds this mob makes.
	 */
	@Override
	protected float getSoundVolume() {
		return 0.4F;
	}

	/**
	 * Returns the item ID for the item the mob drops on death.
	 */
	@Override
	protected int getDropItemId() {
		return -1;
	}

	/**
	 * Called frequently so the entity can update its state every tick as
	 * required. For example, zombies and skeletons use this to react to
	 * sunlight and start to burn.
	 */
	@Override
	public void onLivingUpdate() {
		super.onLivingUpdate();

		if (!worldObj.isRemote && isShaking && !field_70928_h && !hasPath()
				&& onGround) {
			field_70928_h = true;
			timeWolfIsShaking = 0.0F;
			prevTimeWolfIsShaking = 0.0F;
			worldObj.setEntityState(this, (byte) 8);
		}
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate() {
		super.onUpdate();
		field_70924_f = field_70926_e;

		if (func_70922_bv()) {
			field_70926_e += (1.0F - field_70926_e) * 0.4F;
		} else {
			field_70926_e += (0.0F - field_70926_e) * 0.4F;
		}

		if (func_70922_bv()) {
			numTicksToChaseTarget = 10;
		}

		if (isWet()) {
			isShaking = true;
			field_70928_h = false;
			timeWolfIsShaking = 0.0F;
			prevTimeWolfIsShaking = 0.0F;
		} else if ((isShaking || field_70928_h) && field_70928_h) {
			if (timeWolfIsShaking == 0.0F) {
				playSound("mob.wolf.shake", getSoundVolume(),
						(rand.nextFloat() - rand.nextFloat()) * 0.2F + 1.0F);
			}

			prevTimeWolfIsShaking = timeWolfIsShaking;
			timeWolfIsShaking += 0.05F;

			if (prevTimeWolfIsShaking >= 2.0F) {
				isShaking = false;
				field_70928_h = false;
				prevTimeWolfIsShaking = 0.0F;
				timeWolfIsShaking = 0.0F;
			}

			if (timeWolfIsShaking > 0.4F) {
				final float var1 = (float) boundingBox.minY;
				final int var2 = (int) (MathHelper
						.sin((timeWolfIsShaking - 0.4F) * (float) Math.PI) * 7.0F);

				for (int var3 = 0; var3 < var2; ++var3) {
					final float var4 = (rand.nextFloat() * 2.0F - 1.0F) * width
							* 0.5F;
					final float var5 = (rand.nextFloat() * 2.0F - 1.0F) * width
							* 0.5F;
					worldObj.spawnParticle("splash", posX + var4, var1 + 0.8F,
							posZ + var5, motionX, motionY, motionZ);
				}
			}
		}
	}

	public boolean getWolfShaking() {
		return isShaking;
	}

	/**
	 * Used when calculating the amount of shading to apply while the wolf is
	 * shaking.
	 */
	public float getShadingWhileShaking(final float par1) {
		return 0.75F + (prevTimeWolfIsShaking + (timeWolfIsShaking - prevTimeWolfIsShaking)
				* par1) / 2.0F * 0.25F;
	}

	public float getShakeAngle(final float par1, final float par2) {
		float var3 = (prevTimeWolfIsShaking
				+ (timeWolfIsShaking - prevTimeWolfIsShaking) * par1 + par2) / 1.8F;

		if (var3 < 0.0F) {
			var3 = 0.0F;
		} else if (var3 > 1.0F) {
			var3 = 1.0F;
		}

		return MathHelper.sin(var3 * (float) Math.PI)
				* MathHelper.sin(var3 * (float) Math.PI * 11.0F) * 0.15F
				* (float) Math.PI;
	}

	public float getInterestedAngle(final float par1) {
		return (field_70924_f + (field_70926_e - field_70924_f) * par1) * 0.15F
				* (float) Math.PI;
	}

	@Override
	public float getEyeHeight() {
		return height * 0.8F;
	}

	/**
	 * The speed it takes to move the entityliving's rotationPitch through the
	 * faceEntity method. This is only currently use in wolves.
	 */
	@Override
	public int getVerticalFaceSpeed() {
		return isSitting() ? 20 : super.getVerticalFaceSpeed();
	}

	/**
	 * Called when the entity is attacked.
	 */
	@Override
	public boolean attackEntityFrom(final DamageSource par1DamageSource,
			int par2) {
		if (isEntityInvulnerable()) {
			return false;
		} else {
			final Entity var3 = par1DamageSource.getEntity();
			aiSit.setSitting(false);

			if (var3 != null && !(var3 instanceof EntityPlayer)
					&& !(var3 instanceof EntityArrow)) {
				par2 = (par2 + 1) / 2;
			}

			return super.attackEntityFrom(par1DamageSource, par2);
		}
	}

	@Override
	public boolean attackEntityAsMob(final Entity par1Entity) {
		final int var2 = isTamed() ? 4 : 2;
		return par1Entity.attackEntityFrom(DamageSource.causeMobDamage(this),
				var2);
	}

	/**
	 * Called when a player interacts with a mob. e.g. gets milk from a cow,
	 * gets into the saddle on a pig.
	 */
	@Override
	public boolean interact(final EntityPlayer par1EntityPlayer) {
		final ItemStack var2 = par1EntityPlayer.inventory.getCurrentItem();

		if (isTamed()) {
			if (var2 != null) {
				if (Item.itemsList[var2.itemID] instanceof ItemFood) {
					final ItemFood var3 = (ItemFood) Item.itemsList[var2.itemID];

					if (var3.isWolfsFavoriteMeat()
							&& dataWatcher.getWatchableObjectInt(18) < 20) {
						if (!par1EntityPlayer.capabilities.isCreativeMode) {
							--var2.stackSize;
						}

						heal(var3.getHealAmount());

						if (var2.stackSize <= 0) {
							par1EntityPlayer.inventory
									.setInventorySlotContents(
											par1EntityPlayer.inventory.currentItem,
											(ItemStack) null);
						}

						return true;
					}
				} else if (var2.itemID == Item.dyePowder.itemID) {
					final int var4 = BlockCloth.getBlockFromDye(var2
							.getItemDamage());

					if (var4 != getCollarColor()) {
						setCollarColor(var4);

						if (!par1EntityPlayer.capabilities.isCreativeMode
								&& --var2.stackSize <= 0) {
							par1EntityPlayer.inventory
									.setInventorySlotContents(
											par1EntityPlayer.inventory.currentItem,
											(ItemStack) null);
						}

						return true;
					}
				}
			}

			if (par1EntityPlayer.username.equalsIgnoreCase(getOwnerName())
					&& !worldObj.isRemote && !isBreedingItem(var2)) {
				aiSit.setSitting(!isSitting());
				isJumping = false;
				setPathToEntity((PathEntity) null);
			}
		} else if (var2 != null && var2.itemID == Item.bone.itemID
				&& !isAngry()) {
			if (!par1EntityPlayer.capabilities.isCreativeMode) {
				--var2.stackSize;
			}

			if (var2.stackSize <= 0) {
				par1EntityPlayer.inventory.setInventorySlotContents(
						par1EntityPlayer.inventory.currentItem,
						(ItemStack) null);
			}

			if (!worldObj.isRemote) {
				if (rand.nextInt(3) == 0) {
					setTamed(true);
					setPathToEntity((PathEntity) null);
					setAttackTarget((EntityLiving) null);
					aiSit.setSitting(true);
					setEntityHealth(20);
					setOwner(par1EntityPlayer.username);
					playTameEffect(true);
					worldObj.setEntityState(this, (byte) 7);
				} else {
					playTameEffect(false);
					worldObj.setEntityState(this, (byte) 6);
				}
			}

			return true;
		}

		return super.interact(par1EntityPlayer);
	}

	@Override
	public void handleHealthUpdate(final byte par1) {
		if (par1 == 8) {
			field_70928_h = true;
			timeWolfIsShaking = 0.0F;
			prevTimeWolfIsShaking = 0.0F;
		} else {
			super.handleHealthUpdate(par1);
		}
	}

	public float getTailRotation() {
		return isAngry() ? 1.5393804F : isTamed() ? (0.55F - (20 - dataWatcher
				.getWatchableObjectInt(18)) * 0.02F) * (float) Math.PI
				: (float) Math.PI / 5F;
	}

	/**
	 * Checks if the parameter is an item which this animal can be fed to breed
	 * it (wheat, carrots or seeds depending on the animal type)
	 */
	@Override
	public boolean isBreedingItem(final ItemStack par1ItemStack) {
		return par1ItemStack == null ? false
				: !(Item.itemsList[par1ItemStack.itemID] instanceof ItemFood) ? false
						: ((ItemFood) Item.itemsList[par1ItemStack.itemID])
								.isWolfsFavoriteMeat();
	}

	/**
	 * Will return how many at most can spawn in a chunk at once.
	 */
	@Override
	public int getMaxSpawnedInChunk() {
		return 8;
	}

	/**
	 * Determines whether this wolf is angry or not.
	 */
	public boolean isAngry() {
		return (dataWatcher.getWatchableObjectByte(16) & 2) != 0;
	}

	/**
	 * Sets whether this wolf is angry or not.
	 */
	public void setAngry(final boolean par1) {
		final byte var2 = dataWatcher.getWatchableObjectByte(16);

		if (par1) {
			dataWatcher.updateObject(16, Byte.valueOf((byte) (var2 | 2)));
		} else {
			dataWatcher.updateObject(16, Byte.valueOf((byte) (var2 & -3)));
		}
	}

	/**
	 * Return this wolf's collar color.
	 */
	public int getCollarColor() {
		return dataWatcher.getWatchableObjectByte(20) & 15;
	}

	/**
	 * Set this wolf's collar color.
	 */
	public void setCollarColor(final int par1) {
		dataWatcher.updateObject(20, Byte.valueOf((byte) (par1 & 15)));
	}

	/**
	 * This function is used when two same-species animals in 'love mode' breed
	 * to generate the new baby animal.
	 */
	public EntityWolf spawnBabyAnimal(final EntityAgeable par1EntityAgeable) {
		final EntityWolf var2 = new EntityWolf(worldObj);
		final String var3 = getOwnerName();

		if (var3 != null && var3.trim().length() > 0) {
			var2.setOwner(var3);
			var2.setTamed(true);
		}

		return var2;
	}

	public void func_70918_i(final boolean par1) {
		dataWatcher.getWatchableObjectByte(19);

		if (par1) {
			dataWatcher.updateObject(19, Byte.valueOf((byte) 1));
		} else {
			dataWatcher.updateObject(19, Byte.valueOf((byte) 0));
		}
	}

	/**
	 * Returns true if the mob is currently able to mate with the specified mob.
	 */
	@Override
	public boolean canMateWith(final EntityAnimal par1EntityAnimal) {
		if (par1EntityAnimal == this) {
			return false;
		} else if (!isTamed()) {
			return false;
		} else if (!(par1EntityAnimal instanceof EntityWolf)) {
			return false;
		} else {
			final EntityWolf var2 = (EntityWolf) par1EntityAnimal;
			return !var2.isTamed() ? false : var2.isSitting() ? false
					: isInLove() && var2.isInLove();
		}
	}

	public boolean func_70922_bv() {
		return dataWatcher.getWatchableObjectByte(19) == 1;
	}

	@Override
	public EntityAgeable createChild(final EntityAgeable par1EntityAgeable) {
		return spawnBabyAnimal(par1EntityAgeable);
	}
}
