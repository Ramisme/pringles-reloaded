package net.minecraft.src;

import java.util.List;
import java.util.Random;

public class ComponentVillageWell extends ComponentVillage {
	private int averageGroundLevel = -1;

	public ComponentVillageWell(
			final ComponentVillageStartPiece par1ComponentVillageStartPiece,
			final int par2, final Random par3Random, final int par4,
			final int par5) {
		super(par1ComponentVillageStartPiece, par2);
		coordBaseMode = par3Random.nextInt(4);

		switch (coordBaseMode) {
		case 0:
		case 2:
			boundingBox = new StructureBoundingBox(par4, 64, par5,
					par4 + 6 - 1, 78, par5 + 6 - 1);
			break;

		default:
			boundingBox = new StructureBoundingBox(par4, 64, par5,
					par4 + 6 - 1, 78, par5 + 6 - 1);
		}
	}

	/**
	 * Initiates construction of the Structure Component picked, at the current
	 * Location of StructGen
	 */
	@Override
	public void buildComponent(final StructureComponent par1StructureComponent,
			final List par2List, final Random par3Random) {
		StructureVillagePieces.getNextStructureComponentVillagePath(
				(ComponentVillageStartPiece) par1StructureComponent, par2List,
				par3Random, boundingBox.minX - 1, boundingBox.maxY - 4,
				boundingBox.minZ + 1, 1, getComponentType());
		StructureVillagePieces.getNextStructureComponentVillagePath(
				(ComponentVillageStartPiece) par1StructureComponent, par2List,
				par3Random, boundingBox.maxX + 1, boundingBox.maxY - 4,
				boundingBox.minZ + 1, 3, getComponentType());
		StructureVillagePieces.getNextStructureComponentVillagePath(
				(ComponentVillageStartPiece) par1StructureComponent, par2List,
				par3Random, boundingBox.minX + 1, boundingBox.maxY - 4,
				boundingBox.minZ - 1, 2, getComponentType());
		StructureVillagePieces.getNextStructureComponentVillagePath(
				(ComponentVillageStartPiece) par1StructureComponent, par2List,
				par3Random, boundingBox.minX + 1, boundingBox.maxY - 4,
				boundingBox.maxZ + 1, 0, getComponentType());
	}

	/**
	 * second Part of Structure generating, this for example places Spiderwebs,
	 * Mob Spawners, it closes Mineshafts at the end, it adds Fences...
	 */
	@Override
	public boolean addComponentParts(final World par1World,
			final Random par2Random,
			final StructureBoundingBox par3StructureBoundingBox) {
		if (averageGroundLevel < 0) {
			averageGroundLevel = getAverageGroundLevel(par1World,
					par3StructureBoundingBox);

			if (averageGroundLevel < 0) {
				return true;
			}

			boundingBox.offset(0, averageGroundLevel - boundingBox.maxY + 3, 0);
		}

		fillWithBlocks(par1World, par3StructureBoundingBox, 1, 0, 1, 4, 12, 4,
				Block.cobblestone.blockID, Block.waterMoving.blockID, false);
		placeBlockAtCurrentPosition(par1World, 0, 0, 2, 12, 2,
				par3StructureBoundingBox);
		placeBlockAtCurrentPosition(par1World, 0, 0, 3, 12, 2,
				par3StructureBoundingBox);
		placeBlockAtCurrentPosition(par1World, 0, 0, 2, 12, 3,
				par3StructureBoundingBox);
		placeBlockAtCurrentPosition(par1World, 0, 0, 3, 12, 3,
				par3StructureBoundingBox);
		placeBlockAtCurrentPosition(par1World, Block.fence.blockID, 0, 1, 13,
				1, par3StructureBoundingBox);
		placeBlockAtCurrentPosition(par1World, Block.fence.blockID, 0, 1, 14,
				1, par3StructureBoundingBox);
		placeBlockAtCurrentPosition(par1World, Block.fence.blockID, 0, 4, 13,
				1, par3StructureBoundingBox);
		placeBlockAtCurrentPosition(par1World, Block.fence.blockID, 0, 4, 14,
				1, par3StructureBoundingBox);
		placeBlockAtCurrentPosition(par1World, Block.fence.blockID, 0, 1, 13,
				4, par3StructureBoundingBox);
		placeBlockAtCurrentPosition(par1World, Block.fence.blockID, 0, 1, 14,
				4, par3StructureBoundingBox);
		placeBlockAtCurrentPosition(par1World, Block.fence.blockID, 0, 4, 13,
				4, par3StructureBoundingBox);
		placeBlockAtCurrentPosition(par1World, Block.fence.blockID, 0, 4, 14,
				4, par3StructureBoundingBox);
		fillWithBlocks(par1World, par3StructureBoundingBox, 1, 15, 1, 4, 15, 4,
				Block.cobblestone.blockID, Block.cobblestone.blockID, false);

		for (int var4 = 0; var4 <= 5; ++var4) {
			for (int var5 = 0; var5 <= 5; ++var5) {
				if (var5 == 0 || var5 == 5 || var4 == 0 || var4 == 5) {
					placeBlockAtCurrentPosition(par1World,
							Block.gravel.blockID, 0, var5, 11, var4,
							par3StructureBoundingBox);
					clearCurrentPositionBlocksUpwards(par1World, var5, 12,
							var4, par3StructureBoundingBox);
				}
			}
		}

		return true;
	}
}
