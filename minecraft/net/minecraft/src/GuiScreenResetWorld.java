package net.minecraft.src;

import net.minecraft.client.Minecraft;

import org.lwjgl.input.Keyboard;

public class GuiScreenResetWorld extends GuiScreen {
	private final GuiScreen field_96152_a;
	private final McoServer field_96150_b;
	private GuiTextField field_96151_c;
	private GuiButton field_96154_o;

	public GuiScreenResetWorld(final GuiScreen par1, final McoServer par2) {
		field_96152_a = par1;
		field_96150_b = par2;
	}

	/**
	 * Called from the main game loop to update the screen.
	 */
	@Override
	public void updateScreen() {
		field_96151_c.updateCursorCounter();
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui() {
		final StringTranslate var1 = StringTranslate.getInstance();
		Keyboard.enableRepeatEvents(true);
		buttonList.clear();
		buttonList.add(field_96154_o = new GuiButton(1, width / 2 - 100,
				height / 4 + 96 + 12, var1
						.translateKey("mco.configure.world.buttons.reset")));
		buttonList.add(new GuiButton(2, width / 2 - 100, height / 4 + 120 + 12,
				var1.translateKey("gui.cancel")));
		field_96151_c = new GuiTextField(fontRenderer, width / 2 - 100, 109,
				200, 20);
		field_96151_c.setFocused(true);
		field_96151_c.setMaxStringLength(32);
		field_96151_c.setText("");
	}

	/**
	 * Called when the screen is unloaded. Used to disable keyboard repeat
	 * events
	 */
	@Override
	public void onGuiClosed() {
		Keyboard.enableRepeatEvents(false);
	}

	/**
	 * Fired when a key is typed. This is the equivalent of
	 * KeyListener.keyTyped(KeyEvent e).
	 */
	@Override
	protected void keyTyped(final char par1, final int par2) {
		field_96151_c.textboxKeyTyped(par1, par2);

		if (par1 == 13) {
			actionPerformed(field_96154_o);
		}
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	@Override
	protected void actionPerformed(final GuiButton par1GuiButton) {
		if (par1GuiButton.enabled) {
			if (par1GuiButton.id == 2) {
				mc.displayGuiScreen(field_96152_a);
			} else if (par1GuiButton.id == 1) {
				final TaskResetWorld var2 = new TaskResetWorld(this,
						field_96150_b.field_96408_a, field_96151_c.getText());
				final GuiScreenLongRunningTask var3 = new GuiScreenLongRunningTask(
						mc, field_96152_a, var2);
				var3.func_98117_g();
				mc.displayGuiScreen(var3);
			}
		}
	}

	/**
	 * Called when the mouse is clicked.
	 */
	@Override
	protected void mouseClicked(final int par1, final int par2, final int par3) {
		super.mouseClicked(par1, par2, par3);
		field_96151_c.mouseClicked(par1, par2, par3);
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(final int par1, final int par2, final float par3) {
		final StringTranslate var4 = StringTranslate.getInstance();
		drawDefaultBackground();
		drawCenteredString(fontRenderer,
				var4.translateKey("mco.reset.world.title"), width / 2, 17,
				16777215);
		drawCenteredString(fontRenderer,
				var4.translateKey("mco.reset.world.warning"), width / 2, 66,
				16711680);
		drawString(fontRenderer, var4.translateKey("mco.reset.world.seed"),
				width / 2 - 100, 96, 10526880);
		field_96151_c.drawTextBox();
		super.drawScreen(par1, par2, par3);
	}

	static GuiScreen func_96148_a(
			final GuiScreenResetWorld par0GuiScreenResetWorld) {
		return par0GuiScreenResetWorld.field_96152_a;
	}

	static Minecraft func_96147_b(
			final GuiScreenResetWorld par0GuiScreenResetWorld) {
		return par0GuiScreenResetWorld.mc;
	}
}
