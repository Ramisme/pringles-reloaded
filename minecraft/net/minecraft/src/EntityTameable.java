package net.minecraft.src;

public abstract class EntityTameable extends EntityAnimal {
	protected EntityAISit aiSit = new EntityAISit(this);

	public EntityTameable(final World par1World) {
		super(par1World);
	}

	@Override
	protected void entityInit() {
		super.entityInit();
		dataWatcher.addObject(16, Byte.valueOf((byte) 0));
		dataWatcher.addObject(17, "");
	}

	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	@Override
	public void writeEntityToNBT(final NBTTagCompound par1NBTTagCompound) {
		super.writeEntityToNBT(par1NBTTagCompound);

		if (getOwnerName() == null) {
			par1NBTTagCompound.setString("Owner", "");
		} else {
			par1NBTTagCompound.setString("Owner", getOwnerName());
		}

		par1NBTTagCompound.setBoolean("Sitting", isSitting());
	}

	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	@Override
	public void readEntityFromNBT(final NBTTagCompound par1NBTTagCompound) {
		super.readEntityFromNBT(par1NBTTagCompound);
		final String var2 = par1NBTTagCompound.getString("Owner");

		if (var2.length() > 0) {
			setOwner(var2);
			setTamed(true);
		}

		aiSit.setSitting(par1NBTTagCompound.getBoolean("Sitting"));
		setSitting(par1NBTTagCompound.getBoolean("Sitting"));
	}

	/**
	 * Play the taming effect, will either be hearts or smoke depending on
	 * status
	 */
	protected void playTameEffect(final boolean par1) {
		String var2 = "heart";

		if (!par1) {
			var2 = "smoke";
		}

		for (int var3 = 0; var3 < 7; ++var3) {
			final double var4 = rand.nextGaussian() * 0.02D;
			final double var6 = rand.nextGaussian() * 0.02D;
			final double var8 = rand.nextGaussian() * 0.02D;
			worldObj.spawnParticle(var2, posX + rand.nextFloat() * width * 2.0F
					- width, posY + 0.5D + rand.nextFloat() * height, posZ
					+ rand.nextFloat() * width * 2.0F - width, var4, var6, var8);
		}
	}

	@Override
	public void handleHealthUpdate(final byte par1) {
		if (par1 == 7) {
			playTameEffect(true);
		} else if (par1 == 6) {
			playTameEffect(false);
		} else {
			super.handleHealthUpdate(par1);
		}
	}

	public boolean isTamed() {
		return (dataWatcher.getWatchableObjectByte(16) & 4) != 0;
	}

	public void setTamed(final boolean par1) {
		final byte var2 = dataWatcher.getWatchableObjectByte(16);

		if (par1) {
			dataWatcher.updateObject(16, Byte.valueOf((byte) (var2 | 4)));
		} else {
			dataWatcher.updateObject(16, Byte.valueOf((byte) (var2 & -5)));
		}
	}

	public boolean isSitting() {
		return (dataWatcher.getWatchableObjectByte(16) & 1) != 0;
	}

	public void setSitting(final boolean par1) {
		final byte var2 = dataWatcher.getWatchableObjectByte(16);

		if (par1) {
			dataWatcher.updateObject(16, Byte.valueOf((byte) (var2 | 1)));
		} else {
			dataWatcher.updateObject(16, Byte.valueOf((byte) (var2 & -2)));
		}
	}

	public String getOwnerName() {
		return dataWatcher.getWatchableObjectString(17);
	}

	public void setOwner(final String par1Str) {
		dataWatcher.updateObject(17, par1Str);
	}

	public EntityLiving getOwner() {
		return worldObj.getPlayerEntityByName(getOwnerName());
	}

	public EntityAISit func_70907_r() {
		return aiSit;
	}
}
