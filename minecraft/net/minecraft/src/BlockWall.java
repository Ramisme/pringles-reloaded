package net.minecraft.src;

import java.util.List;

public class BlockWall extends Block {
	/** The types of the wall. */
	public static final String[] types = new String[] { "normal", "mossy" };

	public BlockWall(final int par1, final Block par2Block) {
		super(par1, par2Block.blockMaterial);
		setHardness(par2Block.blockHardness);
		setResistance(par2Block.blockResistance / 3.0F);
		setStepSound(par2Block.stepSound);
		setCreativeTab(CreativeTabs.tabBlock);
	}

	/**
	 * From the specified side and block metadata retrieves the blocks texture.
	 * Args: side, metadata
	 */
	@Override
	public Icon getIcon(final int par1, final int par2) {
		return par2 == 1 ? Block.cobblestoneMossy.getBlockTextureFromSide(par1)
				: Block.cobblestone.getBlockTextureFromSide(par1);
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return 32;
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	@Override
	public boolean getBlocksMovement(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		return false;
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * Updates the blocks bounds based on its current state. Args: world, x, y,
	 * z
	 */
	@Override
	public void setBlockBoundsBasedOnState(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		final boolean var5 = canConnectWallTo(par1IBlockAccess, par2, par3,
				par4 - 1);
		final boolean var6 = canConnectWallTo(par1IBlockAccess, par2, par3,
				par4 + 1);
		final boolean var7 = canConnectWallTo(par1IBlockAccess, par2 - 1, par3,
				par4);
		final boolean var8 = canConnectWallTo(par1IBlockAccess, par2 + 1, par3,
				par4);
		float var9 = 0.25F;
		float var10 = 0.75F;
		float var11 = 0.25F;
		float var12 = 0.75F;
		float var13 = 1.0F;

		if (var5) {
			var11 = 0.0F;
		}

		if (var6) {
			var12 = 1.0F;
		}

		if (var7) {
			var9 = 0.0F;
		}

		if (var8) {
			var10 = 1.0F;
		}

		if (var5 && var6 && !var7 && !var8) {
			var13 = 0.8125F;
			var9 = 0.3125F;
			var10 = 0.6875F;
		} else if (!var5 && !var6 && var7 && var8) {
			var13 = 0.8125F;
			var11 = 0.3125F;
			var12 = 0.6875F;
		}

		setBlockBounds(var9, 0.0F, var11, var10, var13, var12);
	}

	/**
	 * Returns a bounding box from the pool of bounding boxes (this means this
	 * box can change after the pool has been cleared to be reused)
	 */
	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(final World par1World,
			final int par2, final int par3, final int par4) {
		setBlockBoundsBasedOnState(par1World, par2, par3, par4);
		maxY = 1.5D;
		return super.getCollisionBoundingBoxFromPool(par1World, par2, par3,
				par4);
	}

	/**
	 * Return whether an adjacent block can connect to a wall.
	 */
	public boolean canConnectWallTo(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		final int var5 = par1IBlockAccess.getBlockId(par2, par3, par4);

		if (var5 != blockID && var5 != Block.fenceGate.blockID) {
			final Block var6 = Block.blocksList[var5];
			return var6 != null && var6.blockMaterial.isOpaque()
					&& var6.renderAsNormalBlock() ? var6.blockMaterial != Material.pumpkin
					: false;
		} else {
			return true;
		}
	}

	/**
	 * returns a list of blocks with the same ID, but different meta (eg: wood
	 * returns 4 blocks)
	 */
	@Override
	public void getSubBlocks(final int par1,
			final CreativeTabs par2CreativeTabs, final List par3List) {
		par3List.add(new ItemStack(par1, 1, 0));
		par3List.add(new ItemStack(par1, 1, 1));
	}

	/**
	 * Determines the damage on the item the block drops. Used in cloth and
	 * wood.
	 */
	@Override
	public int damageDropped(final int par1) {
		return par1;
	}

	/**
	 * Returns true if the given side of this block type should be rendered, if
	 * the adjacent block is at the given coordinates. Args: blockAccess, x, y,
	 * z, side
	 */
	@Override
	public boolean shouldSideBeRendered(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		return par5 == 0 ? super.shouldSideBeRendered(par1IBlockAccess, par2,
				par3, par4, par5) : true;
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
	}
}
