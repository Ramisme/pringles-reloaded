package net.minecraft.src;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import net.minecraft.server.MinecraftServer;

public class BanEntry {
	public static final SimpleDateFormat dateFormat = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss Z");
	private final String username;
	private Date banStartDate = new Date();
	private String bannedBy = "(Unknown)";
	private Date banEndDate = null;
	private String reason = "Banned by an operator.";

	public BanEntry(final String par1Str) {
		username = par1Str;
	}

	public String getBannedUsername() {
		return username;
	}

	public Date getBanStartDate() {
		return banStartDate;
	}

	/**
	 * null == start ban now
	 */
	public void setBanStartDate(final Date par1Date) {
		banStartDate = par1Date != null ? par1Date : new Date();
	}

	public String getBannedBy() {
		return bannedBy;
	}

	public void setBannedBy(final String par1Str) {
		bannedBy = par1Str;
	}

	public Date getBanEndDate() {
		return banEndDate;
	}

	public void setBanEndDate(final Date par1Date) {
		banEndDate = par1Date;
	}

	public boolean hasBanExpired() {
		return banEndDate == null ? false : banEndDate.before(new Date());
	}

	public String getBanReason() {
		return reason;
	}

	public void setBanReason(final String par1Str) {
		reason = par1Str;
	}

	public String buildBanString() {
		final StringBuilder var1 = new StringBuilder();
		var1.append(getBannedUsername());
		var1.append("|");
		var1.append(BanEntry.dateFormat.format(getBanStartDate()));
		var1.append("|");
		var1.append(getBannedBy());
		var1.append("|");
		var1.append(getBanEndDate() == null ? "Forever" : BanEntry.dateFormat
				.format(getBanEndDate()));
		var1.append("|");
		var1.append(getBanReason());
		return var1.toString();
	}

	public static BanEntry parse(final String par0Str) {
		if (par0Str.trim().length() < 2) {
			return null;
		} else {
			final String[] var1 = par0Str.trim().split(Pattern.quote("|"), 5);
			final BanEntry var2 = new BanEntry(var1[0].trim());
			final byte var3 = 0;
			int var10000 = var1.length;
			int var7 = var3 + 1;

			if (var10000 <= var7) {
				return var2;
			} else {
				try {
					var2.setBanStartDate(BanEntry.dateFormat.parse(var1[var7]
							.trim()));
				} catch (final ParseException var6) {
					MinecraftServer
							.getServer()
							.getLogAgent()
							.logWarningException(
									"Could not read creation date format for ban entry \'"
											+ var2.getBannedUsername()
											+ "\' (was: \'" + var1[var7]
											+ "\')", var6);
				}

				var10000 = var1.length;
				++var7;

				if (var10000 <= var7) {
					return var2;
				} else {
					var2.setBannedBy(var1[var7].trim());
					var10000 = var1.length;
					++var7;

					if (var10000 <= var7) {
						return var2;
					} else {
						try {
							final String var4 = var1[var7].trim();

							if (!var4.equalsIgnoreCase("Forever")
									&& var4.length() > 0) {
								var2.setBanEndDate(BanEntry.dateFormat
										.parse(var4));
							}
						} catch (final ParseException var5) {
							MinecraftServer
									.getServer()
									.getLogAgent()
									.logWarningException(
											"Could not read expiry date format for ban entry \'"
													+ var2.getBannedUsername()
													+ "\' (was: \'"
													+ var1[var7] + "\')", var5);
						}

						var10000 = var1.length;
						++var7;

						if (var10000 <= var7) {
							return var2;
						} else {
							var2.setBanReason(var1[var7].trim());
							return var2;
						}
					}
				}
			}
		}
	}
}
