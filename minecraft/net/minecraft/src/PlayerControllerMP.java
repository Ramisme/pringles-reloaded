package net.minecraft.src;

import net.minecraft.client.Minecraft;

import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.events.block.BlockClickEvent;
import org.ramisme.pringles.events.block.BlockDamageEvent;
import org.ramisme.pringles.events.player.PlayerAttackEvent;

public class PlayerControllerMP {
	/** The Minecraft instance. */
	private final Minecraft mc;
	private final NetClientHandler netClientHandler;

	/** PosX of the current block being destroyed */
	private int currentBlockX = -1;

	/** PosY of the current block being destroyed */
	private int currentBlockY = -1;

	/** PosZ of the current block being destroyed */
	private int currentblockZ = -1;
	private ItemStack field_85183_f = null;

	/** Current block damage (MP) */
	private float curBlockDamageMP = 0.0F;

	/**
	 * Tick counter, when it hits 4 it resets back to 0 and plays the step sound
	 */
	private float stepSoundTickCounter = 0.0F;

	/**
	 * Delays the first damage on the block after the first click on the block
	 */
	private int blockHitDelay = 0;

	/** Tells if the player is hitting a block */
	private boolean isHittingBlock = false;

	/** Current game type for the player */
	private EnumGameType currentGameType;

	/** Index of the current item held by the player in the inventory hotbar */
	private int currentPlayerItem;

	public PlayerControllerMP(final Minecraft par1Minecraft,
			final NetClientHandler par2NetClientHandler) {
		currentGameType = EnumGameType.SURVIVAL;
		currentPlayerItem = 0;
		mc = par1Minecraft;
		netClientHandler = par2NetClientHandler;
	}

	/**
	 * Block dig operation in creative mode (instantly digs the block).
	 */
	public static void clickBlockCreative(final Minecraft par0Minecraft,
			final PlayerControllerMP par1PlayerControllerMP, final int par2,
			final int par3, final int par4, final int par5) {
		if (!par0Minecraft.theWorld.extinguishFire(par0Minecraft.thePlayer,
				par2, par3, par4, par5)) {
			par1PlayerControllerMP.onPlayerDestroyBlock(par2, par3, par4, par5);
		}
	}

	/**
	 * Sets player capabilities depending on current gametype. params: player
	 */
	public void setPlayerCapabilities(final EntityPlayer par1EntityPlayer) {
		currentGameType
				.configurePlayerCapabilities(par1EntityPlayer.capabilities);
	}

	/**
	 * If modified to return true, the player spins around slowly around (0,
	 * 68.5, 0). The GUI is disabled, the view is set to first person, and both
	 * chat and menu are disabled. Unless the server is modified to ignore
	 * illegal stances, attempting to enter a world at all will result in an
	 * immediate kick due to an illegal stance. Appears to be left-over debug,
	 * or demo code.
	 */
	public boolean enableEverythingIsScrewedUpMode() {
		return false;
	}

	/**
	 * Sets the game type for the player.
	 */
	public void setGameType(final EnumGameType par1EnumGameType) {
		currentGameType = par1EnumGameType;
		currentGameType.configurePlayerCapabilities(mc.thePlayer.capabilities);
	}

	/**
	 * Flips the player around. Args: player
	 */
	public void flipPlayer(final EntityPlayer par1EntityPlayer) {
		par1EntityPlayer.rotationYaw = -180.0F;
	}

	public boolean shouldDrawHUD() {
		return currentGameType.isSurvivalOrAdventure();
	}

	/**
	 * Called when a player completes the destruction of a block
	 */
	public boolean onPlayerDestroyBlock(final int par1, final int par2,
			final int par3, final int par4) {
		if (currentGameType.isAdventure()
				&& !mc.thePlayer.canCurrentToolHarvestBlock(par1, par2, par3)) {
			return false;
		} else {
			final WorldClient var5 = mc.theWorld;
			final Block var6 = Block.blocksList[var5.getBlockId(par1, par2,
					par3)];

			if (var6 == null) {
				return false;
			} else {
				var5.playAuxSFX(
						2001,
						par1,
						par2,
						par3,
						var6.blockID
								+ (var5.getBlockMetadata(par1, par2, par3) << 12));
				final int var7 = var5.getBlockMetadata(par1, par2, par3);
				final boolean var8 = var5.setBlockToAir(par1, par2, par3);

				if (var8) {
					var6.onBlockDestroyedByPlayer(var5, par1, par2, par3, var7);
				}

				currentBlockY = -1;

				if (!currentGameType.isCreative()) {
					final ItemStack var9 = mc.thePlayer
							.getCurrentEquippedItem();

					if (var9 != null) {
						var9.onBlockDestroyed(var5, var6.blockID, par1, par2,
								par3, mc.thePlayer);

						if (var9.stackSize == 0) {
							mc.thePlayer.destroyCurrentEquippedItem();
						}
					}
				}

				return var8;
			}
		}
	}

	/**
	 * Called by Minecraft class when the player is hitting a block with an
	 * item. Args: x, y, z, side
	 */
	public void clickBlock(final int par1, final int par2, final int par3,
			final int par4) {
		if (!currentGameType.isAdventure()
				|| mc.thePlayer.canCurrentToolHarvestBlock(par1, par2, par3)) {
			if (currentGameType.isCreative()) {
				// TODO: PlayerControllerMP#onPlayerDamageBlock
				final BlockClickEvent event = new BlockClickEvent(par1, par2,
						par3);
				Pringles.getInstance().getFactory().getEventManager()
						.sendEvent(event);
				if (event.isCancelled()) {
					return;
				}

				netClientHandler.addToSendQueue(new Packet14BlockDig(0, par1,
						par2, par3, par4));
				PlayerControllerMP.clickBlockCreative(mc, this, par1, par2,
						par3, par4);
				blockHitDelay = 5;
			} else if (!isHittingBlock || !sameToolAndBlock(par1, par2, par3)) {
				if (isHittingBlock) {
					netClientHandler.addToSendQueue(new Packet14BlockDig(1,
							currentBlockX, currentBlockY, currentblockZ, par4));
				}

				netClientHandler.addToSendQueue(new Packet14BlockDig(0, par1,
						par2, par3, par4));
				final int var5 = mc.theWorld.getBlockId(par1, par2, par3);

				if (var5 > 0 && curBlockDamageMP == 0.0F) {
					Block.blocksList[var5].onBlockClicked(mc.theWorld, par1,
							par2, par3, mc.thePlayer);
				}

				if (var5 > 0
						&& Block.blocksList[var5]
								.getPlayerRelativeBlockHardness(mc.thePlayer,
										mc.thePlayer.worldObj, par1, par2, par3) >= 1.0F) {
					onPlayerDestroyBlock(par1, par2, par3, par4);
				} else {
					isHittingBlock = true;
					currentBlockX = par1;
					currentBlockY = par2;
					currentblockZ = par3;
					field_85183_f = mc.thePlayer.getHeldItem();
					curBlockDamageMP = 0.0F;
					stepSoundTickCounter = 0.0F;
					mc.theWorld.destroyBlockInWorldPartially(
							mc.thePlayer.entityId, currentBlockX,
							currentBlockY, currentblockZ,
							(int) (curBlockDamageMP * 10.0F) - 1);
				}
			}
		}
	}

	/**
	 * Resets current block damage and isHittingBlock
	 */
	public void resetBlockRemoving() {
		if (isHittingBlock) {
			netClientHandler.addToSendQueue(new Packet14BlockDig(1,
					currentBlockX, currentBlockY, currentblockZ, -1));
		}

		isHittingBlock = false;
		curBlockDamageMP = 0.0F;
		mc.theWorld.destroyBlockInWorldPartially(mc.thePlayer.entityId,
				currentBlockX, currentBlockY, currentblockZ, -1);
	}

	/**
	 * Called when a player damages a block and updates damage counters
	 */
	public void onPlayerDamageBlock(final int par1, final int par2,
			final int par3, final int par4) {
		syncCurrentPlayItem();

		// TODO: PlayerControllerMP#onPlayerDamageBlock
		final BlockDamageEvent event = new BlockDamageEvent(par1, par2, par3);
		Pringles.getInstance().getFactory().getEventManager().sendEvent(event);
		if (event.isCancelled()) {
			return;
		}

		if (blockHitDelay > 0) {
			--blockHitDelay;
		} else if (currentGameType.isCreative()) {
			blockHitDelay = 5;
			netClientHandler.addToSendQueue(new Packet14BlockDig(0, par1, par2,
					par3, par4));
			PlayerControllerMP.clickBlockCreative(mc, this, par1, par2, par3,
					par4);
		} else {
			if (sameToolAndBlock(par1, par2, par3)) {
				final int var5 = mc.theWorld.getBlockId(par1, par2, par3);

				if (var5 == 0) {
					isHittingBlock = false;
					return;
				}

				final Block var6 = Block.blocksList[var5];
				curBlockDamageMP += var6.getPlayerRelativeBlockHardness(
						mc.thePlayer, mc.thePlayer.worldObj, par1, par2, par3)
						* event.getDamageModifier();

				if (stepSoundTickCounter % 4.0F == 0.0F && var6 != null) {
					mc.sndManager.playSound(var6.stepSound.getStepSound(),
							par1 + 0.5F, par2 + 0.5F, par3 + 0.5F,
							(var6.stepSound.getVolume() + 1.0F) / 8.0F,
							var6.stepSound.getPitch() * 0.5F);
				}

				++stepSoundTickCounter;

				if (curBlockDamageMP >= 1.0F) {
					isHittingBlock = false;
					netClientHandler.addToSendQueue(new Packet14BlockDig(2,
							par1, par2, par3, par4));
					onPlayerDestroyBlock(par1, par2, par3, par4);
					curBlockDamageMP = 0.0F;
					stepSoundTickCounter = 0.0F;
					blockHitDelay = event.getHitDelay();
				}

				mc.theWorld.destroyBlockInWorldPartially(mc.thePlayer.entityId,
						currentBlockX, currentBlockY, currentblockZ,
						(int) (curBlockDamageMP * 10.0F) - 1);
			} else {
				clickBlock(par1, par2, par3, par4);
			}
		}
	}

	/**
	 * player reach distance = 4F
	 */
	public float getBlockReachDistance() {
		return currentGameType.isCreative() ? 5.0F : 4.5F;
	}

	public void updateController() {
		syncCurrentPlayItem();
		mc.sndManager.playRandomMusicIfReady();
	}

	private boolean sameToolAndBlock(final int par1, final int par2,
			final int par3) {
		final ItemStack var4 = mc.thePlayer.getHeldItem();
		boolean var5 = field_85183_f == null && var4 == null;

		if (field_85183_f != null && var4 != null) {
			var5 = var4.itemID == field_85183_f.itemID
					&& ItemStack.areItemStackTagsEqual(var4, field_85183_f)
					&& (var4.isItemStackDamageable() || var4.getItemDamage() == field_85183_f
							.getItemDamage());
		}

		return par1 == currentBlockX && par2 == currentBlockY
				&& par3 == currentblockZ && var5;
	}

	/**
	 * Syncs the current player item with the server
	 */
	private void syncCurrentPlayItem() {
		final int var1 = mc.thePlayer.inventory.currentItem;

		if (var1 != currentPlayerItem) {
			currentPlayerItem = var1;
			netClientHandler.addToSendQueue(new Packet16BlockItemSwitch(
					currentPlayerItem));
		}
	}

	/**
	 * Handles a players right click. Args: player, world, x, y, z, side, hitVec
	 */
	public boolean onPlayerRightClick(final EntityPlayer par1EntityPlayer,
			final World par2World, final ItemStack par3ItemStack,
			final int par4, final int par5, final int par6, final int par7,
			final Vec3 par8Vec3) {
		syncCurrentPlayItem();
		final float var9 = (float) par8Vec3.xCoord - par4;
		final float var10 = (float) par8Vec3.yCoord - par5;
		final float var11 = (float) par8Vec3.zCoord - par6;
		boolean var12 = false;
		int var13;

		if (!par1EntityPlayer.isSneaking()
				|| par1EntityPlayer.getHeldItem() == null) {
			var13 = par2World.getBlockId(par4, par5, par6);

			if (var13 > 0
					&& Block.blocksList[var13].onBlockActivated(par2World,
							par4, par5, par6, par1EntityPlayer, par7, var9,
							var10, var11)) {
				var12 = true;
			}
		}

		if (!var12 && par3ItemStack != null
				&& par3ItemStack.getItem() instanceof ItemBlock) {
			final ItemBlock var16 = (ItemBlock) par3ItemStack.getItem();

			if (!var16.canPlaceItemBlockOnSide(par2World, par4, par5, par6,
					par7, par1EntityPlayer, par3ItemStack)) {
				return false;
			}
		}

		netClientHandler.addToSendQueue(new Packet15Place(par4, par5, par6,
				par7, par1EntityPlayer.inventory.getCurrentItem(), var9, var10,
				var11));

		if (var12) {
			return true;
		} else if (par3ItemStack == null) {
			return false;
		} else if (currentGameType.isCreative()) {
			var13 = par3ItemStack.getItemDamage();
			final int var14 = par3ItemStack.stackSize;
			final boolean var15 = par3ItemStack.tryPlaceItemIntoWorld(
					par1EntityPlayer, par2World, par4, par5, par6, par7, var9,
					var10, var11);
			par3ItemStack.setItemDamage(var13);
			par3ItemStack.stackSize = var14;
			return var15;
		} else {
			return par3ItemStack.tryPlaceItemIntoWorld(par1EntityPlayer,
					par2World, par4, par5, par6, par7, var9, var10, var11);
		}
	}

	/**
	 * Notifies the server of things like consuming food, etc...
	 */
	public boolean sendUseItem(final EntityPlayer par1EntityPlayer,
			final World par2World, final ItemStack par3ItemStack) {
		syncCurrentPlayItem();
		netClientHandler.addToSendQueue(new Packet15Place(-1, -1, -1, 255,
				par1EntityPlayer.inventory.getCurrentItem(), 0.0F, 0.0F, 0.0F));
		final int var4 = par3ItemStack.stackSize;
		final ItemStack var5 = par3ItemStack.useItemRightClick(par2World,
				par1EntityPlayer);

		if (var5 == par3ItemStack && (var5 == null || var5.stackSize == var4)) {
			return false;
		} else {
			par1EntityPlayer.inventory.mainInventory[par1EntityPlayer.inventory.currentItem] = var5;

			if (var5.stackSize == 0) {
				par1EntityPlayer.inventory.mainInventory[par1EntityPlayer.inventory.currentItem] = null;
			}

			return true;
		}
	}

	public EntityClientPlayerMP func_78754_a(final World par1World) {
		return new EntityClientPlayerMP(mc, par1World, mc.session,
				netClientHandler);
	}

	/**
	 * Attacks an entity
	 */
	public void attackEntity(final EntityPlayer par1EntityPlayer,
			final Entity par2Entity) {
		// TODO: PlayerControllerMP#attackEntity
		final PlayerAttackEvent event = new PlayerAttackEvent(par2Entity);
		Pringles.getInstance().getFactory().getEventManager().sendEvent(event);
		if (event.isCancelled()) {
			return;
		}

		syncCurrentPlayItem();
		netClientHandler.addToSendQueue(new Packet7UseEntity(
				par1EntityPlayer.entityId, par2Entity.entityId, 1));
		par1EntityPlayer.attackTargetEntityWithCurrentItem(par2Entity);
	}

	public boolean func_78768_b(final EntityPlayer par1EntityPlayer,
			final Entity par2Entity) {
		syncCurrentPlayItem();
		netClientHandler.addToSendQueue(new Packet7UseEntity(
				par1EntityPlayer.entityId, par2Entity.entityId, 0));
		return par1EntityPlayer.interactWith(par2Entity);
	}

	public ItemStack windowClick(final int par1, final int par2,
			final int par3, final int par4, final EntityPlayer par5EntityPlayer) {
		final short var6 = par5EntityPlayer.openContainer
				.getNextTransactionID(par5EntityPlayer.inventory);
		final ItemStack var7 = par5EntityPlayer.openContainer.slotClick(par2,
				par3, par4, par5EntityPlayer);
		netClientHandler.addToSendQueue(new Packet102WindowClick(par1, par2,
				par3, par4, var7, var6));
		return var7;
	}

	/**
	 * GuiEnchantment uses this during multiplayer to tell PlayerControllerMP to
	 * send a packet indicating the enchantment action the player has taken.
	 */
	public void sendEnchantPacket(final int par1, final int par2) {
		netClientHandler.addToSendQueue(new Packet108EnchantItem(par1, par2));
	}

	/**
	 * Used in PlayerControllerMP to update the server with an ItemStack in a
	 * slot.
	 */
	public void sendSlotPacket(final ItemStack par1ItemStack, final int par2) {
		if (currentGameType.isCreative()) {
			netClientHandler.addToSendQueue(new Packet107CreativeSetSlot(par2,
					par1ItemStack));
		}
	}

	public void func_78752_a(final ItemStack par1ItemStack) {
		if (currentGameType.isCreative() && par1ItemStack != null) {
			netClientHandler.addToSendQueue(new Packet107CreativeSetSlot(-1,
					par1ItemStack));
		}
	}

	public void onStoppedUsingItem(final EntityPlayer par1EntityPlayer) {
		syncCurrentPlayItem();
		netClientHandler.addToSendQueue(new Packet14BlockDig(5, 0, 0, 0, 255));
		par1EntityPlayer.stopUsingItem();
	}

	public boolean func_78763_f() {
		return true;
	}

	/**
	 * Checks if the player is not creative, used for checking if it should
	 * break a block instantly
	 */
	public boolean isNotCreative() {
		return !currentGameType.isCreative();
	}

	/**
	 * returns true if player is in creative mode
	 */
	public boolean isInCreativeMode() {
		return currentGameType.isCreative();
	}

	/**
	 * true for hitting entities far away.
	 */
	public boolean extendedReach() {
		return currentGameType.isCreative();
	}
}
