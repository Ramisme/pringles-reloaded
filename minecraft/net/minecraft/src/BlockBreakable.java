package net.minecraft.src;

public class BlockBreakable extends Block {
	private final boolean localFlag;
	private final String breakableBlockIcon;

	protected BlockBreakable(final int par1, final String par2Str,
			final Material par3Material, final boolean par4) {
		super(par1, par3Material);
		localFlag = par4;
		breakableBlockIcon = par2Str;
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * Returns true if the given side of this block type should be rendered, if
	 * the adjacent block is at the given coordinates. Args: blockAccess, x, y,
	 * z, side
	 */
	@Override
	public boolean shouldSideBeRendered(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		final int var6 = par1IBlockAccess.getBlockId(par2, par3, par4);
		return !localFlag && var6 == blockID ? false : super
				.shouldSideBeRendered(par1IBlockAccess, par2, par3, par4, par5);
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		blockIcon = par1IconRegister.registerIcon(breakableBlockIcon);
	}
}
