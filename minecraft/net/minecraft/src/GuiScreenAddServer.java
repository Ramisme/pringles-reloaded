package net.minecraft.src;

import org.lwjgl.input.Keyboard;

public class GuiScreenAddServer extends GuiScreen {
	/** This GUI's parent GUI. */
	private final GuiScreen parentGui;
	private GuiTextField serverAddress;
	private GuiTextField serverName;

	/** ServerData to be modified by this GUI */
	private final ServerData newServerData;

	public GuiScreenAddServer(final GuiScreen par1GuiScreen,
			final ServerData par2ServerData) {
		parentGui = par1GuiScreen;
		newServerData = par2ServerData;
	}

	/**
	 * Called from the main game loop to update the screen.
	 */
	@Override
	public void updateScreen() {
		serverName.updateCursorCounter();
		serverAddress.updateCursorCounter();
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui() {
		final StringTranslate var1 = StringTranslate.getInstance();
		Keyboard.enableRepeatEvents(true);
		buttonList.clear();
		buttonList.add(new GuiButton(0, width / 2 - 100, height / 4 + 96 + 12,
				var1.translateKey("addServer.add")));
		buttonList.add(new GuiButton(1, width / 2 - 100, height / 4 + 120 + 12,
				var1.translateKey("gui.cancel")));
		buttonList
				.add(new GuiButton(2, width / 2 - 100, 142, var1
						.translateKey("addServer.hideAddress")
						+ ": "
						+ (newServerData.isHidingAddress() ? var1
								.translateKey("gui.yes") : var1
								.translateKey("gui.no"))));
		serverName = new GuiTextField(fontRenderer, width / 2 - 100, 66, 200,
				20);
		serverName.setFocused(true);
		serverName.setText(newServerData.serverName);
		serverAddress = new GuiTextField(fontRenderer, width / 2 - 100, 106,
				200, 20);
		serverAddress.setMaxStringLength(128);
		serverAddress.setText(newServerData.serverIP);
		((GuiButton) buttonList.get(0)).enabled = serverAddress.getText()
				.length() > 0
				&& serverAddress.getText().split(":").length > 0
				&& serverName.getText().length() > 0;
	}

	/**
	 * Called when the screen is unloaded. Used to disable keyboard repeat
	 * events
	 */
	@Override
	public void onGuiClosed() {
		Keyboard.enableRepeatEvents(false);
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	@Override
	protected void actionPerformed(final GuiButton par1GuiButton) {
		if (par1GuiButton.enabled) {
			if (par1GuiButton.id == 1) {
				parentGui.confirmClicked(false, 0);
			} else if (par1GuiButton.id == 0) {
				newServerData.serverName = serverName.getText();
				newServerData.serverIP = serverAddress.getText();
				parentGui.confirmClicked(true, 0);
			} else if (par1GuiButton.id == 2) {
				final StringTranslate var2 = StringTranslate.getInstance();
				newServerData.setHideAddress(!newServerData.isHidingAddress());
				((GuiButton) buttonList.get(2)).displayString = var2
						.translateKey("addServer.hideAddress")
						+ ": "
						+ (newServerData.isHidingAddress() ? var2
								.translateKey("gui.yes") : var2
								.translateKey("gui.no"));
			}
		}
	}

	/**
	 * Fired when a key is typed. This is the equivalent of
	 * KeyListener.keyTyped(KeyEvent e).
	 */
	@Override
	protected void keyTyped(final char par1, final int par2) {
		serverName.textboxKeyTyped(par1, par2);
		serverAddress.textboxKeyTyped(par1, par2);

		if (par1 == 9) {
			if (serverName.isFocused()) {
				serverName.setFocused(false);
				serverAddress.setFocused(true);
			} else {
				serverName.setFocused(true);
				serverAddress.setFocused(false);
			}
		}

		if (par1 == 13) {
			actionPerformed((GuiButton) buttonList.get(0));
		}

		((GuiButton) buttonList.get(0)).enabled = serverAddress.getText()
				.length() > 0
				&& serverAddress.getText().split(":").length > 0
				&& serverName.getText().length() > 0;
	}

	/**
	 * Called when the mouse is clicked.
	 */
	@Override
	protected void mouseClicked(final int par1, final int par2, final int par3) {
		super.mouseClicked(par1, par2, par3);
		serverAddress.mouseClicked(par1, par2, par3);
		serverName.mouseClicked(par1, par2, par3);
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(final int par1, final int par2, final float par3) {
		final StringTranslate var4 = StringTranslate.getInstance();
		drawDefaultBackground();
		drawCenteredString(fontRenderer, var4.translateKey("addServer.title"),
				width / 2, 17, 16777215);
		drawString(fontRenderer, var4.translateKey("addServer.enterName"),
				width / 2 - 100, 53, 10526880);
		drawString(fontRenderer, var4.translateKey("addServer.enterIp"),
				width / 2 - 100, 94, 10526880);
		serverName.drawTextBox();
		serverAddress.drawTextBox();
		super.drawScreen(par1, par2, par3);
	}
}
