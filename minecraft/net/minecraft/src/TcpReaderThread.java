package net.minecraft.src;

class TcpReaderThread extends Thread {
	final TcpConnection theTcpConnection;

	TcpReaderThread(final TcpConnection par1TcpConnection, final String par2Str) {
		super(par2Str);
		theTcpConnection = par1TcpConnection;
	}

	@Override
	public void run() {
		TcpConnection.field_74471_a.getAndIncrement();

		try {
			while (TcpConnection.isRunning(theTcpConnection)
					&& !TcpConnection.isServerTerminating(theTcpConnection)) {
				if (!TcpConnection.readNetworkPacket(theTcpConnection)) {
					try {
						Thread.sleep(2L);
					} catch (final InterruptedException var5) {
						;
					}
				}
			}
		} finally {
			TcpConnection.field_74471_a.getAndDecrement();
		}
	}
}
