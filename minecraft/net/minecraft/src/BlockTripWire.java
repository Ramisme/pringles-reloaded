package net.minecraft.src;

import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class BlockTripWire extends Block {
	public BlockTripWire(final int par1) {
		super(par1, Material.circuits);
		setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.15625F, 1.0F);
		setTickRandomly(true);
	}

	/**
	 * How many world ticks before ticking
	 */
	@Override
	public int tickRate(final World par1World) {
		return 10;
	}

	/**
	 * Returns a bounding box from the pool of bounding boxes (this means this
	 * box can change after the pool has been cleared to be reused)
	 */
	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(final World par1World,
			final int par2, final int par3, final int par4) {
		return null;
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	/**
	 * Returns which pass should this block be rendered on. 0 for solids and 1
	 * for alpha
	 */
	@Override
	public int getRenderBlockPass() {
		return 1;
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return 30;
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return Item.silk.itemID;
	}

	/**
	 * only called by clickMiddleMouseButton , and passed to
	 * inventory.setCurrentItem (along with isCreative)
	 */
	@Override
	public int idPicked(final World par1World, final int par2, final int par3,
			final int par4) {
		return Item.silk.itemID;
	}

	/**
	 * Lets the block know when one of its neighbor changes. Doesn't know which
	 * neighbor changed (coordinates passed are their own) Args: x, y, z,
	 * neighbor blockID
	 */
	@Override
	public void onNeighborBlockChange(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		final int var6 = par1World.getBlockMetadata(par2, par3, par4);
		final boolean var7 = (var6 & 2) == 2;
		final boolean var8 = !par1World.doesBlockHaveSolidTopSurface(par2,
				par3 - 1, par4);

		if (var7 != var8) {
			dropBlockAsItem(par1World, par2, par3, par4, var6, 0);
			par1World.setBlockToAir(par2, par3, par4);
		}
	}

	/**
	 * Updates the blocks bounds based on its current state. Args: world, x, y,
	 * z
	 */
	@Override
	public void setBlockBoundsBasedOnState(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		final int var5 = par1IBlockAccess.getBlockMetadata(par2, par3, par4);
		final boolean var6 = (var5 & 4) == 4;
		final boolean var7 = (var5 & 2) == 2;

		if (!var7) {
			setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.09375F, 1.0F);
		} else if (!var6) {
			setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.5F, 1.0F);
		} else {
			setBlockBounds(0.0F, 0.0625F, 0.0F, 1.0F, 0.15625F, 1.0F);
		}
	}

	/**
	 * Called whenever the block is added into the world. Args: world, x, y, z
	 */
	@Override
	public void onBlockAdded(final World par1World, final int par2,
			final int par3, final int par4) {
		final int var5 = par1World.doesBlockHaveSolidTopSurface(par2, par3 - 1,
				par4) ? 0 : 2;
		par1World.setBlockMetadataWithNotify(par2, par3, par4, var5, 3);
		func_72149_e(par1World, par2, par3, par4, var5);
	}

	/**
	 * ejects contained items into the world, and notifies neighbours of an
	 * update, as appropriate
	 */
	@Override
	public void breakBlock(final World par1World, final int par2,
			final int par3, final int par4, final int par5, final int par6) {
		func_72149_e(par1World, par2, par3, par4, par6 | 1);
	}

	/**
	 * Called when the block is attempted to be harvested
	 */
	@Override
	public void onBlockHarvested(final World par1World, final int par2,
			final int par3, final int par4, final int par5,
			final EntityPlayer par6EntityPlayer) {
		if (!par1World.isRemote) {
			if (par6EntityPlayer.getCurrentEquippedItem() != null
					&& par6EntityPlayer.getCurrentEquippedItem().itemID == Item.shears.itemID) {
				par1World.setBlockMetadataWithNotify(par2, par3, par4,
						par5 | 8, 4);
			}
		}
	}

	private void func_72149_e(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		int var6 = 0;

		while (var6 < 2) {
			int var7 = 1;

			while (true) {
				if (var7 < 42) {
					final int var8 = par2 + Direction.offsetX[var6] * var7;
					final int var9 = par4 + Direction.offsetZ[var6] * var7;
					final int var10 = par1World.getBlockId(var8, par3, var9);

					if (var10 == Block.tripWireSource.blockID) {
						final int var11 = par1World.getBlockMetadata(var8,
								par3, var9) & 3;

						if (var11 == Direction.rotateOpposite[var6]) {
							Block.tripWireSource
									.func_72143_a(par1World, var8, par3, var9,
											var10, par1World.getBlockMetadata(
													var8, par3, var9), true,
											var7, par5);
						}
					} else if (var10 == Block.tripWire.blockID) {
						++var7;
						continue;
					}
				}

				++var6;
				break;
			}
		}
	}

	/**
	 * Triggered whenever an entity collides with this block (enters into the
	 * block). Args: world, x, y, z, entity
	 */
	@Override
	public void onEntityCollidedWithBlock(final World par1World,
			final int par2, final int par3, final int par4,
			final Entity par5Entity) {
		if (!par1World.isRemote) {
			if ((par1World.getBlockMetadata(par2, par3, par4) & 1) != 1) {
				updateTripWireState(par1World, par2, par3, par4);
			}
		}
	}

	/**
	 * Ticks the block if it's been scheduled
	 */
	@Override
	public void updateTick(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		if (!par1World.isRemote) {
			if ((par1World.getBlockMetadata(par2, par3, par4) & 1) == 1) {
				updateTripWireState(par1World, par2, par3, par4);
			}
		}
	}

	private void updateTripWireState(final World par1World, final int par2,
			final int par3, final int par4) {
		int var5 = par1World.getBlockMetadata(par2, par3, par4);
		final boolean var6 = (var5 & 1) == 1;
		boolean var7 = false;
		final List var8 = par1World.getEntitiesWithinAABBExcludingEntity(
				(Entity) null,
				AxisAlignedBB.getAABBPool().getAABB(par2 + minX, par3 + minY,
						par4 + minZ, par2 + maxX, par3 + maxY, par4 + maxZ));

		if (!var8.isEmpty()) {
			final Iterator var9 = var8.iterator();

			while (var9.hasNext()) {
				final Entity var10 = (Entity) var9.next();

				if (!var10.doesEntityNotTriggerPressurePlate()) {
					var7 = true;
					break;
				}
			}
		}

		if (var7 && !var6) {
			var5 |= 1;
		}

		if (!var7 && var6) {
			var5 &= -2;
		}

		if (var7 != var6) {
			par1World.setBlockMetadataWithNotify(par2, par3, par4, var5, 3);
			func_72149_e(par1World, par2, par3, par4, var5);
		}

		if (var7) {
			par1World.scheduleBlockUpdate(par2, par3, par4, blockID,
					tickRate(par1World));
		}
	}

	public static boolean func_72148_a(final IBlockAccess par0IBlockAccess,
			final int par1, final int par2, final int par3, final int par4,
			final int par5) {
		final int var6 = par1 + Direction.offsetX[par5];
		final int var8 = par3 + Direction.offsetZ[par5];
		final int var9 = par0IBlockAccess.getBlockId(var6, par2, var8);
		final boolean var10 = (par4 & 2) == 2;
		int var11;

		if (var9 == Block.tripWireSource.blockID) {
			var11 = par0IBlockAccess.getBlockMetadata(var6, par2, var8);
			final int var13 = var11 & 3;
			return var13 == Direction.rotateOpposite[par5];
		} else if (var9 == Block.tripWire.blockID) {
			var11 = par0IBlockAccess.getBlockMetadata(var6, par2, var8);
			final boolean var12 = (var11 & 2) == 2;
			return var10 == var12;
		} else {
			return false;
		}
	}
}
