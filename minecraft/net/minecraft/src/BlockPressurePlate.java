package net.minecraft.src;

import java.util.Iterator;
import java.util.List;

public class BlockPressurePlate extends BlockBasePressurePlate {
	/** The mob type that can trigger this pressure plate. */
	private final EnumMobType triggerMobType;

	protected BlockPressurePlate(final int par1, final String par2Str,
			final Material par3Material, final EnumMobType par4EnumMobType) {
		super(par1, par2Str, par3Material);
		triggerMobType = par4EnumMobType;
	}

	/**
	 * Argument is weight (0-15). Return the metadata to be set because of it.
	 */
	@Override
	protected int getMetaFromWeight(final int par1) {
		return par1 > 0 ? 1 : 0;
	}

	/**
	 * Argument is metadata. Returns power level (0-15)
	 */
	@Override
	protected int getPowerSupply(final int par1) {
		return par1 == 1 ? 15 : 0;
	}

	/**
	 * Returns the current state of the pressure plate. Returns a value between
	 * 0 and 15 based on the number of items on it.
	 */
	@Override
	protected int getPlateState(final World par1World, final int par2,
			final int par3, final int par4) {
		List var5 = null;

		if (triggerMobType == EnumMobType.everything) {
			var5 = par1World.getEntitiesWithinAABBExcludingEntity(
					(Entity) null, getSensitiveAABB(par2, par3, par4));
		}

		if (triggerMobType == EnumMobType.mobs) {
			var5 = par1World.getEntitiesWithinAABB(EntityLiving.class,
					getSensitiveAABB(par2, par3, par4));
		}

		if (triggerMobType == EnumMobType.players) {
			var5 = par1World.getEntitiesWithinAABB(EntityPlayer.class,
					getSensitiveAABB(par2, par3, par4));
		}

		if (!var5.isEmpty()) {
			final Iterator var6 = var5.iterator();

			while (var6.hasNext()) {
				final Entity var7 = (Entity) var6.next();

				if (!var7.doesEntityNotTriggerPressurePlate()) {
					return 15;
				}
			}
		}

		return 0;
	}
}
