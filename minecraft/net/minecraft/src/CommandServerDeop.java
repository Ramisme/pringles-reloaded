package net.minecraft.src;

import java.util.List;

import net.minecraft.server.MinecraftServer;

public class CommandServerDeop extends CommandBase {
	@Override
	public String getCommandName() {
		return "deop";
	}

	/**
	 * Return the required permission level for this command.
	 */
	@Override
	public int getRequiredPermissionLevel() {
		return 3;
	}

	@Override
	public String getCommandUsage(final ICommandSender par1ICommandSender) {
		return par1ICommandSender.translateString("commands.deop.usage",
				new Object[0]);
	}

	@Override
	public void processCommand(final ICommandSender par1ICommandSender,
			final String[] par2ArrayOfStr) {
		if (par2ArrayOfStr.length == 1 && par2ArrayOfStr[0].length() > 0) {
			MinecraftServer.getServer().getConfigurationManager()
					.removeOp(par2ArrayOfStr[0]);
			CommandBase
					.notifyAdmins(par1ICommandSender, "commands.deop.success",
							new Object[] { par2ArrayOfStr[0] });
		} else {
			throw new WrongUsageException("commands.deop.usage", new Object[0]);
		}
	}

	/**
	 * Adds the strings available in this command to the given list of tab
	 * completion options.
	 */
	@Override
	public List addTabCompletionOptions(
			final ICommandSender par1ICommandSender,
			final String[] par2ArrayOfStr) {
		return par2ArrayOfStr.length == 1 ? CommandBase
				.getListOfStringsFromIterableMatchingLastWord(par2ArrayOfStr,
						MinecraftServer.getServer().getConfigurationManager()
								.getOps()) : null;
	}
}
