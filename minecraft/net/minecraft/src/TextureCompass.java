package net.minecraft.src;

import net.minecraft.client.Minecraft;

public class TextureCompass extends TextureStitched {
	public static TextureCompass compassTexture;

	/** Current compass heading in radians */
	public double currentAngle;

	/** Speed and direction of compass rotation */
	public double angleDelta;

	public TextureCompass() {
		super("compass");
		TextureCompass.compassTexture = this;
	}

	@Override
	public void updateAnimation() {
		final Minecraft var1 = Minecraft.getMinecraft();

		if (var1.theWorld != null && var1.thePlayer != null) {
			updateCompass(var1.theWorld, var1.thePlayer.posX,
					var1.thePlayer.posZ, var1.thePlayer.rotationYaw, false,
					false);
		} else {
			updateCompass((World) null, 0.0D, 0.0D, 0.0D, true, false);
		}
	}

	/**
	 * Updates the compass based on the given x,z coords and camera direction
	 */
	public void updateCompass(final World par1World, final double par2,
			final double par4, double par6, final boolean par8,
			final boolean par9) {
		double var10 = 0.0D;

		if (par1World != null && !par8) {
			final ChunkCoordinates var12 = par1World.getSpawnPoint();
			final double var13 = var12.posX - par2;
			final double var15 = var12.posZ - par4;
			par6 %= 360.0D;
			var10 = -((par6 - 90.0D) * Math.PI / 180.0D - Math.atan2(var15,
					var13));

			if (!par1World.provider.isSurfaceWorld()) {
				var10 = Math.random() * Math.PI * 2.0D;
			}
		}

		if (par9) {
			currentAngle = var10;
		} else {
			double var17;

			for (var17 = var10 - currentAngle; var17 < -Math.PI; var17 += Math.PI * 2D) {
				;
			}

			while (var17 >= Math.PI) {
				var17 -= Math.PI * 2D;
			}

			if (var17 < -1.0D) {
				var17 = -1.0D;
			}

			if (var17 > 1.0D) {
				var17 = 1.0D;
			}

			angleDelta += var17 * 0.1D;
			angleDelta *= 0.8D;
			currentAngle += angleDelta;
		}

		int var18;

		for (var18 = (int) ((currentAngle / (Math.PI * 2D) + 1.0D) * textureList
				.size()) % textureList.size(); var18 < 0; var18 = (var18 + textureList
				.size()) % textureList.size()) {
			;
		}

		if (var18 != frameCounter) {
			frameCounter = var18;
			textureSheet.copyFrom(originX, originY,
					(Texture) textureList.get(frameCounter), rotated);
		}
	}
}
