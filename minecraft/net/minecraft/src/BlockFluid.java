package net.minecraft.src;

import java.util.Random;

public abstract class BlockFluid extends Block {
	private Icon[] theIcon;

	protected BlockFluid(final int par1, final Material par2Material) {
		super(par1, par2Material);
		final float var3 = 0.0F;
		final float var4 = 0.0F;
		setBlockBounds(0.0F + var4, 0.0F + var3, 0.0F + var4, 1.0F + var4,
				1.0F + var3, 1.0F + var4);
		setTickRandomly(true);
	}

	@Override
	public boolean getBlocksMovement(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		return blockMaterial != Material.lava;
	}

	@Override
	public int getBlockColor() {
		return 16777215;
	}

	/**
	 * Returns a integer with hex for 0xrrggbb with this color multiplied
	 * against the blocks color. Note only called when first determining what to
	 * render.
	 */
	@Override
	public int colorMultiplier(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		if (blockMaterial != Material.water) {
			return 16777215;
		} else {
			int var5 = 0;
			int var6 = 0;
			int var7 = 0;

			for (int var8 = -1; var8 <= 1; ++var8) {
				for (int var9 = -1; var9 <= 1; ++var9) {
					final int var10 = par1IBlockAccess.getBiomeGenForCoords(
							par2 + var9, par4 + var8).waterColorMultiplier;
					var5 += (var10 & 16711680) >> 16;
					var6 += (var10 & 65280) >> 8;
					var7 += var10 & 255;
				}
			}

			return (var5 / 9 & 255) << 16 | (var6 / 9 & 255) << 8 | var7 / 9
					& 255;
		}
	}

	/**
	 * Returns the percentage of the fluid block that is air, based on the given
	 * flow decay of the fluid.
	 */
	public static float getFluidHeightPercent(int par0) {
		if (par0 >= 8) {
			par0 = 0;
		}

		return (par0 + 1) / 9.0F;
	}

	/**
	 * From the specified side and block metadata retrieves the blocks texture.
	 * Args: side, metadata
	 */
	@Override
	public Icon getIcon(final int par1, final int par2) {
		return par1 != 0 && par1 != 1 ? theIcon[1] : theIcon[0];
	}

	/**
	 * Returns the amount of fluid decay at the coordinates, or -1 if the block
	 * at the coordinates is not the same material as the fluid.
	 */
	protected int getFlowDecay(final World par1World, final int par2,
			final int par3, final int par4) {
		return par1World.getBlockMaterial(par2, par3, par4) == blockMaterial ? par1World
				.getBlockMetadata(par2, par3, par4) : -1;
	}

	/**
	 * Returns the flow decay but converts values indicating falling liquid
	 * (values >=8) to their effective source block value of zero.
	 */
	protected int getEffectiveFlowDecay(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		if (par1IBlockAccess.getBlockMaterial(par2, par3, par4) != blockMaterial) {
			return -1;
		} else {
			int var5 = par1IBlockAccess.getBlockMetadata(par2, par3, par4);

			if (var5 >= 8) {
				var5 = 0;
			}

			return var5;
		}
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False
	 * (examples: signs, buttons, stairs, etc)
	 */
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube? This determines whether
	 * or not to render the shared face of two adjacent blocks and also whether
	 * the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * Returns whether this block is collideable based on the arguments passed
	 * in Args: blockMetaData, unknownFlag
	 */
	@Override
	public boolean canCollideCheck(final int par1, final boolean par2) {
		return par2 && par1 == 0;
	}

	/**
	 * Returns Returns true if the given side of this block type should be
	 * rendered (if it's solid or not), if the adjacent block is at the given
	 * coordinates. Args: blockAccess, x, y, z, side
	 */
	@Override
	public boolean isBlockSolid(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		final Material var6 = par1IBlockAccess.getBlockMaterial(par2, par3,
				par4);
		return var6 == blockMaterial ? false : par5 == 1 ? true
				: var6 == Material.ice ? false : super.isBlockSolid(
						par1IBlockAccess, par2, par3, par4, par5);
	}

	/**
	 * Returns true if the given side of this block type should be rendered, if
	 * the adjacent block is at the given coordinates. Args: blockAccess, x, y,
	 * z, side
	 */
	@Override
	public boolean shouldSideBeRendered(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4, final int par5) {
		final Material var6 = par1IBlockAccess.getBlockMaterial(par2, par3,
				par4);
		return var6 == blockMaterial ? false : par5 == 1 ? true
				: var6 == Material.ice ? false : super.shouldSideBeRendered(
						par1IBlockAccess, par2, par3, par4, par5);
	}

	/**
	 * Returns a bounding box from the pool of bounding boxes (this means this
	 * box can change after the pool has been cleared to be reused)
	 */
	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(final World par1World,
			final int par2, final int par3, final int par4) {
		return null;
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return 4;
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return 0;
	}

	/**
	 * Returns the quantity of items to drop on block destruction.
	 */
	@Override
	public int quantityDropped(final Random par1Random) {
		return 0;
	}

	/**
	 * Returns a vector indicating the direction and intensity of fluid flow.
	 */
	private Vec3 getFlowVector(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		Vec3 var5 = par1IBlockAccess.getWorldVec3Pool().getVecFromPool(0.0D,
				0.0D, 0.0D);
		final int var6 = getEffectiveFlowDecay(par1IBlockAccess, par2, par3,
				par4);

		for (int var7 = 0; var7 < 4; ++var7) {
			int var8 = par2;
			int var10 = par4;

			if (var7 == 0) {
				var8 = par2 - 1;
			}

			if (var7 == 1) {
				var10 = par4 - 1;
			}

			if (var7 == 2) {
				++var8;
			}

			if (var7 == 3) {
				++var10;
			}

			int var11 = getEffectiveFlowDecay(par1IBlockAccess, var8, par3,
					var10);
			int var12;

			if (var11 < 0) {
				if (!par1IBlockAccess.getBlockMaterial(var8, par3, var10)
						.blocksMovement()) {
					var11 = getEffectiveFlowDecay(par1IBlockAccess, var8,
							par3 - 1, var10);

					if (var11 >= 0) {
						var12 = var11 - (var6 - 8);
						var5 = var5.addVector((var8 - par2) * var12,
								(par3 - par3) * var12, (var10 - par4) * var12);
					}
				}
			} else if (var11 >= 0) {
				var12 = var11 - var6;
				var5 = var5.addVector((var8 - par2) * var12, (par3 - par3)
						* var12, (var10 - par4) * var12);
			}
		}

		if (par1IBlockAccess.getBlockMetadata(par2, par3, par4) >= 8) {
			boolean var13 = false;

			if (var13
					|| isBlockSolid(par1IBlockAccess, par2, par3, par4 - 1, 2)) {
				var13 = true;
			}

			if (var13
					|| isBlockSolid(par1IBlockAccess, par2, par3, par4 + 1, 3)) {
				var13 = true;
			}

			if (var13
					|| isBlockSolid(par1IBlockAccess, par2 - 1, par3, par4, 4)) {
				var13 = true;
			}

			if (var13
					|| isBlockSolid(par1IBlockAccess, par2 + 1, par3, par4, 5)) {
				var13 = true;
			}

			if (var13
					|| isBlockSolid(par1IBlockAccess, par2, par3 + 1, par4 - 1,
							2)) {
				var13 = true;
			}

			if (var13
					|| isBlockSolid(par1IBlockAccess, par2, par3 + 1, par4 + 1,
							3)) {
				var13 = true;
			}

			if (var13
					|| isBlockSolid(par1IBlockAccess, par2 - 1, par3 + 1, par4,
							4)) {
				var13 = true;
			}

			if (var13
					|| isBlockSolid(par1IBlockAccess, par2 + 1, par3 + 1, par4,
							5)) {
				var13 = true;
			}

			if (var13) {
				var5 = var5.normalize().addVector(0.0D, -6.0D, 0.0D);
			}
		}

		var5 = var5.normalize();
		return var5;
	}

	/**
	 * Can add to the passed in vector for a movement vector to be applied to
	 * the entity. Args: x, y, z, entity, vec3d
	 */
	@Override
	public void velocityToAddToEntity(final World par1World, final int par2,
			final int par3, final int par4, final Entity par5Entity,
			final Vec3 par6Vec3) {
		final Vec3 var7 = getFlowVector(par1World, par2, par3, par4);
		par6Vec3.xCoord += var7.xCoord;
		par6Vec3.yCoord += var7.yCoord;
		par6Vec3.zCoord += var7.zCoord;
	}

	/**
	 * How many world ticks before ticking
	 */
	@Override
	public int tickRate(final World par1World) {
		return blockMaterial == Material.water ? 5
				: blockMaterial == Material.lava ? par1World.provider.hasNoSky ? 10
						: 30
						: 0;
	}

	/**
	 * Goes straight to getLightBrightnessForSkyBlocks for Blocks, does some
	 * fancy computing for Fluids
	 */
	@Override
	public int getMixedBrightnessForBlock(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		final int var5 = par1IBlockAccess.getLightBrightnessForSkyBlocks(par2,
				par3, par4, 0);
		final int var6 = par1IBlockAccess.getLightBrightnessForSkyBlocks(par2,
				par3 + 1, par4, 0);
		final int var7 = var5 & 255;
		final int var8 = var6 & 255;
		final int var9 = var5 >> 16 & 255;
		final int var10 = var6 >> 16 & 255;
		return (var7 > var8 ? var7 : var8)
				| (var9 > var10 ? var9 : var10) << 16;
	}

	/**
	 * How bright to render this block based on the light its receiving. Args:
	 * iBlockAccess, x, y, z
	 */
	@Override
	public float getBlockBrightness(final IBlockAccess par1IBlockAccess,
			final int par2, final int par3, final int par4) {
		final float var5 = par1IBlockAccess
				.getLightBrightness(par2, par3, par4);
		final float var6 = par1IBlockAccess.getLightBrightness(par2, par3 + 1,
				par4);
		return var5 > var6 ? var5 : var6;
	}

	/**
	 * Returns which pass should this block be rendered on. 0 for solids and 1
	 * for alpha
	 */
	@Override
	public int getRenderBlockPass() {
		return blockMaterial == Material.water ? 1 : 0;
	}

	/**
	 * A randomly called display update to be able to add particles or other
	 * items for display
	 */
	@Override
	public void randomDisplayTick(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		int var6;

		if (blockMaterial == Material.water) {
			if (par5Random.nextInt(10) == 0) {
				var6 = par1World.getBlockMetadata(par2, par3, par4);

				if (var6 <= 0 || var6 >= 8) {
					par1World.spawnParticle("suspended",
							par2 + par5Random.nextFloat(),
							par3 + par5Random.nextFloat(),
							par4 + par5Random.nextFloat(), 0.0D, 0.0D, 0.0D);
				}
			}

			for (var6 = 0; var6 < 0; ++var6) {
				final int var7 = par5Random.nextInt(4);
				int var8 = par2;
				int var9 = par4;

				if (var7 == 0) {
					var8 = par2 - 1;
				}

				if (var7 == 1) {
					++var8;
				}

				if (var7 == 2) {
					var9 = par4 - 1;
				}

				if (var7 == 3) {
					++var9;
				}

				if (par1World.getBlockMaterial(var8, par3, var9) == Material.air
						&& (par1World.getBlockMaterial(var8, par3 - 1, var9)
								.blocksMovement() || par1World
								.getBlockMaterial(var8, par3 - 1, var9)
								.isLiquid())) {
					final float var10 = 0.0625F;
					double var11 = par2 + par5Random.nextFloat();
					final double var13 = par3 + par5Random.nextFloat();
					double var15 = par4 + par5Random.nextFloat();

					if (var7 == 0) {
						var11 = par2 - var10;
					}

					if (var7 == 1) {
						var11 = par2 + 1 + var10;
					}

					if (var7 == 2) {
						var15 = par4 - var10;
					}

					if (var7 == 3) {
						var15 = par4 + 1 + var10;
					}

					double var17 = 0.0D;
					double var19 = 0.0D;

					if (var7 == 0) {
						var17 = -var10;
					}

					if (var7 == 1) {
						var17 = var10;
					}

					if (var7 == 2) {
						var19 = -var10;
					}

					if (var7 == 3) {
						var19 = var10;
					}

					par1World.spawnParticle("splash", var11, var13, var15,
							var17, 0.0D, var19);
				}
			}
		}

		if (blockMaterial == Material.water && par5Random.nextInt(64) == 0) {
			var6 = par1World.getBlockMetadata(par2, par3, par4);

			if (var6 > 0 && var6 < 8) {
				par1World.playSound(par2 + 0.5F, par3 + 0.5F, par4 + 0.5F,
						"liquid.water", par5Random.nextFloat() * 0.25F + 0.75F,
						par5Random.nextFloat() * 1.0F + 0.5F, false);
			}
		}

		double var21;
		double var23;
		double var22;

		if (blockMaterial == Material.lava
				&& par1World.getBlockMaterial(par2, par3 + 1, par4) == Material.air
				&& !par1World.isBlockOpaqueCube(par2, par3 + 1, par4)) {
			if (par5Random.nextInt(100) == 0) {
				var21 = par2 + par5Random.nextFloat();
				var22 = par3 + maxY;
				var23 = par4 + par5Random.nextFloat();
				par1World.spawnParticle("lava", var21, var22, var23, 0.0D,
						0.0D, 0.0D);
				par1World.playSound(var21, var22, var23, "liquid.lavapop",
						0.2F + par5Random.nextFloat() * 0.2F,
						0.9F + par5Random.nextFloat() * 0.15F, false);
			}

			if (par5Random.nextInt(200) == 0) {
				par1World.playSound(par2, par3, par4, "liquid.lava",
						0.2F + par5Random.nextFloat() * 0.2F,
						0.9F + par5Random.nextFloat() * 0.15F, false);
			}
		}

		if (par5Random.nextInt(10) == 0
				&& par1World.doesBlockHaveSolidTopSurface(par2, par3 - 1, par4)
				&& !par1World.getBlockMaterial(par2, par3 - 2, par4)
						.blocksMovement()) {
			var21 = par2 + par5Random.nextFloat();
			var22 = par3 - 1.05D;
			var23 = par4 + par5Random.nextFloat();

			if (blockMaterial == Material.water) {
				par1World.spawnParticle("dripWater", var21, var22, var23, 0.0D,
						0.0D, 0.0D);
			} else {
				par1World.spawnParticle("dripLava", var21, var22, var23, 0.0D,
						0.0D, 0.0D);
			}
		}
	}

	/**
	 * the sin and cos of this number determine the surface gradient of the
	 * flowing block.
	 */
	public static double getFlowDirection(final IBlockAccess par0IBlockAccess,
			final int par1, final int par2, final int par3,
			final Material par4Material) {
		Vec3 var5 = null;

		if (par4Material == Material.water) {
			var5 = Block.waterMoving.getFlowVector(par0IBlockAccess, par1,
					par2, par3);
		}

		if (par4Material == Material.lava) {
			var5 = Block.lavaMoving.getFlowVector(par0IBlockAccess, par1, par2,
					par3);
		}

		return var5.xCoord == 0.0D && var5.zCoord == 0.0D ? -1000.0D : Math
				.atan2(var5.zCoord, var5.xCoord) - Math.PI / 2D;
	}

	/**
	 * Called whenever the block is added into the world. Args: world, x, y, z
	 */
	@Override
	public void onBlockAdded(final World par1World, final int par2,
			final int par3, final int par4) {
		checkForHarden(par1World, par2, par3, par4);
	}

	/**
	 * Lets the block know when one of its neighbor changes. Doesn't know which
	 * neighbor changed (coordinates passed are their own) Args: x, y, z,
	 * neighbor blockID
	 */
	@Override
	public void onNeighborBlockChange(final World par1World, final int par2,
			final int par3, final int par4, final int par5) {
		checkForHarden(par1World, par2, par3, par4);
	}

	/**
	 * Forces lava to check to see if it is colliding with water, and then
	 * decide what it should harden to.
	 */
	private void checkForHarden(final World par1World, final int par2,
			final int par3, final int par4) {
		if (par1World.getBlockId(par2, par3, par4) == blockID) {
			if (blockMaterial == Material.lava) {
				boolean var5 = false;

				if (var5
						|| par1World.getBlockMaterial(par2, par3, par4 - 1) == Material.water) {
					var5 = true;
				}

				if (var5
						|| par1World.getBlockMaterial(par2, par3, par4 + 1) == Material.water) {
					var5 = true;
				}

				if (var5
						|| par1World.getBlockMaterial(par2 - 1, par3, par4) == Material.water) {
					var5 = true;
				}

				if (var5
						|| par1World.getBlockMaterial(par2 + 1, par3, par4) == Material.water) {
					var5 = true;
				}

				if (var5
						|| par1World.getBlockMaterial(par2, par3 + 1, par4) == Material.water) {
					var5 = true;
				}

				if (var5) {
					final int var6 = par1World.getBlockMetadata(par2, par3,
							par4);

					if (var6 == 0) {
						par1World.setBlock(par2, par3, par4,
								Block.obsidian.blockID);
					} else if (var6 <= 4) {
						par1World.setBlock(par2, par3, par4,
								Block.cobblestone.blockID);
					}

					triggerLavaMixEffects(par1World, par2, par3, par4);
				}
			}
		}
	}

	/**
	 * Creates fizzing sound and smoke. Used when lava flows over block or mixes
	 * with water.
	 */
	protected void triggerLavaMixEffects(final World par1World, final int par2,
			final int par3, final int par4) {
		par1World
				.playSoundEffect(par2 + 0.5F, par3 + 0.5F, par4 + 0.5F,
						"random.fizz", 0.5F,
						2.6F + (par1World.rand.nextFloat() - par1World.rand
								.nextFloat()) * 0.8F);

		for (int var5 = 0; var5 < 8; ++var5) {
			par1World.spawnParticle("largesmoke", par2 + Math.random(),
					par3 + 1.2D, par4 + Math.random(), 0.0D, 0.0D, 0.0D);
		}
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		if (blockMaterial == Material.lava) {
			theIcon = new Icon[] { par1IconRegister.registerIcon("lava"),
					par1IconRegister.registerIcon("lava_flow") };
		} else {
			theIcon = new Icon[] { par1IconRegister.registerIcon("water"),
					par1IconRegister.registerIcon("water_flow") };
		}
	}

	public static Icon func_94424_b(final String par0Str) {
		return par0Str == "water" ? Block.waterMoving.theIcon[0]
				: par0Str == "water_flow" ? Block.waterMoving.theIcon[1]
						: par0Str == "lava" ? Block.lavaMoving.theIcon[0]
								: par0Str == "lava_flow" ? Block.lavaMoving.theIcon[1]
										: null;
	}
}
