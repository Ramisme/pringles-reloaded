package net.minecraft.src;

public class ItemSlab extends ItemBlock {
	private final boolean isFullBlock;

	/** Instance of BlockHalfSlab. */
	private final BlockHalfSlab theHalfSlab;

	/** The double-slab block corresponding to this item. */
	private final BlockHalfSlab doubleSlab;

	public ItemSlab(final int par1, final BlockHalfSlab par2BlockHalfSlab,
			final BlockHalfSlab par3BlockHalfSlab, final boolean par4) {
		super(par1);
		theHalfSlab = par2BlockHalfSlab;
		doubleSlab = par3BlockHalfSlab;
		isFullBlock = par4;
		setMaxDamage(0);
		setHasSubtypes(true);
	}

	/**
	 * Gets an icon index based on an item's damage value
	 */
	@Override
	public Icon getIconFromDamage(final int par1) {
		return Block.blocksList[itemID].getIcon(2, par1);
	}

	/**
	 * Returns the metadata of the block which this Item (ItemBlock) can place
	 */
	@Override
	public int getMetadata(final int par1) {
		return par1;
	}

	/**
	 * Returns the unlocalized name of this item. This version accepts an
	 * ItemStack so different stacks can have different names based on their
	 * damage or NBT.
	 */
	@Override
	public String getUnlocalizedName(final ItemStack par1ItemStack) {
		return theHalfSlab.getFullSlabName(par1ItemStack.getItemDamage());
	}

	/**
	 * Callback for item usage. If the item does something special on right
	 * clicking, he will have one of those. Return True if something happen and
	 * false if it don't. This is for ITEMS, not BLOCKS
	 */
	@Override
	public boolean onItemUse(final ItemStack par1ItemStack,
			final EntityPlayer par2EntityPlayer, final World par3World,
			final int par4, final int par5, final int par6, final int par7,
			final float par8, final float par9, final float par10) {
		if (isFullBlock) {
			return super.onItemUse(par1ItemStack, par2EntityPlayer, par3World,
					par4, par5, par6, par7, par8, par9, par10);
		} else if (par1ItemStack.stackSize == 0) {
			return false;
		} else if (!par2EntityPlayer.canPlayerEdit(par4, par5, par6, par7,
				par1ItemStack)) {
			return false;
		} else {
			final int var11 = par3World.getBlockId(par4, par5, par6);
			final int var12 = par3World.getBlockMetadata(par4, par5, par6);
			final int var13 = var12 & 7;
			final boolean var14 = (var12 & 8) != 0;

			if ((par7 == 1 && !var14 || par7 == 0 && var14)
					&& var11 == theHalfSlab.blockID
					&& var13 == par1ItemStack.getItemDamage()) {
				if (par3World.checkNoEntityCollision(doubleSlab
						.getCollisionBoundingBoxFromPool(par3World, par4, par5,
								par6))
						&& par3World.setBlock(par4, par5, par6,
								doubleSlab.blockID, var13, 3)) {
					par3World.playSoundEffect(par4 + 0.5F, par5 + 0.5F,
							par6 + 0.5F, doubleSlab.stepSound.getPlaceSound(),
							(doubleSlab.stepSound.getVolume() + 1.0F) / 2.0F,
							doubleSlab.stepSound.getPitch() * 0.8F);
					--par1ItemStack.stackSize;
				}

				return true;
			} else {
				return func_77888_a(par1ItemStack, par2EntityPlayer, par3World,
						par4, par5, par6, par7) ? true : super.onItemUse(
						par1ItemStack, par2EntityPlayer, par3World, par4, par5,
						par6, par7, par8, par9, par10);
			}
		}
	}

	/**
	 * Returns true if the given ItemBlock can be placed on the given side of
	 * the given block position.
	 */
	@Override
	public boolean canPlaceItemBlockOnSide(final World par1World, int par2,
			int par3, int par4, final int par5,
			final EntityPlayer par6EntityPlayer, final ItemStack par7ItemStack) {
		final int var8 = par2;
		final int var9 = par3;
		final int var10 = par4;
		int var11 = par1World.getBlockId(par2, par3, par4);
		int var12 = par1World.getBlockMetadata(par2, par3, par4);
		int var13 = var12 & 7;
		boolean var14 = (var12 & 8) != 0;

		if ((par5 == 1 && !var14 || par5 == 0 && var14)
				&& var11 == theHalfSlab.blockID
				&& var13 == par7ItemStack.getItemDamage()) {
			return true;
		} else {
			if (par5 == 0) {
				--par3;
			}

			if (par5 == 1) {
				++par3;
			}

			if (par5 == 2) {
				--par4;
			}

			if (par5 == 3) {
				++par4;
			}

			if (par5 == 4) {
				--par2;
			}

			if (par5 == 5) {
				++par2;
			}

			var11 = par1World.getBlockId(par2, par3, par4);
			var12 = par1World.getBlockMetadata(par2, par3, par4);
			var13 = var12 & 7;
			var14 = (var12 & 8) != 0;
			return var11 == theHalfSlab.blockID
					&& var13 == par7ItemStack.getItemDamage() ? true : super
					.canPlaceItemBlockOnSide(par1World, var8, var9, var10,
							par5, par6EntityPlayer, par7ItemStack);
		}
	}

	private boolean func_77888_a(final ItemStack par1ItemStack,
			final EntityPlayer par2EntityPlayer, final World par3World,
			int par4, int par5, int par6, final int par7) {
		if (par7 == 0) {
			--par5;
		}

		if (par7 == 1) {
			++par5;
		}

		if (par7 == 2) {
			--par6;
		}

		if (par7 == 3) {
			++par6;
		}

		if (par7 == 4) {
			--par4;
		}

		if (par7 == 5) {
			++par4;
		}

		final int var8 = par3World.getBlockId(par4, par5, par6);
		final int var9 = par3World.getBlockMetadata(par4, par5, par6);
		final int var10 = var9 & 7;

		if (var8 == theHalfSlab.blockID
				&& var10 == par1ItemStack.getItemDamage()) {
			if (par3World.checkNoEntityCollision(doubleSlab
					.getCollisionBoundingBoxFromPool(par3World, par4, par5,
							par6))
					&& par3World.setBlock(par4, par5, par6, doubleSlab.blockID,
							var10, 3)) {
				par3World.playSoundEffect(par4 + 0.5F, par5 + 0.5F,
						par6 + 0.5F, doubleSlab.stepSound.getPlaceSound(),
						(doubleSlab.stepSound.getVolume() + 1.0F) / 2.0F,
						doubleSlab.stepSound.getPitch() * 0.8F);
				--par1ItemStack.stackSize;
			}

			return true;
		} else {
			return false;
		}
	}
}
