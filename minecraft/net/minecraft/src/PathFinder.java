package net.minecraft.src;

public class PathFinder {
	/** Used to find obstacles */
	private final IBlockAccess worldMap;

	/** The path being generated */
	private final Path path = new Path();

	/** The points in the path */
	private final IntHashMap pointMap = new IntHashMap();

	/** Selection of path points to add to the path */
	private final PathPoint[] pathOptions = new PathPoint[32];

	/** should the PathFinder go through wodden door blocks */
	private final boolean isWoddenDoorAllowed;

	/**
	 * should the PathFinder disregard BlockMovement type materials in its path
	 */
	private final boolean isMovementBlockAllowed;
	private boolean isPathingInWater;

	/** tells the FathFinder to not stop pathing underwater */
	private final boolean canEntityDrown;

	public PathFinder(final IBlockAccess par1IBlockAccess, final boolean par2,
			final boolean par3, final boolean par4, final boolean par5) {
		worldMap = par1IBlockAccess;
		isWoddenDoorAllowed = par2;
		isMovementBlockAllowed = par3;
		isPathingInWater = par4;
		canEntityDrown = par5;
	}

	/**
	 * Creates a path from one entity to another within a minimum distance
	 */
	public PathEntity createEntityPathTo(final Entity par1Entity,
			final Entity par2Entity, final float par3) {
		return this.createEntityPathTo(par1Entity, par2Entity.posX,
				par2Entity.boundingBox.minY, par2Entity.posZ, par3);
	}

	/**
	 * Creates a path from an entity to a specified location within a minimum
	 * distance
	 */
	public PathEntity createEntityPathTo(final Entity par1Entity,
			final int par2, final int par3, final int par4, final float par5) {
		return this.createEntityPathTo(par1Entity, par2 + 0.5F, par3 + 0.5F,
				par4 + 0.5F, par5);
	}

	/**
	 * Internal implementation of creating a path from an entity to a point
	 */
	private PathEntity createEntityPathTo(final Entity par1Entity,
			final double par2, final double par4, final double par6,
			final float par8) {
		path.clearPath();
		pointMap.clearMap();
		boolean var9 = isPathingInWater;
		int var10 = MathHelper.floor_double(par1Entity.boundingBox.minY + 0.5D);

		if (canEntityDrown && par1Entity.isInWater()) {
			var10 = (int) par1Entity.boundingBox.minY;

			for (int var11 = worldMap.getBlockId(
					MathHelper.floor_double(par1Entity.posX), var10,
					MathHelper.floor_double(par1Entity.posZ)); var11 == Block.waterMoving.blockID
					|| var11 == Block.waterStill.blockID; var11 = worldMap
					.getBlockId(MathHelper.floor_double(par1Entity.posX),
							var10, MathHelper.floor_double(par1Entity.posZ))) {
				++var10;
			}

			var9 = isPathingInWater;
			isPathingInWater = false;
		} else {
			var10 = MathHelper.floor_double(par1Entity.boundingBox.minY + 0.5D);
		}

		final PathPoint var15 = openPoint(
				MathHelper.floor_double(par1Entity.boundingBox.minX), var10,
				MathHelper.floor_double(par1Entity.boundingBox.minZ));
		final PathPoint var12 = openPoint(
				MathHelper.floor_double(par2 - par1Entity.width / 2.0F),
				MathHelper.floor_double(par4),
				MathHelper.floor_double(par6 - par1Entity.width / 2.0F));
		final PathPoint var13 = new PathPoint(
				MathHelper.floor_float(par1Entity.width + 1.0F),
				MathHelper.floor_float(par1Entity.height + 1.0F),
				MathHelper.floor_float(par1Entity.width + 1.0F));
		final PathEntity var14 = addToPath(par1Entity, var15, var12, var13,
				par8);
		isPathingInWater = var9;
		return var14;
	}

	/**
	 * Adds a path from start to end and returns the whole path (args: unused,
	 * start, end, unused, maxDistance)
	 */
	private PathEntity addToPath(final Entity par1Entity,
			final PathPoint par2PathPoint, final PathPoint par3PathPoint,
			final PathPoint par4PathPoint, final float par5) {
		par2PathPoint.totalPathDistance = 0.0F;
		par2PathPoint.distanceToNext = par2PathPoint
				.func_75832_b(par3PathPoint);
		par2PathPoint.distanceToTarget = par2PathPoint.distanceToNext;
		path.clearPath();
		path.addPoint(par2PathPoint);
		PathPoint var6 = par2PathPoint;

		while (!path.isPathEmpty()) {
			final PathPoint var7 = path.dequeue();

			if (var7.equals(par3PathPoint)) {
				return createEntityPath(par2PathPoint, par3PathPoint);
			}

			if (var7.func_75832_b(par3PathPoint) < var6
					.func_75832_b(par3PathPoint)) {
				var6 = var7;
			}

			var7.isFirst = true;
			final int var8 = findPathOptions(par1Entity, var7, par4PathPoint,
					par3PathPoint, par5);

			for (int var9 = 0; var9 < var8; ++var9) {
				final PathPoint var10 = pathOptions[var9];
				final float var11 = var7.totalPathDistance
						+ var7.func_75832_b(var10);

				if (!var10.isAssigned() || var11 < var10.totalPathDistance) {
					var10.previous = var7;
					var10.totalPathDistance = var11;
					var10.distanceToNext = var10.func_75832_b(par3PathPoint);

					if (var10.isAssigned()) {
						path.changeDistance(var10, var10.totalPathDistance
								+ var10.distanceToNext);
					} else {
						var10.distanceToTarget = var10.totalPathDistance
								+ var10.distanceToNext;
						path.addPoint(var10);
					}
				}
			}
		}

		if (var6 == par2PathPoint) {
			return null;
		} else {
			return createEntityPath(par2PathPoint, var6);
		}
	}

	/**
	 * populates pathOptions with available points and returns the number of
	 * options found (args: unused1, currentPoint, unused2, targetPoint,
	 * maxDistance)
	 */
	private int findPathOptions(final Entity par1Entity,
			final PathPoint par2PathPoint, final PathPoint par3PathPoint,
			final PathPoint par4PathPoint, final float par5) {
		int var6 = 0;
		byte var7 = 0;

		if (getVerticalOffset(par1Entity, par2PathPoint.xCoord,
				par2PathPoint.yCoord + 1, par2PathPoint.zCoord, par3PathPoint) == 1) {
			var7 = 1;
		}

		final PathPoint var8 = getSafePoint(par1Entity, par2PathPoint.xCoord,
				par2PathPoint.yCoord, par2PathPoint.zCoord + 1, par3PathPoint,
				var7);
		final PathPoint var9 = getSafePoint(par1Entity,
				par2PathPoint.xCoord - 1, par2PathPoint.yCoord,
				par2PathPoint.zCoord, par3PathPoint, var7);
		final PathPoint var10 = getSafePoint(par1Entity,
				par2PathPoint.xCoord + 1, par2PathPoint.yCoord,
				par2PathPoint.zCoord, par3PathPoint, var7);
		final PathPoint var11 = getSafePoint(par1Entity, par2PathPoint.xCoord,
				par2PathPoint.yCoord, par2PathPoint.zCoord - 1, par3PathPoint,
				var7);

		if (var8 != null && !var8.isFirst
				&& var8.distanceTo(par4PathPoint) < par5) {
			pathOptions[var6++] = var8;
		}

		if (var9 != null && !var9.isFirst
				&& var9.distanceTo(par4PathPoint) < par5) {
			pathOptions[var6++] = var9;
		}

		if (var10 != null && !var10.isFirst
				&& var10.distanceTo(par4PathPoint) < par5) {
			pathOptions[var6++] = var10;
		}

		if (var11 != null && !var11.isFirst
				&& var11.distanceTo(par4PathPoint) < par5) {
			pathOptions[var6++] = var11;
		}

		return var6;
	}

	/**
	 * Returns a point that the entity can safely move to
	 */
	private PathPoint getSafePoint(final Entity par1Entity, final int par2,
			int par3, final int par4, final PathPoint par5PathPoint,
			final int par6) {
		PathPoint var7 = null;
		final int var8 = getVerticalOffset(par1Entity, par2, par3, par4,
				par5PathPoint);

		if (var8 == 2) {
			return openPoint(par2, par3, par4);
		} else {
			if (var8 == 1) {
				var7 = openPoint(par2, par3, par4);
			}

			if (var7 == null
					&& par6 > 0
					&& var8 != -3
					&& var8 != -4
					&& getVerticalOffset(par1Entity, par2, par3 + par6, par4,
							par5PathPoint) == 1) {
				var7 = openPoint(par2, par3 + par6, par4);
				par3 += par6;
			}

			if (var7 != null) {
				int var9 = 0;
				int var10 = 0;

				while (par3 > 0) {
					var10 = getVerticalOffset(par1Entity, par2, par3 - 1, par4,
							par5PathPoint);

					if (isPathingInWater && var10 == -1) {
						return null;
					}

					if (var10 != 1) {
						break;
					}

					if (var9++ >= par1Entity.func_82143_as()) {
						return null;
					}

					--par3;

					if (par3 > 0) {
						var7 = openPoint(par2, par3, par4);
					}
				}

				if (var10 == -2) {
					return null;
				}
			}

			return var7;
		}
	}

	/**
	 * Returns a mapped point or creates and adds one
	 */
	private final PathPoint openPoint(final int par1, final int par2,
			final int par3) {
		final int var4 = PathPoint.makeHash(par1, par2, par3);
		PathPoint var5 = (PathPoint) pointMap.lookup(var4);

		if (var5 == null) {
			var5 = new PathPoint(par1, par2, par3);
			pointMap.addKey(var4, var5);
		}

		return var5;
	}

	/**
	 * Checks if an entity collides with blocks at a position. Returns 1 if
	 * clear, 0 for colliding with any solid block, -1 for water(if avoiding
	 * water) but otherwise clear, -2 for lava, -3 for fence, -4 for closed
	 * trapdoor, 2 if otherwise clear except for open trapdoor or water(if not
	 * avoiding)
	 */
	public int getVerticalOffset(final Entity par1Entity, final int par2,
			final int par3, final int par4, final PathPoint par5PathPoint) {
		return PathFinder.func_82565_a(par1Entity, par2, par3, par4,
				par5PathPoint, isPathingInWater, isMovementBlockAllowed,
				isWoddenDoorAllowed);
	}

	public static int func_82565_a(final Entity par0Entity, final int par1,
			final int par2, final int par3, final PathPoint par4PathPoint,
			final boolean par5, final boolean par6, final boolean par7) {
		boolean var8 = false;

		for (int var9 = par1; var9 < par1 + par4PathPoint.xCoord; ++var9) {
			for (int var10 = par2; var10 < par2 + par4PathPoint.yCoord; ++var10) {
				for (int var11 = par3; var11 < par3 + par4PathPoint.zCoord; ++var11) {
					final int var12 = par0Entity.worldObj.getBlockId(var9,
							var10, var11);

					if (var12 > 0) {
						if (var12 == Block.trapdoor.blockID) {
							var8 = true;
						} else if (var12 != Block.waterMoving.blockID
								&& var12 != Block.waterStill.blockID) {
							if (!par7 && var12 == Block.doorWood.blockID) {
								return 0;
							}
						} else {
							if (par5) {
								return -1;
							}

							var8 = true;
						}

						final Block var13 = Block.blocksList[var12];
						final int var14 = var13.getRenderType();

						if (par0Entity.worldObj.blockGetRenderType(var9, var10,
								var11) == 9) {
							final int var18 = MathHelper
									.floor_double(par0Entity.posX);
							final int var16 = MathHelper
									.floor_double(par0Entity.posY);
							final int var17 = MathHelper
									.floor_double(par0Entity.posZ);

							if (par0Entity.worldObj.blockGetRenderType(var18,
									var16, var17) != 9
									&& par0Entity.worldObj.blockGetRenderType(
											var18, var16 - 1, var17) != 9) {
								return -3;
							}
						} else if (!var13.getBlocksMovement(
								par0Entity.worldObj, var9, var10, var11)
								&& (!par6 || var12 != Block.doorWood.blockID)) {
							if (var14 == 11 || var12 == Block.fenceGate.blockID
									|| var14 == 32) {
								return -3;
							}

							if (var12 == Block.trapdoor.blockID) {
								return -4;
							}

							final Material var15 = var13.blockMaterial;

							if (var15 != Material.lava) {
								return 0;
							}

							if (!par0Entity.handleLavaMovement()) {
								return -2;
							}
						}
					}
				}
			}
		}

		return var8 ? 2 : 1;
	}

	/**
	 * Returns a new PathEntity for a given start and end point
	 */
	private PathEntity createEntityPath(final PathPoint par1PathPoint,
			final PathPoint par2PathPoint) {
		int var3 = 1;
		PathPoint var4;

		for (var4 = par2PathPoint; var4.previous != null; var4 = var4.previous) {
			++var3;
		}

		final PathPoint[] var5 = new PathPoint[var3];
		var4 = par2PathPoint;
		--var3;

		for (var5[var3] = par2PathPoint; var4.previous != null; var5[var3] = var4) {
			var4 = var4.previous;
			--var3;
		}

		return new PathEntity(var5);
	}
}
