package net.minecraft.src;

import java.util.Random;

public class WorldGenShrub extends WorldGenerator {
	private final int field_76527_a;
	private final int field_76526_b;

	public WorldGenShrub(final int par1, final int par2) {
		field_76526_b = par1;
		field_76527_a = par2;
	}

	@Override
	public boolean generate(final World par1World, final Random par2Random,
			final int par3, int par4, final int par5) {
		int var15;

		for (; ((var15 = par1World.getBlockId(par3, par4, par5)) == 0 || var15 == Block.leaves.blockID)
				&& par4 > 0; --par4) {
			;
		}

		final int var7 = par1World.getBlockId(par3, par4, par5);

		if (var7 == Block.dirt.blockID || var7 == Block.grass.blockID) {
			++par4;
			setBlockAndMetadata(par1World, par3, par4, par5,
					Block.wood.blockID, field_76526_b);

			for (int var8 = par4; var8 <= par4 + 2; ++var8) {
				final int var9 = var8 - par4;
				final int var10 = 2 - var9;

				for (int var11 = par3 - var10; var11 <= par3 + var10; ++var11) {
					final int var12 = var11 - par3;

					for (int var13 = par5 - var10; var13 <= par5 + var10; ++var13) {
						final int var14 = var13 - par5;

						if ((Math.abs(var12) != var10
								|| Math.abs(var14) != var10 || par2Random
								.nextInt(2) != 0)
								&& !Block.opaqueCubeLookup[par1World
										.getBlockId(var11, var8, var13)]) {
							setBlockAndMetadata(par1World, var11, var8, var13,
									Block.leaves.blockID, field_76527_a);
						}
					}
				}
			}
		}

		return true;
	}
}
