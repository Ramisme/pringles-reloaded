package net.minecraft.src;

import java.util.Iterator;
import java.util.List;

public class EntityAIHurtByTarget extends EntityAITarget {
	boolean field_75312_a;

	/** The PathNavigate of our entity. */
	EntityLiving entityPathNavigate;

	public EntityAIHurtByTarget(final EntityLiving par1EntityLiving,
			final boolean par2) {
		super(par1EntityLiving, 16.0F, false);
		field_75312_a = par2;
		setMutexBits(1);
	}

	/**
	 * Returns whether the EntityAIBase should begin execution.
	 */
	@Override
	public boolean shouldExecute() {
		return isSuitableTarget(taskOwner.getAITarget(), true);
	}

	/**
	 * Returns whether an in-progress EntityAIBase should continue executing
	 */
	@Override
	public boolean continueExecuting() {
		return taskOwner.getAITarget() != null
				&& taskOwner.getAITarget() != entityPathNavigate;
	}

	/**
	 * Execute a one shot task or start executing a continuous task
	 */
	@Override
	public void startExecuting() {
		taskOwner.setAttackTarget(taskOwner.getAITarget());
		entityPathNavigate = taskOwner.getAITarget();

		if (field_75312_a) {
			final List var1 = taskOwner.worldObj.getEntitiesWithinAABB(
					taskOwner.getClass(),
					AxisAlignedBB
							.getAABBPool()
							.getAABB(taskOwner.posX, taskOwner.posY,
									taskOwner.posZ, taskOwner.posX + 1.0D,
									taskOwner.posY + 1.0D,
									taskOwner.posZ + 1.0D)
							.expand(targetDistance, 10.0D, targetDistance));
			final Iterator var2 = var1.iterator();

			while (var2.hasNext()) {
				final EntityLiving var3 = (EntityLiving) var2.next();

				if (taskOwner != var3 && var3.getAttackTarget() == null) {
					var3.setAttackTarget(taskOwner.getAITarget());
				}
			}
		}

		super.startExecuting();
	}

	/**
	 * Resets the task
	 */
	@Override
	public void resetTask() {
		if (taskOwner.getAttackTarget() != null
				&& taskOwner.getAttackTarget() instanceof EntityPlayer
				&& ((EntityPlayer) taskOwner.getAttackTarget()).capabilities.disableDamage) {
			super.resetTask();
		}
	}
}
