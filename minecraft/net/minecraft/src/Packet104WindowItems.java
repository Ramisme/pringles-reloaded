package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.List;

public class Packet104WindowItems extends Packet {
	/**
	 * The id of window which items are being sent for. 0 for player inventory.
	 */
	public int windowId;

	/** Stack of items */
	public ItemStack[] itemStack;

	public Packet104WindowItems() {
	}

	public Packet104WindowItems(final int par1, final List par2List) {
		windowId = par1;
		itemStack = new ItemStack[par2List.size()];

		for (int var3 = 0; var3 < itemStack.length; ++var3) {
			final ItemStack var4 = (ItemStack) par2List.get(var3);
			itemStack[var3] = var4 == null ? null : var4.copy();
		}
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		windowId = par1DataInputStream.readByte();
		final short var2 = par1DataInputStream.readShort();
		itemStack = new ItemStack[var2];

		for (int var3 = 0; var3 < var2; ++var3) {
			itemStack[var3] = Packet.readItemStack(par1DataInputStream);
		}
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		par1DataOutputStream.writeByte(windowId);
		par1DataOutputStream.writeShort(itemStack.length);

		for (final ItemStack element : itemStack) {
			Packet.writeItemStack(element, par1DataOutputStream);
		}
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleWindowItems(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 3 + itemStack.length * 5;
	}
}
