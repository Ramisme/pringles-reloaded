package net.minecraft.src;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.lwjgl.opengl.GL11;

public class WorldRenderer {
	/** Reference to the World object. */
	public World worldObj;
	protected int glRenderList = -1;
	public static volatile int chunksUpdated = 0;
	public int posX;
	public int posY;
	public int posZ;

	/** Pos X minus */
	public int posXMinus;

	/** Pos Y minus */
	public int posYMinus;

	/** Pos Z minus */
	public int posZMinus;

	/** Pos X clipped */
	public int posXClip;

	/** Pos Y clipped */
	public int posYClip;

	/** Pos Z clipped */
	public int posZClip;
	public boolean isInFrustum = false;

	/** Should this renderer skip this render pass */
	public boolean[] skipRenderPass = new boolean[2];

	/** Pos X plus */
	public int posXPlus;

	/** Pos Y plus */
	public int posYPlus;

	/** Pos Z plus */
	public int posZPlus;

	/** Boolean for whether this renderer needs to be updated or not */
	public volatile boolean needsUpdate;

	/** Axis aligned bounding box */
	public AxisAlignedBB rendererBoundingBox;

	/** Chunk index */
	public int chunkIndex;

	/** Is this renderer visible according to the occlusion query */
	public boolean isVisible = true;

	/** Is this renderer waiting on the result of the occlusion query */
	public boolean isWaitingOnOcclusionQuery;

	/** OpenGL occlusion query */
	public int glOcclusionQuery;

	/** Is the chunk lit */
	public boolean isChunkLit;
	protected boolean isInitialized = false;

	/** All the tile entities that have special rendering code for this chunk */
	public List tileEntityRenderers = new ArrayList();
	protected List tileEntities;

	/** Bytes sent to the GPU */
	protected int bytesDrawn;
	public boolean isVisibleFromPosition = false;
	public double visibleFromX;
	public double visibleFromY;
	public double visibleFromZ;
	public boolean isInFrustrumFully = false;
	protected boolean needsBoxUpdate = false;
	public volatile boolean isUpdating = false;
	public static int globalChunkOffsetX = 0;
	public static int globalChunkOffsetZ = 0;

	public WorldRenderer(final World par1World, final List par2List,
			final int par3, final int par4, final int par5, final int par6) {
		worldObj = par1World;
		tileEntities = par2List;
		glRenderList = par6;
		posX = -999;
		setPosition(par3, par4, par5);
		needsUpdate = false;
	}

	/**
	 * Sets a new position for the renderer and setting it up so it can be
	 * reloaded with the new data for that position
	 */
	public void setPosition(final int par1, final int par2, final int par3) {
		if (par1 != posX || par2 != posY || par3 != posZ) {
			setDontDraw();
			posX = par1;
			posY = par2;
			posZ = par3;
			posXPlus = par1 + 8;
			posYPlus = par2 + 8;
			posZPlus = par3 + 8;
			posXClip = par1 & 1023;
			posYClip = par2;
			posZClip = par3 & 1023;
			posXMinus = par1 - posXClip;
			posYMinus = par2 - posYClip;
			posZMinus = par3 - posZClip;
			final float var4 = 0.0F;
			rendererBoundingBox = AxisAlignedBB.getBoundingBox(par1 - var4,
					par2 - var4, par3 - var4, par1 + 16 + var4, par2 + 16
							+ var4, par3 + 16 + var4);
			needsBoxUpdate = true;
			markDirty();
			isVisibleFromPosition = false;
		}
	}

	/**
	 * Will update this chunk renderer
	 */
	public void updateRenderer() {
		if (worldObj != null) {
			if (needsUpdate) {
				if (needsBoxUpdate) {
					final float var1 = 0.0F;
					GL11.glNewList(glRenderList + 2, GL11.GL_COMPILE);
					Render.renderAABB(AxisAlignedBB.getAABBPool().getAABB(
							posXClip - var1, posYClip - var1, posZClip - var1,
							posXClip + 16 + var1, posYClip + 16 + var1,
							posZClip + 16 + var1));
					GL11.glEndList();
					needsBoxUpdate = false;
				}

				isVisible = true;
				isVisibleFromPosition = false;
				needsUpdate = false;
				final int var24 = posX;
				final int var2 = posY;
				final int var3 = posZ;
				final int var4 = posX + 16;
				final int var5 = posY + 16;
				final int var6 = posZ + 16;

				for (int var7 = 0; var7 < 2; ++var7) {
					skipRenderPass[var7] = true;
				}

				if (Reflector.LightCache.exists()) {
					final Object var26 = Reflector
							.getFieldValue(Reflector.LightCache_cache);
					Reflector.callVoid(var26, Reflector.LightCache_clear,
							new Object[0]);
					Reflector.callVoid(Reflector.BlockCoord_resetPool,
							new Object[0]);
				}

				Chunk.isLit = false;
				final HashSet var25 = new HashSet();
				var25.addAll(tileEntityRenderers);
				tileEntityRenderers.clear();
				final byte var8 = 1;
				final ChunkCache var9 = new ChunkCache(worldObj, var24 - var8,
						var2 - var8, var3 - var8, var4 + var8, var5 + var8,
						var6 + var8, var8);

				if (!var9.extendedLevelsInChunkCache()) {
					++WorldRenderer.chunksUpdated;
					final RenderBlocks var10 = new RenderBlocks(var9);
					bytesDrawn = 0;
					final Tessellator var11 = Tessellator.instance;
					final boolean var12 = Reflector.ForgeHooksClient.exists();

					for (int var13 = 0; var13 < 2; ++var13) {
						boolean var14 = false;
						boolean var15 = false;
						boolean var16 = false;

						for (int var17 = var2; var17 < var5; ++var17) {
							for (int var18 = var3; var18 < var6; ++var18) {
								for (int var19 = var24; var19 < var4; ++var19) {
									final int var20 = var9.getBlockId(var19,
											var17, var18);

									if (var20 > 0) {
										if (!var16) {
											var16 = true;
											GL11.glNewList(
													glRenderList + var13,
													GL11.GL_COMPILE);
											var11.setRenderingChunk(true);
											var11.startDrawingQuads();
											var11.setTranslation(
													-WorldRenderer.globalChunkOffsetX,
													0.0D,
													-WorldRenderer.globalChunkOffsetZ);
										}

										final Block var21 = Block.blocksList[var20];

										if (var21 != null) {
											if (var13 == 0
													&& var21.hasTileEntity()) {
												final TileEntity var22 = var9
														.getBlockTileEntity(
																var19, var17,
																var18);

												if (TileEntityRenderer.instance
														.hasSpecialRenderer(var22)) {
													tileEntityRenderers
															.add(var22);
												}
											}

											final int var28 = var21
													.getRenderBlockPass();
											boolean var23 = true;

											if (var28 != var13) {
												var14 = true;
												var23 = false;
											}

											if (var12) {
												var23 = Reflector
														.callBoolean(
																var21,
																Reflector.ForgeBlock_canRenderInPass,
																new Object[] { Integer
																		.valueOf(var13) });
											}

											if (var23) {
												var15 |= var10
														.renderBlockByRenderType(
																var21, var19,
																var17, var18);
											}
										}
									}
								}
							}
						}

						if (var16) {
							bytesDrawn += var11.draw();
							GL11.glEndList();
							var11.setRenderingChunk(false);
							var11.setTranslation(0.0D, 0.0D, 0.0D);
						} else {
							var15 = false;
						}

						if (var15) {
							skipRenderPass[var13] = false;
						}

						if (!var14) {
							break;
						}
					}
				}

				final HashSet var27 = new HashSet();
				var27.addAll(tileEntityRenderers);
				var27.removeAll(var25);
				tileEntities.addAll(var27);
				var25.removeAll(tileEntityRenderers);
				tileEntities.removeAll(var25);
				isChunkLit = Chunk.isLit;
				isInitialized = true;
			}
		}
	}

	/**
	 * Returns the distance of this chunk renderer to the entity without
	 * performing the final normalizing square root, for performance reasons.
	 */
	public float distanceToEntitySquared(final Entity par1Entity) {
		final float var2 = (float) (par1Entity.posX - posXPlus);
		final float var3 = (float) (par1Entity.posY - posYPlus);
		final float var4 = (float) (par1Entity.posZ - posZPlus);
		return var2 * var2 + var3 * var3 + var4 * var4;
	}

	/**
	 * When called this renderer won't draw anymore until its gets initialized
	 * again
	 */
	public void setDontDraw() {
		for (int var1 = 0; var1 < 2; ++var1) {
			skipRenderPass[var1] = true;
		}

		isInFrustum = false;
		isInitialized = false;
	}

	public void stopRendering() {
		setDontDraw();
		worldObj = null;
	}

	/**
	 * Takes in the pass the call list is being requested for. Args: renderPass
	 */
	public int getGLCallListForPass(final int par1) {
		return !isInFrustum ? -1 : !skipRenderPass[par1] ? glRenderList + par1
				: -1;
	}

	public void updateInFrustum(final ICamera par1ICamera) {
		isInFrustum = par1ICamera.isBoundingBoxInFrustum(rendererBoundingBox);

		if (isInFrustum && Config.isOcclusionEnabled()
				&& Config.isOcclusionFancy()) {
			isInFrustrumFully = par1ICamera
					.isBoundingBoxInFrustumFully(rendererBoundingBox);
		} else {
			isInFrustrumFully = false;
		}
	}

	/**
	 * Renders the occlusion query GL List
	 */
	public void callOcclusionQueryList() {
		GL11.glCallList(glRenderList + 2);
	}

	/**
	 * Checks if all render passes are to be skipped. Returns false if the
	 * renderer is not initialized
	 */
	public boolean skipAllRenderPasses() {
		return !isInitialized ? false : skipRenderPass[0] && skipRenderPass[1];
	}

	/**
	 * Marks the current renderer data as dirty and needing to be updated.
	 */
	public void markDirty() {
		needsUpdate = true;
	}
}
