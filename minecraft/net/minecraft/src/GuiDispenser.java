package net.minecraft.src;

import org.lwjgl.opengl.GL11;

public class GuiDispenser extends GuiContainer {
	public TileEntityDispenser field_94078_r;

	public GuiDispenser(final InventoryPlayer par1InventoryPlayer,
			final TileEntityDispenser par2TileEntityDispenser) {
		super(new ContainerDispenser(par1InventoryPlayer,
				par2TileEntityDispenser));
		field_94078_r = par2TileEntityDispenser;
	}

	/**
	 * Draw the foreground layer for the GuiContainer (everything in front of
	 * the items)
	 */
	@Override
	protected void drawGuiContainerForegroundLayer(final int par1,
			final int par2) {
		final String var3 = field_94078_r.isInvNameLocalized() ? field_94078_r
				.getInvName() : StatCollector.translateToLocal(field_94078_r
				.getInvName());
		fontRenderer.drawString(var3,
				xSize / 2 - fontRenderer.getStringWidth(var3) / 2, 6, 4210752);
		fontRenderer.drawString(
				StatCollector.translateToLocal("container.inventory"), 8,
				ySize - 96 + 2, 4210752);
	}

	/**
	 * Draw the background layer for the GuiContainer (everything behind the
	 * items)
	 */
	@Override
	protected void drawGuiContainerBackgroundLayer(final float par1,
			final int par2, final int par3) {
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		mc.renderEngine.bindTexture("/gui/trap.png");
		final int var4 = (width - xSize) / 2;
		final int var5 = (height - ySize) / 2;
		drawTexturedModalRect(var4, var5, 0, 0, xSize, ySize);
	}
}
