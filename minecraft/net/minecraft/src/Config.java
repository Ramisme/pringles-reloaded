package net.minecraft.src;

import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;
import java.util.StringTokenizer;

import net.minecraft.client.Minecraft;

import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLContext;
import org.lwjgl.util.glu.GLU;

public class Config {
	public static final String OF_NAME = "OptiFine";
	public static final String MC_VERSION = "1.5.2";
	public static final String OF_EDITION = "HD";
	public static final String OF_RELEASE = "D2";
	public static final String VERSION = "OptiFine_1.5.2_HD_D2";
	private static String newRelease = null;
	private static GameSettings gameSettings = null;
	private static Minecraft minecraft = null;
	private static Thread minecraftThread = null;
	private static DisplayMode desktopDisplayMode = null;
	private static int availableProcessors = 0;
	public static boolean zoomMode = false;
	private static int texturePackClouds = 0;
	private static PrintStream systemOut = new PrintStream(
			new FileOutputStream(FileDescriptor.out));
	public static final Boolean DEF_FOG_FANCY = Boolean.valueOf(true);
	public static final Float DEF_FOG_START = Float.valueOf(0.2F);
	public static final Boolean DEF_OPTIMIZE_RENDER_DISTANCE = Boolean
			.valueOf(false);
	public static final Boolean DEF_OCCLUSION_ENABLED = Boolean.valueOf(false);
	public static final Integer DEF_MIPMAP_LEVEL = Integer.valueOf(0);
	public static final Integer DEF_MIPMAP_TYPE = Integer.valueOf(9984);
	public static final Float DEF_ALPHA_FUNC_LEVEL = Float.valueOf(0.1F);
	public static final Boolean DEF_LOAD_CHUNKS_FAR = Boolean.valueOf(false);
	public static final Integer DEF_PRELOADED_CHUNKS = Integer.valueOf(0);
	public static final Integer DEF_CHUNKS_LIMIT = Integer.valueOf(25);
	public static final Integer DEF_UPDATES_PER_FRAME = Integer.valueOf(3);
	public static final Boolean DEF_DYNAMIC_UPDATES = Boolean.valueOf(false);

	public static String getVersion() {
		return "OptiFine_1.5.2_HD_D2";
	}

	private static void checkOpenGlCaps() {
		Config.log("");
		Config.log(Config.getVersion());
		Config.log("" + new Date());
		Config.log("OS: " + System.getProperty("os.name") + " ("
				+ System.getProperty("os.arch") + ") version "
				+ System.getProperty("os.version"));
		Config.log("Java: " + System.getProperty("java.version") + ", "
				+ System.getProperty("java.vendor"));
		Config.log("VM: " + System.getProperty("java.vm.name") + " ("
				+ System.getProperty("java.vm.info") + "), "
				+ System.getProperty("java.vm.vendor"));
		Config.log("LWJGL: " + Sys.getVersion());
		Config.log("OpenGL: " + GL11.glGetString(GL11.GL_RENDERER)
				+ " version " + GL11.glGetString(GL11.GL_VERSION) + ", "
				+ GL11.glGetString(GL11.GL_VENDOR));
		final int var0 = Config.getOpenGlVersion();
		final String var1 = "" + var0 / 10 + "." + var0 % 10;
		Config.log("OpenGL Version: " + var1);

		if (!GLContext.getCapabilities().OpenGL12) {
			Config.log("OpenGL Mipmap levels: Not available (GL12.GL_TEXTURE_MAX_LEVEL)");
		}

		if (!GLContext.getCapabilities().GL_NV_fog_distance) {
			Config.log("OpenGL Fancy fog: Not available (GL_NV_fog_distance)");
		}

		if (!GLContext.getCapabilities().GL_ARB_occlusion_query) {
			Config.log("OpenGL Occlussion culling: Not available (GL_ARB_occlusion_query)");
		}

		final int var2 = Minecraft.getGLMaximumTextureSize();
		Config.dbg("Maximum texture size: " + var2 + "x" + var2);
	}

	public static boolean isFancyFogAvailable() {
		return GLContext.getCapabilities().GL_NV_fog_distance;
	}

	public static boolean isOcclusionAvailable() {
		return GLContext.getCapabilities().GL_ARB_occlusion_query;
	}

	private static int getOpenGlVersion() {
		return !GLContext.getCapabilities().OpenGL11 ? 10 : !GLContext
				.getCapabilities().OpenGL12 ? 11
				: !GLContext.getCapabilities().OpenGL13 ? 12 : !GLContext
						.getCapabilities().OpenGL14 ? 13 : !GLContext
						.getCapabilities().OpenGL15 ? 14 : !GLContext
						.getCapabilities().OpenGL20 ? 15 : !GLContext
						.getCapabilities().OpenGL21 ? 20 : !GLContext
						.getCapabilities().OpenGL30 ? 21 : !GLContext
						.getCapabilities().OpenGL31 ? 30 : !GLContext
						.getCapabilities().OpenGL32 ? 31 : !GLContext
						.getCapabilities().OpenGL33 ? 32 : !GLContext
						.getCapabilities().OpenGL40 ? 33 : 40;
	}

	public static void setGameSettings(final GameSettings var0) {
		if (Config.gameSettings == null) {
			if (!Display.isCreated()) {
				return;
			}

			Config.checkOpenGlCaps();
			Config.startVersionCheckThread();
		}

		Config.gameSettings = var0;
		Config.minecraft = Config.gameSettings.mc;
		Config.minecraftThread = Thread.currentThread();

		if (Config.gameSettings != null) {
		}

		Config.updateThreadPriorities();
	}

	public static void updateThreadPriorities() {
		try {
			final ThreadGroup var0 = Thread.currentThread().getThreadGroup();

			if (var0 == null) {
				return;
			}

			final int var1 = (var0.activeCount() + 10) * 2;
			final Thread[] var2 = new Thread[var1];
			var0.enumerate(var2, false);
			final byte var3 = 5;
			byte var4 = 5;

			if (Config.isSmoothWorld()) {
				var4 = 3;
			}

			Config.minecraftThread.setPriority(var3);

			for (final Thread var6 : var2) {
				if (var6 != null && var6 instanceof ThreadMinecraftServer) {
					var6.setPriority(var4);
				}
			}
		} catch (final Throwable var7) {
			Config.dbg(var7.getMessage());
		}
	}

	public static boolean isMinecraftThread() {
		return Thread.currentThread() == Config.minecraftThread;
	}

	private static void startVersionCheckThread() {
		final VersionCheckThread var0 = new VersionCheckThread();
		var0.start();
	}

	public static boolean isUseMipmaps() {
		final int var0 = Config.getMipmapLevel();
		return var0 > 0;
	}

	public static int getMipmapLevel() {
		return Config.gameSettings == null ? Config.DEF_MIPMAP_LEVEL.intValue()
				: Config.gameSettings.ofMipmapLevel;
	}

	public static int getMipmapType() {
		if (Config.gameSettings == null) {
			return Config.DEF_MIPMAP_TYPE.intValue();
		} else {
			switch (Config.gameSettings.ofMipmapType) {
			case 0:
				return 9984;

			case 1:
				return 9986;

			case 2:
				if (Config.isMultiTexture()) {
					return 9985;
				}

				return 9986;

			case 3:
				if (Config.isMultiTexture()) {
					return 9987;
				}

				return 9986;

			default:
				return 9984;
			}
		}
	}

	public static boolean isUseAlphaFunc() {
		final float var0 = Config.getAlphaFuncLevel();
		return var0 > Config.DEF_ALPHA_FUNC_LEVEL.floatValue() + 1.0E-5F;
	}

	public static float getAlphaFuncLevel() {
		return Config.DEF_ALPHA_FUNC_LEVEL.floatValue();
	}

	public static boolean isFogFancy() {
		return !Config.isFancyFogAvailable() ? false
				: Config.gameSettings == null ? false
						: Config.gameSettings.ofFogType == 2;
	}

	public static boolean isFogFast() {
		return Config.gameSettings == null ? false
				: Config.gameSettings.ofFogType == 1;
	}

	public static boolean isFogOff() {
		return Config.gameSettings == null ? false
				: Config.gameSettings.ofFogType == 3;
	}

	public static float getFogStart() {
		return Config.gameSettings == null ? Config.DEF_FOG_START.floatValue()
				: Config.gameSettings.ofFogStart;
	}

	public static boolean isOcclusionEnabled() {
		return Config.gameSettings == null ? Config.DEF_OCCLUSION_ENABLED
				.booleanValue() : Config.gameSettings.advancedOpengl;
	}

	public static boolean isOcclusionFancy() {
		return !Config.isOcclusionEnabled() ? false
				: Config.gameSettings == null ? false
						: Config.gameSettings.ofOcclusionFancy;
	}

	public static boolean isLoadChunksFar() {
		return Config.gameSettings == null ? Config.DEF_LOAD_CHUNKS_FAR
				.booleanValue() : Config.gameSettings.ofLoadFar;
	}

	public static int getPreloadedChunks() {
		return Config.gameSettings == null ? Config.DEF_PRELOADED_CHUNKS
				.intValue() : Config.gameSettings.ofPreloadedChunks;
	}

	public static void dbg(final String var0) {
		Config.systemOut.print("[OptiFine] ");
		Config.systemOut.println(var0);
	}

	public static void log(final String var0) {
		Config.dbg(var0);
	}

	public static int getUpdatesPerFrame() {
		return Config.gameSettings != null ? Config.gameSettings.ofChunkUpdates
				: 1;
	}

	public static boolean isDynamicUpdates() {
		return Config.gameSettings != null ? Config.gameSettings.ofChunkUpdatesDynamic
				: true;
	}

	public static boolean isRainFancy() {
		return Config.gameSettings.ofRain == 0 ? Config.gameSettings.fancyGraphics
				: Config.gameSettings.ofRain == 2;
	}

	public static boolean isWaterFancy() {
		return Config.gameSettings.ofWater == 0 ? Config.gameSettings.fancyGraphics
				: Config.gameSettings.ofWater == 2;
	}

	public static boolean isRainOff() {
		return Config.gameSettings.ofRain == 3;
	}

	public static boolean isCloudsFancy() {
		return Config.gameSettings.ofClouds != 0 ? Config.gameSettings.ofClouds == 2
				: Config.texturePackClouds != 0 ? Config.texturePackClouds == 2
						: Config.gameSettings.fancyGraphics;
	}

	public static boolean isCloudsOff() {
		return Config.gameSettings.ofClouds == 3;
	}

	public static void updateTexturePackClouds() {
		Config.texturePackClouds = 0;
		final RenderEngine var0 = Config.getRenderEngine();

		if (var0 != null) {
			final ITexturePack var1 = var0.getTexturePack()
					.getSelectedTexturePack();

			if (var1 != null) {
				try {
					final InputStream var2 = var1
							.getResourceAsStream("/color.properties");

					if (var2 == null) {
						return;
					}

					final Properties var3 = new Properties();
					var3.load(var2);
					var2.close();
					String var4 = var3.getProperty("clouds");

					if (var4 == null) {
						return;
					}

					Config.dbg("Texture pack clouds: " + var4);
					var4 = var4.toLowerCase();

					if (var4.equals("fast")) {
						Config.texturePackClouds = 1;
					}

					if (var4.equals("fancy")) {
						Config.texturePackClouds = 2;
					}
				} catch (final Exception var5) {
					;
				}
			}
		}
	}

	public static boolean isTreesFancy() {
		return Config.gameSettings.ofTrees == 0 ? Config.gameSettings.fancyGraphics
				: Config.gameSettings.ofTrees == 2;
	}

	public static boolean isGrassFancy() {
		return Config.gameSettings.ofGrass == 0 ? Config.gameSettings.fancyGraphics
				: Config.gameSettings.ofGrass == 2;
	}

	public static boolean isDroppedItemsFancy() {
		return Config.gameSettings.ofDroppedItems == 0 ? Config.gameSettings.fancyGraphics
				: Config.gameSettings.ofDroppedItems == 2;
	}

	public static int limit(final int var0, final int var1, final int var2) {
		return var0 < var1 ? var1 : var0 > var2 ? var2 : var0;
	}

	public static float limit(final float var0, final float var1,
			final float var2) {
		return var0 < var1 ? var1 : var0 > var2 ? var2 : var0;
	}

	public static float limitTo1(final float var0) {
		return var0 < 0.0F ? 0.0F : var0 > 1.0F ? 1.0F : var0;
	}

	public static boolean isAnimatedWater() {
		return Config.gameSettings != null ? Config.gameSettings.ofAnimatedWater != 2
				: true;
	}

	public static boolean isGeneratedWater() {
		return Config.gameSettings != null ? Config.gameSettings.ofAnimatedWater == 1
				: true;
	}

	public static boolean isAnimatedPortal() {
		return Config.gameSettings != null ? Config.gameSettings.ofAnimatedPortal
				: true;
	}

	public static boolean isAnimatedLava() {
		return Config.gameSettings != null ? Config.gameSettings.ofAnimatedLava != 2
				: true;
	}

	public static boolean isGeneratedLava() {
		return Config.gameSettings != null ? Config.gameSettings.ofAnimatedLava == 1
				: true;
	}

	public static boolean isAnimatedFire() {
		return Config.gameSettings != null ? Config.gameSettings.ofAnimatedFire
				: true;
	}

	public static boolean isAnimatedRedstone() {
		return Config.gameSettings != null ? Config.gameSettings.ofAnimatedRedstone
				: true;
	}

	public static boolean isAnimatedExplosion() {
		return Config.gameSettings != null ? Config.gameSettings.ofAnimatedExplosion
				: true;
	}

	public static boolean isAnimatedFlame() {
		return Config.gameSettings != null ? Config.gameSettings.ofAnimatedFlame
				: true;
	}

	public static boolean isAnimatedSmoke() {
		return Config.gameSettings != null ? Config.gameSettings.ofAnimatedSmoke
				: true;
	}

	public static boolean isVoidParticles() {
		return Config.gameSettings != null ? Config.gameSettings.ofVoidParticles
				: true;
	}

	public static boolean isWaterParticles() {
		return Config.gameSettings != null ? Config.gameSettings.ofWaterParticles
				: true;
	}

	public static boolean isRainSplash() {
		return Config.gameSettings != null ? Config.gameSettings.ofRainSplash
				: true;
	}

	public static boolean isPortalParticles() {
		return Config.gameSettings != null ? Config.gameSettings.ofPortalParticles
				: true;
	}

	public static boolean isPotionParticles() {
		return Config.gameSettings != null ? Config.gameSettings.ofPotionParticles
				: true;
	}

	public static boolean isDepthFog() {
		return Config.gameSettings != null ? Config.gameSettings.ofDepthFog
				: true;
	}

	public static float getAmbientOcclusionLevel() {
		return Config.gameSettings != null ? Config.gameSettings.ofAoLevel
				: 0.0F;
	}

	public static String arrayToString(final Object[] var0) {
		if (var0 == null) {
			return "";
		} else {
			final StringBuffer var1 = new StringBuffer(var0.length * 5);

			for (int var2 = 0; var2 < var0.length; ++var2) {
				final Object var3 = var0[var2];

				if (var2 > 0) {
					var1.append(", ");
				}

				var1.append(String.valueOf(var3));
			}

			return var1.toString();
		}
	}

	public static String arrayToString(final int[] var0) {
		if (var0 == null) {
			return "";
		} else {
			final StringBuffer var1 = new StringBuffer(var0.length * 5);

			for (int var2 = 0; var2 < var0.length; ++var2) {
				final int var3 = var0[var2];

				if (var2 > 0) {
					var1.append(", ");
				}

				var1.append(String.valueOf(var3));
			}

			return var1.toString();
		}
	}

	public static Minecraft getMinecraft() {
		return Config.minecraft;
	}

	public static RenderEngine getRenderEngine() {
		return Config.minecraft.renderEngine;
	}

	public static RenderGlobal getRenderGlobal() {
		return Config.minecraft == null ? null : Config.minecraft.renderGlobal;
	}

	public static int getMaxDynamicTileWidth() {
		return 64;
	}

	public static Icon getSideGrassTexture(final IBlockAccess var0, int var1,
			int var2, int var3, final int var4, final Icon var5) {
		if (!Config.isBetterGrass()) {
			return var5;
		} else {
			Icon var6 = TextureUtils.iconGrassTop;
			byte var7 = 2;

			if (var5 == TextureUtils.iconMycelSide) {
				var6 = TextureUtils.iconMycelTop;
				var7 = 110;
			}

			if (Config.isBetterGrassFancy()) {
				--var2;

				switch (var4) {
				case 2:
					--var3;
					break;

				case 3:
					++var3;
					break;

				case 4:
					--var1;
					break;

				case 5:
					++var1;
				}

				final int var8 = var0.getBlockId(var1, var2, var3);

				if (var8 != var7) {
					return var5;
				}
			}

			return var6;
		}
	}

	public static Icon getSideSnowGrassTexture(final IBlockAccess var0,
			int var1, final int var2, int var3, final int var4) {
		if (!Config.isBetterGrass()) {
			return TextureUtils.iconSnowSide;
		} else {
			if (Config.isBetterGrassFancy()) {
				switch (var4) {
				case 2:
					--var3;
					break;

				case 3:
					++var3;
					break;

				case 4:
					--var1;
					break;

				case 5:
					++var1;
				}

				final int var5 = var0.getBlockId(var1, var2, var3);

				if (var5 != 78 && var5 != 80) {
					return TextureUtils.iconSnowSide;
				}
			}

			return TextureUtils.iconSnow;
		}
	}

	public static boolean isBetterGrass() {
		return Config.gameSettings == null ? false
				: Config.gameSettings.ofBetterGrass != 3;
	}

	public static boolean isBetterGrassFancy() {
		return Config.gameSettings == null ? false
				: Config.gameSettings.ofBetterGrass == 2;
	}

	public static boolean isWeatherEnabled() {
		return Config.gameSettings == null ? true
				: Config.gameSettings.ofWeather;
	}

	public static boolean isSkyEnabled() {
		return Config.gameSettings == null ? true : Config.gameSettings.ofSky;
	}

	public static boolean isSunMoonEnabled() {
		return Config.gameSettings == null ? true
				: Config.gameSettings.ofSunMoon;
	}

	public static boolean isStarsEnabled() {
		return Config.gameSettings == null ? true : Config.gameSettings.ofStars;
	}

	public static void sleep(final long var0) {
		try {
			Thread.currentThread();
			Thread.sleep(var0);
		} catch (final InterruptedException var3) {
			var3.printStackTrace();
		}
	}

	public static boolean isTimeDayOnly() {
		return Config.gameSettings == null ? false
				: Config.gameSettings.ofTime == 1;
	}

	public static boolean isTimeDefault() {
		return Config.gameSettings == null ? false
				: Config.gameSettings.ofTime == 0
						|| Config.gameSettings.ofTime == 2;
	}

	public static boolean isTimeNightOnly() {
		return Config.gameSettings == null ? false
				: Config.gameSettings.ofTime == 3;
	}

	public static boolean isClearWater() {
		return Config.gameSettings == null ? false
				: Config.gameSettings.ofClearWater;
	}

	public static int getAnisotropicFilterLevel() {
		return 1;
	}

	public static int getAntialiasingLevel() {
		return 0;
	}

	public static boolean between(final int var0, final int var1, final int var2) {
		return var0 >= var1 && var0 <= var2;
	}

	public static boolean isMultiTexture() {
		return false;
	}

	public static boolean isDrippingWaterLava() {
		return Config.gameSettings == null ? false
				: Config.gameSettings.ofDrippingWaterLava;
	}

	public static boolean isBetterSnow() {
		return Config.gameSettings == null ? false
				: Config.gameSettings.ofBetterSnow;
	}

	public static Dimension getFullscreenDimension() {
		if (Config.desktopDisplayMode == null) {
			return null;
		} else if (Config.gameSettings == null) {
			return new Dimension(Config.desktopDisplayMode.getWidth(),
					Config.desktopDisplayMode.getHeight());
		} else {
			final String var0 = Config.gameSettings.ofFullscreenMode;

			if (var0.equals("Default")) {
				return new Dimension(Config.desktopDisplayMode.getWidth(),
						Config.desktopDisplayMode.getHeight());
			} else {
				final String[] var1 = Config.tokenize(var0, " x");
				return var1.length < 2 ? new Dimension(
						Config.desktopDisplayMode.getWidth(),
						Config.desktopDisplayMode.getHeight()) : new Dimension(
						Config.parseInt(var1[0], -1), Config.parseInt(var1[1],
								-1));
			}
		}
	}

	public static int parseInt(final String var0, final int var1) {
		try {
			return var0 == null ? var1 : Integer.parseInt(var0);
		} catch (final NumberFormatException var3) {
			return var1;
		}
	}

	public static float parseFloat(final String var0, final float var1) {
		try {
			return var0 == null ? var1 : Float.parseFloat(var0);
		} catch (final NumberFormatException var3) {
			return var1;
		}
	}

	public static String[] tokenize(final String var0, final String var1) {
		final StringTokenizer var2 = new StringTokenizer(var0, var1);
		final ArrayList var3 = new ArrayList();

		while (var2.hasMoreTokens()) {
			final String var4 = var2.nextToken();
			var3.add(var4);
		}

		final String[] var5 = (String[]) var3.toArray(new String[var3.size()]);
		return var5;
	}

	public static DisplayMode getDesktopDisplayMode() {
		return Config.desktopDisplayMode;
	}

	public static void setDesktopDisplayMode(final DisplayMode var0) {
		Config.desktopDisplayMode = var0;
	}

	public static DisplayMode[] getFullscreenDisplayModes() {
		try {
			final DisplayMode[] var0 = Display.getAvailableDisplayModes();
			final ArrayList var1 = new ArrayList();

			for (final DisplayMode var3 : var0) {
				if (Config.desktopDisplayMode == null
						|| var3.getBitsPerPixel() == Config.desktopDisplayMode
								.getBitsPerPixel()
						&& var3.getFrequency() == Config.desktopDisplayMode
								.getFrequency()) {
					var1.add(var3);
				}
			}

			final DisplayMode[] var5 = (DisplayMode[]) var1
					.toArray(new DisplayMode[var1.size()]);
			final Config$1 var6 = new Config$1();
			Arrays.sort(var5, var6);
			return var5;
		} catch (final Exception var4) {
			var4.printStackTrace();
			return new DisplayMode[] { Config.desktopDisplayMode };
		}
	}

	public static String[] getFullscreenModes() {
		final DisplayMode[] var0 = Config.getFullscreenDisplayModes();
		final String[] var1 = new String[var0.length];

		for (int var2 = 0; var2 < var0.length; ++var2) {
			final DisplayMode var3 = var0[var2];
			final String var4 = "" + var3.getWidth() + "x" + var3.getHeight();
			var1[var2] = var4;
		}

		return var1;
	}

	public static DisplayMode getDisplayMode(final Dimension var0)
			throws LWJGLException {
		final DisplayMode[] var1 = Display.getAvailableDisplayModes();

		for (final DisplayMode var3 : var1) {
			if (var3.getWidth() == var0.width
					&& var3.getHeight() == var0.height
					&& (Config.desktopDisplayMode == null || var3
							.getBitsPerPixel() == Config.desktopDisplayMode
							.getBitsPerPixel()
							&& var3.getFrequency() == Config.desktopDisplayMode
									.getFrequency())) {
				return var3;
			}
		}

		return Config.desktopDisplayMode;
	}

	public static boolean isAnimatedTerrain() {
		return Config.gameSettings != null ? Config.gameSettings.ofAnimatedTerrain
				: true;
	}

	public static boolean isAnimatedItems() {
		return Config.gameSettings != null ? Config.gameSettings.ofAnimatedItems
				: true;
	}

	public static boolean isAnimatedTextures() {
		return Config.gameSettings != null ? Config.gameSettings.ofAnimatedTextures
				: true;
	}

	public static boolean isSwampColors() {
		return Config.gameSettings != null ? Config.gameSettings.ofSwampColors
				: true;
	}

	public static boolean isRandomMobs() {
		return Config.gameSettings != null ? Config.gameSettings.ofRandomMobs
				: true;
	}

	public static void checkGlError(final String var0) {
		final int var1 = GL11.glGetError();

		if (var1 != 0) {
			final String var2 = GLU.gluErrorString(var1);
			Config.dbg("OpenGlError: " + var1 + " (" + var2 + "), at: " + var0);
		}
	}

	public static boolean isSmoothBiomes() {
		return Config.gameSettings != null ? Config.gameSettings.ofSmoothBiomes
				: true;
	}

	public static boolean isCustomColors() {
		return Config.gameSettings != null ? Config.gameSettings.ofCustomColors
				: true;
	}

	public static boolean isCustomSky() {
		return Config.gameSettings != null ? Config.gameSettings.ofCustomSky
				: true;
	}

	public static boolean isCustomFonts() {
		return Config.gameSettings != null ? Config.gameSettings.ofCustomFonts
				: true;
	}

	public static boolean isShowCapes() {
		return Config.gameSettings != null ? Config.gameSettings.ofShowCapes
				: true;
	}

	public static boolean isConnectedTextures() {
		return Config.gameSettings != null ? Config.gameSettings.ofConnectedTextures != 3
				: false;
	}

	public static boolean isNaturalTextures() {
		return Config.gameSettings != null ? Config.gameSettings.ofNaturalTextures
				: false;
	}

	public static boolean isConnectedTexturesFancy() {
		return Config.gameSettings != null ? Config.gameSettings.ofConnectedTextures == 2
				: false;
	}

	public static String[] readLines(final File var0) throws IOException {
		final ArrayList var1 = new ArrayList();
		final FileInputStream var2 = new FileInputStream(var0);
		final InputStreamReader var3 = new InputStreamReader(var2, "ASCII");
		final BufferedReader var4 = new BufferedReader(var3);

		while (true) {
			final String var5 = var4.readLine();

			if (var5 == null) {
				final String[] var6 = (String[]) var1.toArray(new String[var1
						.size()]);
				var4.close();
				return var6;
			}

			var1.add(var5);
		}
	}

	public static String readFile(final File var0) throws IOException {
		final FileInputStream var1 = new FileInputStream(var0);
		return Config.readInputStream(var1, "ASCII");
	}

	public static String readInputStream(final InputStream var0)
			throws IOException {
		return Config.readInputStream(var0, "ASCII");
	}

	public static String readInputStream(final InputStream var0,
			final String var1) throws IOException {
		final InputStreamReader var2 = new InputStreamReader(var0, var1);
		final BufferedReader var3 = new BufferedReader(var2);
		final StringBuffer var4 = new StringBuffer();

		while (true) {
			final String var5 = var3.readLine();

			if (var5 == null) {
				return var4.toString();
			}

			var4.append(var5);
			var4.append("\n");
		}
	}

	public static GameSettings getGameSettings() {
		return Config.gameSettings;
	}

	public static String getNewRelease() {
		return Config.newRelease;
	}

	public static void setNewRelease(final String var0) {
		Config.newRelease = var0;
	}

	public static int compareRelease(final String var0, final String var1) {
		final String[] var2 = Config.splitRelease(var0);
		final String[] var3 = Config.splitRelease(var1);
		final String var4 = var2[0];
		final String var5 = var3[0];

		if (!var4.equals(var5)) {
			return var4.compareTo(var5);
		} else {
			final int var6 = Config.parseInt(var2[1], -1);
			final int var7 = Config.parseInt(var3[1], -1);

			if (var6 != var7) {
				return var6 - var7;
			} else {
				final String var8 = var2[2];
				final String var9 = var3[2];
				return var8.compareTo(var9);
			}
		}
	}

	private static String[] splitRelease(final String var0) {
		if (var0 != null && var0.length() > 0) {
			final String var1 = var0.substring(0, 1);

			if (var0.length() <= 1) {
				return new String[] { var1, "", "" };
			} else {
				int var2;

				for (var2 = 1; var2 < var0.length()
						&& Character.isDigit(var0.charAt(var2)); ++var2) {
					;
				}

				final String var3 = var0.substring(1, var2);

				if (var2 >= var0.length()) {
					return new String[] { var1, var3, "" };
				} else {
					final String var4 = var0.substring(var2);
					return new String[] { var1, var3, var4 };
				}
			}
		} else {
			return new String[] { "", "", "" };
		}
	}

	public static int intHash(int var0) {
		var0 = var0 ^ 61 ^ var0 >> 16;
		var0 += var0 << 3;
		var0 ^= var0 >> 4;
		var0 *= 668265261;
		var0 ^= var0 >> 15;
		return var0;
	}

	public static int getRandom(final int var0, final int var1, final int var2,
			final int var3) {
		int var4 = Config.intHash(var3 + 37);
		var4 = Config.intHash(var4 + var0);
		var4 = Config.intHash(var4 + var2);
		var4 = Config.intHash(var4 + var1);
		return var4;
	}

	public static WorldServer getWorldServer() {
		if (Config.minecraft == null) {
			return null;
		} else {
			final WorldClient var0 = Config.minecraft.theWorld;

			if (var0 == null) {
				return null;
			} else {
				final WorldProvider var1 = var0.provider;

				if (var1 == null) {
					return null;
				} else {
					final int var2 = var1.dimensionId;
					final IntegratedServer var3 = Config.minecraft
							.getIntegratedServer();

					if (var3 == null) {
						return null;
					} else {
						final WorldServer var4 = var3
								.worldServerForDimension(var2);
						return var4;
					}
				}
			}
		}
	}

	public static int getAvailableProcessors() {
		if (Config.availableProcessors < 1) {
			Config.availableProcessors = Runtime.getRuntime()
					.availableProcessors();
		}

		return Config.availableProcessors;
	}

	public static boolean isSingleProcessor() {
		return Config.getAvailableProcessors() <= 1;
	}

	public static boolean isSmoothWorld() {
		return Config.getAvailableProcessors() > 1 ? false
				: Config.gameSettings == null ? true
						: Config.gameSettings.ofSmoothWorld;
	}

	public static boolean isLazyChunkLoading() {
		return Config.getAvailableProcessors() > 1 ? false
				: Config.gameSettings == null ? true
						: Config.gameSettings.ofLazyChunkLoading;
	}

	public static int getChunkViewDistance() {
		if (Config.gameSettings == null) {
			return 10;
		} else {
			final int var0 = Config.gameSettings.ofRenderDistanceFine / 16;
			return var0 <= 16 ? 10 : var0;
		}
	}
}
