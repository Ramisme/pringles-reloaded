package net.minecraft.src;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class MapGenMineshaft extends MapGenStructure {
	private double field_82673_e = 0.01D;

	public MapGenMineshaft() {
	}

	public MapGenMineshaft(final Map par1Map) {
		final Iterator var2 = par1Map.entrySet().iterator();

		while (var2.hasNext()) {
			final Entry var3 = (Entry) var2.next();

			if (((String) var3.getKey()).equals("chance")) {
				field_82673_e = MathHelper.parseDoubleWithDefault(
						(String) var3.getValue(), field_82673_e);
			}
		}
	}

	@Override
	protected boolean canSpawnStructureAtCoords(final int par1, final int par2) {
		return rand.nextDouble() < field_82673_e
				&& rand.nextInt(80) < Math.max(Math.abs(par1), Math.abs(par2));
	}

	@Override
	protected StructureStart getStructureStart(final int par1, final int par2) {
		return new StructureMineshaftStart(worldObj, rand, par1, par2);
	}
}
