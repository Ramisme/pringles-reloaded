package net.minecraft.src;

import org.lwjgl.opengl.GL11;

public class RenderBat extends RenderLiving {
	/**
	 * not actually sure this is size, is not used as of now, but the model
	 * would be recreated if the value changed and it seems a good match for a
	 * bats size
	 */
	private int renderedBatSize;

	public RenderBat() {
		super(new ModelBat(), 0.25F);
		renderedBatSize = ((ModelBat) mainModel).getBatSize();
	}

	public void func_82443_a(final EntityBat par1EntityBat, final double par2,
			final double par4, final double par6, final float par8,
			final float par9) {
		final int var10 = ((ModelBat) mainModel).getBatSize();

		if (var10 != renderedBatSize) {
			renderedBatSize = var10;
			mainModel = new ModelBat();
		}

		super.doRenderLiving(par1EntityBat, par2, par4, par6, par8, par9);
	}

	protected void func_82442_a(final EntityBat par1EntityBat, final float par2) {
		GL11.glScalef(0.35F, 0.35F, 0.35F);
	}

	protected void func_82445_a(final EntityBat par1EntityBat,
			final double par2, final double par4, final double par6) {
		super.renderLivingAt(par1EntityBat, par2, par4, par6);
	}

	protected void func_82444_a(final EntityBat par1EntityBat,
			final float par2, final float par3, final float par4) {
		if (!par1EntityBat.getIsBatHanging()) {
			GL11.glTranslatef(0.0F, MathHelper.cos(par2 * 0.3F) * 0.1F, 0.0F);
		} else {
			GL11.glTranslatef(0.0F, -0.1F, 0.0F);
		}

		super.rotateCorpse(par1EntityBat, par2, par3, par4);
	}

	/**
	 * Allows the render to do any OpenGL state modifications necessary before
	 * the model is rendered. Args: entityLiving, partialTickTime
	 */
	@Override
	protected void preRenderCallback(final EntityLiving par1EntityLiving,
			final float par2) {
		func_82442_a((EntityBat) par1EntityLiving, par2);
	}

	@Override
	protected void rotateCorpse(final EntityLiving par1EntityLiving,
			final float par2, final float par3, final float par4) {
		func_82444_a((EntityBat) par1EntityLiving, par2, par3, par4);
	}

	/**
	 * Sets a simple glTranslate on a LivingEntity.
	 */
	@Override
	protected void renderLivingAt(final EntityLiving par1EntityLiving,
			final double par2, final double par4, final double par6) {
		func_82445_a((EntityBat) par1EntityLiving, par2, par4, par6);
	}

	@Override
	public void doRenderLiving(final EntityLiving par1EntityLiving,
			final double par2, final double par4, final double par6,
			final float par8, final float par9) {
		func_82443_a((EntityBat) par1EntityLiving, par2, par4, par6, par8, par9);
	}

	/**
	 * Actually renders the given argument. This is a synthetic bridge method,
	 * always casting down its argument and then handing it off to a worker
	 * function which does the actual work. In all probabilty, the class Render
	 * is generic (Render<T extends Entity) and this method has signature public
	 * void doRender(T entity, double d, double d1, double d2, float f, float
	 * f1). But JAD is pre 1.5 so doesn't do that.
	 */
	@Override
	public void doRender(final Entity par1Entity, final double par2,
			final double par4, final double par6, final float par8,
			final float par9) {
		func_82443_a((EntityBat) par1Entity, par2, par4, par6, par8, par9);
	}
}
