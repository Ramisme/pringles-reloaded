package net.minecraft.src;

import java.util.concurrent.Callable;

class CallableMPL2 implements Callable {
	/** Reference to the WorldClient object. */
	final WorldClient theWorldClient;

	CallableMPL2(final WorldClient par1WorldClient) {
		theWorldClient = par1WorldClient;
	}

	/**
	 * Returns the size and contents of the entity spawn queue.
	 */
	public String getEntitySpawnQueueCountAndList() {
		return WorldClient.getEntitySpawnQueue(theWorldClient).size()
				+ " total; "
				+ WorldClient.getEntitySpawnQueue(theWorldClient).toString();
	}

	@Override
	public Object call() {
		return getEntitySpawnQueueCountAndList();
	}
}
