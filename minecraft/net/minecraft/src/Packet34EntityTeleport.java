package net.minecraft.src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Packet34EntityTeleport extends Packet {
	/** ID of the entity. */
	public int entityId;

	/** X position of the entity. */
	public int xPosition;

	/** Y position of the entity. */
	public int yPosition;

	/** Z position of the entity. */
	public int zPosition;

	/** Yaw of the entity. */
	public byte yaw;

	/** Pitch of the entity. */
	public byte pitch;

	public Packet34EntityTeleport() {
	}

	public Packet34EntityTeleport(final Entity par1Entity) {
		entityId = par1Entity.entityId;
		xPosition = MathHelper.floor_double(par1Entity.posX * 32.0D);
		yPosition = MathHelper.floor_double(par1Entity.posY * 32.0D);
		zPosition = MathHelper.floor_double(par1Entity.posZ * 32.0D);
		yaw = (byte) (int) (par1Entity.rotationYaw * 256.0F / 360.0F);
		pitch = (byte) (int) (par1Entity.rotationPitch * 256.0F / 360.0F);
	}

	public Packet34EntityTeleport(final int par1, final int par2,
			final int par3, final int par4, final byte par5, final byte par6) {
		entityId = par1;
		xPosition = par2;
		yPosition = par3;
		zPosition = par4;
		yaw = par5;
		pitch = par6;
	}

	/**
	 * Abstract. Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(final DataInputStream par1DataInputStream)
			throws IOException {
		entityId = par1DataInputStream.readInt();
		xPosition = par1DataInputStream.readInt();
		yPosition = par1DataInputStream.readInt();
		zPosition = par1DataInputStream.readInt();
		yaw = (byte) par1DataInputStream.read();
		pitch = (byte) par1DataInputStream.read();
	}

	/**
	 * Abstract. Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(final DataOutputStream par1DataOutputStream)
			throws IOException {
		par1DataOutputStream.writeInt(entityId);
		par1DataOutputStream.writeInt(xPosition);
		par1DataOutputStream.writeInt(yPosition);
		par1DataOutputStream.writeInt(zPosition);
		par1DataOutputStream.write(yaw);
		par1DataOutputStream.write(pitch);
	}

	/**
	 * Passes this Packet on to the NetHandler for processing.
	 */
	@Override
	public void processPacket(final NetHandler par1NetHandler) {
		par1NetHandler.handleEntityTeleport(this);
	}

	/**
	 * Abstract. Return the size of the packet (not counting the header).
	 */
	@Override
	public int getPacketSize() {
		return 34;
	}

	/**
	 * only false for the abstract Packet class, all real packets return true
	 */
	@Override
	public boolean isRealPacket() {
		return true;
	}

	/**
	 * eg return packet30entity.entityId == entityId; WARNING : will throw if
	 * you compare a packet to a different packet class
	 */
	@Override
	public boolean containsSameEntityIDAs(final Packet par1Packet) {
		final Packet34EntityTeleport var2 = (Packet34EntityTeleport) par1Packet;
		return var2.entityId == entityId;
	}
}
