package net.minecraft.src;

public class ClippingHelper {
	public float[][] frustum = new float[16][16];
	public float[] projectionMatrix = new float[16];
	public float[] modelviewMatrix = new float[16];
	public float[] clippingMatrix = new float[16];

	/**
	 * Returns true if the box is inside all 6 clipping planes, otherwise
	 * returns false.
	 */
	public boolean isBoxInFrustum(final double par1, final double par3,
			final double par5, final double par7, final double par9,
			final double par11) {
		for (int var13 = 0; var13 < 6; ++var13) {
			final float var14 = (float) par1;
			final float var15 = (float) par3;
			final float var16 = (float) par5;
			final float var17 = (float) par7;
			final float var18 = (float) par9;
			final float var19 = (float) par11;

			if (frustum[var13][0] * var14 + frustum[var13][1] * var15
					+ frustum[var13][2] * var16 + frustum[var13][3] <= 0.0F
					&& frustum[var13][0] * var17 + frustum[var13][1] * var15
							+ frustum[var13][2] * var16 + frustum[var13][3] <= 0.0F
					&& frustum[var13][0] * var14 + frustum[var13][1] * var18
							+ frustum[var13][2] * var16 + frustum[var13][3] <= 0.0F
					&& frustum[var13][0] * var17 + frustum[var13][1] * var18
							+ frustum[var13][2] * var16 + frustum[var13][3] <= 0.0F
					&& frustum[var13][0] * var14 + frustum[var13][1] * var15
							+ frustum[var13][2] * var19 + frustum[var13][3] <= 0.0F
					&& frustum[var13][0] * var17 + frustum[var13][1] * var15
							+ frustum[var13][2] * var19 + frustum[var13][3] <= 0.0F
					&& frustum[var13][0] * var14 + frustum[var13][1] * var18
							+ frustum[var13][2] * var19 + frustum[var13][3] <= 0.0F
					&& frustum[var13][0] * var17 + frustum[var13][1] * var18
							+ frustum[var13][2] * var19 + frustum[var13][3] <= 0.0F) {
				return false;
			}
		}

		return true;
	}

	public boolean isBoxInFrustumFully(final double var1, final double var3,
			final double var5, final double var7, final double var9,
			final double var11) {
		for (int var13 = 0; var13 < 6; ++var13) {
			final float var14 = (float) var1;
			final float var15 = (float) var3;
			final float var16 = (float) var5;
			final float var17 = (float) var7;
			final float var18 = (float) var9;
			final float var19 = (float) var11;

			if (var13 < 4) {
				if (frustum[var13][0] * var14 + frustum[var13][1] * var15
						+ frustum[var13][2] * var16 + frustum[var13][3] <= 0.0F
						|| frustum[var13][0] * var17 + frustum[var13][1]
								* var15 + frustum[var13][2] * var16
								+ frustum[var13][3] <= 0.0F
						|| frustum[var13][0] * var14 + frustum[var13][1]
								* var18 + frustum[var13][2] * var16
								+ frustum[var13][3] <= 0.0F
						|| frustum[var13][0] * var17 + frustum[var13][1]
								* var18 + frustum[var13][2] * var16
								+ frustum[var13][3] <= 0.0F
						|| frustum[var13][0] * var14 + frustum[var13][1]
								* var15 + frustum[var13][2] * var19
								+ frustum[var13][3] <= 0.0F
						|| frustum[var13][0] * var17 + frustum[var13][1]
								* var15 + frustum[var13][2] * var19
								+ frustum[var13][3] <= 0.0F
						|| frustum[var13][0] * var14 + frustum[var13][1]
								* var18 + frustum[var13][2] * var19
								+ frustum[var13][3] <= 0.0F
						|| frustum[var13][0] * var17 + frustum[var13][1]
								* var18 + frustum[var13][2] * var19
								+ frustum[var13][3] <= 0.0F) {
					return false;
				}
			} else if (frustum[var13][0] * var14 + frustum[var13][1] * var15
					+ frustum[var13][2] * var16 + frustum[var13][3] <= 0.0F
					&& frustum[var13][0] * var17 + frustum[var13][1] * var15
							+ frustum[var13][2] * var16 + frustum[var13][3] <= 0.0F
					&& frustum[var13][0] * var14 + frustum[var13][1] * var18
							+ frustum[var13][2] * var16 + frustum[var13][3] <= 0.0F
					&& frustum[var13][0] * var17 + frustum[var13][1] * var18
							+ frustum[var13][2] * var16 + frustum[var13][3] <= 0.0F
					&& frustum[var13][0] * var14 + frustum[var13][1] * var15
							+ frustum[var13][2] * var19 + frustum[var13][3] <= 0.0F
					&& frustum[var13][0] * var17 + frustum[var13][1] * var15
							+ frustum[var13][2] * var19 + frustum[var13][3] <= 0.0F
					&& frustum[var13][0] * var14 + frustum[var13][1] * var18
							+ frustum[var13][2] * var19 + frustum[var13][3] <= 0.0F
					&& frustum[var13][0] * var17 + frustum[var13][1] * var18
							+ frustum[var13][2] * var19 + frustum[var13][3] <= 0.0F) {
				return false;
			}
		}

		return true;
	}
}
