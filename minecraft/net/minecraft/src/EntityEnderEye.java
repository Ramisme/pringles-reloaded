package net.minecraft.src;

public class EntityEnderEye extends Entity {
	public int field_70226_a = 0;

	/** 'x' location the eye should float towards. */
	private double targetX;

	/** 'y' location the eye should float towards. */
	private double targetY;

	/** 'z' location the eye should float towards. */
	private double targetZ;
	private int despawnTimer;
	private boolean shatterOrDrop;

	public EntityEnderEye(final World par1World) {
		super(par1World);
		setSize(0.25F, 0.25F);
	}

	@Override
	protected void entityInit() {
	}

	/**
	 * Checks if the entity is in range to render by using the past in distance
	 * and comparing it to its average edge length * 64 * renderDistanceWeight
	 * Args: distance
	 */
	@Override
	public boolean isInRangeToRenderDist(final double par1) {
		double var3 = boundingBox.getAverageEdgeLength() * 4.0D;
		var3 *= 64.0D;
		return par1 < var3 * var3;
	}

	public EntityEnderEye(final World par1World, final double par2,
			final double par4, final double par6) {
		super(par1World);
		despawnTimer = 0;
		setSize(0.25F, 0.25F);
		setPosition(par2, par4, par6);
		yOffset = 0.0F;
	}

	/**
	 * The location the eye should float/move towards. Currently used for moving
	 * towards the nearest stronghold. Args: strongholdX, strongholdY,
	 * strongholdZ
	 */
	public void moveTowards(final double par1, final int par3, final double par4) {
		final double var6 = par1 - posX;
		final double var8 = par4 - posZ;
		final float var10 = MathHelper.sqrt_double(var6 * var6 + var8 * var8);

		if (var10 > 12.0F) {
			targetX = posX + var6 / var10 * 12.0D;
			targetZ = posZ + var8 / var10 * 12.0D;
			targetY = posY + 8.0D;
		} else {
			targetX = par1;
			targetY = par3;
			targetZ = par4;
		}

		despawnTimer = 0;
		shatterOrDrop = rand.nextInt(5) > 0;
	}

	/**
	 * Sets the velocity to the args. Args: x, y, z
	 */
	@Override
	public void setVelocity(final double par1, final double par3,
			final double par5) {
		motionX = par1;
		motionY = par3;
		motionZ = par5;

		if (prevRotationPitch == 0.0F && prevRotationYaw == 0.0F) {
			final float var7 = MathHelper
					.sqrt_double(par1 * par1 + par5 * par5);
			prevRotationYaw = rotationYaw = (float) (Math.atan2(par1, par5) * 180.0D / Math.PI);
			prevRotationPitch = rotationPitch = (float) (Math.atan2(par3, var7) * 180.0D / Math.PI);
		}
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate() {
		lastTickPosX = posX;
		lastTickPosY = posY;
		lastTickPosZ = posZ;
		super.onUpdate();
		posX += motionX;
		posY += motionY;
		posZ += motionZ;
		final float var1 = MathHelper.sqrt_double(motionX * motionX + motionZ
				* motionZ);
		rotationYaw = (float) (Math.atan2(motionX, motionZ) * 180.0D / Math.PI);

		for (rotationPitch = (float) (Math.atan2(motionY, var1) * 180.0D / Math.PI); rotationPitch
				- prevRotationPitch < -180.0F; prevRotationPitch -= 360.0F) {
			;
		}

		while (rotationPitch - prevRotationPitch >= 180.0F) {
			prevRotationPitch += 360.0F;
		}

		while (rotationYaw - prevRotationYaw < -180.0F) {
			prevRotationYaw -= 360.0F;
		}

		while (rotationYaw - prevRotationYaw >= 180.0F) {
			prevRotationYaw += 360.0F;
		}

		rotationPitch = prevRotationPitch + (rotationPitch - prevRotationPitch)
				* 0.2F;
		rotationYaw = prevRotationYaw + (rotationYaw - prevRotationYaw) * 0.2F;

		if (!worldObj.isRemote) {
			final double var2 = targetX - posX;
			final double var4 = targetZ - posZ;
			final float var6 = (float) Math.sqrt(var2 * var2 + var4 * var4);
			final float var7 = (float) Math.atan2(var4, var2);
			double var8 = var1 + (var6 - var1) * 0.0025D;

			if (var6 < 1.0F) {
				var8 *= 0.8D;
				motionY *= 0.8D;
			}

			motionX = Math.cos(var7) * var8;
			motionZ = Math.sin(var7) * var8;

			if (posY < targetY) {
				motionY += (1.0D - motionY) * 0.014999999664723873D;
			} else {
				motionY += (-1.0D - motionY) * 0.014999999664723873D;
			}
		}

		final float var10 = 0.25F;

		if (isInWater()) {
			for (int var3 = 0; var3 < 4; ++var3) {
				worldObj.spawnParticle("bubble", posX - motionX * var10, posY
						- motionY * var10, posZ - motionZ * var10, motionX,
						motionY, motionZ);
			}
		} else {
			worldObj.spawnParticle("portal",
					posX - motionX * var10 + rand.nextDouble() * 0.6D - 0.3D,
					posY - motionY * var10 - 0.5D, posZ - motionZ * var10
							+ rand.nextDouble() * 0.6D - 0.3D, motionX,
					motionY, motionZ);
		}

		if (!worldObj.isRemote) {
			setPosition(posX, posY, posZ);
			++despawnTimer;

			if (despawnTimer > 80 && !worldObj.isRemote) {
				setDead();

				if (shatterOrDrop) {
					worldObj.spawnEntityInWorld(new EntityItem(worldObj, posX,
							posY, posZ, new ItemStack(Item.eyeOfEnder)));
				} else {
					worldObj.playAuxSFX(2003, (int) Math.round(posX),
							(int) Math.round(posY), (int) Math.round(posZ), 0);
				}
			}
		}
	}

	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	@Override
	public void writeEntityToNBT(final NBTTagCompound par1NBTTagCompound) {
	}

	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	@Override
	public void readEntityFromNBT(final NBTTagCompound par1NBTTagCompound) {
	}

	@Override
	public float getShadowSize() {
		return 0.0F;
	}

	/**
	 * Gets how bright this entity is.
	 */
	@Override
	public float getBrightness(final float par1) {
		return 1.0F;
	}

	@Override
	public int getBrightnessForRender(final float par1) {
		return 15728880;
	}

	/**
	 * If returns false, the item will not inflict any damage against entities.
	 */
	@Override
	public boolean canAttackWithItem() {
		return false;
	}
}
