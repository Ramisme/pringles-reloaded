package net.minecraft.src;

final class DispenserBehaviorPotion implements IBehaviorDispenseItem {
	private final BehaviorDefaultDispenseItem defaultDispenserItemBehavior = new BehaviorDefaultDispenseItem();

	/**
	 * Dispenses the specified ItemStack from a dispenser.
	 */
	@Override
	public ItemStack dispense(final IBlockSource par1IBlockSource,
			final ItemStack par2ItemStack) {
		return ItemPotion.isSplash(par2ItemStack.getItemDamage()) ? new DispenserBehaviorPotionProjectile(
				this, par2ItemStack).dispense(par1IBlockSource, par2ItemStack)
				: defaultDispenserItemBehavior.dispense(par1IBlockSource,
						par2ItemStack);
	}
}
