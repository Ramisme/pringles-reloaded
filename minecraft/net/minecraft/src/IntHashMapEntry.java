package net.minecraft.src;

class IntHashMapEntry {
	/** The hash code of this entry */
	final int hashEntry;

	/** The object stored in this entry */
	Object valueEntry;

	/** The next entry in this slot */
	IntHashMapEntry nextEntry;

	/** The id of the hash slot computed from the hash */
	final int slotHash;

	IntHashMapEntry(final int par1, final int par2, final Object par3Obj,
			final IntHashMapEntry par4IntHashMapEntry) {
		valueEntry = par3Obj;
		nextEntry = par4IntHashMapEntry;
		hashEntry = par2;
		slotHash = par1;
	}

	/**
	 * Returns the hash code for this entry
	 */
	public final int getHash() {
		return hashEntry;
	}

	/**
	 * Returns the object stored in this entry
	 */
	public final Object getValue() {
		return valueEntry;
	}

	@Override
	public final boolean equals(final Object par1Obj) {
		if (!(par1Obj instanceof IntHashMapEntry)) {
			return false;
		} else {
			final IntHashMapEntry var2 = (IntHashMapEntry) par1Obj;
			final Integer var3 = Integer.valueOf(getHash());
			final Integer var4 = Integer.valueOf(var2.getHash());

			if (var3 == var4 || var3 != null && var3.equals(var4)) {
				final Object var5 = getValue();
				final Object var6 = var2.getValue();

				if (var5 == var6 || var5 != null && var5.equals(var6)) {
					return true;
				}
			}

			return false;
		}
	}

	@Override
	public final int hashCode() {
		return IntHashMap.getHash(hashEntry);
	}

	@Override
	public final String toString() {
		return getHash() + "=" + getValue();
	}
}
