package net.minecraft.src;

import java.util.Random;

public class BlockNetherStalk extends BlockFlower {
	private static final String[] field_94373_a = new String[] {
			"netherStalk_0", "netherStalk_1", "netherStalk_2" };
	private Icon[] iconArray;

	protected BlockNetherStalk(final int par1) {
		super(par1);
		setTickRandomly(true);
		final float var2 = 0.5F;
		setBlockBounds(0.5F - var2, 0.0F, 0.5F - var2, 0.5F + var2, 0.25F,
				0.5F + var2);
		setCreativeTab((CreativeTabs) null);
	}

	/**
	 * Gets passed in the blockID of the block below and supposed to return true
	 * if its allowed to grow on the type of blockID passed in. Args: blockID
	 */
	@Override
	protected boolean canThisPlantGrowOnThisBlockID(final int par1) {
		return par1 == Block.slowSand.blockID;
	}

	/**
	 * Can this block stay at this position. Similar to canPlaceBlockAt except
	 * gets checked often with plants.
	 */
	@Override
	public boolean canBlockStay(final World par1World, final int par2,
			final int par3, final int par4) {
		return canThisPlantGrowOnThisBlockID(par1World.getBlockId(par2,
				par3 - 1, par4));
	}

	/**
	 * Ticks the block if it's been scheduled
	 */
	@Override
	public void updateTick(final World par1World, final int par2,
			final int par3, final int par4, final Random par5Random) {
		int var6 = par1World.getBlockMetadata(par2, par3, par4);

		if (var6 < 3 && par5Random.nextInt(10) == 0) {
			++var6;
			par1World.setBlockMetadataWithNotify(par2, par3, par4, var6, 2);
		}

		super.updateTick(par1World, par2, par3, par4, par5Random);
	}

	/**
	 * From the specified side and block metadata retrieves the blocks texture.
	 * Args: side, metadata
	 */
	@Override
	public Icon getIcon(final int par1, final int par2) {
		return par2 >= 3 ? iconArray[2] : par2 > 0 ? iconArray[1]
				: iconArray[0];
	}

	/**
	 * The type of render function that is called for this block
	 */
	@Override
	public int getRenderType() {
		return 6;
	}

	/**
	 * Drops the block items with a specified chance of dropping the specified
	 * items
	 */
	@Override
	public void dropBlockAsItemWithChance(final World par1World,
			final int par2, final int par3, final int par4, final int par5,
			final float par6, final int par7) {
		if (!par1World.isRemote) {
			int var8 = 1;

			if (par5 >= 3) {
				var8 = 2 + par1World.rand.nextInt(3);

				if (par7 > 0) {
					var8 += par1World.rand.nextInt(par7 + 1);
				}
			}

			for (int var9 = 0; var9 < var8; ++var9) {
				dropBlockAsItem_do(par1World, par2, par3, par4, new ItemStack(
						Item.netherStalkSeeds));
			}
		}
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(final int par1, final Random par2Random, final int par3) {
		return 0;
	}

	/**
	 * Returns the quantity of items to drop on block destruction.
	 */
	@Override
	public int quantityDropped(final Random par1Random) {
		return 0;
	}

	/**
	 * only called by clickMiddleMouseButton , and passed to
	 * inventory.setCurrentItem (along with isCreative)
	 */
	@Override
	public int idPicked(final World par1World, final int par2, final int par3,
			final int par4) {
		return Item.netherStalkSeeds.itemID;
	}

	/**
	 * When this method is called, your block should register all the icons it
	 * needs with the given IconRegister. This is the only chance you get to
	 * register icons.
	 */
	@Override
	public void registerIcons(final IconRegister par1IconRegister) {
		iconArray = new Icon[BlockNetherStalk.field_94373_a.length];

		for (int var2 = 0; var2 < iconArray.length; ++var2) {
			iconArray[var2] = par1IconRegister
					.registerIcon(BlockNetherStalk.field_94373_a[var2]);
		}
	}
}
