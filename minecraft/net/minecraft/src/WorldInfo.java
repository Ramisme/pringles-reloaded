package net.minecraft.src;

public class WorldInfo {
	/** Holds the seed of the currently world. */
	private long randomSeed;
	private WorldType terrainType;
	private String generatorOptions;

	/** The spawn zone position X coordinate. */
	private int spawnX;

	/** The spawn zone position Y coordinate. */
	private int spawnY;

	/** The spawn zone position Z coordinate. */
	private int spawnZ;

	/** Total time for this world. */
	private long totalTime;

	/** The current world time in ticks, ranging from 0 to 23999. */
	private long worldTime;

	/** The last time the player was in this world. */
	private long lastTimePlayed;

	/** The size of entire save of current world on the disk, isn't exactly. */
	private long sizeOnDisk;
	private NBTTagCompound playerTag;
	private int dimension;

	/** The name of the save defined at world creation. */
	private String levelName;

	/** Introduced in beta 1.3, is the save version for future control. */
	private int saveVersion;

	/** True if it's raining, false otherwise. */
	private boolean raining;

	/** Number of ticks until next rain. */
	private int rainTime;

	/** Is thunderbolts failing now? */
	private boolean thundering;

	/** Number of ticks untils next thunderbolt. */
	private int thunderTime;

	/** The Game Type. */
	private EnumGameType theGameType;

	/**
	 * Whether the map features (e.g. strongholds) generation is enabled or
	 * disabled.
	 */
	private boolean mapFeaturesEnabled;

	/** Hardcore mode flag */
	private boolean hardcore;
	private boolean allowCommands;
	private boolean initialized;
	private GameRules theGameRules;

	protected WorldInfo() {
		terrainType = WorldType.DEFAULT;
		generatorOptions = "";
		theGameRules = new GameRules();
	}

	public WorldInfo(final NBTTagCompound par1NBTTagCompound) {
		terrainType = WorldType.DEFAULT;
		generatorOptions = "";
		theGameRules = new GameRules();
		randomSeed = par1NBTTagCompound.getLong("RandomSeed");

		if (par1NBTTagCompound.hasKey("generatorName")) {
			final String var2 = par1NBTTagCompound.getString("generatorName");
			terrainType = WorldType.parseWorldType(var2);

			if (terrainType == null) {
				terrainType = WorldType.DEFAULT;
			} else if (terrainType.isVersioned()) {
				int var3 = 0;

				if (par1NBTTagCompound.hasKey("generatorVersion")) {
					var3 = par1NBTTagCompound.getInteger("generatorVersion");
				}

				terrainType = terrainType.getWorldTypeForGeneratorVersion(var3);
			}

			if (par1NBTTagCompound.hasKey("generatorOptions")) {
				generatorOptions = par1NBTTagCompound
						.getString("generatorOptions");
			}
		}

		theGameType = EnumGameType.getByID(par1NBTTagCompound
				.getInteger("GameType"));

		if (par1NBTTagCompound.hasKey("MapFeatures")) {
			mapFeaturesEnabled = par1NBTTagCompound.getBoolean("MapFeatures");
		} else {
			mapFeaturesEnabled = true;
		}

		spawnX = par1NBTTagCompound.getInteger("SpawnX");
		spawnY = par1NBTTagCompound.getInteger("SpawnY");
		spawnZ = par1NBTTagCompound.getInteger("SpawnZ");
		totalTime = par1NBTTagCompound.getLong("Time");

		if (par1NBTTagCompound.hasKey("DayTime")) {
			worldTime = par1NBTTagCompound.getLong("DayTime");
		} else {
			worldTime = totalTime;
		}

		lastTimePlayed = par1NBTTagCompound.getLong("LastPlayed");
		sizeOnDisk = par1NBTTagCompound.getLong("SizeOnDisk");
		levelName = par1NBTTagCompound.getString("LevelName");
		saveVersion = par1NBTTagCompound.getInteger("version");
		rainTime = par1NBTTagCompound.getInteger("rainTime");
		raining = par1NBTTagCompound.getBoolean("raining");
		thunderTime = par1NBTTagCompound.getInteger("thunderTime");
		thundering = par1NBTTagCompound.getBoolean("thundering");
		hardcore = par1NBTTagCompound.getBoolean("hardcore");

		if (par1NBTTagCompound.hasKey("initialized")) {
			initialized = par1NBTTagCompound.getBoolean("initialized");
		} else {
			initialized = true;
		}

		if (par1NBTTagCompound.hasKey("allowCommands")) {
			allowCommands = par1NBTTagCompound.getBoolean("allowCommands");
		} else {
			allowCommands = theGameType == EnumGameType.CREATIVE;
		}

		if (par1NBTTagCompound.hasKey("Player")) {
			playerTag = par1NBTTagCompound.getCompoundTag("Player");
			dimension = playerTag.getInteger("Dimension");
		}

		if (par1NBTTagCompound.hasKey("GameRules")) {
			theGameRules.readGameRulesFromNBT(par1NBTTagCompound
					.getCompoundTag("GameRules"));
		}
	}

	public WorldInfo(final WorldSettings par1WorldSettings, final String par2Str) {
		terrainType = WorldType.DEFAULT;
		generatorOptions = "";
		theGameRules = new GameRules();
		randomSeed = par1WorldSettings.getSeed();
		theGameType = par1WorldSettings.getGameType();
		mapFeaturesEnabled = par1WorldSettings.isMapFeaturesEnabled();
		levelName = par2Str;
		hardcore = par1WorldSettings.getHardcoreEnabled();
		terrainType = par1WorldSettings.getTerrainType();
		generatorOptions = par1WorldSettings.func_82749_j();
		allowCommands = par1WorldSettings.areCommandsAllowed();
		initialized = false;
	}

	public WorldInfo(final WorldInfo par1WorldInfo) {
		terrainType = WorldType.DEFAULT;
		generatorOptions = "";
		theGameRules = new GameRules();
		randomSeed = par1WorldInfo.randomSeed;
		terrainType = par1WorldInfo.terrainType;
		generatorOptions = par1WorldInfo.generatorOptions;
		theGameType = par1WorldInfo.theGameType;
		mapFeaturesEnabled = par1WorldInfo.mapFeaturesEnabled;
		spawnX = par1WorldInfo.spawnX;
		spawnY = par1WorldInfo.spawnY;
		spawnZ = par1WorldInfo.spawnZ;
		totalTime = par1WorldInfo.totalTime;
		worldTime = par1WorldInfo.worldTime;
		lastTimePlayed = par1WorldInfo.lastTimePlayed;
		sizeOnDisk = par1WorldInfo.sizeOnDisk;
		playerTag = par1WorldInfo.playerTag;
		dimension = par1WorldInfo.dimension;
		levelName = par1WorldInfo.levelName;
		saveVersion = par1WorldInfo.saveVersion;
		rainTime = par1WorldInfo.rainTime;
		raining = par1WorldInfo.raining;
		thunderTime = par1WorldInfo.thunderTime;
		thundering = par1WorldInfo.thundering;
		hardcore = par1WorldInfo.hardcore;
		allowCommands = par1WorldInfo.allowCommands;
		initialized = par1WorldInfo.initialized;
		theGameRules = par1WorldInfo.theGameRules;
	}

	/**
	 * Gets the NBTTagCompound for the worldInfo
	 */
	public NBTTagCompound getNBTTagCompound() {
		final NBTTagCompound var1 = new NBTTagCompound();
		updateTagCompound(var1, playerTag);
		return var1;
	}

	/**
	 * Creates a new NBTTagCompound for the world, with the given NBTTag as the
	 * "Player"
	 */
	public NBTTagCompound cloneNBTCompound(
			final NBTTagCompound par1NBTTagCompound) {
		final NBTTagCompound var2 = new NBTTagCompound();
		updateTagCompound(var2, par1NBTTagCompound);
		return var2;
	}

	private void updateTagCompound(final NBTTagCompound par1NBTTagCompound,
			final NBTTagCompound par2NBTTagCompound) {
		par1NBTTagCompound.setLong("RandomSeed", randomSeed);
		par1NBTTagCompound.setString("generatorName",
				terrainType.getWorldTypeName());
		par1NBTTagCompound.setInteger("generatorVersion",
				terrainType.getGeneratorVersion());
		par1NBTTagCompound.setString("generatorOptions", generatorOptions);
		par1NBTTagCompound.setInteger("GameType", theGameType.getID());
		par1NBTTagCompound.setBoolean("MapFeatures", mapFeaturesEnabled);
		par1NBTTagCompound.setInteger("SpawnX", spawnX);
		par1NBTTagCompound.setInteger("SpawnY", spawnY);
		par1NBTTagCompound.setInteger("SpawnZ", spawnZ);
		par1NBTTagCompound.setLong("Time", totalTime);
		par1NBTTagCompound.setLong("DayTime", worldTime);
		par1NBTTagCompound.setLong("SizeOnDisk", sizeOnDisk);
		par1NBTTagCompound.setLong("LastPlayed", System.currentTimeMillis());
		par1NBTTagCompound.setString("LevelName", levelName);
		par1NBTTagCompound.setInteger("version", saveVersion);
		par1NBTTagCompound.setInteger("rainTime", rainTime);
		par1NBTTagCompound.setBoolean("raining", raining);
		par1NBTTagCompound.setInteger("thunderTime", thunderTime);
		par1NBTTagCompound.setBoolean("thundering", thundering);
		par1NBTTagCompound.setBoolean("hardcore", hardcore);
		par1NBTTagCompound.setBoolean("allowCommands", allowCommands);
		par1NBTTagCompound.setBoolean("initialized", initialized);
		par1NBTTagCompound.setCompoundTag("GameRules",
				theGameRules.writeGameRulesToNBT());

		if (par2NBTTagCompound != null) {
			par1NBTTagCompound.setCompoundTag("Player", par2NBTTagCompound);
		}
	}

	/**
	 * Returns the seed of current world.
	 */
	public long getSeed() {
		return randomSeed;
	}

	/**
	 * Returns the x spawn position
	 */
	public int getSpawnX() {
		return spawnX;
	}

	/**
	 * Return the Y axis spawning point of the player.
	 */
	public int getSpawnY() {
		return spawnY;
	}

	/**
	 * Returns the z spawn position
	 */
	public int getSpawnZ() {
		return spawnZ;
	}

	public long getWorldTotalTime() {
		return totalTime;
	}

	/**
	 * Get current world time
	 */
	public long getWorldTime() {
		return worldTime;
	}

	public long getSizeOnDisk() {
		return sizeOnDisk;
	}

	/**
	 * Returns the player's NBTTagCompound to be loaded
	 */
	public NBTTagCompound getPlayerNBTTagCompound() {
		return playerTag;
	}

	public int getDimension() {
		return dimension;
	}

	/**
	 * Set the x spawn position to the passed in value
	 */
	public void setSpawnX(final int par1) {
		spawnX = par1;
	}

	/**
	 * Sets the y spawn position
	 */
	public void setSpawnY(final int par1) {
		spawnY = par1;
	}

	/**
	 * Set the z spawn position to the passed in value
	 */
	public void setSpawnZ(final int par1) {
		spawnZ = par1;
	}

	public void incrementTotalWorldTime(final long par1) {
		totalTime = par1;
	}

	/**
	 * Set current world time
	 */
	public void setWorldTime(final long par1) {
		worldTime = par1;
	}

	/**
	 * Sets the spawn zone position. Args: x, y, z
	 */
	public void setSpawnPosition(final int par1, final int par2, final int par3) {
		spawnX = par1;
		spawnY = par2;
		spawnZ = par3;
	}

	/**
	 * Get current world name
	 */
	public String getWorldName() {
		return levelName;
	}

	public void setWorldName(final String par1Str) {
		levelName = par1Str;
	}

	/**
	 * Returns the save version of this world
	 */
	public int getSaveVersion() {
		return saveVersion;
	}

	/**
	 * Sets the save version of the world
	 */
	public void setSaveVersion(final int par1) {
		saveVersion = par1;
	}

	/**
	 * Return the last time the player was in this world.
	 */
	public long getLastTimePlayed() {
		return lastTimePlayed;
	}

	/**
	 * Returns true if it is thundering, false otherwise.
	 */
	public boolean isThundering() {
		return thundering;
	}

	/**
	 * Sets whether it is thundering or not.
	 */
	public void setThundering(final boolean par1) {
		thundering = par1;
	}

	/**
	 * Returns the number of ticks until next thunderbolt.
	 */
	public int getThunderTime() {
		return thunderTime;
	}

	/**
	 * Defines the number of ticks until next thunderbolt.
	 */
	public void setThunderTime(final int par1) {
		thunderTime = par1;
	}

	/**
	 * Returns true if it is raining, false otherwise.
	 */
	public boolean isRaining() {
		return raining;
	}

	/**
	 * Sets whether it is raining or not.
	 */
	public void setRaining(final boolean par1) {
		raining = par1;
	}

	/**
	 * Return the number of ticks until rain.
	 */
	public int getRainTime() {
		return rainTime;
	}

	/**
	 * Sets the number of ticks until rain.
	 */
	public void setRainTime(final int par1) {
		rainTime = par1;
	}

	/**
	 * Gets the GameType.
	 */
	public EnumGameType getGameType() {
		return theGameType;
	}

	/**
	 * Get whether the map features (e.g. strongholds) generation is enabled or
	 * disabled.
	 */
	public boolean isMapFeaturesEnabled() {
		return mapFeaturesEnabled;
	}

	/**
	 * Sets the GameType.
	 */
	public void setGameType(final EnumGameType par1EnumGameType) {
		theGameType = par1EnumGameType;
	}

	/**
	 * Returns true if hardcore mode is enabled, otherwise false
	 */
	public boolean isHardcoreModeEnabled() {
		return hardcore;
	}

	public WorldType getTerrainType() {
		return terrainType;
	}

	public void setTerrainType(final WorldType par1WorldType) {
		terrainType = par1WorldType;
	}

	public String getGeneratorOptions() {
		return generatorOptions;
	}

	/**
	 * Returns true if commands are allowed on this World.
	 */
	public boolean areCommandsAllowed() {
		return allowCommands;
	}

	/**
	 * Returns true if the World is initialized.
	 */
	public boolean isInitialized() {
		return initialized;
	}

	/**
	 * Sets the initialization status of the World.
	 */
	public void setServerInitialized(final boolean par1) {
		initialized = par1;
	}

	/**
	 * Gets the GameRules class Instance.
	 */
	public GameRules getGameRulesInstance() {
		return theGameRules;
	}

	/**
	 * Adds this WorldInfo instance to the crash report.
	 */
	public void addToCrashReport(
			final CrashReportCategory par1CrashReportCategory) {
		par1CrashReportCategory.addCrashSectionCallable("Level seed",
				new CallableLevelSeed(this));
		par1CrashReportCategory.addCrashSectionCallable("Level generator",
				new CallableLevelGenerator(this));
		par1CrashReportCategory.addCrashSectionCallable(
				"Level generator options", new CallableLevelGeneratorOptions(
						this));
		par1CrashReportCategory.addCrashSectionCallable("Level spawn location",
				new CallableLevelSpawnLocation(this));
		par1CrashReportCategory.addCrashSectionCallable("Level time",
				new CallableLevelTime(this));
		par1CrashReportCategory.addCrashSectionCallable("Level dimension",
				new CallableLevelDimension(this));
		par1CrashReportCategory.addCrashSectionCallable(
				"Level storage version", new CallableLevelStorageVersion(this));
		par1CrashReportCategory.addCrashSectionCallable("Level weather",
				new CallableLevelWeather(this));
		par1CrashReportCategory.addCrashSectionCallable("Level game mode",
				new CallableLevelGamemode(this));
	}

	/**
	 * Return the terrain type of a world
	 */
	static WorldType getTerrainTypeOfWorld(final WorldInfo par0WorldInfo) {
		return par0WorldInfo.terrainType;
	}

	/**
	 * Return the map feautures enabled of a world
	 */
	static boolean getMapFeaturesEnabled(final WorldInfo par0WorldInfo) {
		return par0WorldInfo.mapFeaturesEnabled;
	}

	static String getWorldGeneratorOptions(final WorldInfo par0WorldInfo) {
		return par0WorldInfo.generatorOptions;
	}

	static int getSpawnXCoordinate(final WorldInfo par0WorldInfo) {
		return par0WorldInfo.spawnX;
	}

	static int getSpawnYCoordinate(final WorldInfo par0WorldInfo) {
		return par0WorldInfo.spawnY;
	}

	static int getSpawnZCoordinate(final WorldInfo par0WorldInfo) {
		return par0WorldInfo.spawnZ;
	}

	static long func_85126_g(final WorldInfo par0WorldInfo) {
		return par0WorldInfo.totalTime;
	}

	static long getWorldTime(final WorldInfo par0WorldInfo) {
		return par0WorldInfo.worldTime;
	}

	static int func_85122_i(final WorldInfo par0WorldInfo) {
		return par0WorldInfo.dimension;
	}

	static int getSaveVersion(final WorldInfo par0WorldInfo) {
		return par0WorldInfo.saveVersion;
	}

	static int getRainTime(final WorldInfo par0WorldInfo) {
		return par0WorldInfo.rainTime;
	}

	/**
	 * Returns wether it's raining or not.
	 */
	static boolean getRaining(final WorldInfo par0WorldInfo) {
		return par0WorldInfo.raining;
	}

	static int getThunderTime(final WorldInfo par0WorldInfo) {
		return par0WorldInfo.thunderTime;
	}

	/**
	 * Returns wether it's thundering or not.
	 */
	static boolean getThundering(final WorldInfo par0WorldInfo) {
		return par0WorldInfo.thundering;
	}

	static EnumGameType getGameType(final WorldInfo par0WorldInfo) {
		return par0WorldInfo.theGameType;
	}

	static boolean func_85117_p(final WorldInfo par0WorldInfo) {
		return par0WorldInfo.hardcore;
	}

	static boolean func_85131_q(final WorldInfo par0WorldInfo) {
		return par0WorldInfo.allowCommands;
	}
}
