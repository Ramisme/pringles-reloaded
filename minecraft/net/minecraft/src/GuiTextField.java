package net.minecraft.src;

import org.lwjgl.opengl.GL11;

public class GuiTextField extends Gui {
	/**
	 * Have the font renderer from GuiScreen to render the textbox text into the
	 * screen.
	 */
	private final FontRenderer fontRenderer;
	private final int xPos;
	private final int yPos;

	/** The width of this text field. */
	private final int width;
	private final int height;

	/** Have the current text beign edited on the textbox. */
	private String text = "";
	private int maxStringLength = 32;
	private int cursorCounter;
	private boolean enableBackgroundDrawing = true;

	/**
	 * if true the textbox can lose focus by clicking elsewhere on the screen
	 */
	private boolean canLoseFocus = true;

	/**
	 * If this value is true along isEnabled, keyTyped will process the keys.
	 */
	private boolean isFocused = false;

	/**
	 * If this value is true along isFocused, keyTyped will process the keys.
	 */
	private boolean isEnabled = true;

	/**
	 * The current character index that should be used as start of the rendered
	 * text.
	 */
	private int lineScrollOffset = 0;
	private int cursorPosition = 0;

	/** other selection position, maybe the same as the cursor */
	private int selectionEnd = 0;
	private int enabledColor = 14737632;
	private int disabledColor = 7368816;

	/** True if this textbox is visible */
	private boolean visible = true;

	public GuiTextField(final FontRenderer par1FontRenderer, final int par2,
			final int par3, final int par4, final int par5) {
		fontRenderer = par1FontRenderer;
		xPos = par2;
		yPos = par3;
		width = par4;
		height = par5;
	}

	/**
	 * Increments the cursor counter
	 */
	public void updateCursorCounter() {
		++cursorCounter;
	}

	/**
	 * Sets the text of the textbox.
	 */
	public void setText(final String par1Str) {
		if (par1Str.length() > maxStringLength) {
			text = par1Str.substring(0, maxStringLength);
		} else {
			text = par1Str;
		}

		setCursorPositionEnd();
	}

	/**
	 * Returns the text beign edited on the textbox.
	 */
	public String getText() {
		return text;
	}

	/**
	 * @return returns the text between the cursor and selectionEnd
	 */
	public String getSelectedtext() {
		final int var1 = cursorPosition < selectionEnd ? cursorPosition
				: selectionEnd;
		final int var2 = cursorPosition < selectionEnd ? selectionEnd
				: cursorPosition;
		return text.substring(var1, var2);
	}

	/**
	 * replaces selected text, or inserts text at the position on the cursor
	 */
	public void writeText(final String par1Str) {
		String var2 = "";
		final String var3 = ChatAllowedCharacters
				.filerAllowedCharacters(par1Str);
		final int var4 = cursorPosition < selectionEnd ? cursorPosition
				: selectionEnd;
		final int var5 = cursorPosition < selectionEnd ? selectionEnd
				: cursorPosition;
		final int var6 = maxStringLength - text.length()
				- (var4 - selectionEnd);
		if (text.length() > 0) {
			var2 = var2 + text.substring(0, var4);
		}

		int var8;

		if (var6 < var3.length()) {
			var2 = var2 + var3.substring(0, var6);
			var8 = var6;
		} else {
			var2 = var2 + var3;
			var8 = var3.length();
		}

		if (text.length() > 0 && var5 < text.length()) {
			var2 = var2 + text.substring(var5);
		}

		text = var2;
		moveCursorBy(var4 - selectionEnd + var8);
	}

	/**
	 * Deletes the specified number of words starting at the cursor position.
	 * Negative numbers will delete words left of the cursor.
	 */
	public void deleteWords(final int par1) {
		if (text.length() != 0) {
			if (selectionEnd != cursorPosition) {
				writeText("");
			} else {
				deleteFromCursor(getNthWordFromCursor(par1) - cursorPosition);
			}
		}
	}

	/**
	 * delete the selected text, otherwsie deletes characters from either side
	 * of the cursor. params: delete num
	 */
	public void deleteFromCursor(final int par1) {
		if (text.length() != 0) {
			if (selectionEnd != cursorPosition) {
				writeText("");
			} else {
				final boolean var2 = par1 < 0;
				final int var3 = var2 ? cursorPosition + par1 : cursorPosition;
				final int var4 = var2 ? cursorPosition : cursorPosition + par1;
				String var5 = "";

				if (var3 >= 0) {
					var5 = text.substring(0, var3);
				}

				if (var4 < text.length()) {
					var5 = var5 + text.substring(var4);
				}

				text = var5;

				if (var2) {
					moveCursorBy(par1);
				}
			}
		}
	}

	/**
	 * see @getNthNextWordFromPos() params: N, position
	 */
	public int getNthWordFromCursor(final int par1) {
		return getNthWordFromPos(par1, getCursorPosition());
	}

	/**
	 * gets the position of the nth word. N may be negative, then it looks
	 * backwards. params: N, position
	 */
	public int getNthWordFromPos(final int par1, final int par2) {
		return func_73798_a(par1, getCursorPosition(), true);
	}

	public int func_73798_a(final int par1, final int par2, final boolean par3) {
		int var4 = par2;
		final boolean var5 = par1 < 0;
		final int var6 = Math.abs(par1);

		for (int var7 = 0; var7 < var6; ++var7) {
			if (var5) {
				while (par3 && var4 > 0 && text.charAt(var4 - 1) == 32) {
					--var4;
				}

				while (var4 > 0 && text.charAt(var4 - 1) != 32) {
					--var4;
				}
			} else {
				final int var8 = text.length();
				var4 = text.indexOf(32, var4);

				if (var4 == -1) {
					var4 = var8;
				} else {
					while (par3 && var4 < var8 && text.charAt(var4) == 32) {
						++var4;
					}
				}
			}
		}

		return var4;
	}

	/**
	 * Moves the text cursor by a specified number of characters and clears the
	 * selection
	 */
	public void moveCursorBy(final int par1) {
		setCursorPosition(selectionEnd + par1);
	}

	/**
	 * sets the position of the cursor to the provided index
	 */
	public void setCursorPosition(final int par1) {
		cursorPosition = par1;
		final int var2 = text.length();

		if (cursorPosition < 0) {
			cursorPosition = 0;
		}

		if (cursorPosition > var2) {
			cursorPosition = var2;
		}

		setSelectionPos(cursorPosition);
	}

	/**
	 * sets the cursors position to the beginning
	 */
	public void setCursorPositionZero() {
		setCursorPosition(0);
	}

	/**
	 * sets the cursors position to after the text
	 */
	public void setCursorPositionEnd() {
		setCursorPosition(text.length());
	}

	/**
	 * Call this method from you GuiScreen to process the keys into textbox.
	 */
	public boolean textboxKeyTyped(final char par1, final int par2) {
		if (isEnabled && isFocused) {
			switch (par1) {
			case 1:
				setCursorPositionEnd();
				setSelectionPos(0);
				return true;

			case 3:
				GuiScreen.setClipboardString(getSelectedtext());
				return true;

			case 22:
				writeText(GuiScreen.getClipboardString());
				return true;

			case 24:
				GuiScreen.setClipboardString(getSelectedtext());
				writeText("");
				return true;

			default:
				switch (par2) {
				case 14:
					if (GuiScreen.isCtrlKeyDown()) {
						deleteWords(-1);
					} else {
						deleteFromCursor(-1);
					}

					return true;

				case 199:
					if (GuiScreen.isShiftKeyDown()) {
						setSelectionPos(0);
					} else {
						setCursorPositionZero();
					}

					return true;

				case 203:
					if (GuiScreen.isShiftKeyDown()) {
						if (GuiScreen.isCtrlKeyDown()) {
							setSelectionPos(getNthWordFromPos(-1,
									getSelectionEnd()));
						} else {
							setSelectionPos(getSelectionEnd() - 1);
						}
					} else if (GuiScreen.isCtrlKeyDown()) {
						setCursorPosition(getNthWordFromCursor(-1));
					} else {
						moveCursorBy(-1);
					}

					return true;

				case 205:
					if (GuiScreen.isShiftKeyDown()) {
						if (GuiScreen.isCtrlKeyDown()) {
							setSelectionPos(getNthWordFromPos(1,
									getSelectionEnd()));
						} else {
							setSelectionPos(getSelectionEnd() + 1);
						}
					} else if (GuiScreen.isCtrlKeyDown()) {
						setCursorPosition(getNthWordFromCursor(1));
					} else {
						moveCursorBy(1);
					}

					return true;

				case 207:
					if (GuiScreen.isShiftKeyDown()) {
						setSelectionPos(text.length());
					} else {
						setCursorPositionEnd();
					}

					return true;

				case 211:
					if (GuiScreen.isCtrlKeyDown()) {
						deleteWords(1);
					} else {
						deleteFromCursor(1);
					}

					return true;

				default:
					if (ChatAllowedCharacters.isAllowedCharacter(par1)) {
						writeText(Character.toString(par1));
						return true;
					} else {
						return false;
					}
				}
			}
		} else {
			return false;
		}
	}

	/**
	 * Args: x, y, buttonClicked
	 */
	public void mouseClicked(final int par1, final int par2, final int par3) {
		final boolean var4 = par1 >= xPos && par1 < xPos + width
				&& par2 >= yPos && par2 < yPos + height;

		if (canLoseFocus) {
			setFocused(isEnabled && var4);
		}

		if (isFocused && par3 == 0) {
			int var5 = par1 - xPos;

			if (enableBackgroundDrawing) {
				var5 -= 4;
			}

			final String var6 = fontRenderer.trimStringToWidth(
					text.substring(lineScrollOffset), getWidth());
			setCursorPosition(fontRenderer.trimStringToWidth(var6, var5)
					.length() + lineScrollOffset);
		}
	}

	/**
	 * Draws the textbox
	 */
	public void drawTextBox() {
		if (getVisible()) {
			if (getEnableBackgroundDrawing()) {
				Gui.drawRect(xPos - 1, yPos - 1, xPos + width + 1, yPos
						+ height + 1, -6250336);
				Gui.drawRect(xPos, yPos, xPos + width, yPos + height, -16777216);
			}

			final int var1 = isEnabled ? enabledColor : disabledColor;
			final int var2 = cursorPosition - lineScrollOffset;
			int var3 = selectionEnd - lineScrollOffset;
			final String var4 = fontRenderer.trimStringToWidth(
					text.substring(lineScrollOffset), getWidth());
			final boolean var5 = var2 >= 0 && var2 <= var4.length();
			final boolean var6 = isFocused && cursorCounter / 6 % 2 == 0
					&& var5;
			final int var7 = enableBackgroundDrawing ? xPos + 4 : xPos;
			final int var8 = enableBackgroundDrawing ? yPos + (height - 8) / 2
					: yPos;
			int var9 = var7;

			if (var3 > var4.length()) {
				var3 = var4.length();
			}

			if (var4.length() > 0) {
				final String var10 = var5 ? var4.substring(0, var2) : var4;
				var9 = fontRenderer.drawStringWithShadow(var10, var7, var8,
						var1);
			}

			final boolean var13 = cursorPosition < text.length()
					|| text.length() >= getMaxStringLength();
			int var11 = var9;

			if (!var5) {
				var11 = var2 > 0 ? var7 + width : var7;
			} else if (var13) {
				var11 = var9 - 1;
				--var9;
			}

			if (var4.length() > 0 && var5 && var2 < var4.length()) {
				fontRenderer.drawStringWithShadow(var4.substring(var2), var9,
						var8, var1);
			}

			if (var6) {
				if (var13) {
					Gui.drawRect(var11, var8 - 1, var11 + 1, var8 + 1
							+ fontRenderer.FONT_HEIGHT, -3092272);
				} else {
					fontRenderer.drawStringWithShadow("_", var11, var8, var1);
				}
			}

			if (var3 != var2) {
				final int var12 = var7
						+ fontRenderer.getStringWidth(var4.substring(0, var3));
				drawCursorVertical(var11, var8 - 1, var12 - 1, var8 + 1
						+ fontRenderer.FONT_HEIGHT);
			}
		}
	}

	/**
	 * draws the vertical line cursor in the textbox
	 */
	private void drawCursorVertical(int par1, int par2, int par3, int par4) {
		int var5;

		if (par1 < par3) {
			var5 = par1;
			par1 = par3;
			par3 = var5;
		}

		if (par2 < par4) {
			var5 = par2;
			par2 = par4;
			par4 = var5;
		}

		final Tessellator var6 = Tessellator.instance;
		GL11.glColor4f(0.0F, 0.0F, 255.0F, 255.0F);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_COLOR_LOGIC_OP);
		GL11.glLogicOp(GL11.GL_OR_REVERSE);
		var6.startDrawingQuads();
		var6.addVertex(par1, par4, 0.0D);
		var6.addVertex(par3, par4, 0.0D);
		var6.addVertex(par3, par2, 0.0D);
		var6.addVertex(par1, par2, 0.0D);
		var6.draw();
		GL11.glDisable(GL11.GL_COLOR_LOGIC_OP);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
	}

	public void setMaxStringLength(final int par1) {
		maxStringLength = par1;

		if (text.length() > par1) {
			text = text.substring(0, par1);
		}
	}

	/**
	 * returns the maximum number of character that can be contained in this
	 * textbox
	 */
	public int getMaxStringLength() {
		return maxStringLength;
	}

	/**
	 * returns the current position of the cursor
	 */
	public int getCursorPosition() {
		return cursorPosition;
	}

	/**
	 * get enable drawing background and outline
	 */
	public boolean getEnableBackgroundDrawing() {
		return enableBackgroundDrawing;
	}

	/**
	 * enable drawing background and outline
	 */
	public void setEnableBackgroundDrawing(final boolean par1) {
		enableBackgroundDrawing = par1;
	}

	/**
	 * Sets the text colour for this textbox (disabled text will not use this
	 * colour)
	 */
	public void setTextColor(final int par1) {
		enabledColor = par1;
	}

	public void setDisabledTextColour(final int par1) {
		disabledColor = par1;
	}

	/**
	 * setter for the focused field
	 */
	public void setFocused(final boolean par1) {
		if (par1 && !isFocused) {
			cursorCounter = 0;
		}

		isFocused = par1;
	}

	/**
	 * getter for the focused field
	 */
	public boolean isFocused() {
		return isFocused;
	}

	public void setEnabled(final boolean par1) {
		isEnabled = par1;
	}

	/**
	 * the side of the selection that is not the cursor, maye be the same as the
	 * cursor
	 */
	public int getSelectionEnd() {
		return selectionEnd;
	}

	/**
	 * returns the width of the textbox depending on if the the box is enabled
	 */
	public int getWidth() {
		return getEnableBackgroundDrawing() ? width - 8 : width;
	}

	/**
	 * Sets the position of the selection anchor (i.e. position the selection
	 * was started at)
	 */
	public void setSelectionPos(int par1) {
		final int var2 = text.length();

		if (par1 > var2) {
			par1 = var2;
		}

		if (par1 < 0) {
			par1 = 0;
		}

		selectionEnd = par1;

		if (fontRenderer != null) {
			if (lineScrollOffset > var2) {
				lineScrollOffset = var2;
			}

			final int var3 = getWidth();
			final String var4 = fontRenderer.trimStringToWidth(
					text.substring(lineScrollOffset), var3);
			final int var5 = var4.length() + lineScrollOffset;

			if (par1 == lineScrollOffset) {
				lineScrollOffset -= fontRenderer.trimStringToWidth(text, var3,
						true).length();
			}

			if (par1 > var5) {
				lineScrollOffset += par1 - var5;
			} else if (par1 <= lineScrollOffset) {
				lineScrollOffset -= lineScrollOffset - par1;
			}

			if (lineScrollOffset < 0) {
				lineScrollOffset = 0;
			}

			if (lineScrollOffset > var2) {
				lineScrollOffset = var2;
			}
		}
	}

	/**
	 * if true the textbox can lose focus by clicking elsewhere on the screen
	 */
	public void setCanLoseFocus(final boolean par1) {
		canLoseFocus = par1;
	}

	/**
	 * @return {@code true} if this textbox is visible
	 */
	public boolean getVisible() {
		return visible;
	}

	/**
	 * Sets whether or not this textbox is visible
	 */
	public void setVisible(final boolean par1) {
		visible = par1;
	}
}
