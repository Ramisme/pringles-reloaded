package net.minecraft.server;

import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.IOException;
import java.security.KeyPair;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import net.minecraft.src.AnvilSaveConverter;
import net.minecraft.src.AxisAlignedBB;
import net.minecraft.src.CallableIsServerModded;
import net.minecraft.src.CallableServerMemoryStats;
import net.minecraft.src.CallableServerProfiler;
import net.minecraft.src.ChunkCoordinates;
import net.minecraft.src.CommandBase;
import net.minecraft.src.ConvertingProgressUpdate;
import net.minecraft.src.CrashReport;
import net.minecraft.src.DemoWorldServer;
import net.minecraft.src.DispenserBehaviors;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.EnumGameType;
import net.minecraft.src.ICommandManager;
import net.minecraft.src.ICommandSender;
import net.minecraft.src.ILogAgent;
import net.minecraft.src.IPlayerUsage;
import net.minecraft.src.IProgressUpdate;
import net.minecraft.src.ISaveFormat;
import net.minecraft.src.ISaveHandler;
import net.minecraft.src.IUpdatePlayerListBox;
import net.minecraft.src.MathHelper;
import net.minecraft.src.MinecraftException;
import net.minecraft.src.NetworkListenThread;
import net.minecraft.src.Packet;
import net.minecraft.src.Packet4UpdateTime;
import net.minecraft.src.PlayerUsageSnooper;
import net.minecraft.src.Profiler;
import net.minecraft.src.RConConsoleSource;
import net.minecraft.src.ReportedException;
import net.minecraft.src.ServerCommandManager;
import net.minecraft.src.ServerConfigurationManager;
import net.minecraft.src.StringTranslate;
import net.minecraft.src.StringUtils;
import net.minecraft.src.ThreadMinecraftServer;
import net.minecraft.src.World;
import net.minecraft.src.WorldInfo;
import net.minecraft.src.WorldManager;
import net.minecraft.src.WorldServer;
import net.minecraft.src.WorldServerMulti;
import net.minecraft.src.WorldSettings;
import net.minecraft.src.WorldType;

public abstract class MinecraftServer implements ICommandSender, Runnable,
		IPlayerUsage {
	/** Instance of Minecraft Server. */
	private static MinecraftServer mcServer = null;
	private final ISaveFormat anvilConverterForAnvilFile;

	/** The PlayerUsageSnooper instance. */
	private final PlayerUsageSnooper usageSnooper = new PlayerUsageSnooper(
			"server", this);
	/**
	 * Collection of objects to update every tick. Type:
	 * List<IUpdatePlayerListBox>
	 */
	private final List tickables = new ArrayList();
	private final ICommandManager commandManager;
	public final Profiler theProfiler = new Profiler();

	/** The server's hostname. */
	private String hostname;

	/** The server's port. */
	private int serverPort = -1;

	/** The server world instances. */
	public WorldServer[] worldServers;

	/** The ServerConfigurationManager instance. */
	private ServerConfigurationManager serverConfigManager;

	/**
	 * Indicates whether the server is running or not. Set to false to initiate
	 * a shutdown.
	 */
	private boolean serverRunning = true;

	/** Indicates to other classes that the server is safely stopped. */
	private boolean serverStopped = false;

	/** Incremented every tick. */
	private int tickCounter = 0;

	/**
	 * The task the server is currently working on(and will output on
	 * outputPercentRemaining).
	 */
	public String currentTask;

	/** The percentage of the current task finished so far. */
	public int percentDone;

	/** True if the server is in online mode. */
	private boolean onlineMode;

	/** True if the server has animals turned on. */
	private boolean canSpawnAnimals;
	private boolean canSpawnNPCs;

	/** Indicates whether PvP is active on the server or not. */
	private boolean pvpEnabled;

	/** Determines if flight is allowed or not. */
	private boolean allowFlight;

	/** The server MOTD string. */
	private String motd;

	/** Maximum build height. */
	private int buildLimit;
	private long lastSentPacketID;
	private long lastSentPacketSize;
	private long lastReceivedID;
	private long lastReceivedSize;
	public final long[] sentPacketCountArray = new long[100];
	public final long[] sentPacketSizeArray = new long[100];
	public final long[] receivedPacketCountArray = new long[100];
	public final long[] receivedPacketSizeArray = new long[100];
	public final long[] tickTimeArray = new long[100];

	/** Stats are [dimension][tick%100] system.nanoTime is stored. */
	public long[][] timeOfLastDimensionTick;
	private KeyPair serverKeyPair;

	/** Username of the server owner (for integrated servers) */
	private String serverOwner;
	private String folderName;
	private String worldName;
	private boolean isDemo;
	private boolean enableBonusChest;

	/**
	 * If true, there is no need to save chunks or stop the server, because that
	 * is already being done.
	 */
	private boolean worldIsBeingDeleted;
	private String texturePack = "";
	private boolean serverIsRunning = false;

	/**
	 * Set when warned for "Can't keep up", which triggers again after 15
	 * seconds.
	 */
	private long timeOfLastWarning;
	private String userMessage;
	private boolean startProfiling;
	private boolean field_104057_T = false;

	public MinecraftServer(final File par1File) {
		MinecraftServer.mcServer = this;
		commandManager = new ServerCommandManager();
		anvilConverterForAnvilFile = new AnvilSaveConverter(par1File);
		registerDispenseBehaviors();
	}

	/**
	 * Register all dispense behaviors.
	 */
	private void registerDispenseBehaviors() {
		DispenserBehaviors.func_96467_a();
	}

	/**
	 * Initialises the server and starts it.
	 */
	protected abstract boolean startServer() throws IOException;

	protected void convertMapIfNeeded(final String par1Str) {
		if (getActiveAnvilConverter().isOldMapFormat(par1Str)) {
			getLogAgent().logInfo("Converting map!");
			setUserMessage("menu.convertingLevel");
			getActiveAnvilConverter().convertMapFormat(par1Str,
					new ConvertingProgressUpdate(this));
		}
	}

	/**
	 * Typically "menu.convertingLevel", "menu.loadingLevel" or others.
	 */
	protected synchronized void setUserMessage(final String par1Str) {
		userMessage = par1Str;
	}

	public synchronized String getUserMessage() {
		return userMessage;
	}

	protected void loadAllWorlds(final String par1Str, final String par2Str,
			final long par3, final WorldType par5WorldType, final String par6Str) {
		convertMapIfNeeded(par1Str);
		setUserMessage("menu.loadingLevel");
		worldServers = new WorldServer[3];
		timeOfLastDimensionTick = new long[worldServers.length][100];
		final ISaveHandler var7 = anvilConverterForAnvilFile.getSaveLoader(
				par1Str, true);
		final WorldInfo var9 = var7.loadWorldInfo();
		WorldSettings var8;

		if (var9 == null) {
			var8 = new WorldSettings(par3, getGameType(), canStructuresSpawn(),
					isHardcore(), par5WorldType);
			var8.func_82750_a(par6Str);
		} else {
			var8 = new WorldSettings(var9);
		}

		if (enableBonusChest) {
			var8.enableBonusChest();
		}

		for (int var10 = 0; var10 < worldServers.length; ++var10) {
			byte var11 = 0;

			if (var10 == 1) {
				var11 = -1;
			}

			if (var10 == 2) {
				var11 = 1;
			}

			if (var10 == 0) {
				if (isDemo()) {
					worldServers[var10] = new DemoWorldServer(this, var7,
							par2Str, var11, theProfiler, getLogAgent());
				} else {
					worldServers[var10] = new WorldServer(this, var7, par2Str,
							var11, var8, theProfiler, getLogAgent());
				}
			} else {
				worldServers[var10] = new WorldServerMulti(this, var7, par2Str,
						var11, var8, worldServers[0], theProfiler,
						getLogAgent());
			}

			worldServers[var10].addWorldAccess(new WorldManager(this,
					worldServers[var10]));

			if (!isSinglePlayer()) {
				worldServers[var10].getWorldInfo().setGameType(getGameType());
			}

			serverConfigManager.setPlayerManager(worldServers);
		}

		setDifficultyForAllWorlds(getDifficulty());
		initialWorldChunkLoad();
	}

	protected void initialWorldChunkLoad() {
		int var5 = 0;
		setUserMessage("menu.generatingTerrain");
		final byte var6 = 0;
		getLogAgent().logInfo("Preparing start region for level " + var6);
		final WorldServer var7 = worldServers[var6];
		final ChunkCoordinates var8 = var7.getSpawnPoint();
		long var9 = System.currentTimeMillis();

		for (int var11 = -192; var11 <= 192 && isServerRunning(); var11 += 16) {
			for (int var12 = -192; var12 <= 192 && isServerRunning(); var12 += 16) {
				final long var13 = System.currentTimeMillis();

				if (var13 - var9 > 1000L) {
					outputPercentRemaining("Preparing spawn area",
							var5 * 100 / 625);
					var9 = var13;
				}

				++var5;
				var7.theChunkProviderServer.loadChunk(var8.posX + var11 >> 4,
						var8.posZ + var12 >> 4);
			}
		}

		clearCurrentTask();
	}

	public abstract boolean canStructuresSpawn();

	public abstract EnumGameType getGameType();

	/**
	 * Defaults to "1" (Easy) for the dedicated server, defaults to "2" (Normal)
	 * on the client.
	 */
	public abstract int getDifficulty();

	/**
	 * Defaults to false.
	 */
	public abstract boolean isHardcore();

	/**
	 * Used to display a percent remaining given text and the percentage.
	 */
	protected void outputPercentRemaining(final String par1Str, final int par2) {
		currentTask = par1Str;
		percentDone = par2;
		getLogAgent().logInfo(par1Str + ": " + par2 + "%");
	}

	/**
	 * Set current task to null and set its percentage to 0.
	 */
	protected void clearCurrentTask() {
		currentTask = null;
		percentDone = 0;
	}

	/**
	 * par1 indicates if a log message should be output.
	 */
	protected void saveAllWorlds(final boolean par1) {
		if (!worldIsBeingDeleted) {
			final WorldServer[] var2 = worldServers;
			final int var3 = var2.length;

			for (int var4 = 0; var4 < var3; ++var4) {
				final WorldServer var5 = var2[var4];

				if (var5 != null) {
					if (!par1) {
						getLogAgent().logInfo(
								"Saving chunks for level \'"
										+ var5.getWorldInfo().getWorldName()
										+ "\'/"
										+ var5.provider.getDimensionName());
					}

					try {
						var5.saveAllChunks(true, (IProgressUpdate) null);
					} catch (final MinecraftException var7) {
						getLogAgent().logWarning(var7.getMessage());
					}
				}
			}
		}
	}

	/**
	 * Saves all necessary data as preparation for stopping the server.
	 */
	public void stopServer() {
		if (!worldIsBeingDeleted) {
			getLogAgent().logInfo("Stopping server");

			if (getNetworkThread() != null) {
				getNetworkThread().stopListening();
			}

			if (serverConfigManager != null) {
				getLogAgent().logInfo("Saving players");
				serverConfigManager.saveAllPlayerData();
				serverConfigManager.removeAllPlayers();
			}

			getLogAgent().logInfo("Saving worlds");
			saveAllWorlds(false);

			for (final WorldServer var2 : worldServers) {
				var2.flush();
			}

			if (usageSnooper != null && usageSnooper.isSnooperRunning()) {
				usageSnooper.stopSnooper();
			}
		}
	}

	/**
	 * "getHostname" is already taken, but both return the hostname.
	 */
	public String getServerHostname() {
		return hostname;
	}

	public void setHostname(final String par1Str) {
		hostname = par1Str;
	}

	public boolean isServerRunning() {
		return serverRunning;
	}

	/**
	 * Sets the serverRunning variable to false, in order to get the server to
	 * shut down.
	 */
	public void initiateShutdown() {
		serverRunning = false;
	}

	@Override
	public void run() {
		try {
			if (startServer()) {
				long var1 = System.currentTimeMillis();

				for (long var50 = 0L; serverRunning; serverIsRunning = true) {
					final long var5 = System.currentTimeMillis();
					long var7 = var5 - var1;

					if (var7 > 2000L && var1 - timeOfLastWarning >= 15000L) {
						getLogAgent()
								.logWarning(
										"Can\'t keep up! Did the system time change, or is the server overloaded?");
						var7 = 2000L;
						timeOfLastWarning = var1;
					}

					if (var7 < 0L) {
						getLogAgent()
								.logWarning(
										"Time ran backwards! Did the system time change?");
						var7 = 0L;
					}

					var50 += var7;
					var1 = var5;

					if (worldServers[0].areAllPlayersAsleep()) {
						tick();
						var50 = 0L;
					} else {
						while (var50 > 50L) {
							var50 -= 50L;
							tick();
						}
					}

					Thread.sleep(1L);
				}
			} else {
				finalTick((CrashReport) null);
			}
		} catch (final Throwable var48) {
			var48.printStackTrace();
			getLogAgent().logSevereException(
					"Encountered an unexpected exception "
							+ var48.getClass().getSimpleName(), var48);
			CrashReport var2 = null;

			if (var48 instanceof ReportedException) {
				var2 = addServerInfoToCrashReport(((ReportedException) var48)
						.getCrashReport());
			} else {
				var2 = addServerInfoToCrashReport(new CrashReport(
						"Exception in server tick loop", var48));
			}

			final File var3 = new File(new File(getDataDirectory(),
					"crash-reports"),
					"crash-"
							+ new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss")
									.format(new Date()) + "-server.txt");

			if (var2.saveToFile(var3, getLogAgent())) {
				getLogAgent().logSevere(
						"This crash report has been saved to: "
								+ var3.getAbsolutePath());
			} else {
				getLogAgent().logSevere(
						"We were unable to save this crash report to disk.");
			}

			finalTick(var2);
		} finally {
			try {
				stopServer();
				serverStopped = true;
			} catch (final Throwable var46) {
				var46.printStackTrace();
			} finally {
				systemExitNow();
			}
		}
	}

	protected File getDataDirectory() {
		return new File(".");
	}

	/**
	 * Called on exit from the main run() loop.
	 */
	protected void finalTick(final CrashReport par1CrashReport) {
	}

	/**
	 * Directly calls System.exit(0), instantly killing the program.
	 */
	protected void systemExitNow() {
	}

	/**
	 * Main function called by run() every loop.
	 */
	public void tick() {
		final long var1 = System.nanoTime();
		AxisAlignedBB.getAABBPool().cleanPool();
		++tickCounter;

		if (startProfiling) {
			startProfiling = false;
			theProfiler.profilingEnabled = true;
			theProfiler.clearProfiling();
		}

		theProfiler.startSection("root");
		updateTimeLightAndEntities();

		if (tickCounter % 900 == 0) {
			theProfiler.startSection("save");
			serverConfigManager.saveAllPlayerData();
			saveAllWorlds(true);
			theProfiler.endSection();
		}

		theProfiler.startSection("tallying");
		tickTimeArray[tickCounter % 100] = System.nanoTime() - var1;
		sentPacketCountArray[tickCounter % 100] = Packet.sentID
				- lastSentPacketID;
		lastSentPacketID = Packet.sentID;
		sentPacketSizeArray[tickCounter % 100] = Packet.sentSize
				- lastSentPacketSize;
		lastSentPacketSize = Packet.sentSize;
		receivedPacketCountArray[tickCounter % 100] = Packet.receivedID
				- lastReceivedID;
		lastReceivedID = Packet.receivedID;
		receivedPacketSizeArray[tickCounter % 100] = Packet.receivedSize
				- lastReceivedSize;
		lastReceivedSize = Packet.receivedSize;
		theProfiler.endSection();
		theProfiler.startSection("snooper");

		if (!usageSnooper.isSnooperRunning() && tickCounter > 100) {
			usageSnooper.startSnooper();
		}

		if (tickCounter % 6000 == 0) {
			usageSnooper.addMemoryStatsToSnooper();
		}

		theProfiler.endSection();
		theProfiler.endSection();
	}

	public void updateTimeLightAndEntities() {
		theProfiler.startSection("levels");
		int var1;

		for (var1 = 0; var1 < worldServers.length; ++var1) {
			final long var2 = System.nanoTime();

			if (var1 == 0 || getAllowNether()) {
				final WorldServer var4 = worldServers[var1];
				theProfiler.startSection(var4.getWorldInfo().getWorldName());
				theProfiler.startSection("pools");
				var4.getWorldVec3Pool().clear();
				theProfiler.endSection();

				if (tickCounter % 20 == 0) {
					theProfiler.startSection("timeSync");
					serverConfigManager.sendPacketToAllPlayersInDimension(
							new Packet4UpdateTime(var4.getTotalWorldTime(),
									var4.getWorldTime()),
							var4.provider.dimensionId);
					theProfiler.endSection();
				}

				theProfiler.startSection("tick");
				CrashReport var6;

				try {
					var4.tick();
				} catch (final Throwable var8) {
					var6 = CrashReport.makeCrashReport(var8,
							"Exception ticking world");
					var4.addWorldInfoToCrashReport(var6);
					throw new ReportedException(var6);
				}

				try {
					var4.updateEntities();
				} catch (final Throwable var7) {
					var6 = CrashReport.makeCrashReport(var7,
							"Exception ticking world entities");
					var4.addWorldInfoToCrashReport(var6);
					throw new ReportedException(var6);
				}

				theProfiler.endSection();
				theProfiler.startSection("tracker");
				var4.getEntityTracker().updateTrackedEntities();
				theProfiler.endSection();
				theProfiler.endSection();
			}

			timeOfLastDimensionTick[var1][tickCounter % 100] = System
					.nanoTime() - var2;
		}

		theProfiler.endStartSection("connection");
		getNetworkThread().networkTick();
		theProfiler.endStartSection("players");
		serverConfigManager.sendPlayerInfoToAllPlayers();
		theProfiler.endStartSection("tickables");

		for (var1 = 0; var1 < tickables.size(); ++var1) {
			((IUpdatePlayerListBox) tickables.get(var1)).update();
		}

		theProfiler.endSection();
	}

	public boolean getAllowNether() {
		return true;
	}

	public void startServerThread() {
		new ThreadMinecraftServer(this, "Server thread").start();
	}

	/**
	 * Returns a File object from the specified string.
	 */
	public File getFile(final String par1Str) {
		return new File(getDataDirectory(), par1Str);
	}

	/**
	 * Logs the message with a level of INFO.
	 */
	public void logInfo(final String par1Str) {
		getLogAgent().logInfo(par1Str);
	}

	/**
	 * Logs the message with a level of WARN.
	 */
	public void logWarning(final String par1Str) {
		getLogAgent().logWarning(par1Str);
	}

	/**
	 * Gets the worldServer by the given dimension.
	 */
	public WorldServer worldServerForDimension(final int par1) {
		return par1 == -1 ? worldServers[1] : par1 == 1 ? worldServers[2]
				: worldServers[0];
	}

	/**
	 * Returns the server's hostname.
	 */
	public String getHostname() {
		return hostname;
	}

	/**
	 * Never used, but "getServerPort" is already taken.
	 */
	public int getPort() {
		return serverPort;
	}

	/**
	 * Returns the server message of the day
	 */
	public String getServerMOTD() {
		return motd;
	}

	/**
	 * Returns the server's Minecraft version as string.
	 */
	public String getMinecraftVersion() {
		return "1.5.2";
	}

	/**
	 * Returns the number of players currently on the server.
	 */
	public int getCurrentPlayerCount() {
		return serverConfigManager.getCurrentPlayerCount();
	}

	/**
	 * Returns the maximum number of players allowed on the server.
	 */
	public int getMaxPlayers() {
		return serverConfigManager.getMaxPlayers();
	}

	/**
	 * Returns an array of the usernames of all the connected players.
	 */
	public String[] getAllUsernames() {
		return serverConfigManager.getAllUsernames();
	}

	/**
	 * Used by RCon's Query in the form of
	 * "MajorServerMod 1.2.3: MyPlugin 1.3; AnotherPlugin 2.1; AndSoForth 1.0".
	 */
	public String getPlugins() {
		return "";
	}

	public String executeCommand(final String par1Str) {
		RConConsoleSource.consoleBuffer.resetLog();
		commandManager.executeCommand(RConConsoleSource.consoleBuffer, par1Str);
		return RConConsoleSource.consoleBuffer.getChatBuffer();
	}

	/**
	 * Returns true if debugging is enabled, false otherwise.
	 */
	public boolean isDebuggingEnabled() {
		return false;
	}

	/**
	 * Logs the error message with a level of SEVERE.
	 */
	public void logSevere(final String par1Str) {
		getLogAgent().logSevere(par1Str);
	}

	/**
	 * If isDebuggingEnabled(), logs the message with a level of INFO.
	 */
	public void logDebug(final String par1Str) {
		if (isDebuggingEnabled()) {
			getLogAgent().logInfo(par1Str);
		}
	}

	public String getServerModName() {
		return "vanilla";
	}

	/**
	 * Adds the server info, including from theWorldServer, to the crash report.
	 */
	public CrashReport addServerInfoToCrashReport(
			final CrashReport par1CrashReport) {
		par1CrashReport.func_85056_g().addCrashSectionCallable(
				"Profiler Position", new CallableIsServerModded(this));

		if (worldServers != null && worldServers.length > 0
				&& worldServers[0] != null) {
			par1CrashReport.func_85056_g().addCrashSectionCallable(
					"Vec3 Pool Size", new CallableServerProfiler(this));
		}

		if (serverConfigManager != null) {
			par1CrashReport.func_85056_g().addCrashSectionCallable(
					"Player Count", new CallableServerMemoryStats(this));
		}

		return par1CrashReport;
	}

	/**
	 * If par2Str begins with /, then it searches for commands, otherwise it
	 * returns players.
	 */
	public List getPossibleCompletions(final ICommandSender par1ICommandSender,
			String par2Str) {
		final ArrayList var3 = new ArrayList();

		if (par2Str.startsWith("/")) {
			par2Str = par2Str.substring(1);
			final boolean var10 = !par2Str.contains(" ");
			final List var11 = commandManager.getPossibleCommands(
					par1ICommandSender, par2Str);

			if (var11 != null) {
				final Iterator var12 = var11.iterator();

				while (var12.hasNext()) {
					final String var13 = (String) var12.next();

					if (var10) {
						var3.add("/" + var13);
					} else {
						var3.add(var13);
					}
				}
			}

			return var3;
		} else {
			final String[] var4 = par2Str.split(" ", -1);
			final String var5 = var4[var4.length - 1];
			final String[] var6 = serverConfigManager.getAllUsernames();
			final int var7 = var6.length;

			for (int var8 = 0; var8 < var7; ++var8) {
				final String var9 = var6[var8];

				if (CommandBase.doesStringStartWith(var5, var9)) {
					var3.add(var9);
				}
			}

			return var3;
		}
	}

	/**
	 * Gets mcServer.
	 */
	public static MinecraftServer getServer() {
		return MinecraftServer.mcServer;
	}

	/**
	 * Gets the name of this command sender (usually username, but possibly
	 * "Rcon")
	 */
	@Override
	public String getCommandSenderName() {
		return "Server";
	}

	@Override
	public void sendChatToPlayer(final String par1Str) {
		getLogAgent().logInfo(StringUtils.stripControlCodes(par1Str));
	}

	/**
	 * Returns true if the command sender is allowed to use the given command.
	 */
	@Override
	public boolean canCommandSenderUseCommand(final int par1,
			final String par2Str) {
		return true;
	}

	/**
	 * Translates and formats the given string key with the given arguments.
	 */
	@Override
	public String translateString(final String par1Str,
			final Object... par2ArrayOfObj) {
		return StringTranslate.getInstance().translateKeyFormat(par1Str,
				par2ArrayOfObj);
	}

	public ICommandManager getCommandManager() {
		return commandManager;
	}

	/**
	 * Gets KeyPair instanced in MinecraftServer.
	 */
	public KeyPair getKeyPair() {
		return serverKeyPair;
	}

	/**
	 * Gets serverPort.
	 */
	public int getServerPort() {
		return serverPort;
	}

	public void setServerPort(final int par1) {
		serverPort = par1;
	}

	/**
	 * Returns the username of the server owner (for integrated servers)
	 */
	public String getServerOwner() {
		return serverOwner;
	}

	/**
	 * Sets the username of the owner of this server (in the case of an
	 * integrated server)
	 */
	public void setServerOwner(final String par1Str) {
		serverOwner = par1Str;
	}

	public boolean isSinglePlayer() {
		return serverOwner != null;
	}

	public String getFolderName() {
		return folderName;
	}

	public void setFolderName(final String par1Str) {
		folderName = par1Str;
	}

	public void setWorldName(final String par1Str) {
		worldName = par1Str;
	}

	public String getWorldName() {
		return worldName;
	}

	public void setKeyPair(final KeyPair par1KeyPair) {
		serverKeyPair = par1KeyPair;
	}

	public void setDifficultyForAllWorlds(final int par1) {
		for (final WorldServer worldServer : worldServers) {
			final WorldServer var3 = worldServer;

			if (var3 != null) {
				if (var3.getWorldInfo().isHardcoreModeEnabled()) {
					var3.difficultySetting = 3;
					var3.setAllowedSpawnTypes(true, true);
				} else if (isSinglePlayer()) {
					var3.difficultySetting = par1;
					var3.setAllowedSpawnTypes(var3.difficultySetting > 0, true);
				} else {
					var3.difficultySetting = par1;
					var3.setAllowedSpawnTypes(allowSpawnMonsters(),
							canSpawnAnimals);
				}
			}
		}
	}

	protected boolean allowSpawnMonsters() {
		return true;
	}

	/**
	 * Gets whether this is a demo or not.
	 */
	public boolean isDemo() {
		return isDemo;
	}

	/**
	 * Sets whether this is a demo or not.
	 */
	public void setDemo(final boolean par1) {
		isDemo = par1;
	}

	public void canCreateBonusChest(final boolean par1) {
		enableBonusChest = par1;
	}

	public ISaveFormat getActiveAnvilConverter() {
		return anvilConverterForAnvilFile;
	}

	/**
	 * WARNING : directly calls
	 * getActiveAnvilConverter().deleteWorldDirectory(theWorldServer
	 * [0].getSaveHandler().getWorldDirectoryName());
	 */
	public void deleteWorldAndStopServer() {
		worldIsBeingDeleted = true;
		getActiveAnvilConverter().flushCache();

		for (final WorldServer var2 : worldServers) {
			if (var2 != null) {
				var2.flush();
			}
		}

		getActiveAnvilConverter().deleteWorldDirectory(
				worldServers[0].getSaveHandler().getWorldDirectoryName());
		initiateShutdown();
	}

	public String getTexturePack() {
		return texturePack;
	}

	public void setTexturePack(final String par1Str) {
		texturePack = par1Str;
	}

	@Override
	public void addServerStatsToSnooper(
			final PlayerUsageSnooper par1PlayerUsageSnooper) {
		par1PlayerUsageSnooper.addData("whitelist_enabled",
				Boolean.valueOf(false));
		par1PlayerUsageSnooper.addData("whitelist_count", Integer.valueOf(0));
		par1PlayerUsageSnooper.addData("players_current",
				Integer.valueOf(getCurrentPlayerCount()));
		par1PlayerUsageSnooper.addData("players_max",
				Integer.valueOf(getMaxPlayers()));
		par1PlayerUsageSnooper.addData("players_seen", Integer
				.valueOf(serverConfigManager.getAvailablePlayerDat().length));
		par1PlayerUsageSnooper
				.addData("uses_auth", Boolean.valueOf(onlineMode));
		par1PlayerUsageSnooper.addData("gui_state", getGuiEnabled() ? "enabled"
				: "disabled");
		par1PlayerUsageSnooper.addData("avg_tick_ms", Integer
				.valueOf((int) (MathHelper.average(tickTimeArray) * 1.0E-6D)));
		par1PlayerUsageSnooper
				.addData("avg_sent_packet_count",
						Integer.valueOf((int) MathHelper
								.average(sentPacketCountArray)));
		par1PlayerUsageSnooper.addData("avg_sent_packet_size",
				Integer.valueOf((int) MathHelper.average(sentPacketSizeArray)));
		par1PlayerUsageSnooper.addData("avg_rec_packet_count", Integer
				.valueOf((int) MathHelper.average(receivedPacketCountArray)));
		par1PlayerUsageSnooper.addData("avg_rec_packet_size", Integer
				.valueOf((int) MathHelper.average(receivedPacketSizeArray)));
		int var2 = 0;

		for (final WorldServer var4 : worldServers) {
			if (var4 != null) {
				final WorldInfo var5 = var4.getWorldInfo();
				par1PlayerUsageSnooper.addData(
						"world[" + var2 + "][dimension]",
						Integer.valueOf(var4.provider.dimensionId));
				par1PlayerUsageSnooper.addData("world[" + var2 + "][mode]",
						var5.getGameType());
				par1PlayerUsageSnooper.addData("world[" + var2
						+ "][difficulty]",
						Integer.valueOf(var4.difficultySetting));
				par1PlayerUsageSnooper.addData("world[" + var2 + "][hardcore]",
						Boolean.valueOf(var5.isHardcoreModeEnabled()));
				par1PlayerUsageSnooper.addData("world[" + var2
						+ "][generator_name]", var5.getTerrainType()
						.getWorldTypeName());
				par1PlayerUsageSnooper.addData("world[" + var2
						+ "][generator_version]", Integer.valueOf(var5
						.getTerrainType().getGeneratorVersion()));
				par1PlayerUsageSnooper.addData("world[" + var2 + "][height]",
						Integer.valueOf(buildLimit));
				par1PlayerUsageSnooper.addData("world[" + var2
						+ "][chunks_loaded]", Integer.valueOf(var4
						.getChunkProvider().getLoadedChunkCount()));
				++var2;
			}
		}

		par1PlayerUsageSnooper.addData("worlds", Integer.valueOf(var2));
	}

	@Override
	public void addServerTypeToSnooper(
			final PlayerUsageSnooper par1PlayerUsageSnooper) {
		par1PlayerUsageSnooper.addData("singleplayer",
				Boolean.valueOf(isSinglePlayer()));
		par1PlayerUsageSnooper.addData("server_brand", getServerModName());
		par1PlayerUsageSnooper.addData("gui_supported",
				GraphicsEnvironment.isHeadless() ? "headless" : "supported");
		par1PlayerUsageSnooper.addData("dedicated",
				Boolean.valueOf(isDedicatedServer()));
	}

	/**
	 * Returns whether snooping is enabled or not.
	 */
	@Override
	public boolean isSnooperEnabled() {
		return true;
	}

	/**
	 * This is checked to be 16 upon receiving the packet, otherwise the packet
	 * is ignored.
	 */
	public int textureSize() {
		return 16;
	}

	public abstract boolean isDedicatedServer();

	public boolean isServerInOnlineMode() {
		return onlineMode;
	}

	public void setOnlineMode(final boolean par1) {
		onlineMode = par1;
	}

	public boolean getCanSpawnAnimals() {
		return canSpawnAnimals;
	}

	public void setCanSpawnAnimals(final boolean par1) {
		canSpawnAnimals = par1;
	}

	public boolean getCanSpawnNPCs() {
		return canSpawnNPCs;
	}

	public void setCanSpawnNPCs(final boolean par1) {
		canSpawnNPCs = par1;
	}

	public boolean isPVPEnabled() {
		return pvpEnabled;
	}

	public void setAllowPvp(final boolean par1) {
		pvpEnabled = par1;
	}

	public boolean isFlightAllowed() {
		return allowFlight;
	}

	public void setAllowFlight(final boolean par1) {
		allowFlight = par1;
	}

	/**
	 * Return whether command blocks are enabled.
	 */
	public abstract boolean isCommandBlockEnabled();

	public String getMOTD() {
		return motd;
	}

	public void setMOTD(final String par1Str) {
		motd = par1Str;
	}

	public int getBuildLimit() {
		return buildLimit;
	}

	public void setBuildLimit(final int par1) {
		buildLimit = par1;
	}

	public boolean isServerStopped() {
		return serverStopped;
	}

	public ServerConfigurationManager getConfigurationManager() {
		return serverConfigManager;
	}

	public void setConfigurationManager(
			final ServerConfigurationManager par1ServerConfigurationManager) {
		serverConfigManager = par1ServerConfigurationManager;
	}

	/**
	 * Sets the game type for all worlds.
	 */
	public void setGameType(final EnumGameType par1EnumGameType) {
		for (int var2 = 0; var2 < worldServers.length; ++var2) {
			MinecraftServer.getServer().worldServers[var2].getWorldInfo()
					.setGameType(par1EnumGameType);
		}
	}

	public abstract NetworkListenThread getNetworkThread();

	public boolean serverIsInRunLoop() {
		return serverIsRunning;
	}

	public boolean getGuiEnabled() {
		return false;
	}

	/**
	 * On dedicated does nothing. On integrated, sets commandsAllowedForAll,
	 * gameType and allows external connections.
	 */
	public abstract String shareToLAN(EnumGameType var1, boolean var2);

	public int getTickCounter() {
		return tickCounter;
	}

	public void enableProfiling() {
		startProfiling = true;
	}

	public PlayerUsageSnooper getPlayerUsageSnooper() {
		return usageSnooper;
	}

	/**
	 * Return the position for this command sender.
	 */
	@Override
	public ChunkCoordinates getPlayerCoordinates() {
		return new ChunkCoordinates(0, 0, 0);
	}

	/**
	 * Return the spawn protection area's size.
	 */
	public int getSpawnProtectionSize() {
		return 16;
	}

	public boolean func_96290_a(final World par1World, final int par2,
			final int par3, final int par4, final EntityPlayer par5EntityPlayer) {
		return false;
	}

	@Override
	public abstract ILogAgent getLogAgent();

	public void func_104055_i(final boolean par1) {
		field_104057_T = par1;
	}

	public boolean func_104056_am() {
		return field_104057_T;
	}

	/**
	 * Gets the current player count, maximum player count, and player entity
	 * list.
	 */
	public static ServerConfigurationManager getServerConfigurationManager(
			final MinecraftServer par0MinecraftServer) {
		return par0MinecraftServer.serverConfigManager;
	}
}
