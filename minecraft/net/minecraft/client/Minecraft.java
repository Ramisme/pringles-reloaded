package net.minecraft.client;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.JPanel;

import net.minecraft.src.*;

import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.ContextCapabilities;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GLContext;
import org.lwjgl.opengl.PixelFormat;
import org.lwjgl.util.glu.GLU;
import org.ramisme.pringles.Pringles;
import org.ramisme.pringles.events.keyboard.KeyEvent;
import org.ramisme.pringles.ui.ingame.IngameUI;

public abstract class Minecraft implements Runnable, IPlayerUsage {
	/** A 10MiB preallocation to ensure the heap is reasonably sized. */
	public static byte[] memoryReserve = new byte[10485760];
	private final ILogAgent field_94139_O = new LogAgent("Minecraft-Client",
			" [CLIENT]", new File(Minecraft.getMinecraftDir(),
					"output-client.log").getAbsolutePath());
	private ServerData currentServerData;

	/**
	 * Set to 'this' in Minecraft constructor; used by some settings get methods
	 */
	private static Minecraft theMinecraft;
	public PlayerControllerMP playerController;
	private boolean fullscreen = false;
	private boolean hasCrashed = false;

	/** Instance of CrashReport. */
	private CrashReport crashReporter;
	public int displayWidth;
	public int displayHeight;
	private final Timer timer = new Timer(20.0F);

	/** Instance of PlayerUsageSnooper. */
	private final PlayerUsageSnooper usageSnooper = new PlayerUsageSnooper(
			"client", this);
	public WorldClient theWorld;
	public RenderGlobal renderGlobal;
	public EntityClientPlayerMP thePlayer;

	/**
	 * The Entity from which the renderer determines the render viewpoint.
	 * Currently is always the parent Minecraft class's 'thePlayer' instance.
	 * Modification of its location, rotation, or other settings at render time
	 * will modify the camera likewise, with the caveat of triggering chunk
	 * rebuilds as it moves, making it unsuitable for changing the viewpoint
	 * mid-render.
	 */
	public EntityLiving renderViewEntity;
	public EntityLiving pointedEntityLiving;
	public EffectRenderer effectRenderer;
	public Session session = null;
	public String minecraftUri;
	public Canvas mcCanvas;

	/** a boolean to hide a Quit button from the main menu */
	public boolean hideQuitButton = false;
	public volatile boolean isGamePaused = false;

	/** The RenderEngine instance used by Minecraft */
	public RenderEngine renderEngine;

	/** The font renderer used for displaying and measuring text. */
	public FontRenderer fontRenderer;
	public FontRenderer standardGalacticFontRenderer;

	/** The GuiScreen that's being displayed at the moment. */
	public GuiScreen currentScreen = null;
	public LoadingScreenRenderer loadingScreen;
	public EntityRenderer entityRenderer;

	/** Reference to the download resources thread. */
	private ThreadDownloadResources downloadResourcesThread;

	/** Mouse left click counter */
	private int leftClickCounter = 0;

	/** Display width */
	private int tempDisplayWidth;

	/** Display height */
	private final int tempDisplayHeight;

	/** Instance of IntegratedServer. */
	private IntegratedServer theIntegratedServer;

	/** Gui achievement */
	public GuiAchievement guiAchievement;
	public GuiIngame ingameGUI;

	/** Skip render world */
	public boolean skipRenderWorld = false;

	/** The ray trace hit that the mouse is over. */
	public MovingObjectPosition objectMouseOver = null;

	/** The game settings that currently hold effect. */
	public GameSettings gameSettings;
	protected MinecraftApplet mcApplet;
	public SoundManager sndManager = new SoundManager();

	/** Mouse helper instance. */
	public MouseHelper mouseHelper;

	/** The TexturePackLister used by this instance of Minecraft... */
	public TexturePackList texturePackList;
	public File mcDataDir;
	private ISaveFormat saveLoader;

	/**
	 * This is set to fpsCounter every debug screen update, and is shown on the
	 * debug screen. It's also sent as part of the usage snooping.
	 */
	private static int debugFPS;

	/**
	 * When you place a block, it's set to 6, decremented once per tick, when
	 * it's 0, you can place another block.
	 */
	private int rightClickDelayTimer = 0;

	/**
	 * Checked in Minecraft's while(running) loop, if true it's set to false and
	 * the textures refreshed.
	 */
	private boolean refreshTexturePacksScheduled;

	/** Stat file writer */
	public StatFileWriter statFileWriter;
	private String serverName;
	private int serverPort;

	/**
	 * Makes sure it doesn't keep taking screenshots when both buttons are down.
	 */
	boolean isTakingScreenshot = false;

	/**
	 * Does the actual gameplay have focus. If so then mouse and keys will
	 * effect the player instead of menus.
	 */
	public boolean inGameHasFocus = false;
	long systemTime = Minecraft.getSystemTime();

	/** Join player counter */
	private int joinPlayerCounter = 0;
	private boolean isDemo;
	private INetworkManager myNetworkManager;
	private boolean integratedServerIsRunning;

	/** The profiler instance */
	public final Profiler mcProfiler = new Profiler();
	private long field_83002_am = -1L;

	/** The working dir (OS specific) for minecraft */
	private static File minecraftDir = null;

	/**
	 * Set to true to keep the game loop running. Set to false by shutdown() to
	 * allow the game loop to exit cleanly.
	 */
	public volatile boolean running = true;

	/** String that shows the debug information */
	public String debug = "";

	/** Approximate time (in ms) of last update to debug string */
	long debugUpdateTime = Minecraft.getSystemTime();

	/** holds the current fps */
	int fpsCounter = 0;
	long prevFrameTime = -1L;

	/** Profiler currently displayed in the debug screen pie chart */
	private String debugProfilerName = "root";

	public Minecraft(final Canvas par1Canvas,
			final MinecraftApplet par2MinecraftApplet, final int par3,
			final int par4, final boolean par5) {
		StatList.nopInit();
		tempDisplayHeight = par4;
		fullscreen = par5;
		mcApplet = par2MinecraftApplet;
		Packet3Chat.maxChatLength = 32767;
		startTimerHackThread();
		mcCanvas = par1Canvas;
		displayWidth = par3;
		displayHeight = par4;
		fullscreen = par5;
		Minecraft.theMinecraft = this;
		TextureManager.init();
		guiAchievement = new GuiAchievement(this);
	}

	private void startTimerHackThread() {
		final ThreadClientSleep var1 = new ThreadClientSleep(this,
				"Timer hack thread");
		var1.setDaemon(true);
		var1.start();
	}

	public void crashed(final CrashReport par1CrashReport) {
		hasCrashed = true;
		crashReporter = par1CrashReport;
	}

	/**
	 * Wrapper around displayCrashReportInternal
	 */
	public void displayCrashReport(final CrashReport par1CrashReport) {
		hasCrashed = true;
		displayCrashReportInternal(par1CrashReport);
	}

	public abstract void displayCrashReportInternal(CrashReport var1);

	public void setServer(final String par1Str, final int par2) {
		serverName = par1Str;
		serverPort = par2;
	}

	/**
	 * Starts the game: initializes the canvas, the title, the settings,
	 * etcetera.
	 */
	public void startGame() throws LWJGLException {
		if (mcCanvas != null) {
			final Graphics var1 = mcCanvas.getGraphics();

			if (var1 != null) {
				var1.setColor(Color.BLACK);
				var1.fillRect(0, 0, displayWidth, displayHeight);
				var1.dispose();
			}

			Display.setParent(mcCanvas);
		} else if (fullscreen) {
			Display.setFullscreen(true);
			displayWidth = Display.getDisplayMode().getWidth();
			displayHeight = Display.getDisplayMode().getHeight();

			if (displayWidth <= 0) {
				displayWidth = 1;
			}

			if (displayHeight <= 0) {
				displayHeight = 1;
			}
		} else {
			Display.setDisplayMode(new DisplayMode(displayWidth, displayHeight));
		}

		Display.setTitle("Minecraft Minecraft 1.5.2");
		getLogAgent().logInfo("LWJGL Version: " + Sys.getVersion());

		try {
			Display.create(new PixelFormat().withDepthBits(24));
		} catch (final LWJGLException var5) {
			var5.printStackTrace();

			try {
				Thread.sleep(1000L);
			} catch (final InterruptedException var4) {
				;
			}

			Display.create();
		}

		OpenGlHelper.initializeTextures();
		mcDataDir = Minecraft.getMinecraftDir();
		saveLoader = new AnvilSaveConverter(new File(mcDataDir, "saves"));
		gameSettings = new GameSettings(this, mcDataDir);
		texturePackList = new TexturePackList(mcDataDir, this);
		renderEngine = new RenderEngine(texturePackList, gameSettings);
		loadScreen();
		fontRenderer = new FontRenderer(gameSettings, "/font/default.png",
				renderEngine, false);
		standardGalacticFontRenderer = new FontRenderer(gameSettings,
				"/font/alternate.png", renderEngine, false);

		if (gameSettings.language != null) {
			StringTranslate.getInstance().setLanguage(gameSettings.language,
					false);
			fontRenderer.setUnicodeFlag(StringTranslate.getInstance()
					.isUnicode());
			fontRenderer.setBidiFlag(StringTranslate
					.isBidirectional(gameSettings.language));
		}

		ColorizerGrass.setGrassBiomeColorizer(renderEngine
				.getTextureContents("/misc/grasscolor.png"));
		ColorizerFoliage.setFoliageBiomeColorizer(renderEngine
				.getTextureContents("/misc/foliagecolor.png"));
		entityRenderer = new EntityRenderer(this);
		RenderManager.instance.itemRenderer = new ItemRenderer(this);
		statFileWriter = new StatFileWriter(session, mcDataDir);
		AchievementList.openInventory
				.setStatStringFormatter(new StatStringFormatKeyInv(this));
		loadScreen();
		Mouse.create();
		mouseHelper = new MouseHelper(mcCanvas, gameSettings);
		checkGLError("Pre startup");
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glShadeModel(GL11.GL_SMOOTH);
		GL11.glClearDepth(1.0D);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glDepthFunc(GL11.GL_LEQUAL);
		GL11.glEnable(GL11.GL_ALPHA_TEST);
		GL11.glAlphaFunc(GL11.GL_GREATER, 0.1F);
		GL11.glCullFace(GL11.GL_BACK);
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		checkGLError("Startup");
		sndManager.loadSoundSettings(gameSettings);
		renderGlobal = new RenderGlobal(this, renderEngine);
		renderEngine.refreshTextureMaps();
		GL11.glViewport(0, 0, displayWidth, displayHeight);
		effectRenderer = new EffectRenderer(theWorld, renderEngine);

		try {
			downloadResourcesThread = new ThreadDownloadResources(mcDataDir,
					this);
			downloadResourcesThread.start();
		} catch (final Exception var3) {
			;
		}

		checkGLError("Post startup");
		// TODO: Minecraft#startGame
		ingameGUI = new IngameUI(this);
		// TODO: Minecraft#startGame
		Pringles.getInstance().onLoad();

		if (serverName != null) {
			displayGuiScreen(new GuiConnecting(new GuiMainMenu(), this,
					serverName, serverPort));
		} else {
			displayGuiScreen(new GuiMainMenu());
		}

		loadingScreen = new LoadingScreenRenderer(this);

		if (gameSettings.fullScreen && !fullscreen) {
			toggleFullscreen();
		}
	}

	/**
	 * Displays a new screen.
	 */
	private void loadScreen() throws LWJGLException {
		final ScaledResolution var1 = new ScaledResolution(gameSettings,
				displayWidth, displayHeight);
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glOrtho(0.0D, var1.getScaledWidth_double(),
				var1.getScaledHeight_double(), 0.0D, 1000.0D, 3000.0D);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glLoadIdentity();
		GL11.glTranslatef(0.0F, 0.0F, -2000.0F);
		GL11.glViewport(0, 0, displayWidth, displayHeight);
		GL11.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_FOG);
		final Tessellator var2 = Tessellator.instance;
		renderEngine.bindTexture("/title/mojang.png");
		var2.startDrawingQuads();
		var2.setColorOpaque_I(16777215);
		var2.addVertexWithUV(0.0D, displayHeight, 0.0D, 0.0D, 0.0D);
		var2.addVertexWithUV(displayWidth, displayHeight, 0.0D, 0.0D, 0.0D);
		var2.addVertexWithUV(displayWidth, 0.0D, 0.0D, 0.0D, 0.0D);
		var2.addVertexWithUV(0.0D, 0.0D, 0.0D, 0.0D, 0.0D);
		var2.draw();
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		var2.setColorOpaque_I(16777215);
		final short var3 = 256;
		final short var4 = 256;
		scaledTessellator((var1.getScaledWidth() - var3) / 2,
				(var1.getScaledHeight() - var4) / 2, 0, 0, var3, var4);
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glDisable(GL11.GL_FOG);
		GL11.glEnable(GL11.GL_ALPHA_TEST);
		GL11.glAlphaFunc(GL11.GL_GREATER, 0.1F);
		Display.swapBuffers();
	}

	/**
	 * Loads Tessellator with a scaled resolution
	 */
	public void scaledTessellator(final int par1, final int par2,
			final int par3, final int par4, final int par5, final int par6) {
		final float var7 = 0.00390625F;
		final float var8 = 0.00390625F;
		final Tessellator var9 = Tessellator.instance;
		var9.startDrawingQuads();
		var9.addVertexWithUV(par1 + 0, par2 + par6, 0.0D, (par3 + 0) * var7,
				(par4 + par6) * var8);
		var9.addVertexWithUV(par1 + par5, par2 + par6, 0.0D, (par3 + par5)
				* var7, (par4 + par6) * var8);
		var9.addVertexWithUV(par1 + par5, par2 + 0, 0.0D, (par3 + par5) * var7,
				(par4 + 0) * var8);
		var9.addVertexWithUV(par1 + 0, par2 + 0, 0.0D, (par3 + 0) * var7,
				(par4 + 0) * var8);
		var9.draw();
	}

	/**
	 * gets the working dir (OS specific) for minecraft
	 */
	public static File getMinecraftDir() {
		if (Minecraft.minecraftDir == null) {
			Minecraft.minecraftDir = Minecraft.getAppDir("minecraft");
		}

		return Minecraft.minecraftDir;
	}

	/**
	 * gets the working dir (OS specific) for the specific application (which is
	 * always minecraft)
	 */
	public static File getAppDir(final String par0Str) {
		final String var1 = System.getProperty("user.home", ".");
		File var2;

		switch (EnumOSHelper.field_90049_a[Minecraft.getOs().ordinal()]) {
		case 1:
		case 2:
			var2 = new File(var1, '.' + par0Str + '/');
			break;

		case 3:
			final String var3 = System.getenv("APPDATA");

			if (var3 != null) {
				var2 = new File(var3, "." + par0Str + '/');
			} else {
				var2 = new File(var1, '.' + par0Str + '/');
			}

			break;

		case 4:
			var2 = new File(var1, "Library/Application Support/" + par0Str);
			break;

		default:
			var2 = new File(var1, par0Str + '/');
		}

		if (!var2.exists() && !var2.mkdirs()) {
			throw new RuntimeException(
					"The working directory could not be created: " + var2);
		} else {
			return var2;
		}
	}

	public static EnumOS getOs() {
		final String var0 = System.getProperty("os.name").toLowerCase();
		return var0.contains("win") ? EnumOS.WINDOWS
				: var0.contains("mac") ? EnumOS.MACOS : var0
						.contains("solaris") ? EnumOS.SOLARIS : var0
						.contains("sunos") ? EnumOS.SOLARIS : var0
						.contains("linux") ? EnumOS.LINUX : var0
						.contains("unix") ? EnumOS.LINUX : EnumOS.UNKNOWN;
	}

	/**
	 * Returns the save loader that is currently being used
	 */
	public ISaveFormat getSaveLoader() {
		return saveLoader;
	}

	/**
	 * Sets the argument GuiScreen as the main (topmost visible) screen.
	 */
	public void displayGuiScreen(GuiScreen par1GuiScreen) {
		if (currentScreen != null) {
			currentScreen.onGuiClosed();
		}

		statFileWriter.syncStats();

		if (par1GuiScreen == null && theWorld == null) {
			par1GuiScreen = new GuiMainMenu();
		} else if (par1GuiScreen == null && thePlayer.getHealth() <= 0) {
			par1GuiScreen = new GuiGameOver();
		}

		if (par1GuiScreen instanceof GuiMainMenu) {
			gameSettings.showDebugInfo = false;
			ingameGUI.getChatGUI().clearChatMessages();
		}

		currentScreen = par1GuiScreen;

		if (par1GuiScreen != null) {
			setIngameNotInFocus();
			final ScaledResolution var2 = new ScaledResolution(gameSettings,
					displayWidth, displayHeight);
			final int var3 = var2.getScaledWidth();
			final int var4 = var2.getScaledHeight();
			par1GuiScreen.setWorldAndResolution(this, var3, var4);
			skipRenderWorld = false;
		} else {
			setIngameFocus();
		}
	}

	/**
	 * Checks for an OpenGL error. If there is one, prints the error ID and
	 * error string.
	 */
	private void checkGLError(final String par1Str) {
		final int var2 = GL11.glGetError();

		if (var2 != 0) {
			final String var3 = GLU.gluErrorString(var2);
			getLogAgent().logSevere("########## GL ERROR ##########");
			getLogAgent().logSevere("@ " + par1Str);
			getLogAgent().logSevere(var2 + ": " + var3);
		}
	}

	/**
	 * Shuts down the minecraft applet by stopping the resource downloads, and
	 * clearing up GL stuff; called when the application (or web page) is
	 * exited.
	 */
	public void shutdownMinecraftApplet() {
		try {
			statFileWriter.syncStats();

			try {
				if (downloadResourcesThread != null) {
					downloadResourcesThread.closeMinecraft();
				}
			} catch (final Exception var9) {
				;
			}

			getLogAgent().logInfo("Stopping!");

			try {
				this.loadWorld((WorldClient) null);
			} catch (final Throwable var8) {
				;
			}

			try {
				GLAllocation.deleteTexturesAndDisplayLists();
			} catch (final Throwable var7) {
				;
			}

			sndManager.closeMinecraft();
			Mouse.destroy();
			Keyboard.destroy();
		} finally {
			Display.destroy();

			if (!hasCrashed) {
				System.exit(0);
			}
		}

		System.gc();
	}

	@Override
	public void run() {
		running = true;

		try {
			startGame();
		} catch (final Exception var11) {
			var11.printStackTrace();
			displayCrashReport(addGraphicsAndWorldToCrashReport(new CrashReport(
					"Failed to start game", var11)));
			return;
		}

		try {
			while (running) {
				if (hasCrashed && crashReporter != null) {
					displayCrashReport(crashReporter);
					return;
				}

				if (refreshTexturePacksScheduled) {
					refreshTexturePacksScheduled = false;
					renderEngine.refreshTextures();
				}

				try {
					runGameLoop();
				} catch (final OutOfMemoryError var10) {
					freeMemory();
					displayGuiScreen(new GuiMemoryErrorScreen());
					System.gc();
				}
			}
		} catch (final MinecraftError var12) {
			;
		} catch (final ReportedException var13) {
			addGraphicsAndWorldToCrashReport(var13.getCrashReport());
			freeMemory();
			var13.printStackTrace();
			displayCrashReport(var13.getCrashReport());
		} catch (final Throwable var14) {
			final CrashReport var2 = addGraphicsAndWorldToCrashReport(new CrashReport(
					"Unexpected error", var14));
			freeMemory();
			var14.printStackTrace();
			displayCrashReport(var2);
		} finally {
			shutdownMinecraftApplet();
		}
	}

	/**
	 * Called repeatedly from run()
	 */
	private void runGameLoop() {
		if (mcApplet != null && !mcApplet.isActive()) {
			running = false;
		} else {
			AxisAlignedBB.getAABBPool().cleanPool();

			if (theWorld != null) {
				theWorld.getWorldVec3Pool().clear();
			}

			mcProfiler.startSection("root");

			if (mcCanvas == null && Display.isCloseRequested()) {
				shutdown();
			}

			if (isGamePaused && theWorld != null) {
				final float var1 = timer.renderPartialTicks;
				timer.updateTimer();
				timer.renderPartialTicks = var1;
			} else {
				timer.updateTimer();
			}

			final long var6 = System.nanoTime();
			mcProfiler.startSection("tick");

			for (int var3 = 0; var3 < timer.elapsedTicks; ++var3) {
				runTick();
			}

			mcProfiler.endStartSection("preRenderErrors");
			final long var7 = System.nanoTime() - var6;
			checkGLError("Pre render");
			RenderBlocks.fancyGrass = gameSettings.fancyGraphics;
			mcProfiler.endStartSection("sound");
			sndManager.setListener(thePlayer, timer.renderPartialTicks);

			if (!isGamePaused) {
				sndManager.func_92071_g();
			}

			mcProfiler.endSection();
			mcProfiler.startSection("render");
			mcProfiler.startSection("display");
			GL11.glEnable(GL11.GL_TEXTURE_2D);

			if (!Keyboard.isKeyDown(65)) {
				Display.update();
			}

			if (thePlayer != null && thePlayer.isEntityInsideOpaqueBlock()) {
				gameSettings.thirdPersonView = 0;
			}

			mcProfiler.endSection();

			if (!skipRenderWorld) {
				mcProfiler.endStartSection("gameRenderer");
				entityRenderer.updateCameraAndRender(timer.renderPartialTicks);
				mcProfiler.endSection();
			}

			GL11.glFlush();
			mcProfiler.endSection();

			if (!Display.isActive() && fullscreen) {
				toggleFullscreen();
			}

			if (gameSettings.showDebugInfo
					&& gameSettings.showDebugProfilerChart) {
				if (!mcProfiler.profilingEnabled) {
					mcProfiler.clearProfiling();
				}

				mcProfiler.profilingEnabled = true;
				displayDebugInfo(var7);
			} else {
				mcProfiler.profilingEnabled = false;
				prevFrameTime = System.nanoTime();
			}

			guiAchievement.updateAchievementWindow();
			mcProfiler.startSection("root");
			Thread.yield();

			if (Keyboard.isKeyDown(65)) {
				Display.update();
			}

			screenshotListener();

			if (mcCanvas != null
					&& !fullscreen
					&& (mcCanvas.getWidth() != displayWidth || mcCanvas
							.getHeight() != displayHeight)) {
				displayWidth = mcCanvas.getWidth();
				displayHeight = mcCanvas.getHeight();

				if (displayWidth <= 0) {
					displayWidth = 1;
				}

				if (displayHeight <= 0) {
					displayHeight = 1;
				}

				resize(displayWidth, displayHeight);
			}

			checkGLError("Post render");
			++fpsCounter;
			final boolean var5 = isGamePaused;
			isGamePaused = isSingleplayer() && currentScreen != null
					&& currentScreen.doesGuiPauseGame()
					&& !theIntegratedServer.getPublic();

			if (isIntegratedServerRunning() && thePlayer != null
					&& thePlayer.sendQueue != null && isGamePaused != var5) {
				((MemoryConnection) thePlayer.sendQueue.getNetManager())
						.setGamePaused(isGamePaused);
			}

			while (Minecraft.getSystemTime() >= debugUpdateTime + 1000L) {
				Minecraft.debugFPS = fpsCounter;
				debug = Minecraft.debugFPS + " fps, "
						+ WorldRenderer.chunksUpdated + " chunk updates";
				WorldRenderer.chunksUpdated = 0;
				debugUpdateTime += 1000L;
				fpsCounter = 0;
				usageSnooper.addMemoryStatsToSnooper();

				if (!usageSnooper.isSnooperRunning()) {
					usageSnooper.startSnooper();
				}
			}

			mcProfiler.endSection();

			if (func_90020_K() > 0) {
				Display.sync(EntityRenderer.performanceToFps(func_90020_K()));
			}
		}
	}

	private int func_90020_K() {
		return currentScreen != null && currentScreen instanceof GuiMainMenu ? 2
				: gameSettings.limitFramerate;
	}

	public void freeMemory() {
		try {
			Minecraft.memoryReserve = new byte[0];
			renderGlobal.deleteAllDisplayLists();
		} catch (final Throwable var4) {
			;
		}

		try {
			System.gc();
			AxisAlignedBB.getAABBPool().clearPool();
			theWorld.getWorldVec3Pool().clearAndFreeCache();
		} catch (final Throwable var3) {
			;
		}

		try {
			System.gc();
			this.loadWorld((WorldClient) null);
		} catch (final Throwable var2) {
			;
		}

		System.gc();
	}

	/**
	 * checks if keys are down
	 */
	private void screenshotListener() {
		if (Keyboard.isKeyDown(60)) {
			if (!isTakingScreenshot) {
				isTakingScreenshot = true;
				ingameGUI.getChatGUI().printChatMessage(
						ScreenShotHelper.saveScreenshot(Minecraft.minecraftDir,
								displayWidth, displayHeight));
			}
		} else {
			isTakingScreenshot = false;
		}
	}

	/**
	 * Update debugProfilerName in response to number keys in debug screen
	 */
	private void updateDebugProfilerName(int par1) {
		final List var2 = mcProfiler.getProfilingData(debugProfilerName);

		if (var2 != null && !var2.isEmpty()) {
			final ProfilerResult var3 = (ProfilerResult) var2.remove(0);

			if (par1 == 0) {
				if (var3.field_76331_c.length() > 0) {
					final int var4 = debugProfilerName.lastIndexOf(".");

					if (var4 >= 0) {
						debugProfilerName = debugProfilerName
								.substring(0, var4);
					}
				}
			} else {
				--par1;

				if (par1 < var2.size()
						&& !((ProfilerResult) var2.get(par1)).field_76331_c
								.equals("unspecified")) {
					if (debugProfilerName.length() > 0) {
						debugProfilerName = debugProfilerName + ".";
					}

					debugProfilerName = debugProfilerName
							+ ((ProfilerResult) var2.get(par1)).field_76331_c;
				}
			}
		}
	}

	private void displayDebugInfo(final long par1) {
		if (mcProfiler.profilingEnabled) {
			final List var3 = mcProfiler.getProfilingData(debugProfilerName);
			final ProfilerResult var4 = (ProfilerResult) var3.remove(0);
			GL11.glClear(GL11.GL_DEPTH_BUFFER_BIT);
			GL11.glMatrixMode(GL11.GL_PROJECTION);
			GL11.glEnable(GL11.GL_COLOR_MATERIAL);
			GL11.glLoadIdentity();
			GL11.glOrtho(0.0D, displayWidth, displayHeight, 0.0D, 1000.0D,
					3000.0D);
			GL11.glMatrixMode(GL11.GL_MODELVIEW);
			GL11.glLoadIdentity();
			GL11.glTranslatef(0.0F, 0.0F, -2000.0F);
			GL11.glLineWidth(1.0F);
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			final Tessellator var5 = Tessellator.instance;
			final short var6 = 160;
			final int var7 = displayWidth - var6 - 10;
			final int var8 = displayHeight - var6 * 2;
			GL11.glEnable(GL11.GL_BLEND);
			var5.startDrawingQuads();
			var5.setColorRGBA_I(0, 200);
			var5.addVertex(var7 - var6 * 1.1F, var8 - var6 * 0.6F - 16.0F, 0.0D);
			var5.addVertex(var7 - var6 * 1.1F, var8 + var6 * 2, 0.0D);
			var5.addVertex(var7 + var6 * 1.1F, var8 + var6 * 2, 0.0D);
			var5.addVertex(var7 + var6 * 1.1F, var8 - var6 * 0.6F - 16.0F, 0.0D);
			var5.draw();
			GL11.glDisable(GL11.GL_BLEND);
			double var9 = 0.0D;
			int var13;

			for (int var11 = 0; var11 < var3.size(); ++var11) {
				final ProfilerResult var12 = (ProfilerResult) var3.get(var11);
				var13 = MathHelper.floor_double(var12.field_76332_a / 4.0D) + 1;
				var5.startDrawing(6);
				var5.setColorOpaque_I(var12.func_76329_a());
				var5.addVertex(var7, var8, 0.0D);
				int var14;
				float var15;
				float var17;
				float var16;

				for (var14 = var13; var14 >= 0; --var14) {
					var15 = (float) ((var9 + var12.field_76332_a * var14
							/ var13)
							* Math.PI * 2.0D / 100.0D);
					var16 = MathHelper.sin(var15) * var6;
					var17 = MathHelper.cos(var15) * var6 * 0.5F;
					var5.addVertex(var7 + var16, var8 - var17, 0.0D);
				}

				var5.draw();
				var5.startDrawing(5);
				var5.setColorOpaque_I((var12.func_76329_a() & 16711422) >> 1);

				for (var14 = var13; var14 >= 0; --var14) {
					var15 = (float) ((var9 + var12.field_76332_a * var14
							/ var13)
							* Math.PI * 2.0D / 100.0D);
					var16 = MathHelper.sin(var15) * var6;
					var17 = MathHelper.cos(var15) * var6 * 0.5F;
					var5.addVertex(var7 + var16, var8 - var17, 0.0D);
					var5.addVertex(var7 + var16, var8 - var17 + 10.0F, 0.0D);
				}

				var5.draw();
				var9 += var12.field_76332_a;
			}

			final DecimalFormat var19 = new DecimalFormat("##0.00");
			GL11.glEnable(GL11.GL_TEXTURE_2D);
			String var18 = "";

			if (!var4.field_76331_c.equals("unspecified")) {
				var18 = var18 + "[0] ";
			}

			if (var4.field_76331_c.length() == 0) {
				var18 = var18 + "ROOT ";
			} else {
				var18 = var18 + var4.field_76331_c + " ";
			}

			var13 = 16777215;
			fontRenderer.drawStringWithShadow(var18, var7 - var6, var8 - var6
					/ 2 - 16, var13);
			fontRenderer.drawStringWithShadow(
					var18 = var19.format(var4.field_76330_b) + "%", var7 + var6
							- fontRenderer.getStringWidth(var18), var8 - var6
							/ 2 - 16, var13);

			for (int var21 = 0; var21 < var3.size(); ++var21) {
				final ProfilerResult var20 = (ProfilerResult) var3.get(var21);
				String var22 = "";

				if (var20.field_76331_c.equals("unspecified")) {
					var22 = var22 + "[?] ";
				} else {
					var22 = var22 + "[" + (var21 + 1) + "] ";
				}

				var22 = var22 + var20.field_76331_c;
				fontRenderer.drawStringWithShadow(var22, var7 - var6, var8
						+ var6 / 2 + var21 * 8 + 20, var20.func_76329_a());
				fontRenderer.drawStringWithShadow(
						var22 = var19.format(var20.field_76332_a) + "%",
						var7 + var6 - 50 - fontRenderer.getStringWidth(var22),
						var8 + var6 / 2 + var21 * 8 + 20, var20.func_76329_a());
				fontRenderer.drawStringWithShadow(
						var22 = var19.format(var20.field_76330_b) + "%", var7
								+ var6 - fontRenderer.getStringWidth(var22),
						var8 + var6 / 2 + var21 * 8 + 20, var20.func_76329_a());
			}
		}
	}

	/**
	 * Called when the window is closing. Sets 'running' to false which allows
	 * the game loop to exit cleanly.
	 */
	public void shutdown() {
		running = false;
	}

	/**
	 * Will set the focus to ingame if the Minecraft window is the active with
	 * focus. Also clears any GUI screen currently displayed
	 */
	public void setIngameFocus() {
		if (Display.isActive()) {
			if (!inGameHasFocus) {
				inGameHasFocus = true;
				mouseHelper.grabMouseCursor();
				displayGuiScreen((GuiScreen) null);
				leftClickCounter = 10000;
			}
		}
	}

	/**
	 * Resets the player keystate, disables the ingame focus, and ungrabs the
	 * mouse cursor.
	 */
	public void setIngameNotInFocus() {
		if (inGameHasFocus) {
			KeyBinding.unPressAllKeys();
			inGameHasFocus = false;
			mouseHelper.ungrabMouseCursor();
		}
	}

	/**
	 * Displays the ingame menu
	 */
	public void displayInGameMenu() {
		if (currentScreen == null) {
			displayGuiScreen(new GuiIngameMenu());

			if (isSingleplayer() && !theIntegratedServer.getPublic()) {
				sndManager.pauseAllSounds();
			}
		}
	}

	private void sendClickBlockToController(final int par1, final boolean par2) {
		if (!par2) {
			leftClickCounter = 0;
		}

		if (par1 != 0 || leftClickCounter <= 0) {
			if (par2 && objectMouseOver != null
					&& objectMouseOver.typeOfHit == EnumMovingObjectType.TILE
					&& par1 == 0) {
				final int var3 = objectMouseOver.blockX;
				final int var4 = objectMouseOver.blockY;
				final int var5 = objectMouseOver.blockZ;
				playerController.onPlayerDamageBlock(var3, var4, var5,
						objectMouseOver.sideHit);

				if (thePlayer.canCurrentToolHarvestBlock(var3, var4, var5)) {
					effectRenderer.addBlockHitEffects(var3, var4, var5,
							objectMouseOver.sideHit);
					thePlayer.swingItem();
				}
			} else {
				playerController.resetBlockRemoving();
			}
		}
	}

	/**
	 * Called whenever the mouse is clicked. Button clicked is 0 for left
	 * clicking and 1 for right clicking. Args: buttonClicked
	 */
	private void clickMouse(final int par1) {
		if (par1 != 0 || leftClickCounter <= 0) {
			if (par1 == 0) {
				thePlayer.swingItem();
			}

			if (par1 == 1) {
				rightClickDelayTimer = 4;
			}

			boolean var2 = true;
			final ItemStack var3 = thePlayer.inventory.getCurrentItem();

			if (objectMouseOver == null) {
				if (par1 == 0 && playerController.isNotCreative()) {
					leftClickCounter = 10;
				}
			} else if (objectMouseOver.typeOfHit == EnumMovingObjectType.ENTITY) {
				if (par1 == 0) {
					playerController.attackEntity(thePlayer,
							objectMouseOver.entityHit);
				}

				if (par1 == 1
						&& playerController.func_78768_b(thePlayer,
								objectMouseOver.entityHit)) {
					var2 = false;
				}
			} else if (objectMouseOver.typeOfHit == EnumMovingObjectType.TILE) {
				final int var4 = objectMouseOver.blockX;
				final int var5 = objectMouseOver.blockY;
				final int var6 = objectMouseOver.blockZ;
				final int var7 = objectMouseOver.sideHit;

				if (par1 == 0) {
					playerController.clickBlock(var4, var5, var6,
							objectMouseOver.sideHit);
				} else {
					final int var8 = var3 != null ? var3.stackSize : 0;

					if (playerController.onPlayerRightClick(thePlayer,
							theWorld, var3, var4, var5, var6, var7,
							objectMouseOver.hitVec)) {
						var2 = false;
						thePlayer.swingItem();
					}

					if (var3 == null) {
						return;
					}

					if (var3.stackSize == 0) {
						thePlayer.inventory.mainInventory[thePlayer.inventory.currentItem] = null;
					} else if (var3.stackSize != var8
							|| playerController.isInCreativeMode()) {
						entityRenderer.itemRenderer.resetEquippedProgress();
					}
				}
			}

			if (var2 && par1 == 1) {
				final ItemStack var9 = thePlayer.inventory.getCurrentItem();

				if (var9 != null
						&& playerController.sendUseItem(thePlayer, theWorld,
								var9)) {
					entityRenderer.itemRenderer.resetEquippedProgress2();
				}
			}
		}
	}

	/**
	 * Toggles fullscreen mode.
	 */
	public void toggleFullscreen() {
		try {
			fullscreen = !fullscreen;

			if (fullscreen) {
				Display.setDisplayMode(Display.getDesktopDisplayMode());
				displayWidth = Display.getDisplayMode().getWidth();
				displayHeight = Display.getDisplayMode().getHeight();

				if (displayWidth <= 0) {
					displayWidth = 1;
				}

				if (displayHeight <= 0) {
					displayHeight = 1;
				}
			} else {
				if (mcCanvas != null) {
					displayWidth = mcCanvas.getWidth();
					displayHeight = mcCanvas.getHeight();
				} else {
					displayWidth = tempDisplayWidth;
					displayHeight = tempDisplayHeight;
				}

				if (displayWidth <= 0) {
					displayWidth = 1;
				}

				if (displayHeight <= 0) {
					displayHeight = 1;
				}
			}

			if (currentScreen != null) {
				resize(displayWidth, displayHeight);
			}

			Display.setFullscreen(fullscreen);
			Display.setVSyncEnabled(gameSettings.enableVsync);
			Display.update();
		} catch (final Exception var2) {
			var2.printStackTrace();
		}
	}

	/**
	 * Called to resize the current screen.
	 */
	private void resize(final int par1, final int par2) {
		displayWidth = par1 <= 0 ? 1 : par1;
		displayHeight = par2 <= 0 ? 1 : par2;

		if (currentScreen != null) {
			final ScaledResolution var3 = new ScaledResolution(gameSettings,
					par1, par2);
			final int var4 = var3.getScaledWidth();
			final int var5 = var3.getScaledHeight();
			currentScreen.setWorldAndResolution(this, var4, var5);
		}
	}

	/**
	 * Runs the current tick.
	 */
	public void runTick() {
		if (rightClickDelayTimer > 0) {
			--rightClickDelayTimer;
		}

		mcProfiler.startSection("stats");
		statFileWriter.func_77449_e();
		mcProfiler.endStartSection("gui");

		if (!isGamePaused) {
			ingameGUI.updateTick();
		}

		mcProfiler.endStartSection("pick");
		entityRenderer.getMouseOver(1.0F);
		mcProfiler.endStartSection("gameMode");

		if (!isGamePaused && theWorld != null) {
			playerController.updateController();
		}

		renderEngine.bindTexture("/terrain.png");
		mcProfiler.endStartSection("textures");

		if (!isGamePaused) {
			renderEngine.updateDynamicTextures();
		}

		if (currentScreen == null && thePlayer != null) {
			if (thePlayer.getHealth() <= 0) {
				displayGuiScreen((GuiScreen) null);
			} else if (thePlayer.isPlayerSleeping() && theWorld != null) {
				displayGuiScreen(new GuiSleepMP());
			}
		} else if (currentScreen != null && currentScreen instanceof GuiSleepMP
				&& !thePlayer.isPlayerSleeping()) {
			displayGuiScreen((GuiScreen) null);
		}

		if (currentScreen != null) {
			leftClickCounter = 10000;
		}

		CrashReport var2;
		CrashReportCategory var3;

		if (currentScreen != null) {
			try {
				currentScreen.handleInput();
			} catch (final Throwable var6) {
				var2 = CrashReport.makeCrashReport(var6,
						"Updating screen events");
				var3 = var2.makeCategory("Affected screen");
				var3.addCrashSectionCallable("Screen name",
						new CallableUpdatingScreenName(this));
				throw new ReportedException(var2);
			}

			if (currentScreen != null) {
				try {
					currentScreen.guiParticles.update();
				} catch (final Throwable var5) {
					var2 = CrashReport.makeCrashReport(var5,
							"Ticking screen particles");
					var3 = var2.makeCategory("Affected screen");
					var3.addCrashSectionCallable("Screen name",
							new CallableParticleScreenName(this));
					throw new ReportedException(var2);
				}

				try {
					currentScreen.updateScreen();
				} catch (final Throwable var4) {
					var2 = CrashReport.makeCrashReport(var4, "Ticking screen");
					var3 = var2.makeCategory("Affected screen");
					var3.addCrashSectionCallable("Screen name",
							new CallableTickingScreenName(this));
					throw new ReportedException(var2);
				}
			}
		}

		if (currentScreen == null || currentScreen.allowUserInput) {
			mcProfiler.endStartSection("mouse");

			while (Mouse.next()) {
				KeyBinding.setKeyBindState(Mouse.getEventButton() - 100,
						Mouse.getEventButtonState());

				if (Mouse.getEventButtonState()) {
					KeyBinding.onTick(Mouse.getEventButton() - 100);
				}

				final long var1 = Minecraft.getSystemTime() - systemTime;

				if (var1 <= 200L) {
					int var10 = Mouse.getEventDWheel();

					if (var10 != 0) {
						thePlayer.inventory.changeCurrentItem(var10);

						if (gameSettings.noclip) {
							if (var10 > 0) {
								var10 = 1;
							}

							if (var10 < 0) {
								var10 = -1;
							}

							gameSettings.noclipRate += var10 * 0.25F;
						}
					}

					if (currentScreen == null) {
						if (!inGameHasFocus && Mouse.getEventButtonState()) {
							setIngameFocus();
						}
					} else if (currentScreen != null) {
						currentScreen.handleMouseInput();
					}
				}
			}

			if (leftClickCounter > 0) {
				--leftClickCounter;
			}

			mcProfiler.endStartSection("keyboard");
			boolean var8;

			while (Keyboard.next()) {
				KeyBinding.setKeyBindState(Keyboard.getEventKey(),
						Keyboard.getEventKeyState());

				if (Keyboard.getEventKeyState()) {
					KeyBinding.onTick(Keyboard.getEventKey());
				}

				if (field_83002_am > 0L) {
					if (Minecraft.getSystemTime() - field_83002_am >= 6000L) {
						throw new ReportedException(new CrashReport(
								"Manually triggered debug crash",
								new Throwable()));
					}

					if (!Keyboard.isKeyDown(46) || !Keyboard.isKeyDown(61)) {
						field_83002_am = -1L;
					}
				} else if (Keyboard.isKeyDown(46) && Keyboard.isKeyDown(61)) {
					field_83002_am = Minecraft.getSystemTime();
				}

				if (Keyboard.getEventKeyState()) {
					if (Keyboard.getEventKey() == 87) {
						toggleFullscreen();
					} else {
						if (currentScreen != null) {
							currentScreen.handleKeyboardInput();
						} else {
							if (Keyboard.getEventKey() == 1) {
								displayInGameMenu();
							}

							if (Keyboard.getEventKey() == 31
									&& Keyboard.isKeyDown(61)) {
								forceReload();
							}

							if (Keyboard.getEventKey() == 20
									&& Keyboard.isKeyDown(61)) {
								renderEngine.refreshTextures();
								renderGlobal.loadRenderers();
							}

							if (Keyboard.getEventKey() == 33
									&& Keyboard.isKeyDown(61)) {
								var8 = Keyboard.isKeyDown(42)
										| Keyboard.isKeyDown(54);
								gameSettings.setOptionValue(
										EnumOptions.RENDER_DISTANCE, var8 ? -1
												: 1);
							}

							if (Keyboard.getEventKey() == 30
									&& Keyboard.isKeyDown(61)) {
								renderGlobal.loadRenderers();
							}

							if (Keyboard.getEventKey() == 35
									&& Keyboard.isKeyDown(61)) {
								gameSettings.advancedItemTooltips = !gameSettings.advancedItemTooltips;
								gameSettings.saveOptions();
							}

							if (Keyboard.getEventKey() == 48
									&& Keyboard.isKeyDown(61)) {
								RenderManager.field_85095_o = !RenderManager.field_85095_o;
							}

							if (Keyboard.getEventKey() == 25
									&& Keyboard.isKeyDown(61)) {
								gameSettings.pauseOnLostFocus = !gameSettings.pauseOnLostFocus;
								gameSettings.saveOptions();
							}

							if (Keyboard.getEventKey() == 59) {
								gameSettings.hideGUI = !gameSettings.hideGUI;
							}

							if (Keyboard.getEventKey() == 61) {
								gameSettings.showDebugInfo = !gameSettings.showDebugInfo;
								gameSettings.showDebugProfilerChart = GuiScreen
										.isShiftKeyDown();
							}

							if (Keyboard.getEventKey() == 63) {
								++gameSettings.thirdPersonView;

								if (gameSettings.thirdPersonView > 2) {
									gameSettings.thirdPersonView = 0;
								}
							}

							if (Keyboard.getEventKey() == 66) {
								gameSettings.smoothCamera = !gameSettings.smoothCamera;
							}

							// TODO: Minecraft#runTick
							final KeyEvent event = new KeyEvent(
									Keyboard.getEventKey());
							Pringles.getInstance().getFactory()
									.getEventManager().sendEvent(event);
						}

						int var9;

						for (var9 = 0; var9 < 9; ++var9) {
							if (Keyboard.getEventKey() == 2 + var9) {
								thePlayer.inventory.currentItem = var9;
							}
						}

						if (gameSettings.showDebugInfo
								&& gameSettings.showDebugProfilerChart) {
							if (Keyboard.getEventKey() == 11) {
								updateDebugProfilerName(0);
							}

							for (var9 = 0; var9 < 9; ++var9) {
								if (Keyboard.getEventKey() == 2 + var9) {
									updateDebugProfilerName(var9 + 1);
								}
							}
						}
					}
				}
			}

			var8 = gameSettings.chatVisibility != 2;

			while (gameSettings.keyBindInventory.isPressed()) {
				displayGuiScreen(new GuiInventory(thePlayer));
			}

			while (gameSettings.keyBindDrop.isPressed()) {
				thePlayer.dropOneItem(GuiScreen.isCtrlKeyDown());
			}

			while (gameSettings.keyBindChat.isPressed() && var8) {
				displayGuiScreen(new GuiChat());
			}

			if (currentScreen == null
					&& gameSettings.keyBindCommand.isPressed() && var8) {
				displayGuiScreen(new GuiChat("/"));
			}

			if (thePlayer.isUsingItem()) {
				if (!gameSettings.keyBindUseItem.pressed) {
					playerController.onStoppedUsingItem(thePlayer);
				}

				label379:

				while (true) {
					if (!gameSettings.keyBindAttack.isPressed()) {
						while (gameSettings.keyBindUseItem.isPressed()) {
							;
						}

						while (true) {
							if (gameSettings.keyBindPickBlock.isPressed()) {
								continue;
							}

							break label379;
						}
					}
				}
			} else {
				while (gameSettings.keyBindAttack.isPressed()) {
					clickMouse(0);
				}

				while (gameSettings.keyBindUseItem.isPressed()) {
					clickMouse(1);
				}

				while (gameSettings.keyBindPickBlock.isPressed()) {
					clickMiddleMouseButton();
				}
			}

			if (gameSettings.keyBindUseItem.pressed
					&& rightClickDelayTimer == 0 && !thePlayer.isUsingItem()) {
				clickMouse(1);
			}

			sendClickBlockToController(0, currentScreen == null
					&& gameSettings.keyBindAttack.pressed && inGameHasFocus);
		}

		if (theWorld != null) {
			if (thePlayer != null) {
				++joinPlayerCounter;

				if (joinPlayerCounter == 30) {
					joinPlayerCounter = 0;
					theWorld.joinEntityInSurroundings(thePlayer);
				}
			}

			mcProfiler.endStartSection("gameRenderer");

			if (!isGamePaused) {
				entityRenderer.updateRenderer();
			}

			mcProfiler.endStartSection("levelRenderer");

			if (!isGamePaused) {
				renderGlobal.updateClouds();
			}

			mcProfiler.endStartSection("level");

			if (!isGamePaused) {
				if (theWorld.lastLightningBolt > 0) {
					--theWorld.lastLightningBolt;
				}

				theWorld.updateEntities();
			}

			if (!isGamePaused) {
				theWorld.setAllowedSpawnTypes(theWorld.difficultySetting > 0,
						true);

				try {
					theWorld.tick();
				} catch (final Throwable var7) {
					var2 = CrashReport.makeCrashReport(var7,
							"Exception in world tick");

					if (theWorld == null) {
						var3 = var2.makeCategory("Affected level");
						var3.addCrashSection("Problem", "Level is null!");
					} else {
						theWorld.addWorldInfoToCrashReport(var2);
					}

					throw new ReportedException(var2);
				}
			}

			mcProfiler.endStartSection("animateTick");

			if (!isGamePaused && theWorld != null) {
				theWorld.doVoidFogParticles(
						MathHelper.floor_double(thePlayer.posX),
						MathHelper.floor_double(thePlayer.posY),
						MathHelper.floor_double(thePlayer.posZ));
			}

			mcProfiler.endStartSection("particles");

			if (!isGamePaused) {
				effectRenderer.updateEffects();
			}
		} else if (myNetworkManager != null) {
			mcProfiler.endStartSection("pendingConnection");
			myNetworkManager.processReadPackets();
		}

		mcProfiler.endSection();
		systemTime = Minecraft.getSystemTime();
	}

	/**
	 * Forces a reload of the sound manager and all the resources. Called in
	 * game by holding 'F3' and pressing 'S'.
	 */
	private void forceReload() {
		getLogAgent().logInfo("FORCING RELOAD!");

		if (sndManager != null) {
			sndManager.stopAllSounds();
		}

		sndManager = new SoundManager();
		sndManager.loadSoundSettings(gameSettings);
		downloadResourcesThread.reloadResources();
	}

	/**
	 * Arguments: World foldername, World ingame name, WorldSettings
	 */
	public void launchIntegratedServer(final String par1Str,
			final String par2Str, WorldSettings par3WorldSettings) {
		this.loadWorld((WorldClient) null);
		System.gc();
		final ISaveHandler var4 = saveLoader.getSaveLoader(par1Str, false);
		WorldInfo var5 = var4.loadWorldInfo();

		if (var5 == null && par3WorldSettings != null) {
			statFileWriter.readStat(StatList.createWorldStat, 1);
			var5 = new WorldInfo(par3WorldSettings, par1Str);
			var4.saveWorldInfo(var5);
		}

		if (par3WorldSettings == null) {
			par3WorldSettings = new WorldSettings(var5);
		}

		statFileWriter.readStat(StatList.startGameStat, 1);
		theIntegratedServer = new IntegratedServer(this, par1Str, par2Str,
				par3WorldSettings);
		theIntegratedServer.startServerThread();
		integratedServerIsRunning = true;
		loadingScreen.displayProgressMessage(StatCollector
				.translateToLocal("menu.loadingLevel"));

		while (!theIntegratedServer.serverIsInRunLoop()) {
			final String var6 = theIntegratedServer.getUserMessage();

			if (var6 != null) {
				loadingScreen.resetProgresAndWorkingMessage(StatCollector
						.translateToLocal(var6));
			} else {
				loadingScreen.resetProgresAndWorkingMessage("");
			}

			try {
				Thread.sleep(200L);
			} catch (final InterruptedException var9) {
				;
			}
		}

		displayGuiScreen((GuiScreen) null);

		try {
			final NetClientHandler var10 = new NetClientHandler(this,
					theIntegratedServer);
			myNetworkManager = var10.getNetManager();
		} catch (final IOException var8) {
			displayCrashReport(addGraphicsAndWorldToCrashReport(new CrashReport(
					"Connecting to integrated server", var8)));
		}
	}

	/**
	 * unloads the current world first
	 */
	public void loadWorld(final WorldClient par1WorldClient) {
		this.loadWorld(par1WorldClient, "");
	}

	/**
	 * par2Str is displayed on the loading screen to the user unloads the
	 * current world first
	 */
	public void loadWorld(final WorldClient par1WorldClient,
			final String par2Str) {
		statFileWriter.syncStats();

		if (par1WorldClient == null) {
			final NetClientHandler var3 = getNetHandler();

			if (var3 != null) {
				var3.cleanup();
			}

			if (myNetworkManager != null) {
				myNetworkManager.closeConnections();
			}

			if (theIntegratedServer != null) {
				theIntegratedServer.initiateShutdown();
			}

			theIntegratedServer = null;
		}

		renderViewEntity = null;
		myNetworkManager = null;

		if (loadingScreen != null) {
			loadingScreen.resetProgressAndMessage(par2Str);
			loadingScreen.resetProgresAndWorkingMessage("");
		}

		if (par1WorldClient == null && theWorld != null) {
			if (texturePackList.getIsDownloading()) {
				texturePackList.onDownloadFinished();
			}

			setServerData((ServerData) null);
			integratedServerIsRunning = false;
		}

		sndManager.playStreaming((String) null, 0.0F, 0.0F, 0.0F);
		sndManager.stopAllSounds();
		theWorld = par1WorldClient;

		if (par1WorldClient != null) {
			if (renderGlobal != null) {
				renderGlobal.setWorldAndLoadRenderers(par1WorldClient);
			}

			if (effectRenderer != null) {
				effectRenderer.clearEffects(par1WorldClient);
			}

			if (thePlayer == null) {
				thePlayer = playerController.func_78754_a(par1WorldClient);
				playerController.flipPlayer(thePlayer);
			}

			thePlayer.preparePlayerToSpawn();
			par1WorldClient.spawnEntityInWorld(thePlayer);
			thePlayer.movementInput = new MovementInputFromOptions(gameSettings);
			playerController.setPlayerCapabilities(thePlayer);
			renderViewEntity = thePlayer;
		} else {
			saveLoader.flushCache();
			thePlayer = null;
		}

		System.gc();
		systemTime = 0L;
	}

	/**
	 * Installs a resource. Currently only sounds are download so this method
	 * just adds them to the SoundManager.
	 */
	public void installResource(String par1Str, final File par2File) {
		final int var3 = par1Str.indexOf("/");
		final String var4 = par1Str.substring(0, var3);
		par1Str = par1Str.substring(var3 + 1);

		if (var4.equalsIgnoreCase("sound3")) {
			sndManager.addSound(par1Str, par2File);
		} else if (var4.equalsIgnoreCase("streaming")) {
			sndManager.addStreaming(par1Str, par2File);
		} else if (!var4.equalsIgnoreCase("music")
				&& !var4.equalsIgnoreCase("newmusic")) {
			if (var4.equalsIgnoreCase("lang")) {
				StringTranslate.getInstance().func_94519_a(par1Str, par2File);
			}
		} else {
			sndManager.addMusic(par1Str, par2File);
		}
	}

	/**
	 * A String of renderGlobal.getDebugInfoRenders
	 */
	public String debugInfoRenders() {
		return renderGlobal.getDebugInfoRenders();
	}

	/**
	 * Gets the information in the F3 menu about how many entities are
	 * infront/around you
	 */
	public String getEntityDebug() {
		return renderGlobal.getDebugInfoEntities();
	}

	/**
	 * Gets the name of the world's current chunk provider
	 */
	public String getWorldProviderName() {
		return theWorld.getProviderName();
	}

	/**
	 * A String of how many entities are in the world
	 */
	public String debugInfoEntities() {
		return "P: " + effectRenderer.getStatistics() + ". T: "
				+ theWorld.getDebugLoadedEntities();
	}

	public void setDimensionAndSpawnPlayer(final int par1) {
		theWorld.setSpawnLocation();
		theWorld.removeAllEntities();
		int var2 = 0;

		if (thePlayer != null) {
			var2 = thePlayer.entityId;
			theWorld.removeEntity(thePlayer);
		}

		renderViewEntity = null;
		thePlayer = playerController.func_78754_a(theWorld);
		thePlayer.dimension = par1;
		renderViewEntity = thePlayer;
		thePlayer.preparePlayerToSpawn();
		theWorld.spawnEntityInWorld(thePlayer);
		playerController.flipPlayer(thePlayer);
		thePlayer.movementInput = new MovementInputFromOptions(gameSettings);
		thePlayer.entityId = var2;
		playerController.setPlayerCapabilities(thePlayer);

		if (currentScreen instanceof GuiGameOver) {
			displayGuiScreen((GuiScreen) null);
		}
	}

	/**
	 * Sets whether this is a demo or not.
	 */
	void setDemo(final boolean par1) {
		isDemo = par1;
	}

	/**
	 * Gets whether this is a demo or not.
	 */
	public final boolean isDemo() {
		return isDemo;
	}

	/**
	 * Returns the NetClientHandler.
	 */
	public NetClientHandler getNetHandler() {
		return thePlayer != null ? thePlayer.sendQueue : null;
	}

	public static void main(final String[] par0ArrayOfStr) {
		final HashMap var1 = new HashMap();
		boolean var2 = false;
		boolean var3 = true;
		final boolean var4 = false;
		final String var5 = "Player" + Minecraft.getSystemTime() % 1000L;
		String var6 = var5;

		if (par0ArrayOfStr.length > 0) {
			var6 = par0ArrayOfStr[0];
		}

		String var7 = "-";

		if (par0ArrayOfStr.length > 1) {
			var7 = par0ArrayOfStr[1];
		}

		final ArrayList var8 = new ArrayList();

		for (int var9 = 2; var9 < par0ArrayOfStr.length; ++var9) {
			final String var10 = par0ArrayOfStr[var9];
			final String var11 = var9 == par0ArrayOfStr.length - 1 ? null
					: par0ArrayOfStr[var9 + 1];
			boolean var12 = false;

			if (!var10.equals("-demo") && !var10.equals("--demo")) {
				if (var10.equals("--applet")) {
					var3 = false;
				} else if (var10.equals("--password") && var11 != null) {
					final String[] var13 = HttpUtil.loginToMinecraft(
							(ILogAgent) null, var6, var11);

					if (var13 != null) {
						var6 = var13[0];
						var7 = var13[1];
						var8.add("Logged in insecurely as " + var6);
					} else {
						var8.add("Could not log in as " + var6
								+ " with given password");
					}

					var12 = true;
				}
			} else {
				var2 = true;
			}

			if (var12) {
				++var9;
			}
		}

		if (var6.contains("@") && var7.length() <= 1) {
			var6 = var5;
		}

		var1.put("demo", "" + var2);
		var1.put("stand-alone", "" + var3);
		var1.put("username", var6);
		var1.put("fullscreen", "" + var4);
		var1.put("sessionid", var7);
		final Frame var16 = new Frame();
		var16.setTitle("Minecraft");
		var16.setBackground(Color.BLACK);
		final JPanel var17 = new JPanel();
		var16.setLayout(new BorderLayout());
		var17.setPreferredSize(new Dimension(854, 480));
		var16.add(var17, "Center");
		var16.pack();
		var16.setLocationRelativeTo((Component) null);
		var16.setVisible(true);
		var16.addWindowListener(new GameWindowListener());
		final MinecraftFakeLauncher var15 = new MinecraftFakeLauncher(var1);
		final MinecraftApplet var18 = new MinecraftApplet();
		var18.setStub(var15);
		var15.setLayout(new BorderLayout());
		var15.add(var18, "Center");
		var15.validate();
		var16.removeAll();
		var16.setLayout(new BorderLayout());
		var16.add(var15, "Center");
		var16.validate();
		var18.init();
		final Iterator var19 = var8.iterator();

		while (var19.hasNext()) {
			final String var14 = (String) var19.next();
			Minecraft.getMinecraft().getLogAgent().logInfo(var14);
		}

		var18.start();
		Runtime.getRuntime().addShutdownHook(new ThreadShutdown());
	}

	public static boolean isGuiEnabled() {
		return Minecraft.theMinecraft == null
				|| !Minecraft.theMinecraft.gameSettings.hideGUI;
	}

	public static boolean isFancyGraphicsEnabled() {
		return Minecraft.theMinecraft != null
				&& Minecraft.theMinecraft.gameSettings.fancyGraphics;
	}

	/**
	 * Returns if ambient occlusion is enabled
	 */
	public static boolean isAmbientOcclusionEnabled() {
		return Minecraft.theMinecraft != null
				&& Minecraft.theMinecraft.gameSettings.ambientOcclusion != 0;
	}

	/**
	 * Returns true if the message is a client command and should not be sent to
	 * the server. However there are no such commands at this point in time.
	 */
	public boolean handleClientCommand(final String par1Str) {
		return !par1Str.startsWith("/") ? false : false;
	}

	/**
	 * Called when the middle mouse button gets clicked
	 */
	private void clickMiddleMouseButton() {
		if (objectMouseOver != null) {
			final boolean var1 = thePlayer.capabilities.isCreativeMode;
			int var3 = 0;
			boolean var4 = false;
			int var2;
			int var5;

			if (objectMouseOver.typeOfHit == EnumMovingObjectType.TILE) {
				var5 = objectMouseOver.blockX;
				final int var6 = objectMouseOver.blockY;
				final int var7 = objectMouseOver.blockZ;
				final Block var8 = Block.blocksList[theWorld.getBlockId(var5,
						var6, var7)];

				if (var8 == null) {
					return;
				}

				var2 = var8.idPicked(theWorld, var5, var6, var7);

				if (var2 == 0) {
					return;
				}

				var4 = Item.itemsList[var2].getHasSubtypes();
				final int var9 = var2 < 256
						&& !Block.blocksList[var8.blockID].isFlowerPot() ? var2
						: var8.blockID;
				var3 = Block.blocksList[var9].getDamageValue(theWorld, var5,
						var6, var7);
			} else {
				if (objectMouseOver.typeOfHit != EnumMovingObjectType.ENTITY
						|| objectMouseOver.entityHit == null || !var1) {
					return;
				}

				if (objectMouseOver.entityHit instanceof EntityPainting) {
					var2 = Item.painting.itemID;
				} else if (objectMouseOver.entityHit instanceof EntityItemFrame) {
					final EntityItemFrame var10 = (EntityItemFrame) objectMouseOver.entityHit;

					if (var10.getDisplayedItem() == null) {
						var2 = Item.itemFrame.itemID;
					} else {
						var2 = var10.getDisplayedItem().itemID;
						var3 = var10.getDisplayedItem().getItemDamage();
						var4 = true;
					}
				} else if (objectMouseOver.entityHit instanceof EntityMinecart) {
					final EntityMinecart var11 = (EntityMinecart) objectMouseOver.entityHit;

					if (var11.getMinecartType() == 2) {
						var2 = Item.minecartPowered.itemID;
					} else if (var11.getMinecartType() == 1) {
						var2 = Item.minecartCrate.itemID;
					} else if (var11.getMinecartType() == 3) {
						var2 = Item.minecartTnt.itemID;
					} else if (var11.getMinecartType() == 5) {
						var2 = Item.minecartHopper.itemID;
					} else {
						var2 = Item.minecartEmpty.itemID;
					}
				} else if (objectMouseOver.entityHit instanceof EntityBoat) {
					var2 = Item.boat.itemID;
				} else {
					var2 = Item.monsterPlacer.itemID;
					var3 = EntityList.getEntityID(objectMouseOver.entityHit);
					var4 = true;

					if (var3 <= 0
							|| !EntityList.entityEggs.containsKey(Integer
									.valueOf(var3))) {
						return;
					}
				}
			}

			thePlayer.inventory.setCurrentItem(var2, var3, var4, var1);

			if (var1) {
				var5 = thePlayer.inventoryContainer.inventorySlots.size() - 9
						+ thePlayer.inventory.currentItem;
				playerController.sendSlotPacket(thePlayer.inventory
						.getStackInSlot(thePlayer.inventory.currentItem), var5);
			}
		}
	}

	/**
	 * adds core server Info (GL version , Texture pack, isModded, type), and
	 * the worldInfo to the crash report
	 */
	public CrashReport addGraphicsAndWorldToCrashReport(
			final CrashReport par1CrashReport) {
		par1CrashReport.func_85056_g().addCrashSectionCallable("LWJGL",
				new CallableLWJGLVersion(this));
		par1CrashReport.func_85056_g().addCrashSectionCallable("OpenGL",
				new CallableGLInfo(this));
		par1CrashReport.func_85056_g().addCrashSectionCallable("Is Modded",
				new CallableModded(this));
		par1CrashReport.func_85056_g().addCrashSectionCallable("Type",
				new CallableType2(this));
		par1CrashReport.func_85056_g().addCrashSectionCallable("Texture Pack",
				new CallableTexturePack(this));
		par1CrashReport.func_85056_g().addCrashSectionCallable(
				"Profiler Position", new CallableClientProfiler(this));
		par1CrashReport.func_85056_g().addCrashSectionCallable(
				"Vec3 Pool Size", new CallableClientMemoryStats(this));

		if (theWorld != null) {
			theWorld.addWorldInfoToCrashReport(par1CrashReport);
		}

		return par1CrashReport;
	}

	/**
	 * Return the singleton Minecraft instance for the game
	 */
	public static Minecraft getMinecraft() {
		return Minecraft.theMinecraft;
	}

	/**
	 * Sets refreshTexturePacksScheduled to true, triggering a texture pack
	 * refresh next time the while(running) loop is run
	 */
	public void scheduleTexturePackRefresh() {
		refreshTexturePacksScheduled = true;
	}

	@Override
	public void addServerStatsToSnooper(
			final PlayerUsageSnooper par1PlayerUsageSnooper) {
		par1PlayerUsageSnooper.addData("fps",
				Integer.valueOf(Minecraft.debugFPS));
		par1PlayerUsageSnooper.addData("texpack_name", texturePackList
				.getSelectedTexturePack().getTexturePackFileName());
		par1PlayerUsageSnooper.addData("vsync_enabled",
				Boolean.valueOf(gameSettings.enableVsync));
		par1PlayerUsageSnooper.addData("display_frequency",
				Integer.valueOf(Display.getDisplayMode().getFrequency()));
		par1PlayerUsageSnooper.addData("display_type",
				fullscreen ? "fullscreen" : "windowed");

		if (theIntegratedServer != null
				&& theIntegratedServer.getPlayerUsageSnooper() != null) {
			par1PlayerUsageSnooper.addData("snooper_partner",
					theIntegratedServer.getPlayerUsageSnooper().getUniqueID());
		}
	}

	@Override
	public void addServerTypeToSnooper(
			final PlayerUsageSnooper par1PlayerUsageSnooper) {
		par1PlayerUsageSnooper.addData("opengl_version",
				GL11.glGetString(GL11.GL_VERSION));
		par1PlayerUsageSnooper.addData("opengl_vendor",
				GL11.glGetString(GL11.GL_VENDOR));
		par1PlayerUsageSnooper.addData("client_brand",
				ClientBrandRetriever.getClientModName());
		par1PlayerUsageSnooper.addData("applet",
				Boolean.valueOf(hideQuitButton));
		final ContextCapabilities var2 = GLContext.getCapabilities();
		par1PlayerUsageSnooper.addData("gl_caps[ARB_multitexture]",
				Boolean.valueOf(var2.GL_ARB_multitexture));
		par1PlayerUsageSnooper.addData("gl_caps[ARB_multisample]",
				Boolean.valueOf(var2.GL_ARB_multisample));
		par1PlayerUsageSnooper.addData("gl_caps[ARB_texture_cube_map]",
				Boolean.valueOf(var2.GL_ARB_texture_cube_map));
		par1PlayerUsageSnooper.addData("gl_caps[ARB_vertex_blend]",
				Boolean.valueOf(var2.GL_ARB_vertex_blend));
		par1PlayerUsageSnooper.addData("gl_caps[ARB_matrix_palette]",
				Boolean.valueOf(var2.GL_ARB_matrix_palette));
		par1PlayerUsageSnooper.addData("gl_caps[ARB_vertex_program]",
				Boolean.valueOf(var2.GL_ARB_vertex_program));
		par1PlayerUsageSnooper.addData("gl_caps[ARB_vertex_shader]",
				Boolean.valueOf(var2.GL_ARB_vertex_shader));
		par1PlayerUsageSnooper.addData("gl_caps[ARB_fragment_program]",
				Boolean.valueOf(var2.GL_ARB_fragment_program));
		par1PlayerUsageSnooper.addData("gl_caps[ARB_fragment_shader]",
				Boolean.valueOf(var2.GL_ARB_fragment_shader));
		par1PlayerUsageSnooper.addData("gl_caps[ARB_shader_objects]",
				Boolean.valueOf(var2.GL_ARB_shader_objects));
		par1PlayerUsageSnooper.addData("gl_caps[ARB_vertex_buffer_object]",
				Boolean.valueOf(var2.GL_ARB_vertex_buffer_object));
		par1PlayerUsageSnooper.addData("gl_caps[ARB_framebuffer_object]",
				Boolean.valueOf(var2.GL_ARB_framebuffer_object));
		par1PlayerUsageSnooper.addData("gl_caps[ARB_pixel_buffer_object]",
				Boolean.valueOf(var2.GL_ARB_pixel_buffer_object));
		par1PlayerUsageSnooper.addData("gl_caps[ARB_uniform_buffer_object]",
				Boolean.valueOf(var2.GL_ARB_uniform_buffer_object));
		par1PlayerUsageSnooper.addData("gl_caps[ARB_texture_non_power_of_two]",
				Boolean.valueOf(var2.GL_ARB_texture_non_power_of_two));
		par1PlayerUsageSnooper.addData("gl_caps[gl_max_vertex_uniforms]",
				Integer.valueOf(GL11
						.glGetInteger(GL20.GL_MAX_VERTEX_UNIFORM_COMPONENTS)));
		par1PlayerUsageSnooper
				.addData(
						"gl_caps[gl_max_fragment_uniforms]",
						Integer.valueOf(GL11
								.glGetInteger(GL20.GL_MAX_FRAGMENT_UNIFORM_COMPONENTS)));
		par1PlayerUsageSnooper.addData("gl_max_texture_size",
				Integer.valueOf(Minecraft.getGLMaximumTextureSize()));
	}

	/**
	 * Used in the usage snooper.
	 */
	public static int getGLMaximumTextureSize() {
		for (int var0 = 16384; var0 > 0; var0 >>= 1) {
			GL11.glTexImage2D(GL11.GL_PROXY_TEXTURE_2D, 0, GL11.GL_RGBA, var0,
					var0, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE,
					(ByteBuffer) null);
			final int var1 = GL11.glGetTexLevelParameteri(
					GL11.GL_PROXY_TEXTURE_2D, 0, GL11.GL_TEXTURE_WIDTH);

			if (var1 != 0) {
				return var0;
			}
		}

		return -1;
	}

	/**
	 * Returns whether snooping is enabled or not.
	 */
	@Override
	public boolean isSnooperEnabled() {
		return gameSettings.snooperEnabled;
	}

	/**
	 * Set the current ServerData instance.
	 */
	public void setServerData(final ServerData par1ServerData) {
		currentServerData = par1ServerData;
	}

	/**
	 * Get the current ServerData instance.
	 */
	public ServerData getServerData() {
		return currentServerData;
	}

	public boolean isIntegratedServerRunning() {
		return integratedServerIsRunning;
	}

	/**
	 * Returns true if there is only one player playing, and the current server
	 * is the integrated one.
	 */
	public boolean isSingleplayer() {
		return integratedServerIsRunning && theIntegratedServer != null;
	}

	/**
	 * Returns the currently running integrated server
	 */
	public IntegratedServer getIntegratedServer() {
		return theIntegratedServer;
	}

	public static void stopIntegratedServer() {
		if (Minecraft.theMinecraft != null) {
			final IntegratedServer var0 = Minecraft.theMinecraft
					.getIntegratedServer();

			if (var0 != null) {
				var0.stopServer();
			}
		}
	}

	/**
	 * Returns the PlayerUsageSnooper instance.
	 */
	public PlayerUsageSnooper getPlayerUsageSnooper() {
		return usageSnooper;
	}

	/**
	 * Gets the system time in milliseconds.
	 */
	public static long getSystemTime() {
		return Sys.getTime() * 1000L / Sys.getTimerResolution();
	}

	/**
	 * Returns whether we're in full screen or not.
	 */
	public boolean isFullScreen() {
		return fullscreen;
	}

	@Override
	public ILogAgent getLogAgent() {
		return field_94139_O;
	}
}
